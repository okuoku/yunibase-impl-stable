/* Generated from eval.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: eval.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file eval.c -emit-import-library chicken.eval -emit-import-library chicken.load
   unit: eval
   uses: modules library internal expand
*/
#include "chicken.h"


#ifndef C_INSTALL_EGG_HOME
# define C_INSTALL_EGG_HOME    "."
#endif

#ifndef C_INSTALL_SHARE_HOME
# define C_INSTALL_SHARE_HOME NULL
#endif

#ifndef C_BINARY_VERSION
# define C_BINARY_VERSION      0
#endif


#define C_store_result(x, ptr)   (*((C_word *)C_block_item(ptr, 0)) = (x), C_SCHEME_TRUE)


#define C_copy_result_string(str, buf, n)  (C_memcpy((char *)C_block_item(buf, 0), C_c_string(str), C_unfix(n)), ((char *)C_block_item(buf, 0))[ C_unfix(n) ] = '\0', C_SCHEME_TRUE)


C_externexport  void  CHICKEN_get_error_message(char *t0,int t1);

C_externexport  int  CHICKEN_load(char * t0);

C_externexport  int  CHICKEN_read(char * t0,C_word *t1);

C_externexport  int  CHICKEN_apply_to_string(C_word t0,C_word t1,char *t2,int t3);

C_externexport  int  CHICKEN_apply(C_word t0,C_word t1,C_word *t2);

C_externexport  int  CHICKEN_eval_string_to_string(char * t0,char *t1,int t2);

C_externexport  int  CHICKEN_eval_to_string(C_word t0,char *t1,int t2);

C_externexport  int  CHICKEN_eval_string(char * t0,C_word *t1);

C_externexport  int  CHICKEN_eval(C_word t0,C_word *t1);

C_externexport  int  CHICKEN_yield();

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_modules_toplevel)
C_externimport void C_ccall C_modules_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[370];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,15),40,102,105,110,100,45,105,100,32,105,100,32,115,101,41,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,7),40,97,51,54,52,53,41,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,13),40,97,51,54,53,49,32,46,32,116,109,112,41,0,0,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,12),40,114,101,110,97,109,101,32,118,97,114,41,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,8),40,103,50,51,55,32,112,41};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,101,110,118,115,32,101,105,41,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,15),40,108,111,111,107,117,112,32,118,97,114,48,32,101,41,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,31),40,101,109,105,116,45,116,114,97,99,101,45,105,110,102,111,32,105,110,102,111,32,99,110,116,114,32,101,32,118,41,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,9),40,97,51,53,51,51,32,120,41,0,0,0,0,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,11),40,97,51,53,52,54,32,112,32,105,41,0,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,15),40,100,101,99,111,114,97,116,101,32,112,32,108,108,41,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,12),40,102,95,51,55,57,53,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,7),40,97,51,56,48,54,41,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,10),40,102,95,51,56,51,51,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,12),40,102,95,51,56,52,52,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,12),40,102,95,51,56,52,57,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,10),40,102,95,51,57,48,55,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,10),40,102,95,51,57,50,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,10),40,102,95,51,57,52,49,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,10),40,102,95,51,57,54,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,10),40,102,95,51,57,56,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,11),40,97,51,56,49,50,32,105,32,106,41,0,0,0,0,0};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,49,49,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,49,57,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,50,55,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,51,53,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,51,55,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,53,54,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,53,56,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,12),40,102,95,52,48,54,57,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,50,50,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,51,48,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,51,56,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,52,54,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,53,52,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,54,50,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,55,48,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,12),40,102,95,52,49,55,50,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,12),40,102,95,52,50,48,49,32,46,32,118,41,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,10),40,102,95,52,50,51,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,10),40,102,95,52,50,53,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,10),40,102,95,52,51,51,55,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,10),40,102,95,52,51,54,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,7),40,97,52,52,51,48,41,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,10),40,102,95,52,52,53,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,10),40,102,95,52,52,54,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,10),40,102,95,52,52,57,55,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,10),40,102,95,52,53,49,48,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,11),40,97,52,52,51,54,32,105,32,106,41,0,0,0,0,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,10),40,102,95,52,53,55,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,10),40,102,95,52,54,49,49,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,10),40,102,95,52,54,54,54,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,10),40,102,95,52,55,51,54,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,8),40,103,53,49,56,32,120,41};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,19),40,100,111,108,111,111,112,53,51,57,32,105,32,118,108,105,115,116,41,0,0,0,0,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,10),40,102,95,52,56,49,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,53,49,50,32,103,53,50,52,41,0,0,0,0,0,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,7),40,97,52,57,48,49,41,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,7),40,97,52,57,49,56,41,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,7),40,97,52,57,51,54,41,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,52,52,53,32,103,52,53,55,41,0,0,0,0,0,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,52,49,55,32,103,52,50,57,41,0,0,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,53,56,48,32,103,53,57,50,41,0,0,0,0,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,53,53,51,32,103,53,54,53,41,0,0,0,0,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,23),40,109,97,112,45,108,111,111,112,55,50,57,32,103,55,52,49,32,103,55,52,50,41,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,23),40,109,97,112,45,108,111,111,112,54,57,54,32,103,55,48,56,32,103,55,48,57,41,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,54,57,32,103,54,56,49,41,0,0,0,0,0,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,51,57,32,103,54,53,49,41,0,0,0,0,0,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,54,49,50,32,103,54,50,52,41,0,0,0,0,0,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,11),40,97,53,53,50,55,32,46,32,114,41,0,0,0,0,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,10),40,102,95,53,53,50,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,7),40,97,53,53,52,54,41,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,10),40,102,95,53,53,52,49,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,14),40,97,53,53,55,48,32,97,49,32,46,32,114,41,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,10),40,102,95,53,53,54,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,10),40,97,53,53,56,57,32,97,49,41,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,10),40,102,95,53,53,56,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,17),40,97,53,54,49,55,32,97,49,32,97,50,32,46,32,114,41,0,0,0,0,0,0,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,10),40,102,95,53,54,49,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,13),40,97,53,54,51,54,32,97,49,32,97,50,41,0,0,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,10),40,102,95,53,54,51,49,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,20),40,97,53,54,54,52,32,97,49,32,97,50,32,97,51,32,46,32,114,41,0,0,0,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,10),40,102,95,53,54,53,57,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,16),40,97,53,54,56,51,32,97,49,32,97,50,32,97,51,41};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,10),40,102,95,53,54,55,56,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,23),40,97,53,55,49,49,32,97,49,32,97,50,32,97,51,32,97,52,32,46,32,114,41,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,10),40,102,95,53,55,48,54,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,19),40,97,53,55,51,48,32,97,49,32,97,50,32,97,51,32,97,52,41,0,0,0,0,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,10),40,102,95,53,55,50,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,26),40,100,111,108,111,111,112,49,50,50,57,32,110,32,99,32,97,114,103,115,32,108,97,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,12),40,97,53,55,53,50,32,46,32,97,115,41,0,0,0,0};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,10),40,102,95,53,55,52,55,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,12),40,97,53,55,55,53,32,46,32,97,115,41,0,0,0,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,10),40,102,95,53,55,55,48,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,7),40,97,53,56,49,51,41,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,7),40,97,53,56,51,48,41,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,7),40,97,53,56,52,52,41,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,55,56,48,32,103,55,57,50,41,0,0,0,0,0,0};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,22),40,97,53,52,57,50,32,118,97,114,115,32,97,114,103,99,32,114,101,115,116,41,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,7),40,97,53,56,57,56,41,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,24),40,97,53,57,48,56,32,108,108,105,115,116,55,54,57,32,98,111,100,121,55,55,48,41};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,7),40,97,53,57,51,51,41,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,7),40,97,53,57,53,48,41,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,7),40,97,53,57,54,56,41,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,56,56,51,32,103,56,57,53,41,0,0,0,0,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,6),40,103,57,52,55,41,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,18),40,102,111,114,45,101,97,99,104,45,108,111,111,112,57,52,54,41,0,0,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,7),40,97,54,49,50,54,41,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,7),40,97,54,49,52,51,41,0};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,7),40,97,54,49,54,49,41,0};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,18),40,109,97,112,45,108,111,111,112,57,49,56,32,103,57,51,48,41,0,0,0,0,0,0};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,13),40,97,54,51,51,53,32,102,111,114,109,115,41,0,0,0};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,7),40,97,54,52,48,48,41,0};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,19),40,109,97,112,45,108,111,111,112,57,57,49,32,103,49,48,48,51,41,0,0,0,0,0};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,7),40,97,54,52,55,56,41,0};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,50,32,120,115,41,0,0,0,0,0,0};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,10),40,102,95,54,53,54,56,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,14),40,108,111,111,112,32,98,111,100,121,32,120,115,41,0,0};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,7),40,97,54,53,52,48,41,0};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,7),40,97,54,53,51,52,41,0};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,7),40,97,54,54,51,48,41,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,11),40,103,49,48,51,52,32,101,120,112,41,0,0,0,0,0};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,48,50,56,32,103,49,48,52,48,41,0,0,0,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,7),40,97,54,56,51,54,41,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,21),40,97,54,56,52,50,32,101,120,112,49,49,53,51,32,95,49,49,53,53,41,0,0,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,10),40,103,49,50,49,49,32,99,108,41,0,0,0,0,0,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,7),40,97,55,48,49,49,41,0};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,7),40,97,55,48,49,54,41,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,7),40,97,55,48,50,54,41,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,27),40,99,111,109,112,105,108,101,32,120,32,101,32,104,32,116,102,32,99,110,116,114,32,116,108,63,41,0,0,0,0,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,110,41};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,10),40,102,95,55,51,54,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,10),40,102,95,55,51,56,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,10),40,102,95,55,52,49,54,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,10),40,102,95,55,52,53,53,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,10),40,102,95,55,53,48,50,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,9),40,103,49,51,48,50,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,9),40,103,49,51,51,48,32,97,41,0,0,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,51,50,52,32,103,49,51,51,54,41,0,0,0,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,10),40,102,95,55,53,53,48,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,49,50,57,54,32,103,49,51,48,56,41,0,0,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,12),40,102,95,55,54,54,48,32,46,32,95,41,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,26),40,99,111,109,112,105,108,101,45,99,97,108,108,32,120,32,101,32,116,102,32,99,110,116,114,41,0,0,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,46),40,99,104,105,99,107,101,110,46,101,118,97,108,35,99,111,109,112,105,108,101,45,116,111,45,99,108,111,115,117,114,101,32,101,120,112,32,101,110,118,32,114,101,115,116,41,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,7),40,97,55,55,52,48,41,0};
static C_char C_TLS li147[] C_aligned={C_lihdr(0,0,7),40,97,55,55,56,57,41,0};
static C_char C_TLS li148[] C_aligned={C_lihdr(0,0,7),40,97,55,55,57,56,41,0};
static C_char C_TLS li149[] C_aligned={C_lihdr(0,0,7),40,97,55,55,56,51,41,0};
static C_char C_TLS li150[] C_aligned={C_lihdr(0,0,7),40,97,55,56,49,53,41,0};
static C_char C_TLS li151[] C_aligned={C_lihdr(0,0,22),40,35,35,115,121,115,35,101,118,97,108,47,109,101,116,97,32,102,111,114,109,41,0,0};
static C_char C_TLS li152[] C_aligned={C_lihdr(0,0,21),40,115,99,104,101,109,101,35,101,118,97,108,32,120,32,46,32,101,110,118,41,0,0,0};
static C_char C_TLS li153[] C_aligned={C_lihdr(0,0,38),40,99,104,105,99,107,101,110,46,101,118,97,108,35,109,111,100,117,108,101,45,101,110,118,105,114,111,110,109,101,110,116,32,110,97,109,101,41,0,0};
static C_char C_TLS li154[] C_aligned={C_lihdr(0,0,5),40,101,114,114,41,0,0,0};
static C_char C_TLS li155[] C_aligned={C_lihdr(0,0,22),40,108,111,111,112,32,108,108,105,115,116,32,118,97,114,115,32,97,114,103,99,41,0,0};
static C_char C_TLS li156[] C_aligned={C_lihdr(0,0,38),40,35,35,115,121,115,35,100,101,99,111,109,112,111,115,101,45,108,97,109,98,100,97,45,108,105,115,116,32,108,108,105,115,116,48,32,107,41,0,0};
static C_char C_TLS li157[] C_aligned={C_lihdr(0,0,32),40,115,99,104,101,109,101,35,105,110,116,101,114,97,99,116,105,111,110,45,101,110,118,105,114,111,110,109,101,110,116,41};
static C_char C_TLS li158[] C_aligned={C_lihdr(0,0,17),40,102,111,108,100,114,49,53,50,51,32,103,49,53,50,52,41,0,0,0,0,0,0,0};
static C_char C_TLS li159[] C_aligned={C_lihdr(0,0,10),40,115,116,114,105,112,32,115,101,41,0,0,0,0,0,0};
static C_char C_TLS li160[] C_aligned={C_lihdr(0,0,36),40,115,99,104,101,109,101,35,115,99,104,101,109,101,45,114,101,112,111,114,116,45,101,110,118,105,114,111,110,109,101,110,116,32,110,41,0,0,0,0};
static C_char C_TLS li161[] C_aligned={C_lihdr(0,0,27),40,115,99,104,101,109,101,35,110,117,108,108,45,101,110,118,105,114,111,110,109,101,110,116,32,110,41,0,0,0,0,0};
static C_char C_TLS li162[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li163[] C_aligned={C_lihdr(0,0,27),40,99,104,105,99,107,101,110,46,108,111,97,100,35,97,110,121,32,112,114,101,100,32,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li164[] C_aligned={C_lihdr(0,0,18),40,35,35,115,121,115,35,112,114,111,118,105,100,101,32,105,100,41,0,0,0,0,0,0};
static C_char C_TLS li165[] C_aligned={C_lihdr(0,0,20),40,35,35,115,121,115,35,112,114,111,118,105,100,101,100,63,32,105,100,41,0,0,0,0};
static C_char C_TLS li166[] C_aligned={C_lihdr(0,0,6),40,108,111,111,112,41,0,0};
static C_char C_TLS li167[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,108,111,97,100,35,112,97,116,104,45,115,101,112,97,114,97,116,111,114,45,105,110,100,101,120,47,114,105,103,104,116,32,115,41,0,0,0,0,0};
static C_char C_TLS li168[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,108,111,97,100,35,109,97,107,101,45,114,101,108,97,116,105,118,101,45,112,97,116,104,110,97,109,101,32,102,114,111,109,32,102,105,108,101,41,0};
static C_char C_TLS li169[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,32,109,111,100,101,41,0,0,0,0,0};
static C_char C_TLS li170[] C_aligned={C_lihdr(0,0,42),40,99,104,105,99,107,101,110,46,108,111,97,100,35,115,101,116,45,100,121,110,97,109,105,99,45,108,111,97,100,45,109,111,100,101,33,32,109,111,100,101,41,0,0,0,0,0,0};
static C_char C_TLS li171[] C_aligned={C_lihdr(0,0,34),40,99,104,105,99,107,101,110,46,108,111,97,100,35,99,45,116,111,112,108,101,118,101,108,32,110,97,109,101,32,108,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li172[] C_aligned={C_lihdr(0,0,13),40,102,95,57,54,50,50,32,112,97,116,104,41,0,0,0};
static C_char C_TLS li173[] C_aligned={C_lihdr(0,0,7),40,97,57,54,56,48,41,0};
static C_char C_TLS li174[] C_aligned={C_lihdr(0,0,7),40,97,57,54,57,56,41,0};
static C_char C_TLS li175[] C_aligned={C_lihdr(0,0,7),40,97,57,55,53,48,41,0};
static C_char C_TLS li176[] C_aligned={C_lihdr(0,0,11),40,97,57,55,53,54,32,46,32,116,41,0,0,0,0,0};
static C_char C_TLS li177[] C_aligned={C_lihdr(0,0,7),40,97,57,55,51,56,41,0};
static C_char C_TLS li178[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,51,48,55,32,103,50,51,49,52,41,0,0,0,0,0,0,0};
static C_char C_TLS li179[] C_aligned={C_lihdr(0,0,17),40,97,57,55,55,50,32,46,32,114,101,115,117,108,116,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li180[] C_aligned={C_lihdr(0,0,14),40,100,111,108,111,111,112,50,50,57,55,32,120,41,0,0};
static C_char C_TLS li181[] C_aligned={C_lihdr(0,0,7),40,97,57,55,48,49,41,0};
static C_char C_TLS li182[] C_aligned={C_lihdr(0,0,7),40,97,57,56,51,50,41,0};
static C_char C_TLS li183[] C_aligned={C_lihdr(0,0,7),40,97,57,54,56,57,41,0};
static C_char C_TLS li184[] C_aligned={C_lihdr(0,0,7),40,97,57,56,52,49,41,0};
static C_char C_TLS li185[] C_aligned={C_lihdr(0,0,7),40,97,57,54,55,52,41,0};
static C_char C_TLS li186[] C_aligned={C_lihdr(0,0,49),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,47,105,110,116,101,114,110,97,108,32,105,110,112,117,116,32,101,118,97,108,117,97,116,111,114,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li187[] C_aligned={C_lihdr(0,0,29),40,115,99,104,101,109,101,35,108,111,97,100,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li188[] C_aligned={C_lihdr(0,0,44),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,45,114,101,108,97,116,105,118,101,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0};
static C_char C_TLS li189[] C_aligned={C_lihdr(0,0,8),40,97,49,48,48,50,56,41};
static C_char C_TLS li190[] C_aligned={C_lihdr(0,0,8),40,97,49,48,48,51,49,41};
static C_char C_TLS li191[] C_aligned={C_lihdr(0,0,8),40,97,49,48,48,51,52,41};
static C_char C_TLS li192[] C_aligned={C_lihdr(0,0,43),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,45,110,111,105,115,105,108,121,32,102,105,108,101,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0,0};
static C_char C_TLS li193[] C_aligned={C_lihdr(0,0,16),40,99,111,109,112,108,101,116,101,32,103,50,51,56,49,41};
static C_char C_TLS li194[] C_aligned={C_lihdr(0,0,11),40,108,111,111,112,32,108,105,98,115,41,0,0,0,0,0};
static C_char C_TLS li195[] C_aligned={C_lihdr(0,0,50),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,45,108,105,98,114,97,114,121,47,105,110,116,101,114,110,97,108,32,117,110,97,109,101,32,108,105,98,32,108,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li196[] C_aligned={C_lihdr(0,0,33),40,35,35,115,121,115,35,108,111,97,100,45,108,105,98,114,97,114,121,32,117,110,97,109,101,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li197[] C_aligned={C_lihdr(0,0,40),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,45,108,105,98,114,97,114,121,32,117,110,97,109,101,32,46,32,114,101,115,116,41};
static C_char C_TLS li198[] C_aligned={C_lihdr(0,0,8),40,97,49,48,50,50,56,41};
static C_char C_TLS li199[] C_aligned={C_lihdr(0,0,17),40,100,111,108,111,111,112,50,52,56,51,32,120,32,120,115,41,0,0,0,0,0,0,0};
static C_char C_TLS li200[] C_aligned={C_lihdr(0,0,8),40,97,49,48,50,51,51,41};
static C_char C_TLS li201[] C_aligned={C_lihdr(0,0,8),40,97,49,48,50,55,48,41};
static C_char C_TLS li202[] C_aligned={C_lihdr(0,0,8),40,97,49,48,50,50,50,41};
static C_char C_TLS li203[] C_aligned={C_lihdr(0,0,49),40,35,35,115,121,115,35,105,110,99,108,117,100,101,45,102,111,114,109,115,45,102,114,111,109,45,102,105,108,101,32,102,105,108,101,110,97,109,101,32,115,111,117,114,99,101,32,107,41,0,0,0,0,0,0,0};
static C_char C_TLS li204[] C_aligned={C_lihdr(0,0,32),40,99,104,105,99,107,101,110,46,108,111,97,100,35,102,105,108,101,45,101,120,105,115,116,115,63,32,110,97,109,101,41};
static C_char C_TLS li205[] C_aligned={C_lihdr(0,0,41),40,99,104,105,99,107,101,110,46,108,111,97,100,35,102,105,110,100,45,102,105,108,101,32,110,97,109,101,32,115,101,97,114,99,104,45,112,97,116,104,41,0,0,0,0,0,0,0};
static C_char C_TLS li206[] C_aligned={C_lihdr(0,0,12),40,99,104,101,99,107,32,112,97,116,104,41,0,0,0,0};
static C_char C_TLS li207[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,112,97,116,104,115,41,0,0,0,0};
static C_char C_TLS li208[] C_aligned={C_lihdr(0,0,45),40,99,104,105,99,107,101,110,46,108,111,97,100,35,102,105,110,100,45,100,121,110,97,109,105,99,45,101,120,116,101,110,115,105,111,110,32,105,100,32,105,110,99,63,41,0,0,0};
static C_char C_TLS li209[] C_aligned={C_lihdr(0,0,11),40,103,50,53,52,57,32,101,120,116,41,0,0,0,0,0};
static C_char C_TLS li210[] C_aligned={C_lihdr(0,0,47),40,99,104,105,99,107,101,110,46,108,111,97,100,35,108,111,97,100,45,101,120,116,101,110,115,105,111,110,32,105,100,32,97,108,116,101,114,110,97,116,101,115,32,108,111,99,41,0};
static C_char C_TLS li211[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,53,54,50,41,0,0,0,0,0};
static C_char C_TLS li212[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,53,55,50,32,103,50,53,55,57,41,0,0,0,0,0,0,0};
static C_char C_TLS li213[] C_aligned={C_lihdr(0,0,28),40,99,104,105,99,107,101,110,46,108,111,97,100,35,114,101,113,117,105,114,101,32,46,32,105,100,115,41,0,0,0,0};
static C_char C_TLS li214[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,54,49,53,41,0,0,0,0,0};
static C_char C_TLS li215[] C_aligned={C_lihdr(0,0,25),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,54,50,53,32,103,50,54,51,50,41,0,0,0,0,0,0,0};
static C_char C_TLS li216[] C_aligned={C_lihdr(0,0,28),40,99,104,105,99,107,101,110,46,108,111,97,100,35,112,114,111,118,105,100,101,32,46,32,105,100,115,41,0,0,0,0};
static C_char C_TLS li217[] C_aligned={C_lihdr(0,0,19),40,102,111,114,45,101,97,99,104,45,108,111,111,112,50,54,54,56,41,0,0,0,0,0};
static C_char C_TLS li218[] C_aligned={C_lihdr(0,0,10),40,108,111,111,112,32,108,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li219[] C_aligned={C_lihdr(0,0,30),40,99,104,105,99,107,101,110,46,108,111,97,100,35,112,114,111,118,105,100,101,100,63,32,46,32,105,100,115,41,0,0};
static C_char C_TLS li220[] C_aligned={C_lihdr(0,0,14),40,97,49,48,56,49,53,32,103,50,55,51,48,41,0,0};
static C_char C_TLS li221[] C_aligned={C_lihdr(0,0,34),40,35,35,115,121,115,35,112,114,111,99,101,115,115,45,114,101,113,117,105,114,101,32,108,105,98,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li222[] C_aligned={C_lihdr(0,0,27),40,116,101,115,116,45,101,120,116,101,110,115,105,111,110,115,32,102,110,97,109,101,32,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li223[] C_aligned={C_lihdr(0,0,12),40,116,101,115,116,32,102,110,97,109,101,41,0,0,0,0};
static C_char C_TLS li224[] C_aligned={C_lihdr(0,0,12),40,108,111,111,112,32,112,97,116,104,115,41,0,0,0,0};
static C_char C_TLS li225[] C_aligned={C_lihdr(0,0,55),40,35,35,115,121,115,35,114,101,115,111,108,118,101,45,105,110,99,108,117,100,101,45,102,105,108,101,110,97,109,101,32,102,110,97,109,101,32,101,120,116,115,32,114,101,112,111,32,115,111,117,114,99,101,41,0};
static C_char C_TLS li226[] C_aligned={C_lihdr(0,0,8),40,97,49,49,48,51,57,41};
static C_char C_TLS li227[] C_aligned={C_lihdr(0,0,11),40,97,49,49,48,51,51,32,101,120,41,0,0,0,0,0};
static C_char C_TLS li228[] C_aligned={C_lihdr(0,0,8),40,97,49,49,48,53,56,41};
static C_char C_TLS li229[] C_aligned={C_lihdr(0,0,8),40,97,49,49,48,55,48,41};
static C_char C_TLS li230[] C_aligned={C_lihdr(0,0,15),40,97,49,49,48,54,52,32,46,32,97,114,103,115,41,0};
static C_char C_TLS li231[] C_aligned={C_lihdr(0,0,8),40,97,49,49,48,53,50,41};
static C_char C_TLS li232[] C_aligned={C_lihdr(0,0,10),40,97,49,49,48,50,55,32,107,41,0,0,0,0,0,0};
static C_char C_TLS li233[] C_aligned={C_lihdr(0,0,16),40,114,117,110,45,115,97,102,101,32,116,104,117,110,107,41};
static C_char C_TLS li234[] C_aligned={C_lihdr(0,0,23),40,115,116,111,114,101,45,114,101,115,117,108,116,32,120,32,114,101,115,117,108,116,41,0};
static C_char C_TLS li235[] C_aligned={C_lihdr(0,0,8),40,97,49,49,48,57,49,41};
static C_char C_TLS li236[] C_aligned={C_lihdr(0,0,15),40,67,72,73,67,75,69,78,95,121,105,101,108,100,41,0};
static C_char C_TLS li237[] C_aligned={C_lihdr(0,0,8),40,97,49,49,49,48,51,41};
static C_char C_TLS li238[] C_aligned={C_lihdr(0,0,25),40,67,72,73,67,75,69,78,95,101,118,97,108,32,101,120,112,32,114,101,115,117,108,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li239[] C_aligned={C_lihdr(0,0,8),40,97,49,49,49,50,50,41};
static C_char C_TLS li240[] C_aligned={C_lihdr(0,0,32),40,67,72,73,67,75,69,78,95,101,118,97,108,95,115,116,114,105,110,103,32,115,116,114,32,114,101,115,117,108,116,41};
static C_char C_TLS li241[] C_aligned={C_lihdr(0,0,26),40,115,116,111,114,101,45,115,116,114,105,110,103,32,98,117,102,115,105,122,101,32,98,117,102,41,0,0,0,0,0,0};
static C_char C_TLS li242[] C_aligned={C_lihdr(0,0,8),40,97,49,49,49,53,56,41};
static C_char C_TLS li243[] C_aligned={C_lihdr(0,0,40),40,67,72,73,67,75,69,78,95,101,118,97,108,95,116,111,95,115,116,114,105,110,103,32,101,120,112,32,98,117,102,32,98,117,102,115,105,122,101,41};
static C_char C_TLS li244[] C_aligned={C_lihdr(0,0,8),40,97,49,49,49,56,55,41};
static C_char C_TLS li245[] C_aligned={C_lihdr(0,0,47),40,67,72,73,67,75,69,78,95,101,118,97,108,95,115,116,114,105,110,103,95,116,111,95,115,116,114,105,110,103,32,115,116,114,32,98,117,102,32,98,117,102,115,105,122,101,41,0};
static C_char C_TLS li246[] C_aligned={C_lihdr(0,0,8),40,97,49,49,50,50,49,41};
static C_char C_TLS li247[] C_aligned={C_lihdr(0,0,32),40,67,72,73,67,75,69,78,95,97,112,112,108,121,32,102,117,110,99,32,97,114,103,115,32,114,101,115,117,108,116,41};
static C_char C_TLS li248[] C_aligned={C_lihdr(0,0,8),40,97,49,49,50,51,55,41};
static C_char C_TLS li249[] C_aligned={C_lihdr(0,0,47),40,67,72,73,67,75,69,78,95,97,112,112,108,121,95,116,111,95,115,116,114,105,110,103,32,102,117,110,99,32,97,114,103,115,32,98,117,102,32,98,117,102,115,105,122,101,41,0};
static C_char C_TLS li250[] C_aligned={C_lihdr(0,0,8),40,97,49,49,50,54,54,41};
static C_char C_TLS li251[] C_aligned={C_lihdr(0,0,25),40,67,72,73,67,75,69,78,95,114,101,97,100,32,115,116,114,32,114,101,115,117,108,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li252[] C_aligned={C_lihdr(0,0,8),40,97,49,49,50,56,56,41};
static C_char C_TLS li253[] C_aligned={C_lihdr(0,0,18),40,67,72,73,67,75,69,78,95,108,111,97,100,32,115,116,114,41,0,0,0,0,0,0};
static C_char C_TLS li254[] C_aligned={C_lihdr(0,0,39),40,67,72,73,67,75,69,78,95,103,101,116,95,101,114,114,111,114,95,109,101,115,115,97,103,101,32,98,117,102,32,98,117,102,115,105,122,101,41,0};
static C_char C_TLS li255[] C_aligned={C_lihdr(0,0,10),40,97,49,49,51,49,53,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li256[] C_aligned={C_lihdr(0,0,20),40,109,97,112,45,108,111,111,112,50,51,56,56,32,103,50,52,48,48,41,0,0,0,0};
static C_char C_TLS li257[] C_aligned={C_lihdr(0,0,12),40,97,49,49,52,52,57,32,101,32,112,41,0,0,0,0};
static C_char C_TLS li258[] C_aligned={C_lihdr(0,0,21),40,97,49,49,52,55,49,32,115,121,109,32,112,114,111,112,32,118,97,108,41,0,0,0};
static C_char C_TLS li259[] C_aligned={C_lihdr(0,0,8),40,97,49,49,52,57,56,41};
static C_char C_TLS li260[] C_aligned={C_lihdr(0,0,12),40,100,111,108,111,111,112,49,52,56,51,41,0,0,0,0};
static C_char C_TLS li261[] C_aligned={C_lihdr(0,0,8),40,97,49,49,53,48,51,41};
static C_char C_TLS li262[] C_aligned={C_lihdr(0,0,14),40,97,49,49,52,57,50,32,116,104,117,110,107,41,0,0};
static C_char C_TLS li263[] C_aligned={C_lihdr(0,0,8),40,97,49,49,52,54,53,41};
static C_char C_TLS li264[] C_aligned={C_lihdr(0,0,63),40,97,49,49,53,51,51,32,35,35,115,121,115,35,112,117,116,47,114,101,115,116,111,114,101,33,49,52,55,48,32,35,35,115,121,115,35,119,105,116,104,45,112,114,111,112,101,114,116,121,45,114,101,115,116,111,114,101,49,52,55,49,41,0};
static C_char C_TLS li265[] C_aligned={C_lihdr(0,0,8),40,97,49,49,53,54,56,41};
static C_char C_TLS li266[] C_aligned={C_lihdr(0,0,8),40,97,49,49,53,57,56,41};
static C_char C_TLS li267[] C_aligned={C_lihdr(0,0,8),40,97,49,49,54,48,56,41};
static C_char C_TLS li268[] C_aligned={C_lihdr(0,0,17),40,97,49,49,53,51,56,32,120,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li269[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


C_noret_decl(f_10003)
static void C_ccall f_10003(C_word c,C_word *av) C_noret;
C_noret_decl(f_10014)
static void C_ccall f_10014(C_word c,C_word *av) C_noret;
C_noret_decl(f_10018)
static void C_ccall f_10018(C_word c,C_word *av) C_noret;
C_noret_decl(f_10021)
static void C_ccall f_10021(C_word c,C_word *av) C_noret;
C_noret_decl(f_10024)
static void C_ccall f_10024(C_word c,C_word *av) C_noret;
C_noret_decl(f_10029)
static void C_ccall f_10029(C_word c,C_word *av) C_noret;
C_noret_decl(f_10032)
static void C_ccall f_10032(C_word c,C_word *av) C_noret;
C_noret_decl(f_10035)
static void C_ccall f_10035(C_word c,C_word *av) C_noret;
C_noret_decl(f_10040)
static void C_ccall f_10040(C_word c,C_word *av) C_noret;
C_noret_decl(f_10042)
static void C_fcall f_10042(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10049)
static void C_ccall f_10049(C_word c,C_word *av) C_noret;
C_noret_decl(f_10051)
static void C_fcall f_10051(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10055)
static void C_fcall f_10055(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10058)
static void C_ccall f_10058(C_word c,C_word *av) C_noret;
C_noret_decl(f_10061)
static void C_ccall f_10061(C_word c,C_word *av) C_noret;
C_noret_decl(f_10066)
static void C_fcall f_10066(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10080)
static void C_ccall f_10080(C_word c,C_word *av) C_noret;
C_noret_decl(f_10083)
static void C_ccall f_10083(C_word c,C_word *av) C_noret;
C_noret_decl(f_10097)
static void C_ccall f_10097(C_word c,C_word *av) C_noret;
C_noret_decl(f_10104)
static void C_ccall f_10104(C_word c,C_word *av) C_noret;
C_noret_decl(f_10107)
static void C_ccall f_10107(C_word c,C_word *av) C_noret;
C_noret_decl(f_10110)
static void C_ccall f_10110(C_word c,C_word *av) C_noret;
C_noret_decl(f_10123)
static void C_ccall f_10123(C_word c,C_word *av) C_noret;
C_noret_decl(f_10127)
static void C_ccall f_10127(C_word c,C_word *av) C_noret;
C_noret_decl(f_10133)
static void C_ccall f_10133(C_word c,C_word *av) C_noret;
C_noret_decl(f_10152)
static void C_ccall f_10152(C_word c,C_word *av) C_noret;
C_noret_decl(f_10155)
static void C_ccall f_10155(C_word c,C_word *av) C_noret;
C_noret_decl(f_10181)
static void C_ccall f_10181(C_word c,C_word *av) C_noret;
C_noret_decl(f_10208)
static void C_ccall f_10208(C_word c,C_word *av) C_noret;
C_noret_decl(f_10212)
static void C_ccall f_10212(C_word c,C_word *av) C_noret;
C_noret_decl(f_10215)
static void C_ccall f_10215(C_word c,C_word *av) C_noret;
C_noret_decl(f_10218)
static void C_ccall f_10218(C_word c,C_word *av) C_noret;
C_noret_decl(f_10223)
static void C_ccall f_10223(C_word c,C_word *av) C_noret;
C_noret_decl(f_10229)
static void C_ccall f_10229(C_word c,C_word *av) C_noret;
C_noret_decl(f_10234)
static void C_ccall f_10234(C_word c,C_word *av) C_noret;
C_noret_decl(f_10242)
static void C_ccall f_10242(C_word c,C_word *av) C_noret;
C_noret_decl(f_10244)
static void C_fcall f_10244(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10258)
static void C_ccall f_10258(C_word c,C_word *av) C_noret;
C_noret_decl(f_10265)
static void C_ccall f_10265(C_word c,C_word *av) C_noret;
C_noret_decl(f_10271)
static void C_ccall f_10271(C_word c,C_word *av) C_noret;
C_noret_decl(f_10277)
static void C_ccall f_10277(C_word c,C_word *av) C_noret;
C_noret_decl(f_10289)
static void C_fcall f_10289(C_word t0,C_word t1) C_noret;
C_noret_decl(f_10296)
static void C_ccall f_10296(C_word c,C_word *av) C_noret;
C_noret_decl(f_10298)
static void C_ccall f_10298(C_word c,C_word *av) C_noret;
C_noret_decl(f_10327)
static void C_ccall f_10327(C_word c,C_word *av) C_noret;
C_noret_decl(f_10339)
static void C_ccall f_10339(C_word c,C_word *av) C_noret;
C_noret_decl(f_10345)
static void C_ccall f_10345(C_word c,C_word *av) C_noret;
C_noret_decl(f_10349)
static void C_ccall f_10349(C_word c,C_word *av) C_noret;
C_noret_decl(f_10352)
static void C_ccall f_10352(C_word c,C_word *av) C_noret;
C_noret_decl(f_10354)
static void C_fcall f_10354(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10358)
static void C_ccall f_10358(C_word c,C_word *av) C_noret;
C_noret_decl(f_10361)
static void C_ccall f_10361(C_word c,C_word *av) C_noret;
C_noret_decl(f_10371)
static void C_ccall f_10371(C_word c,C_word *av) C_noret;
C_noret_decl(f_10383)
static void C_ccall f_10383(C_word c,C_word *av) C_noret;
C_noret_decl(f_10390)
static void C_ccall f_10390(C_word c,C_word *av) C_noret;
C_noret_decl(f_10397)
static void C_ccall f_10397(C_word c,C_word *av) C_noret;
C_noret_decl(f_10399)
static void C_fcall f_10399(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10412)
static void C_ccall f_10412(C_word c,C_word *av) C_noret;
C_noret_decl(f_10450)
static void C_ccall f_10450(C_word c,C_word *av) C_noret;
C_noret_decl(f_10456)
static void C_ccall f_10456(C_word c,C_word *av) C_noret;
C_noret_decl(f_10471)
static void C_ccall f_10471(C_word c,C_word *av) C_noret;
C_noret_decl(f_10475)
static void C_fcall f_10475(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10479)
static void C_ccall f_10479(C_word c,C_word *av) C_noret;
C_noret_decl(f_10490)
static void C_ccall f_10490(C_word c,C_word *av) C_noret;
C_noret_decl(f_10494)
static void C_ccall f_10494(C_word c,C_word *av) C_noret;
C_noret_decl(f_10496)
static void C_ccall f_10496(C_word c,C_word *av) C_noret;
C_noret_decl(f_10515)
static void C_fcall f_10515(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10525)
static void C_ccall f_10525(C_word c,C_word *av) C_noret;
C_noret_decl(f_10538)
static C_word C_fcall f_10538(C_word t0);
C_noret_decl(f_10561)
static void C_ccall f_10561(C_word c,C_word *av) C_noret;
C_noret_decl(f_10580)
static void C_fcall f_10580(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10590)
static void C_ccall f_10590(C_word c,C_word *av) C_noret;
C_noret_decl(f_10603)
static C_word C_fcall f_10603(C_word t0);
C_noret_decl(f_10626)
static void C_ccall f_10626(C_word c,C_word *av) C_noret;
C_noret_decl(f_10640)
static C_word C_fcall f_10640(C_word t0);
C_noret_decl(f_10665)
static void C_ccall f_10665(C_word c,C_word *av) C_noret;
C_noret_decl(f_10699)
static void C_ccall f_10699(C_word c,C_word *av) C_noret;
C_noret_decl(f_10741)
static void C_ccall f_10741(C_word c,C_word *av) C_noret;
C_noret_decl(f_10784)
static void C_ccall f_10784(C_word c,C_word *av) C_noret;
C_noret_decl(f_10816)
static void C_ccall f_10816(C_word c,C_word *av) C_noret;
C_noret_decl(f_10886)
static void C_ccall f_10886(C_word c,C_word *av) C_noret;
C_noret_decl(f_10889)
static void C_fcall f_10889(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_10902)
static void C_ccall f_10902(C_word c,C_word *av) C_noret;
C_noret_decl(f_10905)
static void C_ccall f_10905(C_word c,C_word *av) C_noret;
C_noret_decl(f_10908)
static void C_ccall f_10908(C_word c,C_word *av) C_noret;
C_noret_decl(f_10922)
static void C_fcall f_10922(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10958)
static void C_ccall f_10958(C_word c,C_word *av) C_noret;
C_noret_decl(f_10961)
static void C_ccall f_10961(C_word c,C_word *av) C_noret;
C_noret_decl(f_10971)
static void C_ccall f_10971(C_word c,C_word *av) C_noret;
C_noret_decl(f_10973)
static void C_fcall f_10973(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_10983)
static void C_ccall f_10983(C_word c,C_word *av) C_noret;
C_noret_decl(f_10997)
static void C_ccall f_10997(C_word c,C_word *av) C_noret;
C_noret_decl(f_11008)
static void C_ccall f_11008(C_word c,C_word *av) C_noret;
C_noret_decl(f_11015)
static void C_ccall f_11015(C_word c,C_word *av) C_noret;
C_noret_decl(f_11018)
static void C_fcall f_11018(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11023)
static void C_ccall f_11023(C_word c,C_word *av) C_noret;
C_noret_decl(f_11028)
static void C_ccall f_11028(C_word c,C_word *av) C_noret;
C_noret_decl(f_11034)
static void C_ccall f_11034(C_word c,C_word *av) C_noret;
C_noret_decl(f_11040)
static void C_ccall f_11040(C_word c,C_word *av) C_noret;
C_noret_decl(f_11044)
static void C_ccall f_11044(C_word c,C_word *av) C_noret;
C_noret_decl(f_11047)
static void C_ccall f_11047(C_word c,C_word *av) C_noret;
C_noret_decl(f_11051)
static void C_ccall f_11051(C_word c,C_word *av) C_noret;
C_noret_decl(f_11053)
static void C_ccall f_11053(C_word c,C_word *av) C_noret;
C_noret_decl(f_11059)
static void C_ccall f_11059(C_word c,C_word *av) C_noret;
C_noret_decl(f_11065)
static void C_ccall f_11065(C_word c,C_word *av) C_noret;
C_noret_decl(f_11071)
static void C_ccall f_11071(C_word c,C_word *av) C_noret;
C_noret_decl(f_11077)
static void C_fcall f_11077(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11081)
static void C_ccall f_11081(C_word c,C_word *av) C_noret;
C_noret_decl(f_11086)
static void C_ccall f_11086(C_word c,C_word *av) C_noret;
C_noret_decl(f_11092)
static void C_ccall f_11092(C_word c,C_word *av) C_noret;
C_noret_decl(f_11096)
static void C_ccall f_11096(C_word c,C_word *av) C_noret;
C_noret_decl(f_11098)
static void C_ccall f_11098(C_word c,C_word *av) C_noret;
C_noret_decl(f_11104)
static void C_ccall f_11104(C_word c,C_word *av) C_noret;
C_noret_decl(f_11112)
static void C_ccall f_11112(C_word c,C_word *av) C_noret;
C_noret_decl(f_11114)
static void C_ccall f_11114(C_word c,C_word *av) C_noret;
C_noret_decl(f_11118)
static void C_ccall f_11118(C_word c,C_word *av) C_noret;
C_noret_decl(f_11123)
static void C_ccall f_11123(C_word c,C_word *av) C_noret;
C_noret_decl(f_11127)
static void C_ccall f_11127(C_word c,C_word *av) C_noret;
C_noret_decl(f_11134)
static void C_ccall f_11134(C_word c,C_word *av) C_noret;
C_noret_decl(f_11138)
static void C_ccall f_11138(C_word c,C_word *av) C_noret;
C_noret_decl(f_11140)
static C_word C_fcall f_11140(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_11153)
static void C_ccall f_11153(C_word c,C_word *av) C_noret;
C_noret_decl(f_11159)
static void C_ccall f_11159(C_word c,C_word *av) C_noret;
C_noret_decl(f_11163)
static void C_ccall f_11163(C_word c,C_word *av) C_noret;
C_noret_decl(f_11166)
static void C_ccall f_11166(C_word c,C_word *av) C_noret;
C_noret_decl(f_11173)
static void C_ccall f_11173(C_word c,C_word *av) C_noret;
C_noret_decl(f_11177)
static void C_ccall f_11177(C_word c,C_word *av) C_noret;
C_noret_decl(f_11179)
static void C_ccall f_11179(C_word c,C_word *av) C_noret;
C_noret_decl(f_11183)
static void C_ccall f_11183(C_word c,C_word *av) C_noret;
C_noret_decl(f_11188)
static void C_ccall f_11188(C_word c,C_word *av) C_noret;
C_noret_decl(f_11192)
static void C_ccall f_11192(C_word c,C_word *av) C_noret;
C_noret_decl(f_11195)
static void C_ccall f_11195(C_word c,C_word *av) C_noret;
C_noret_decl(f_11202)
static void C_ccall f_11202(C_word c,C_word *av) C_noret;
C_noret_decl(f_11206)
static void C_ccall f_11206(C_word c,C_word *av) C_noret;
C_noret_decl(f_11210)
static void C_ccall f_11210(C_word c,C_word *av) C_noret;
C_noret_decl(f_11214)
static void C_ccall f_11214(C_word c,C_word *av) C_noret;
C_noret_decl(f_11216)
static void C_ccall f_11216(C_word c,C_word *av) C_noret;
C_noret_decl(f_11222)
static void C_ccall f_11222(C_word c,C_word *av) C_noret;
C_noret_decl(f_11230)
static void C_ccall f_11230(C_word c,C_word *av) C_noret;
C_noret_decl(f_11232)
static void C_ccall f_11232(C_word c,C_word *av) C_noret;
C_noret_decl(f_11238)
static void C_ccall f_11238(C_word c,C_word *av) C_noret;
C_noret_decl(f_11242)
static void C_ccall f_11242(C_word c,C_word *av) C_noret;
C_noret_decl(f_11245)
static void C_ccall f_11245(C_word c,C_word *av) C_noret;
C_noret_decl(f_11252)
static void C_ccall f_11252(C_word c,C_word *av) C_noret;
C_noret_decl(f_11256)
static void C_ccall f_11256(C_word c,C_word *av) C_noret;
C_noret_decl(f_11258)
static void C_ccall f_11258(C_word c,C_word *av) C_noret;
C_noret_decl(f_11262)
static void C_ccall f_11262(C_word c,C_word *av) C_noret;
C_noret_decl(f_11267)
static void C_ccall f_11267(C_word c,C_word *av) C_noret;
C_noret_decl(f_11271)
static void C_ccall f_11271(C_word c,C_word *av) C_noret;
C_noret_decl(f_11278)
static void C_ccall f_11278(C_word c,C_word *av) C_noret;
C_noret_decl(f_11280)
static void C_ccall f_11280(C_word c,C_word *av) C_noret;
C_noret_decl(f_11284)
static void C_ccall f_11284(C_word c,C_word *av) C_noret;
C_noret_decl(f_11289)
static void C_ccall f_11289(C_word c,C_word *av) C_noret;
C_noret_decl(f_11293)
static void C_ccall f_11293(C_word c,C_word *av) C_noret;
C_noret_decl(f_11295)
static void C_ccall f_11295(C_word c,C_word *av) C_noret;
C_noret_decl(f_11307)
static void C_ccall f_11307(C_word c,C_word *av) C_noret;
C_noret_decl(f_11314)
static void C_ccall f_11314(C_word c,C_word *av) C_noret;
C_noret_decl(f_11316)
static void C_ccall f_11316(C_word c,C_word *av) C_noret;
C_noret_decl(f_11322)
static void C_fcall f_11322(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_11347)
static void C_ccall f_11347(C_word c,C_word *av) C_noret;
C_noret_decl(f_11361)
static void C_ccall f_11361(C_word c,C_word *av) C_noret;
C_noret_decl(f_11373)
static void C_ccall f_11373(C_word c,C_word *av) C_noret;
C_noret_decl(f_11377)
static void C_ccall f_11377(C_word c,C_word *av) C_noret;
C_noret_decl(f_11404)
static void C_ccall f_11404(C_word c,C_word *av) C_noret;
C_noret_decl(f_11408)
static void C_ccall f_11408(C_word c,C_word *av) C_noret;
C_noret_decl(f_11412)
static void C_ccall f_11412(C_word c,C_word *av) C_noret;
C_noret_decl(f_11416)
static void C_ccall f_11416(C_word c,C_word *av) C_noret;
C_noret_decl(f_11420)
static void C_ccall f_11420(C_word c,C_word *av) C_noret;
C_noret_decl(f_11428)
static void C_ccall f_11428(C_word c,C_word *av) C_noret;
C_noret_decl(f_11436)
static void C_ccall f_11436(C_word c,C_word *av) C_noret;
C_noret_decl(f_11444)
static void C_ccall f_11444(C_word c,C_word *av) C_noret;
C_noret_decl(f_11450)
static void C_ccall f_11450(C_word c,C_word *av) C_noret;
C_noret_decl(f_11454)
static void C_ccall f_11454(C_word c,C_word *av) C_noret;
C_noret_decl(f_11457)
static void C_ccall f_11457(C_word c,C_word *av) C_noret;
C_noret_decl(f_11466)
static void C_ccall f_11466(C_word c,C_word *av) C_noret;
C_noret_decl(f_11472)
static void C_ccall f_11472(C_word c,C_word *av) C_noret;
C_noret_decl(f_11476)
static void C_fcall f_11476(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11479)
static void C_ccall f_11479(C_word c,C_word *av) C_noret;
C_noret_decl(f_11491)
static void C_ccall f_11491(C_word c,C_word *av) C_noret;
C_noret_decl(f_11493)
static void C_ccall f_11493(C_word c,C_word *av) C_noret;
C_noret_decl(f_11499)
static void C_ccall f_11499(C_word c,C_word *av) C_noret;
C_noret_decl(f_11504)
static void C_ccall f_11504(C_word c,C_word *av) C_noret;
C_noret_decl(f_11508)
static void C_ccall f_11508(C_word c,C_word *av) C_noret;
C_noret_decl(f_11511)
static void C_fcall f_11511(C_word t0,C_word t1) C_noret;
C_noret_decl(f_11521)
static void C_ccall f_11521(C_word c,C_word *av) C_noret;
C_noret_decl(f_11534)
static void C_ccall f_11534(C_word c,C_word *av) C_noret;
C_noret_decl(f_11539)
static void C_ccall f_11539(C_word c,C_word *av) C_noret;
C_noret_decl(f_11546)
static void C_ccall f_11546(C_word c,C_word *av) C_noret;
C_noret_decl(f_11549)
static void C_ccall f_11549(C_word c,C_word *av) C_noret;
C_noret_decl(f_11561)
static void C_ccall f_11561(C_word c,C_word *av) C_noret;
C_noret_decl(f_11569)
static void C_ccall f_11569(C_word c,C_word *av) C_noret;
C_noret_decl(f_11573)
static void C_ccall f_11573(C_word c,C_word *av) C_noret;
C_noret_decl(f_11576)
static void C_ccall f_11576(C_word c,C_word *av) C_noret;
C_noret_decl(f_11580)
static void C_ccall f_11580(C_word c,C_word *av) C_noret;
C_noret_decl(f_11584)
static void C_ccall f_11584(C_word c,C_word *av) C_noret;
C_noret_decl(f_11587)
static void C_ccall f_11587(C_word c,C_word *av) C_noret;
C_noret_decl(f_11590)
static void C_ccall f_11590(C_word c,C_word *av) C_noret;
C_noret_decl(f_11599)
static void C_ccall f_11599(C_word c,C_word *av) C_noret;
C_noret_decl(f_11609)
static void C_ccall f_11609(C_word c,C_word *av) C_noret;
C_noret_decl(f_11613)
static void C_ccall f_11613(C_word c,C_word *av) C_noret;
C_noret_decl(f_11616)
static void C_ccall f_11616(C_word c,C_word *av) C_noret;
C_noret_decl(f_11619)
static void C_ccall f_11619(C_word c,C_word *av) C_noret;
C_noret_decl(f_11622)
static void C_ccall f_11622(C_word c,C_word *av) C_noret;
C_noret_decl(f_11630)
static void C_ccall f_11630(C_word c,C_word *av) C_noret;
C_noret_decl(f_11639)
static void C_ccall f_11639(C_word c,C_word *av) C_noret;
C_noret_decl(f_3517)
static void C_ccall f_3517(C_word c,C_word *av) C_noret;
C_noret_decl(f_3520)
static void C_ccall f_3520(C_word c,C_word *av) C_noret;
C_noret_decl(f_3523)
static void C_ccall f_3523(C_word c,C_word *av) C_noret;
C_noret_decl(f_3526)
static void C_ccall f_3526(C_word c,C_word *av) C_noret;
C_noret_decl(f_3534)
static void C_ccall f_3534(C_word c,C_word *av) C_noret;
C_noret_decl(f_3547)
static void C_ccall f_3547(C_word c,C_word *av) C_noret;
C_noret_decl(f_3555)
static void C_ccall f_3555(C_word c,C_word *av) C_noret;
C_noret_decl(f_3559)
static void C_ccall f_3559(C_word c,C_word *av) C_noret;
C_noret_decl(f_3562)
static void C_ccall f_3562(C_word c,C_word *av) C_noret;
C_noret_decl(f_3565)
static void C_ccall f_3565(C_word c,C_word *av) C_noret;
C_noret_decl(f_3570)
static void C_ccall f_3570(C_word c,C_word *av) C_noret;
C_noret_decl(f_3572)
static void C_fcall f_3572(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3599)
static void C_fcall f_3599(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3612)
static void C_fcall f_3612(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3634)
static void C_fcall f_3634(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3638)
static void C_ccall f_3638(C_word c,C_word *av) C_noret;
C_noret_decl(f_3646)
static void C_ccall f_3646(C_word c,C_word *av) C_noret;
C_noret_decl(f_3652)
static void C_ccall f_3652(C_word c,C_word *av) C_noret;
C_noret_decl(f_3659)
static void C_ccall f_3659(C_word c,C_word *av) C_noret;
C_noret_decl(f_3666)
static void C_ccall f_3666(C_word c,C_word *av) C_noret;
C_noret_decl(f_3668)
static void C_fcall f_3668(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3672)
static void C_ccall f_3672(C_word c,C_word *av) C_noret;
C_noret_decl(f_3680)
static void C_fcall f_3680(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3697)
static void C_fcall f_3697(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3727)
static C_word C_fcall f_3727(C_word t0,C_word t1,C_word t2);
C_noret_decl(f_3757)
static C_word C_fcall f_3757(C_word *a,C_word t0,C_word t1,C_word t2,C_word t3,C_word t4);
C_noret_decl(f_3781)
static void C_fcall f_3781(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3787)
static void C_fcall f_3787(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7) C_noret;
C_noret_decl(f_3794)
static void C_ccall f_3794(C_word c,C_word *av) C_noret;
C_noret_decl(f_3795)
static void C_ccall f_3795(C_word c,C_word *av) C_noret;
C_noret_decl(f_3807)
static void C_ccall f_3807(C_word c,C_word *av) C_noret;
C_noret_decl(f_3813)
static void C_ccall f_3813(C_word c,C_word *av) C_noret;
C_noret_decl(f_3823)
static void C_ccall f_3823(C_word c,C_word *av) C_noret;
C_noret_decl(f_3826)
static void C_fcall f_3826(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3833)
static void C_ccall f_3833(C_word c,C_word *av) C_noret;
C_noret_decl(f_3843)
static void C_ccall f_3843(C_word c,C_word *av) C_noret;
C_noret_decl(f_3844)
static void C_ccall f_3844(C_word c,C_word *av) C_noret;
C_noret_decl(f_3849)
static void C_ccall f_3849(C_word c,C_word *av) C_noret;
C_noret_decl(f_3853)
static void C_fcall f_3853(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3874)
static void C_ccall f_3874(C_word c,C_word *av) C_noret;
C_noret_decl(f_3896)
static void C_ccall f_3896(C_word c,C_word *av) C_noret;
C_noret_decl(f_3907)
static void C_ccall f_3907(C_word c,C_word *av) C_noret;
C_noret_decl(f_3922)
static void C_ccall f_3922(C_word c,C_word *av) C_noret;
C_noret_decl(f_3941)
static void C_ccall f_3941(C_word c,C_word *av) C_noret;
C_noret_decl(f_3964)
static void C_ccall f_3964(C_word c,C_word *av) C_noret;
C_noret_decl(f_3985)
static void C_ccall f_3985(C_word c,C_word *av) C_noret;
C_noret_decl(f_4004)
static void C_ccall f_4004(C_word c,C_word *av) C_noret;
C_noret_decl(f_4011)
static void C_ccall f_4011(C_word c,C_word *av) C_noret;
C_noret_decl(f_4019)
static void C_ccall f_4019(C_word c,C_word *av) C_noret;
C_noret_decl(f_4027)
static void C_ccall f_4027(C_word c,C_word *av) C_noret;
C_noret_decl(f_4035)
static void C_ccall f_4035(C_word c,C_word *av) C_noret;
C_noret_decl(f_4037)
static void C_ccall f_4037(C_word c,C_word *av) C_noret;
C_noret_decl(f_4056)
static void C_ccall f_4056(C_word c,C_word *av) C_noret;
C_noret_decl(f_4058)
static void C_ccall f_4058(C_word c,C_word *av) C_noret;
C_noret_decl(f_4068)
static void C_ccall f_4068(C_word c,C_word *av) C_noret;
C_noret_decl(f_4069)
static void C_ccall f_4069(C_word c,C_word *av) C_noret;
C_noret_decl(f_4088)
static void C_fcall f_4088(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4091)
static void C_ccall f_4091(C_word c,C_word *av) C_noret;
C_noret_decl(f_4106)
static void C_ccall f_4106(C_word c,C_word *av) C_noret;
C_noret_decl(f_4115)
static void C_ccall f_4115(C_word c,C_word *av) C_noret;
C_noret_decl(f_4122)
static void C_ccall f_4122(C_word c,C_word *av) C_noret;
C_noret_decl(f_4130)
static void C_ccall f_4130(C_word c,C_word *av) C_noret;
C_noret_decl(f_4138)
static void C_ccall f_4138(C_word c,C_word *av) C_noret;
C_noret_decl(f_4146)
static void C_ccall f_4146(C_word c,C_word *av) C_noret;
C_noret_decl(f_4154)
static void C_ccall f_4154(C_word c,C_word *av) C_noret;
C_noret_decl(f_4162)
static void C_ccall f_4162(C_word c,C_word *av) C_noret;
C_noret_decl(f_4170)
static void C_ccall f_4170(C_word c,C_word *av) C_noret;
C_noret_decl(f_4172)
static void C_ccall f_4172(C_word c,C_word *av) C_noret;
C_noret_decl(f_4201)
static void C_ccall f_4201(C_word c,C_word *av) C_noret;
C_noret_decl(f_4235)
static void C_ccall f_4235(C_word c,C_word *av) C_noret;
C_noret_decl(f_4245)
static void C_ccall f_4245(C_word c,C_word *av) C_noret;
C_noret_decl(f_4248)
static void C_ccall f_4248(C_word c,C_word *av) C_noret;
C_noret_decl(f_4251)
static void C_ccall f_4251(C_word c,C_word *av) C_noret;
C_noret_decl(f_4252)
static void C_ccall f_4252(C_word c,C_word *av) C_noret;
C_noret_decl(f_4259)
static void C_ccall f_4259(C_word c,C_word *av) C_noret;
C_noret_decl(f_4333)
static void C_ccall f_4333(C_word c,C_word *av) C_noret;
C_noret_decl(f_4336)
static void C_ccall f_4336(C_word c,C_word *av) C_noret;
C_noret_decl(f_4337)
static void C_ccall f_4337(C_word c,C_word *av) C_noret;
C_noret_decl(f_4341)
static void C_ccall f_4341(C_word c,C_word *av) C_noret;
C_noret_decl(f_4355)
static void C_ccall f_4355(C_word c,C_word *av) C_noret;
C_noret_decl(f_4358)
static void C_ccall f_4358(C_word c,C_word *av) C_noret;
C_noret_decl(f_4361)
static void C_ccall f_4361(C_word c,C_word *av) C_noret;
C_noret_decl(f_4362)
static void C_ccall f_4362(C_word c,C_word *av) C_noret;
C_noret_decl(f_4366)
static void C_ccall f_4366(C_word c,C_word *av) C_noret;
C_noret_decl(f_4369)
static void C_ccall f_4369(C_word c,C_word *av) C_noret;
C_noret_decl(f_4407)
static void C_ccall f_4407(C_word c,C_word *av) C_noret;
C_noret_decl(f_4431)
static void C_ccall f_4431(C_word c,C_word *av) C_noret;
C_noret_decl(f_4437)
static void C_ccall f_4437(C_word c,C_word *av) C_noret;
C_noret_decl(f_4441)
static void C_ccall f_4441(C_word c,C_word *av) C_noret;
C_noret_decl(f_4450)
static void C_ccall f_4450(C_word c,C_word *av) C_noret;
C_noret_decl(f_4454)
static void C_ccall f_4454(C_word c,C_word *av) C_noret;
C_noret_decl(f_4461)
static void C_ccall f_4461(C_word c,C_word *av) C_noret;
C_noret_decl(f_4462)
static void C_ccall f_4462(C_word c,C_word *av) C_noret;
C_noret_decl(f_4466)
static void C_ccall f_4466(C_word c,C_word *av) C_noret;
C_noret_decl(f_4492)
static void C_ccall f_4492(C_word c,C_word *av) C_noret;
C_noret_decl(f_4497)
static void C_ccall f_4497(C_word c,C_word *av) C_noret;
C_noret_decl(f_4509)
static void C_ccall f_4509(C_word c,C_word *av) C_noret;
C_noret_decl(f_4510)
static void C_ccall f_4510(C_word c,C_word *av) C_noret;
C_noret_decl(f_4519)
static void C_ccall f_4519(C_word c,C_word *av) C_noret;
C_noret_decl(f_4546)
static void C_ccall f_4546(C_word c,C_word *av) C_noret;
C_noret_decl(f_4555)
static void C_ccall f_4555(C_word c,C_word *av) C_noret;
C_noret_decl(f_4561)
static void C_ccall f_4561(C_word c,C_word *av) C_noret;
C_noret_decl(f_4564)
static void C_ccall f_4564(C_word c,C_word *av) C_noret;
C_noret_decl(f_4573)
static void C_ccall f_4573(C_word c,C_word *av) C_noret;
C_noret_decl(f_4574)
static void C_ccall f_4574(C_word c,C_word *av) C_noret;
C_noret_decl(f_4590)
static void C_ccall f_4590(C_word c,C_word *av) C_noret;
C_noret_decl(f_4594)
static void C_ccall f_4594(C_word c,C_word *av) C_noret;
C_noret_decl(f_4607)
static void C_ccall f_4607(C_word c,C_word *av) C_noret;
C_noret_decl(f_4610)
static void C_ccall f_4610(C_word c,C_word *av) C_noret;
C_noret_decl(f_4611)
static void C_ccall f_4611(C_word c,C_word *av) C_noret;
C_noret_decl(f_4627)
static void C_ccall f_4627(C_word c,C_word *av) C_noret;
C_noret_decl(f_4631)
static void C_ccall f_4631(C_word c,C_word *av) C_noret;
C_noret_decl(f_4635)
static void C_ccall f_4635(C_word c,C_word *av) C_noret;
C_noret_decl(f_4643)
static void C_ccall f_4643(C_word c,C_word *av) C_noret;
C_noret_decl(f_4656)
static void C_ccall f_4656(C_word c,C_word *av) C_noret;
C_noret_decl(f_4659)
static void C_ccall f_4659(C_word c,C_word *av) C_noret;
C_noret_decl(f_4665)
static void C_ccall f_4665(C_word c,C_word *av) C_noret;
C_noret_decl(f_4666)
static void C_ccall f_4666(C_word c,C_word *av) C_noret;
C_noret_decl(f_4682)
static void C_ccall f_4682(C_word c,C_word *av) C_noret;
C_noret_decl(f_4686)
static void C_ccall f_4686(C_word c,C_word *av) C_noret;
C_noret_decl(f_4690)
static void C_ccall f_4690(C_word c,C_word *av) C_noret;
C_noret_decl(f_4694)
static void C_ccall f_4694(C_word c,C_word *av) C_noret;
C_noret_decl(f_4702)
static void C_ccall f_4702(C_word c,C_word *av) C_noret;
C_noret_decl(f_4710)
static void C_ccall f_4710(C_word c,C_word *av) C_noret;
C_noret_decl(f_4723)
static void C_ccall f_4723(C_word c,C_word *av) C_noret;
C_noret_decl(f_4726)
static void C_ccall f_4726(C_word c,C_word *av) C_noret;
C_noret_decl(f_4732)
static void C_ccall f_4732(C_word c,C_word *av) C_noret;
C_noret_decl(f_4735)
static void C_ccall f_4735(C_word c,C_word *av) C_noret;
C_noret_decl(f_4736)
static void C_ccall f_4736(C_word c,C_word *av) C_noret;
C_noret_decl(f_4752)
static void C_ccall f_4752(C_word c,C_word *av) C_noret;
C_noret_decl(f_4756)
static void C_ccall f_4756(C_word c,C_word *av) C_noret;
C_noret_decl(f_4760)
static void C_ccall f_4760(C_word c,C_word *av) C_noret;
C_noret_decl(f_4764)
static void C_ccall f_4764(C_word c,C_word *av) C_noret;
C_noret_decl(f_4768)
static void C_ccall f_4768(C_word c,C_word *av) C_noret;
C_noret_decl(f_4776)
static void C_ccall f_4776(C_word c,C_word *av) C_noret;
C_noret_decl(f_4784)
static void C_ccall f_4784(C_word c,C_word *av) C_noret;
C_noret_decl(f_4792)
static void C_ccall f_4792(C_word c,C_word *av) C_noret;
C_noret_decl(f_4800)
static void C_fcall f_4800(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4813)
static void C_ccall f_4813(C_word c,C_word *av) C_noret;
C_noret_decl(f_4814)
static void C_ccall f_4814(C_word c,C_word *av) C_noret;
C_noret_decl(f_4818)
static void C_ccall f_4818(C_word c,C_word *av) C_noret;
C_noret_decl(f_4821)
static void C_ccall f_4821(C_word c,C_word *av) C_noret;
C_noret_decl(f_4830)
static void C_fcall f_4830(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_4855)
static void C_ccall f_4855(C_word c,C_word *av) C_noret;
C_noret_decl(f_4860)
static void C_fcall f_4860(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4885)
static void C_ccall f_4885(C_word c,C_word *av) C_noret;
C_noret_decl(f_4902)
static void C_ccall f_4902(C_word c,C_word *av) C_noret;
C_noret_decl(f_4906)
static void C_ccall f_4906(C_word c,C_word *av) C_noret;
C_noret_decl(f_4910)
static void C_ccall f_4910(C_word c,C_word *av) C_noret;
C_noret_decl(f_4913)
static void C_ccall f_4913(C_word c,C_word *av) C_noret;
C_noret_decl(f_4919)
static void C_ccall f_4919(C_word c,C_word *av) C_noret;
C_noret_decl(f_4927)
static void C_ccall f_4927(C_word c,C_word *av) C_noret;
C_noret_decl(f_4935)
static void C_ccall f_4935(C_word c,C_word *av) C_noret;
C_noret_decl(f_4937)
static void C_ccall f_4937(C_word c,C_word *av) C_noret;
C_noret_decl(f_4941)
static void C_ccall f_4941(C_word c,C_word *av) C_noret;
C_noret_decl(f_4944)
static void C_ccall f_4944(C_word c,C_word *av) C_noret;
C_noret_decl(f_4949)
static void C_ccall f_4949(C_word c,C_word *av) C_noret;
C_noret_decl(f_4951)
static void C_fcall f_4951(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4976)
static void C_ccall f_4976(C_word c,C_word *av) C_noret;
C_noret_decl(f_4985)
static void C_fcall f_4985(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5058)
static void C_ccall f_5058(C_word c,C_word *av) C_noret;
C_noret_decl(f_5062)
static void C_ccall f_5062(C_word c,C_word *av) C_noret;
C_noret_decl(f_5082)
static void C_ccall f_5082(C_word c,C_word *av) C_noret;
C_noret_decl(f_5096)
static void C_fcall f_5096(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5130)
static void C_fcall f_5130(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5180)
static void C_ccall f_5180(C_word c,C_word *av) C_noret;
C_noret_decl(f_5189)
static void C_ccall f_5189(C_word c,C_word *av) C_noret;
C_noret_decl(f_5215)
static void C_ccall f_5215(C_word c,C_word *av) C_noret;
C_noret_decl(f_5242)
static void C_ccall f_5242(C_word c,C_word *av) C_noret;
C_noret_decl(f_5246)
static void C_ccall f_5246(C_word c,C_word *av) C_noret;
C_noret_decl(f_5258)
static void C_ccall f_5258(C_word c,C_word *av) C_noret;
C_noret_decl(f_5272)
static void C_fcall f_5272(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5320)
static void C_fcall f_5320(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5368)
static void C_fcall f_5368(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5402)
static void C_fcall f_5402(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5427)
static void C_ccall f_5427(C_word c,C_word *av) C_noret;
C_noret_decl(f_5436)
static void C_fcall f_5436(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5477)
static void C_ccall f_5477(C_word c,C_word *av) C_noret;
C_noret_decl(f_5488)
static void C_ccall f_5488(C_word c,C_word *av) C_noret;
C_noret_decl(f_5493)
static void C_ccall f_5493(C_word c,C_word *av) C_noret;
C_noret_decl(f_5503)
static void C_ccall f_5503(C_word c,C_word *av) C_noret;
C_noret_decl(f_5506)
static void C_ccall f_5506(C_word c,C_word *av) C_noret;
C_noret_decl(f_5512)
static void C_ccall f_5512(C_word c,C_word *av) C_noret;
C_noret_decl(f_5522)
static void C_ccall f_5522(C_word c,C_word *av) C_noret;
C_noret_decl(f_5528)
static void C_ccall f_5528(C_word c,C_word *av) C_noret;
C_noret_decl(f_5541)
static void C_ccall f_5541(C_word c,C_word *av) C_noret;
C_noret_decl(f_5547)
static void C_ccall f_5547(C_word c,C_word *av) C_noret;
C_noret_decl(f_5565)
static void C_ccall f_5565(C_word c,C_word *av) C_noret;
C_noret_decl(f_5571)
static void C_ccall f_5571(C_word c,C_word *av) C_noret;
C_noret_decl(f_5584)
static void C_ccall f_5584(C_word c,C_word *av) C_noret;
C_noret_decl(f_5590)
static void C_ccall f_5590(C_word c,C_word *av) C_noret;
C_noret_decl(f_5612)
static void C_ccall f_5612(C_word c,C_word *av) C_noret;
C_noret_decl(f_5618)
static void C_ccall f_5618(C_word c,C_word *av) C_noret;
C_noret_decl(f_5631)
static void C_ccall f_5631(C_word c,C_word *av) C_noret;
C_noret_decl(f_5637)
static void C_ccall f_5637(C_word c,C_word *av) C_noret;
C_noret_decl(f_5659)
static void C_ccall f_5659(C_word c,C_word *av) C_noret;
C_noret_decl(f_5665)
static void C_ccall f_5665(C_word c,C_word *av) C_noret;
C_noret_decl(f_5678)
static void C_ccall f_5678(C_word c,C_word *av) C_noret;
C_noret_decl(f_5684)
static void C_ccall f_5684(C_word c,C_word *av) C_noret;
C_noret_decl(f_5706)
static void C_ccall f_5706(C_word c,C_word *av) C_noret;
C_noret_decl(f_5712)
static void C_ccall f_5712(C_word c,C_word *av) C_noret;
C_noret_decl(f_5725)
static void C_ccall f_5725(C_word c,C_word *av) C_noret;
C_noret_decl(f_5731)
static void C_ccall f_5731(C_word c,C_word *av) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5747)
static void C_ccall f_5747(C_word c,C_word *av) C_noret;
C_noret_decl(f_5753)
static void C_ccall f_5753(C_word c,C_word *av) C_noret;
C_noret_decl(f_5765)
static void C_ccall f_5765(C_word c,C_word *av) C_noret;
C_noret_decl(f_5769)
static void C_ccall f_5769(C_word c,C_word *av) C_noret;
C_noret_decl(f_5770)
static void C_ccall f_5770(C_word c,C_word *av) C_noret;
C_noret_decl(f_5776)
static void C_ccall f_5776(C_word c,C_word *av) C_noret;
C_noret_decl(f_5798)
static void C_ccall f_5798(C_word c,C_word *av) C_noret;
C_noret_decl(f_5814)
static void C_ccall f_5814(C_word c,C_word *av) C_noret;
C_noret_decl(f_5818)
static void C_ccall f_5818(C_word c,C_word *av) C_noret;
C_noret_decl(f_5822)
static void C_ccall f_5822(C_word c,C_word *av) C_noret;
C_noret_decl(f_5825)
static void C_ccall f_5825(C_word c,C_word *av) C_noret;
C_noret_decl(f_5831)
static void C_ccall f_5831(C_word c,C_word *av) C_noret;
C_noret_decl(f_5839)
static void C_ccall f_5839(C_word c,C_word *av) C_noret;
C_noret_decl(f_5845)
static void C_ccall f_5845(C_word c,C_word *av) C_noret;
C_noret_decl(f_5849)
static void C_ccall f_5849(C_word c,C_word *av) C_noret;
C_noret_decl(f_5852)
static void C_ccall f_5852(C_word c,C_word *av) C_noret;
C_noret_decl(f_5857)
static void C_ccall f_5857(C_word c,C_word *av) C_noret;
C_noret_decl(f_5859)
static void C_fcall f_5859(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5884)
static void C_ccall f_5884(C_word c,C_word *av) C_noret;
C_noret_decl(f_5894)
static void C_ccall f_5894(C_word c,C_word *av) C_noret;
C_noret_decl(f_5899)
static void C_ccall f_5899(C_word c,C_word *av) C_noret;
C_noret_decl(f_5907)
static void C_ccall f_5907(C_word c,C_word *av) C_noret;
C_noret_decl(f_5909)
static void C_ccall f_5909(C_word c,C_word *av) C_noret;
C_noret_decl(f_5920)
static void C_ccall f_5920(C_word c,C_word *av) C_noret;
C_noret_decl(f_5929)
static void C_ccall f_5929(C_word c,C_word *av) C_noret;
C_noret_decl(f_5934)
static void C_ccall f_5934(C_word c,C_word *av) C_noret;
C_noret_decl(f_5938)
static void C_ccall f_5938(C_word c,C_word *av) C_noret;
C_noret_decl(f_5942)
static void C_ccall f_5942(C_word c,C_word *av) C_noret;
C_noret_decl(f_5945)
static void C_ccall f_5945(C_word c,C_word *av) C_noret;
C_noret_decl(f_5951)
static void C_ccall f_5951(C_word c,C_word *av) C_noret;
C_noret_decl(f_5959)
static void C_ccall f_5959(C_word c,C_word *av) C_noret;
C_noret_decl(f_5967)
static void C_ccall f_5967(C_word c,C_word *av) C_noret;
C_noret_decl(f_5969)
static void C_ccall f_5969(C_word c,C_word *av) C_noret;
C_noret_decl(f_5973)
static void C_ccall f_5973(C_word c,C_word *av) C_noret;
C_noret_decl(f_5976)
static void C_ccall f_5976(C_word c,C_word *av) C_noret;
C_noret_decl(f_5994)
static void C_ccall f_5994(C_word c,C_word *av) C_noret;
C_noret_decl(f_5998)
static void C_ccall f_5998(C_word c,C_word *av) C_noret;
C_noret_decl(f_6002)
static void C_ccall f_6002(C_word c,C_word *av) C_noret;
C_noret_decl(f_6006)
static void C_ccall f_6006(C_word c,C_word *av) C_noret;
C_noret_decl(f_6021)
static void C_ccall f_6021(C_word c,C_word *av) C_noret;
C_noret_decl(f_6025)
static void C_ccall f_6025(C_word c,C_word *av) C_noret;
C_noret_decl(f_6027)
static void C_fcall f_6027(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6081)
static void C_ccall f_6081(C_word c,C_word *av) C_noret;
C_noret_decl(f_6085)
static void C_ccall f_6085(C_word c,C_word *av) C_noret;
C_noret_decl(f_6089)
static void C_ccall f_6089(C_word c,C_word *av) C_noret;
C_noret_decl(f_6104)
static void C_ccall f_6104(C_word c,C_word *av) C_noret;
C_noret_decl(f_6107)
static void C_ccall f_6107(C_word c,C_word *av) C_noret;
C_noret_decl(f_6108)
static C_word C_fcall f_6108(C_word t0,C_word t1);
C_noret_decl(f_6127)
static void C_ccall f_6127(C_word c,C_word *av) C_noret;
C_noret_decl(f_6131)
static void C_ccall f_6131(C_word c,C_word *av) C_noret;
C_noret_decl(f_6135)
static void C_ccall f_6135(C_word c,C_word *av) C_noret;
C_noret_decl(f_6138)
static void C_ccall f_6138(C_word c,C_word *av) C_noret;
C_noret_decl(f_6144)
static void C_ccall f_6144(C_word c,C_word *av) C_noret;
C_noret_decl(f_6152)
static void C_ccall f_6152(C_word c,C_word *av) C_noret;
C_noret_decl(f_6160)
static void C_ccall f_6160(C_word c,C_word *av) C_noret;
C_noret_decl(f_6162)
static void C_ccall f_6162(C_word c,C_word *av) C_noret;
C_noret_decl(f_6166)
static void C_ccall f_6166(C_word c,C_word *av) C_noret;
C_noret_decl(f_6169)
static void C_ccall f_6169(C_word c,C_word *av) C_noret;
C_noret_decl(f_6172)
static C_word C_fcall f_6172(C_word t0,C_word t1);
C_noret_decl(f_6197)
static void C_ccall f_6197(C_word c,C_word *av) C_noret;
C_noret_decl(f_6199)
static void C_fcall f_6199(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6246)
static void C_ccall f_6246(C_word c,C_word *av) C_noret;
C_noret_decl(f_6249)
static void C_ccall f_6249(C_word c,C_word *av) C_noret;
C_noret_decl(f_6252)
static void C_ccall f_6252(C_word c,C_word *av) C_noret;
C_noret_decl(f_6255)
static void C_ccall f_6255(C_word c,C_word *av) C_noret;
C_noret_decl(f_6262)
static void C_ccall f_6262(C_word c,C_word *av) C_noret;
C_noret_decl(f_6266)
static void C_ccall f_6266(C_word c,C_word *av) C_noret;
C_noret_decl(f_6270)
static void C_ccall f_6270(C_word c,C_word *av) C_noret;
C_noret_decl(f_6287)
static void C_ccall f_6287(C_word c,C_word *av) C_noret;
C_noret_decl(f_6309)
static void C_ccall f_6309(C_word c,C_word *av) C_noret;
C_noret_decl(f_6317)
static void C_ccall f_6317(C_word c,C_word *av) C_noret;
C_noret_decl(f_6336)
static void C_ccall f_6336(C_word c,C_word *av) C_noret;
C_noret_decl(f_6344)
static void C_ccall f_6344(C_word c,C_word *av) C_noret;
C_noret_decl(f_6354)
static void C_ccall f_6354(C_word c,C_word *av) C_noret;
C_noret_decl(f_6358)
static void C_ccall f_6358(C_word c,C_word *av) C_noret;
C_noret_decl(f_6387)
static void C_ccall f_6387(C_word c,C_word *av) C_noret;
C_noret_decl(f_6399)
static void C_ccall f_6399(C_word c,C_word *av) C_noret;
C_noret_decl(f_6401)
static void C_ccall f_6401(C_word c,C_word *av) C_noret;
C_noret_decl(f_6415)
static void C_fcall f_6415(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6440)
static void C_ccall f_6440(C_word c,C_word *av) C_noret;
C_noret_decl(f_6456)
static void C_ccall f_6456(C_word c,C_word *av) C_noret;
C_noret_decl(f_6465)
static void C_ccall f_6465(C_word c,C_word *av) C_noret;
C_noret_decl(f_6468)
static void C_ccall f_6468(C_word c,C_word *av) C_noret;
C_noret_decl(f_6471)
static void C_ccall f_6471(C_word c,C_word *av) C_noret;
C_noret_decl(f_6474)
static void C_ccall f_6474(C_word c,C_word *av) C_noret;
C_noret_decl(f_6479)
static void C_ccall f_6479(C_word c,C_word *av) C_noret;
C_noret_decl(f_6483)
static void C_ccall f_6483(C_word c,C_word *av) C_noret;
C_noret_decl(f_6486)
static void C_ccall f_6486(C_word c,C_word *av) C_noret;
C_noret_decl(f_6489)
static void C_ccall f_6489(C_word c,C_word *av) C_noret;
C_noret_decl(f_6492)
static void C_ccall f_6492(C_word c,C_word *av) C_noret;
C_noret_decl(f_6496)
static void C_ccall f_6496(C_word c,C_word *av) C_noret;
C_noret_decl(f_6500)
static void C_ccall f_6500(C_word c,C_word *av) C_noret;
C_noret_decl(f_6504)
static void C_ccall f_6504(C_word c,C_word *av) C_noret;
C_noret_decl(f_6508)
static void C_ccall f_6508(C_word c,C_word *av) C_noret;
C_noret_decl(f_6511)
static void C_ccall f_6511(C_word c,C_word *av) C_noret;
C_noret_decl(f_6514)
static void C_ccall f_6514(C_word c,C_word *av) C_noret;
C_noret_decl(f_6517)
static void C_ccall f_6517(C_word c,C_word *av) C_noret;
C_noret_decl(f_6520)
static void C_ccall f_6520(C_word c,C_word *av) C_noret;
C_noret_decl(f_6535)
static void C_ccall f_6535(C_word c,C_word *av) C_noret;
C_noret_decl(f_6541)
static void C_ccall f_6541(C_word c,C_word *av) C_noret;
C_noret_decl(f_6551)
static void C_fcall f_6551(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_6561)
static void C_ccall f_6561(C_word c,C_word *av) C_noret;
C_noret_decl(f_6564)
static void C_ccall f_6564(C_word c,C_word *av) C_noret;
C_noret_decl(f_6567)
static void C_ccall f_6567(C_word c,C_word *av) C_noret;
C_noret_decl(f_6568)
static void C_ccall f_6568(C_word c,C_word *av) C_noret;
C_noret_decl(f_6574)
static void C_fcall f_6574(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6597)
static void C_ccall f_6597(C_word c,C_word *av) C_noret;
C_noret_decl(f_6608)
static void C_ccall f_6608(C_word c,C_word *av) C_noret;
C_noret_decl(f_6612)
static void C_ccall f_6612(C_word c,C_word *av) C_noret;
C_noret_decl(f_6627)
static void C_ccall f_6627(C_word c,C_word *av) C_noret;
C_noret_decl(f_6631)
static void C_ccall f_6631(C_word c,C_word *av) C_noret;
C_noret_decl(f_6635)
static void C_ccall f_6635(C_word c,C_word *av) C_noret;
C_noret_decl(f_6638)
static void C_ccall f_6638(C_word c,C_word *av) C_noret;
C_noret_decl(f_6641)
static void C_ccall f_6641(C_word c,C_word *av) C_noret;
C_noret_decl(f_6644)
static void C_ccall f_6644(C_word c,C_word *av) C_noret;
C_noret_decl(f_6647)
static void C_ccall f_6647(C_word c,C_word *av) C_noret;
C_noret_decl(f_6650)
static void C_ccall f_6650(C_word c,C_word *av) C_noret;
C_noret_decl(f_6653)
static void C_ccall f_6653(C_word c,C_word *av) C_noret;
C_noret_decl(f_6656)
static void C_ccall f_6656(C_word c,C_word *av) C_noret;
C_noret_decl(f_6663)
static void C_ccall f_6663(C_word c,C_word *av) C_noret;
C_noret_decl(f_6670)
static void C_fcall f_6670(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6683)
static void C_fcall f_6683(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6694)
static C_word C_fcall f_6694(C_word t0);
C_noret_decl(f_6727)
static void C_fcall f_6727(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6752)
static void C_ccall f_6752(C_word c,C_word *av) C_noret;
C_noret_decl(f_6780)
static void C_ccall f_6780(C_word c,C_word *av) C_noret;
C_noret_decl(f_6814)
static void C_ccall f_6814(C_word c,C_word *av) C_noret;
C_noret_decl(f_6837)
static void C_ccall f_6837(C_word c,C_word *av) C_noret;
C_noret_decl(f_6843)
static void C_ccall f_6843(C_word c,C_word *av) C_noret;
C_noret_decl(f_6859)
static void C_ccall f_6859(C_word c,C_word *av) C_noret;
C_noret_decl(f_6900)
static void C_ccall f_6900(C_word c,C_word *av) C_noret;
C_noret_decl(f_6923)
static void C_ccall f_6923(C_word c,C_word *av) C_noret;
C_noret_decl(f_6936)
static void C_fcall f_6936(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6956)
static void C_ccall f_6956(C_word c,C_word *av) C_noret;
C_noret_decl(f_6982)
static void C_fcall f_6982(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7000)
static void C_ccall f_7000(C_word c,C_word *av) C_noret;
C_noret_decl(f_7012)
static void C_ccall f_7012(C_word c,C_word *av) C_noret;
C_noret_decl(f_7017)
static void C_ccall f_7017(C_word c,C_word *av) C_noret;
C_noret_decl(f_7025)
static void C_ccall f_7025(C_word c,C_word *av) C_noret;
C_noret_decl(f_7027)
static void C_ccall f_7027(C_word c,C_word *av) C_noret;
C_noret_decl(f_7202)
static void C_ccall f_7202(C_word c,C_word *av) C_noret;
C_noret_decl(f_7205)
static void C_fcall f_7205(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7212)
static void C_ccall f_7212(C_word c,C_word *av) C_noret;
C_noret_decl(f_7235)
static void C_ccall f_7235(C_word c,C_word *av) C_noret;
C_noret_decl(f_7264)
static void C_fcall f_7264(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_7293)
static void C_ccall f_7293(C_word c,C_word *av) C_noret;
C_noret_decl(f_7310)
static C_word C_fcall f_7310(C_word t0,C_word t1);
C_noret_decl(f_7336)
static void C_fcall f_7336(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_7343)
static void C_ccall f_7343(C_word c,C_word *av) C_noret;
C_noret_decl(f_7365)
static void C_ccall f_7365(C_word c,C_word *av) C_noret;
C_noret_decl(f_7372)
static void C_ccall f_7372(C_word c,C_word *av) C_noret;
C_noret_decl(f_7384)
static void C_ccall f_7384(C_word c,C_word *av) C_noret;
C_noret_decl(f_7385)
static void C_ccall f_7385(C_word c,C_word *av) C_noret;
C_noret_decl(f_7392)
static void C_ccall f_7392(C_word c,C_word *av) C_noret;
C_noret_decl(f_7399)
static void C_ccall f_7399(C_word c,C_word *av) C_noret;
C_noret_decl(f_7412)
static void C_ccall f_7412(C_word c,C_word *av) C_noret;
C_noret_decl(f_7415)
static void C_ccall f_7415(C_word c,C_word *av) C_noret;
C_noret_decl(f_7416)
static void C_ccall f_7416(C_word c,C_word *av) C_noret;
C_noret_decl(f_7423)
static void C_ccall f_7423(C_word c,C_word *av) C_noret;
C_noret_decl(f_7430)
static void C_ccall f_7430(C_word c,C_word *av) C_noret;
C_noret_decl(f_7434)
static void C_ccall f_7434(C_word c,C_word *av) C_noret;
C_noret_decl(f_7448)
static void C_ccall f_7448(C_word c,C_word *av) C_noret;
C_noret_decl(f_7451)
static void C_ccall f_7451(C_word c,C_word *av) C_noret;
C_noret_decl(f_7454)
static void C_ccall f_7454(C_word c,C_word *av) C_noret;
C_noret_decl(f_7455)
static void C_ccall f_7455(C_word c,C_word *av) C_noret;
C_noret_decl(f_7462)
static void C_ccall f_7462(C_word c,C_word *av) C_noret;
C_noret_decl(f_7469)
static void C_ccall f_7469(C_word c,C_word *av) C_noret;
C_noret_decl(f_7473)
static void C_ccall f_7473(C_word c,C_word *av) C_noret;
C_noret_decl(f_7477)
static void C_ccall f_7477(C_word c,C_word *av) C_noret;
C_noret_decl(f_7492)
static void C_ccall f_7492(C_word c,C_word *av) C_noret;
C_noret_decl(f_7495)
static void C_ccall f_7495(C_word c,C_word *av) C_noret;
C_noret_decl(f_7498)
static void C_ccall f_7498(C_word c,C_word *av) C_noret;
C_noret_decl(f_7501)
static void C_ccall f_7501(C_word c,C_word *av) C_noret;
C_noret_decl(f_7502)
static void C_ccall f_7502(C_word c,C_word *av) C_noret;
C_noret_decl(f_7509)
static void C_ccall f_7509(C_word c,C_word *av) C_noret;
C_noret_decl(f_7516)
static void C_ccall f_7516(C_word c,C_word *av) C_noret;
C_noret_decl(f_7520)
static void C_ccall f_7520(C_word c,C_word *av) C_noret;
C_noret_decl(f_7524)
static void C_ccall f_7524(C_word c,C_word *av) C_noret;
C_noret_decl(f_7528)
static void C_ccall f_7528(C_word c,C_word *av) C_noret;
C_noret_decl(f_7539)
static void C_fcall f_7539(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7549)
static void C_ccall f_7549(C_word c,C_word *av) C_noret;
C_noret_decl(f_7550)
static void C_ccall f_7550(C_word c,C_word *av) C_noret;
C_noret_decl(f_7561)
static void C_ccall f_7561(C_word c,C_word *av) C_noret;
C_noret_decl(f_7566)
static void C_fcall f_7566(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7576)
static void C_ccall f_7576(C_word c,C_word *av) C_noret;
C_noret_decl(f_7578)
static void C_fcall f_7578(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7603)
static void C_ccall f_7603(C_word c,C_word *av) C_noret;
C_noret_decl(f_7612)
static void C_fcall f_7612(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7637)
static void C_ccall f_7637(C_word c,C_word *av) C_noret;
C_noret_decl(f_7660)
static void C_ccall f_7660(C_word c,C_word *av) C_noret;
C_noret_decl(f_7679)
static void C_ccall f_7679(C_word c,C_word *av) C_noret;
C_noret_decl(f_7729)
static void C_ccall f_7729(C_word c,C_word *av) C_noret;
C_noret_decl(f_7733)
static void C_ccall f_7733(C_word c,C_word *av) C_noret;
C_noret_decl(f_7736)
static void C_ccall f_7736(C_word c,C_word *av) C_noret;
C_noret_decl(f_7741)
static void C_ccall f_7741(C_word c,C_word *av) C_noret;
C_noret_decl(f_7745)
static void C_ccall f_7745(C_word c,C_word *av) C_noret;
C_noret_decl(f_7748)
static void C_ccall f_7748(C_word c,C_word *av) C_noret;
C_noret_decl(f_7751)
static void C_ccall f_7751(C_word c,C_word *av) C_noret;
C_noret_decl(f_7755)
static void C_ccall f_7755(C_word c,C_word *av) C_noret;
C_noret_decl(f_7759)
static void C_ccall f_7759(C_word c,C_word *av) C_noret;
C_noret_decl(f_7763)
static void C_ccall f_7763(C_word c,C_word *av) C_noret;
C_noret_decl(f_7766)
static void C_ccall f_7766(C_word c,C_word *av) C_noret;
C_noret_decl(f_7769)
static void C_ccall f_7769(C_word c,C_word *av) C_noret;
C_noret_decl(f_7772)
static void C_ccall f_7772(C_word c,C_word *av) C_noret;
C_noret_decl(f_7784)
static void C_ccall f_7784(C_word c,C_word *av) C_noret;
C_noret_decl(f_7790)
static void C_ccall f_7790(C_word c,C_word *av) C_noret;
C_noret_decl(f_7794)
static void C_ccall f_7794(C_word c,C_word *av) C_noret;
C_noret_decl(f_7799)
static void C_ccall f_7799(C_word c,C_word *av) C_noret;
C_noret_decl(f_7803)
static void C_ccall f_7803(C_word c,C_word *av) C_noret;
C_noret_decl(f_7810)
static void C_ccall f_7810(C_word c,C_word *av) C_noret;
C_noret_decl(f_7814)
static void C_ccall f_7814(C_word c,C_word *av) C_noret;
C_noret_decl(f_7816)
static void C_ccall f_7816(C_word c,C_word *av) C_noret;
C_noret_decl(f_7820)
static void C_ccall f_7820(C_word c,C_word *av) C_noret;
C_noret_decl(f_7823)
static void C_ccall f_7823(C_word c,C_word *av) C_noret;
C_noret_decl(f_7826)
static void C_ccall f_7826(C_word c,C_word *av) C_noret;
C_noret_decl(f_7829)
static void C_ccall f_7829(C_word c,C_word *av) C_noret;
C_noret_decl(f_7832)
static void C_ccall f_7832(C_word c,C_word *av) C_noret;
C_noret_decl(f_7835)
static void C_ccall f_7835(C_word c,C_word *av) C_noret;
C_noret_decl(f_7842)
static void C_ccall f_7842(C_word c,C_word *av) C_noret;
C_noret_decl(f_7844)
static void C_ccall f_7844(C_word c,C_word *av) C_noret;
C_noret_decl(f_7852)
static void C_ccall f_7852(C_word c,C_word *av) C_noret;
C_noret_decl(f_7854)
static void C_ccall f_7854(C_word c,C_word *av) C_noret;
C_noret_decl(f_7861)
static void C_ccall f_7861(C_word c,C_word *av) C_noret;
C_noret_decl(f_7863)
static void C_ccall f_7863(C_word c,C_word *av) C_noret;
C_noret_decl(f_7866)
static void C_fcall f_7866(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7876)
static void C_fcall f_7876(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_7890)
static void C_ccall f_7890(C_word c,C_word *av) C_noret;
C_noret_decl(f_7909)
static void C_ccall f_7909(C_word c,C_word *av) C_noret;
C_noret_decl(f_7948)
static void C_ccall f_7948(C_word c,C_word *av) C_noret;
C_noret_decl(f_7952)
static void C_ccall f_7952(C_word c,C_word *av) C_noret;
C_noret_decl(f_7955)
static void C_ccall f_7955(C_word c,C_word *av) C_noret;
C_noret_decl(f_7958)
static void C_ccall f_7958(C_word c,C_word *av) C_noret;
C_noret_decl(f_7961)
static void C_ccall f_7961(C_word c,C_word *av) C_noret;
C_noret_decl(f_7964)
static void C_ccall f_7964(C_word c,C_word *av) C_noret;
C_noret_decl(f_7966)
static void C_fcall f_7966(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7975)
static void C_fcall f_7975(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8006)
static void C_ccall f_8006(C_word c,C_word *av) C_noret;
C_noret_decl(f_8024)
static void C_ccall f_8024(C_word c,C_word *av) C_noret;
C_noret_decl(f_8028)
static void C_ccall f_8028(C_word c,C_word *av) C_noret;
C_noret_decl(f_8049)
static void C_ccall f_8049(C_word c,C_word *av) C_noret;
C_noret_decl(f_8053)
static void C_ccall f_8053(C_word c,C_word *av) C_noret;
C_noret_decl(f_8449)
static void C_fcall f_8449(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8471)
static void C_ccall f_8471(C_word c,C_word *av) C_noret;
C_noret_decl(f_8477)
static void C_fcall f_8477(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8483)
static void C_fcall f_8483(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8493)
static void C_ccall f_8493(C_word c,C_word *av) C_noret;
C_noret_decl(f_9362)
static void C_fcall f_9362(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9368)
static void C_ccall f_9368(C_word c,C_word *av) C_noret;
C_noret_decl(f_9371)
static void C_fcall f_9371(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9373)
static void C_ccall f_9373(C_word c,C_word *av) C_noret;
C_noret_decl(f_9376)
static void C_ccall f_9376(C_word c,C_word *av) C_noret;
C_noret_decl(f_9383)
static void C_fcall f_9383(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9393)
static C_word C_fcall f_9393(C_word t0,C_word t1);
C_noret_decl(f_9418)
static void C_fcall f_9418(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9422)
static void C_ccall f_9422(C_word c,C_word *av) C_noret;
C_noret_decl(f_9435)
static void C_ccall f_9435(C_word c,C_word *av) C_noret;
C_noret_decl(f_9461)
static void C_ccall f_9461(C_word c,C_word *av) C_noret;
C_noret_decl(f_9465)
static void C_ccall f_9465(C_word c,C_word *av) C_noret;
C_noret_decl(f_9472)
static void C_ccall f_9472(C_word c,C_word *av) C_noret;
C_noret_decl(f_9477)
static void C_fcall f_9477(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9490)
static void C_ccall f_9490(C_word c,C_word *av) C_noret;
C_noret_decl(f_9559)
static void C_ccall f_9559(C_word c,C_word *av) C_noret;
C_noret_decl(f_9565)
static void C_fcall f_9565(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9573)
static void C_ccall f_9573(C_word c,C_word *av) C_noret;
C_noret_decl(f_9577)
static void C_ccall f_9577(C_word c,C_word *av) C_noret;
C_noret_decl(f_9579)
static void C_fcall f_9579(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_9617)
static void C_ccall f_9617(C_word c,C_word *av) C_noret;
C_noret_decl(f_9622)
static void C_fcall f_9622(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9626)
static void C_ccall f_9626(C_word c,C_word *av) C_noret;
C_noret_decl(f_9629)
static void C_ccall f_9629(C_word c,C_word *av) C_noret;
C_noret_decl(f_9645)
static void C_ccall f_9645(C_word c,C_word *av) C_noret;
C_noret_decl(f_9649)
static void C_ccall f_9649(C_word c,C_word *av) C_noret;
C_noret_decl(f_9653)
static void C_ccall f_9653(C_word c,C_word *av) C_noret;
C_noret_decl(f_9657)
static void C_ccall f_9657(C_word c,C_word *av) C_noret;
C_noret_decl(f_9661)
static void C_ccall f_9661(C_word c,C_word *av) C_noret;
C_noret_decl(f_9664)
static void C_ccall f_9664(C_word c,C_word *av) C_noret;
C_noret_decl(f_9667)
static void C_ccall f_9667(C_word c,C_word *av) C_noret;
C_noret_decl(f_9670)
static void C_ccall f_9670(C_word c,C_word *av) C_noret;
C_noret_decl(f_9675)
static void C_fcall f_9675(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9681)
static void C_ccall f_9681(C_word c,C_word *av) C_noret;
C_noret_decl(f_9690)
static void C_ccall f_9690(C_word c,C_word *av) C_noret;
C_noret_decl(f_9694)
static void C_ccall f_9694(C_word c,C_word *av) C_noret;
C_noret_decl(f_9699)
static void C_ccall f_9699(C_word c,C_word *av) C_noret;
C_noret_decl(f_9702)
static void C_ccall f_9702(C_word c,C_word *av) C_noret;
C_noret_decl(f_9706)
static void C_ccall f_9706(C_word c,C_word *av) C_noret;
C_noret_decl(f_9709)
static void C_ccall f_9709(C_word c,C_word *av) C_noret;
C_noret_decl(f_9712)
static void C_ccall f_9712(C_word c,C_word *av) C_noret;
C_noret_decl(f_9717)
static void C_fcall f_9717(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9727)
static void C_ccall f_9727(C_word c,C_word *av) C_noret;
C_noret_decl(f_9730)
static void C_ccall f_9730(C_word c,C_word *av) C_noret;
C_noret_decl(f_9737)
static void C_ccall f_9737(C_word c,C_word *av) C_noret;
C_noret_decl(f_9739)
static void C_ccall f_9739(C_word c,C_word *av) C_noret;
C_noret_decl(f_9746)
static void C_ccall f_9746(C_word c,C_word *av) C_noret;
C_noret_decl(f_9751)
static void C_ccall f_9751(C_word c,C_word *av) C_noret;
C_noret_decl(f_9757)
static void C_ccall f_9757(C_word c,C_word *av) C_noret;
C_noret_decl(f_9761)
static void C_ccall f_9761(C_word c,C_word *av) C_noret;
C_noret_decl(f_9768)
static void C_ccall f_9768(C_word c,C_word *av) C_noret;
C_noret_decl(f_9773)
static void C_ccall f_9773(C_word c,C_word *av) C_noret;
C_noret_decl(f_9782)
static void C_ccall f_9782(C_word c,C_word *av) C_noret;
C_noret_decl(f_9790)
static void C_fcall f_9790(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_9800)
static void C_ccall f_9800(C_word c,C_word *av) C_noret;
C_noret_decl(f_9824)
static void C_ccall f_9824(C_word c,C_word *av) C_noret;
C_noret_decl(f_9828)
static void C_ccall f_9828(C_word c,C_word *av) C_noret;
C_noret_decl(f_9833)
static void C_ccall f_9833(C_word c,C_word *av) C_noret;
C_noret_decl(f_9842)
static void C_ccall f_9842(C_word c,C_word *av) C_noret;
C_noret_decl(f_9861)
static void C_ccall f_9861(C_word c,C_word *av) C_noret;
C_noret_decl(f_9864)
static void C_ccall f_9864(C_word c,C_word *av) C_noret;
C_noret_decl(f_9867)
static void C_ccall f_9867(C_word c,C_word *av) C_noret;
C_noret_decl(f_9873)
static void C_ccall f_9873(C_word c,C_word *av) C_noret;
C_noret_decl(f_9876)
static void C_ccall f_9876(C_word c,C_word *av) C_noret;
C_noret_decl(f_9891)
static void C_ccall f_9891(C_word c,C_word *av) C_noret;
C_noret_decl(f_9894)
static void C_ccall f_9894(C_word c,C_word *av) C_noret;
C_noret_decl(f_9897)
static void C_fcall f_9897(C_word t0,C_word t1) C_noret;
C_noret_decl(f_9903)
static void C_ccall f_9903(C_word c,C_word *av) C_noret;
C_noret_decl(f_9915)
static void C_ccall f_9915(C_word c,C_word *av) C_noret;
C_noret_decl(f_9921)
static void C_ccall f_9921(C_word c,C_word *av) C_noret;
C_noret_decl(f_9981)
static void C_ccall f_9981(C_word c,C_word *av) C_noret;
C_noret_decl(f_9996)
static void C_ccall f_9996(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externexport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;

/* from CHICKEN_get_error_message */
 void  CHICKEN_get_error_message(char *t0,int t1){
C_word x,s=0+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_fix((C_word)t1);
C_save(x);
x=C_mpointer_or_false(&a,(void*)t0);
C_save(x);C_callback_wrapper((void *)f_11295,2);}

/* from CHICKEN_load */
 int  CHICKEN_load(char * t0){
C_word x,s=0+2+(t0==NULL?1:C_bytestowords(C_strlen(t0))),*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_mpointer(&a,(void*)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11280,1));}

/* from CHICKEN_read */
 int  CHICKEN_read(char * t0,C_word *t1){
C_word x,s=0+2+(t0==NULL?1:C_bytestowords(C_strlen(t0)))+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_mpointer_or_false(&a,(void*)t1);
C_save(x);
x=C_mpointer(&a,(void*)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11258,2));}

/* from CHICKEN_apply_to_string */
 int  CHICKEN_apply_to_string(C_word t0,C_word t1,char *t2,int t3){
C_word x,s=0+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_fix((C_word)t3);
C_save(x);
x=C_mpointer_or_false(&a,(void*)t2);
C_save(x);
x=((C_word)t1);
C_save(x);
x=((C_word)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11232,4));}

/* from CHICKEN_apply */
 int  CHICKEN_apply(C_word t0,C_word t1,C_word *t2){
C_word x,s=0+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_mpointer_or_false(&a,(void*)t2);
C_save(x);
x=((C_word)t1);
C_save(x);
x=((C_word)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11216,3));}

/* from CHICKEN_eval_string_to_string */
 int  CHICKEN_eval_string_to_string(char * t0,char *t1,int t2){
C_word x,s=0+2+(t0==NULL?1:C_bytestowords(C_strlen(t0)))+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_fix((C_word)t2);
C_save(x);
x=C_mpointer_or_false(&a,(void*)t1);
C_save(x);
x=C_mpointer(&a,(void*)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11179,3));}

/* from CHICKEN_eval_to_string */
 int  CHICKEN_eval_to_string(C_word t0,char *t1,int t2){
C_word x,s=0+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_fix((C_word)t2);
C_save(x);
x=C_mpointer_or_false(&a,(void*)t1);
C_save(x);
x=((C_word)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11153,3));}

/* from CHICKEN_eval_string */
 int  CHICKEN_eval_string(char * t0,C_word *t1){
C_word x,s=0+2+(t0==NULL?1:C_bytestowords(C_strlen(t0)))+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_mpointer_or_false(&a,(void*)t1);
C_save(x);
x=C_mpointer(&a,(void*)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11114,2));}

/* from CHICKEN_eval */
 int  CHICKEN_eval(C_word t0,C_word *t1){
C_word x,s=0+3,*a=C_alloc(s);
C_callback_adjust_stack(a,s);
x=C_mpointer_or_false(&a,(void*)t1);
C_save(x);
x=((C_word)t0);
C_save(x);
return C_truep(C_callback_wrapper((void *)f_11098,2));}

/* from CHICKEN_yield */
 int  CHICKEN_yield(){
C_word x,s=0,*a=C_stack_pointer;
C_callback_adjust_stack(a,s);
return C_truep(C_callback_wrapper((void *)f_11086,0));}

C_noret_decl(trf_10042)
static void C_ccall trf_10042(C_word c,C_word *av) C_noret;
static void C_ccall trf_10042(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10042(t0,t1,t2);}

C_noret_decl(trf_10051)
static void C_ccall trf_10051(C_word c,C_word *av) C_noret;
static void C_ccall trf_10051(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10051(t0,t1,t2,t3);}

C_noret_decl(trf_10055)
static void C_ccall trf_10055(C_word c,C_word *av) C_noret;
static void C_ccall trf_10055(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10055(t0,t1);}

C_noret_decl(trf_10066)
static void C_ccall trf_10066(C_word c,C_word *av) C_noret;
static void C_ccall trf_10066(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10066(t0,t1,t2);}

C_noret_decl(trf_10244)
static void C_ccall trf_10244(C_word c,C_word *av) C_noret;
static void C_ccall trf_10244(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10244(t0,t1,t2,t3);}

C_noret_decl(trf_10289)
static void C_ccall trf_10289(C_word c,C_word *av) C_noret;
static void C_ccall trf_10289(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_10289(t0,t1);}

C_noret_decl(trf_10354)
static void C_ccall trf_10354(C_word c,C_word *av) C_noret;
static void C_ccall trf_10354(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10354(t0,t1,t2);}

C_noret_decl(trf_10399)
static void C_ccall trf_10399(C_word c,C_word *av) C_noret;
static void C_ccall trf_10399(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10399(t0,t1,t2);}

C_noret_decl(trf_10475)
static void C_ccall trf_10475(C_word c,C_word *av) C_noret;
static void C_ccall trf_10475(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10475(t0,t1,t2);}

C_noret_decl(trf_10515)
static void C_ccall trf_10515(C_word c,C_word *av) C_noret;
static void C_ccall trf_10515(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10515(t0,t1,t2);}

C_noret_decl(trf_10580)
static void C_ccall trf_10580(C_word c,C_word *av) C_noret;
static void C_ccall trf_10580(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10580(t0,t1,t2);}

C_noret_decl(trf_10889)
static void C_ccall trf_10889(C_word c,C_word *av) C_noret;
static void C_ccall trf_10889(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_10889(t0,t1,t2,t3);}

C_noret_decl(trf_10922)
static void C_ccall trf_10922(C_word c,C_word *av) C_noret;
static void C_ccall trf_10922(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10922(t0,t1,t2);}

C_noret_decl(trf_10973)
static void C_ccall trf_10973(C_word c,C_word *av) C_noret;
static void C_ccall trf_10973(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_10973(t0,t1,t2);}

C_noret_decl(trf_11018)
static void C_ccall trf_11018(C_word c,C_word *av) C_noret;
static void C_ccall trf_11018(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11018(t0,t1);}

C_noret_decl(trf_11077)
static void C_ccall trf_11077(C_word c,C_word *av) C_noret;
static void C_ccall trf_11077(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11077(t0,t1,t2);}

C_noret_decl(trf_11322)
static void C_ccall trf_11322(C_word c,C_word *av) C_noret;
static void C_ccall trf_11322(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_11322(t0,t1,t2);}

C_noret_decl(trf_11476)
static void C_ccall trf_11476(C_word c,C_word *av) C_noret;
static void C_ccall trf_11476(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11476(t0,t1);}

C_noret_decl(trf_11511)
static void C_ccall trf_11511(C_word c,C_word *av) C_noret;
static void C_ccall trf_11511(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_11511(t0,t1);}

C_noret_decl(trf_3572)
static void C_ccall trf_3572(C_word c,C_word *av) C_noret;
static void C_ccall trf_3572(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3572(t0,t1,t2,t3);}

C_noret_decl(trf_3599)
static void C_ccall trf_3599(C_word c,C_word *av) C_noret;
static void C_ccall trf_3599(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3599(t0,t1,t2,t3);}

C_noret_decl(trf_3612)
static void C_ccall trf_3612(C_word c,C_word *av) C_noret;
static void C_ccall trf_3612(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3612(t0,t1);}

C_noret_decl(trf_3634)
static void C_ccall trf_3634(C_word c,C_word *av) C_noret;
static void C_ccall trf_3634(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3634(t0,t1,t2);}

C_noret_decl(trf_3668)
static void C_ccall trf_3668(C_word c,C_word *av) C_noret;
static void C_ccall trf_3668(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3668(t0,t1,t2,t3);}

C_noret_decl(trf_3680)
static void C_ccall trf_3680(C_word c,C_word *av) C_noret;
static void C_ccall trf_3680(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3680(t0,t1,t2,t3);}

C_noret_decl(trf_3697)
static void C_ccall trf_3697(C_word c,C_word *av) C_noret;
static void C_ccall trf_3697(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3697(t0,t1,t2);}

C_noret_decl(trf_3781)
static void C_ccall trf_3781(C_word c,C_word *av) C_noret;
static void C_ccall trf_3781(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3781(t0,t1,t2);}

C_noret_decl(trf_3787)
static void C_ccall trf_3787(C_word c,C_word *av) C_noret;
static void C_ccall trf_3787(C_word c,C_word *av){
C_word t0=av[7];
C_word t1=av[6];
C_word t2=av[5];
C_word t3=av[4];
C_word t4=av[3];
C_word t5=av[2];
C_word t6=av[1];
C_word t7=av[0];
f_3787(t0,t1,t2,t3,t4,t5,t6,t7);}

C_noret_decl(trf_3826)
static void C_ccall trf_3826(C_word c,C_word *av) C_noret;
static void C_ccall trf_3826(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3826(t0,t1);}

C_noret_decl(trf_3853)
static void C_ccall trf_3853(C_word c,C_word *av) C_noret;
static void C_ccall trf_3853(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3853(t0,t1);}

C_noret_decl(trf_4088)
static void C_ccall trf_4088(C_word c,C_word *av) C_noret;
static void C_ccall trf_4088(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4088(t0,t1);}

C_noret_decl(trf_4800)
static void C_ccall trf_4800(C_word c,C_word *av) C_noret;
static void C_ccall trf_4800(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4800(t0,t1,t2);}

C_noret_decl(trf_4830)
static void C_ccall trf_4830(C_word c,C_word *av) C_noret;
static void C_ccall trf_4830(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_4830(t0,t1,t2,t3);}

C_noret_decl(trf_4860)
static void C_ccall trf_4860(C_word c,C_word *av) C_noret;
static void C_ccall trf_4860(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4860(t0,t1,t2);}

C_noret_decl(trf_4951)
static void C_ccall trf_4951(C_word c,C_word *av) C_noret;
static void C_ccall trf_4951(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4951(t0,t1,t2);}

C_noret_decl(trf_4985)
static void C_ccall trf_4985(C_word c,C_word *av) C_noret;
static void C_ccall trf_4985(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4985(t0,t1,t2);}

C_noret_decl(trf_5096)
static void C_ccall trf_5096(C_word c,C_word *av) C_noret;
static void C_ccall trf_5096(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5096(t0,t1,t2);}

C_noret_decl(trf_5130)
static void C_ccall trf_5130(C_word c,C_word *av) C_noret;
static void C_ccall trf_5130(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5130(t0,t1,t2);}

C_noret_decl(trf_5272)
static void C_ccall trf_5272(C_word c,C_word *av) C_noret;
static void C_ccall trf_5272(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5272(t0,t1,t2,t3);}

C_noret_decl(trf_5320)
static void C_ccall trf_5320(C_word c,C_word *av) C_noret;
static void C_ccall trf_5320(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5320(t0,t1,t2,t3);}

C_noret_decl(trf_5368)
static void C_ccall trf_5368(C_word c,C_word *av) C_noret;
static void C_ccall trf_5368(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5368(t0,t1,t2);}

C_noret_decl(trf_5402)
static void C_ccall trf_5402(C_word c,C_word *av) C_noret;
static void C_ccall trf_5402(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5402(t0,t1,t2);}

C_noret_decl(trf_5436)
static void C_ccall trf_5436(C_word c,C_word *av) C_noret;
static void C_ccall trf_5436(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5436(t0,t1,t2);}

C_noret_decl(trf_5859)
static void C_ccall trf_5859(C_word c,C_word *av) C_noret;
static void C_ccall trf_5859(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5859(t0,t1,t2);}

C_noret_decl(trf_6027)
static void C_ccall trf_6027(C_word c,C_word *av) C_noret;
static void C_ccall trf_6027(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6027(t0,t1,t2);}

C_noret_decl(trf_6199)
static void C_ccall trf_6199(C_word c,C_word *av) C_noret;
static void C_ccall trf_6199(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6199(t0,t1,t2);}

C_noret_decl(trf_6415)
static void C_ccall trf_6415(C_word c,C_word *av) C_noret;
static void C_ccall trf_6415(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6415(t0,t1,t2);}

C_noret_decl(trf_6551)
static void C_ccall trf_6551(C_word c,C_word *av) C_noret;
static void C_ccall trf_6551(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_6551(t0,t1,t2,t3);}

C_noret_decl(trf_6574)
static void C_ccall trf_6574(C_word c,C_word *av) C_noret;
static void C_ccall trf_6574(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6574(t0,t1,t2);}

C_noret_decl(trf_6670)
static void C_ccall trf_6670(C_word c,C_word *av) C_noret;
static void C_ccall trf_6670(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6670(t0,t1,t2);}

C_noret_decl(trf_6683)
static void C_ccall trf_6683(C_word c,C_word *av) C_noret;
static void C_ccall trf_6683(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6683(t0,t1);}

C_noret_decl(trf_6727)
static void C_ccall trf_6727(C_word c,C_word *av) C_noret;
static void C_ccall trf_6727(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6727(t0,t1,t2);}

C_noret_decl(trf_6936)
static void C_ccall trf_6936(C_word c,C_word *av) C_noret;
static void C_ccall trf_6936(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6936(t0,t1);}

C_noret_decl(trf_6982)
static void C_ccall trf_6982(C_word c,C_word *av) C_noret;
static void C_ccall trf_6982(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6982(t0,t1,t2);}

C_noret_decl(trf_7205)
static void C_ccall trf_7205(C_word c,C_word *av) C_noret;
static void C_ccall trf_7205(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7205(t0,t1);}

C_noret_decl(trf_7264)
static void C_ccall trf_7264(C_word c,C_word *av) C_noret;
static void C_ccall trf_7264(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_7264(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_7336)
static void C_ccall trf_7336(C_word c,C_word *av) C_noret;
static void C_ccall trf_7336(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_7336(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_7539)
static void C_ccall trf_7539(C_word c,C_word *av) C_noret;
static void C_ccall trf_7539(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7539(t0,t1,t2);}

C_noret_decl(trf_7566)
static void C_ccall trf_7566(C_word c,C_word *av) C_noret;
static void C_ccall trf_7566(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7566(t0,t1,t2);}

C_noret_decl(trf_7578)
static void C_ccall trf_7578(C_word c,C_word *av) C_noret;
static void C_ccall trf_7578(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7578(t0,t1,t2);}

C_noret_decl(trf_7612)
static void C_ccall trf_7612(C_word c,C_word *av) C_noret;
static void C_ccall trf_7612(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7612(t0,t1,t2);}

C_noret_decl(trf_7866)
static void C_ccall trf_7866(C_word c,C_word *av) C_noret;
static void C_ccall trf_7866(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7866(t0,t1);}

C_noret_decl(trf_7876)
static void C_ccall trf_7876(C_word c,C_word *av) C_noret;
static void C_ccall trf_7876(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_7876(t0,t1,t2,t3,t4);}

C_noret_decl(trf_7966)
static void C_ccall trf_7966(C_word c,C_word *av) C_noret;
static void C_ccall trf_7966(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7966(t0,t1);}

C_noret_decl(trf_7975)
static void C_ccall trf_7975(C_word c,C_word *av) C_noret;
static void C_ccall trf_7975(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7975(t0,t1,t2);}

C_noret_decl(trf_8449)
static void C_ccall trf_8449(C_word c,C_word *av) C_noret;
static void C_ccall trf_8449(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8449(t0,t1,t2);}

C_noret_decl(trf_8477)
static void C_ccall trf_8477(C_word c,C_word *av) C_noret;
static void C_ccall trf_8477(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8477(t0,t1,t2);}

C_noret_decl(trf_8483)
static void C_ccall trf_8483(C_word c,C_word *av) C_noret;
static void C_ccall trf_8483(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8483(t0,t1,t2);}

C_noret_decl(trf_9362)
static void C_ccall trf_9362(C_word c,C_word *av) C_noret;
static void C_ccall trf_9362(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9362(t0,t1);}

C_noret_decl(trf_9371)
static void C_ccall trf_9371(C_word c,C_word *av) C_noret;
static void C_ccall trf_9371(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9371(t0,t1);}

C_noret_decl(trf_9383)
static void C_ccall trf_9383(C_word c,C_word *av) C_noret;
static void C_ccall trf_9383(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9383(t0,t1);}

C_noret_decl(trf_9418)
static void C_ccall trf_9418(C_word c,C_word *av) C_noret;
static void C_ccall trf_9418(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9418(t0,t1,t2);}

C_noret_decl(trf_9477)
static void C_ccall trf_9477(C_word c,C_word *av) C_noret;
static void C_ccall trf_9477(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9477(t0,t1,t2);}

C_noret_decl(trf_9565)
static void C_ccall trf_9565(C_word c,C_word *av) C_noret;
static void C_ccall trf_9565(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9565(t0,t1,t2);}

C_noret_decl(trf_9579)
static void C_ccall trf_9579(C_word c,C_word *av) C_noret;
static void C_ccall trf_9579(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_9579(t0,t1,t2,t3,t4);}

C_noret_decl(trf_9622)
static void C_ccall trf_9622(C_word c,C_word *av) C_noret;
static void C_ccall trf_9622(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9622(t0,t1,t2);}

C_noret_decl(trf_9675)
static void C_ccall trf_9675(C_word c,C_word *av) C_noret;
static void C_ccall trf_9675(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9675(t0,t1);}

C_noret_decl(trf_9717)
static void C_ccall trf_9717(C_word c,C_word *av) C_noret;
static void C_ccall trf_9717(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9717(t0,t1,t2);}

C_noret_decl(trf_9790)
static void C_ccall trf_9790(C_word c,C_word *av) C_noret;
static void C_ccall trf_9790(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_9790(t0,t1,t2);}

C_noret_decl(trf_9897)
static void C_ccall trf_9897(C_word c,C_word *av) C_noret;
static void C_ccall trf_9897(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_9897(t0,t1);}

/* k10001 in chicken.load#load-relative in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_10003(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10003,c,av);}
/* eval.scm:1103: load/internal */
t2=lf[221];
f_9579(t2,((C_word*)t0)[2],t1,((C_word*)t0)[3],C_SCHEME_END_OF_LIST);}

/* chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_10014(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +8,c,4)))){
C_save_and_reclaim((void*)f_10014,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+8);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10018,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10035,a[2]=((C_word)li191),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1105: ##sys#get-keyword */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[253]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[253]+1);
av2[1]=t4;
av2[2]=lf[256];
av2[3]=t3;
av2[4]=t5;
tp(5,av2);}}

/* k10016 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_10018(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_10018,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10021,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10032,a[2]=((C_word)li190),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1105: ##sys#get-keyword */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[253]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[253]+1);
av2[1]=t2;
av2[2]=lf[255];
av2[3]=((C_word*)t0)[4];
av2[4]=t3;
tp(5,av2);}}

/* k10019 in k10016 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_10021,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10024,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10029,a[2]=((C_word)li189),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1105: ##sys#get-keyword */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[253]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[253]+1);
av2[1]=t2;
av2[2]=lf[254];
av2[3]=((C_word*)t0)[5];
av2[4]=t3;
tp(5,av2);}}

/* k10022 in k10019 in k10016 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_10024,c,av);}
a=C_alloc(9);
/* eval.scm:1106: load/internal */
t2=lf[221];
f_9579(t2,((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],C_a_i_list(&a,3,C_SCHEME_TRUE,((C_word*)t0)[5],t1));}

/* a10028 in k10019 in k10016 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10029,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a10031 in k10016 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10032,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a10034 in chicken.load#load-noisily in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_10035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10035,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_10040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_10040,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10042,a[2]=t1,a[3]=((C_word)li193),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10049,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=lf[191];
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11314,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11322,a[2]=t6,a[3]=t11,a[4]=t2,a[5]=t7,a[6]=((C_word)li256),tmp=(C_word)a,a+=7,tmp));
t13=((C_word*)t11)[1];
f_11322(t13,t9,lf[191]);}

/* complete in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_fcall f_10042(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_10042,3,t0,t1,t2);}
t3=*((C_word*)lf[216]+1);
/* eval.scm:1117: g2384 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}

/* k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_10049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(40,c,5)))){
C_save_and_reclaim((void *)f_10049,c,av);}
a=C_alloc(40);
t2=C_mutate((C_word*)lf[257]+1 /* (set! chicken.load#dynamic-load-libraries ...) */,t1);
t3=C_mutate(&lf[258] /* (set! chicken.load#load-library/internal ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10051,a[2]=((C_word)li195),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[263]+1 /* (set! ##sys#load-library ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10133,a[2]=((C_word)li196),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[264]+1 /* (set! chicken.load#load-library ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10181,a[2]=((C_word)li197),tmp=(C_word)a,a+=3,tmp));
t6=*((C_word*)lf[265]+1);
t7=C_mutate((C_word*)lf[83]+1 /* (set! ##sys#include-forms-from-file ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10208,a[2]=t6,a[3]=((C_word)li203),tmp=(C_word)a,a+=4,tmp));
t8=C_set_block_item(lf[272] /* ##sys#setup-mode */,0,C_SCHEME_FALSE);
t9=C_mutate(&lf[273] /* (set! chicken.load#file-exists? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10289,a[2]=((C_word)li204),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[274]+1 /* (set! chicken.load#find-file ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10298,a[2]=((C_word)li205),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[276]+1 /* (set! chicken.load#find-dynamic-extension ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10345,a[2]=((C_word)li208),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[106]+1 /* (set! chicken.load#load-extension ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10490,a[2]=((C_word)li210),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[284]+1 /* (set! chicken.load#require ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10496,a[2]=((C_word)li213),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[285]+1 /* (set! chicken.load#provide ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10561,a[2]=((C_word)li216),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[287]+1 /* (set! chicken.load#provided? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10626,a[2]=((C_word)li219),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[274]+1 /* (set! chicken.load#find-file ...) */,*((C_word*)lf[274]+1));
t17=C_mutate((C_word*)lf[276]+1 /* (set! chicken.load#find-dynamic-extension ...) */,*((C_word*)lf[276]+1));
t18=C_mutate((C_word*)lf[109]+1 /* (set! ##sys#process-require ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10665,a[2]=((C_word)li221),tmp=(C_word)a,a+=3,tmp));
t19=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11307,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1275: chicken.platform#chicken-home */
t20=*((C_word*)lf[336]+1);{
C_word *av2=av;
av2[0]=t20;
av2[1]=t19;
((C_proc)(void*)(*((C_word*)t20+1)))(2,av2);}}

/* chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_fcall f_10051(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_10051,4,t1,t2,t3,t4);}
a=C_alloc(8);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10055,a[2]=t4,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
t6=t5;
f_10055(t6,C_a_i_list(&a,1,t3));}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10123,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1130: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t6;
av2[2]=C_slot(t2,C_fix(1));
av2[3]=lf[189];
tp(4,av2);}}}

/* k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_10055(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_10055,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10058,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1133: c-toplevel */
f_9565(t2,((C_word*)t0)[3],((C_word*)t0)[2]);}

/* k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10058(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_10058,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10061,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10104,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1134: load-verbose */
t4=*((C_word*)lf[202]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10059 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10061(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_10061,c,av);}
a=C_alloc(9);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10066,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t3,a[5]=((C_word*)t0)[4],a[6]=((C_word)li194),tmp=(C_word)a,a+=7,tmp));
t5=((C_word*)t3)[1];
f_10066(t5,((C_word*)t0)[5],((C_word*)t0)[6]);}

/* loop in k10059 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_fcall f_10066(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_10066,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10080,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* ##sys#peek-c-string */
t4=*((C_word*)lf[234]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_mpointer(&a,(void*)C_dlerror);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10083,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10097,a[2]=t3,a[3]=((C_word*)t0)[5],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1141: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[215]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[215]+1);
av2[1]=t4;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=lf[260];
tp(4,av2);}}}

/* k10078 in loop in k10059 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10080(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_10080,c,av);}
/* eval.scm:1140: ##sys#error */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[259];
av2[4]=((C_word*)t0)[4];
av2[5]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k10081 in loop in k10059 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10083(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10083,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:1143: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_10066(t2,((C_word*)t0)[2],C_slot(((C_word*)t0)[4],C_fix(1)));}}

/* k10095 in loop in k10059 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10097(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10097,c,av);}
/* eval.scm:1141: ##sys#dload */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[222]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[222]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k10102 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10104,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10107,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1135: display */
t3=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[262];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_10061(2,av2);}}}

/* k10105 in k10102 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_10107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_10107,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10110,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1136: display */
t3=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k10108 in k10105 in k10102 in k10056 in k10053 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10110(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10110,c,av);}
/* eval.scm:1137: display */
t2=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[261];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k10121 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10123,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10127,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1131: dynamic-load-libraries */
t3=*((C_word*)lf[257]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10125 in k10121 in chicken.load#load-library/internal in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_10127,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_10055(t2,C_a_i_cons(&a,2,((C_word*)t0)[3],t1));}

/* ##sys#load-library in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10133(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10133,c,av);}
a=C_alloc(6);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10152,a[2]=t1,a[3]=t2,a[4]=t4,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1146: ##sys#provided? */
t10=*((C_word*)lf[192]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}

/* k10150 in ##sys#load-library in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_10152,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10155,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1147: load-library/internal */
f_10051(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5]);}}

/* k10153 in k10150 in ##sys#load-library in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10155(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10155,c,av);}
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.load#load-library in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10181(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10181,c,av);}
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_i_check_symbol_2(t2,lf[260]);
if(C_truep(C_i_not(t4))){
/* eval.scm:1153: ##sys#load-library */
t6=*((C_word*)lf[263]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
av2[4]=lf[260];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t6=C_i_check_string_2(t4,lf[260]);
/* eval.scm:1153: ##sys#load-library */
t7=*((C_word*)lf[263]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
av2[4]=lf[260];
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}}

/* ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10208(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_10208,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10212,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1160: ##sys#resolve-include-filename */
t6=*((C_word*)lf[271]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(6,av2);}}

/* k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,5)))){
C_save_and_reclaim((void *)f_10212,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10215,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_not(t1))){
/* eval.scm:1162: ##sys#signal-hook */
t3=*((C_word*)lf[211]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[244];
av2[3]=lf[269];
av2[4]=lf[270];
av2[5]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_10215(2,av2);}}}

/* k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_10215,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10218,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10277,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1163: load-verbose */
t4=*((C_word*)lf[202]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_10218,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10223,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li202),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1165: with-input-from-file */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_10223(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_10223,c,av);}
a=C_alloc(18);
t2=((C_word*)t0)[2];
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10229,a[2]=t5,a[3]=t3,a[4]=((C_word)li198),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10234,a[2]=((C_word*)t0)[3],a[3]=((C_word)li200),tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10271,a[2]=t3,a[3]=t5,a[4]=((C_word)li201),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1167: ##sys#dynamic-wind */
t9=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=t1;
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a10228 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10229(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10229,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[226]+1));
t3=C_mutate((C_word*)lf[226]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a10233 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_10234,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10242,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1168: read */
t3=*((C_word*)lf[227]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k10240 in a10233 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_10242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_10242,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10244,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word)li199),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_10244(t5,((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* doloop2483 in k10240 in a10233 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in ... */
static void C_fcall f_10244(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_10244,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eofp(t2))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10258,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1171: reverse */
t5=*((C_word*)lf[96]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10265,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1168: read */
t5=*((C_word*)lf[227]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k10256 in doloop2483 in k10240 in a10233 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in ... */
static void C_ccall f_10258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10258,c,av);}
/* eval.scm:1171: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k10263 in doloop2483 in k10240 in a10233 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in ... */
static void C_ccall f_10265(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10265,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=((C_word*)((C_word*)t0)[4])[1];
f_10244(t3,((C_word*)t0)[5],t1,t2);}

/* a10270 in a10222 in k10216 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10271(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10271,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[226]+1));
t3=C_mutate((C_word*)lf[226]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k10275 in k10213 in k10210 in ##sys#include-forms-from-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_10277,c,av);}
if(C_truep(t1)){
/* eval.scm:1164: chicken.base#print */
t2=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[267];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[268];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_10218(2,av2);}}}

/* chicken.load#file-exists? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_fcall f_10289(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,5)))){
C_save_and_reclaim_args((void *)trf_10289,2,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10296,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1179: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[246]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[246]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=C_SCHEME_FALSE;
tp(6,av2);}}

/* k10294 in chicken.load#file-exists? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10296(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10296,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?((C_word*)t0)[3]:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.load#find-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10298(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_10298,c,av);}
a=C_alloc(8);
if(C_truep(C_i_not(t3))){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_stringp(t3))){
t4=C_a_i_list1(&a,1,t3);
/* eval.scm:1184: find-file */
t5=*((C_word*)lf[274]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10327,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10339,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1185: scheme#string-append */
t6=*((C_word*)lf[199]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_car(t3);
av2[3]=lf[275];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}}}}

/* k10325 in chicken.load#find-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10327(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10327,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:1186: find-file */
t2=*((C_word*)lf[274]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=C_u_i_cdr(((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k10337 in chicken.load#find-file in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10339(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10339,c,av);}
/* eval.scm:1185: file-exists? */
f_10289(((C_word*)t0)[2],t1);}

/* chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10345(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_10345,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10349,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1191: chicken.platform#repository-path */
t5=*((C_word*)lf[282]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10349(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_10349,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10352,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_symbolp(((C_word*)t0)[4]))){
/* eval.scm:1192: scheme#symbol->string */
t3=*((C_word*)lf[281]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
f_10352(2,av2);}}}

/* k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_10352,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10354,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li206),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10397,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(C_truep(*((C_word*)lf[272]+1))?lf[278]:C_SCHEME_END_OF_LIST);
t5=(C_truep(((C_word*)t0)[2])?((C_word*)t0)[2]:C_SCHEME_END_OF_LIST);
t6=(C_truep(((C_word*)t0)[4])?*((C_word*)lf[279]+1):C_SCHEME_END_OF_LIST);
if(C_truep(*((C_word*)lf[272]+1))){
/* eval.scm:1200: ##sys#append */
t7=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=t6;
av2[5]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t7+1)))(6,av2);}}
else{
/* eval.scm:1200: ##sys#append */
t7=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t7;
av2[1]=t3;
av2[2]=t4;
av2[3]=t5;
av2[4]=t6;
av2[5]=lf[280];
((C_proc)(void*)(*((C_word*)t7+1)))(6,av2);}}}

/* check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_fcall f_10354(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_10354,3,t0,t1,t2);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10358,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1194: string-append */
t4=*((C_word*)lf[199]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[277];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k10356 in check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_10358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_10358,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10361,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[3])){
if(C_truep(C_i_not(*((C_word*)lf[204]+1)))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10383,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1197: chicken.platform#feature? */
t4=*((C_word*)lf[248]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[249];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_10361(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_10361(2,av2);}}}

/* k10359 in k10356 in check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10361(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10361,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10371,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1199: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[187];
tp(4,av2);}}}

/* k10369 in k10359 in k10356 in check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_10371(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10371,c,av);}
/* eval.scm:1199: file-exists? */
f_10289(((C_word*)t0)[2],t1);}

/* k10381 in k10356 in check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10383(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_10383,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10390,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1198: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[190]+1);
tp(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_10361(2,av2);}}}

/* k10388 in k10381 in k10356 in check in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_10390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10390,c,av);}
/* eval.scm:1198: file-exists? */
f_10289(((C_word*)t0)[2],t1);}

/* k10395 in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10397(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_10397,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10399,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word)li207),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_10399(t5,((C_word*)t0)[3],t1);}

/* loop in k10395 in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_fcall f_10399(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_10399,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10412,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1207: check */
t5=((C_word*)t0)[3];
f_10354(t5,t4,t3);}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k10410 in loop in k10395 in k10350 in k10347 in chicken.load#find-dynamic-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10412,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:1208: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_10399(t2,((C_word*)t0)[2],C_slot(((C_word*)t0)[4],C_fix(1)));}}

/* k10448 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10450,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10456,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1212: any */
f_8477(t2,*((C_word*)lf[192]+1),((C_word*)t0)[6]);}}

/* k10454 in k10448 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_10456,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_memq(((C_word*)t0)[3],lf[183]))){
/* eval.scm:1214: load-library/internal */
f_10051(((C_word*)t0)[4],((C_word*)t0)[3],C_SCHEME_FALSE,((C_word*)t0)[5]);}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10471,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1215: find-dynamic-extension */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[276]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[276]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
tp(4,av2);}}}}

/* k10469 in k10454 in k10448 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_10471,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10475,a[2]=((C_word*)t0)[2],a[3]=((C_word)li209),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1211: g2549 */
t3=t2;
f_10475(t3,((C_word*)t0)[3],t1);}
else{
/* eval.scm:1220: ##sys#error */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[283];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* g2549 in k10469 in k10454 in k10448 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_fcall f_10475(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,4)))){
C_save_and_reclaim_args((void *)trf_10475,3,t0,t1,t2);}
a=C_alloc(16);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10479,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1217: load/internal */
t4=lf[221];
f_9579(t4,t3,t2,C_SCHEME_FALSE,C_a_i_list(&a,4,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,((C_word*)t0)[2]));}

/* k10477 in g2549 in k10469 in k10454 in k10448 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10479,c,av);}
/* eval.scm:1218: ##sys#provide */
t2=*((C_word*)lf[93]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10490(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_10490,c,av);}
a=C_alloc(10);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10494,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_10450,a[2]=t1,a[3]=t2,a[4]=t5,a[5]=t4,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:1211: ##sys#provided? */
t7=*((C_word*)lf[192]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k10492 in chicken.load#load-extension in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10494,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.load#require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +9,c,3)))){
C_save_and_reclaim((void*)f_10496,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+9);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10538,a[2]=((C_word)li211),tmp=(C_word)a,a+=3,tmp);
t4=(
  f_10538(t2)
);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10515,a[2]=t6,a[3]=((C_word)li212),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_10515(t8,t1,t2);}

/* for-each-loop2572 in chicken.load#require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_10515(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_10515,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10525,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[106]+1);
/* eval.scm:1228: g2603 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[106]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[106]+1);
av2[1]=t3;
av2[2]=t4;
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=lf[107];
tp(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10523 in for-each-loop2572 in chicken.load#require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10525(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10525,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_10515(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop2562 in chicken.load#require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static C_word C_fcall f_10538(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=C_slot(t1,C_fix(0));
t3=C_i_check_symbol_2(t2,lf[107]);
t5=C_slot(t1,C_fix(1));
t1=t5;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* chicken.load#provide in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +9,c,3)))){
C_save_and_reclaim((void*)f_10561,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+9);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10603,a[2]=((C_word)li214),tmp=(C_word)a,a+=3,tmp);
t4=(
  f_10603(t2)
);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10580,a[2]=t6,a[3]=((C_word)li215),tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_10580(t8,t1,t2);}

/* for-each-loop2625 in chicken.load#provide in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_10580(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_10580,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10590,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[93]+1);
/* eval.scm:1232: g2656 */
t6=*((C_word*)lf[93]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t3;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k10588 in for-each-loop2625 in chicken.load#provide in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10590,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_10580(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop2615 in chicken.load#provide in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static C_word C_fcall f_10603(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=C_slot(t1,C_fix(0));
t3=C_i_check_symbol_2(t2,lf[286]);
t5=C_slot(t1,C_fix(1));
t1=t5;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* chicken.load#provided? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +10,c,3)))){
C_save_and_reclaim((void*)f_10626,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+10);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10640,a[2]=((C_word)li217),tmp=(C_word)a,a+=3,tmp);
t4=(
  f_10640(t2)
);
t5=*((C_word*)lf[192]+1);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8449,a[2]=t7,a[3]=t5,a[4]=((C_word)li218),tmp=(C_word)a,a+=5,tmp));
t9=((C_word*)t7)[1];
f_8449(t9,t1,t2);}

/* for-each-loop2668 in chicken.load#provided? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static C_word C_fcall f_10640(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=C_slot(t1,C_fix(0));
t3=C_i_check_symbol_2(t2,lf[288]);
t5=C_slot(t1,C_fix(1));
t1=t5;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* ##sys#process-require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_10665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_10665,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_END_OF_LIST:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=C_rest_nullp(c,6);
t13=(C_truep(t12)?C_SCHEME_FALSE:C_get_rest_arg(c,6,av,3,t0));
t14=C_rest_nullp(c,6);
t15=C_rest_nullp(c,7);
t16=(C_truep(t15)?C_SCHEME_FALSE:C_get_rest_arg(c,7,av,3,t0));
t17=C_rest_nullp(c,7);
t18=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10699,a[2]=t1,a[3]=t10,a[4]=t4,a[5]=t13,a[6]=t16,a[7]=t7,tmp=(C_word)a,a+=8,tmp);
/* eval.scm:1249: chicken.internal#library-id */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[312]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[312]+1);
av2[1]=t18;
av2[2]=t2;
tp(3,av2);}}

/* k10697 in ##sys#process-require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_10699,c,av);}
a=C_alloc(12);
t2=C_i_assq(t1,lf[289]);
if(C_truep(t2)){
/* eval.scm:1252: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cdr(t2);
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
if(C_truep((C_truep(C_eqp(t1,lf[290]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[291]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[292]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[293]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[294]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[295]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[296]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[297]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[298]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[299]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[300]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[301]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[302]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[303]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[304]))?C_SCHEME_TRUE:(C_truep(C_eqp(t1,lf[305]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))))))))))))))))){
/* eval.scm:1254: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[306];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
if(C_truep(C_i_memq(t1,((C_word*)t0)[3]))){
/* eval.scm:1256: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[307];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_10741,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10816,a[2]=((C_word*)t0)[3],a[3]=((C_word)li220),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1257: any */
f_8477(t3,t4,((C_word*)t0)[7]);}}}}

/* k10739 in k10697 in ##sys#process-require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,3)))){
C_save_and_reclaim((void *)f_10741,c,av);}
a=C_alloc(30);
if(C_truep(t1)){
/* eval.scm:1258: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[308];
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
if(C_truep(C_i_memq(((C_word*)t0)[3],lf[183]))){
if(C_truep(((C_word*)t0)[4])){
t2=C_a_i_list(&a,2,lf[309],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,lf[117],t2);
/* eval.scm:1261: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t3;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}
else{
t2=C_a_i_list(&a,2,lf[23],((C_word*)t0)[3]);
t3=C_a_i_list(&a,2,lf[263],t2);
/* eval.scm:1262: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t3;
av2[3]=C_SCHEME_FALSE;
C_values(4,av2);}}}
else{
t2=(C_truep(((C_word*)t0)[4])?((C_word*)t0)[5]:C_SCHEME_FALSE);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10784,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1264: mark-static */
t4=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t3=C_a_i_list(&a,2,lf[23],((C_word*)t0)[3]);
t4=C_a_i_list(&a,2,lf[23],((C_word*)t0)[7]);
t5=C_a_i_list(&a,2,lf[23],lf[107]);
t6=C_a_i_list(&a,4,lf[106],t3,t4,t5);
/* eval.scm:1267: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t6;
av2[3]=lf[311];
C_values(4,av2);}}}}}

/* k10782 in k10739 in k10697 in ##sys#process-require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_10784,c,av);}
a=C_alloc(12);
t2=C_a_i_list(&a,2,lf[309],((C_word*)t0)[2]);
t3=C_a_i_list(&a,2,lf[117],t2);
/* eval.scm:1265: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t3;
av2[3]=lf[310];
C_values(4,av2);}}

/* a10815 in k10697 in ##sys#process-require in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10816,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_memq(t2,((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_10886(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_10886,c,av);}
a=C_alloc(23);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10889,a[2]=t7,a[3]=((C_word)li222),tmp=(C_word)a,a+=4,tmp));
t11=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10922,a[2]=t3,a[3]=t7,a[4]=((C_word)li223),tmp=(C_word)a,a+=5,tmp));
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10961,a[2]=t1,a[3]=t9,a[4]=t2,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
t13=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11015,a[2]=t9,a[3]=t12,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1296: make-relative-pathname */
f_9418(t13,t5,t2);}

/* test-extensions in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_fcall f_10889(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_10889,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t3))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10902,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1282: file-exists? */
f_10289(t4,t2);}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10905,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1283: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t4;
av2[2]=t2;
av2[3]=C_i_car(t3);
tp(4,av2);}}}

/* k10900 in test-extensions in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_10902,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?((C_word*)t0)[3]:C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k10903 in test-extensions in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_10905,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10908,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1284: file-exists? */
f_10289(t2,t1);}

/* k10906 in k10903 in test-extensions in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_10908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_10908,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:1285: test-extensions */
t2=((C_word*)((C_word*)t0)[3])[1];
f_10889(t2,((C_word*)t0)[2],((C_word*)t0)[4],C_u_i_cdr(((C_word*)t0)[5]));}}

/* test in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_fcall f_10922(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_10922,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
/* eval.scm:1287: test-extensions */
t3=((C_word*)((C_word*)t0)[3])[1];
f_10889(t3,t1,t2,((C_word*)t0)[2]);}
else{
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10958,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1290: chicken.platform#feature? */
t4=*((C_word*)lf[248]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[249];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k10956 in test in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_10958,c,av);}
a=C_alloc(6);
if(C_truep(C_i_not(t1))){
t2=C_a_i_list1(&a,1,lf[187]);
/* eval.scm:1287: test-extensions */
t3=((C_word*)((C_word*)t0)[2])[1];
f_10889(t3,((C_word*)t0)[3],((C_word*)t0)[4],t2);}
else{
t2=C_i_not(((C_word*)t0)[5]);
t3=(C_truep(t2)?C_a_i_list2(&a,2,*((C_word*)lf[190]+1),lf[187]):C_a_i_list2(&a,2,lf[187],*((C_word*)lf[190]+1)));
/* eval.scm:1287: test-extensions */
t4=((C_word*)((C_word*)t0)[2])[1];
f_10889(t4,((C_word*)t0)[3],((C_word*)t0)[4],t3);}}

/* k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_10961(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_10961,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10971,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[5])){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11008,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1300: chicken.platform#repository-path */
t4=*((C_word*)lf[282]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=*((C_word*)lf[279]+1);
f_10971(2,av2);}}}}

/* k10969 in k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_10971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_10971,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_10973,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li224),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_10973(t5,((C_word*)t0)[4],t1);}

/* loop in k10969 in k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_fcall f_10973(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_10973,3,t0,t1,t2);}
a=C_alloc(9);
t3=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_10983,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10997,a[2]=((C_word*)t0)[3],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1303: string-append */
t6=*((C_word*)lf[199]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=lf[313];
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}}

/* k10981 in loop in k10969 in k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10983,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:1306: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_10973(t2,((C_word*)t0)[2],C_slot(((C_word*)t0)[4],C_fix(1)));}}

/* k10995 in loop in k10969 in k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_10997(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_10997,c,av);}
/* eval.scm:1303: test */
t2=((C_word*)((C_word*)t0)[2])[1];
f_10922(t2,((C_word*)t0)[3],t1);}

/* k11006 in k10959 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11008(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11008,c,av);}
if(C_truep(t1)){
/* eval.scm:1298: ##sys#append */
t2=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[279]+1);
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
/* eval.scm:1298: ##sys#append */
t2=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[279]+1);
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k11013 in ##sys#resolve-include-filename in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11015(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11015,c,av);}
/* eval.scm:1296: test */
t2=((C_word*)((C_word*)t0)[2])[1];
f_10922(t2,((C_word*)t0)[3],t1);}

/* run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_11018(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_11018,2,t1,t2);}
a=C_alloc(7);
t3=lf[314] /* last-error */ =C_SCHEME_FALSE;;
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11023,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11028,a[2]=t2,a[3]=((C_word)li232),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1326: scheme#call-with-current-continuation */
t6=*((C_word*)lf[318]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t4;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k11021 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11023(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11023,c,av);}
/* eval.scm:1324: g2854 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_11028,c,av);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11034,a[2]=t2,a[3]=((C_word)li227),tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11053,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word)li231),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1326: chicken.condition#with-exception-handler */
t5=*((C_word*)lf[317]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t3;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* a11033 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11034,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11040,a[2]=t2,a[3]=((C_word)li226),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1326: k2851 */
t4=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a11039 in a11033 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11040,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11044,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1327: chicken.base#open-output-string */
t3=*((C_word*)lf[15]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11042 in a11039 in a11033 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11044,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11047,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1328: chicken.condition#print-error-message */
t3=*((C_word*)lf[316]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k11045 in k11042 in a11039 in a11033 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_11047(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11047,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11051,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1329: chicken.base#get-output-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11049 in k11045 in k11042 in a11039 in a11033 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in ... */
static void C_ccall f_11051(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11051,c,av);}
t2=C_mutate(&lf[314] /* (set! last-error ...) */,t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a11052 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11053(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_11053,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11059,a[2]=((C_word*)t0)[2],a[3]=((C_word)li228),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11065,a[2]=((C_word*)t0)[3],a[3]=((C_word)li230),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1326: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a11058 in a11052 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11059(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11059,c,av);}
/* eval.scm:1331: thunk */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* a11064 in a11052 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11065(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +4,c,2)))){
C_save_and_reclaim((void*)f_11065,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+4);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11071,a[2]=t2,a[3]=((C_word)li229),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1326: k2851 */
t4=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t3;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* a11070 in a11064 in a11052 in a11027 in run-safe in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11071(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11071,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
C_apply_values(3,av2);}}

/* store-result in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_11077(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_11077,3,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11081,a[2]=t3,a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1338: ##sys#gc */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[320]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[320]+1);
av2[1]=t4;
av2[2]=C_SCHEME_FALSE;
tp(3,av2);}}

/* k11079 in store-result in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11081,c,av);}
if(C_truep(((C_word*)t0)[2])){
t2=C_store_result(((C_word*)t0)[3],((C_word*)t0)[2]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* CHICKEN_yield in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11086(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11086,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11092,a[2]=((C_word)li235),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1344: run-safe */
f_11018(t1,t2);}

/* a11091 in CHICKEN_yield in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11092(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11092,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11096,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1344: ##sys#thread-yield! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[322]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[322]+1);
av2[1]=t2;
tp(2,av2);}}

/* k11094 in a11091 in CHICKEN_yield in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11096,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* CHICKEN_eval in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11098(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11098,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11104,a[2]=t3,a[3]=t2,a[4]=((C_word)li237),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1347: run-safe */
f_11018(t1,t4);}

/* a11103 in CHICKEN_eval in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11104,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11112,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1349: scheme#eval */
t3=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11110 in a11103 in CHICKEN_eval in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11112,c,av);}
/* eval.scm:1349: store-result */
f_11077(((C_word*)t0)[2],t1,((C_word*)t0)[3]);}

/* CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11114(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11114,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11118,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1351: ##sys#peek-c-string */
t5=*((C_word*)lf[234]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k11116 in CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11118,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11123,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li239),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1352: run-safe */
f_11018(((C_word*)t0)[3],t2);}

/* a11122 in k11116 in CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11123,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11127,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1354: chicken.base#open-input-string */
t3=*((C_word*)lf[325]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11125 in a11122 in k11116 in CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11127,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11134,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11138,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1355: scheme#read */
t4=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11132 in k11125 in a11122 in k11116 in CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11134(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11134,c,av);}
/* eval.scm:1355: store-result */
f_11077(((C_word*)t0)[2],t1,((C_word*)t0)[3]);}

/* k11136 in k11125 in a11122 in k11116 in CHICKEN_eval_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11138(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11138,c,av);}
/* eval.scm:1355: scheme#eval */
t2=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* store-string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static C_word C_fcall f_11140(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_stack_overflow_check;{}
t4=C_block_size(t1);
if(C_truep(C_fixnum_greater_or_equal_p(t4,t2))){
t5=C_mutate(&lf[314] /* (set! last-error ...) */,lf[327]);
return(C_SCHEME_FALSE);}
else{
return(C_copy_result_string(t1,t3,t4));}}

/* CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11153(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11153,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11159,a[2]=t4,a[3]=t3,a[4]=t2,a[5]=((C_word)li242),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1371: run-safe */
f_11018(t1,t5);}

/* a11158 in CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11159(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11159,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11163,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1373: chicken.base#open-output-string */
t3=*((C_word*)lf[15]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11161 in a11158 in CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11163(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11163,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11166,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11177,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1374: scheme#eval */
t4=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k11164 in k11161 in a11158 in CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11166,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11173,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1375: chicken.base#get-output-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11171 in k11164 in k11161 in a11158 in CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11173(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11173,c,av);}
/* eval.scm:1375: store-string */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* eval.scm:1375: store-string */
  f_11140(t1,((C_word*)t0)[3],((C_word*)t0)[4])
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11175 in k11161 in a11158 in CHICKEN_eval_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11177(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11177,c,av);}
/* eval.scm:1374: scheme#write */
t2=*((C_word*)lf[14]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_11179,c,av);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11183,a[2]=t4,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1377: ##sys#peek-c-string */
t6=*((C_word*)lf[234]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}

/* k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11183(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11183,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11188,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word)li244),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1380: run-safe */
f_11018(((C_word*)t0)[4],t2);}

/* a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11188,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11192,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1382: chicken.base#open-output-string */
t3=*((C_word*)lf[15]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11192(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_11192,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11195,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11206,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11210,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11214,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1383: chicken.base#open-input-string */
t6=*((C_word*)lf[325]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k11193 in k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11195(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11195,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11202,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1384: chicken.base#get-output-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11200 in k11193 in k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_11202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11202,c,av);}
/* eval.scm:1384: store-string */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* eval.scm:1384: store-string */
  f_11140(t1,((C_word*)t0)[3],((C_word*)t0)[4])
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11204 in k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11206(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11206,c,av);}
/* eval.scm:1383: scheme#write */
t2=*((C_word*)lf[14]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k11208 in k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11210,c,av);}
/* eval.scm:1383: scheme#eval */
t2=*((C_word*)lf[153]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k11212 in k11190 in a11187 in k11181 in CHICKEN_eval_string_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11214(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11214,c,av);}
/* eval.scm:1383: scheme#read */
t2=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* CHICKEN_apply in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11216(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_11216,c,av);}
a=C_alloc(6);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11222,a[2]=t4,a[3]=t2,a[4]=t3,a[5]=((C_word)li246),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1389: run-safe */
f_11018(t1,t5);}

/* a11221 in CHICKEN_apply in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11222,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11230,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
C_apply(4,av2);}}

/* k11228 in a11221 in CHICKEN_apply in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11230(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11230,c,av);}
/* eval.scm:1389: store-result */
f_11077(((C_word*)t0)[2],t1,((C_word*)t0)[3]);}

/* CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11232,c,av);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11238,a[2]=t5,a[3]=t4,a[4]=t2,a[5]=t3,a[6]=((C_word)li248),tmp=(C_word)a,a+=7,tmp);
/* eval.scm:1394: run-safe */
f_11018(t1,t6);}

/* a11237 in CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11238(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11238,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11242,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:1396: chicken.base#open-output-string */
t3=*((C_word*)lf[15]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k11240 in a11237 in CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_11242,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11245,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11256,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
C_apply(4,av2);}}

/* k11243 in k11240 in a11237 in CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11245,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11252,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1398: chicken.base#get-output-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11250 in k11243 in k11240 in a11237 in CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11252,c,av);}
/* eval.scm:1398: store-string */
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(
/* eval.scm:1398: store-string */
  f_11140(t1,((C_word*)t0)[3],((C_word*)t0)[4])
);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11254 in k11240 in a11237 in CHICKEN_apply_to_string in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11256(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11256,c,av);}
/* eval.scm:1397: scheme#write */
t2=*((C_word*)lf[14]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* CHICKEN_read in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_11258,c,av);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11262,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1400: ##sys#peek-c-string */
t5=*((C_word*)lf[234]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k11260 in CHICKEN_read in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11262,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11267,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li250),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1401: run-safe */
f_11018(((C_word*)t0)[3],t2);}

/* a11266 in k11260 in CHICKEN_read in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11267(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11267,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11271,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1403: chicken.base#open-input-string */
t3=*((C_word*)lf[325]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11269 in a11266 in k11260 in CHICKEN_read in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11271(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11271,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11278,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1404: scheme#read */
t3=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11276 in k11269 in a11266 in k11260 in CHICKEN_read in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_11278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11278,c,av);}
/* eval.scm:1404: store-result */
f_11077(((C_word*)t0)[2],t1,((C_word*)t0)[3]);}

/* CHICKEN_load in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_11280,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11284,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1406: ##sys#peek-c-string */
t4=*((C_word*)lf[234]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k11282 in CHICKEN_load in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_11284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_11284,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11289,a[2]=t1,a[3]=((C_word)li252),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1407: run-safe */
f_11018(((C_word*)t0)[2],t2);}

/* a11288 in k11282 in CHICKEN_load in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_11289(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11289,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11293,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1407: scheme#load */
t3=*((C_word*)lf[250]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k11291 in a11288 in k11282 in CHICKEN_load in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_11293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11293,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* CHICKEN_get_error_message in k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_11295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11295,c,av);}
t4=lf[314];
if(C_truep(lf[314])){
t5=lf[314];
t6=lf[314];
/* eval.scm:1410: store-string */
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=(
/* eval.scm:1410: store-string */
  f_11140(lf[314],t3,t2)
);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
/* eval.scm:1410: store-string */
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(
/* eval.scm:1410: store-string */
  f_11140(lf[335],t3,t2)
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}

/* k11305 in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_11307(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(45,c,6)))){
C_save_and_reclaim((void *)f_11307,c,av);}
a=C_alloc(45);
t2=C_a_i_list1(&a,1,t1);
t3=C_mutate((C_word*)lf[279]+1 /* (set! ##sys#include-pathnames ...) */,t2);
t4=C_mutate((C_word*)lf[271]+1 /* (set! ##sys#resolve-include-filename ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10886,a[2]=((C_word)li225),tmp=(C_word)a,a+=3,tmp));
t5=lf[314] /* last-error */ =C_SCHEME_FALSE;;
t6=C_mutate(&lf[315] /* (set! run-safe ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11018,a[2]=((C_word)li233),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate(&lf[319] /* (set! store-result ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11077,a[2]=((C_word)li234),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate(&lf[321] /* (set! CHICKEN_yield ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11086,a[2]=((C_word)li236),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate(&lf[323] /* (set! CHICKEN_eval ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11098,a[2]=((C_word)li238),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate(&lf[324] /* (set! CHICKEN_eval_string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11114,a[2]=((C_word)li240),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate(&lf[326] /* (set! store-string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11140,a[2]=((C_word)li241),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate(&lf[328] /* (set! CHICKEN_eval_to_string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11153,a[2]=((C_word)li243),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate(&lf[329] /* (set! CHICKEN_eval_string_to_string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11179,a[2]=((C_word)li245),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate(&lf[330] /* (set! CHICKEN_apply ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11216,a[2]=((C_word)li247),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate(&lf[331] /* (set! CHICKEN_apply_to_string ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11232,a[2]=((C_word)li249),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate(&lf[332] /* (set! CHICKEN_read ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11258,a[2]=((C_word)li251),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate(&lf[333] /* (set! CHICKEN_load ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11280,a[2]=((C_word)li253),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate(&lf[334] /* (set! CHICKEN_get_error_message ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11295,a[2]=((C_word)li254),tmp=(C_word)a,a+=3,tmp));
t19=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t19;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t19+1)))(2,av2);}}

/* k11312 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_11314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_11314,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11316,a[2]=((C_word)li255),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1118: chicken.base#make-parameter */
t3=*((C_word*)lf[337]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a11315 in k11312 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_11316(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11316,c,av);}
t3=C_i_check_list(t2);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* map-loop2388 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_fcall f_11322(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_11322,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11347,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:1119: g2394 */
t4=((C_word*)t0)[4];
f_10042(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k11345 in map-loop2388 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_11347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11347,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_11322(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k11359 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11361(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_11361,c,av);}
/* eval.scm:1111: scheme#string-append */
t2=*((C_word*)lf[199]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[189];
av2[3]=lf[338];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k11371 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_11373,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];
f_9371(t2,C_a_i_list(&a,1,t1));}

/* k11375 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11377,c,av);}
/* eval.scm:946: scheme#string-append */
t2=*((C_word*)lf[199]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[342];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k11402 in k11406 in k11410 in k11414 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11404,c,av);}
t2=C_eqp(t1,lf[349]);
t3=((C_word*)t0)[2];
f_9362(t3,(C_truep(t2)?lf[350]:lf[185]));}

/* k11406 in k11410 in k11414 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11408,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[348]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11404,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:931: chicken.platform#machine-type */
t4=*((C_word*)lf[351]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=((C_word*)t0)[2];
f_9362(t3,lf[185]);}}

/* k11410 in k11414 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11412,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[346]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];
f_9362(t3,lf[347]);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11408,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:930: chicken.platform#software-version */
t4=*((C_word*)lf[343]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11414 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_11416,c,av);}
a=C_alloc(3);
t2=C_eqp(t1,lf[344]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];
f_9362(t3,lf[345]);}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11412,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:929: chicken.platform#software-version */
t4=*((C_word*)lf[343]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11420(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_11420,c,av);}
a=C_alloc(27);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(2),t1);
t3=C_mutate((C_word*)lf[174]+1 /* (set! scheme#scheme-report-environment ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8024,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word)li160),tmp=(C_word)a,a+=5,tmp));
t4=C_mutate((C_word*)lf[178]+1 /* (set! scheme#null-environment ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8049,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=((C_word)li161),tmp=(C_word)a,a+=5,tmp));
t5=C_a_i_provide(&a,1,lf[181]);
t6=C_mutate(&lf[182] /* (set! chicken.load#any ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8477,a[2]=((C_word)li163),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate(&lf[183] /* (set! chicken.load#constant2087 ...) */,lf[184]);
t8=C_mutate(&lf[185] /* (set! chicken.load#constant2117 ...) */,lf[186]);
t9=C_mutate(&lf[187] /* (set! chicken.load#constant2126 ...) */,lf[188]);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9362,a[2]=((C_word*)t0)[6],tmp=(C_word)a,a+=3,tmp);
t11=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11416,a[2]=t10,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:928: chicken.platform#software-type */
t12=*((C_word*)lf[352]+1);{
C_word *av2=av;
av2[0]=t12;
av2[1]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(2,av2);}}

/* k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11428,c,av);}
a=C_alloc(7);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(2),t1);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11420,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:857: strip */
f_7966(t3,C_slot(((C_word*)t0)[3],C_fix(2)));}

/* k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11436(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_11436,c,av);}
a=C_alloc(8);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(2),t1);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11428,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:856: strip */
f_7966(t3,C_slot(((C_word*)t0)[3],C_fix(2)));}

/* k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11444(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_11444,c,av);}
a=C_alloc(8);
t2=C_i_setslot(((C_word*)t0)[2],C_fix(2),t1);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11436,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:855: strip */
f_7966(t3,C_slot(((C_word*)t0)[3],C_fix(2)));}

/* a11449 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_11450,c,av);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11454,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:823: ##sys#print */
t5=*((C_word*)lf[362]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[363];
av2[3]=C_SCHEME_FALSE;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k11452 in a11449 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11454,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11457,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:824: ##sys#print */
t3=*((C_word*)lf[362]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_slot(((C_word*)t0)[4],C_fix(1));
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k11455 in k11452 in a11449 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_11457,c,av);}
/* eval.scm:825: ##sys#write-char-0 */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[361]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[361]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(62);
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_11466,c,av);}
a=C_alloc(14);
t2=C_SCHEME_END_OF_LIST;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11472,a[2]=t5,a[3]=t3,a[4]=((C_word)li258),tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11493,a[2]=t5,a[3]=t3,a[4]=((C_word)li262),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:778: scheme#values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t1;
av2[2]=t6;
av2[3]=t7;
C_values(4,av2);}}

/* a11471 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11472(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_11472,c,av);}
a=C_alloc(12);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11476,a[2]=t1,a[3]=t4,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11491,a[2]=t2,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t5,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:781: ##sys#get */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[5]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[5]+1);
av2[1]=t6;
av2[2]=t2;
av2[3]=t3;
tp(4,av2);}}
else{
t6=t5;
f_11476(t6,C_SCHEME_UNDEFINED);}}

/* k11474 in a11471 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_11476(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,4)))){
C_save_and_reclaim_args((void *)trf_11476,2,t0,t1);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11479,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:782: ##sys#put! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[366]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[366]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[3];
tp(5,av2);}}

/* k11477 in k11474 in a11471 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11479,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k11489 in a11471 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11491(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_11491,c,av);}
a=C_alloc(12);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)((C_word*)t0)[4])[1]);
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,t3);
t5=((C_word*)t0)[5];
f_11476(t5,t4);}

/* a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_11493,c,av);}
a=C_alloc(15);
t3=C_SCHEME_FALSE;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)((C_word*)t0)[2])[1];
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11499,a[2]=t4,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word)li259),tmp=(C_word)a,a+=6,tmp);
t7=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11504,a[2]=((C_word*)t0)[2],a[3]=t5,a[4]=t4,a[5]=((C_word*)t0)[3],a[6]=((C_word)li261),tmp=(C_word)a,a+=7,tmp);
/* eval.scm:787: scheme#dynamic-wind */
t8=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t1;
av2[2]=t6;
av2[3]=t2;
av2[4]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}

/* a11498 in a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11499(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11499,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)((C_word*)t0)[3])[1]);
t3=C_set_block_item(((C_word*)t0)[4],0,C_SCHEME_TRUE);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a11503 in a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_11504,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11508,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11511,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t4,a[5]=((C_word)li260),tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_11511(t6,t2);}

/* k11506 in a11503 in a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11508,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop1483 in a11503 in a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_11511(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_11511,2,t0,t1);}
a=C_alloc(5);
t2=C_eqp(((C_word*)((C_word*)t0)[2])[1],((C_word*)((C_word*)t0)[3])[1]);
if(C_truep(t2)){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11521,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);{
C_word av2[4];
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[366]+1);
av2[3]=C_i_car(((C_word*)((C_word*)t0)[3])[1]);
C_apply(4,av2);}}}

/* k11519 in doloop1483 in a11503 in a11492 in a11465 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11521,c,av);}
t2=C_i_cdr(((C_word*)((C_word*)t0)[2])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=((C_word*)((C_word*)t0)[3])[1];
f_11511(t4,((C_word*)t0)[4]);}

/* a11533 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11534,c,av);}
t4=C_mutate((C_word*)lf[367]+1 /* (set! ##sys#put/restore! ...) */,t2);
t5=C_mutate((C_word*)lf[97]+1 /* (set! ##sys#with-property-restore ...) */,t3);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11539(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_11539,c,av);}
a=C_alloc(5);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11546,a[2]=t4,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:741: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t5;
tp(2,av2);}}

/* k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_11546,c,av);}
a=C_alloc(35);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11549,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_nullp(t1))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11639,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_cons(&a,2,lf[167],*((C_word*)lf[368]+1));
t5=C_a_i_cons(&a,2,lf[166],*((C_word*)lf[369]+1));
t6=C_a_i_list(&a,3,lf[31],t4,t5);
/* eval.scm:747: compile-to-closure */
f_3572(t3,t6,C_SCHEME_END_OF_LIST,C_a_i_list(&a,4,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_TRUE));}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_11549(2,av2);}}}

/* k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,4)))){
C_save_and_reclaim((void *)f_11549,c,av);}
a=C_alloc(29);
if(C_truep(((C_word*)t0)[2])){
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[158],lf[0]);
t3=C_slot(((C_word*)t0)[2],C_fix(2));
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11561,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t3)){
t5=*((C_word*)lf[89]+1);
t6=*((C_word*)lf[9]+1);
t7=C_SCHEME_END_OF_LIST;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=t3;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_SCHEME_TRUE;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11569,a[2]=t8,a[3]=t10,a[4]=t12,a[5]=t6,a[6]=t5,a[7]=((C_word)li265),tmp=(C_word)a,a+=8,tmp);
t14=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_11599,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word)li266),tmp=(C_word)a,a+=5,tmp);
t15=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11609,a[2]=t8,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li267),tmp=(C_word)a,a+=7,tmp);
/* eval.scm:755: ##sys#dynamic-wind */
t16=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t16;
av2[1]=t4;
av2[2]=t13;
av2[3]=t14;
av2[4]=t15;
((C_proc)(void*)(*((C_word*)t16+1)))(5,av2);}}
else{
/* eval.scm:758: compile-to-closure */
f_3572(t4,((C_word*)t0)[4],C_SCHEME_END_OF_LIST,C_a_i_list(&a,4,C_SCHEME_FALSE,((C_word*)t0)[2],C_SCHEME_FALSE,C_SCHEME_TRUE));}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11630,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:761: compile-to-closure */
f_3572(t2,((C_word*)t0)[4],C_SCHEME_END_OF_LIST,C_a_i_list(&a,4,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_TRUE));}}

/* k11559 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11561,c,av);}
/* eval.scm:753: g1431 */
t2=t1;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11569(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_11569,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11573,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
/* eval.scm:755: ##sys#macro-environment1434 */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_11573(2,av2);}}}

/* k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_11573,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11576,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
/* eval.scm:755: ##sys#current-environment1435 */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
f_11576(2,av2);}}}

/* k11574 in k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_11576,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_11580,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:755: ##sys#macro-environment1434 */
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k11578 in k11574 in k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_11580,c,av);}
a=C_alloc(9);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_11584,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:755: ##sys#current-environment1435 */
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k11582 in k11578 in k11574 in k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_11584,c,av);}
a=C_alloc(6);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_11587,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:755: ##sys#macro-environment1434 */
t4=((C_word*)t0)[7];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[8];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k11585 in k11582 in k11578 in k11574 in k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11587(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_11587,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_11590,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:755: ##sys#current-environment1435 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k11588 in k11585 in k11582 in k11578 in k11574 in k11571 in a11568 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11590,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a11598 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11599(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_11599,c,av);}
a=C_alloc(12);
t2=C_slot(((C_word*)t0)[2],C_fix(3));
/* eval.scm:757: compile-to-closure */
f_3572(t1,((C_word*)t0)[3],C_SCHEME_END_OF_LIST,C_a_i_list(&a,4,C_SCHEME_FALSE,((C_word*)t0)[2],t2,C_SCHEME_TRUE));}

/* a11608 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11609(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_11609,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11613,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:755: ##sys#macro-environment1434 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k11611 in a11608 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_11613,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11616,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:755: ##sys#current-environment1435 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k11614 in k11611 in a11608 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11616(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_11616,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11619,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:755: ##sys#macro-environment1434 */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k11617 in k11614 in k11611 in a11608 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11619(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_11619,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_11622,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:755: ##sys#current-environment1435 */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k11620 in k11617 in k11614 in k11611 in a11608 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11622(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_11622,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,((C_word*)t0)[5]);
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k11628 in k11547 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11630,c,av);}
/* eval.scm:751: g1463 */
t2=t1;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k11637 in k11544 in a11538 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_11639(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_11639,c,av);}
/* eval.scm:746: g1425 */
t2=t1;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k3515 */
static void C_ccall f_3517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3517,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3520,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k3518 in k3515 */
static void C_ccall f_3520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3520,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3523,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k3521 in k3518 in k3515 */
static void C_ccall f_3523(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3523,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3526,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_3526,c,av);}
a=C_alloc(19);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_set_block_item(lf[2] /* ##sys#unbound-in-eval */,0,C_SCHEME_FALSE);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3570,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:79: chicken.base#make-parameter */
t6=*((C_word*)lf[337]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_fix(1);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* a3533 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3534(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3534,c,av);}
t3=C_immp(t2);
t4=C_i_not(t3);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(C_truep(t4)?C_lambdainfop(t2):C_SCHEME_FALSE);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* a3546 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_3547,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3555,a[2]=t2,a[3]=t3,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3559,a[2]=t4,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:73: chicken.base#open-output-string */
t6=*((C_word*)lf[15]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k3553 in a3546 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3555,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k3557 in a3546 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_3559,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3562,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:74: scheme#write */
t3=*((C_word*)lf[14]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3560 in k3557 in a3546 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_3562,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3565,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:75: chicken.base#get-output-string */
t3=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k3563 in k3560 in k3557 in a3546 in decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3565,c,av);}
/* eval.scm:72: ##sys#make-lambda-info */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[12]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[12]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_3570,c,av);}
a=C_alloc(12);
t2=C_mutate((C_word*)lf[3]+1 /* (set! ##sys#eval-debug-level ...) */,t1);
t3=C_mutate(&lf[4] /* (set! chicken.eval#compile-to-closure ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3572,a[2]=((C_word)li145),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[70]+1 /* (set! ##sys#eval/meta ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7729,a[2]=((C_word)li151),tmp=(C_word)a,a+=3,tmp));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7842,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11539,a[2]=((C_word)li268),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:739: chicken.base#make-parameter */
t7=*((C_word*)lf[337]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t5;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3572(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(55,0,8)))){
C_save_and_reclaim_args((void *)trf_3572,4,t1,t2,t3,t4);}
a=C_alloc(55);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_i_car(t4));
t7=C_i_nullp(t4);
t8=(C_truep(t7)?C_SCHEME_END_OF_LIST:C_i_cdr(t4));
t9=C_i_nullp(t8);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_i_car(t8));
t11=C_i_nullp(t8);
t12=(C_truep(t11)?C_SCHEME_END_OF_LIST:C_i_cdr(t8));
t13=C_i_nullp(t12);
t14=(C_truep(t13)?C_SCHEME_FALSE:C_i_car(t12));
t15=C_i_nullp(t12);
t16=(C_truep(t15)?C_SCHEME_END_OF_LIST:C_i_cdr(t12));
t17=C_i_nullp(t16);
t18=(C_truep(t17)?C_SCHEME_FALSE:C_i_car(t16));
t19=C_i_nullp(t16);
t20=(C_truep(t19)?C_SCHEME_END_OF_LIST:C_i_cdr(t16));
t21=C_SCHEME_UNDEFINED;
t22=(*a=C_VECTOR_TYPE|1,a[1]=t21,tmp=(C_word)a,a+=2,tmp);
t23=C_SCHEME_UNDEFINED;
t24=(*a=C_VECTOR_TYPE|1,a[1]=t23,tmp=(C_word)a,a+=2,tmp);
t25=C_SCHEME_UNDEFINED;
t26=(*a=C_VECTOR_TYPE|1,a[1]=t25,tmp=(C_word)a,a+=2,tmp);
t27=C_SCHEME_UNDEFINED;
t28=(*a=C_VECTOR_TYPE|1,a[1]=t27,tmp=(C_word)a,a+=2,tmp);
t29=C_SCHEME_UNDEFINED;
t30=(*a=C_VECTOR_TYPE|1,a[1]=t29,tmp=(C_word)a,a+=2,tmp);
t31=C_SCHEME_UNDEFINED;
t32=(*a=C_VECTOR_TYPE|1,a[1]=t31,tmp=(C_word)a,a+=2,tmp);
t33=C_SCHEME_UNDEFINED;
t34=(*a=C_VECTOR_TYPE|1,a[1]=t33,tmp=(C_word)a,a+=2,tmp);
t35=C_set_block_item(t22,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3599,a[2]=t22,a[3]=((C_word)li0),tmp=(C_word)a,a+=4,tmp));
t36=C_set_block_item(t24,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3634,a[2]=t22,a[3]=((C_word)li3),tmp=(C_word)a,a+=4,tmp));
t37=C_set_block_item(t26,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3668,a[2]=t24,a[3]=((C_word)li7),tmp=(C_word)a,a+=4,tmp));
t38=C_set_block_item(t28,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3757,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp));
t39=C_set_block_item(t30,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3781,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t40=C_set_block_item(t32,0,(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_3787,a[2]=t26,a[3]=t14,a[4]=t32,a[5]=t10,a[6]=t30,a[7]=t24,a[8]=t34,a[9]=((C_word)li131),tmp=(C_word)a,a+=10,tmp));
t41=C_set_block_item(t34,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7336,a[2]=t28,a[3]=t32,a[4]=((C_word)li144),tmp=(C_word)a,a+=5,tmp));
t42=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7679,a[2]=t32,a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t6,a[7]=t18,tmp=(C_word)a,a+=8,tmp);
/* eval.scm:712: ##sys#eval-debug-level */
t43=*((C_word*)lf[3]+1);{
C_word av2[2];
av2[0]=t43;
av2[1]=t42;
((C_proc)(void*)(*((C_word*)t43+1)))(2,av2);}}

/* find-id in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3599(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_3599,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t3))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3612,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t5=C_i_caar(t3);
t6=C_eqp(t2,t5);
if(C_truep(t6)){
t7=C_u_i_car(t3);
t8=t4;
f_3612(t8,C_i_symbolp(C_u_i_cdr(t7)));}
else{
t7=t4;
f_3612(t7,C_SCHEME_FALSE);}}}

/* k3610 in find-id in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3612(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_3612,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=C_u_i_cdr(C_u_i_car(((C_word*)t0)[3]));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:91: find-id */
t2=((C_word*)((C_word*)t0)[4])[1];
f_3599(t2,((C_word*)t0)[2],((C_word*)t0)[5],C_u_i_cdr(((C_word*)t0)[3]));}}

/* rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3634(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_3634,3,t0,t1,t2);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3638,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3666,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:94: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}

/* k3636 in rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3638,c,av);}
a=C_alloc(8);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3646,a[2]=((C_word*)t0)[3],a[3]=((C_word)li1),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3652,a[2]=((C_word*)t0)[3],a[3]=((C_word)li2),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:94: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}}

/* a3645 in k3636 in rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3646(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3646,c,av);}
/* eval.scm:95: ##sys#get */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[5]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[5]+1);
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[6];
tp(4,av2);}}

/* a3651 in k3636 in rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3652(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +5,c,3)))){
C_save_and_reclaim((void*)f_3652,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+5);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3659,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[8]+1);
av2[3]=t2;
C_apply(4,av2);}}

/* k3657 in a3651 in k3636 in rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3659,c,av);}
if(C_truep(t1)){{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[7]+1);
av2[3]=((C_word*)t0)[3];
C_apply(4,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k3664 in rename in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3666,c,av);}
/* eval.scm:94: find-id */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3599(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* lookup in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3668(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3668,4,t0,t1,t2,t3);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3672,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:99: rename */
t5=((C_word*)((C_word*)t0)[2])[1];
f_3634(t5,t4,t2);}

/* k3670 in lookup in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_3672,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3680,a[2]=t1,a[3]=t3,a[4]=((C_word)li6),tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_3680(t5,((C_word*)t0)[2],((C_word*)t0)[3],C_fix(0));}

/* loop in k3670 in lookup in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3680(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,3)))){
C_save_and_reclaim_args((void *)trf_3680,4,t0,t1,t2,t3);}
a=C_alloc(8);
if(C_truep(C_i_nullp(t2))){
/* eval.scm:102: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=C_SCHEME_FALSE;
av2[3]=((C_word*)t0)[2];
C_values(4,av2);}}
else{
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3727,a[2]=((C_word*)t0)[2],a[3]=((C_word)li4),tmp=(C_word)a,a+=4,tmp);
t6=(
  f_3727(t5,t4,C_fix(0))
);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3697,a[2]=t3,a[3]=((C_word)li5),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:102: g237 */
t8=t7;
f_3697(t8,t1,t6);}
else{
/* eval.scm:104: loop */
t9=t1;
t10=C_slot(t2,C_fix(1));
t11=C_fixnum_plus(t3,C_fix(1));
t1=t9;
t2=t10;
t3=t11;
goto loop;}}}

/* g237 in loop in k3670 in lookup in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3697(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_3697,3,t0,t1,t2);}
/* eval.scm:103: scheme#values */{
C_word av2[4];
av2[0]=0;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
C_values(4,av2);}}

/* loop in loop in k3670 in lookup in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_3727(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(C_SCHEME_FALSE);}
else{
t3=C_slot(t1,C_fix(0));
t4=C_eqp(((C_word*)t0)[2],t3);
if(C_truep(t4)){
return(t2);}
else{
t6=C_slot(t1,C_fix(1));
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}}

/* emit-trace-info in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_3757(C_word *a,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_stack_overflow_check;{}
if(C_truep(t1)){
t6=C_a_i_record4(&a,4,lf[10],t3,t4,t5);
return(C_emit_eval_trace_info(t2,t6,C_slot(*((C_word*)lf[11]+1),C_fix(14))));}
else{
t6=C_SCHEME_UNDEFINED;
return(t6);}}

/* decorate in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3781(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,4)))){
C_save_and_reclaim_args((void *)trf_3781,3,t1,t2,t3);}
a=C_alloc(7);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3534,a[2]=((C_word)li9),tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3547,a[2]=t3,a[3]=((C_word)li10),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:66: ##sys#decorate-lambda */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[16]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[16]+1);
av2[1]=t1;
av2[2]=t2;
av2[3]=t4;
av2[4]=t5;
tp(5,av2);}}

/* compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3787(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,2)))){
C_save_and_reclaim_args((void *)trf_3787,8,t0,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(16);
t8=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_3794,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t3,a[6]=t6,a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=t4,a[10]=t5,a[11]=t7,a[12]=((C_word*)t0)[5],a[13]=((C_word*)t0)[6],a[14]=((C_word*)t0)[7],a[15]=((C_word*)t0)[8],tmp=(C_word)a,a+=16,tmp);
/* eval.scm:132: chicken.keyword#keyword? */
t9=*((C_word*)lf[146]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_3794,c,av);}
a=C_alloc(16);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3795,a[2]=((C_word*)t0)[2],a[3]=((C_word)li12),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3807,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word)li13),tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3813,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word)li22),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:134: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4004,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[4],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
/* eval.scm:172: ##sys#number? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[145]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[145]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}}}

/* f_3795 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3795(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3795,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a3806 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3807(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3807,c,av);}
/* eval.scm:134: lookup */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3668(t2,t1,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_3813,c,av);}
a=C_alloc(12);
if(C_truep(C_i_not(t2))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3823,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t5=C_i_symbolp(t3);
if(C_truep(C_i_not(t5))){
t6=t4;{
C_word *av2=av;
av2[0]=t6;
av2[1]=((C_word*)t0)[2];
f_3823(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_3896,a[2]=((C_word*)t0)[2],a[3]=t4,a[4]=t3,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[3],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:137: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t6;
tp(2,av2);}}}
else{
switch(t2){
case C_fix(0):
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3907,a[2]=t3,a[3]=((C_word)li17),tmp=(C_word)a,a+=4,tmp);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}
case C_fix(1):
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3922,a[2]=t3,a[3]=((C_word)li18),tmp=(C_word)a,a+=4,tmp);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}
case C_fix(2):
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3941,a[2]=t3,a[3]=((C_word)li19),tmp=(C_word)a,a+=4,tmp);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}
case C_fix(3):
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3964,a[2]=t3,a[3]=((C_word)li20),tmp=(C_word)a,a+=4,tmp);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}
default:
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3985,a[2]=t2,a[3]=t3,a[4]=((C_word)li21),tmp=(C_word)a,a+=5,tmp);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3823(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_3823,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3826,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3853,a[2]=t1,a[3]=((C_word*)t0)[4],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[2]+1))){
t4=C_i_not(t1);
if(C_truep(t4)){
t5=t3;
f_3853(t5,t4);}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3874,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:143: ##sys#symbol-has-toplevel-binding? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t5;
av2[2]=t1;
tp(3,av2);}}}
else{
t4=t3;
f_3853(t4,C_SCHEME_FALSE);}}

/* k3824 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3826(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_3826,2,t0,t1);}
a=C_alloc(4);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word av2[2];
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3833,a[2]=((C_word*)t0)[4],a[3]=((C_word)li14),tmp=(C_word)a,a+=4,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3843,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:149: ##sys#symbol-has-toplevel-binding? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[19]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[19]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
tp(3,av2);}}}

/* f_3833 in k3824 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3833,c,av);}
/* eval.scm:148: ##sys#error */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[18];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k3841 in k3824 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3843,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3844,a[2]=((C_word*)t0)[2],a[3]=((C_word)li15),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3849,a[2]=((C_word*)t0)[2],a[3]=((C_word)li16),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* f_3844 in k3841 in k3824 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3844,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_slot(((C_word*)t0)[2],C_fix(0));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_3849 in k3841 in k3824 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3849,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fast_retrieve(((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3851 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_3853(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,1)))){
C_save_and_reclaim_args((void *)trf_3853,2,t0,t1);}
a=C_alloc(6);
if(C_truep(t1)){
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_a_i_cons(&a,2,t2,*((C_word*)lf[2]+1));
t4=C_mutate((C_word*)lf[2]+1 /* (set! ##sys#unbound-in-eval ...) */,t3);
t5=((C_word*)t0)[4];
f_3826(t5,t4);}
else{
t2=((C_word*)t0)[4];
f_3826(t2,C_SCHEME_UNDEFINED);}}

/* k3872 in k3821 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3874(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3874,c,av);}
t2=((C_word*)t0)[2];
f_3853(t2,C_i_not(t1));}

/* k3894 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3896,c,av);}
if(C_truep(C_i_assq(((C_word*)t0)[2],t1))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
f_3823(2,av2);}}
else{
if(C_truep(C_i_not(((C_word*)t0)[5]))){
/* eval.scm:139: ##sys#alias-global-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[6];
tp(5,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_3823(2,av2);}}}}

/* f_3907 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3907,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_slot(C_slot(t2,C_fix(0)),((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_3922 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3922(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3922,c,av);}
t3=C_slot(t2,C_fix(1));
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(C_slot(t3,C_fix(0)),((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f_3941 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3941,c,av);}
t3=C_slot(t2,C_fix(1));
t4=C_slot(t3,C_fix(1));
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_slot(C_slot(t4,C_fix(0)),((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* f_3964 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3964,c,av);}
t3=C_slot(t2,C_fix(1));
t4=C_slot(t3,C_fix(1));
t5=C_slot(t4,C_fix(1));
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_slot(C_slot(t5,C_fix(0)),((C_word*)t0)[2]);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* f_3985 in a3812 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_3985(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3985,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_slot(C_u_i_list_ref(t2,((C_word*)t0)[2]),((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4004(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_4004,c,av);}
a=C_alloc(20);
if(C_truep(t1)){
switch(((C_word*)t0)[2]){
case C_fix(-1):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4011,a[2]=((C_word)li23),tmp=(C_word)a,a+=3,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(0):
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4019,a[2]=((C_word)li24),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}
case C_fix(1):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4027,a[2]=((C_word)li25),tmp=(C_word)a,a+=3,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(2):
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4035,a[2]=((C_word)li26),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}
default:
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4037,a[2]=((C_word*)t0)[2],a[3]=((C_word)li27),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
if(C_truep(C_booleanp(((C_word*)t0)[2]))){
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4056,a[2]=((C_word)li28),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4058,a[2]=((C_word)li29),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}
else{
t2=C_charp(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4068,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
if(C_truep(t2)){
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t2;
f_4068(2,av2);}}
else{
t4=C_eofp(((C_word*)t0)[2]);
if(C_truep(t4)){
t5=t3;{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_4068(2,av2);}}
else{
t5=C_i_stringp(((C_word*)t0)[2]);
if(C_truep(t5)){
t6=t3;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
f_4068(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7235,a[2]=t3,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:186: chicken.blob#blob? */
t7=*((C_word*)lf[144]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}}}}}

/* f_4011 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4011(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4011,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4019 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4019(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4019,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4027 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4027,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4035 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4035,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(2);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4037 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4037,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4056 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4056,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4058 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4058(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4058,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4068(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_4068,c,av);}
a=C_alloc(16);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4069,a[2]=((C_word*)t0)[2],a[3]=((C_word)li30),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_i_pairp(((C_word*)t0)[2]);
if(C_truep(C_i_not(t2))){
/* eval.scm:191: ##sys#syntax-error/context */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=lf[22];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t3=C_slot(((C_word*)t0)[2],C_fix(0));
if(C_truep(C_i_symbolp(t3))){
t4=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4088,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
if(C_truep(((C_word*)t0)[7])){
t5=t4;
f_4088(t5,C_emit_syntax_trace_info(((C_word*)t0)[2],((C_word*)t0)[8],C_slot(*((C_word*)lf[11]+1),C_fix(14))));}
else{
t5=C_SCHEME_UNDEFINED;
t6=t4;
f_4088(t6,t5);}}
else{
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7205,a[2]=((C_word*)t0)[15],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[7])){
t5=t4;
f_7205(t5,C_emit_syntax_trace_info(((C_word*)t0)[2],((C_word*)t0)[8],C_slot(*((C_word*)lf[11]+1),C_fix(14))));}
else{
t5=C_SCHEME_UNDEFINED;
t6=t4;
f_7205(t6,t5);}}}}}

/* f_4069 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4069,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_4088(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,0,2)))){
C_save_and_reclaim_args((void *)trf_4088,2,t0,t1);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4091,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7202,a[2]=t2,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:194: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}

/* k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,7)))){
C_save_and_reclaim((void *)f_4091,c,av);}
a=C_alloc(16);
t2=C_eqp(t1,((C_word*)t0)[2]);
if(C_truep(C_i_not(t2))){
/* eval.scm:197: compile */
t3=((C_word*)((C_word*)t0)[3])[1];
f_3787(t3,((C_word*)t0)[4],t1,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);}
else{
t3=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_4106,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
/* eval.scm:198: rename */
t4=((C_word*)((C_word*)t0)[14])[1];
f_3634(t4,t3,C_slot(((C_word*)t0)[2],C_fix(0)));}}

/* k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,7)))){
C_save_and_reclaim((void *)f_4106,c,av);}
a=C_alloc(25);
t2=C_eqp(t1,lf[23]);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4115,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:204: chicken.syntax#strip-syntax */
t4=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_eqp(t1,lf[25]);
if(C_truep(t3)){
t4=C_i_cadr(((C_word*)t0)[3]);
t5=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4201,a[2]=t4,a[3]=((C_word)li39),tmp=(C_word)a,a+=4,tmp);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=C_eqp(t1,lf[26]);
if(C_truep(t4)){
/* eval.scm:220: compile */
t5=((C_word*)((C_word*)t0)[4])[1];
f_3787(t5,((C_word*)t0)[2],C_i_cadr(((C_word*)t0)[3]),((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t5=C_eqp(t1,lf[27]);
if(C_truep(t5)){
/* eval.scm:223: compile */
t6=((C_word*)((C_word*)t0)[4])[1];
f_3787(t6,((C_word*)t0)[2],C_i_cadr(((C_word*)t0)[3]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t6=C_eqp(t1,lf[28]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4235,a[2]=((C_word)li40),tmp=(C_word)a,a+=3,tmp);
t8=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}
else{
t7=C_eqp(t1,lf[29]);
if(C_truep(t7)){
t8=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4245,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:228: compile */
t9=((C_word*)((C_word*)t0)[4])[1];
f_3787(t9,t8,C_i_cadr(((C_word*)t0)[3]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t8=C_eqp(t1,lf[31]);
if(C_truep(t8)){
t9=C_slot(((C_word*)t0)[3],C_fix(1));
t10=C_i_length(t9);
switch(t10){
case C_fix(0):
/* eval.scm:239: compile */
t11=((C_word*)((C_word*)t0)[4])[1];
f_3787(t11,((C_word*)t0)[2],lf[32],((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);
case C_fix(1):
/* eval.scm:240: compile */
t11=((C_word*)((C_word*)t0)[4])[1];
f_3787(t11,((C_word*)t0)[2],C_slot(t9,C_fix(0)),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);
case C_fix(2):
t11=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4333,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],a[4]=t9,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:241: compile */
t12=((C_word*)((C_word*)t0)[4])[1];
f_3787(t12,t11,C_slot(t9,C_fix(0)),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);
default:
t11=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4355,a[2]=((C_word*)t0)[2],a[3]=t9,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:245: compile */
t12=((C_word*)((C_word*)t0)[4])[1];
f_3787(t12,t11,C_slot(t9,C_fix(0)),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);}}
else{
t9=C_eqp(t1,lf[33]);
if(C_truep(t9)){
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4407,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)t0)[9])){
/* eval.scm:253: compile */
t11=((C_word*)((C_word*)t0)[4])[1];
f_3787(t11,((C_word*)t0)[2],lf[34],((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
/* eval.scm:252: ##sys#error */
t11=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t11;
av2[1]=t10;
av2[2]=lf[35];
av2[3]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t11+1)))(4,av2);}}}
else{
t10=C_eqp(t1,lf[36]);
if(C_truep(t10)){
t11=C_i_cadr(((C_word*)t0)[3]);
t12=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4431,a[2]=((C_word*)t0)[10],a[3]=t11,a[4]=((C_word*)t0)[5],a[5]=((C_word)li44),tmp=(C_word)a,a+=6,tmp);
t13=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4437,a[2]=((C_word*)t0)[11],a[3]=((C_word*)t0)[12],a[4]=t11,a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[7],a[10]=((C_word)li49),tmp=(C_word)a,a+=11,tmp);
/* eval.scm:258: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t12;
av2[3]=t13;
C_call_with_values(4,av2);}}
else{
t11=C_eqp(t1,lf[41]);
if(C_truep(t11)){
t12=C_i_cadr(((C_word*)t0)[3]);
t13=C_i_length(t12);
t14=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t15=t14;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=((C_word*)t16)[1];
t18=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4546,a[2]=((C_word*)t0)[5],a[3]=t13,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=t12,a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[3],tmp=(C_word)a,a+=12,tmp);
t19=C_SCHEME_UNDEFINED;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=C_set_block_item(t20,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4985,a[2]=t16,a[3]=t20,a[4]=t17,a[5]=((C_word)li62),tmp=(C_word)a,a+=6,tmp));
t22=((C_word*)t20)[1];
f_4985(t22,t18,t12);}
else{
t12=C_eqp(t1,lf[50]);
if(C_truep(t12)){
t13=C_i_cadr(((C_word*)t0)[3]);
t14=C_u_i_cdr(((C_word*)t0)[3]);
t15=C_u_i_cdr(t14);
t16=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t17=t16;
t18=(*a=C_VECTOR_TYPE|1,a[1]=t17,tmp=(C_word)a,a+=2,tmp);
t19=((C_word*)t18)[1];
t20=C_i_check_list_2(t13,lf[42]);
t21=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5058,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=t15,a[9]=t13,tmp=(C_word)a,a+=10,tmp);
t22=C_SCHEME_UNDEFINED;
t23=(*a=C_VECTOR_TYPE|1,a[1]=t22,tmp=(C_word)a,a+=2,tmp);
t24=C_set_block_item(t23,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5130,a[2]=t18,a[3]=t23,a[4]=t19,a[5]=((C_word)li64),tmp=(C_word)a,a+=6,tmp));
t25=((C_word*)t23)[1];
f_5130(t25,t21,t13);}
else{
t13=C_eqp(t1,lf[53]);
if(C_truep(t13)){
t14=C_i_cadr(((C_word*)t0)[3]);
t15=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t16=t15;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=((C_word*)t17)[1];
t19=C_i_check_list_2(t14,lf[42]);
t20=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5180,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t14,tmp=(C_word)a,a+=10,tmp);
t21=C_SCHEME_UNDEFINED;
t22=(*a=C_VECTOR_TYPE|1,a[1]=t21,tmp=(C_word)a,a+=2,tmp);
t23=C_set_block_item(t22,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5436,a[2]=t17,a[3]=t22,a[4]=t18,a[5]=((C_word)li69),tmp=(C_word)a,a+=6,tmp));
t24=((C_word*)t22)[1];
f_5436(t24,t20,t14);}
else{
t14=C_eqp(t1,lf[55]);
if(C_truep(t14)){
t15=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5477,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[13],a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[2],tmp=(C_word)a,a+=10,tmp);
t16=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5920,a[2]=t15,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:361: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t16;
tp(2,av2);}}
else{
t15=C_eqp(t1,lf[67]);
if(C_truep(t15)){
t16=*((C_word*)lf[9]+1);
t17=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5929,a[2]=t16,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
t18=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t19=t18;
t20=(*a=C_VECTOR_TYPE|1,a[1]=t19,tmp=(C_word)a,a+=2,tmp);
t21=((C_word*)t20)[1];
t22=C_i_cadr(((C_word*)t0)[3]);
t23=C_i_check_list_2(t22,lf[42]);
t24=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6021,a[2]=t17,tmp=(C_word)a,a+=3,tmp);
t25=C_SCHEME_UNDEFINED;
t26=(*a=C_VECTOR_TYPE|1,a[1]=t25,tmp=(C_word)a,a+=2,tmp);
t27=C_set_block_item(t26,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6027,a[2]=t20,a[3]=t26,a[4]=t21,a[5]=((C_word)li105),tmp=(C_word)a,a+=6,tmp));
t28=((C_word*)t26)[1];
f_6027(t28,t24,t22);}
else{
t16=C_eqp(t1,lf[71]);
if(C_truep(t16)){
t17=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t18=t17;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=((C_word*)t19)[1];
t21=C_i_cadr(((C_word*)t0)[3]);
t22=C_i_check_list_2(t21,lf[42]);
t23=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6104,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
t24=C_SCHEME_UNDEFINED;
t25=(*a=C_VECTOR_TYPE|1,a[1]=t24,tmp=(C_word)a,a+=2,tmp);
t26=C_set_block_item(t25,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6199,a[2]=t19,a[3]=t25,a[4]=t20,a[5]=((C_word)li111),tmp=(C_word)a,a+=6,tmp));
t27=((C_word*)t25)[1];
f_6199(t27,t23,t21);}
else{
t17=C_eqp(t1,lf[73]);
if(C_truep(t17)){
t18=C_i_cadr(((C_word*)t0)[3]);
t19=C_i_caddr(((C_word*)t0)[3]);
t20=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6246,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=t19,a[8]=((C_word*)t0)[11],a[9]=t18,a[10]=((C_word*)t0)[12],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:493: rename */
t21=((C_word*)((C_word*)t0)[14])[1];
f_3634(t21,t20,t18);}
else{
t18=C_eqp(t1,lf[79]);
if(C_truep(t18)){
/* eval.scm:506: compile */
t19=((C_word*)((C_word*)t0)[4])[1];
f_3787(t19,((C_word*)t0)[2],lf[80],((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t19=C_eqp(t1,lf[81]);
if(C_truep(t19)){
t20=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6309,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
t21=C_i_cddr(((C_word*)t0)[3]);
t22=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6317,a[2]=t20,a[3]=t21,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:510: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t22;
tp(2,av2);}}
else{
t20=C_eqp(t1,lf[82]);
if(C_truep(t20)){
t21=C_i_cadr(((C_word*)t0)[3]);
t22=C_i_caddr(((C_word*)t0)[3]);
t23=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6336,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[3],a[8]=((C_word)li112),tmp=(C_word)a,a+=9,tmp);
/* eval.scm:514: ##sys#include-forms-from-file */
t24=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t24;
av2[1]=((C_word*)t0)[2];
av2[2]=t21;
av2[3]=t22;
av2[4]=t23;
((C_proc)(void*)(*((C_word*)t24+1)))(5,av2);}}
else{
t21=C_eqp(t1,lf[84]);
if(C_truep(t21)){
t22=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t23=t22;
t24=(*a=C_VECTOR_TYPE|1,a[1]=t23,tmp=(C_word)a,a+=2,tmp);
t25=((C_word*)t24)[1];
t26=C_i_cadr(((C_word*)t0)[3]);
t27=C_i_check_list_2(t26,lf[42]);
t28=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6399,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[2],tmp=(C_word)a,a+=9,tmp);
t29=C_SCHEME_UNDEFINED;
t30=(*a=C_VECTOR_TYPE|1,a[1]=t29,tmp=(C_word)a,a+=2,tmp);
t31=C_set_block_item(t30,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6415,a[2]=t24,a[3]=t30,a[4]=t25,a[5]=((C_word)li114),tmp=(C_word)a,a+=6,tmp));
t32=((C_word*)t30)[1];
f_6415(t32,t28,t26);}
else{
t22=C_eqp(t1,lf[88]);
if(C_truep(t22)){
t23=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6456,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:536: chicken.syntax#strip-syntax */
t24=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t24;
av2[1]=t23;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t24+1)))(3,av2);}}
else{
t23=C_eqp(t1,lf[102]);
if(C_truep(t23)){
t24=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6780,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:588: rename */
t25=((C_word*)((C_word*)t0)[14])[1];
f_3634(t25,t24,lf[65]);}
else{
t24=C_eqp(t1,lf[103]);
if(C_truep(t24)){
t25=C_i_cadr(((C_word*)t0)[3]);
t26=C_a_i_list(&a,2,lf[23],t25);
t27=C_a_i_list(&a,2,lf[93],t26);
/* eval.scm:591: compile */
t28=((C_word*)((C_word*)t0)[4])[1];
f_3787(t28,((C_word*)t0)[2],t27,((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
t25=C_eqp(t1,lf[104]);
if(C_truep(t25)){
t26=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6814,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:594: chicken.load#load-extension */
t27=*((C_word*)lf[106]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t27;
av2[1]=t26;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
av2[3]=C_SCHEME_END_OF_LIST;
av2[4]=lf[107];
((C_proc)(void*)(*((C_word*)t27+1)))(5,av2);}}
else{
t26=C_eqp(t1,lf[108]);
if(C_truep(t26)){
t27=C_i_cadr(((C_word*)t0)[3]);
t28=C_u_i_cdr(((C_word*)t0)[3]);
t29=C_u_i_cdr(t28);
t30=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6837,a[2]=t27,a[3]=t29,a[4]=((C_word)li125),tmp=(C_word)a,a+=5,tmp);
t31=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6843,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word)li126),tmp=(C_word)a,a+=7,tmp);
/* eval.scm:600: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t30;
av2[3]=t31;
C_call_with_values(4,av2);}}
else{
t27=C_eqp(t1,lf[110]);
t28=(C_truep(t27)?t27:C_eqp(t1,lf[111]));
if(C_truep(t28)){
t29=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6859,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:604: ##sys#eval/meta */
t30=*((C_word*)lf[70]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t30;
av2[1]=t29;
av2[2]=C_i_cadr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t30+1)))(3,av2);}}
else{
t29=C_eqp(t1,lf[113]);
if(C_truep(t29)){
/* eval.scm:608: compile */
t30=((C_word*)((C_word*)t0)[4])[1];
f_3787(t30,((C_word*)t0)[2],C_i_cadr(((C_word*)t0)[3]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);}
else{
t30=C_eqp(t1,lf[114]);
t31=(C_truep(t30)?t30:C_eqp(t1,lf[115]));
if(C_truep(t31)){
/* eval.scm:611: compile */
t32=((C_word*)((C_word*)t0)[4])[1];
f_3787(t32,((C_word*)t0)[2],lf[116],((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);}
else{
t32=C_eqp(t1,lf[117]);
if(C_truep(t32)){
t33=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6900,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:614: ##sys#notice */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=t33;
av2[2]=lf[119];
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}
else{
t33=C_eqp(t1,lf[120]);
t34=(C_truep(t33)?t33:C_eqp(t1,lf[121]));
if(C_truep(t34)){
t35=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6923,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:618: rename */
t36=((C_word*)((C_word*)t0)[14])[1];
f_3634(t36,t35,lf[122]);}
else{
t35=C_eqp(t1,lf[123]);
t36=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6936,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[15],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[9],tmp=(C_word)a,a+=12,tmp);
if(C_truep(t35)){
t37=t36;
f_6936(t37,t35);}
else{
t37=C_eqp(t1,lf[132]);
if(C_truep(t37)){
t38=t36;
f_6936(t38,t37);}
else{
t38=C_eqp(t1,lf[133]);
if(C_truep(t38)){
t39=t36;
f_6936(t39,t38);}
else{
t39=C_eqp(t1,lf[134]);
if(C_truep(t39)){
t40=t36;
f_6936(t40,t39);}
else{
t40=C_eqp(t1,lf[135]);
if(C_truep(t40)){
t41=t36;
f_6936(t41,t40);}
else{
t41=C_eqp(t1,lf[136]);
if(C_truep(t41)){
t42=t36;
f_6936(t42,t41);}
else{
t42=C_eqp(t1,lf[137]);
if(C_truep(t42)){
t43=t36;
f_6936(t43,t42);}
else{
t43=C_eqp(t1,lf[138]);
if(C_truep(t43)){
t44=t36;
f_6936(t44,t43);}
else{
t44=C_eqp(t1,lf[139]);
if(C_truep(t44)){
t45=t36;
f_6936(t45,t44);}
else{
t45=C_eqp(t1,lf[140]);
t46=t36;
f_6936(t46,(C_truep(t45)?t45:C_eqp(t1,lf[141])));}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}

/* k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4115,c,av);}
a=C_alloc(4);
switch(t1){
case C_fix(-1):
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4122,a[2]=((C_word)li31),tmp=(C_word)a,a+=3,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(0):
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4130,a[2]=((C_word)li32),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}
case C_fix(1):
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4138,a[2]=((C_word)li33),tmp=(C_word)a,a+=3,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(2):
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4146,a[2]=((C_word)li34),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}
case C_SCHEME_TRUE:
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4154,a[2]=((C_word)li35),tmp=(C_word)a,a+=3,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_SCHEME_FALSE:
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4162,a[2]=((C_word)li36),tmp=(C_word)a,a+=3,tmp);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}
default:
t2=C_eqp(t1,C_SCHEME_END_OF_LIST);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4170,a[2]=((C_word)li37),tmp=(C_word)a,a+=3,tmp):(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4172,a[2]=t1,a[3]=((C_word)li38),tmp=(C_word)a,a+=4,tmp));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* f_4122 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4122,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(-1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4130 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4130,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(0);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4138 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4138(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4138,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4146 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4146(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4146,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_fix(2);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4154 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4154(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4154,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4162 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4162,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4170 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4170(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4170,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4172 in k4113 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4172,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4201 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4201,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4235 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4235,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4243 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,7)))){
C_save_and_reclaim((void *)f_4245,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4248,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:229: compile */
t3=((C_word*)((C_word*)t0)[4])[1];
f_3787(t3,t2,C_i_caddr(((C_word*)t0)[3]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4246 in k4243 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4248(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,7)))){
C_save_and_reclaim((void *)f_4248,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4251,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_i_cdddr(((C_word*)t0)[4]);
if(C_truep(C_i_pairp(t3))){
/* eval.scm:231: compile */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3787(t4,t2,C_i_cadddr(((C_word*)t0)[4]),((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}
else{
/* eval.scm:232: compile */
t4=((C_word*)((C_word*)t0)[5])[1];
f_3787(t4,t2,lf[30],((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}}

/* k4249 in k4246 in k4243 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4251(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4251,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4252,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word)li41),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4252 in k4249 in k4246 in k4243 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4252,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4259,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4257 */
static void C_ccall f_4259(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4259,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k4331 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4333(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,7)))){
C_save_and_reclaim((void *)f_4333,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4336,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:242: compile */
t3=((C_word*)((C_word*)t0)[3])[1];
f_3787(t3,t2,C_i_cadr(((C_word*)t0)[4]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8]);}

/* k4334 in k4331 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4336(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4336,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4337,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word)li42),tmp=(C_word)a,a+=5,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4337 in k4334 in k4331 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4337(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4337,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4341,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4339 */
static void C_ccall f_4341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4341,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4353 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4355(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,7)))){
C_save_and_reclaim((void *)f_4355,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4358,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:246: compile */
t3=((C_word*)((C_word*)t0)[4])[1];
f_3787(t3,t2,C_i_cadr(((C_word*)t0)[3]),((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8]);}

/* k4356 in k4353 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,7)))){
C_save_and_reclaim((void *)f_4358,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4361,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=C_slot(((C_word*)t0)[4],C_fix(1));
t4=C_slot(t3,C_fix(1));
t5=C_a_i_cons(&a,2,lf[31],t4);
/* eval.scm:247: compile */
t6=((C_word*)((C_word*)t0)[5])[1];
f_3787(t6,t2,t5,((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9]);}

/* k4359 in k4356 in k4353 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4361(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4361,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4362,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li43),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4362 in k4359 in k4356 in k4353 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4362,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4366,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4364 */
static void C_ccall f_4366(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4366,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4369,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4367 in k4364 */
static void C_ccall f_4369(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4369,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k4405 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4407,c,av);}
/* eval.scm:253: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],lf[34],((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* a4430 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4431,c,av);}
/* eval.scm:258: lookup */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3668(t2,t1,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4437(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,7)))){
C_save_and_reclaim((void *)f_4437,c,av);}
a=C_alloc(9);
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4441,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=t3,a[8]=((C_word*)t0)[5],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:259: compile */
t5=((C_word*)((C_word*)t0)[6])[1];
f_3787(t5,t4,C_i_caddr(((C_word*)t0)[7]),((C_word*)t0)[8],((C_word*)t0)[4],((C_word*)t0)[9],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4441(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_4441,c,av);}
a=C_alloc(13);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4450,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=t1,a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(*((C_word*)lf[38]+1))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4492,a[2]=((C_word*)t0)[6],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:262: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4450(2,av2);}}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(C_i_zerop(((C_word*)t0)[2]))?(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4497,a[2]=((C_word*)t0)[7],a[3]=t1,a[4]=((C_word)li47),tmp=(C_word)a,a+=5,tmp):(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4510,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[7],a[4]=t1,a[5]=((C_word)li48),tmp=(C_word)a,a+=6,tmp));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k4448 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4450(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4450,c,av);}
a=C_alloc(5);
if(C_truep(((C_word*)t0)[2])){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4454,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word)li45),tmp=(C_word)a,a+=5,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4461,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:268: ##sys#alias-global-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[20]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[20]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[8];
tp(5,av2);}}}

/* f_4454 in k4448 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4454,c,av);}
/* eval.scm:267: ##sys#error */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=lf[0];
av2[3]=lf[37];
av2[4]=((C_word*)t0)[2];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}

/* k4459 in k4448 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4461,c,av);}
a=C_alloc(5);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4462,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word)li46),tmp=(C_word)a,a+=5,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4462 in k4459 in k4448 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_4462,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4466,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4464 */
static void C_ccall f_4466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4466,c,av);}
t2=C_i_persist_symbol(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_setslot(((C_word*)t0)[2],C_fix(0),t1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4490 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4492,c,av);}
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(t2)){
t3=C_i_cdr(t2);
if(C_truep(C_i_symbolp(t3))){
/* eval.scm:264: ##sys#notice */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[39]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[39]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=lf[40];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}
else{
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_4450(2,av2);}}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_4450(2,av2);}}}

/* f_4497 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4497(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4497,c,av);}
a=C_alloc(5);
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4509,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k4507 */
static void C_ccall f_4509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4509,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_setslot(((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4510 in k4439 in a4436 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4510,c,av);}
a=C_alloc(5);
t3=C_u_i_list_ref(t2,((C_word*)t0)[2]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4519,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k4517 */
static void C_ccall f_4519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4519,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_setslot(((C_word*)t0)[3],((C_word*)t0)[4],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4546(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_4546,c,av);}
a=C_alloc(26);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(t1,lf[42]);
t7=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4555,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4951,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li61),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_4951(t11,t7,t1);}

/* k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,2)))){
C_save_and_reclaim((void *)f_4555,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_4561,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t2,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4949,a[2]=t3,a[3]=((C_word*)t0)[6],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:285: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}

/* k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,4)))){
C_save_and_reclaim((void *)f_4561,c,av);}
a=C_alloc(33);
t2=*((C_word*)lf[9]+1);
t3=t1;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_TRUE;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4902,a[2]=t4,a[3]=t6,a[4]=t2,a[5]=((C_word)li58),tmp=(C_word)a,a+=6,tmp);
t9=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4919,a[2]=((C_word*)t0)[10],a[3]=((C_word*)t0)[8],a[4]=((C_word*)t0)[11],a[5]=((C_word*)t0)[12],a[6]=((C_word*)t0)[13],a[7]=((C_word)li59),tmp=(C_word)a,a+=8,tmp);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4937,a[2]=t4,a[3]=t2,a[4]=((C_word)li60),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:286: ##sys#dynamic-wind */
t11=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t11;
av2[1]=t7;
av2[2]=t8;
av2[3]=t9;
av2[4]=t10;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}

/* k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_4564,c,av);}
a=C_alloc(26);
switch(((C_word*)t0)[2]){
case C_fix(1):
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4573,a[2]=t1,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4594,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:291: scheme#cadar */
t4=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}
case C_fix(2):
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4607,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4643,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:294: scheme#cadar */
t4=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}
case C_fix(3):
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4656,a[2]=((C_word*)t0)[9],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4710,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:298: scheme#cadar */
t4=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}
case C_fix(4):
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4723,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4792,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:306: scheme#cadar */
t4=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}
default:
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4800,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word)li54),tmp=(C_word)a,a+=7,tmp);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4813,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4860,a[2]=t4,a[3]=t9,a[4]=t6,a[5]=t5,a[6]=((C_word)li57),tmp=(C_word)a,a+=7,tmp));
t11=((C_word*)t9)[1];
f_4860(t11,t7,((C_word*)t0)[9]);}}

/* k4571 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4573,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4574,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word)li50),tmp=(C_word)a,a+=5,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_4574 in k4571 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4574,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4590,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4588 */
static void C_ccall f_4590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4590,c,av);}
a=C_alloc(5);
t2=C_a_i_vector1(&a,1,t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[2]);
t4=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[4];
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4592 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4594,c,av);}
/* eval.scm:291: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_car(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4605 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_4607,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4610,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4635,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:295: scheme#cadadr */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4608 in k4605 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4610,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4611,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word)li51),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4611 in k4608 in k4605 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4611(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4611,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4627,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t4=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4625 */
static void C_ccall f_4627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4627,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4631,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4629 in k4625 */
static void C_ccall f_4631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_4631,c,av);}
a=C_alloc(6);
t2=C_a_i_vector2(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[3]);
t4=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4633 in k4605 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4635,c,av);}
/* eval.scm:295: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_cadr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4641 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4643,c,av);}
/* eval.scm:294: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_car(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_4656,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4659,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4702,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:299: scheme#cadadr */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4657 in k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_4659,c,av);}
a=C_alloc(14);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4665,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4694,a[2]=((C_word*)t0)[6],a[3]=t3,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:301: scheme#cadar */
t5=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k4663 in k4657 in k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4665,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4666,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word)li52),tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_4666 in k4663 in k4657 in k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4666,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4682,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
t4=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4680 */
static void C_ccall f_4682(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4682,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4686,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4684 in k4680 */
static void C_ccall f_4686(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4686,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4690,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4688 in k4684 in k4680 */
static void C_ccall f_4690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4690,c,av);}
a=C_alloc(7);
t2=C_a_i_vector3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[4]);
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[6];
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4692 in k4657 in k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4694,c,av);}
/* eval.scm:301: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_caddr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4700 in k4654 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4702,c,av);}
/* eval.scm:299: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_cadr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4708 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4710(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4710,c,av);}
/* eval.scm:298: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_car(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_4723,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4726,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4784,a[2]=((C_word*)t0)[5],a[3]=t2,a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:307: scheme#cadadr */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,2)))){
C_save_and_reclaim((void *)f_4726,c,av);}
a=C_alloc(20);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4732,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t2,tmp=(C_word)a,a+=12,tmp);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4776,a[2]=((C_word*)t0)[6],a[3]=t3,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:309: scheme#cadar */
t5=*((C_word*)lf[43]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k4730 in k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_4732,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4735,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4768,a[2]=((C_word*)t0)[6],a[3]=t2,a[4]=((C_word*)t0)[7],a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:310: scheme#cadadr */
t4=*((C_word*)lf[44]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[11];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4733 in k4730 in k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4735,c,av);}
a=C_alloc(8);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4736,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li53),tmp=(C_word)a,a+=8,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4736 in k4733 in k4730 in k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_4736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4736,c,av);}
a=C_alloc(8);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4752,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4750 */
static void C_ccall f_4752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4752,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4756,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4754 in k4750 */
static void C_ccall f_4756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4756,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4760,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4758 in k4754 in k4750 */
static void C_ccall f_4760(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4760,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4764,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4762 in k4758 in k4754 in k4750 */
static void C_ccall f_4764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_4764,c,av);}
a=C_alloc(8);
t2=C_a_i_vector4(&a,4,((C_word*)t0)[2],((C_word*)t0)[3],((C_word*)t0)[4],t1);
t3=C_a_i_cons(&a,2,t2,((C_word*)t0)[5]);
t4=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[7];
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k4766 in k4730 in k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4768,c,av);}
/* eval.scm:310: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_cadddr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4774 in k4724 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4776,c,av);}
/* eval.scm:309: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_caddr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4782 in k4721 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4784,c,av);}
/* eval.scm:307: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_cadr(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k4790 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4792,c,av);}
/* eval.scm:306: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_i_car(((C_word*)t0)[5]),((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* g518 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_4800(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_4800,3,t0,t1,t2);}
/* eval.scm:320: compile */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3787(t3,t1,C_i_cadr(t2),((C_word*)t0)[3],C_u_i_car(t2),((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k4811 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4813(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4813,c,av);}
a=C_alloc(6);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4814,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,a[5]=((C_word)li56),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_4814 in k4811 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4814,c,av);}
a=C_alloc(7);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4818,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
/* eval.scm:322: ##sys#make-vector */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[45]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[45]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
tp(3,av2);}}

/* k4816 */
static void C_ccall f_4818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_4818,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4821,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4830,a[2]=((C_word*)t0)[5],a[3]=t1,a[4]=t4,a[5]=((C_word*)t0)[2],a[6]=((C_word)li55),tmp=(C_word)a,a+=7,tmp));
t6=((C_word*)t4)[1];
f_4830(t6,t2,C_fix(0),((C_word*)t0)[6]);}

/* k4819 in k4816 */
static void C_ccall f_4821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4821,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[5];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* doloop539 in k4816 */
static void C_fcall f_4830(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_4830,4,t0,t1,t2,t3);}
a=C_alloc(7);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t4=C_SCHEME_UNDEFINED;
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4855,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
t5=C_slot(t3,C_fix(0));{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k4853 in doloop539 in k4816 */
static void C_ccall f_4855(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4855,c,av);}
t2=C_i_setslot(((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=((C_word*)((C_word*)t0)[4])[1];
f_4830(t3,((C_word*)t0)[5],C_fixnum_plus(((C_word*)t0)[3],C_fix(1)),C_slot(((C_word*)t0)[6],C_fix(1)));}

/* map-loop512 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_4860(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4860,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4885,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:320: g518 */
t4=((C_word*)t0)[4];
f_4800(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4883 in map-loop512 in k4562 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4885,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4860(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* a4901 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4902,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4906,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* eval.scm:286: ##sys#current-environment473 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_4906(2,av2);}}}

/* k4904 in a4901 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4906(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4906,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4910,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:286: ##sys#current-environment473 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k4908 in k4904 in a4901 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4910(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4910,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4913,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:286: ##sys#current-environment473 */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k4911 in k4908 in k4904 in a4901 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4913(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4913,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a4918 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4919(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_4919,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4927,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=C_i_cddr(((C_word*)t0)[6]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4935,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:288: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}

/* k4925 in a4918 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4927(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_4927,c,av);}
a=C_alloc(12);
/* eval.scm:287: compile-to-closure */
f_3572(((C_word*)t0)[2],t1,((C_word*)t0)[3],C_a_i_list(&a,4,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE));}

/* k4933 in a4918 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4935,c,av);}
/* eval.scm:288: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}

/* a4936 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4937(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4937,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4941,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:286: ##sys#current-environment473 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k4939 in a4936 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4941(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4941,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4944,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:286: ##sys#current-environment473 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k4942 in k4939 in a4936 in k4559 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4944,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4947 in k4553 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4949(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4949,c,av);}
/* eval.scm:285: ##sys#extend-se */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[48]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[48]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}

/* map-loop445 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_4951(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_4951,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4976,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:283: g451 */
t4=*((C_word*)lf[49]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4974 in map-loop445 in k4544 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_4976(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_4976,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_4951(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop417 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_4985(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_4985,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5056 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5058(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_5058,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5062,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5082,a[2]=((C_word*)t0)[8],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5096,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li63),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_5096(t11,t7,((C_word*)t0)[9]);}

/* k5060 in k5056 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5062(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_5062,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[41],t2);
/* eval.scm:332: compile */
t4=((C_word*)((C_word*)t0)[3])[1];
f_3787(t4,((C_word*)t0)[4],t3,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8],C_SCHEME_FALSE);}

/* k5080 in k5056 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5082,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[41],t2);
t4=C_a_i_list(&a,1,t3);
/* eval.scm:333: ##sys#append */
t5=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* map-loop580 in k5056 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5096(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_5096,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_i_cadr(t3);
t6=C_a_i_list(&a,3,lf[36],t4,t5);
t7=C_a_i_cons(&a,2,t6,C_SCHEME_END_OF_LIST);
t8=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t7);
t9=C_mutate(((C_word *)((C_word*)t0)[2])+1,t7);
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop553 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5130(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_5130,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_list2(&a,2,t4,lf[52]);
t6=C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST);
t7=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t6);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t6);
t10=t1;
t11=C_slot(t2,C_fix(1));
t1=t10;
t2=t11;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_5180,c,av);}
a=C_alloc(24);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(t1,lf[42]);
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5189,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5402,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li68),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_5402(t11,t7,t1);}

/* k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5189(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_5189,c,av);}
a=C_alloc(25);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5215,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t2,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5368,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li67),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_5368(t11,t7,((C_word*)t0)[10]);}

/* k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5215(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,4)))){
C_save_and_reclaim((void *)f_5215,c,av);}
a=C_alloc(25);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=C_i_check_list_2(((C_word*)t0)[2],lf[42]);
t7=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5242,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[2],tmp=(C_word)a,a+=12,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5320,a[2]=t4,a[3]=t9,a[4]=t5,a[5]=((C_word)li66),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_5320(t11,t7,((C_word*)t0)[2],((C_word*)t0)[11]);}

/* k5240 in k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_5242,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5246,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5258,a[2]=((C_word*)t0)[9],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5272,a[2]=t5,a[3]=t9,a[4]=t6,a[5]=((C_word)li65),tmp=(C_word)a,a+=6,tmp));
t11=((C_word*)t9)[1];
f_5272(t11,t7,((C_word*)t0)[10],((C_word*)t0)[11]);}

/* k5244 in k5240 in k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,7)))){
C_save_and_reclaim((void *)f_5246,c,av);}
a=C_alloc(15);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,lf[41],t2);
t4=C_a_i_list(&a,3,lf[41],((C_word*)t0)[3],t3);
/* eval.scm:348: compile */
t5=((C_word*)((C_word*)t0)[4])[1];
f_3787(t5,((C_word*)t0)[5],t4,((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[9],C_SCHEME_FALSE);}

/* k5256 in k5240 in k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_5258,c,av);}
a=C_alloc(9);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[41],t2);
t4=C_a_i_list(&a,1,t3);
/* eval.scm:349: ##sys#append */
t5=*((C_word*)lf[51]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* map-loop729 in k5240 in k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5272(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_5272,4,t0,t1,t2,t3);}
a=C_alloc(12);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_a_i_list(&a,3,lf[36],t6,t7);
t9=C_a_i_cons(&a,2,t8,C_SCHEME_END_OF_LIST);
t10=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t9);
t11=C_mutate(((C_word *)((C_word*)t0)[2])+1,t9);
t13=t1;
t14=C_slot(t2,C_fix(1));
t15=C_slot(t3,C_fix(1));
t1=t13;
t2=t14;
t3=t15;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop696 in k5213 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5320(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_5320,4,t0,t1,t2,t3);}
a=C_alloc(9);
t4=C_i_pairp(t2);
t5=(C_truep(t4)?C_i_pairp(t3):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_slot(t2,C_fix(0));
t7=C_slot(t3,C_fix(0));
t8=C_i_cadr(t7);
t9=C_a_i_list2(&a,2,t6,t8);
t10=C_a_i_cons(&a,2,t9,C_SCHEME_END_OF_LIST);
t11=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t10);
t12=C_mutate(((C_word *)((C_word*)t0)[2])+1,t10);
t14=t1;
t15=C_slot(t2,C_fix(1));
t16=C_slot(t3,C_fix(1));
t1=t14;
t2=t15;
t3=t16;
goto loop;}
else{
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}}

/* map-loop669 in k5187 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5368(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_5368,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_list2(&a,2,t4,lf[54]);
t6=C_a_i_cons(&a,2,t5,C_SCHEME_END_OF_LIST);
t7=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t6);
t8=C_mutate(((C_word *)((C_word*)t0)[2])+1,t6);
t10=t1;
t11=C_slot(t2,C_fix(1));
t1=t10;
t2=t11;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop639 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5402(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5402,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5427,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:346: g645 */
t4=*((C_word*)lf[49]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5425 in map-loop639 in k5178 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5427,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5402(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop612 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5436(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_5436,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_5477,c,av);}
a=C_alloc(24);
t2=C_i_cadr(((C_word*)t0)[2]);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_u_i_cdr(((C_word*)t0)[2]);
t6=C_u_i_cdr(t5);
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=(C_truep(((C_word*)t0)[3])?C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t4)[1]):C_a_i_cons(&a,2,lf[56],((C_word*)t4)[1]));
t9=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_5488,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=t8,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t7,a[10]=((C_word*)t0)[9],a[11]=t4,tmp=(C_word)a,a+=12,tmp);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5894,a[2]=t4,a[3]=t7,a[4]=t9,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:365: ##sys#extended-lambda-list? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[63]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[63]+1);
av2[1]=t10;
av2[2]=((C_word*)t4)[1];
tp(3,av2);}}

/* k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,5)))){
C_save_and_reclaim((void *)f_5488,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5493,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word)li99),tmp=(C_word)a,a+=11,tmp);
/* eval.scm:370: ##sys#decompose-lambda-list */
t3=*((C_word*)lf[60]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[10];
av2[2]=((C_word*)((C_word*)t0)[11])[1];
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_5493,c,av);}
a=C_alloc(27);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=C_i_check_list_2(t2,lf[42]);
t10=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5503,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t1,a[5]=t4,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],a[12]=((C_word*)t0)[9],a[13]=t2,tmp=(C_word)a,a+=14,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5859,a[2]=t7,a[3]=t12,a[4]=t8,a[5]=((C_word)li98),tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_5859(t14,t10,t2);}

/* k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5503(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_5503,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5506,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5857,a[2]=t2,a[3]=((C_word*)t0)[13],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:374: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}

/* k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_5506,c,av);}
a=C_alloc(35);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],((C_word*)t0)[3]);
t3=*((C_word*)lf[9]+1);
t4=t1;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_TRUE;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5512,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5814,a[2]=t5,a[3]=t7,a[4]=t3,a[5]=((C_word)li95),tmp=(C_word)a,a+=6,tmp);
t10=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5831,a[2]=((C_word*)t0)[9],a[3]=t2,a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[12],a[7]=((C_word*)t0)[13],a[8]=t1,a[9]=((C_word)li96),tmp=(C_word)a,a+=10,tmp);
t11=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5845,a[2]=t5,a[3]=t3,a[4]=((C_word)li97),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:377: ##sys#dynamic-wind */
t12=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t12;
av2[1]=t8;
av2[2]=t9;
av2[3]=t10;
av2[4]=t11;
((C_proc)(void*)(*((C_word*)t12+1)))(5,av2);}}

/* k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5512,c,av);}
a=C_alloc(7);
switch(((C_word*)t0)[2]){
case C_fix(0):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(((C_word*)t0)[4])?(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5522,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li71),tmp=(C_word)a,a+=6,tmp):(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5541,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li73),tmp=(C_word)a,a+=6,tmp));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(1):
if(C_truep(((C_word*)t0)[4])){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5565,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li75),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5584,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li77),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
case C_fix(2):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(((C_word*)t0)[4])?(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5612,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li79),tmp=(C_word)a,a+=6,tmp):(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5631,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li81),tmp=(C_word)a,a+=6,tmp));
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(3):
if(C_truep(((C_word*)t0)[4])){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5659,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li83),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5678,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li85),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
default:
t2=C_eqp(((C_word*)t0)[2],C_fix(4));
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep(t2)?(C_truep(((C_word*)t0)[4])?(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5706,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li87),tmp=(C_word)a,a+=6,tmp):(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5725,a[2]=t1,a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word)li89),tmp=(C_word)a,a+=6,tmp)):(C_truep(((C_word*)t0)[4])?(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5747,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li92),tmp=(C_word)a,a+=7,tmp):(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5770,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li94),tmp=(C_word)a,a+=7,tmp)));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* f_5522 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5522,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5528,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li70),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:384: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5527 */
static void C_ccall f_5528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_5528,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+5);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
t3=C_a_i_vector1(&a,1,t2);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[2]);
t5=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* f_5541 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5541,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5547,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li72),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:389: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5546 */
static void C_ccall f_5547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5547,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,C_SCHEME_FALSE,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* f_5565 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5565,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5571,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li74),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:394: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5570 */
static void C_ccall f_5571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +6,c,2)))){
C_save_and_reclaim((void*)f_5571,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+6);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
C_word t6;
t4=C_a_i_vector2(&a,2,t2,t3);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[2]);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t1;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* f_5584 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5584,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5590,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li76),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:399: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5589 */
static void C_ccall f_5590(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5590,c,av);}
a=C_alloc(5);
t3=C_a_i_vector1(&a,1,t2);
t4=C_a_i_cons(&a,2,t3,((C_word*)t0)[2]);
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t1;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* f_5612 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5612,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5618,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li78),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:405: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5617 */
static void C_ccall f_5618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +7,c,2)))){
C_save_and_reclaim((void*)f_5618,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+7);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
t5=C_a_i_vector3(&a,3,t2,t3,t4);
t6=C_a_i_cons(&a,2,t5,((C_word*)t0)[2]);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* f_5631 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5631,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5637,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li80),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:410: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5636 */
static void C_ccall f_5637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5637,c,av);}
a=C_alloc(6);
t4=C_a_i_vector2(&a,2,t2,t3);
t5=C_a_i_cons(&a,2,t4,((C_word*)t0)[2]);
t6=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t1;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* f_5659 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_5659,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5665,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li82),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:416: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5664 */
static void C_ccall f_5665(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c<5) C_bad_min_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-5)*C_SIZEOF_PAIR +8,c,2)))){
C_save_and_reclaim((void*)f_5665,c,av);}
a=C_alloc((c-5)*C_SIZEOF_PAIR+8);
t5=C_build_rest(&a,c,5,av);
C_word t6;
C_word t7;
C_word t8;
t6=C_a_i_vector4(&a,4,t2,t3,t4,t5);
t7=C_a_i_cons(&a,2,t6,((C_word*)t0)[2]);
t8=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t8;
av2[1]=t1;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* f_5678 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_5678,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5684,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li84),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:421: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5683 */
static void C_ccall f_5684(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5684,c,av);}
a=C_alloc(7);
t5=C_a_i_vector3(&a,3,t2,t3,t4);
t6=C_a_i_cons(&a,2,t5,((C_word*)t0)[2]);
t7=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t7;
av2[1]=t1;
av2[2]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* f_5706 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_5706,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5712,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li86),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:427: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5711 */
static void C_ccall f_5712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word *a;
if(c<6) C_bad_min_argc_2(c,6,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-6)*C_SIZEOF_PAIR +9,c,2)))){
C_save_and_reclaim((void*)f_5712,c,av);}
a=C_alloc((c-6)*C_SIZEOF_PAIR+9);
t6=C_build_rest(&a,c,6,av);
C_word t7;
C_word t8;
C_word t9;
t7=C_a_i_vector5(&a,5,t2,t3,t4,t5,t6);
t8=C_a_i_cons(&a,2,t7,((C_word*)t0)[2]);
t9=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t9;
av2[1]=t1;
av2[2]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* f_5725 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,6)))){
C_save_and_reclaim((void *)f_5725,c,av);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5731,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word)li88),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:432: decorate */
f_3781(t1,t3,((C_word*)t0)[4]);}

/* a5730 */
static void C_ccall f_5731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6;
C_word t7;
C_word *a;
if(c!=6) C_bad_argc_2(c,6,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_5731,c,av);}
a=C_alloc(5);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5743,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:434: ##sys#vector */
t7=*((C_word*)lf[57]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
av2[5]=t5;
((C_proc)(void*)(*((C_word*)t7+1)))(6,av2);}}

/* k5741 in a5730 */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5743,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* f_5747 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5747(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5747,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5753,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word)li91),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:439: decorate */
f_3781(t1,t3,((C_word*)t0)[5]);}

/* a5752 */
static void C_ccall f_5753(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +15,c,6)))){
C_save_and_reclaim((void*)f_5753,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+15);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5765,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5769,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(t2))){
t5=C_a_i_list1(&a,1,t2);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[57]+1);
av2[3]=t5;
C_apply(4,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7264,a[2]=t2,a[3]=t6,a[4]=((C_word)li90),tmp=(C_word)a,a+=5,tmp));
t8=((C_word*)t6)[1];
f_7264(t8,t4,((C_word*)t0)[4],C_fix(0),t2,C_SCHEME_FALSE);}}

/* k5763 in a5752 */
static void C_ccall f_5765(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5765,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5767 in a5752 */
static void C_ccall f_5769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5769,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[57]+1);
av2[3]=t1;
C_apply(4,av2);}}

/* f_5770 in k5510 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_5770,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5776,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],a[5]=((C_word)li93),tmp=(C_word)a,a+=6,tmp);
/* eval.scm:446: decorate */
f_3781(t1,t3,((C_word*)t0)[5]);}

/* a5775 */
static void C_ccall f_5776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +5,c,4)))){
C_save_and_reclaim((void*)f_5776,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+5);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
t3=C_fix(c - 2);
t4=C_eqp(t3,((C_word*)t0)[2]);
if(C_truep(C_i_not(t4))){
/* eval.scm:450: ##sys#error */
t5=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[59];
av2[3]=((C_word*)t0)[2];
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5798,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[57]+1);
av2[3]=t2;
C_apply(4,av2);}}}

/* k5796 in a5775 */
static void C_ccall f_5798(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5798,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* a5813 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5814,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5818,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* eval.scm:377: ##sys#current-environment808 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_5818(2,av2);}}}

/* k5816 in a5813 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5818,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5822,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:377: ##sys#current-environment808 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5820 in k5816 in a5813 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_5822,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5825,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:377: ##sys#current-environment808 */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k5823 in k5820 in k5816 in a5813 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_5825(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5825,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a5830 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5831(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_5831,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5839,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:379: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[7])[1];
av2[3]=((C_word*)t0)[8];
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}

/* k5837 in a5830 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5839(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_5839,c,av);}
a=C_alloc(12);
if(C_truep(((C_word*)t0)[2])){
/* eval.scm:378: compile-to-closure */
f_3572(((C_word*)t0)[3],t1,((C_word*)t0)[4],C_a_i_list(&a,4,((C_word*)t0)[2],((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE));}
else{
/* eval.scm:378: compile-to-closure */
f_3572(((C_word*)t0)[3],t1,((C_word*)t0)[4],C_a_i_list(&a,4,((C_word*)t0)[7],((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE));}}

/* a5844 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5845(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5845,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5849,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:377: ##sys#current-environment808 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5847 in a5844 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5849,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5852,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:377: ##sys#current-environment808 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k5850 in k5847 in a5844 in k5504 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5852,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5855 in k5501 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5857,c,av);}
/* eval.scm:374: ##sys#extend-se */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[48]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[48]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}

/* map-loop780 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_5859(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_5859,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5884,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:373: g786 */
t4=*((C_word*)lf[49]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5882 in map-loop780 in a5492 in k5486 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5884(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5884,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_5859(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k5892 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_5894,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5899,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li100),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5909,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li101),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:366: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}
else{
t2=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5488(2,av2);}}}

/* a5898 in k5892 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5899,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5907,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:369: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t2;
tp(2,av2);}}

/* k5905 in a5898 in k5892 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5907(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5907,c,av);}
/* eval.scm:368: ##sys#expand-extended-lambda-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[61]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[61]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)((C_word*)t0)[4])[1];
av2[4]=*((C_word*)lf[62]+1);
av2[5]=t1;
tp(6,av2);}}

/* a5908 in k5892 in k5475 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5909,c,av);}
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=t1;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k5918 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_5920,c,av);}
/* eval.scm:361: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[64]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[64]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[65];
av2[3]=((C_word*)t0)[3];
av2[4]=lf[66];
av2[5]=C_SCHEME_FALSE;
av2[6]=t1;
tp(7,av2);}}

/* k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,4)))){
C_save_and_reclaim((void *)f_5929,c,av);}
a=C_alloc(23);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_TRUE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5934,a[2]=t3,a[3]=t5,a[4]=((C_word*)t0)[2],a[5]=((C_word)li102),tmp=(C_word)a,a+=6,tmp);
t7=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5951,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word)li103),tmp=(C_word)a,a+=8,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5969,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word)li104),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:455: ##sys#dynamic-wind */
t9=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=((C_word*)t0)[8];
av2[2]=t6;
av2[3]=t7;
av2[4]=t8;
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}

/* a5933 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5934(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5934,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5938,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* eval.scm:455: ##sys#current-environment874 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_5938(2,av2);}}}

/* k5936 in a5933 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5938(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_5938,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5942,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:455: ##sys#current-environment874 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5940 in k5936 in a5933 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5942(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_5942,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5945,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:455: ##sys#current-environment874 */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k5943 in k5940 in k5936 in a5933 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5945(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5945,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a5950 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5951(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_5951,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5959,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=C_i_cddr(((C_word*)t0)[6]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5967,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:468: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}

/* k5957 in a5950 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5959(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_5959,c,av);}
/* eval.scm:467: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k5965 in a5950 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5967(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5967,c,av);}
/* eval.scm:468: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}

/* a5968 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5969(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_5969,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5973,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:455: ##sys#current-environment874 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k5971 in a5968 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5973(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5973,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5976,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:455: ##sys#current-environment874 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k5974 in k5971 in a5968 in k5927 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5976(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5976,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5992 in map-loop883 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5994(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_5994,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5998,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6002,a[2]=t2,a[3]=((C_word*)t0)[7],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:463: ##sys#eval/meta */
t4=*((C_word*)lf[70]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_cadr(((C_word*)t0)[7]);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5996 in k5992 in map-loop883 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_5998(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_5998,c,av);}
a=C_alloc(12);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],((C_word*)t0)[3],t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[4])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[4])+1,t3);
t6=((C_word*)((C_word*)t0)[5])[1];
f_6027(t6,((C_word*)t0)[6],C_slot(((C_word*)t0)[7],C_fix(1)));}

/* k6000 in k5992 in map-loop883 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6002,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6006,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:464: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6004 in k6000 in k5992 in map-loop883 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6006(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6006,c,av);}
/* eval.scm:462: ##sys#ensure-transformer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[69]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[69]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k6019 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6021,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6025,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:466: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t2;
tp(2,av2);}}

/* k6023 in k6019 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6025,c,av);}
/* eval.scm:457: scheme#append */
t2=*((C_word*)lf[68]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop883 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6027(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_6027,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5994,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,a[7]=t3,tmp=(C_word)a,a+=8,tmp);
/* eval.scm:461: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t5;
tp(2,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6079 in map-loop918 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6081,c,av);}
a=C_alloc(12);
t2=C_a_i_list3(&a,3,((C_word*)t0)[2],C_SCHEME_FALSE,t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=((C_word*)((C_word*)t0)[4])[1];
f_6199(t6,((C_word*)t0)[5],C_slot(((C_word*)t0)[6],C_fix(1)));}

/* k6083 in map-loop918 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6085(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6085,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6089,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:478: chicken.syntax#strip-syntax */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6087 in k6083 in map-loop918 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6089,c,av);}
/* eval.scm:476: ##sys#ensure-transformer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[69]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[69]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6104(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6104,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6107,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6197,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:480: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_6107,c,av);}
a=C_alloc(31);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6108,a[2]=t1,a[3]=((C_word)li106),tmp=(C_word)a,a+=4,tmp);
t3=C_i_check_list_2(((C_word*)t0)[2],lf[72]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6172,a[2]=t2,a[3]=((C_word)li107),tmp=(C_word)a,a+=4,tmp);
t5=(
  f_6172(t4,((C_word*)t0)[2])
);
t6=*((C_word*)lf[9]+1);
t7=t1;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_SCHEME_TRUE;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6127,a[2]=t8,a[3]=t10,a[4]=t6,a[5]=((C_word)li108),tmp=(C_word)a,a+=6,tmp);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6144,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word)li109),tmp=(C_word)a,a+=8,tmp);
t13=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6162,a[2]=t8,a[3]=t6,a[4]=((C_word)li110),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:485: ##sys#dynamic-wind */
t14=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t14;
av2[1]=((C_word*)t0)[8];
av2[2]=t11;
av2[3]=t12;
av2[4]=t13;
((C_proc)(void*)(*((C_word*)t14+1)))(5,av2);}}

/* g947 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_6108(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_set_car(C_i_cdr(t1),((C_word*)t0)[2]));}

/* a6126 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6127,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6131,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* eval.scm:485: ##sys#current-environment955 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_6131(2,av2);}}}

/* k6129 in a6126 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6131(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6131,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6135,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:485: ##sys#current-environment955 */
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6133 in k6129 in a6126 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6135,c,av);}
a=C_alloc(4);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6138,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:485: ##sys#current-environment955 */
t4=((C_word*)t0)[5];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k6136 in k6133 in k6129 in a6126 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6138(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6138,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a6143 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6144,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6152,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=C_i_cddr(((C_word*)t0)[6]);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6160,a[2]=t2,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:487: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}

/* k6150 in a6143 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6152(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6152,c,av);}
/* eval.scm:486: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k6158 in a6143 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6160(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6160,c,av);}
/* eval.scm:487: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}

/* a6161 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6162(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6162,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6166,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:485: ##sys#current-environment955 */
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6164 in a6161 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6166,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6169,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:485: ##sys#current-environment955 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6167 in k6164 in a6161 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6169(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6169,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* for-each-loop946 in k6105 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_6172(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_pairp(t1))){
t2=(
/* eval.scm:481: g947 */
  f_6108(((C_word*)t0)[2],C_slot(t1,C_fix(0)))
);
t4=C_slot(t1,C_fix(1));
t1=t4;
goto loop;}
else{
t2=C_SCHEME_UNDEFINED;
return(t2);}}

/* k6195 in k6102 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6197(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6197,c,av);}
/* eval.scm:480: scheme#append */
t2=*((C_word*)lf[68]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop918 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6199(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_6199,3,t0,t1,t2);}
a=C_alloc(11);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6081,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6085,a[2]=t5,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:477: ##sys#eval/meta */
t7=*((C_word*)lf[70]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=C_i_cadr(t3);
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6246,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6249,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[8])){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6287,a[2]=((C_word*)t0)[9],a[3]=t2,a[4]=((C_word*)t0)[10],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:494: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6249(2,av2);}}}

/* k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6249(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6249,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6252,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6270,a[2]=t2,a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:497: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[77]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[77]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6250 in k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6252,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6255,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6262,a[2]=t2,a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[8],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:501: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6253 in k6250 in k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6255(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6255,c,av);}
/* eval.scm:503: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],lf[74],((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k6260 in k6250 in k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6262(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6262,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6266,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:502: ##sys#eval/meta */
t3=*((C_word*)lf[70]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6264 in k6260 in k6250 in k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6266,c,av);}
/* eval.scm:499: ##sys#extend-macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[75]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[75]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
tp(5,av2);}}

/* k6268 in k6247 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6270(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6270,c,av);}
/* eval.scm:496: ##sys#register-syntax-export */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[76]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[76]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=((C_word*)t0)[4];
tp(5,av2);}}

/* k6285 in k6244 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6287,c,av);}
t2=C_i_assq(((C_word*)t0)[2],t1);
if(C_truep(C_i_not(t2))){
/* eval.scm:495: ##sys#error */
t3=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[0];
av2[3]=lf[78];
av2[4]=((C_word*)t0)[4];
av2[5]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6249(2,av2);}}}

/* k6307 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6309,c,av);}
/* eval.scm:509: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k6315 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6317,c,av);}
/* eval.scm:510: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
av2[4]=C_SCHEME_FALSE;
tp(5,av2);}}

/* a6335 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6336(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,7)))){
C_save_and_reclaim((void *)f_6336,c,av);}
a=C_alloc(11);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6344,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t4=C_i_cdddr(((C_word*)t0)[7]);
if(C_truep(C_i_pairp(t4))){
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6354,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:521: scheme#append */
t6=*((C_word*)lf[68]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
av2[3]=C_i_cadddr(((C_word*)t0)[7]);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t5=C_a_i_cons(&a,2,lf[31],t2);
/* eval.scm:518: compile */
t6=((C_word*)((C_word*)t0)[2])[1];
f_3787(t6,t1,t5,((C_word*)t0)[3],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6]);}}

/* k6342 in a6335 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6344,c,av);}
/* eval.scm:518: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],t1,((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k6352 in a6335 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6354(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6354,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6358,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:522: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t2;
tp(2,av2);}}

/* k6356 in k6352 in a6335 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6358,c,av);}
/* eval.scm:520: ##sys#canonicalize-body */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k6385 in map-loop991 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6387,c,av);}
/* eval.scm:530: chicken.syntax#strip-syntax */
t2=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6397 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_6399,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6401,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word)li113),tmp=(C_word)a,a+=9,tmp);
/* eval.scm:527: ##sys#with-module-aliases */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[85]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[85]+1);
av2[1]=((C_word*)t0)[8];
av2[2]=t1;
av2[3]=t2;
tp(4,av2);}}

/* a6400 in k6397 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6401(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_6401,c,av);}
a=C_alloc(3);
t2=C_i_cddr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,lf[31],t2);
/* eval.scm:533: compile */
t4=((C_word*)((C_word*)t0)[3])[1];
f_3787(t4,t1,t3,((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* map-loop991 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6415(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_6415,3,t0,t1,t2);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6440,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6387,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:529: ##sys#check-syntax */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[64]+1));
C_word av2[5];
av2[0]=*((C_word*)lf[64]+1);
av2[1]=t5;
av2[2]=lf[86];
av2[3]=t4;
av2[4]=lf[87];
tp(5,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6438 in map-loop991 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6440,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6415(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6456(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_6456,c,av);}
a=C_alloc(26);
t2=C_i_cadr(t1);
t3=C_i_caddr(t1);
t4=C_eqp(C_SCHEME_TRUE,t3);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6465,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],tmp=(C_word)a,a+=8,tmp);
if(C_truep(t4)){
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t4;
f_6465(2,av2);}}
else{
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6670,a[2]=t2,a[3]=((C_word)li123),tmp=(C_word)a,a+=4,tmp);
t11=C_u_i_cdr(t1);
t12=C_u_i_cdr(t11);
t13=C_u_i_car(t12);
t14=C_i_check_list_2(t13,lf[42]);
t15=C_SCHEME_UNDEFINED;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_set_block_item(t16,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6727,a[2]=t8,a[3]=t16,a[4]=t10,a[5]=t9,a[6]=((C_word)li124),tmp=(C_word)a,a+=7,tmp));
t18=((C_word*)t16)[1];
f_6727(t18,t5,t13);}}

/* k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6465,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6468,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6663,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:553: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[77]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[77]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6468(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6468,c,av);}
a=C_alloc(12);
t2=*((C_word*)lf[77]+1);
t3=*((C_word*)lf[9]+1);
t4=*((C_word*)lf[89]+1);
t5=*((C_word*)lf[90]+1);
t6=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6471,a[2]=t5,a[3]=t4,a[4]=t3,a[5]=t2,a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:556: ##sys#register-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[98]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[98]+1);
av2[1]=t6;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[8];
tp(5,av2);}}

/* k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6471,c,av);}
a=C_alloc(14);
t2=*((C_word*)lf[91]+1);
t3=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6474,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word*)t0)[10],a[13]=((C_word*)t0)[11],tmp=(C_word)a,a+=14,tmp);
/* eval.scm:561: ##sys#module-alias-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[90]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[90]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6474(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,4)))){
C_save_and_reclaim((void *)f_6474,c,av);}
a=C_alloc(41);
t2=((C_word*)t0)[2];
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_END_OF_LIST;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t0)[3];
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=t1;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_TRUE;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6479,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=t11,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word)li115),tmp=(C_word)a,a+=12,tmp);
t13=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6535,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[12],a[7]=((C_word)li120),tmp=(C_word)a,a+=8,tmp);
t14=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6631,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word)li121),tmp=(C_word)a,a+=11,tmp);
/* eval.scm:555: ##sys#dynamic-wind */
t15=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t15;
av2[1]=((C_word*)t0)[13];
av2[2]=t12;
av2[3]=t13;
av2[4]=t14;
((C_proc)(void*)(*((C_word*)t15+1)))(5,av2);}}

/* a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6479(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6479,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6483,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* eval.scm:555: ##sys#current-module1066 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_6483(2,av2);}}}

/* k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_6483,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6486,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=t1,tmp=(C_word)a,a+=13,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* eval.scm:555: ##sys#current-environment1067 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
f_6486(2,av2);}}}

/* k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6486(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_6486,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6489,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* eval.scm:555: ##sys#macro-environment1068 */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[4])[1];
f_6489(2,av2);}}}

/* k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_6489(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_6489,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_6492,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],tmp=(C_word)a,a+=15,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* eval.scm:555: ##sys#module-alias-environment1069 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[5])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[5])[1];
f_6492(2,av2);}}}

/* k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_6492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_6492,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_6496,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],tmp=(C_word)a,a+=16,tmp);
/* eval.scm:555: ##sys#current-module1066 */
t3=((C_word*)t0)[13];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in ... */
static void C_ccall f_6496(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_6496,c,av);}
a=C_alloc(15);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_6500,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],tmp=(C_word)a,a+=15,tmp);
/* eval.scm:555: ##sys#current-environment1067 */
t4=((C_word*)t0)[12];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in ... */
static void C_ccall f_6500(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6500,c,av);}
a=C_alloc(14);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6504,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],tmp=(C_word)a,a+=14,tmp);
/* eval.scm:555: ##sys#macro-environment1068 */
t4=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in ... */
static void C_ccall f_6504(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6504,c,av);}
a=C_alloc(13);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6508,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],tmp=(C_word)a,a+=13,tmp);
/* eval.scm:555: ##sys#module-alias-environment1069 */
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k6506 in k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in ... */
static void C_ccall f_6508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6508,c,av);}
a=C_alloc(10);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6511,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:555: ##sys#current-module1066 */
t4=((C_word*)t0)[11];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[12];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k6509 in k6506 in k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in ... */
static void C_ccall f_6511(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_6511,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6514,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:555: ##sys#current-environment1067 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6512 in k6509 in k6506 in k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in ... */
static void C_ccall f_6514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_6514,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6517,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:555: ##sys#macro-environment1068 */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6515 in k6512 in k6509 in k6506 in k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in ... */
static void C_ccall f_6517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6517,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6520,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:555: ##sys#module-alias-environment1069 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6518 in k6515 in k6512 in k6509 in k6506 in k6502 in k6498 in k6494 in k6490 in k6487 in k6484 in k6481 in a6478 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in ... */
static void C_ccall f_6520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6520,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6535,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6541,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li119),tmp=(C_word)a,a+=8,tmp);
/* eval.scm:562: ##sys#with-property-restore */
t3=*((C_word*)lf[97]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6541,c,av);}
a=C_alloc(10);
t2=C_i_cdddr(((C_word*)t0)[2]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6551,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word)li118),tmp=(C_word)a,a+=8,tmp));
t6=((C_word*)t4)[1];
f_6551(t6,t1,t2,C_SCHEME_END_OF_LIST);}

/* loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6551(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,7)))){
C_save_and_reclaim_args((void *)trf_6551,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_i_nullp(t2))){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6561,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:566: reverse */
t5=*((C_word*)lf[96]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t4=C_i_cdr(t2);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6627,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t4,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:581: compile */
t6=((C_word*)((C_word*)t0)[4])[1];
f_3787(t6,t5,C_u_i_car(t2),C_SCHEME_END_OF_LIST,C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_TRUE);}}

/* k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_6561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6561,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6564,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6612,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:567: ##sys#current-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[77]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[77]+1);
av2[1]=t3;
tp(2,av2);}}

/* k6562 in k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_6564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6564,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6567,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6608,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:568: chicken.internal#module-requirement */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[94]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[94]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}

/* k6565 in k6562 in k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in ... */
static void C_ccall f_6567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_6567,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6568,a[2]=((C_word*)t0)[2],a[3]=((C_word)li117),tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_6568 in k6565 in k6562 in k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in ... */
static void C_ccall f_6568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6568,c,av);}
a=C_alloc(7);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6574,a[2]=t4,a[3]=t2,a[4]=((C_word)li116),tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_6574(t6,t1,((C_word*)t0)[2]);}

/* loop2 */
static void C_fcall f_6574(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_6574,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=*((C_word*)lf[92]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_cdr(t2);
if(C_truep(C_i_pairp(t3))){
t4=C_u_i_car(t2);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6597,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:574: g1123 */
t6=t4;{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}
else{
/* eval.scm:574: g1126 */
t4=C_u_i_car(t2);{
C_word av2[3];
av2[0]=t4;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}}}

/* k6595 in loop2 */
static void C_ccall f_6597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6597,c,av);}
/* eval.scm:576: loop2 */
t2=((C_word*)((C_word*)t0)[2])[1];
f_6574(t2,((C_word*)t0)[3],((C_word*)t0)[4]);}

/* k6606 in k6562 in k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in ... */
static void C_ccall f_6608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6608,c,av);}
/* eval.scm:568: ##sys#provide */
t2=*((C_word*)lf[93]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6610 in k6559 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_6612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6612,c,av);}
/* eval.scm:567: ##sys#finalize-module */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[95]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[95]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k6625 in loop in a6540 in a6534 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_6627(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_6627,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,((C_word*)t0)[2]);
/* eval.scm:579: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_6551(t3,((C_word*)t0)[4],((C_word*)t0)[5],t2);}

/* a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6631(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6631,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6635,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:555: ##sys#current-module1066 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6635,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6638,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:555: ##sys#current-environment1067 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6638(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6638,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6641,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* eval.scm:555: ##sys#macro-environment1068 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_6641(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6641,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6644,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* eval.scm:555: ##sys#module-alias-environment1069 */
t3=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k6642 in k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_6644(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,4)))){
C_save_and_reclaim((void *)f_6644,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6647,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
/* eval.scm:555: ##sys#current-module1066 */
t3=((C_word*)t0)[13];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6645 in k6642 in k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in ... */
static void C_ccall f_6647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_6647,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6650,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* eval.scm:555: ##sys#current-environment1067 */
t3=((C_word*)t0)[13];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6648 in k6645 in k6642 in k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in ... */
static void C_ccall f_6650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6650,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6653,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:555: ##sys#macro-environment1068 */
t3=((C_word*)t0)[12];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6651 in k6648 in k6645 in k6642 in k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in ... */
static void C_ccall f_6653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_6653,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6656,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:555: ##sys#module-alias-environment1069 */
t3=((C_word*)t0)[11];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[8])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k6654 in k6651 in k6648 in k6645 in k6642 in k6639 in k6636 in k6633 in a6630 in k6472 in k6469 in k6466 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in ... */
static void C_ccall f_6656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6656,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,((C_word*)t0)[5]);
t4=C_mutate(((C_word *)((C_word*)t0)[6])+1,((C_word*)t0)[7]);
t5=C_mutate(((C_word *)((C_word*)t0)[8])+1,((C_word*)t0)[9]);
t6=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k6661 in k6463 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6663,c,av);}
if(C_truep(t1)){
/* eval.scm:554: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[62]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[62]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[99];
av2[3]=lf[100];
av2[4]=((C_word*)t0)[3];
tp(5,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_6468(2,av2);}}}

/* g1034 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6670(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_6670,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_symbolp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6683,a[2]=t1,a[3]=t2,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_pairp(t2))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6694,a[2]=((C_word)li122),tmp=(C_word)a,a+=3,tmp);
t5=t3;
f_6683(t5,(
  f_6694(t2)
));}
else{
t4=t3;
f_6683(t4,C_SCHEME_FALSE);}}}

/* k6681 in g1034 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6683(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,5)))){
C_save_and_reclaim_args((void *)trf_6683,2,t0,t1);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* eval.scm:549: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[62]+1));
C_word av2[6];
av2[0]=*((C_word*)lf[62]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[99];
av2[3]=lf[101];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[4];
tp(6,av2);}}}

/* loop in g1034 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_6694(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_i_nullp(t1);
if(C_truep(t2)){
return(t2);}
else{
t3=C_i_car(t1);
if(C_truep(C_i_symbolp(t3))){
t5=C_u_i_cdr(t1);
t1=t5;
goto loop;}
else{
return(C_SCHEME_FALSE);}}}

/* map-loop1028 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6727(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6727,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6752,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:540: g1034 */
t4=((C_word*)t0)[4];
f_6670(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6750 in map-loop1028 in k6454 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6752,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6727(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6778 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_6780,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
/* eval.scm:588: compile */
t4=((C_word*)((C_word*)t0)[3])[1];
f_3787(t4,((C_word*)t0)[4],t3,((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[6],((C_word*)t0)[7],C_SCHEME_FALSE);}

/* k6812 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6814,c,av);}
/* eval.scm:595: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],lf[105],((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* a6836 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6837,c,av);}
/* eval.scm:600: ##sys#process-require */
t2=*((C_word*)lf[109]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* a6842 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6843,c,av);}
/* eval.scm:601: compile */
t4=((C_word*)((C_word*)t0)[2])[1];
f_3787(t4,t1,t2,((C_word*)t0)[3],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k6857 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6859,c,av);}
/* eval.scm:605: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],lf[112],((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k6898 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6900(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6900,c,av);}
/* eval.scm:615: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],lf[118],((C_word*)t0)[4],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k6921 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,7)))){
C_save_and_reclaim((void *)f_6923,c,av);}
a=C_alloc(3);
t2=C_i_cdr(((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
/* eval.scm:618: compile */
t4=((C_word*)((C_word*)t0)[3])[1];
f_3787(t4,((C_word*)t0)[4],t3,((C_word*)t0)[5],C_SCHEME_FALSE,((C_word*)t0)[6],((C_word*)t0)[7],((C_word*)t0)[8]);}

/* k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6936(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,0,7)))){
C_save_and_reclaim_args((void *)trf_6936,2,t0,t1);}
a=C_alloc(25);
if(C_truep(t1)){
/* eval.scm:625: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[62]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[62]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[124];
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[125]);
if(C_truep(t2)){
t3=C_i_cdr(((C_word*)t0)[3]);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6956,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[2],a[4]=t3,a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:628: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t4;
tp(2,av2);}}
else{
t3=C_eqp(((C_word*)t0)[4],lf[126]);
if(C_truep(t3)){
/* eval.scm:631: compile */
t4=((C_word*)((C_word*)t0)[9])[1];
f_3787(t4,((C_word*)t0)[2],C_i_cadddr(((C_word*)t0)[3]),((C_word*)t0)[6],((C_word*)t0)[10],((C_word*)t0)[7],((C_word*)t0)[8],((C_word*)t0)[11]);}
else{
t4=C_eqp(((C_word*)t0)[4],lf[127]);
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7000,a[2]=((C_word*)t0)[9],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[11],a[8]=((C_word*)t0)[2],a[9]=((C_word*)t0)[3],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:635: chicken.syntax#strip-syntax */
t6=*((C_word*)lf[24]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_i_cdddr(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t5=C_a_i_cons(&a,2,((C_word*)t0)[4],*((C_word*)lf[131]+1));
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_FALSE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7012,a[2]=t9,a[3]=t7,a[4]=((C_word)li128),tmp=(C_word)a,a+=5,tmp);
t11=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7017,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word)li129),tmp=(C_word)a,a+=8,tmp);
t12=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7027,a[2]=t7,a[3]=t9,a[4]=((C_word)li130),tmp=(C_word)a,a+=5,tmp);
/* eval.scm:645: ##sys#dynamic-wind */
t13=*((C_word*)lf[47]+1);{
C_word av2[5];
av2[0]=t13;
av2[1]=((C_word*)t0)[2];
av2[2]=t10;
av2[3]=t11;
av2[4]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(5,av2);}}}}}}

/* k6954 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_6956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6956,c,av);}
/* eval.scm:628: compile-call */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7336(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* g1211 in k6998 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_6982(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_6982,3,t0,t1,t2);}
/* eval.scm:637: compile */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3787(t3,t1,C_i_cadr(t2),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k6998 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_7000,c,av);}
a=C_alloc(9);
t2=C_i_assq(lf[128],t1);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6982,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word)li127),tmp=(C_word)a,a+=9,tmp);
/* eval.scm:635: g1211 */
t4=t3;
f_6982(t4,((C_word*)t0)[8],t2);}
else{
/* eval.scm:639: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[62]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[62]+1);
av2[1]=((C_word*)t0)[8];
av2[2]=lf[129];
av2[3]=lf[130];
av2[4]=((C_word*)t0)[9];
tp(5,av2);}}}

/* a7011 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7012(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7012,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[131]+1));
t3=C_mutate((C_word*)lf[131]+1 /* (set! ##sys#syntax-context ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a7016 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7017,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7025,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:646: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7023 in a7016 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7025,c,av);}
/* eval.scm:646: compile-call */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7336(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* a7026 in k6934 in k4104 in k4089 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7027,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[131]+1));
t3=C_mutate((C_word*)lf[131]+1 /* (set! ##sys#syntax-context ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k7200 in k4086 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7202,c,av);}
/* eval.scm:194: chicken.syntax#expand */
t2=*((C_word*)lf[142]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7203 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7205(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_7205,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7212,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:650: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word av2[2];
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7210 in k7203 in k4066 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7212,c,av);}
/* eval.scm:650: compile-call */
t2=((C_word*)((C_word*)t0)[2])[1];
f_7336(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],((C_word*)t0)[7]);}

/* k7233 in k4002 in k3792 in compile in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7235(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7235,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
f_4068(2,av2);}}
else{
t2=C_i_vectorp(((C_word*)t0)[3]);
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_4068(2,av2);}}
else{
/* eval.scm:188: ##sys#srfi-4-vector? */
t3=*((C_word*)lf[143]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}}

/* doloop1229 in a5752 */
static void C_fcall f_7264(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,5)))){
C_save_and_reclaim_args((void *)trf_7264,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(7);
t6=C_eqp(t2,C_fix(0));
if(C_truep(t6)){
t7=C_a_i_list1(&a,1,t4);
t8=C_i_setslot(t5,C_fix(1),t7);
t9=t1;{
C_word av2[2];
av2[0]=t9;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t9+1)))(2,av2);}}
else{
t7=C_fixnum_difference(t2,C_fix(1));
t8=C_fixnum_plus(t3,C_fix(1));
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7293,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t7,a[5]=t8,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
t10=C_eqp(C_SCHEME_END_OF_LIST,t4);
if(C_truep(t10)){
/* eval.scm:659: ##sys#error */
t11=*((C_word*)lf[17]+1);{
C_word av2[5];
av2[0]=t11;
av2[1]=t9;
av2[2]=lf[58];
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}
else{
t12=t1;
t13=t7;
t14=t8;
t15=C_slot(t4,C_fix(1));
t16=t4;
t1=t12;
t2=t13;
t3=t14;
t4=t15;
t5=t16;
goto loop;}}}

/* k7291 in doloop1229 in a5752 */
static void C_ccall f_7293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7293,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7264(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],t1,((C_word*)t0)[6]);}

/* loop in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_7310(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(t2);}
else{
if(C_truep(C_i_pairp(t1))){
t4=C_slot(t1,C_fix(1));
t5=C_fixnum_plus(t2,C_fix(1));
t1=t4;
t2=t5;
goto loop;}
else{
return(C_SCHEME_FALSE);}}}

/* compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7336(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,0,7)))){
C_save_and_reclaim_args((void *)trf_7336,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(13);
t6=C_slot(t2,C_fix(0));
t7=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7343,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=t4,a[6]=t5,a[7]=t3,a[8]=((C_word*)t0)[3],tmp=(C_word)a,a+=9,tmp);
if(C_truep(C_i_closurep(t6))){
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7660,a[2]=t6,a[3]=((C_word)li143),tmp=(C_word)a,a+=4,tmp);
f_7343(2,av2);}}
else{
/* eval.scm:676: compile */
t8=((C_word*)((C_word*)t0)[3])[1];
f_3787(t8,t7,C_slot(t2,C_fix(0)),t3,C_SCHEME_FALSE,t4,t5,C_SCHEME_FALSE);}}

/* k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,7)))){
C_save_and_reclaim((void *)f_7343,c,av);}
a=C_alloc(33);
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7310,a[2]=((C_word)li132),tmp=(C_word)a,a+=3,tmp);
t4=(
  f_7310(t2,C_fix(0))
);
switch(t4){
case C_SCHEME_FALSE:
/* eval.scm:681: ##sys#syntax-error/context */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[21]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[21]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=lf[147];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}
case C_fix(0):
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7365,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word)li133),tmp=(C_word)a,a+=9,tmp);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}
case C_fix(1):
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7384,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[3],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:685: compile */
t6=((C_word*)((C_word*)t0)[8])[1];
f_3787(t6,t5,C_slot(t2,C_fix(0)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);
case C_fix(2):
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7412,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=t2,tmp=(C_word)a,a+=11,tmp);
/* eval.scm:689: compile */
t6=((C_word*)((C_word*)t0)[8])[1];
f_3787(t6,t5,C_slot(t2,C_fix(0)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);
case C_fix(3):
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7448,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[8],a[10]=t2,tmp=(C_word)a,a+=11,tmp);
/* eval.scm:694: compile */
t6=((C_word*)((C_word*)t0)[8])[1];
f_3787(t6,t5,C_slot(t2,C_fix(0)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);
case C_fix(4):
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7492,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=t2,tmp=(C_word)a,a+=11,tmp);
/* eval.scm:700: compile */
t6=((C_word*)((C_word*)t0)[8])[1];
f_3787(t6,t5,C_slot(t2,C_fix(0)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE);
default:
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7539,a[2]=((C_word*)t0)[8],a[3]=((C_word*)t0)[7],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word)li138),tmp=(C_word)a,a+=7,tmp);
t10=C_i_check_list_2(t2,lf[42]);
t11=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7549,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,tmp=(C_word)a,a+=9,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7612,a[2]=t7,a[3]=t13,a[4]=t9,a[5]=t8,a[6]=((C_word)li142),tmp=(C_word)a,a+=7,tmp));
t15=((C_word*)t13)[1];
f_7612(t15,t11,t2);}}

/* f_7365 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7365(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7365,c,av);}
a=C_alloc(8);
t3=(
/* eval.scm:683: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7372,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t5=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7370 */
static void C_ccall f_7372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7372,c,av);}
/* eval.scm:682: g1264 */
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)C_fast_retrieve_proc(t2))(2,av2);}}

/* k7382 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7384,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7385,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word)li134),tmp=(C_word)a,a+=10,tmp);
t3=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_7385 in k7382 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_7385,c,av);}
a=C_alloc(10);
t3=(
/* eval.scm:687: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7392,a[2]=t1,a[3]=((C_word*)t0)[7],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7390 */
static void C_ccall f_7392(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7392,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7399,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=((C_word*)t0)[3];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7397 in k7390 */
static void C_ccall f_7399(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7399,c,av);}
/* eval.scm:686: g1269 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* k7410 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7412(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,7)))){
C_save_and_reclaim((void *)f_7412,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7415,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:690: compile */
t3=((C_word*)((C_word*)t0)[9])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[10],C_fix(1)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k7413 in k7410 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7415(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7415,c,av);}
a=C_alloc(11);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7416,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word)li135),tmp=(C_word)a,a+=11,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_7416 in k7413 in k7410 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_7416,c,av);}
a=C_alloc(11);
t3=(
/* eval.scm:692: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7423,a[2]=t1,a[3]=((C_word*)t0)[7],a[4]=t2,a[5]=((C_word*)t0)[8],tmp=(C_word)a,a+=6,tmp);
t5=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7421 */
static void C_ccall f_7423(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7423,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7430,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7428 in k7421 */
static void C_ccall f_7430(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7430,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7434,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7432 in k7428 in k7421 */
static void C_ccall f_7434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7434,c,av);}
/* eval.scm:691: g1275 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k7446 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,7)))){
C_save_and_reclaim((void *)f_7448,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7451,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:695: compile */
t3=((C_word*)((C_word*)t0)[9])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[10],C_fix(1)),((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[3],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k7449 in k7446 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7451(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,7)))){
C_save_and_reclaim((void *)f_7451,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7454,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:696: compile */
t3=((C_word*)((C_word*)t0)[10])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[11],C_fix(2)),((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[3],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k7452 in k7449 in k7446 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_7454,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7455,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word)li136),tmp=(C_word)a,a+=12,tmp);
t3=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* f_7455 in k7452 in k7449 in k7446 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7455(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7455,c,av);}
a=C_alloc(12);
t3=(
/* eval.scm:698: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7462,a[2]=t1,a[3]=((C_word*)t0)[7],a[4]=t2,a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],tmp=(C_word)a,a+=7,tmp);
t5=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7460 */
static void C_ccall f_7462(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7462,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7469,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7467 in k7460 */
static void C_ccall f_7469(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7469,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7473,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7471 in k7467 in k7460 */
static void C_ccall f_7473(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7473,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7477,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7475 in k7471 in k7467 in k7460 */
static void C_ccall f_7477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7477,c,av);}
/* eval.scm:697: g1282 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=t1;
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k7490 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7492(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,7)))){
C_save_and_reclaim((void *)f_7492,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7495,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:701: compile */
t3=((C_word*)((C_word*)t0)[9])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[10],C_fix(1)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k7493 in k7490 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7495(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,7)))){
C_save_and_reclaim((void *)f_7495,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7498,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* eval.scm:702: compile */
t3=((C_word*)((C_word*)t0)[10])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[11],C_fix(2)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k7496 in k7493 in k7490 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,7)))){
C_save_and_reclaim((void *)f_7498,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7501,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:703: compile */
t3=((C_word*)((C_word*)t0)[11])[1];
f_3787(t3,t2,C_u_i_list_ref(((C_word*)t0)[12],C_fix(3)),((C_word*)t0)[7],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[6],C_SCHEME_FALSE);}

/* k7499 in k7496 in k7493 in k7490 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7501(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_7501,c,av);}
a=C_alloc(13);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7502,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word)li137),tmp=(C_word)a,a+=13,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_7502 in k7499 in k7496 in k7493 in k7490 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7502(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7502,c,av);}
a=C_alloc(13);
t3=(
/* eval.scm:705: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7509,a[2]=t1,a[3]=((C_word*)t0)[7],a[4]=t2,a[5]=((C_word*)t0)[8],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],tmp=(C_word)a,a+=8,tmp);
t5=((C_word*)t0)[11];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7507 */
static void C_ccall f_7509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7509,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7516,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7514 in k7507 */
static void C_ccall f_7516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7516,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7520,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7518 in k7514 in k7507 */
static void C_ccall f_7520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7520,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7524,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7522 in k7518 in k7514 in k7507 */
static void C_ccall f_7524(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7524,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7528,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,tmp=(C_word)a,a+=7,tmp);
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7526 in k7522 in k7518 in k7514 in k7507 */
static void C_ccall f_7528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_7528,c,av);}
/* eval.scm:704: g1290 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=t1;
((C_proc)C_fast_retrieve_proc(t2))(6,av2);}}

/* g1302 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7539(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,7)))){
C_save_and_reclaim_args((void *)trf_7539,3,t0,t1,t2);}
/* eval.scm:707: compile */
t3=((C_word*)((C_word*)t0)[2])[1];
f_3787(t3,t1,t2,((C_word*)t0)[3],C_SCHEME_FALSE,((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_FALSE);}

/* k7547 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7549,c,av);}
a=C_alloc(10);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7550,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=t1,a[8]=((C_word*)t0)[8],a[9]=((C_word)li141),tmp=(C_word)a,a+=10,tmp);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* f_7550 in k7547 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_7550,c,av);}
a=C_alloc(10);
t3=(
/* eval.scm:709: emit-trace-info */
  f_3757(C_a_i(&a,5),((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],((C_word*)t0)[6],t2)
);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7561,a[2]=t2,a[3]=((C_word*)t0)[7],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t5=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k7559 */
static void C_ccall f_7561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,3)))){
C_save_and_reclaim((void *)f_7561,c,av);}
a=C_alloc(22);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7566,a[2]=((C_word*)t0)[2],a[3]=((C_word)li139),tmp=(C_word)a,a+=4,tmp);
t7=C_i_check_list_2(((C_word*)t0)[3],lf[42]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7576,a[2]=((C_word*)t0)[4],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7578,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,a[6]=((C_word)li140),tmp=(C_word)a,a+=7,tmp));
t12=((C_word*)t10)[1];
f_7578(t12,t8,((C_word*)t0)[3]);}

/* g1330 in k7559 */
static void C_fcall f_7566(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_7566,3,t0,t1,t2);}
t3=t2;{
C_word av2[3];
av2[0]=t3;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7574 in k7559 */
static void C_ccall f_7576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7576,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_apply(4,av2);}}

/* map-loop1324 in k7559 */
static void C_fcall f_7578(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7578,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7603,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:710: g1330 */
t4=((C_word*)t0)[4];
f_7566(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7601 in map-loop1324 in k7559 */
static void C_ccall f_7603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7603,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7578(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1296 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7612(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7612,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7637,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* eval.scm:707: g1302 */
t4=((C_word*)t0)[4];
f_7539(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7635 in map-loop1296 in k7341 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7637(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7637,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7612(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* f_7660 in compile-call in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7660,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7677 in chicken.eval#compile-to-closure in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7679(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_7679,c,av);}
/* eval.scm:712: compile */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3787(t2,((C_word*)t0)[3],((C_word*)t0)[4],((C_word*)t0)[5],C_SCHEME_FALSE,C_fixnum_greaterp(t1,C_fix(0)),((C_word*)t0)[6],((C_word*)t0)[7]);}

/* ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_7729,c,av);}
a=C_alloc(7);
t3=*((C_word*)lf[77]+1);
t4=*((C_word*)lf[89]+1);
t5=*((C_word*)lf[9]+1);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7733,a[2]=t5,a[3]=t4,a[4]=t3,a[5]=t2,a[6]=t1,tmp=(C_word)a,a+=7,tmp);
/* eval.scm:718: ##sys#meta-macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[148]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[148]+1);
av2[1]=t6;
tp(2,av2);}}

/* k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7733(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7733,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7736,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:719: ##sys#current-meta-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[149]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[149]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,4)))){
C_save_and_reclaim((void *)f_7736,c,av);}
a=C_alloc(31);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=((C_word*)t0)[2];
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=t1;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_TRUE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7741,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=((C_word*)t0)[3],a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word)li146),tmp=(C_word)a,a+=10,tmp);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7784,a[2]=((C_word*)t0)[6],a[3]=((C_word)li149),tmp=(C_word)a,a+=4,tmp);
t12=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7816,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word)li150),tmp=(C_word)a,a+=9,tmp);
/* eval.scm:717: ##sys#dynamic-wind */
t13=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t13;
av2[1]=((C_word*)t0)[7];
av2[2]=t10;
av2[3]=t11;
av2[4]=t12;
((C_proc)(void*)(*((C_word*)t13+1)))(5,av2);}}

/* a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7741(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_7741,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7745,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
if(C_truep(((C_word*)((C_word*)t0)[5])[1])){
/* eval.scm:717: ##sys#current-module1364 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[2])[1];
f_7745(2,av2);}}}

/* k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_7745,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7748,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=t1,tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)((C_word*)t0)[5])[1])){
/* eval.scm:717: ##sys#macro-environment1365 */
t3=((C_word*)t0)[8];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[3])[1];
f_7748(2,av2);}}}

/* k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_7748,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7751,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=t1,a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],tmp=(C_word)a,a+=12,tmp);
if(C_truep(((C_word*)((C_word*)t0)[5])[1])){
/* eval.scm:717: ##sys#current-environment1366 */
t3=((C_word*)t0)[7];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)((C_word*)t0)[4])[1];
f_7751(2,av2);}}}

/* k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7751(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_7751,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_7755,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* eval.scm:717: ##sys#current-module1364 */
t3=((C_word*)t0)[10];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_7755,c,av);}
a=C_alloc(12);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_7759,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],tmp=(C_word)a,a+=12,tmp);
/* eval.scm:717: ##sys#macro-environment1365 */
t4=((C_word*)t0)[9];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k7757 in k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7759(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_7759,c,av);}
a=C_alloc(11);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7763,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:717: ##sys#current-environment1366 */
t4=((C_word*)t0)[6];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)C_fast_retrieve_proc(t4))(2,av2);}}

/* k7761 in k7757 in k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7763(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_7763,c,av);}
a=C_alloc(8);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7766,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:717: ##sys#current-module1364 */
t4=((C_word*)t0)[9];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[10];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t4))(5,av2);}}

/* k7764 in k7761 in k7757 in k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7766(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_7766,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7769,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:717: ##sys#macro-environment1365 */
t3=((C_word*)t0)[6];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k7767 in k7764 in k7761 in k7757 in k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7769(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_7769,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7772,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:717: ##sys#current-environment1366 */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k7770 in k7767 in k7764 in k7761 in k7757 in k7753 in k7749 in k7746 in k7743 in a7740 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7772,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_7784,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7790,a[2]=((C_word*)t0)[2],a[3]=((C_word)li147),tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7799,a[2]=((C_word)li148),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:720: scheme#dynamic-wind */
t4=*((C_word*)lf[150]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=*((C_word*)lf[151]+1);
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* a7789 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7790(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_7790,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7794,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:723: compile-to-closure */
f_3572(t2,((C_word*)t0)[2],C_SCHEME_END_OF_LIST,C_a_i_list(&a,4,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_FALSE,C_SCHEME_TRUE));}

/* k7792 in a7789 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7794,c,av);}
/* eval.scm:722: g1398 */
t2=t1;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_END_OF_LIST;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a7798 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_7799,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7803,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7814,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:735: ##sys#current-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[9]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[9]+1);
av2[1]=t3;
tp(2,av2);}}

/* k7801 in a7798 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7803,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7810,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:736: ##sys#macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[89]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[89]+1);
av2[1]=t2;
tp(2,av2);}}

/* k7808 in k7801 in a7798 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7810(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7810,c,av);}
/* eval.scm:736: ##sys#meta-macro-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[148]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[148]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* k7812 in a7798 in a7783 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7814(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7814,c,av);}
/* eval.scm:735: ##sys#current-meta-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[149]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[149]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7816,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7820,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:717: ##sys#current-module1364 */
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_7820,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7823,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:717: ##sys#macro-environment1365 */
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k7821 in k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7823(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_7823,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7826,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:717: ##sys#current-environment1366 */
t3=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)C_fast_retrieve_proc(t3))(2,av2);}}

/* k7824 in k7821 in k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7826(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_7826,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_7829,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* eval.scm:717: ##sys#current-module1364 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k7827 in k7824 in k7821 in k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_7829,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_7832,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* eval.scm:717: ##sys#macro-environment1365 */
t3=((C_word*)t0)[10];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k7830 in k7827 in k7824 in k7821 in k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_7832,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_7835,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:717: ##sys#current-environment1366 */
t3=((C_word*)t0)[9];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
((C_proc)C_fast_retrieve_proc(t3))(5,av2);}}

/* k7833 in k7830 in k7827 in k7824 in k7821 in k7818 in a7815 in k7734 in k7731 in ##sys#eval/meta in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7835,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,((C_word*)t0)[3]);
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,((C_word*)t0)[5]);
t4=C_mutate(((C_word *)((C_word*)t0)[6])+1,((C_word*)t0)[7]);
t5=((C_word*)t0)[8];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,4)))){
C_save_and_reclaim((void *)f_7842,c,av);}
a=C_alloc(15);
t2=C_mutate((C_word*)lf[152]+1 /* (set! chicken.eval#eval-handler ...) */,t1);
t3=C_mutate((C_word*)lf[153]+1 /* (set! scheme#eval ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7844,a[2]=((C_word)li152),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[154]+1 /* (set! chicken.eval#module-environment ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7854,a[2]=((C_word)li153),tmp=(C_word)a,a+=3,tmp));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7861,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11466,a[2]=((C_word)li263),tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11534,a[2]=((C_word)li264),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:775: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t5;
av2[2]=t6;
av2[3]=t7;
C_call_with_values(4,av2);}}

/* scheme#eval in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-3)*C_SIZEOF_PAIR +5,c,2)))){
C_save_and_reclaim((void*)f_7844,c,av);}
a=C_alloc((c-3)*C_SIZEOF_PAIR+5);
t3=C_build_rest(&a,c,3,av);
C_word t4;
C_word t5;
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7852,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:765: eval-handler */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[152]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[152]+1);
av2[1]=t4;
tp(2,av2);}}

/* k7850 in scheme#eval in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7852,c,av);}{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
C_apply(5,av2);}}

/* chicken.eval#module-environment in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7854(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7854,c,av);}
/* eval.scm:770: chicken.module#module-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[155]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[155]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}

/* k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,4)))){
C_save_and_reclaim((void *)f_7861,c,av);}
a=C_alloc(18);
t2=C_mutate((C_word*)lf[60]+1 /* (set! ##sys#decompose-lambda-list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7863,a[2]=((C_word)li156),tmp=(C_word)a,a+=3,tmp));
t3=C_a_i_record4(&a,4,lf[158],lf[159],C_SCHEME_FALSE,C_SCHEME_FALSE);
t4=C_mutate((C_word*)lf[160]+1 /* (set! scheme#interaction-environment ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7948,a[2]=t3,a[3]=((C_word)li157),tmp=(C_word)a,a+=4,tmp));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7952,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11450,a[2]=((C_word)li257),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:822: ##sys#register-record-printer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[364]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[364]+1);
av2[1]=t5;
av2[2]=lf[365];
av2[3]=t6;
tp(4,av2);}}

/* ##sys#decompose-lambda-list in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,5)))){
C_save_and_reclaim((void *)f_7863,c,av);}
a=C_alloc(12);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7866,a[2]=t2,a[3]=((C_word)li154),tmp=(C_word)a,a+=4,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7876,a[2]=t3,a[3]=t4,a[4]=t6,a[5]=((C_word)li155),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_7876(t8,t1,t2,C_SCHEME_END_OF_LIST,C_fix(0));}

/* err in ##sys#decompose-lambda-list in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7866(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_7866,2,t0,t1);}
t2=C_set_block_item(lf[156] /* ##sys#syntax-error-culprit */,0,C_SCHEME_FALSE);
/* eval.scm:807: ##sys#syntax-error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[62]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[62]+1);
av2[1]=t1;
av2[2]=lf[157];
av2[3]=((C_word*)t0)[2];
tp(4,av2);}}

/* loop in ##sys#decompose-lambda-list in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7876(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_7876,5,t0,t1,t2,t3,t4);}
a=C_alloc(9);
t5=C_eqp(t2,C_SCHEME_END_OF_LIST);
if(C_truep(t5)){
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7890,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:810: reverse */
t7=*((C_word*)lf[96]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
if(C_truep(C_i_not(C_blockp(t2)))){
/* eval.scm:811: err */
t6=((C_word*)t0)[3];
f_7866(t6,t1);}
else{
if(C_truep(C_symbolp(t2))){
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7909,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t7=C_a_i_cons(&a,2,t2,t3);
/* eval.scm:812: reverse */
t8=*((C_word*)lf[96]+1);{
C_word av2[3];
av2[0]=t8;
av2[1]=t6;
av2[2]=t7;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}
else{
if(C_truep(C_i_not(C_pairp(t2)))){
/* eval.scm:813: err */
t6=((C_word*)t0)[3];
f_7866(t6,t1);}
else{
t6=C_slot(t2,C_fix(1));
t7=C_slot(t2,C_fix(0));
t8=C_a_i_cons(&a,2,t7,t3);
/* eval.scm:814: loop */
t10=t1;
t11=t6;
t12=t8;
t13=C_fixnum_plus(t4,C_fix(1));
t1=t10;
t2=t11;
t3=t12;
t4=t13;
goto loop;}}}}}

/* k7888 in loop in ##sys#decompose-lambda-list in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7890(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7890,c,av);}
/* eval.scm:810: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=C_SCHEME_FALSE;
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* k7907 in loop in ##sys#decompose-lambda-list in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7909(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7909,c,av);}
/* eval.scm:812: k */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t2))(5,av2);}}

/* scheme#interaction-environment in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7948(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7948,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7952(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7952,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7955,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:827: chicken.module#module-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[155]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[155]+1);
av2[1]=t2;
av2[2]=lf[359];
av2[3]=lf[360];
tp(4,av2);}}

/* k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7955(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_7955,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7958,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:828: chicken.module#module-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[155]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[155]+1);
av2[1]=t2;
av2[2]=lf[357];
av2[3]=lf[358];
tp(4,av2);}}

/* k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7958(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7958,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7961,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:829: chicken.module#module-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[155]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[155]+1);
av2[1]=t2;
av2[2]=lf[355];
av2[3]=lf[356];
tp(4,av2);}}

/* k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7961(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_7961,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7964,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:830: chicken.module#module-environment */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[155]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[155]+1);
av2[1]=t2;
av2[2]=lf[353];
av2[3]=lf[354];
tp(4,av2);}}

/* k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_7964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7964,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7966,a[2]=((C_word)li159),tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_11444,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=t2,tmp=(C_word)a,a+=8,tmp);
/* eval.scm:854: strip */
f_7966(t3,C_slot(((C_word*)t0)[2],C_fix(2)));}

/* strip in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7966(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_7966,2,t1,t2);}
a=C_alloc(6);
t3=C_i_check_list_2(t2,lf[161]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7975,a[2]=t5,a[3]=((C_word)li158),tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_7975(t7,t1,t2);}

/* foldr1523 in strip in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_7975(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_7975,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8006,a[2]=t3,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t6=t4;
t7=C_slot(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8004 in foldr1523 in strip in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8006(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_8006,c,av);}
a=C_alloc(3);
t2=C_i_car(((C_word*)t0)[2]);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(C_truep((C_truep(C_eqp(t2,lf[162]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[163]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[164]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[165]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[86]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[166]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[167]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[168]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[169]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[170]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[99]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[171]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[172]))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,lf[173]))?C_SCHEME_TRUE:C_SCHEME_FALSE)))))))))))))))?t1:C_a_i_cons(&a,2,((C_word*)t0)[2],t1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* scheme#scheme-report-environment in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_8024,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8028,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:860: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[177]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[177]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[175];
tp(4,av2);}}

/* k8026 in scheme#scheme-report-environment in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8028(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8028,c,av);}
switch(((C_word*)t0)[2]){
case C_fix(4):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(5):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
default:
/* eval.scm:865: ##sys#error */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[175];
av2[3]=lf[176];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* scheme#null-environment in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_8049,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8053,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
/* eval.scm:870: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[177]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[177]+1);
av2[1]=t3;
av2[2]=t2;
av2[3]=lf[179];
tp(4,av2);}}

/* k8051 in scheme#null-environment in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8053(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8053,c,av);}
switch(((C_word*)t0)[2]){
case C_fix(4):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
case C_fix(5):
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}
default:
/* eval.scm:875: ##sys#error */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[179];
av2[3]=lf[180];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* loop in chicken.load#provided? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_fcall f_8449(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8449,3,t0,t1,t2);}
a=C_alloc(5);
t3=C_i_nullp(t2);
if(C_truep(t3)){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8471,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:82: pred */
t5=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=C_i_car(t2);
((C_proc)C_fast_retrieve_proc(t5))(3,av2);}}}

/* k8469 in loop in chicken.load#provided? in k10047 in k10038 in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_8471(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8471,c,av);}
if(C_truep(C_i_not(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* mini-srfi-1.scm:83: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_8449(t2,((C_word*)t0)[2],C_u_i_cdr(((C_word*)t0)[4]));}}

/* chicken.load#any in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_8477(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_8477,3,t1,t2,t3);}
a=C_alloc(7);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8483,a[2]=t5,a[3]=t2,a[4]=((C_word)li162),tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_8483(t7,t1,t3);}

/* loop in chicken.load#any in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_8483(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8483,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8493,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:88: pred */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_i_car(t2);
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}}

/* k8491 in loop in chicken.load#any in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_8493(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8493,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* mini-srfi-1.scm:89: loop */
t2=((C_word*)((C_word*)t0)[3])[1];
f_8483(t2,((C_word*)t0)[2],C_u_i_cdr(((C_word*)t0)[4]));}}

/* k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9362(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_9362,2,t0,t1);}
a=C_alloc(3);
t2=C_mutate(&lf[189] /* (set! chicken.load#load-library-extension ...) */,t1);
t3=C_mutate((C_word*)lf[190]+1 /* (set! ##sys#load-dynamic-extension ...) */,lf[185]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9368,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:944: chicken.platform#software-version */
t5=*((C_word*)lf[343]+1);{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9368(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_9368,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9371,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=C_eqp(t1,lf[340]);
if(C_truep(t3)){
t4=t2;
f_9371(t4,lf[341]);}
else{
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11373,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11377,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t6=*((C_word*)lf[234]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_INSTALL_LIB_NAME);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}}

/* k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9371(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,4)))){
C_save_and_reclaim_args((void *)trf_9371,2,t0,t1);}
a=C_alloc(15);
t2=C_mutate(&lf[191] /* (set! chicken.load#default-dynamic-load-libraries ...) */,t1);
t3=C_mutate((C_word*)lf[93]+1 /* (set! ##sys#provide ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9373,a[2]=((C_word)li164),tmp=(C_word)a,a+=3,tmp));
t4=C_mutate((C_word*)lf[192]+1 /* (set! ##sys#provided? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9376,a[2]=((C_word)li165),tmp=(C_word)a,a+=3,tmp));
t5=(C_truep(*((C_word*)lf[193]+1))?lf[194]:lf[195]);
t6=C_mutate(&lf[196] /* (set! chicken.load#path-separators ...) */,t5);
t7=C_mutate(&lf[197] /* (set! chicken.load#path-separator-index/right ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9383,a[2]=((C_word)li167),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate(&lf[198] /* (set! chicken.load#make-relative-pathname ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9418,a[2]=((C_word)li168),tmp=(C_word)a,a+=3,tmp));
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9461,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:979: chicken.base#make-parameter */
t10=*((C_word*)lf[337]+1);{
C_word av2[3];
av2[0]=t10;
av2[1]=t9;
av2[2]=C_i_debug_modep();
((C_proc)(void*)(*((C_word*)t10+1)))(3,av2);}}

/* ##sys#provide in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,1)))){
C_save_and_reclaim((void *)f_9373,c,av);}
a=C_alloc(8);
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_provide(&a,1,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* ##sys#provided? in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9376,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_providedp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* chicken.load#path-separator-index/right in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9383(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_9383,2,t1,t2);}
a=C_alloc(4);
t3=C_block_size(t2);
t4=C_fixnum_difference(t3,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9393,a[2]=t2,a[3]=((C_word)li166),tmp=(C_word)a,a+=4,tmp);
t6=t1;{
C_word av2[2];
av2[0]=t6;
av2[1]=(
  f_9393(t5,t4)
);
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* loop in chicken.load#path-separator-index/right in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static C_word C_fcall f_9393(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_subchar(((C_word*)t0)[2],t1);
t3=lf[196];
if(C_truep(C_u_i_memq(t2,lf[196]))){
return(t1);}
else{
if(C_truep(C_fixnum_lessp(C_fix(0),t1))){
t5=C_fixnum_difference(t1,C_fix(1));
t1=t5;
goto loop;}
else{
return(C_SCHEME_FALSE);}}}

/* chicken.load#make-relative-pathname in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9418(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_9418,3,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9422,a[2]=t1,a[3]=t3,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(C_i_stringp(t2))){
t5=C_block_size(t3);
if(C_truep(C_i_fixnum_positivep(t5))){
t6=C_subchar(t3,C_fix(0));
t7=lf[196];
if(C_truep(C_i_not(C_u_i_memq(t6,lf[196])))){
/* eval.scm:973: path-separator-index/right */
f_9383(t4,t2);}
else{
t8=t4;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
f_9422(2,av2);}}}
else{
t6=t4;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
f_9422(2,av2);}}}
else{
t5=t4;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_FALSE;
f_9422(2,av2);}}}

/* k9420 in chicken.load#make-relative-pathname in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_9422,c,av);}
a=C_alloc(4);
if(C_truep(C_i_not(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9435,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:974: ##sys#substring */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[201]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[201]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_fix(0);
av2[4]=t1;
tp(5,av2);}}}

/* k9433 in k9420 in chicken.load#make-relative-pathname in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9435(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9435,c,av);}
/* eval.scm:974: scheme#string-append */
t2=*((C_word*)lf[199]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[200];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9461(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,5)))){
C_save_and_reclaim((void *)f_9461,c,av);}
a=C_alloc(25);
t2=C_mutate((C_word*)lf[202]+1 /* (set! chicken.load#load-verbose ...) */,t1);
t3=C_set_block_item(lf[203] /* ##sys#current-load-filename */,0,C_SCHEME_FALSE);
t4=C_set_block_item(lf[204] /* ##sys#dload-disabled */,0,C_SCHEME_FALSE);
t5=C_mutate((C_word*)lf[205]+1 /* (set! chicken.load#set-dynamic-load-mode! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9465,a[2]=((C_word)li170),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate(&lf[214] /* (set! chicken.load#c-toplevel ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9565,a[2]=((C_word)li171),tmp=(C_word)a,a+=3,tmp));
t7=*((C_word*)lf[153]+1);
t8=C_mutate(&lf[221] /* (set! chicken.load#load/internal ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9579,a[2]=t7,a[3]=((C_word)li186),tmp=(C_word)a,a+=4,tmp));
t9=C_mutate((C_word*)lf[250]+1 /* (set! scheme#load ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9981,a[2]=((C_word)li187),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[251]+1 /* (set! chicken.load#load-relative ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9996,a[2]=((C_word)li188),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[252]+1 /* (set! chicken.load#load-noisily ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10014,a[2]=((C_word)li192),tmp=(C_word)a,a+=3,tmp));
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_10040,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_mk_bool(C_USES_SONAME))){
t13=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_11361,a[2]=t12,tmp=(C_word)a,a+=3,tmp);
t14=C_fix((C_word)C_BINARY_VERSION);
/* ##sys#fixnum->string */
t15=*((C_word*)lf[339]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t15;
av2[1]=t13;
av2[2]=t14;
av2[3]=C_fix(10);
((C_proc)(void*)(*((C_word*)t15+1)))(4,av2);}}
else{
t13=t12;{
C_word *av2=av;
av2[0]=t13;
av2[1]=lf[189];
f_10040(2,av2);}}}

/* chicken.load#set-dynamic-load-mode! in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9465(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_9465,c,av);}
a=C_alloc(20);
t3=C_i_pairp(t2);
t4=(C_truep(t3)?t2:C_a_i_list1(&a,1,t2));
t5=C_SCHEME_FALSE;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_SCHEME_TRUE;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9472,a[2]=t1,a[3]=t6,a[4]=t8,tmp=(C_word)a,a+=5,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9477,a[2]=t11,a[3]=t8,a[4]=t6,a[5]=((C_word)li169),tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_9477(t13,t9,t4);}

/* k9470 in chicken.load#set-dynamic-load-mode! in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9472(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9472,c,av);}
/* eval.scm:999: ##sys#set-dlopen-flags! */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[206]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[206]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)((C_word*)t0)[4])[1];
tp(4,av2);}}

/* loop in chicken.load#set-dynamic-load-mode! in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_fcall f_9477(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
loop:
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_9477,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9490,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=C_eqp(t3,lf[207]);
if(C_truep(t5)){
t6=C_set_block_item(((C_word*)t0)[3],0,C_SCHEME_TRUE);
/* eval.scm:998: loop */
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t6=C_eqp(t3,lf[208]);
if(C_truep(t6)){
t7=C_set_block_item(((C_word*)t0)[3],0,C_SCHEME_FALSE);
/* eval.scm:998: loop */
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t7=C_eqp(t3,lf[209]);
if(C_truep(t7)){
t8=C_set_block_item(((C_word*)t0)[4],0,C_SCHEME_FALSE);
/* eval.scm:998: loop */
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
t8=C_eqp(t3,lf[210]);
if(C_truep(t8)){
t9=C_set_block_item(((C_word*)t0)[4],0,C_SCHEME_TRUE);
/* eval.scm:998: loop */
t11=t1;
t12=C_slot(t2,C_fix(1));
t1=t11;
t2=t12;
goto loop;}
else{
/* eval.scm:997: ##sys#signal-hook */
t9=*((C_word*)lf[211]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t4;
av2[2]=lf[212];
av2[3]=lf[213];
av2[4]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}}}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9488 in loop in chicken.load#set-dynamic-load-mode! in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_9490(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9490,c,av);}
/* eval.scm:998: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_9477(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k9557 in chicken.load#c-toplevel in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9559,c,av);}
/* eval.scm:1004: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[219];
tp(4,av2);}}

/* chicken.load#c-toplevel in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9565(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_9565,3,t1,t2,t3);}
a=C_alloc(10);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9573,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9577,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_not(t2))){
/* eval.scm:1009: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t4;
av2[2]=lf[217];
av2[3]=lf[218];
tp(4,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9559,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1005: chicken.internal#string->c-identifier */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[220]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[220]+1);
av2[1]=t6;
av2[2]=C_slot(t2,C_fix(1));
tp(3,av2);}}}

/* k9571 in chicken.load#c-toplevel in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9573,c,av);}
/* eval.scm:1009: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[215]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[215]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}

/* k9575 in chicken.load#c-toplevel in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9577,c,av);}
/* eval.scm:1009: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[217];
av2[3]=t1;
tp(4,av2);}}

/* chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_fcall f_9579(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(23,0,3)))){
C_save_and_reclaim_args((void *)trf_9579,5,t0,t1,t2,t3,t4);}
a=C_alloc(23);
t5=C_i_nullp(t4);
t6=(C_truep(t5)?C_SCHEME_FALSE:C_i_car(t4));
t7=C_i_nullp(t4);
t8=(C_truep(t7)?C_SCHEME_END_OF_LIST:C_i_cdr(t4));
t9=C_i_nullp(t8);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_i_car(t8));
t11=C_i_nullp(t8);
t12=(C_truep(t11)?C_SCHEME_END_OF_LIST:C_i_cdr(t8));
t13=C_i_nullp(t12);
t14=(C_truep(t13)?C_SCHEME_FALSE:C_i_car(t12));
t15=C_i_nullp(t12);
t16=(C_truep(t15)?C_SCHEME_END_OF_LIST:C_i_cdr(t12));
t17=C_i_nullp(t16);
t18=(C_truep(t17)?C_SCHEME_FALSE:C_i_car(t16));
t19=C_i_nullp(t16);
t20=(C_truep(t19)?C_SCHEME_END_OF_LIST:C_i_cdr(t16));
t21=C_SCHEME_UNDEFINED;
t22=(*a=C_VECTOR_TYPE|1,a[1]=t21,tmp=(C_word)a,a+=2,tmp);
t23=C_SCHEME_UNDEFINED;
t24=(*a=C_VECTOR_TYPE|1,a[1]=t23,tmp=(C_word)a,a+=2,tmp);
t25=C_SCHEME_UNDEFINED;
t26=(*a=C_VECTOR_TYPE|1,a[1]=t25,tmp=(C_word)a,a+=2,tmp);
t27=C_SCHEME_UNDEFINED;
t28=(*a=C_VECTOR_TYPE|1,a[1]=t27,tmp=(C_word)a,a+=2,tmp);
t29=(C_truep(t3)?t3:((C_word*)t0)[2]);
t30=C_set_block_item(t22,0,t29);
t31=C_set_block_item(t24,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9622,a[2]=t18,a[3]=((C_word)li172),tmp=(C_word)a,a+=4,tmp));
t32=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9657,a[2]=t26,a[3]=t28,a[4]=t1,a[5]=t10,a[6]=t22,a[7]=t6,a[8]=t14,a[9]=t2,a[10]=t24,tmp=(C_word)a,a+=11,tmp);
if(C_truep(C_i_not(*((C_word*)lf[204]+1)))){
/* eval.scm:1039: chicken.platform#feature? */
t33=*((C_word*)lf[248]+1);{
C_word av2[3];
av2[0]=t33;
av2[1]=t32;
av2[2]=lf[249];
((C_proc)(void*)(*((C_word*)t33+1)))(3,av2);}}
else{
t33=t32;{
C_word av2[2];
av2[0]=t33;
av2[1]=C_SCHEME_FALSE;
f_9657(2,av2);}}}

/* k9615 */
static void C_ccall f_9617(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9617,c,av);}
if(C_truep(t1)){
/* eval.scm:1032: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[215]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[215]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[223];
tp(4,av2);}}
else{
/* eval.scm:1029: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=lf[224];
av2[3]=((C_word*)t0)[3];
tp(4,av2);}}}

/* f_9622 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_fcall f_9622(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,2)))){
C_save_and_reclaim_args((void *)trf_9622,3,t0,t1,t2);}
a=C_alloc(12);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9626,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9653,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9617,a[2]=t3,a[3]=t2,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1027: path-separator-index/right */
f_9383(t5,t2);}

/* k9624 */
static void C_ccall f_9626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_9626,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9629,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9649,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1033: c-toplevel */
f_9565(t3,((C_word*)t0)[3],lf[223]);}

/* k9627 in k9624 */
static void C_ccall f_9629(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_9629,c,av);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_i_symbolp(((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9645,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1035: c-toplevel */
f_9565(t2,C_SCHEME_FALSE,lf[223]);}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}}

/* k9643 in k9627 in k9624 */
static void C_ccall f_9645(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9645,c,av);}
/* eval.scm:1035: ##sys#dload */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[222]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[222]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k9647 in k9624 */
static void C_ccall f_9649(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9649,c,av);}
/* eval.scm:1033: ##sys#dload */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[222]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[222]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
tp(4,av2);}}

/* k9651 */
static void C_ccall f_9653(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9653,c,av);}
/* eval.scm:1032: ##sys#make-c-string */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[215]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[215]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[223];
tp(4,av2);}}

/* k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 in ... */
static void C_ccall f_9657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_9657,c,av);}
a=C_alloc(16);
t2=C_set_block_item(((C_word*)t0)[2],0,t1);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9661,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[2],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9876,a[2]=t3,a[3]=((C_word*)t0)[9],a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1042: chicken.base#port? */
t5=*((C_word*)lf[247]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[9];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_9661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_9661,c,av);}
a=C_alloc(15);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_9664,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9873,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1053: load-verbose */
t5=*((C_word*)lf[202]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_9664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_9664,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9667,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
if(C_truep(((C_word*)((C_word*)t0)[9])[1])){
/* eval.scm:1059: dload */
t3=((C_word*)((C_word*)t0)[10])[1];
f_9622(t3,t2,((C_word*)((C_word*)t0)[3])[1]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_9667(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_9667(2,av2);}}}

/* k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_9667(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_9667,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9670,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9675,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word)li185),tmp=(C_word)a,a+=9,tmp);
t4=t3;
f_9675(t4,t2);}}

/* k9668 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_9670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9670,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_fcall f_9675(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(39,0,4)))){
C_save_and_reclaim_args((void *)trf_9675,2,t0,t1);}
a=C_alloc(39);
t2=C_SCHEME_TRUE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=((C_word*)((C_word*)t0)[2])[1];
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)((C_word*)t0)[2])[1];
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_FALSE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_FALSE;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_FALSE;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9681,a[2]=t9,a[3]=t11,a[4]=t13,a[5]=t3,a[6]=t5,a[7]=t7,a[8]=((C_word)li173),tmp=(C_word)a,a+=9,tmp);
t15=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9690,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[7],a[8]=((C_word)li183),tmp=(C_word)a,a+=9,tmp);
t16=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9842,a[2]=t3,a[3]=t5,a[4]=t7,a[5]=t9,a[6]=t11,a[7]=t13,a[8]=((C_word)li184),tmp=(C_word)a,a+=9,tmp);
/* eval.scm:1062: ##sys#dynamic-wind */
t17=*((C_word*)lf[47]+1);{
C_word av2[5];
av2[0]=t17;
av2[1]=t1;
av2[2]=t14;
av2[3]=t15;
av2[4]=t16;
((C_proc)(void*)(*((C_word*)t17+1)))(5,av2);}}

/* a9680 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_9681(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9681,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[225]+1));
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,*((C_word*)lf[203]+1));
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,*((C_word*)lf[226]+1));
t5=C_mutate((C_word*)lf[225]+1 /* (set! ##sys#read-error-with-line-number ...) */,((C_word*)((C_word*)t0)[5])[1]);
t6=C_mutate((C_word*)lf[203]+1 /* (set! ##sys#current-load-filename ...) */,((C_word*)((C_word*)t0)[6])[1]);
t7=C_mutate((C_word*)lf[226]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[7])[1]);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_9690(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_9690,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9694,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)((C_word*)t0)[6])[1])){
/* eval.scm:1065: open-input-file */
t3=*((C_word*)lf[237]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[6])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[7];
f_9694(2,av2);}}}

/* k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_9694(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_9694,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9699,a[2]=((C_word)li174),tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9702,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word)li181),tmp=(C_word)a,a+=9,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9833,a[2]=t1,a[3]=((C_word)li182),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1066: ##sys#dynamic-wind */
t5=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[7];
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* a9698 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_9699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9699,c,av);}
t2=t1;{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_9702(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_9702,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9706,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t1,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* eval.scm:1069: scheme#peek-char */
t3=*((C_word*)lf[235]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in ... */
static void C_ccall f_9706(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_9706,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9709,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_eqp(t1,C_make_character(127));
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9824,a[2]=t2,a[3]=((C_word*)t0)[8],tmp=(C_word)a,a+=4,tmp);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9828,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
/* ##sys#peek-c-string */
t6=*((C_word*)lf[234]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_mpointer(&a,(void*)C_dlerror);
av2[3]=C_fix(0);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_9709(2,av2);}}}

/* k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in ... */
static void C_ccall f_9709(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_9709,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_9712,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* eval.scm:1077: read */
t3=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in ... */
static void C_ccall f_9712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_9712,c,av);}
a=C_alloc(11);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9717,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word)li180),tmp=(C_word)a,a+=9,tmp));
t5=((C_word*)t3)[1];
f_9717(t5,((C_word*)t0)[7],t1);}

/* doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in ... */
static void C_fcall f_9717(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_9717,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_eofp(t2))){
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_9727,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t2,a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp);
if(C_truep(((C_word*)t0)[7])){
/* eval.scm:1080: printer */
t4=((C_word*)t0)[7];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}
else{
t4=t3;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_9727(2,av2);}}}}

/* k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in ... */
static void C_ccall f_9727(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_9727,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9730,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_9739,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[7],a[5]=((C_word)li177),tmp=(C_word)a,a+=6,tmp);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9773,a[2]=((C_word*)t0)[8],a[3]=((C_word)li179),tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1081: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}

/* k9728 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in ... */
static void C_ccall f_9730(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9730,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9737,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1078: read */
t3=*((C_word*)lf[227]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9735 in k9728 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in ... */
static void C_ccall f_9737(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9737,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9717(t2,((C_word*)t0)[3],t1);}

/* a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in ... */
static void C_ccall f_9739(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_9739,c,av);}
a=C_alloc(5);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9746,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1084: ##sys#start-timer */
t3=*((C_word*)lf[230]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* eval.scm:1085: evalproc */
t2=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[4];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}}

/* k9744 in a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in ... */
static void C_ccall f_9746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_9746,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9751,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word)li175),tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9757,a[2]=((C_word)li176),tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1084: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[4];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a9750 in k9744 in a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in ... */
static void C_ccall f_9751(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9751,c,av);}
/* eval.scm:1084: evalproc */
t2=((C_word*)((C_word*)t0)[2])[1];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[3];
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* a9756 in k9744 in a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in ... */
static void C_ccall f_9757(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +7,c,2)))){
C_save_and_reclaim((void*)f_9757,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+7);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9761,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9768,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1084: ##sys#stop-timer */
t5=*((C_word*)lf[229]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k9759 in a9756 in k9744 in a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in ... */
static void C_ccall f_9761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9761,c,av);}{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
C_apply_values(3,av2);}}

/* k9766 in a9756 in k9744 in a9738 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in ... */
static void C_ccall f_9768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9768,c,av);}
/* eval.scm:1084: ##sys#display-times */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[228]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[228]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
tp(3,av2);}}

/* a9772 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in ... */
static void C_ccall f_9773(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +6,c,3)))){
C_save_and_reclaim((void*)f_9773,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+6);
t2=C_build_rest(&a,c,2,av);
C_word t3;
C_word t4;
C_word t5;
C_word t6;
if(C_truep(((C_word*)t0)[2])){
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9790,a[2]=t4,a[3]=((C_word)li178),tmp=(C_word)a,a+=4,tmp));
t6=((C_word*)t4)[1];
f_9790(t6,t1,t2);}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9780 in for-each-loop2307 in a9772 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in ... */
static void C_ccall f_9782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9782,c,av);}
/* eval.scm:1091: newline */
t2=*((C_word*)lf[231]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop2307 in a9772 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in ... */
static void C_fcall f_9790(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_9790,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9800,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9782,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1090: write */
t6=*((C_word*)lf[14]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k9798 in for-each-loop2307 in a9772 in k9725 in doloop2297 in k9710 in k9707 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in ... */
static void C_ccall f_9800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9800,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_9790(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k9822 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in ... */
static void C_ccall f_9824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9824,c,av);}
/* eval.scm:1071: ##sys#error */
t2=*((C_word*)lf[17]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[223];
av2[3]=t1;
av2[4]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k9826 in k9704 in a9701 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in ... */
static void C_ccall f_9828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_9828,c,av);}
if(C_truep(t1)){
/* eval.scm:1073: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[232];
av2[3]=t1;
tp(4,av2);}}
else{
/* eval.scm:1073: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=lf[232];
av2[3]=lf[233];
tp(4,av2);}}}

/* a9832 in k9692 in a9689 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in ... */
static void C_ccall f_9833(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_9833,c,av);}
/* eval.scm:1094: close-input-port */
t2=*((C_word*)lf[236]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a9841 in a9674 in k9665 in k9662 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_9842(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=2) C_bad_argc_2(c,2,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9842,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[225]+1));
t3=C_mutate(((C_word *)((C_word*)t0)[3])+1,*((C_word*)lf[203]+1));
t4=C_mutate(((C_word *)((C_word*)t0)[4])+1,*((C_word*)lf[226]+1));
t5=C_mutate((C_word*)lf[225]+1 /* (set! ##sys#read-error-with-line-number ...) */,((C_word*)((C_word*)t0)[5])[1]);
t6=C_mutate((C_word*)lf[203]+1 /* (set! ##sys#current-load-filename ...) */,((C_word*)((C_word*)t0)[6])[1]);
t7=C_mutate((C_word*)lf[226]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[7])[1]);
t8=t1;{
C_word *av2=av;
av2[0]=t8;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t8+1)))(2,av2);}}

/* k9859 in k9871 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_9861(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9861,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9864,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1055: display */
t3=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9862 in k9859 in k9871 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_9864(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_9864,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_9867,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* eval.scm:1056: display */
t3=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[240];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k9865 in k9862 in k9859 in k9871 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_9867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9867,c,av);}
/* eval.scm:1057: chicken.base#flush-output */
t2=*((C_word*)lf[238]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k9871 in k9659 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_9873(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_9873,c,av);}
a=C_alloc(4);
t2=(C_truep(t1)?((C_word*)((C_word*)t0)[2])[1]:C_SCHEME_FALSE);
if(C_truep(t2)){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9861,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1054: display */
t4=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[241];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_9664(2,av2);}}}

/* k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in ... */
static void C_ccall f_9876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_9876,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_9661(2,av2);}}
else{
t2=C_i_stringp(((C_word*)t0)[3]);
if(C_truep(C_i_not(t2))){
/* eval.scm:1044: ##sys#signal-hook */
t3=*((C_word*)lf[211]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[242];
av2[3]=lf[223];
av2[4]=lf[243];
av2[5]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9891,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1045: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[246]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[246]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[223];
tp(6,av2);}}}}

/* k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in ... */
static void C_ccall f_9891(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_9891,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
f_9661(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9894,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1046: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=*((C_word*)lf[190]+1);
tp(4,av2);}}}

/* k9892 in k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in ... */
static void C_ccall f_9894(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_9894,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9897,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[4])[1])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9921,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1047: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[246]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[246]+1);
av2[1]=t3;
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[223];
tp(6,av2);}}
else{
t3=t2;
f_9897(t3,C_SCHEME_FALSE);}}

/* k9895 in k9892 in k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_fcall f_9897(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_9897,2,t0,t1);}
a=C_alloc(4);
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word av2[2];
av2[0]=t2;
av2[1]=t1;
f_9661(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_9903,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1048: ##sys#string-append */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[216]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[216]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=lf[187];
tp(4,av2);}}}

/* k9901 in k9895 in k9892 in k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in ... */
static void C_ccall f_9903(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,5)))){
C_save_and_reclaim((void *)f_9903,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_9915,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* eval.scm:1049: ##sys#file-exists? */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[246]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[246]+1);
av2[1]=t2;
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=lf[223];
tp(6,av2);}}

/* k9913 in k9901 in k9895 in k9892 in k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in ... */
static void C_ccall f_9915(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_9915,c,av);}
if(C_truep(t1)){
if(C_truep(((C_word*)t0)[2])){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
f_9661(2,av2);}}
else{
/* eval.scm:1051: ##sys#signal-hook */
t2=*((C_word*)lf[211]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[244];
av2[3]=lf[223];
av2[4]=lf[245];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}
else{
/* eval.scm:1051: ##sys#signal-hook */
t2=*((C_word*)lf[211]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[244];
av2[3]=lf[223];
av2[4]=lf[245];
av2[5]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* k9919 in k9892 in k9889 in k9874 in k9655 in chicken.load#load/internal in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in ... */
static void C_ccall f_9921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_9921,c,av);}
t2=((C_word*)t0)[2];
f_9897(t2,(C_truep(t1)?((C_word*)t0)[3]:C_SCHEME_FALSE));}

/* scheme#load in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9981(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_9981,c,av);}
if(C_truep(C_rest_nullp(c,3))){
/* eval.scm:1099: load/internal */
t3=lf[221];
f_9579(t3,t1,t2,C_SCHEME_FALSE,C_SCHEME_END_OF_LIST);}
else{
/* eval.scm:1099: load/internal */
t3=lf[221];
f_9579(t3,t1,t2,C_get_rest_arg(c,3,av,3,t0),C_SCHEME_END_OF_LIST);}}

/* chicken.load#load-relative in k9459 in k9369 in k9366 in k9360 in k11418 in k11426 in k11434 in k11442 in k7962 in k7959 in k7956 in k7953 in k7950 in k7859 in k7840 in k3568 in k3524 in k3521 in k3518 in k3515 */
static void C_ccall f_9996(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
C_check_for_interrupt;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_9996,c,av);}
a=C_alloc(4);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_10003,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* eval.scm:1102: make-relative-pathname */
f_9418(t5,*((C_word*)lf[203]+1),t2);}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_eval_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("eval"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_eval_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(2430))){
C_save(t1);
C_rereclaim2(2430*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,370);
lf[0]=C_h_intern(&lf[0],4, C_text("eval"));
lf[1]=C_h_intern(&lf[1],13, C_text("chicken.eval#"));
lf[2]=C_h_intern(&lf[2],21, C_text("##sys#unbound-in-eval"));
lf[3]=C_h_intern(&lf[3],22, C_text("##sys#eval-debug-level"));
lf[5]=C_h_intern(&lf[5],9, C_text("##sys#get"));
lf[6]=C_h_intern(&lf[6],18, C_text("##core#macro-alias"));
lf[7]=C_h_intern(&lf[7],13, C_text("scheme#values"));
lf[8]=C_h_intern(&lf[8],14, C_text("scheme#symbol\077"));
lf[9]=C_h_intern(&lf[9],25, C_text("##sys#current-environment"));
lf[10]=C_h_intern(&lf[10],9, C_text("frameinfo"));
lf[11]=C_h_intern(&lf[11],20, C_text("##sys#current-thread"));
lf[12]=C_h_intern(&lf[12],22, C_text("##sys#make-lambda-info"));
lf[13]=C_h_intern(&lf[13],30, C_text("chicken.base#get-output-string"));
lf[14]=C_h_intern(&lf[14],12, C_text("scheme#write"));
lf[15]=C_h_intern(&lf[15],31, C_text("chicken.base#open-output-string"));
lf[16]=C_h_intern(&lf[16],21, C_text("##sys#decorate-lambda"));
lf[17]=C_h_intern(&lf[17],11, C_text("##sys#error"));
lf[18]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020unbound variable"));
lf[19]=C_h_intern(&lf[19],34, C_text("##sys#symbol-has-toplevel-binding\077"));
lf[20]=C_h_intern(&lf[20],23, C_text("##sys#alias-global-hook"));
lf[21]=C_h_intern(&lf[21],26, C_text("##sys#syntax-error/context"));
lf[22]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031illegal non-atomic object"));
lf[23]=C_h_intern(&lf[23],12, C_text("##core#quote"));
lf[24]=C_h_intern(&lf[24],27, C_text("chicken.syntax#strip-syntax"));
lf[25]=C_h_intern(&lf[25],13, C_text("##core#syntax"));
lf[26]=C_h_intern(&lf[26],12, C_text("##core#check"));
lf[27]=C_h_intern(&lf[27],16, C_text("##core#immutable"));
lf[28]=C_h_intern(&lf[28],16, C_text("##core#undefined"));
lf[29]=C_h_intern(&lf[29],9, C_text("##core#if"));
lf[30]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[31]=C_h_intern(&lf[31],12, C_text("##core#begin"));
lf[32]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[33]=C_h_intern(&lf[33],33, C_text("##core#ensure-toplevel-definition"));
lf[34]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[35]=C_decode_literal(C_heaptop,C_text("\376B\000\0008toplevel definition in non-toplevel context for variable"));
lf[36]=C_h_intern(&lf[36],11, C_text("##core#set!"));
lf[37]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032environment is not mutable"));
lf[38]=C_h_intern(&lf[38],21, C_text("##sys#notices-enabled"));
lf[39]=C_h_intern(&lf[39],12, C_text("##sys#notice"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376B\000\000$assignment to imported value binding"));
lf[41]=C_h_intern(&lf[41],10, C_text("##core#let"));
lf[42]=C_h_intern(&lf[42],3, C_text("map"));
lf[43]=C_h_intern(&lf[43],12, C_text("scheme#cadar"));
lf[44]=C_h_intern(&lf[44],13, C_text("scheme#cadadr"));
lf[45]=C_h_intern(&lf[45],17, C_text("##sys#make-vector"));
lf[46]=C_h_intern(&lf[46],23, C_text("##sys#canonicalize-body"));
lf[47]=C_h_intern(&lf[47],18, C_text("##sys#dynamic-wind"));
lf[48]=C_h_intern(&lf[48],15, C_text("##sys#extend-se"));
lf[49]=C_h_intern(&lf[49],19, C_text("chicken.base#gensym"));
lf[50]=C_h_intern(&lf[50],14, C_text("##core#letrec\052"));
lf[51]=C_h_intern(&lf[51],12, C_text("##sys#append"));
lf[52]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[53]=C_h_intern(&lf[53],13, C_text("##core#letrec"));
lf[54]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[55]=C_h_intern(&lf[55],13, C_text("##core#lambda"));
lf[56]=C_h_intern(&lf[56],1, C_text("\077"));
lf[57]=C_h_intern(&lf[57],12, C_text("##sys#vector"));
lf[58]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022bad argument count"));
lf[59]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022bad argument count"));
lf[60]=C_h_intern(&lf[60],27, C_text("##sys#decompose-lambda-list"));
lf[61]=C_h_intern(&lf[61],33, C_text("##sys#expand-extended-lambda-list"));
lf[62]=C_h_intern(&lf[62],23, C_text("##sys#syntax-error-hook"));
lf[63]=C_h_intern(&lf[63],27, C_text("##sys#extended-lambda-list\077"));
lf[64]=C_h_intern(&lf[64],18, C_text("##sys#check-syntax"));
lf[65]=C_h_intern(&lf[65],6, C_text("lambda"));
lf[66]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\001\001_\376\003\000\000\002\376\001\000\000\013\001lambda-list\376\000\000\000\002\376\001\000\000\001\001_\376\377\001\000\000\000\001"));
lf[67]=C_h_intern(&lf[67],17, C_text("##core#let-syntax"));
lf[68]=C_h_intern(&lf[68],13, C_text("scheme#append"));
lf[69]=C_h_intern(&lf[69],24, C_text("##sys#ensure-transformer"));
lf[70]=C_h_intern(&lf[70],15, C_text("##sys#eval/meta"));
lf[71]=C_h_intern(&lf[71],20, C_text("##core#letrec-syntax"));
lf[72]=C_h_intern(&lf[72],8, C_text("for-each"));
lf[73]=C_h_intern(&lf[73],20, C_text("##core#define-syntax"));
lf[74]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[75]=C_h_intern(&lf[75],30, C_text("##sys#extend-macro-environment"));
lf[76]=C_h_intern(&lf[76],28, C_text("##sys#register-syntax-export"));
lf[77]=C_h_intern(&lf[77],20, C_text("##sys#current-module"));
lf[78]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032environment is not mutable"));
lf[79]=C_h_intern(&lf[79],29, C_text("##core#define-compiler-syntax"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[81]=C_h_intern(&lf[81],26, C_text("##core#let-compiler-syntax"));
lf[82]=C_h_intern(&lf[82],14, C_text("##core#include"));
lf[83]=C_h_intern(&lf[83],29, C_text("##sys#include-forms-from-file"));
lf[84]=C_h_intern(&lf[84],23, C_text("##core#let-module-alias"));
lf[85]=C_h_intern(&lf[85],25, C_text("##sys#with-module-aliases"));
lf[86]=C_h_intern(&lf[86],7, C_text("functor"));
lf[87]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\006\001symbol\376\003\000\000\002\376\001\000\000\006\001symbol\376\377\016"));
lf[88]=C_h_intern(&lf[88],13, C_text("##core#module"));
lf[89]=C_h_intern(&lf[89],23, C_text("##sys#macro-environment"));
lf[90]=C_h_intern(&lf[90],30, C_text("##sys#module-alias-environment"));
lf[91]=C_h_intern(&lf[91],31, C_text("##sys#initial-macro-environment"));
lf[92]=C_h_intern(&lf[92],21, C_text("##sys#undefined-value"));
lf[93]=C_h_intern(&lf[93],13, C_text("##sys#provide"));
lf[94]=C_h_intern(&lf[94],35, C_text("chicken.internal#module-requirement"));
lf[95]=C_h_intern(&lf[95],21, C_text("##sys#finalize-module"));
lf[96]=C_h_intern(&lf[96],14, C_text("scheme#reverse"));
lf[97]=C_h_intern(&lf[97],27, C_text("##sys#with-property-restore"));
lf[98]=C_h_intern(&lf[98],21, C_text("##sys#register-module"));
lf[99]=C_h_intern(&lf[99],6, C_text("module"));
lf[100]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031modules may not be nested"));
lf[101]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025invalid export syntax"));
lf[102]=C_h_intern(&lf[102],18, C_text("##core#loop-lambda"));
lf[103]=C_h_intern(&lf[103],14, C_text("##core#provide"));
lf[104]=C_h_intern(&lf[104],25, C_text("##core#require-for-syntax"));
lf[105]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[106]=C_h_intern(&lf[106],27, C_text("chicken.load#load-extension"));
lf[107]=C_h_intern(&lf[107],7, C_text("require"));
lf[108]=C_h_intern(&lf[108],14, C_text("##core#require"));
lf[109]=C_h_intern(&lf[109],21, C_text("##sys#process-require"));
lf[110]=C_h_intern(&lf[110],26, C_text("##core#elaborationtimeonly"));
lf[111]=C_h_intern(&lf[111],25, C_text("##core#elaborationtimetoo"));
lf[112]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[113]=C_h_intern(&lf[113],21, C_text("##core#compiletimetoo"));
lf[114]=C_h_intern(&lf[114],22, C_text("##core#compiletimeonly"));
lf[115]=C_h_intern(&lf[115],15, C_text("##core#callunit"));
lf[116]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[117]=C_h_intern(&lf[117],14, C_text("##core#declare"));
lf[118]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[119]=C_decode_literal(C_heaptop,C_text("\376B\000\000,declarations are ignored in interpreted code"));
lf[120]=C_h_intern(&lf[120],20, C_text("##core#define-inline"));
lf[121]=C_h_intern(&lf[121],22, C_text("##core#define-constant"));
lf[122]=C_h_intern(&lf[122],6, C_text("define"));
lf[123]=C_h_intern(&lf[123],16, C_text("##core#primitive"));
lf[124]=C_decode_literal(C_heaptop,C_text("\376B\000\000%cannot evaluate compiler-special-form"));
lf[125]=C_h_intern(&lf[125],10, C_text("##core#app"));
lf[126]=C_h_intern(&lf[126],10, C_text("##core#the"));
lf[127]=C_h_intern(&lf[127],15, C_text("##core#typecase"));
lf[128]=C_h_intern(&lf[128],4, C_text("else"));
lf[129]=C_h_intern(&lf[129],17, C_text("compiler-typecase"));
lf[130]=C_decode_literal(C_heaptop,C_text("\376B\000\0007no `else-clause\047 in unresolved `compiler-typecase\047 form"));
lf[131]=C_h_intern(&lf[131],20, C_text("##sys#syntax-context"));
lf[132]=C_h_intern(&lf[132],13, C_text("##core#inline"));
lf[133]=C_h_intern(&lf[133],22, C_text("##core#inline_allocate"));
lf[134]=C_h_intern(&lf[134],21, C_text("##core#foreign-lambda"));
lf[135]=C_h_intern(&lf[135],30, C_text("##core#define-foreign-variable"));
lf[136]=C_h_intern(&lf[136],31, C_text("##core#define-external-variable"));
lf[137]=C_h_intern(&lf[137],19, C_text("##core#let-location"));
lf[138]=C_h_intern(&lf[138],24, C_text("##core#foreign-primitive"));
lf[139]=C_h_intern(&lf[139],15, C_text("##core#location"));
lf[140]=C_h_intern(&lf[140],22, C_text("##core#foreign-lambda\052"));
lf[141]=C_h_intern(&lf[141],26, C_text("##core#define-foreign-type"));
lf[142]=C_h_intern(&lf[142],21, C_text("chicken.syntax#expand"));
lf[143]=C_h_intern(&lf[143],20, C_text("##sys#srfi-4-vector\077"));
lf[144]=C_h_intern(&lf[144],18, C_text("chicken.blob#blob\077"));
lf[145]=C_h_intern(&lf[145],13, C_text("##sys#number\077"));
lf[146]=C_h_intern(&lf[146],24, C_text("chicken.keyword#keyword\077"));
lf[147]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024malformed expression"));
lf[148]=C_h_intern(&lf[148],28, C_text("##sys#meta-macro-environment"));
lf[149]=C_h_intern(&lf[149],30, C_text("##sys#current-meta-environment"));
lf[150]=C_h_intern(&lf[150],19, C_text("scheme#dynamic-wind"));
lf[151]=C_h_intern(&lf[151],17, C_text("chicken.base#void"));
lf[152]=C_h_intern(&lf[152],25, C_text("chicken.eval#eval-handler"));
lf[153]=C_h_intern(&lf[153],11, C_text("scheme#eval"));
lf[154]=C_h_intern(&lf[154],31, C_text("chicken.eval#module-environment"));
lf[155]=C_h_intern(&lf[155],33, C_text("chicken.module#module-environment"));
lf[156]=C_h_intern(&lf[156],26, C_text("##sys#syntax-error-culprit"));
lf[157]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032illegal lambda-list syntax"));
lf[158]=C_h_intern(&lf[158],11, C_text("environment"));
lf[159]=C_h_intern(&lf[159],23, C_text("interaction-environment"));
lf[160]=C_h_intern(&lf[160],30, C_text("scheme#interaction-environment"));
lf[161]=C_h_intern(&lf[161],5, C_text("foldr"));
lf[162]=C_h_intern(&lf[162],11, C_text("cond-expand"));
lf[163]=C_h_intern(&lf[163],16, C_text("define-interface"));
lf[164]=C_h_intern(&lf[164],11, C_text("delay-force"));
lf[165]=C_h_intern(&lf[165],6, C_text("export"));
lf[166]=C_h_intern(&lf[166],6, C_text("import"));
lf[167]=C_h_intern(&lf[167],17, C_text("import-for-syntax"));
lf[168]=C_h_intern(&lf[168],13, C_text("import-syntax"));
lf[169]=C_h_intern(&lf[169],24, C_text("import-syntax-for-syntax"));
lf[170]=C_h_intern(&lf[170],7, C_text("letrec\052"));
lf[171]=C_h_intern(&lf[171],8, C_text("reexport"));
lf[172]=C_h_intern(&lf[172],15, C_text("require-library"));
lf[173]=C_h_intern(&lf[173],6, C_text("syntax"));
lf[174]=C_h_intern(&lf[174],32, C_text("scheme#scheme-report-environment"));
lf[175]=C_h_intern(&lf[175],25, C_text("scheme-report-environment"));
lf[176]=C_decode_literal(C_heaptop,C_text("\376B\000\000-unsupported scheme report environment version"));
lf[177]=C_h_intern(&lf[177],18, C_text("##sys#check-fixnum"));
lf[178]=C_h_intern(&lf[178],23, C_text("scheme#null-environment"));
lf[179]=C_h_intern(&lf[179],16, C_text("null-environment"));
lf[180]=C_decode_literal(C_heaptop,C_text("\376B\000\000$unsupported null environment version"));
lf[181]=C_h_intern(&lf[181],13, C_text("chicken.load#"));
lf[184]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\016\001chicken-syntax\376\003\000\000\002\376\001\000\000\022\001chicken-ffi-syntax\376\003\000\000\002\376\001\000\000\014\001continuation\376\003\000"
"\000\002\376\001\000\000\017\001data-structures\376\003\000\000\002\376\001\000\000\017\001debugger-client\376\003\000\000\002\376\001\000\000\004\001eval\376\003\000\000\002\376\001\000\000\014\001eval-"
"modules\376\003\000\000\002\376\001\000\000\006\001expand\376\003\000\000\002\376\001\000\000\006\001extras\376\003\000\000\002\376\001\000\000\004\001file\376\003\000\000\002\376\001\000\000\010\001internal\376\003\000\000\002"
"\376\001\000\000\007\001irregex\376\003\000\000\002\376\001\000\000\007\001library\376\003\000\000\002\376\001\000\000\007\001lolevel\376\003\000\000\002\376\001\000\000\010\001pathname\376\003\000\000\002\376\001\000\000\004\001p"
"ort\376\003\000\000\002\376\001\000\000\005\001posix\376\003\000\000\002\376\001\000\000\010\001profiler\376\003\000\000\002\376\001\000\000\013\001read-syntax\376\003\000\000\002\376\001\000\000\004\001repl\376\003\000\000\002"
"\376\001\000\000\011\001scheduler\376\003\000\000\002\376\001\000\000\006\001srfi-4\376\003\000\000\002\376\001\000\000\003\001tcp\376\377\016"));
lf[186]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003.so"));
lf[188]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004.scm"));
lf[190]=C_h_intern(&lf[190],28, C_text("##sys#load-dynamic-extension"));
lf[192]=C_h_intern(&lf[192],15, C_text("##sys#provided\077"));
lf[193]=C_h_intern(&lf[193],22, C_text("##sys#windows-platform"));
lf[194]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000\134\376\003\000\000\002\376\377\012\000\000/\376\377\016"));
lf[195]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\377\012\000\000/\376\377\016"));
lf[199]=C_h_intern(&lf[199],20, C_text("scheme#string-append"));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[201]=C_h_intern(&lf[201],15, C_text("##sys#substring"));
lf[202]=C_h_intern(&lf[202],25, C_text("chicken.load#load-verbose"));
lf[203]=C_h_intern(&lf[203],27, C_text("##sys#current-load-filename"));
lf[204]=C_h_intern(&lf[204],20, C_text("##sys#dload-disabled"));
lf[205]=C_h_intern(&lf[205],35, C_text("chicken.load#set-dynamic-load-mode!"));
lf[206]=C_h_intern(&lf[206],23, C_text("##sys#set-dlopen-flags!"));
lf[207]=C_h_intern(&lf[207],6, C_text("global"));
lf[208]=C_h_intern(&lf[208],5, C_text("local"));
lf[209]=C_h_intern(&lf[209],4, C_text("lazy"));
lf[210]=C_h_intern(&lf[210],3, C_text("now"));
lf[211]=C_h_intern(&lf[211],17, C_text("##sys#signal-hook"));
lf[212]=C_h_intern(&lf[212],22, C_text("set-dynamic-load-mode!"));
lf[213]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031invalid dynamic-load mode"));
lf[215]=C_h_intern(&lf[215],19, C_text("##sys#make-c-string"));
lf[216]=C_h_intern(&lf[216],19, C_text("##sys#string-append"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002C_"));
lf[218]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010toplevel"));
lf[219]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011_toplevel"));
lf[220]=C_h_intern(&lf[220],37, C_text("chicken.internal#string->c-identifier"));
lf[222]=C_h_intern(&lf[222],11, C_text("##sys#dload"));
lf[223]=C_h_intern(&lf[223],4, C_text("load"));
lf[224]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002./"));
lf[225]=C_h_intern(&lf[225],33, C_text("##sys#read-error-with-line-number"));
lf[226]=C_h_intern(&lf[226],29, C_text("##sys#current-source-filename"));
lf[227]=C_h_intern(&lf[227],11, C_text("scheme#read"));
lf[228]=C_h_intern(&lf[228],19, C_text("##sys#display-times"));
lf[229]=C_h_intern(&lf[229],16, C_text("##sys#stop-timer"));
lf[230]=C_h_intern(&lf[230],17, C_text("##sys#start-timer"));
lf[231]=C_h_intern(&lf[231],14, C_text("scheme#newline"));
lf[232]=C_decode_literal(C_heaptop,C_text("\376B\000\000!unable to load compiled module - "));
lf[233]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016unknown reason"));
lf[234]=C_h_intern(&lf[234],19, C_text("##sys#peek-c-string"));
lf[235]=C_h_intern(&lf[235],16, C_text("scheme#peek-char"));
lf[236]=C_h_intern(&lf[236],23, C_text("scheme#close-input-port"));
lf[237]=C_h_intern(&lf[237],22, C_text("scheme#open-input-file"));
lf[238]=C_h_intern(&lf[238],25, C_text("chicken.base#flush-output"));
lf[239]=C_h_intern(&lf[239],14, C_text("scheme#display"));
lf[240]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005 ...\012"));
lf[241]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012; loading "));
lf[242]=C_h_intern_kw(&lf[242],10, C_text("type-error"));
lf[243]=C_decode_literal(C_heaptop,C_text("\376B\000\000(bad argument type - not a port or string"));
lf[244]=C_h_intern_kw(&lf[244],10, C_text("file-error"));
lf[245]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[246]=C_h_intern(&lf[246],18, C_text("##sys#file-exists\077"));
lf[247]=C_h_intern(&lf[247],18, C_text("chicken.base#port\077"));
lf[248]=C_h_intern(&lf[248],25, C_text("chicken.platform#feature\077"));
lf[249]=C_h_intern_kw(&lf[249],5, C_text("dload"));
lf[250]=C_h_intern(&lf[250],11, C_text("scheme#load"));
lf[251]=C_h_intern(&lf[251],26, C_text("chicken.load#load-relative"));
lf[252]=C_h_intern(&lf[252],25, C_text("chicken.load#load-noisily"));
lf[253]=C_h_intern(&lf[253],17, C_text("##sys#get-keyword"));
lf[254]=C_h_intern_kw(&lf[254],7, C_text("printer"));
lf[255]=C_h_intern_kw(&lf[255],4, C_text("time"));
lf[256]=C_h_intern_kw(&lf[256],9, C_text("evaluator"));
lf[257]=C_h_intern(&lf[257],35, C_text("chicken.load#dynamic-load-libraries"));
lf[259]=C_decode_literal(C_heaptop,C_text("\376B\000\000\026unable to load library"));
lf[260]=C_h_intern(&lf[260],12, C_text("load-library"));
lf[261]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005 ...\012"));
lf[262]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022; loading library "));
lf[263]=C_h_intern(&lf[263],18, C_text("##sys#load-library"));
lf[264]=C_h_intern(&lf[264],25, C_text("chicken.load#load-library"));
lf[265]=C_h_intern(&lf[265],27, C_text("scheme#with-input-from-file"));
lf[266]=C_h_intern(&lf[266],18, C_text("chicken.base#print"));
lf[267]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014; including "));
lf[268]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004 ..."));
lf[269]=C_h_intern(&lf[269],7, C_text("include"));
lf[270]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020cannot open file"));
lf[271]=C_h_intern(&lf[271],30, C_text("##sys#resolve-include-filename"));
lf[272]=C_h_intern(&lf[272],16, C_text("##sys#setup-mode"));
lf[274]=C_h_intern(&lf[274],22, C_text("chicken.load#find-file"));
lf[275]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[276]=C_h_intern(&lf[276],35, C_text("chicken.load#find-dynamic-extension"));
lf[277]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[278]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\001.\376\377\016"));
lf[279]=C_h_intern(&lf[279],23, C_text("##sys#include-pathnames"));
lf[280]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\001.\376\377\016"));
lf[281]=C_h_intern(&lf[281],21, C_text("scheme#symbol->string"));
lf[282]=C_h_intern(&lf[282],32, C_text("chicken.platform#repository-path"));
lf[283]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025cannot load extension"));
lf[284]=C_h_intern(&lf[284],20, C_text("chicken.load#require"));
lf[285]=C_h_intern(&lf[285],20, C_text("chicken.load#provide"));
lf[286]=C_h_intern(&lf[286],7, C_text("provide"));
lf[287]=C_h_intern(&lf[287],22, C_text("chicken.load#provided\077"));
lf[288]=C_h_intern(&lf[288],9, C_text("provided\077"));
lf[289]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\017\001chicken.foreign\376\003\000\000\002\376\001\000\000\031\001##core#require-for-syntax\376\003\000\000\002\376\001\000\000\022\001ch"
"icken-ffi-syntax\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\021\001chicken.condition\376\003\000\000\002\376\001\000\000\014\001##core#begin\376\003\000\000\002"
"\376\003\000\000\002\376\001\000\000\031\001##core#require-for-syntax\376\003\000\000\002\376\001\000\000\016\001chicken-syntax\376\377\016\376\003\000\000\002\376\003\000\000\002\376\001\000\000\016\001"
"##core#require\376\003\000\000\002\376\001\000\000\007\001library\376\377\016\376\377\016\376\377\016"));
lf[290]=C_h_intern(&lf[290],7, C_text("srfi-30"));
lf[291]=C_h_intern(&lf[291],7, C_text("srfi-46"));
lf[292]=C_h_intern(&lf[292],7, C_text("srfi-61"));
lf[293]=C_h_intern(&lf[293],7, C_text("srfi-62"));
lf[294]=C_h_intern(&lf[294],6, C_text("srfi-0"));
lf[295]=C_h_intern(&lf[295],6, C_text("srfi-2"));
lf[296]=C_h_intern(&lf[296],6, C_text("srfi-8"));
lf[297]=C_h_intern(&lf[297],6, C_text("srfi-9"));
lf[298]=C_h_intern(&lf[298],7, C_text("srfi-11"));
lf[299]=C_h_intern(&lf[299],7, C_text("srfi-15"));
lf[300]=C_h_intern(&lf[300],7, C_text("srfi-16"));
lf[301]=C_h_intern(&lf[301],7, C_text("srfi-17"));
lf[302]=C_h_intern(&lf[302],7, C_text("srfi-26"));
lf[303]=C_h_intern(&lf[303],7, C_text("srfi-31"));
lf[304]=C_h_intern(&lf[304],7, C_text("srfi-55"));
lf[305]=C_h_intern(&lf[305],7, C_text("srfi-88"));
lf[306]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[307]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[308]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016"));
lf[309]=C_h_intern(&lf[309],4, C_text("uses"));
lf[310]=C_h_intern(&lf[310],6, C_text("static"));
lf[311]=C_h_intern(&lf[311],7, C_text("dynamic"));
lf[312]=C_h_intern(&lf[312],27, C_text("chicken.internal#library-id"));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001/"));
lf[316]=C_h_intern(&lf[316],37, C_text("chicken.condition#print-error-message"));
lf[317]=C_h_intern(&lf[317],40, C_text("chicken.condition#with-exception-handler"));
lf[318]=C_h_intern(&lf[318],37, C_text("scheme#call-with-current-continuation"));
lf[320]=C_h_intern(&lf[320],8, C_text("##sys#gc"));
lf[322]=C_h_intern(&lf[322],19, C_text("##sys#thread-yield!"));
lf[325]=C_h_intern(&lf[325],30, C_text("chicken.base#open-input-string"));
lf[327]=C_decode_literal(C_heaptop,C_text("\376B\000\000(Error: not enough room for result string"));
lf[335]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010No error"));
lf[336]=C_h_intern(&lf[336],29, C_text("chicken.platform#chicken-home"));
lf[337]=C_h_intern(&lf[337],27, C_text("chicken.base#make-parameter"));
lf[338]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001."));
lf[339]=C_h_intern(&lf[339],20, C_text("##sys#fixnum->string"));
lf[340]=C_h_intern(&lf[340],6, C_text("cygwin"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\014cygchicken-0\376\377\016"));
lf[342]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003lib"));
lf[343]=C_h_intern(&lf[343],33, C_text("chicken.platform#software-version"));
lf[344]=C_h_intern(&lf[344],7, C_text("windows"));
lf[345]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004.dll"));
lf[346]=C_h_intern(&lf[346],6, C_text("macosx"));
lf[347]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006.dylib"));
lf[348]=C_h_intern(&lf[348],4, C_text("hpux"));
lf[349]=C_h_intern(&lf[349],4, C_text("hppa"));
lf[350]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003.sl"));
lf[351]=C_h_intern(&lf[351],29, C_text("chicken.platform#machine-type"));
lf[352]=C_h_intern(&lf[352],30, C_text("chicken.platform#software-type"));
lf[353]=C_h_intern(&lf[353],9, C_text("r5rs-null"));
lf[354]=C_h_intern(&lf[354],18, C_text("null-environment/5"));
lf[355]=C_h_intern(&lf[355],9, C_text("r4rs-null"));
lf[356]=C_h_intern(&lf[356],18, C_text("null-environment/4"));
lf[357]=C_h_intern(&lf[357],6, C_text("scheme"));
lf[358]=C_h_intern(&lf[358],27, C_text("scheme-report-environment/5"));
lf[359]=C_h_intern(&lf[359],4, C_text("r4rs"));
lf[360]=C_h_intern(&lf[360],27, C_text("scheme-report-environment/4"));
lf[361]=C_h_intern(&lf[361],18, C_text("##sys#write-char-0"));
lf[362]=C_h_intern(&lf[362],11, C_text("##sys#print"));
lf[363]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016#<environment "));
lf[364]=C_h_intern(&lf[364],29, C_text("##sys#register-record-printer"));
lf[365]=C_h_intern(&lf[365],24, C_text("chicken.eval#environment"));
lf[366]=C_h_intern(&lf[366],10, C_text("##sys#put!"));
lf[367]=C_h_intern(&lf[367],18, C_text("##sys#put/restore!"));
lf[368]=C_h_intern(&lf[368],39, C_text("chicken.internal#default-syntax-imports"));
lf[369]=C_h_intern(&lf[369],32, C_text("chicken.internal#default-imports"));
C_register_lf2(lf,370,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3517,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_modules_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[729] = {
{C_text("f_10003:eval_2escm"),(void*)f_10003},
{C_text("f_10014:eval_2escm"),(void*)f_10014},
{C_text("f_10018:eval_2escm"),(void*)f_10018},
{C_text("f_10021:eval_2escm"),(void*)f_10021},
{C_text("f_10024:eval_2escm"),(void*)f_10024},
{C_text("f_10029:eval_2escm"),(void*)f_10029},
{C_text("f_10032:eval_2escm"),(void*)f_10032},
{C_text("f_10035:eval_2escm"),(void*)f_10035},
{C_text("f_10040:eval_2escm"),(void*)f_10040},
{C_text("f_10042:eval_2escm"),(void*)f_10042},
{C_text("f_10049:eval_2escm"),(void*)f_10049},
{C_text("f_10051:eval_2escm"),(void*)f_10051},
{C_text("f_10055:eval_2escm"),(void*)f_10055},
{C_text("f_10058:eval_2escm"),(void*)f_10058},
{C_text("f_10061:eval_2escm"),(void*)f_10061},
{C_text("f_10066:eval_2escm"),(void*)f_10066},
{C_text("f_10080:eval_2escm"),(void*)f_10080},
{C_text("f_10083:eval_2escm"),(void*)f_10083},
{C_text("f_10097:eval_2escm"),(void*)f_10097},
{C_text("f_10104:eval_2escm"),(void*)f_10104},
{C_text("f_10107:eval_2escm"),(void*)f_10107},
{C_text("f_10110:eval_2escm"),(void*)f_10110},
{C_text("f_10123:eval_2escm"),(void*)f_10123},
{C_text("f_10127:eval_2escm"),(void*)f_10127},
{C_text("f_10133:eval_2escm"),(void*)f_10133},
{C_text("f_10152:eval_2escm"),(void*)f_10152},
{C_text("f_10155:eval_2escm"),(void*)f_10155},
{C_text("f_10181:eval_2escm"),(void*)f_10181},
{C_text("f_10208:eval_2escm"),(void*)f_10208},
{C_text("f_10212:eval_2escm"),(void*)f_10212},
{C_text("f_10215:eval_2escm"),(void*)f_10215},
{C_text("f_10218:eval_2escm"),(void*)f_10218},
{C_text("f_10223:eval_2escm"),(void*)f_10223},
{C_text("f_10229:eval_2escm"),(void*)f_10229},
{C_text("f_10234:eval_2escm"),(void*)f_10234},
{C_text("f_10242:eval_2escm"),(void*)f_10242},
{C_text("f_10244:eval_2escm"),(void*)f_10244},
{C_text("f_10258:eval_2escm"),(void*)f_10258},
{C_text("f_10265:eval_2escm"),(void*)f_10265},
{C_text("f_10271:eval_2escm"),(void*)f_10271},
{C_text("f_10277:eval_2escm"),(void*)f_10277},
{C_text("f_10289:eval_2escm"),(void*)f_10289},
{C_text("f_10296:eval_2escm"),(void*)f_10296},
{C_text("f_10298:eval_2escm"),(void*)f_10298},
{C_text("f_10327:eval_2escm"),(void*)f_10327},
{C_text("f_10339:eval_2escm"),(void*)f_10339},
{C_text("f_10345:eval_2escm"),(void*)f_10345},
{C_text("f_10349:eval_2escm"),(void*)f_10349},
{C_text("f_10352:eval_2escm"),(void*)f_10352},
{C_text("f_10354:eval_2escm"),(void*)f_10354},
{C_text("f_10358:eval_2escm"),(void*)f_10358},
{C_text("f_10361:eval_2escm"),(void*)f_10361},
{C_text("f_10371:eval_2escm"),(void*)f_10371},
{C_text("f_10383:eval_2escm"),(void*)f_10383},
{C_text("f_10390:eval_2escm"),(void*)f_10390},
{C_text("f_10397:eval_2escm"),(void*)f_10397},
{C_text("f_10399:eval_2escm"),(void*)f_10399},
{C_text("f_10412:eval_2escm"),(void*)f_10412},
{C_text("f_10450:eval_2escm"),(void*)f_10450},
{C_text("f_10456:eval_2escm"),(void*)f_10456},
{C_text("f_10471:eval_2escm"),(void*)f_10471},
{C_text("f_10475:eval_2escm"),(void*)f_10475},
{C_text("f_10479:eval_2escm"),(void*)f_10479},
{C_text("f_10490:eval_2escm"),(void*)f_10490},
{C_text("f_10494:eval_2escm"),(void*)f_10494},
{C_text("f_10496:eval_2escm"),(void*)f_10496},
{C_text("f_10515:eval_2escm"),(void*)f_10515},
{C_text("f_10525:eval_2escm"),(void*)f_10525},
{C_text("f_10538:eval_2escm"),(void*)f_10538},
{C_text("f_10561:eval_2escm"),(void*)f_10561},
{C_text("f_10580:eval_2escm"),(void*)f_10580},
{C_text("f_10590:eval_2escm"),(void*)f_10590},
{C_text("f_10603:eval_2escm"),(void*)f_10603},
{C_text("f_10626:eval_2escm"),(void*)f_10626},
{C_text("f_10640:eval_2escm"),(void*)f_10640},
{C_text("f_10665:eval_2escm"),(void*)f_10665},
{C_text("f_10699:eval_2escm"),(void*)f_10699},
{C_text("f_10741:eval_2escm"),(void*)f_10741},
{C_text("f_10784:eval_2escm"),(void*)f_10784},
{C_text("f_10816:eval_2escm"),(void*)f_10816},
{C_text("f_10886:eval_2escm"),(void*)f_10886},
{C_text("f_10889:eval_2escm"),(void*)f_10889},
{C_text("f_10902:eval_2escm"),(void*)f_10902},
{C_text("f_10905:eval_2escm"),(void*)f_10905},
{C_text("f_10908:eval_2escm"),(void*)f_10908},
{C_text("f_10922:eval_2escm"),(void*)f_10922},
{C_text("f_10958:eval_2escm"),(void*)f_10958},
{C_text("f_10961:eval_2escm"),(void*)f_10961},
{C_text("f_10971:eval_2escm"),(void*)f_10971},
{C_text("f_10973:eval_2escm"),(void*)f_10973},
{C_text("f_10983:eval_2escm"),(void*)f_10983},
{C_text("f_10997:eval_2escm"),(void*)f_10997},
{C_text("f_11008:eval_2escm"),(void*)f_11008},
{C_text("f_11015:eval_2escm"),(void*)f_11015},
{C_text("f_11018:eval_2escm"),(void*)f_11018},
{C_text("f_11023:eval_2escm"),(void*)f_11023},
{C_text("f_11028:eval_2escm"),(void*)f_11028},
{C_text("f_11034:eval_2escm"),(void*)f_11034},
{C_text("f_11040:eval_2escm"),(void*)f_11040},
{C_text("f_11044:eval_2escm"),(void*)f_11044},
{C_text("f_11047:eval_2escm"),(void*)f_11047},
{C_text("f_11051:eval_2escm"),(void*)f_11051},
{C_text("f_11053:eval_2escm"),(void*)f_11053},
{C_text("f_11059:eval_2escm"),(void*)f_11059},
{C_text("f_11065:eval_2escm"),(void*)f_11065},
{C_text("f_11071:eval_2escm"),(void*)f_11071},
{C_text("f_11077:eval_2escm"),(void*)f_11077},
{C_text("f_11081:eval_2escm"),(void*)f_11081},
{C_text("f_11086:eval_2escm"),(void*)f_11086},
{C_text("f_11092:eval_2escm"),(void*)f_11092},
{C_text("f_11096:eval_2escm"),(void*)f_11096},
{C_text("f_11098:eval_2escm"),(void*)f_11098},
{C_text("f_11104:eval_2escm"),(void*)f_11104},
{C_text("f_11112:eval_2escm"),(void*)f_11112},
{C_text("f_11114:eval_2escm"),(void*)f_11114},
{C_text("f_11118:eval_2escm"),(void*)f_11118},
{C_text("f_11123:eval_2escm"),(void*)f_11123},
{C_text("f_11127:eval_2escm"),(void*)f_11127},
{C_text("f_11134:eval_2escm"),(void*)f_11134},
{C_text("f_11138:eval_2escm"),(void*)f_11138},
{C_text("f_11140:eval_2escm"),(void*)f_11140},
{C_text("f_11153:eval_2escm"),(void*)f_11153},
{C_text("f_11159:eval_2escm"),(void*)f_11159},
{C_text("f_11163:eval_2escm"),(void*)f_11163},
{C_text("f_11166:eval_2escm"),(void*)f_11166},
{C_text("f_11173:eval_2escm"),(void*)f_11173},
{C_text("f_11177:eval_2escm"),(void*)f_11177},
{C_text("f_11179:eval_2escm"),(void*)f_11179},
{C_text("f_11183:eval_2escm"),(void*)f_11183},
{C_text("f_11188:eval_2escm"),(void*)f_11188},
{C_text("f_11192:eval_2escm"),(void*)f_11192},
{C_text("f_11195:eval_2escm"),(void*)f_11195},
{C_text("f_11202:eval_2escm"),(void*)f_11202},
{C_text("f_11206:eval_2escm"),(void*)f_11206},
{C_text("f_11210:eval_2escm"),(void*)f_11210},
{C_text("f_11214:eval_2escm"),(void*)f_11214},
{C_text("f_11216:eval_2escm"),(void*)f_11216},
{C_text("f_11222:eval_2escm"),(void*)f_11222},
{C_text("f_11230:eval_2escm"),(void*)f_11230},
{C_text("f_11232:eval_2escm"),(void*)f_11232},
{C_text("f_11238:eval_2escm"),(void*)f_11238},
{C_text("f_11242:eval_2escm"),(void*)f_11242},
{C_text("f_11245:eval_2escm"),(void*)f_11245},
{C_text("f_11252:eval_2escm"),(void*)f_11252},
{C_text("f_11256:eval_2escm"),(void*)f_11256},
{C_text("f_11258:eval_2escm"),(void*)f_11258},
{C_text("f_11262:eval_2escm"),(void*)f_11262},
{C_text("f_11267:eval_2escm"),(void*)f_11267},
{C_text("f_11271:eval_2escm"),(void*)f_11271},
{C_text("f_11278:eval_2escm"),(void*)f_11278},
{C_text("f_11280:eval_2escm"),(void*)f_11280},
{C_text("f_11284:eval_2escm"),(void*)f_11284},
{C_text("f_11289:eval_2escm"),(void*)f_11289},
{C_text("f_11293:eval_2escm"),(void*)f_11293},
{C_text("f_11295:eval_2escm"),(void*)f_11295},
{C_text("f_11307:eval_2escm"),(void*)f_11307},
{C_text("f_11314:eval_2escm"),(void*)f_11314},
{C_text("f_11316:eval_2escm"),(void*)f_11316},
{C_text("f_11322:eval_2escm"),(void*)f_11322},
{C_text("f_11347:eval_2escm"),(void*)f_11347},
{C_text("f_11361:eval_2escm"),(void*)f_11361},
{C_text("f_11373:eval_2escm"),(void*)f_11373},
{C_text("f_11377:eval_2escm"),(void*)f_11377},
{C_text("f_11404:eval_2escm"),(void*)f_11404},
{C_text("f_11408:eval_2escm"),(void*)f_11408},
{C_text("f_11412:eval_2escm"),(void*)f_11412},
{C_text("f_11416:eval_2escm"),(void*)f_11416},
{C_text("f_11420:eval_2escm"),(void*)f_11420},
{C_text("f_11428:eval_2escm"),(void*)f_11428},
{C_text("f_11436:eval_2escm"),(void*)f_11436},
{C_text("f_11444:eval_2escm"),(void*)f_11444},
{C_text("f_11450:eval_2escm"),(void*)f_11450},
{C_text("f_11454:eval_2escm"),(void*)f_11454},
{C_text("f_11457:eval_2escm"),(void*)f_11457},
{C_text("f_11466:eval_2escm"),(void*)f_11466},
{C_text("f_11472:eval_2escm"),(void*)f_11472},
{C_text("f_11476:eval_2escm"),(void*)f_11476},
{C_text("f_11479:eval_2escm"),(void*)f_11479},
{C_text("f_11491:eval_2escm"),(void*)f_11491},
{C_text("f_11493:eval_2escm"),(void*)f_11493},
{C_text("f_11499:eval_2escm"),(void*)f_11499},
{C_text("f_11504:eval_2escm"),(void*)f_11504},
{C_text("f_11508:eval_2escm"),(void*)f_11508},
{C_text("f_11511:eval_2escm"),(void*)f_11511},
{C_text("f_11521:eval_2escm"),(void*)f_11521},
{C_text("f_11534:eval_2escm"),(void*)f_11534},
{C_text("f_11539:eval_2escm"),(void*)f_11539},
{C_text("f_11546:eval_2escm"),(void*)f_11546},
{C_text("f_11549:eval_2escm"),(void*)f_11549},
{C_text("f_11561:eval_2escm"),(void*)f_11561},
{C_text("f_11569:eval_2escm"),(void*)f_11569},
{C_text("f_11573:eval_2escm"),(void*)f_11573},
{C_text("f_11576:eval_2escm"),(void*)f_11576},
{C_text("f_11580:eval_2escm"),(void*)f_11580},
{C_text("f_11584:eval_2escm"),(void*)f_11584},
{C_text("f_11587:eval_2escm"),(void*)f_11587},
{C_text("f_11590:eval_2escm"),(void*)f_11590},
{C_text("f_11599:eval_2escm"),(void*)f_11599},
{C_text("f_11609:eval_2escm"),(void*)f_11609},
{C_text("f_11613:eval_2escm"),(void*)f_11613},
{C_text("f_11616:eval_2escm"),(void*)f_11616},
{C_text("f_11619:eval_2escm"),(void*)f_11619},
{C_text("f_11622:eval_2escm"),(void*)f_11622},
{C_text("f_11630:eval_2escm"),(void*)f_11630},
{C_text("f_11639:eval_2escm"),(void*)f_11639},
{C_text("f_3517:eval_2escm"),(void*)f_3517},
{C_text("f_3520:eval_2escm"),(void*)f_3520},
{C_text("f_3523:eval_2escm"),(void*)f_3523},
{C_text("f_3526:eval_2escm"),(void*)f_3526},
{C_text("f_3534:eval_2escm"),(void*)f_3534},
{C_text("f_3547:eval_2escm"),(void*)f_3547},
{C_text("f_3555:eval_2escm"),(void*)f_3555},
{C_text("f_3559:eval_2escm"),(void*)f_3559},
{C_text("f_3562:eval_2escm"),(void*)f_3562},
{C_text("f_3565:eval_2escm"),(void*)f_3565},
{C_text("f_3570:eval_2escm"),(void*)f_3570},
{C_text("f_3572:eval_2escm"),(void*)f_3572},
{C_text("f_3599:eval_2escm"),(void*)f_3599},
{C_text("f_3612:eval_2escm"),(void*)f_3612},
{C_text("f_3634:eval_2escm"),(void*)f_3634},
{C_text("f_3638:eval_2escm"),(void*)f_3638},
{C_text("f_3646:eval_2escm"),(void*)f_3646},
{C_text("f_3652:eval_2escm"),(void*)f_3652},
{C_text("f_3659:eval_2escm"),(void*)f_3659},
{C_text("f_3666:eval_2escm"),(void*)f_3666},
{C_text("f_3668:eval_2escm"),(void*)f_3668},
{C_text("f_3672:eval_2escm"),(void*)f_3672},
{C_text("f_3680:eval_2escm"),(void*)f_3680},
{C_text("f_3697:eval_2escm"),(void*)f_3697},
{C_text("f_3727:eval_2escm"),(void*)f_3727},
{C_text("f_3757:eval_2escm"),(void*)f_3757},
{C_text("f_3781:eval_2escm"),(void*)f_3781},
{C_text("f_3787:eval_2escm"),(void*)f_3787},
{C_text("f_3794:eval_2escm"),(void*)f_3794},
{C_text("f_3795:eval_2escm"),(void*)f_3795},
{C_text("f_3807:eval_2escm"),(void*)f_3807},
{C_text("f_3813:eval_2escm"),(void*)f_3813},
{C_text("f_3823:eval_2escm"),(void*)f_3823},
{C_text("f_3826:eval_2escm"),(void*)f_3826},
{C_text("f_3833:eval_2escm"),(void*)f_3833},
{C_text("f_3843:eval_2escm"),(void*)f_3843},
{C_text("f_3844:eval_2escm"),(void*)f_3844},
{C_text("f_3849:eval_2escm"),(void*)f_3849},
{C_text("f_3853:eval_2escm"),(void*)f_3853},
{C_text("f_3874:eval_2escm"),(void*)f_3874},
{C_text("f_3896:eval_2escm"),(void*)f_3896},
{C_text("f_3907:eval_2escm"),(void*)f_3907},
{C_text("f_3922:eval_2escm"),(void*)f_3922},
{C_text("f_3941:eval_2escm"),(void*)f_3941},
{C_text("f_3964:eval_2escm"),(void*)f_3964},
{C_text("f_3985:eval_2escm"),(void*)f_3985},
{C_text("f_4004:eval_2escm"),(void*)f_4004},
{C_text("f_4011:eval_2escm"),(void*)f_4011},
{C_text("f_4019:eval_2escm"),(void*)f_4019},
{C_text("f_4027:eval_2escm"),(void*)f_4027},
{C_text("f_4035:eval_2escm"),(void*)f_4035},
{C_text("f_4037:eval_2escm"),(void*)f_4037},
{C_text("f_4056:eval_2escm"),(void*)f_4056},
{C_text("f_4058:eval_2escm"),(void*)f_4058},
{C_text("f_4068:eval_2escm"),(void*)f_4068},
{C_text("f_4069:eval_2escm"),(void*)f_4069},
{C_text("f_4088:eval_2escm"),(void*)f_4088},
{C_text("f_4091:eval_2escm"),(void*)f_4091},
{C_text("f_4106:eval_2escm"),(void*)f_4106},
{C_text("f_4115:eval_2escm"),(void*)f_4115},
{C_text("f_4122:eval_2escm"),(void*)f_4122},
{C_text("f_4130:eval_2escm"),(void*)f_4130},
{C_text("f_4138:eval_2escm"),(void*)f_4138},
{C_text("f_4146:eval_2escm"),(void*)f_4146},
{C_text("f_4154:eval_2escm"),(void*)f_4154},
{C_text("f_4162:eval_2escm"),(void*)f_4162},
{C_text("f_4170:eval_2escm"),(void*)f_4170},
{C_text("f_4172:eval_2escm"),(void*)f_4172},
{C_text("f_4201:eval_2escm"),(void*)f_4201},
{C_text("f_4235:eval_2escm"),(void*)f_4235},
{C_text("f_4245:eval_2escm"),(void*)f_4245},
{C_text("f_4248:eval_2escm"),(void*)f_4248},
{C_text("f_4251:eval_2escm"),(void*)f_4251},
{C_text("f_4252:eval_2escm"),(void*)f_4252},
{C_text("f_4259:eval_2escm"),(void*)f_4259},
{C_text("f_4333:eval_2escm"),(void*)f_4333},
{C_text("f_4336:eval_2escm"),(void*)f_4336},
{C_text("f_4337:eval_2escm"),(void*)f_4337},
{C_text("f_4341:eval_2escm"),(void*)f_4341},
{C_text("f_4355:eval_2escm"),(void*)f_4355},
{C_text("f_4358:eval_2escm"),(void*)f_4358},
{C_text("f_4361:eval_2escm"),(void*)f_4361},
{C_text("f_4362:eval_2escm"),(void*)f_4362},
{C_text("f_4366:eval_2escm"),(void*)f_4366},
{C_text("f_4369:eval_2escm"),(void*)f_4369},
{C_text("f_4407:eval_2escm"),(void*)f_4407},
{C_text("f_4431:eval_2escm"),(void*)f_4431},
{C_text("f_4437:eval_2escm"),(void*)f_4437},
{C_text("f_4441:eval_2escm"),(void*)f_4441},
{C_text("f_4450:eval_2escm"),(void*)f_4450},
{C_text("f_4454:eval_2escm"),(void*)f_4454},
{C_text("f_4461:eval_2escm"),(void*)f_4461},
{C_text("f_4462:eval_2escm"),(void*)f_4462},
{C_text("f_4466:eval_2escm"),(void*)f_4466},
{C_text("f_4492:eval_2escm"),(void*)f_4492},
{C_text("f_4497:eval_2escm"),(void*)f_4497},
{C_text("f_4509:eval_2escm"),(void*)f_4509},
{C_text("f_4510:eval_2escm"),(void*)f_4510},
{C_text("f_4519:eval_2escm"),(void*)f_4519},
{C_text("f_4546:eval_2escm"),(void*)f_4546},
{C_text("f_4555:eval_2escm"),(void*)f_4555},
{C_text("f_4561:eval_2escm"),(void*)f_4561},
{C_text("f_4564:eval_2escm"),(void*)f_4564},
{C_text("f_4573:eval_2escm"),(void*)f_4573},
{C_text("f_4574:eval_2escm"),(void*)f_4574},
{C_text("f_4590:eval_2escm"),(void*)f_4590},
{C_text("f_4594:eval_2escm"),(void*)f_4594},
{C_text("f_4607:eval_2escm"),(void*)f_4607},
{C_text("f_4610:eval_2escm"),(void*)f_4610},
{C_text("f_4611:eval_2escm"),(void*)f_4611},
{C_text("f_4627:eval_2escm"),(void*)f_4627},
{C_text("f_4631:eval_2escm"),(void*)f_4631},
{C_text("f_4635:eval_2escm"),(void*)f_4635},
{C_text("f_4643:eval_2escm"),(void*)f_4643},
{C_text("f_4656:eval_2escm"),(void*)f_4656},
{C_text("f_4659:eval_2escm"),(void*)f_4659},
{C_text("f_4665:eval_2escm"),(void*)f_4665},
{C_text("f_4666:eval_2escm"),(void*)f_4666},
{C_text("f_4682:eval_2escm"),(void*)f_4682},
{C_text("f_4686:eval_2escm"),(void*)f_4686},
{C_text("f_4690:eval_2escm"),(void*)f_4690},
{C_text("f_4694:eval_2escm"),(void*)f_4694},
{C_text("f_4702:eval_2escm"),(void*)f_4702},
{C_text("f_4710:eval_2escm"),(void*)f_4710},
{C_text("f_4723:eval_2escm"),(void*)f_4723},
{C_text("f_4726:eval_2escm"),(void*)f_4726},
{C_text("f_4732:eval_2escm"),(void*)f_4732},
{C_text("f_4735:eval_2escm"),(void*)f_4735},
{C_text("f_4736:eval_2escm"),(void*)f_4736},
{C_text("f_4752:eval_2escm"),(void*)f_4752},
{C_text("f_4756:eval_2escm"),(void*)f_4756},
{C_text("f_4760:eval_2escm"),(void*)f_4760},
{C_text("f_4764:eval_2escm"),(void*)f_4764},
{C_text("f_4768:eval_2escm"),(void*)f_4768},
{C_text("f_4776:eval_2escm"),(void*)f_4776},
{C_text("f_4784:eval_2escm"),(void*)f_4784},
{C_text("f_4792:eval_2escm"),(void*)f_4792},
{C_text("f_4800:eval_2escm"),(void*)f_4800},
{C_text("f_4813:eval_2escm"),(void*)f_4813},
{C_text("f_4814:eval_2escm"),(void*)f_4814},
{C_text("f_4818:eval_2escm"),(void*)f_4818},
{C_text("f_4821:eval_2escm"),(void*)f_4821},
{C_text("f_4830:eval_2escm"),(void*)f_4830},
{C_text("f_4855:eval_2escm"),(void*)f_4855},
{C_text("f_4860:eval_2escm"),(void*)f_4860},
{C_text("f_4885:eval_2escm"),(void*)f_4885},
{C_text("f_4902:eval_2escm"),(void*)f_4902},
{C_text("f_4906:eval_2escm"),(void*)f_4906},
{C_text("f_4910:eval_2escm"),(void*)f_4910},
{C_text("f_4913:eval_2escm"),(void*)f_4913},
{C_text("f_4919:eval_2escm"),(void*)f_4919},
{C_text("f_4927:eval_2escm"),(void*)f_4927},
{C_text("f_4935:eval_2escm"),(void*)f_4935},
{C_text("f_4937:eval_2escm"),(void*)f_4937},
{C_text("f_4941:eval_2escm"),(void*)f_4941},
{C_text("f_4944:eval_2escm"),(void*)f_4944},
{C_text("f_4949:eval_2escm"),(void*)f_4949},
{C_text("f_4951:eval_2escm"),(void*)f_4951},
{C_text("f_4976:eval_2escm"),(void*)f_4976},
{C_text("f_4985:eval_2escm"),(void*)f_4985},
{C_text("f_5058:eval_2escm"),(void*)f_5058},
{C_text("f_5062:eval_2escm"),(void*)f_5062},
{C_text("f_5082:eval_2escm"),(void*)f_5082},
{C_text("f_5096:eval_2escm"),(void*)f_5096},
{C_text("f_5130:eval_2escm"),(void*)f_5130},
{C_text("f_5180:eval_2escm"),(void*)f_5180},
{C_text("f_5189:eval_2escm"),(void*)f_5189},
{C_text("f_5215:eval_2escm"),(void*)f_5215},
{C_text("f_5242:eval_2escm"),(void*)f_5242},
{C_text("f_5246:eval_2escm"),(void*)f_5246},
{C_text("f_5258:eval_2escm"),(void*)f_5258},
{C_text("f_5272:eval_2escm"),(void*)f_5272},
{C_text("f_5320:eval_2escm"),(void*)f_5320},
{C_text("f_5368:eval_2escm"),(void*)f_5368},
{C_text("f_5402:eval_2escm"),(void*)f_5402},
{C_text("f_5427:eval_2escm"),(void*)f_5427},
{C_text("f_5436:eval_2escm"),(void*)f_5436},
{C_text("f_5477:eval_2escm"),(void*)f_5477},
{C_text("f_5488:eval_2escm"),(void*)f_5488},
{C_text("f_5493:eval_2escm"),(void*)f_5493},
{C_text("f_5503:eval_2escm"),(void*)f_5503},
{C_text("f_5506:eval_2escm"),(void*)f_5506},
{C_text("f_5512:eval_2escm"),(void*)f_5512},
{C_text("f_5522:eval_2escm"),(void*)f_5522},
{C_text("f_5528:eval_2escm"),(void*)f_5528},
{C_text("f_5541:eval_2escm"),(void*)f_5541},
{C_text("f_5547:eval_2escm"),(void*)f_5547},
{C_text("f_5565:eval_2escm"),(void*)f_5565},
{C_text("f_5571:eval_2escm"),(void*)f_5571},
{C_text("f_5584:eval_2escm"),(void*)f_5584},
{C_text("f_5590:eval_2escm"),(void*)f_5590},
{C_text("f_5612:eval_2escm"),(void*)f_5612},
{C_text("f_5618:eval_2escm"),(void*)f_5618},
{C_text("f_5631:eval_2escm"),(void*)f_5631},
{C_text("f_5637:eval_2escm"),(void*)f_5637},
{C_text("f_5659:eval_2escm"),(void*)f_5659},
{C_text("f_5665:eval_2escm"),(void*)f_5665},
{C_text("f_5678:eval_2escm"),(void*)f_5678},
{C_text("f_5684:eval_2escm"),(void*)f_5684},
{C_text("f_5706:eval_2escm"),(void*)f_5706},
{C_text("f_5712:eval_2escm"),(void*)f_5712},
{C_text("f_5725:eval_2escm"),(void*)f_5725},
{C_text("f_5731:eval_2escm"),(void*)f_5731},
{C_text("f_5743:eval_2escm"),(void*)f_5743},
{C_text("f_5747:eval_2escm"),(void*)f_5747},
{C_text("f_5753:eval_2escm"),(void*)f_5753},
{C_text("f_5765:eval_2escm"),(void*)f_5765},
{C_text("f_5769:eval_2escm"),(void*)f_5769},
{C_text("f_5770:eval_2escm"),(void*)f_5770},
{C_text("f_5776:eval_2escm"),(void*)f_5776},
{C_text("f_5798:eval_2escm"),(void*)f_5798},
{C_text("f_5814:eval_2escm"),(void*)f_5814},
{C_text("f_5818:eval_2escm"),(void*)f_5818},
{C_text("f_5822:eval_2escm"),(void*)f_5822},
{C_text("f_5825:eval_2escm"),(void*)f_5825},
{C_text("f_5831:eval_2escm"),(void*)f_5831},
{C_text("f_5839:eval_2escm"),(void*)f_5839},
{C_text("f_5845:eval_2escm"),(void*)f_5845},
{C_text("f_5849:eval_2escm"),(void*)f_5849},
{C_text("f_5852:eval_2escm"),(void*)f_5852},
{C_text("f_5857:eval_2escm"),(void*)f_5857},
{C_text("f_5859:eval_2escm"),(void*)f_5859},
{C_text("f_5884:eval_2escm"),(void*)f_5884},
{C_text("f_5894:eval_2escm"),(void*)f_5894},
{C_text("f_5899:eval_2escm"),(void*)f_5899},
{C_text("f_5907:eval_2escm"),(void*)f_5907},
{C_text("f_5909:eval_2escm"),(void*)f_5909},
{C_text("f_5920:eval_2escm"),(void*)f_5920},
{C_text("f_5929:eval_2escm"),(void*)f_5929},
{C_text("f_5934:eval_2escm"),(void*)f_5934},
{C_text("f_5938:eval_2escm"),(void*)f_5938},
{C_text("f_5942:eval_2escm"),(void*)f_5942},
{C_text("f_5945:eval_2escm"),(void*)f_5945},
{C_text("f_5951:eval_2escm"),(void*)f_5951},
{C_text("f_5959:eval_2escm"),(void*)f_5959},
{C_text("f_5967:eval_2escm"),(void*)f_5967},
{C_text("f_5969:eval_2escm"),(void*)f_5969},
{C_text("f_5973:eval_2escm"),(void*)f_5973},
{C_text("f_5976:eval_2escm"),(void*)f_5976},
{C_text("f_5994:eval_2escm"),(void*)f_5994},
{C_text("f_5998:eval_2escm"),(void*)f_5998},
{C_text("f_6002:eval_2escm"),(void*)f_6002},
{C_text("f_6006:eval_2escm"),(void*)f_6006},
{C_text("f_6021:eval_2escm"),(void*)f_6021},
{C_text("f_6025:eval_2escm"),(void*)f_6025},
{C_text("f_6027:eval_2escm"),(void*)f_6027},
{C_text("f_6081:eval_2escm"),(void*)f_6081},
{C_text("f_6085:eval_2escm"),(void*)f_6085},
{C_text("f_6089:eval_2escm"),(void*)f_6089},
{C_text("f_6104:eval_2escm"),(void*)f_6104},
{C_text("f_6107:eval_2escm"),(void*)f_6107},
{C_text("f_6108:eval_2escm"),(void*)f_6108},
{C_text("f_6127:eval_2escm"),(void*)f_6127},
{C_text("f_6131:eval_2escm"),(void*)f_6131},
{C_text("f_6135:eval_2escm"),(void*)f_6135},
{C_text("f_6138:eval_2escm"),(void*)f_6138},
{C_text("f_6144:eval_2escm"),(void*)f_6144},
{C_text("f_6152:eval_2escm"),(void*)f_6152},
{C_text("f_6160:eval_2escm"),(void*)f_6160},
{C_text("f_6162:eval_2escm"),(void*)f_6162},
{C_text("f_6166:eval_2escm"),(void*)f_6166},
{C_text("f_6169:eval_2escm"),(void*)f_6169},
{C_text("f_6172:eval_2escm"),(void*)f_6172},
{C_text("f_6197:eval_2escm"),(void*)f_6197},
{C_text("f_6199:eval_2escm"),(void*)f_6199},
{C_text("f_6246:eval_2escm"),(void*)f_6246},
{C_text("f_6249:eval_2escm"),(void*)f_6249},
{C_text("f_6252:eval_2escm"),(void*)f_6252},
{C_text("f_6255:eval_2escm"),(void*)f_6255},
{C_text("f_6262:eval_2escm"),(void*)f_6262},
{C_text("f_6266:eval_2escm"),(void*)f_6266},
{C_text("f_6270:eval_2escm"),(void*)f_6270},
{C_text("f_6287:eval_2escm"),(void*)f_6287},
{C_text("f_6309:eval_2escm"),(void*)f_6309},
{C_text("f_6317:eval_2escm"),(void*)f_6317},
{C_text("f_6336:eval_2escm"),(void*)f_6336},
{C_text("f_6344:eval_2escm"),(void*)f_6344},
{C_text("f_6354:eval_2escm"),(void*)f_6354},
{C_text("f_6358:eval_2escm"),(void*)f_6358},
{C_text("f_6387:eval_2escm"),(void*)f_6387},
{C_text("f_6399:eval_2escm"),(void*)f_6399},
{C_text("f_6401:eval_2escm"),(void*)f_6401},
{C_text("f_6415:eval_2escm"),(void*)f_6415},
{C_text("f_6440:eval_2escm"),(void*)f_6440},
{C_text("f_6456:eval_2escm"),(void*)f_6456},
{C_text("f_6465:eval_2escm"),(void*)f_6465},
{C_text("f_6468:eval_2escm"),(void*)f_6468},
{C_text("f_6471:eval_2escm"),(void*)f_6471},
{C_text("f_6474:eval_2escm"),(void*)f_6474},
{C_text("f_6479:eval_2escm"),(void*)f_6479},
{C_text("f_6483:eval_2escm"),(void*)f_6483},
{C_text("f_6486:eval_2escm"),(void*)f_6486},
{C_text("f_6489:eval_2escm"),(void*)f_6489},
{C_text("f_6492:eval_2escm"),(void*)f_6492},
{C_text("f_6496:eval_2escm"),(void*)f_6496},
{C_text("f_6500:eval_2escm"),(void*)f_6500},
{C_text("f_6504:eval_2escm"),(void*)f_6504},
{C_text("f_6508:eval_2escm"),(void*)f_6508},
{C_text("f_6511:eval_2escm"),(void*)f_6511},
{C_text("f_6514:eval_2escm"),(void*)f_6514},
{C_text("f_6517:eval_2escm"),(void*)f_6517},
{C_text("f_6520:eval_2escm"),(void*)f_6520},
{C_text("f_6535:eval_2escm"),(void*)f_6535},
{C_text("f_6541:eval_2escm"),(void*)f_6541},
{C_text("f_6551:eval_2escm"),(void*)f_6551},
{C_text("f_6561:eval_2escm"),(void*)f_6561},
{C_text("f_6564:eval_2escm"),(void*)f_6564},
{C_text("f_6567:eval_2escm"),(void*)f_6567},
{C_text("f_6568:eval_2escm"),(void*)f_6568},
{C_text("f_6574:eval_2escm"),(void*)f_6574},
{C_text("f_6597:eval_2escm"),(void*)f_6597},
{C_text("f_6608:eval_2escm"),(void*)f_6608},
{C_text("f_6612:eval_2escm"),(void*)f_6612},
{C_text("f_6627:eval_2escm"),(void*)f_6627},
{C_text("f_6631:eval_2escm"),(void*)f_6631},
{C_text("f_6635:eval_2escm"),(void*)f_6635},
{C_text("f_6638:eval_2escm"),(void*)f_6638},
{C_text("f_6641:eval_2escm"),(void*)f_6641},
{C_text("f_6644:eval_2escm"),(void*)f_6644},
{C_text("f_6647:eval_2escm"),(void*)f_6647},
{C_text("f_6650:eval_2escm"),(void*)f_6650},
{C_text("f_6653:eval_2escm"),(void*)f_6653},
{C_text("f_6656:eval_2escm"),(void*)f_6656},
{C_text("f_6663:eval_2escm"),(void*)f_6663},
{C_text("f_6670:eval_2escm"),(void*)f_6670},
{C_text("f_6683:eval_2escm"),(void*)f_6683},
{C_text("f_6694:eval_2escm"),(void*)f_6694},
{C_text("f_6727:eval_2escm"),(void*)f_6727},
{C_text("f_6752:eval_2escm"),(void*)f_6752},
{C_text("f_6780:eval_2escm"),(void*)f_6780},
{C_text("f_6814:eval_2escm"),(void*)f_6814},
{C_text("f_6837:eval_2escm"),(void*)f_6837},
{C_text("f_6843:eval_2escm"),(void*)f_6843},
{C_text("f_6859:eval_2escm"),(void*)f_6859},
{C_text("f_6900:eval_2escm"),(void*)f_6900},
{C_text("f_6923:eval_2escm"),(void*)f_6923},
{C_text("f_6936:eval_2escm"),(void*)f_6936},
{C_text("f_6956:eval_2escm"),(void*)f_6956},
{C_text("f_6982:eval_2escm"),(void*)f_6982},
{C_text("f_7000:eval_2escm"),(void*)f_7000},
{C_text("f_7012:eval_2escm"),(void*)f_7012},
{C_text("f_7017:eval_2escm"),(void*)f_7017},
{C_text("f_7025:eval_2escm"),(void*)f_7025},
{C_text("f_7027:eval_2escm"),(void*)f_7027},
{C_text("f_7202:eval_2escm"),(void*)f_7202},
{C_text("f_7205:eval_2escm"),(void*)f_7205},
{C_text("f_7212:eval_2escm"),(void*)f_7212},
{C_text("f_7235:eval_2escm"),(void*)f_7235},
{C_text("f_7264:eval_2escm"),(void*)f_7264},
{C_text("f_7293:eval_2escm"),(void*)f_7293},
{C_text("f_7310:eval_2escm"),(void*)f_7310},
{C_text("f_7336:eval_2escm"),(void*)f_7336},
{C_text("f_7343:eval_2escm"),(void*)f_7343},
{C_text("f_7365:eval_2escm"),(void*)f_7365},
{C_text("f_7372:eval_2escm"),(void*)f_7372},
{C_text("f_7384:eval_2escm"),(void*)f_7384},
{C_text("f_7385:eval_2escm"),(void*)f_7385},
{C_text("f_7392:eval_2escm"),(void*)f_7392},
{C_text("f_7399:eval_2escm"),(void*)f_7399},
{C_text("f_7412:eval_2escm"),(void*)f_7412},
{C_text("f_7415:eval_2escm"),(void*)f_7415},
{C_text("f_7416:eval_2escm"),(void*)f_7416},
{C_text("f_7423:eval_2escm"),(void*)f_7423},
{C_text("f_7430:eval_2escm"),(void*)f_7430},
{C_text("f_7434:eval_2escm"),(void*)f_7434},
{C_text("f_7448:eval_2escm"),(void*)f_7448},
{C_text("f_7451:eval_2escm"),(void*)f_7451},
{C_text("f_7454:eval_2escm"),(void*)f_7454},
{C_text("f_7455:eval_2escm"),(void*)f_7455},
{C_text("f_7462:eval_2escm"),(void*)f_7462},
{C_text("f_7469:eval_2escm"),(void*)f_7469},
{C_text("f_7473:eval_2escm"),(void*)f_7473},
{C_text("f_7477:eval_2escm"),(void*)f_7477},
{C_text("f_7492:eval_2escm"),(void*)f_7492},
{C_text("f_7495:eval_2escm"),(void*)f_7495},
{C_text("f_7498:eval_2escm"),(void*)f_7498},
{C_text("f_7501:eval_2escm"),(void*)f_7501},
{C_text("f_7502:eval_2escm"),(void*)f_7502},
{C_text("f_7509:eval_2escm"),(void*)f_7509},
{C_text("f_7516:eval_2escm"),(void*)f_7516},
{C_text("f_7520:eval_2escm"),(void*)f_7520},
{C_text("f_7524:eval_2escm"),(void*)f_7524},
{C_text("f_7528:eval_2escm"),(void*)f_7528},
{C_text("f_7539:eval_2escm"),(void*)f_7539},
{C_text("f_7549:eval_2escm"),(void*)f_7549},
{C_text("f_7550:eval_2escm"),(void*)f_7550},
{C_text("f_7561:eval_2escm"),(void*)f_7561},
{C_text("f_7566:eval_2escm"),(void*)f_7566},
{C_text("f_7576:eval_2escm"),(void*)f_7576},
{C_text("f_7578:eval_2escm"),(void*)f_7578},
{C_text("f_7603:eval_2escm"),(void*)f_7603},
{C_text("f_7612:eval_2escm"),(void*)f_7612},
{C_text("f_7637:eval_2escm"),(void*)f_7637},
{C_text("f_7660:eval_2escm"),(void*)f_7660},
{C_text("f_7679:eval_2escm"),(void*)f_7679},
{C_text("f_7729:eval_2escm"),(void*)f_7729},
{C_text("f_7733:eval_2escm"),(void*)f_7733},
{C_text("f_7736:eval_2escm"),(void*)f_7736},
{C_text("f_7741:eval_2escm"),(void*)f_7741},
{C_text("f_7745:eval_2escm"),(void*)f_7745},
{C_text("f_7748:eval_2escm"),(void*)f_7748},
{C_text("f_7751:eval_2escm"),(void*)f_7751},
{C_text("f_7755:eval_2escm"),(void*)f_7755},
{C_text("f_7759:eval_2escm"),(void*)f_7759},
{C_text("f_7763:eval_2escm"),(void*)f_7763},
{C_text("f_7766:eval_2escm"),(void*)f_7766},
{C_text("f_7769:eval_2escm"),(void*)f_7769},
{C_text("f_7772:eval_2escm"),(void*)f_7772},
{C_text("f_7784:eval_2escm"),(void*)f_7784},
{C_text("f_7790:eval_2escm"),(void*)f_7790},
{C_text("f_7794:eval_2escm"),(void*)f_7794},
{C_text("f_7799:eval_2escm"),(void*)f_7799},
{C_text("f_7803:eval_2escm"),(void*)f_7803},
{C_text("f_7810:eval_2escm"),(void*)f_7810},
{C_text("f_7814:eval_2escm"),(void*)f_7814},
{C_text("f_7816:eval_2escm"),(void*)f_7816},
{C_text("f_7820:eval_2escm"),(void*)f_7820},
{C_text("f_7823:eval_2escm"),(void*)f_7823},
{C_text("f_7826:eval_2escm"),(void*)f_7826},
{C_text("f_7829:eval_2escm"),(void*)f_7829},
{C_text("f_7832:eval_2escm"),(void*)f_7832},
{C_text("f_7835:eval_2escm"),(void*)f_7835},
{C_text("f_7842:eval_2escm"),(void*)f_7842},
{C_text("f_7844:eval_2escm"),(void*)f_7844},
{C_text("f_7852:eval_2escm"),(void*)f_7852},
{C_text("f_7854:eval_2escm"),(void*)f_7854},
{C_text("f_7861:eval_2escm"),(void*)f_7861},
{C_text("f_7863:eval_2escm"),(void*)f_7863},
{C_text("f_7866:eval_2escm"),(void*)f_7866},
{C_text("f_7876:eval_2escm"),(void*)f_7876},
{C_text("f_7890:eval_2escm"),(void*)f_7890},
{C_text("f_7909:eval_2escm"),(void*)f_7909},
{C_text("f_7948:eval_2escm"),(void*)f_7948},
{C_text("f_7952:eval_2escm"),(void*)f_7952},
{C_text("f_7955:eval_2escm"),(void*)f_7955},
{C_text("f_7958:eval_2escm"),(void*)f_7958},
{C_text("f_7961:eval_2escm"),(void*)f_7961},
{C_text("f_7964:eval_2escm"),(void*)f_7964},
{C_text("f_7966:eval_2escm"),(void*)f_7966},
{C_text("f_7975:eval_2escm"),(void*)f_7975},
{C_text("f_8006:eval_2escm"),(void*)f_8006},
{C_text("f_8024:eval_2escm"),(void*)f_8024},
{C_text("f_8028:eval_2escm"),(void*)f_8028},
{C_text("f_8049:eval_2escm"),(void*)f_8049},
{C_text("f_8053:eval_2escm"),(void*)f_8053},
{C_text("f_8449:eval_2escm"),(void*)f_8449},
{C_text("f_8471:eval_2escm"),(void*)f_8471},
{C_text("f_8477:eval_2escm"),(void*)f_8477},
{C_text("f_8483:eval_2escm"),(void*)f_8483},
{C_text("f_8493:eval_2escm"),(void*)f_8493},
{C_text("f_9362:eval_2escm"),(void*)f_9362},
{C_text("f_9368:eval_2escm"),(void*)f_9368},
{C_text("f_9371:eval_2escm"),(void*)f_9371},
{C_text("f_9373:eval_2escm"),(void*)f_9373},
{C_text("f_9376:eval_2escm"),(void*)f_9376},
{C_text("f_9383:eval_2escm"),(void*)f_9383},
{C_text("f_9393:eval_2escm"),(void*)f_9393},
{C_text("f_9418:eval_2escm"),(void*)f_9418},
{C_text("f_9422:eval_2escm"),(void*)f_9422},
{C_text("f_9435:eval_2escm"),(void*)f_9435},
{C_text("f_9461:eval_2escm"),(void*)f_9461},
{C_text("f_9465:eval_2escm"),(void*)f_9465},
{C_text("f_9472:eval_2escm"),(void*)f_9472},
{C_text("f_9477:eval_2escm"),(void*)f_9477},
{C_text("f_9490:eval_2escm"),(void*)f_9490},
{C_text("f_9559:eval_2escm"),(void*)f_9559},
{C_text("f_9565:eval_2escm"),(void*)f_9565},
{C_text("f_9573:eval_2escm"),(void*)f_9573},
{C_text("f_9577:eval_2escm"),(void*)f_9577},
{C_text("f_9579:eval_2escm"),(void*)f_9579},
{C_text("f_9617:eval_2escm"),(void*)f_9617},
{C_text("f_9622:eval_2escm"),(void*)f_9622},
{C_text("f_9626:eval_2escm"),(void*)f_9626},
{C_text("f_9629:eval_2escm"),(void*)f_9629},
{C_text("f_9645:eval_2escm"),(void*)f_9645},
{C_text("f_9649:eval_2escm"),(void*)f_9649},
{C_text("f_9653:eval_2escm"),(void*)f_9653},
{C_text("f_9657:eval_2escm"),(void*)f_9657},
{C_text("f_9661:eval_2escm"),(void*)f_9661},
{C_text("f_9664:eval_2escm"),(void*)f_9664},
{C_text("f_9667:eval_2escm"),(void*)f_9667},
{C_text("f_9670:eval_2escm"),(void*)f_9670},
{C_text("f_9675:eval_2escm"),(void*)f_9675},
{C_text("f_9681:eval_2escm"),(void*)f_9681},
{C_text("f_9690:eval_2escm"),(void*)f_9690},
{C_text("f_9694:eval_2escm"),(void*)f_9694},
{C_text("f_9699:eval_2escm"),(void*)f_9699},
{C_text("f_9702:eval_2escm"),(void*)f_9702},
{C_text("f_9706:eval_2escm"),(void*)f_9706},
{C_text("f_9709:eval_2escm"),(void*)f_9709},
{C_text("f_9712:eval_2escm"),(void*)f_9712},
{C_text("f_9717:eval_2escm"),(void*)f_9717},
{C_text("f_9727:eval_2escm"),(void*)f_9727},
{C_text("f_9730:eval_2escm"),(void*)f_9730},
{C_text("f_9737:eval_2escm"),(void*)f_9737},
{C_text("f_9739:eval_2escm"),(void*)f_9739},
{C_text("f_9746:eval_2escm"),(void*)f_9746},
{C_text("f_9751:eval_2escm"),(void*)f_9751},
{C_text("f_9757:eval_2escm"),(void*)f_9757},
{C_text("f_9761:eval_2escm"),(void*)f_9761},
{C_text("f_9768:eval_2escm"),(void*)f_9768},
{C_text("f_9773:eval_2escm"),(void*)f_9773},
{C_text("f_9782:eval_2escm"),(void*)f_9782},
{C_text("f_9790:eval_2escm"),(void*)f_9790},
{C_text("f_9800:eval_2escm"),(void*)f_9800},
{C_text("f_9824:eval_2escm"),(void*)f_9824},
{C_text("f_9828:eval_2escm"),(void*)f_9828},
{C_text("f_9833:eval_2escm"),(void*)f_9833},
{C_text("f_9842:eval_2escm"),(void*)f_9842},
{C_text("f_9861:eval_2escm"),(void*)f_9861},
{C_text("f_9864:eval_2escm"),(void*)f_9864},
{C_text("f_9867:eval_2escm"),(void*)f_9867},
{C_text("f_9873:eval_2escm"),(void*)f_9873},
{C_text("f_9876:eval_2escm"),(void*)f_9876},
{C_text("f_9891:eval_2escm"),(void*)f_9891},
{C_text("f_9894:eval_2escm"),(void*)f_9894},
{C_text("f_9897:eval_2escm"),(void*)f_9897},
{C_text("f_9903:eval_2escm"),(void*)f_9903},
{C_text("f_9915:eval_2escm"),(void*)f_9915},
{C_text("f_9921:eval_2escm"),(void*)f_9921},
{C_text("f_9981:eval_2escm"),(void*)f_9981},
{C_text("f_9996:eval_2escm"),(void*)f_9996},
{C_text("toplevel:eval_2escm"),(void*)C_eval_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.eval#d 
o|hiding unexported module binding: chicken.eval#define-alias 
o|hiding unexported module binding: chicken.eval#d 
o|hiding unexported module binding: chicken.eval#eval-decorator 
o|hiding unexported module binding: chicken.eval#compile-to-closure 
o|hiding unexported module binding: chicken.load#partition 
o|hiding unexported module binding: chicken.load#span 
o|hiding unexported module binding: chicken.load#take 
o|hiding unexported module binding: chicken.load#drop 
o|hiding unexported module binding: chicken.load#split-at 
o|hiding unexported module binding: chicken.load#append-map 
o|hiding unexported module binding: chicken.load#every 
o|hiding unexported module binding: chicken.load#any 
o|hiding unexported module binding: chicken.load#cons* 
o|hiding unexported module binding: chicken.load#concatenate 
o|hiding unexported module binding: chicken.load#delete 
o|hiding unexported module binding: chicken.load#first 
o|hiding unexported module binding: chicken.load#second 
o|hiding unexported module binding: chicken.load#third 
o|hiding unexported module binding: chicken.load#fourth 
o|hiding unexported module binding: chicken.load#fifth 
o|hiding unexported module binding: chicken.load#delete-duplicates 
o|hiding unexported module binding: chicken.load#alist-cons 
o|hiding unexported module binding: chicken.load#filter 
o|hiding unexported module binding: chicken.load#filter-map 
o|hiding unexported module binding: chicken.load#remove 
o|hiding unexported module binding: chicken.load#unzip1 
o|hiding unexported module binding: chicken.load#last 
o|hiding unexported module binding: chicken.load#list-index 
o|hiding unexported module binding: chicken.load#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.load#lset-difference/eq? 
o|hiding unexported module binding: chicken.load#lset-union/eq? 
o|hiding unexported module binding: chicken.load#lset-intersection/eq? 
o|hiding unexported module binding: chicken.load#list-tabulate 
o|hiding unexported module binding: chicken.load#lset<=/eq? 
o|hiding unexported module binding: chicken.load#lset=/eq? 
o|hiding unexported module binding: chicken.load#length+ 
o|hiding unexported module binding: chicken.load#find 
o|hiding unexported module binding: chicken.load#find-tail 
o|hiding unexported module binding: chicken.load#iota 
o|hiding unexported module binding: chicken.load#make-list 
o|hiding unexported module binding: chicken.load#posq 
o|hiding unexported module binding: chicken.load#posv 
o|hiding unexported module binding: chicken.load#constant2084 
o|hiding unexported module binding: chicken.load#constant2087 
o|hiding unexported module binding: chicken.load#constant2091 
o|hiding unexported module binding: chicken.load#constant2096 
o|hiding unexported module binding: chicken.load#constant2102 
o|hiding unexported module binding: chicken.load#constant2109 
o|hiding unexported module binding: chicken.load#constant2117 
o|hiding unexported module binding: chicken.load#constant2126 
o|hiding unexported module binding: chicken.load#load-library-extension 
o|hiding unexported module binding: chicken.load#constant2143 
o|hiding unexported module binding: chicken.load#default-dynamic-load-libraries 
o|hiding unexported module binding: chicken.load#path-separators 
o|hiding unexported module binding: chicken.load#path-separator-index/right 
o|hiding unexported module binding: chicken.load#make-relative-pathname 
o|hiding unexported module binding: chicken.load#toplevel 
o|hiding unexported module binding: chicken.load#c-toplevel 
o|hiding unexported module binding: chicken.load#load/internal 
o|hiding unexported module binding: chicken.load#load-library/internal 
o|hiding unexported module binding: chicken.load#file-exists? 
o|hiding unexported module binding: chicken.load#find-file 
o|hiding unexported module binding: chicken.load#find-dynamic-extension 
o|hiding unexported module binding: chicken.load#load-extension/internal 
S|applied compiler syntax:
S|  chicken.base#foldl		3
S|  chicken.base#foldr		4
S|  scheme#for-each		7
S|  ##sys#map		4
S|  scheme#map		17
o|eliminated procedure checks: 311 
o|folded constant expression: (scheme#integer->char (quote 127)) 
o|specializations:
o|  1 (scheme#number->string fixnum)
o|  1 (##sys#debug-mode?)
o|  1 (scheme#positive? fixnum)
o|  1 (scheme#eqv? * *)
o|  3 (scheme#memq * list)
o|  1 (scheme#caddr (pair * (pair * pair)))
o|  1 (scheme#length list)
o|  3 (scheme#cddr (pair * pair))
o|  13 (scheme#car pair)
o|  17 (##sys#check-list (or pair list) *)
o|  1 (scheme#zero? *)
o|  8 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  81 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  26 (scheme#cdr pair)
o|  2 (scheme#cdar (pair pair *))
(o e)|safe calls: 1105 
(o e)|assignments to immediate values: 1 
o|safe globals: (##sys#unbound-in-eval chicken.eval#eval-decorator) 
o|merged explicitly consed rest parameter: rest150153 
o|inlining procedure: k3601 
o|inlining procedure: k3601 
o|inlining procedure: k3639 
o|inlining procedure: k3639 
o|inlining procedure: k3654 
o|inlining procedure: k3654 
o|inlining procedure: k3682 
o|inlining procedure: k3682 
o|contracted procedure: "(eval.scm:103) posq199" 
o|inlining procedure: k3729 
o|inlining procedure: k3729 
o|inlining procedure: k3759 
o|inlining procedure: k3759 
o|inlining procedure: k3773 
o|inlining procedure: k3773 
o|contracted procedure: "(eval.scm:129) chicken.eval#eval-decorator" 
o|inlining procedure: k3536 
o|inlining procedure: k3536 
o|inlining procedure: k3789 
o|inlining procedure: k3789 
o|inlining procedure: k3815 
o|inlining procedure: k3838 
o|inlining procedure: k3838 
o|inlining procedure: k3865 
o|inlining procedure: k3865 
o|inlining procedure: k3878 
o|inlining procedure: k3878 
o|inlining procedure: k3815 
o|inlining procedure: k3916 
o|inlining procedure: k3916 
o|inlining procedure: k3958 
o|inlining procedure: k3958 
o|substituted constant variable: a3992 
o|substituted constant variable: a3994 
o|substituted constant variable: a3996 
o|substituted constant variable: a3998 
o|inlining procedure: k3999 
o|inlining procedure: k4013 
o|inlining procedure: k4013 
o|inlining procedure: k4029 
o|inlining procedure: k4029 
o|substituted constant variable: a4040 
o|substituted constant variable: a4042 
o|substituted constant variable: a4044 
o|substituted constant variable: a4046 
o|inlining procedure: k3999 
o|inlining procedure: k4053 
o|inlining procedure: k4053 
o|inlining procedure: k4060 
o|inlining procedure: k4060 
o|inlining procedure: k4080 
o|inlining procedure: k4107 
o|inlining procedure: k4124 
o|inlining procedure: k4124 
o|inlining procedure: k4140 
o|inlining procedure: k4140 
o|inlining procedure: k4156 
o|inlining procedure: k4156 
o|substituted constant variable: a4175 
o|substituted constant variable: a4177 
o|substituted constant variable: a4179 
o|substituted constant variable: a4181 
o|substituted constant variable: a4183 
o|substituted constant variable: a4185 
o|substituted constant variable: a4187 
o|inlining procedure: k4107 
o|inlining procedure: k4203 
o|inlining procedure: k4203 
o|inlining procedure: k4229 
o|inlining procedure: k4229 
o|inlining procedure: k4254 
o|inlining procedure: k4254 
o|inlining procedure: k4291 
o|inlining procedure: k4312 
o|inlining procedure: k4312 
o|substituted constant variable: a4394 
o|substituted constant variable: a4396 
o|substituted constant variable: a4398 
o|inlining procedure: k4291 
o|inlining procedure: k4418 
o|inlining procedure: k4442 
o|inlining procedure: k4473 
o|inlining procedure: k4473 
o|inlining procedure: k4442 
o|inlining procedure: k4418 
o|inlining procedure: k4565 
o|inlining procedure: k4565 
o|inlining procedure: k4648 
o|inlining procedure: k4648 
o|inlining procedure: k4832 
o|inlining procedure: k4832 
o|inlining procedure: k4862 
o|inlining procedure: k4862 
o|substituted constant variable: a4894 
o|substituted constant variable: a4896 
o|substituted constant variable: a4898 
o|substituted constant variable: a4900 
o|consed rest parameter at call site: "(eval.scm:287) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k4953 
o|inlining procedure: k4953 
o|inlining procedure: k4987 
o|contracted procedure: "(eval.scm:282) g423432" 
o|inlining procedure: k4987 
o|inlining procedure: k5018 
o|inlining procedure: k5098 
o|contracted procedure: "(eval.scm:337) g586595" 
o|inlining procedure: k5098 
o|inlining procedure: k5132 
o|contracted procedure: "(eval.scm:334) g559568" 
o|inlining procedure: k5132 
o|inlining procedure: k5018 
o|inlining procedure: k5274 
o|contracted procedure: "(eval.scm:354) g735745" 
o|inlining procedure: k5274 
o|inlining procedure: k5322 
o|contracted procedure: "(eval.scm:353) g702712" 
o|inlining procedure: k5322 
o|inlining procedure: k5370 
o|contracted procedure: "(eval.scm:350) g675684" 
o|inlining procedure: k5370 
o|inlining procedure: k5404 
o|inlining procedure: k5404 
o|inlining procedure: k5438 
o|inlining procedure: k5438 
o|inlining procedure: k5469 
o|inlining procedure: k5513 
o|inlining procedure: k5513 
o|inlining procedure: k5562 
o|inlining procedure: k5562 
o|inlining procedure: k5603 
o|inlining procedure: k5603 
o|inlining procedure: k5656 
o|inlining procedure: k5656 
o|inlining procedure: k5697 
o|inlining procedure: k5697 
o|contracted procedure: "(eval.scm:443) fudge-argument-list204" 
o|inlining procedure: k7251 
o|inlining procedure: k7251 
o|inlining procedure: k7266 
o|inlining procedure: k7266 
o|inlining procedure: k7291 
o|inlining procedure: k7291 
o|inlining procedure: k5779 
o|inlining procedure: k5779 
o|substituted constant variable: a5804 
o|substituted constant variable: a5806 
o|substituted constant variable: a5808 
o|substituted constant variable: a5810 
o|substituted constant variable: a5812 
o|consed rest parameter at call site: "(eval.scm:378) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k5841 
o|consed rest parameter at call site: "(eval.scm:378) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k5841 
o|consed rest parameter at call site: "(eval.scm:378) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k5861 
o|inlining procedure: k5861 
o|inlining procedure: k5914 
o|inlining procedure: k5914 
o|inlining procedure: k5469 
o|inlining procedure: k6029 
o|contracted procedure: "(eval.scm:458) g889898" 
o|inlining procedure: k6029 
o|inlining procedure: k6060 
o|inlining procedure: k6174 
o|inlining procedure: k6174 
o|inlining procedure: k6201 
o|contracted procedure: "(eval.scm:472) g924933" 
o|inlining procedure: k6201 
o|inlining procedure: k6060 
o|inlining procedure: k6288 
o|inlining procedure: k6288 
o|inlining procedure: k6318 
o|inlining procedure: k6342 
o|inlining procedure: k6342 
o|inlining procedure: k6318 
o|inlining procedure: k6417 
o|contracted procedure: "(eval.scm:528) g9971006" 
o|inlining procedure: k6417 
o|substituted constant variable: saved107210731091 
o|inlining procedure: k6448 
o|inlining procedure: k6553 
o|inlining procedure: k6576 
o|inlining procedure: k6576 
o|inlining procedure: k6553 
o|inlining procedure: k6672 
o|inlining procedure: k6672 
o|inlining procedure: k6699 
o|inlining procedure: k6699 
o|inlining procedure: k6729 
o|inlining procedure: k6729 
o|inlining procedure: k6448 
o|inlining procedure: k6785 
o|inlining procedure: k6785 
o|inlining procedure: k6822 
o|inlining procedure: k6822 
o|inlining procedure: k6867 
o|inlining procedure: k6867 
o|inlining procedure: k6892 
o|inlining procedure: k6892 
o|inlining procedure: k6928 
o|inlining procedure: k6928 
o|removed unused parameter to known procedure: se1250 "(eval.scm:628) compile-call206" 
o|inlining procedure: k6957 
o|inlining procedure: k6957 
o|inlining procedure: k6979 
o|inlining procedure: k6979 
o|removed unused parameter to known procedure: se1250 "(eval.scm:646) compile-call206" 
o|substituted constant variable: a7032 
o|substituted constant variable: a7034 
o|substituted constant variable: a7036 
o|inlining procedure: k7040 
o|inlining procedure: k7040 
o|inlining procedure: k7052 
o|inlining procedure: k7052 
o|inlining procedure: k7064 
o|inlining procedure: k7064 
o|inlining procedure: k7076 
o|inlining procedure: k7076 
o|inlining procedure: k7088 
o|inlining procedure: k7088 
o|substituted constant variable: a7095 
o|substituted constant variable: a7097 
o|substituted constant variable: a7099 
o|substituted constant variable: a7101 
o|substituted constant variable: a7103 
o|substituted constant variable: a7105 
o|substituted constant variable: a7107 
o|substituted constant variable: a7109 
o|substituted constant variable: a7111 
o|substituted constant variable: a7113 
o|substituted constant variable: a7115 
o|substituted constant variable: a7120 
o|substituted constant variable: a7122 
o|substituted constant variable: a7124 
o|substituted constant variable: a7129 
o|substituted constant variable: a7131 
o|substituted constant variable: a7133 
o|substituted constant variable: a7138 
o|substituted constant variable: a7140 
o|substituted constant variable: a7142 
o|substituted constant variable: a7144 
o|substituted constant variable: a7146 
o|substituted constant variable: a7148 
o|substituted constant variable: a7150 
o|substituted constant variable: a7152 
o|substituted constant variable: a7154 
o|substituted constant variable: a7156 
o|substituted constant variable: a7158 
o|substituted constant variable: a7160 
o|substituted constant variable: a7162 
o|substituted constant variable: a7164 
o|substituted constant variable: a7166 
o|substituted constant variable: a7168 
o|substituted constant variable: a7170 
o|substituted constant variable: a7172 
o|substituted constant variable: a7174 
o|substituted constant variable: a7176 
o|substituted constant variable: a7178 
o|substituted constant variable: a7180 
o|substituted constant variable: a7182 
o|substituted constant variable: a7184 
o|substituted constant variable: a7186 
o|substituted constant variable: a7188 
o|substituted constant variable: a7190 
o|inlining procedure: k4080 
o|removed unused parameter to known procedure: se1250 "(eval.scm:650) compile-call206" 
o|inlining procedure: k7224 
o|inlining procedure: k7224 
o|inlining procedure: k7236 
o|inlining procedure: k7236 
o|removed unused formal parameters: (se1250) 
o|inlining procedure: k7350 
o|inlining procedure: k7350 
o|inlining procedure: k7376 
o|inlining procedure: k7376 
o|inlining procedure: k7440 
o|inlining procedure: k7440 
o|inlining procedure: k7580 
o|inlining procedure: k7580 
o|inlining procedure: k7614 
o|inlining procedure: k7614 
o|substituted constant variable: a7646 
o|substituted constant variable: a7648 
o|substituted constant variable: a7650 
o|substituted constant variable: a7652 
o|substituted constant variable: a7654 
o|substituted constant variable: a7656 
o|contracted procedure: "(eval.scm:678) checked-length205" 
o|inlining procedure: k7312 
o|inlining procedure: k7312 
o|substituted constant variable: saved136713681382 
o|consed rest parameter at call site: "(eval.scm:723) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k7878 
o|inlining procedure: k7878 
o|inlining procedure: k7900 
o|inlining procedure: k7900 
o|inlining procedure: k7977 
o|contracted procedure: "(eval.scm:832) g15281529" 
o|substituted constant variable: a7992 
o|inlining procedure: k7985 
o|inlining procedure: k7985 
o|inlining procedure: k7977 
o|inlining procedure: k8029 
o|inlining procedure: k8029 
o|substituted constant variable: a8045 
o|substituted constant variable: a8047 
o|inlining procedure: k8054 
o|inlining procedure: k8054 
o|substituted constant variable: a8070 
o|substituted constant variable: a8072 
o|removed side-effect free assignment to unused variable: chicken.load#partition 
o|removed side-effect free assignment to unused variable: chicken.load#span 
o|removed side-effect free assignment to unused variable: chicken.load#drop 
o|removed side-effect free assignment to unused variable: chicken.load#split-at 
o|removed side-effect free assignment to unused variable: chicken.load#append-map 
o|inlining procedure: k8454 
o|inlining procedure: k8454 
o|inlining procedure: k8485 
o|inlining procedure: k8485 
o|removed side-effect free assignment to unused variable: chicken.load#cons* 
o|removed side-effect free assignment to unused variable: chicken.load#concatenate 
o|removed side-effect free assignment to unused variable: chicken.load#first 
o|removed side-effect free assignment to unused variable: chicken.load#second 
o|removed side-effect free assignment to unused variable: chicken.load#third 
o|removed side-effect free assignment to unused variable: chicken.load#fourth 
o|removed side-effect free assignment to unused variable: chicken.load#fifth 
o|removed side-effect free assignment to unused variable: chicken.load#delete-duplicates 
o|removed side-effect free assignment to unused variable: chicken.load#alist-cons 
o|inlining procedure: k8702 
o|inlining procedure: k8702 
o|inlining procedure: k8694 
o|inlining procedure: k8694 
o|removed side-effect free assignment to unused variable: chicken.load#filter-map 
o|removed side-effect free assignment to unused variable: chicken.load#remove 
o|removed side-effect free assignment to unused variable: chicken.load#unzip1 
o|removed side-effect free assignment to unused variable: chicken.load#last 
o|removed side-effect free assignment to unused variable: chicken.load#list-index 
o|removed side-effect free assignment to unused variable: chicken.load#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.load#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.load#lset-union/eq? 
o|removed side-effect free assignment to unused variable: chicken.load#lset-intersection/eq? 
o|inlining procedure: k9093 
o|inlining procedure: k9093 
o|removed side-effect free assignment to unused variable: chicken.load#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.load#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.load#length+ 
o|removed side-effect free assignment to unused variable: chicken.load#find 
o|removed side-effect free assignment to unused variable: chicken.load#find-tail 
o|removed side-effect free assignment to unused variable: chicken.load#iota 
o|removed side-effect free assignment to unused variable: chicken.load#make-list 
o|removed side-effect free assignment to unused variable: chicken.load#posq 
o|removed side-effect free assignment to unused variable: chicken.load#posv 
o|inlining procedure: k9395 
o|inlining procedure: k9395 
o|inlining procedure: k9423 
o|inlining procedure: k9423 
o|inlining procedure: k9439 
o|inlining procedure: k9439 
o|inlining procedure: k9479 
o|inlining procedure: k9502 
o|inlining procedure: k9502 
o|inlining procedure: k9516 
o|inlining procedure: k9516 
o|substituted constant variable: a9531 
o|substituted constant variable: a9533 
o|substituted constant variable: a9535 
o|substituted constant variable: a9537 
o|inlining procedure: k9479 
o|contracted procedure: "(eval.scm:1009) chicken.load#toplevel" 
o|inlining procedure: k9547 
o|inlining procedure: k9547 
o|merged explicitly consed rest parameter: rest22082211 
o|inlining procedure: k9612 
o|inlining procedure: k9612 
o|inlining procedure: k9630 
o|inlining procedure: k9630 
o|inlining procedure: k9668 
o|inlining procedure: k9668 
o|inlining procedure: k9719 
o|inlining procedure: k9719 
o|inlining procedure: k9741 
o|inlining procedure: k9741 
o|inlining procedure: k9775 
o|inlining procedure: k9792 
o|inlining procedure: k9792 
o|inlining procedure: k9775 
o|inlining procedure: k9829 
o|inlining procedure: k9829 
o|inlining procedure: k9850 
o|inlining procedure: k9850 
o|inlining procedure: k9877 
o|inlining procedure: k9877 
o|inlining procedure: k9898 
o|inlining procedure: k9898 
o|inlining procedure: k9916 
o|inlining procedure: k9916 
o|consed rest parameter at call site: "(eval.scm:1099) chicken.load#load/internal" 3 
o|inlining procedure: k9983 
o|consed rest parameter at call site: "(eval.scm:1099) chicken.load#load/internal" 3 
o|inlining procedure: k9983 
o|consed rest parameter at call site: "(eval.scm:1099) chicken.load#load/internal" 3 
o|consed rest parameter at call site: "(eval.scm:1103) chicken.load#load/internal" 3 
o|consed rest parameter at call site: "(eval.scm:1106) chicken.load#load/internal" 3 
o|propagated global variable: g23842385 ##sys#string-append 
o|inlining procedure: k10068 
o|inlining procedure: k10068 
o|inlining procedure: k10147 
o|inlining procedure: k10147 
o|inlining procedure: k10246 
o|inlining procedure: k10246 
o|inlining procedure: k10291 
o|inlining procedure: k10291 
o|inlining procedure: k10300 
o|inlining procedure: k10300 
o|inlining procedure: k10312 
o|inlining procedure: k10312 
o|inlining procedure: k10362 
o|inlining procedure: k10362 
o|inlining procedure: k10372 
o|inlining procedure: k10372 
o|inlining procedure: k10401 
o|inlining procedure: k10401 
o|inlining procedure: k10436 
o|inlining procedure: k10436 
o|contracted procedure: "(eval.scm:1223) chicken.load#load-extension/internal" 
o|inlining procedure: k10451 
o|inlining procedure: k10451 
o|inlining procedure: k10460 
o|inlining procedure: k10460 
o|consed rest parameter at call site: "(eval.scm:1217) chicken.load#load/internal" 3 
o|inlining procedure: k10517 
o|contracted procedure: "(eval.scm:1228) g25732595" 
o|propagated global variable: g26032604 chicken.load#load-extension 
o|inlining procedure: k10517 
o|inlining procedure: k10540 
o|contracted procedure: "(eval.scm:1227) g25632580" 
o|inlining procedure: k10540 
o|inlining procedure: k10582 
o|contracted procedure: "(eval.scm:1232) g26262648" 
o|propagated global variable: g26562657 ##sys#provide 
o|inlining procedure: k10582 
o|inlining procedure: k10605 
o|contracted procedure: "(eval.scm:1231) g26162633" 
o|inlining procedure: k10605 
o|inlining procedure: k10642 
o|contracted procedure: "(eval.scm:1235) g26692676" 
o|inlining procedure: k10642 
o|inlining procedure: k10703 
o|contracted procedure: "(eval.scm:1250) g27242725" 
o|inlining procedure: k10703 
o|inlining procedure: k10727 
o|inlining procedure: k10727 
o|inlining procedure: k10745 
o|inlining procedure: k10745 
o|substituted constant variable: chicken.load#constant2143 
o|substituted constant variable: chicken.load#constant2084 
o|inlining procedure: k10891 
o|inlining procedure: k10891 
o|inlining procedure: k10928 
o|inlining procedure: k10928 
o|inlining procedure: k10943 
o|inlining procedure: k10943 
o|inlining procedure: k10962 
o|inlining procedure: k10962 
o|inlining procedure: k10975 
o|inlining procedure: k10975 
o|inlining procedure: k11009 
o|inlining procedure: k11009 
o|inlining procedure: k11082 
o|inlining procedure: k11082 
o|inlining procedure: k11145 
o|inlining procedure: k11145 
o|propagated global variable: tmp29622964 last-error 
o|inlining procedure: k11301 
o|propagated global variable: tmp29622964 last-error 
o|inlining procedure: k11301 
o|inlining procedure: k11324 
o|inlining procedure: k11324 
o|substituted constant variable: chicken.load#constant2091 
o|substituted constant variable: a11379 
o|substituted constant variable: chicken.load#constant2102 
o|inlining procedure: k11383 
o|substituted constant variable: chicken.load#constant2096 
o|propagated global variable: r1138412002 chicken.load#constant2096 
o|inlining procedure: k11383 
o|substituted constant variable: chicken.load#constant2109 
o|inlining procedure: k11392 
o|substituted constant variable: chicken.load#constant2109 
o|inlining procedure: k11392 
o|substituted constant variable: chicken.load#constant2109 
o|inlining procedure: k11513 
o|inlining procedure: k11513 
o|inlining procedure: k11550 
o|substituted constant variable: saved143614371446 
o|consed rest parameter at call site: "(eval.scm:757) chicken.eval#compile-to-closure" 3 
o|consed rest parameter at call site: "(eval.scm:758) chicken.eval#compile-to-closure" 3 
o|inlining procedure: k11550 
o|consed rest parameter at call site: "(eval.scm:761) chicken.eval#compile-to-closure" 3 
o|consed rest parameter at call site: "(eval.scm:747) chicken.eval#compile-to-closure" 3 
o|simplifications: ((if . 1)) 
o|replaced variables: 1986 
o|removed binding forms: 455 
o|substituted constant variable: r360211661 
o|substituted constant variable: r373011669 
o|removed call to pure procedure with unused result: "(eval.scm:100) chicken.base#void" 
o|substituted constant variable: r353711676 
o|substituted constant variable: r447411723 
o|substituted constant variable: r591511783 
o|substituted constant variable: r591511783 
o|removed call to pure procedure with unused result: "(eval.scm:195) chicken.base#void" 
o|converted assignments to bindings: (err1496) 
o|substituted constant variable: r797811865 
o|substituted constant variable: r848611872 
o|removed side-effect free assignment to unused variable: chicken.load#filter 
o|removed side-effect free assignment to unused variable: chicken.load#list-tabulate 
o|removed side-effect free assignment to unused variable: chicken.load#constant2084 
o|removed side-effect free assignment to unused variable: chicken.load#constant2091 
o|removed side-effect free assignment to unused variable: chicken.load#constant2102 
o|removed side-effect free assignment to unused variable: chicken.load#constant2109 
o|removed side-effect free assignment to unused variable: chicken.load#constant2143 
o|substituted constant variable: r944011885 
o|substituted constant variable: r954811892 
o|contracted procedure: "(eval.scm:1088) g23082315" 
o|substituted constant variable: r983011912 
o|substituted constant variable: r983011912 
o|substituted constant variable: r985111915 
o|substituted constant variable: r991711921 
o|substituted constant variable: r998411922 
o|substituted constant variable: r998411922 
o|inlining procedure: k10189 
o|substituted constant variable: r1029211939 
o|substituted constant variable: r1030111940 
o|substituted constant variable: r1037311947 
o|substituted constant variable: r1040211949 
o|substituted constant variable: r1043711950 
o|substituted constant variable: r1043711950 
o|substituted constant variable: r1043711952 
o|substituted constant variable: r1043711952 
o|converted assignments to bindings: (check2512) 
o|contracted procedure: "(eval.scm:1236) chicken.load#every" 
o|substituted constant variable: r1097611984 
o|substituted constant variable: r1101011988 
o|substituted constant variable: r1101011988 
o|substituted constant variable: r1114611994 
o|propagated global variable: r1130211996 last-error 
o|substituted constant variable: r1130211998 
o|substituted constant variable: r1130211998 
o|converted assignments to bindings: (complete2377) 
o|substituted constant variable: chicken.load#constant2096 
o|substituted constant variable: r1139312005 
o|converted assignments to bindings: (strip1517) 
o|simplifications: ((let . 4)) 
o|replaced variables: 137 
o|removed binding forms: 1671 
o|removed conditional forms: 1 
o|contracted procedure: k3673 
o|removed unused formal parameters: (h262 cntr263) 
o|contracted procedure: k4092 
o|inlining procedure: k4405 
o|removed unused parameter to known procedure: h262 "(eval.scm:384) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:384) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:389) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:389) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:394) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:394) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:399) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:399) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:405) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:405) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:410) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:410) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:416) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:416) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:421) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:421) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:427) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:427) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:432) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:432) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:439) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:439) decorate202" 
o|removed unused parameter to known procedure: h262 "(eval.scm:446) decorate202" 
o|removed unused parameter to known procedure: cntr263 "(eval.scm:446) decorate202" 
o|inlining procedure: k6271 
o|removed side-effect free assignment to unused variable: chicken.load#constant2096 
o|inlining procedure: k9488 
o|inlining procedure: k9488 
o|inlining procedure: k9488 
o|inlining procedure: k9488 
o|inlining procedure: k9575 
o|inlining procedure: k9904 
o|inlining procedure: k9904 
o|inlining procedure: k10492 
o|inlining procedure: k10492 
o|propagated global variable: r1130211996 last-error 
o|replaced variables: 4 
o|removed binding forms: 142 
o|substituted constant variable: r627212203 
o|substituted constant variable: r957612250 
o|substituted constant variable: r990512256 
o|substituted constant variable: r990512256 
o|replaced variables: 2 
o|removed binding forms: 15 
o|removed conditional forms: 2 
o|removed binding forms: 4 
o|simplifications: ((if . 62) (##core#call . 736) (let . 28)) 
o|  call simplifications:
o|    ##sys#check-structure
o|    scheme#memq	5
o|    ##sys#check-symbol	4
o|    ##sys#check-string
o|    scheme#call-with-current-continuation
o|    ##sys#size	3
o|    chicken.fixnum#fx<
o|    chicken.fixnum#fx>
o|    scheme#procedure?
o|    scheme#boolean?
o|    scheme#char?
o|    scheme#eof-object?	3
o|    scheme#string?	4
o|    scheme#vector?
o|    ##sys#void
o|    scheme#set-car!
o|    chicken.fixnum#fx=	2
o|    chicken.fixnum#fx-	3
o|    scheme#apply	7
o|    scheme#list	14
o|    ##sys#list	20
o|    ##sys#check-list	15
o|    chicken.fixnum#fx>=	2
o|    scheme#cddr	8
o|    scheme#car	37
o|    scheme#vector	12
o|    scheme#cdr	24
o|    scheme#length	2
o|    ##sys#cons	18
o|    scheme#caddr	7
o|    scheme#cdddr	4
o|    scheme#pair?	38
o|    scheme#cadddr	4
o|    scheme#cadr	33
o|    scheme#assq	5
o|    scheme#cons	64
o|    ##sys#setslot	28
o|    ##sys#immediate?
o|    scheme#not	26
o|    ##sys#make-structure	2
o|    ##sys#slot	140
o|    chicken.fixnum#fx+	6
o|    scheme#values	11
o|    ##sys#call-with-values	9
o|    ##sys#apply	4
o|    scheme#null?	48
o|    scheme#caar
o|    scheme#eq?	102
o|    scheme#symbol?	9
o|contracted procedure: k7722 
o|contracted procedure: k3574 
o|contracted procedure: k7716 
o|contracted procedure: k3577 
o|contracted procedure: k7710 
o|contracted procedure: k3580 
o|contracted procedure: k7704 
o|contracted procedure: k3583 
o|contracted procedure: k7698 
o|contracted procedure: k3586 
o|contracted procedure: k7692 
o|contracted procedure: k3589 
o|contracted procedure: k7686 
o|contracted procedure: k3592 
o|contracted procedure: k7680 
o|contracted procedure: k3595 
o|contracted procedure: k3604 
o|contracted procedure: k3630 
o|contracted procedure: k3620 
o|contracted procedure: k3685 
o|contracted procedure: k3709 
o|contracted procedure: k3713 
o|contracted procedure: k3717 
o|contracted procedure: k3732 
o|contracted procedure: k3753 
o|contracted procedure: k3738 
o|contracted procedure: k3745 
o|contracted procedure: k3749 
o|contracted procedure: k3763 
o|contracted procedure: k3767 
o|contracted procedure: k3777 
o|contracted procedure: k3543 
o|contracted procedure: k3539 
o|contracted procedure: k3549 
o|contracted procedure: k3800 
o|contracted procedure: k3818 
o|contracted procedure: k3830 
o|contracted procedure: k3859 
o|contracted procedure: k3855 
o|contracted procedure: k3862 
o|contracted procedure: k3898 
o|contracted procedure: k3875 
o|contracted procedure: k3881 
o|contracted procedure: k3887 
o|contracted procedure: k3904 
o|contracted procedure: k3913 
o|contracted procedure: k3919 
o|contracted procedure: k3932 
o|contracted procedure: k3928 
o|contracted procedure: k3938 
o|contracted procedure: k3955 
o|contracted procedure: k3951 
o|contracted procedure: k3947 
o|contracted procedure: k3961 
o|contracted procedure: k3982 
o|contracted procedure: k3978 
o|contracted procedure: k3974 
o|contracted procedure: k3970 
o|contracted procedure: k4008 
o|contracted procedure: k4016 
o|contracted procedure: k4024 
o|contracted procedure: k4032 
o|contracted procedure: k4050 
o|contracted procedure: k4063 
o|contracted procedure: k7218 
o|contracted procedure: k4074 
o|contracted procedure: k7214 
o|contracted procedure: k4083 
o|contracted procedure: k7196 
o|contracted procedure: k4098 
o|contracted procedure: k4110 
o|contracted procedure: k4119 
o|contracted procedure: k4127 
o|contracted procedure: k4135 
o|contracted procedure: k4143 
o|contracted procedure: k4151 
o|contracted procedure: k4159 
o|contracted procedure: k4167 
o|contracted procedure: k4189 
o|contracted procedure: k4195 
o|contracted procedure: k4198 
o|contracted procedure: k4206 
o|contracted procedure: k4213 
o|contracted procedure: k4219 
o|contracted procedure: k4226 
o|contracted procedure: k4232 
o|contracted procedure: k4240 
o|contracted procedure: k4280 
o|contracted procedure: k4266 
o|contracted procedure: k4273 
o|contracted procedure: k4284 
o|contracted procedure: k4288 
o|contracted procedure: k4294 
o|contracted procedure: k4297 
o|contracted procedure: k4300 
o|contracted procedure: k4306 
o|contracted procedure: k4315 
o|contracted procedure: k4322 
o|contracted procedure: k4328 
o|contracted procedure: k4346 
o|contracted procedure: k4350 
o|contracted procedure: k4382 
o|contracted procedure: k4378 
o|contracted procedure: k4374 
o|contracted procedure: k4386 
o|contracted procedure: k4390 
o|contracted procedure: k4402 
o|contracted procedure: k4415 
o|contracted procedure: k4421 
o|contracted procedure: k4424 
o|contracted procedure: k4445 
o|contracted procedure: k4470 
o|contracted procedure: k4486 
o|contracted procedure: k4476 
o|contracted procedure: k4503 
o|contracted procedure: k4521 
o|contracted procedure: k4527 
o|contracted procedure: k4530 
o|contracted procedure: k4533 
o|contracted procedure: k4536 
o|contracted procedure: k4547 
o|contracted procedure: k4550 
o|contracted procedure: k4556 
o|contracted procedure: k4568 
o|contracted procedure: k4584 
o|contracted procedure: k4580 
o|contracted procedure: k4596 
o|contracted procedure: k4602 
o|contracted procedure: k4621 
o|contracted procedure: k4617 
o|contracted procedure: k4637 
o|contracted procedure: k4645 
o|contracted procedure: k4651 
o|contracted procedure: k4660 
o|contracted procedure: k4676 
o|contracted procedure: k4672 
o|contracted procedure: k4696 
o|contracted procedure: k4704 
o|contracted procedure: k4712 
o|contracted procedure: k4718 
o|contracted procedure: k4727 
o|contracted procedure: k4746 
o|contracted procedure: k4742 
o|contracted procedure: k4770 
o|contracted procedure: k4778 
o|contracted procedure: k4786 
o|contracted procedure: k4794 
o|contracted procedure: k4797 
o|contracted procedure: k4806 
o|contracted procedure: k4826 
o|contracted procedure: k4835 
o|contracted procedure: k4838 
o|contracted procedure: k4845 
o|contracted procedure: k4849 
o|contracted procedure: k4856 
o|contracted procedure: k4865 
o|contracted procedure: k4868 
o|contracted procedure: k4871 
o|contracted procedure: k4879 
o|contracted procedure: k4887 
o|contracted procedure: k4929 
o|contracted procedure: k4956 
o|contracted procedure: k4959 
o|contracted procedure: k4962 
o|contracted procedure: k4970 
o|contracted procedure: k4978 
o|contracted procedure: k4990 
o|contracted procedure: k5012 
o|contracted procedure: k5008 
o|contracted procedure: k4993 
o|contracted procedure: k4996 
o|contracted procedure: k5004 
o|contracted procedure: k5021 
o|contracted procedure: k5024 
o|contracted procedure: k5041 
o|contracted procedure: k5053 
o|contracted procedure: k5037 
o|contracted procedure: k5033 
o|contracted procedure: k5064 
o|contracted procedure: k5092 
o|contracted procedure: k5088 
o|contracted procedure: k5084 
o|contracted procedure: k5101 
o|contracted procedure: k5123 
o|contracted procedure: k5073 
o|contracted procedure: k5077 
o|contracted procedure: k5119 
o|contracted procedure: k5104 
o|contracted procedure: k5107 
o|contracted procedure: k5115 
o|contracted procedure: k5135 
o|contracted procedure: k5157 
o|contracted procedure: k5050 
o|contracted procedure: k5153 
o|contracted procedure: k5138 
o|contracted procedure: k5141 
o|contracted procedure: k5149 
o|contracted procedure: k5166 
o|contracted procedure: k5169 
o|contracted procedure: k5172 
o|contracted procedure: k5175 
o|contracted procedure: k5181 
o|contracted procedure: k5184 
o|contracted procedure: k5190 
o|contracted procedure: k5201 
o|contracted procedure: k5225 
o|contracted procedure: k5237 
o|contracted procedure: k5221 
o|contracted procedure: k5217 
o|contracted procedure: k5197 
o|contracted procedure: k5248 
o|contracted procedure: k5268 
o|contracted procedure: k5264 
o|contracted procedure: k5260 
o|contracted procedure: k5313 
o|contracted procedure: k5277 
o|contracted procedure: k5303 
o|contracted procedure: k5307 
o|contracted procedure: k5299 
o|contracted procedure: k5280 
o|contracted procedure: k5283 
o|contracted procedure: k5291 
o|contracted procedure: k5295 
o|contracted procedure: k5361 
o|contracted procedure: k5325 
o|contracted procedure: k5351 
o|contracted procedure: k5355 
o|contracted procedure: k5234 
o|contracted procedure: k5347 
o|contracted procedure: k5328 
o|contracted procedure: k5331 
o|contracted procedure: k5339 
o|contracted procedure: k5343 
o|contracted procedure: k5373 
o|contracted procedure: k5395 
o|contracted procedure: k5210 
o|contracted procedure: k5391 
o|contracted procedure: k5376 
o|contracted procedure: k5379 
o|contracted procedure: k5387 
o|contracted procedure: k5407 
o|contracted procedure: k5410 
o|contracted procedure: k5413 
o|contracted procedure: k5421 
o|contracted procedure: k5429 
o|contracted procedure: k5441 
o|contracted procedure: k5463 
o|contracted procedure: k5459 
o|contracted procedure: k5444 
o|contracted procedure: k5447 
o|contracted procedure: k5455 
o|contracted procedure: k5472 
o|contracted procedure: k5478 
o|contracted procedure: k5483 
o|contracted procedure: k5495 
o|contracted procedure: k5498 
o|contracted procedure: k5507 
o|contracted procedure: k5516 
o|contracted procedure: k5538 
o|contracted procedure: k5534 
o|contracted procedure: k5553 
o|contracted procedure: k5559 
o|contracted procedure: k5581 
o|contracted procedure: k5577 
o|contracted procedure: k5600 
o|contracted procedure: k5596 
o|contracted procedure: k5606 
o|contracted procedure: k5628 
o|contracted procedure: k5624 
o|contracted procedure: k5647 
o|contracted procedure: k5643 
o|contracted procedure: k5653 
o|contracted procedure: k5675 
o|contracted procedure: k5671 
o|contracted procedure: k5694 
o|contracted procedure: k5690 
o|contracted procedure: k5700 
o|contracted procedure: k5722 
o|contracted procedure: k5718 
o|contracted procedure: k5737 
o|contracted procedure: k5759 
o|contracted procedure: k7254 
o|inlining procedure: k5767 
o|contracted procedure: k7269 
o|contracted procedure: k7276 
o|contracted procedure: k7272 
o|contracted procedure: k7283 
o|contracted procedure: k7287 
o|contracted procedure: k7294 
o|inlining procedure: k7291 
o|contracted procedure: k5800 
o|contracted procedure: k5782 
o|contracted procedure: k5792 
o|contracted procedure: k5864 
o|contracted procedure: k5867 
o|contracted procedure: k5870 
o|contracted procedure: k5878 
o|contracted procedure: k5886 
o|contracted procedure: k5924 
o|contracted procedure: k5961 
o|contracted procedure: k5979 
o|contracted procedure: k6013 
o|contracted procedure: k6016 
o|contracted procedure: k6032 
o|contracted procedure: k6054 
o|contracted procedure: k5988 
o|contracted procedure: k6050 
o|contracted procedure: k6035 
o|contracted procedure: k6038 
o|contracted procedure: k6046 
o|contracted procedure: k6010 
o|contracted procedure: k6063 
o|contracted procedure: k6066 
o|contracted procedure: k6096 
o|contracted procedure: k6099 
o|contracted procedure: k6114 
o|contracted procedure: k6117 
o|contracted procedure: k6154 
o|contracted procedure: k6177 
o|contracted procedure: k6187 
o|contracted procedure: k6191 
o|contracted procedure: k6204 
o|contracted procedure: k6226 
o|contracted procedure: k6075 
o|contracted procedure: k6222 
o|contracted procedure: k6207 
o|contracted procedure: k6210 
o|contracted procedure: k6218 
o|contracted procedure: k6093 
o|contracted procedure: k6235 
o|contracted procedure: k6238 
o|contracted procedure: k6241 
o|contracted procedure: k6281 
o|contracted procedure: k6271 
o|contracted procedure: k6291 
o|contracted procedure: k6300 
o|contracted procedure: k6311 
o|contracted procedure: k6321 
o|contracted procedure: k6328 
o|contracted procedure: k6332 
o|contracted procedure: k6367 
o|contracted procedure: k6345 
o|contracted procedure: k6360 
o|inlining procedure: k6342 
o|contracted procedure: k6373 
o|contracted procedure: k6380 
o|contracted procedure: k6391 
o|contracted procedure: k6394 
o|contracted procedure: k6411 
o|contracted procedure: k6407 
o|contracted procedure: k6420 
o|contracted procedure: k6423 
o|contracted procedure: k6426 
o|contracted procedure: k6434 
o|contracted procedure: k6442 
o|contracted procedure: k6451 
o|contracted procedure: k6457 
o|contracted procedure: k6761 
o|contracted procedure: k6460 
o|contracted procedure: k6547 
o|contracted procedure: k6556 
o|contracted procedure: k6579 
o|contracted procedure: k6585 
o|contracted procedure: k6591 
o|contracted procedure: k6617 
o|contracted procedure: k6621 
o|contracted procedure: k6667 
o|contracted procedure: k6675 
o|contracted procedure: k6687 
o|contracted procedure: k6696 
o|contracted procedure: k6714 
o|contracted procedure: k6705 
o|contracted procedure: k6720 
o|contracted procedure: k6732 
o|contracted procedure: k6735 
o|contracted procedure: k6738 
o|contracted procedure: k6746 
o|contracted procedure: k6754 
o|contracted procedure: k6767 
o|contracted procedure: k6782 
o|contracted procedure: k6774 
o|contracted procedure: k6788 
o|contracted procedure: k6803 
o|contracted procedure: k6799 
o|contracted procedure: k6795 
o|contracted procedure: k6809 
o|contracted procedure: k6819 
o|contracted procedure: k6825 
o|contracted procedure: k6828 
o|contracted procedure: k6851 
o|contracted procedure: k6854 
o|contracted procedure: k6864 
o|contracted procedure: k6870 
o|contracted procedure: k6877 
o|contracted procedure: k6883 
o|contracted procedure: k6886 
o|contracted procedure: k6895 
o|contracted procedure: k6907 
o|contracted procedure: k6910 
o|contracted procedure: k6925 
o|contracted procedure: k6917 
o|contracted procedure: k6931 
o|contracted procedure: k6943 
o|contracted procedure: k6950 
o|contracted procedure: k6960 
o|contracted procedure: k6967 
o|contracted procedure: k6973 
o|contracted procedure: k6976 
o|contracted procedure: k6988 
o|contracted procedure: k7002 
o|contracted procedure: k7005 
o|contracted procedure: k7037 
o|contracted procedure: k7043 
o|contracted procedure: k7049 
o|contracted procedure: k7055 
o|contracted procedure: k7061 
o|contracted procedure: k7067 
o|contracted procedure: k7073 
o|contracted procedure: k7079 
o|contracted procedure: k7085 
o|contracted procedure: k7192 
o|contracted procedure: k7221 
o|contracted procedure: k7227 
o|contracted procedure: k7239 
o|contracted procedure: k7338 
o|contracted procedure: k7344 
o|contracted procedure: k7353 
o|contracted procedure: k7362 
o|contracted procedure: k7379 
o|contracted procedure: k7401 
o|contracted procedure: k7407 
o|contracted procedure: k7437 
o|contracted procedure: k7443 
o|contracted procedure: k7481 
o|contracted procedure: k7487 
o|contracted procedure: k7533 
o|contracted procedure: k7536 
o|contracted procedure: k7544 
o|contracted procedure: k7563 
o|contracted procedure: k7571 
o|contracted procedure: k7583 
o|contracted procedure: k7586 
o|contracted procedure: k7589 
o|contracted procedure: k7597 
o|contracted procedure: k7605 
o|contracted procedure: k7617 
o|contracted procedure: k7620 
o|contracted procedure: k7623 
o|contracted procedure: k7631 
o|contracted procedure: k7639 
o|contracted procedure: k7315 
o|contracted procedure: k7321 
o|contracted procedure: k7328 
o|contracted procedure: k7332 
o|contracted procedure: k7657 
o|contracted procedure: k7666 
o|contracted procedure: k7673 
o|contracted procedure: k7881 
o|contracted procedure: k7894 
o|contracted procedure: k7911 
o|contracted procedure: k7917 
o|contracted procedure: k7927 
o|contracted procedure: k7939 
o|contracted procedure: k7931 
o|contracted procedure: k7935 
o|contracted procedure: k7945 
o|contracted procedure: k7968 
o|contracted procedure: k7980 
o|contracted procedure: k8000 
o|contracted procedure: k7989 
o|contracted procedure: k8008 
o|contracted procedure: k8011 
o|contracted procedure: k8014 
o|contracted procedure: k8017 
o|contracted procedure: k8020 
o|contracted procedure: k8032 
o|contracted procedure: k8038 
o|contracted procedure: k8057 
o|contracted procedure: k8063 
o|contracted procedure: k8488 
o|contracted procedure: k8503 
o|contracted procedure: k9379 
o|contracted procedure: k9414 
o|contracted procedure: k9389 
o|contracted procedure: k9403 
o|contracted procedure: k9410 
o|contracted procedure: k9426 
o|contracted procedure: k9436 
o|contracted procedure: k9443 
o|contracted procedure: k9449 
o|contracted procedure: k9538 
o|contracted procedure: k9467 
o|contracted procedure: k9482 
o|contracted procedure: k9485 
o|contracted procedure: k9495 
o|contracted procedure: k9498 
o|contracted procedure: k949512235 
o|contracted procedure: k9505 
o|contracted procedure: k949512239 
o|contracted procedure: k9512 
o|contracted procedure: k949512243 
o|contracted procedure: k9519 
o|contracted procedure: k949512247 
o|contracted procedure: k9527 
o|contracted procedure: k9550 
o|contracted procedure: k9561 
o|contracted procedure: k9974 
o|contracted procedure: k9581 
o|contracted procedure: k9968 
o|contracted procedure: k9584 
o|contracted procedure: k9962 
o|contracted procedure: k9587 
o|contracted procedure: k9956 
o|contracted procedure: k9590 
o|contracted procedure: k9950 
o|contracted procedure: k9593 
o|contracted procedure: k9944 
o|contracted procedure: k9596 
o|contracted procedure: k9938 
o|contracted procedure: k9599 
o|contracted procedure: k9932 
o|contracted procedure: k9602 
o|contracted procedure: k9606 
o|contracted procedure: k9636 
o|removed unused formal parameters: (abrt2266) 
o|contracted procedure: k9722 
o|contracted procedure: k9795 
o|contracted procedure: k9805 
o|contracted procedure: k9809 
o|contracted procedure: k9815 
o|removed unused parameter to known procedure: abrt2266 a9674 
o|contracted procedure: k9856 
o|contracted procedure: k9923 
o|contracted procedure: k9880 
o|contracted procedure: k9926 
o|contracted procedure: k9989 
o|contracted procedure: k9983 
o|contracted procedure: k10007 
o|contracted procedure: k9998 
o|contracted procedure: k10071 
o|contracted procedure: k10091 
o|contracted procedure: k10099 
o|contracted procedure: k10129 
o|contracted procedure: k10174 
o|contracted procedure: k10135 
o|contracted procedure: k10168 
o|contracted procedure: k10138 
o|contracted procedure: k10162 
o|contracted procedure: k10141 
o|contracted procedure: k10156 
o|contracted procedure: k10144 
o|contracted procedure: k10201 
o|contracted procedure: k10183 
o|contracted procedure: k10186 
o|contracted procedure: k10195 
o|contracted procedure: k10189 
o|contracted procedure: k10249 
o|contracted procedure: k10267 
o|contracted procedure: k10281 
o|contracted procedure: k10303 
o|contracted procedure: k10309 
o|contracted procedure: k10315 
o|contracted procedure: k10322 
o|contracted procedure: k10341 
o|contracted procedure: k10375 
o|contracted procedure: k10404 
o|contracted procedure: k10407 
o|contracted procedure: k10420 
o|contracted procedure: k10424 
o|contracted procedure: k10428 
o|contracted procedure: k10432 
o|contracted procedure: k10439 
o|contracted procedure: k10463 
o|contracted procedure: k10520 
o|contracted procedure: k10530 
o|contracted procedure: k10534 
o|contracted procedure: k10543 
o|contracted procedure: k10557 
o|contracted procedure: k10546 
o|contracted procedure: k10553 
o|contracted procedure: k10585 
o|contracted procedure: k10595 
o|contracted procedure: k10599 
o|contracted procedure: k10608 
o|contracted procedure: k10622 
o|contracted procedure: k10611 
o|contracted procedure: k10618 
o|contracted procedure: k8451 
o|contracted procedure: k8460 
o|contracted procedure: k8473 
o|contracted procedure: k10645 
o|contracted procedure: k10659 
o|contracted procedure: k10648 
o|contracted procedure: k10655 
o|contracted procedure: k10875 
o|contracted procedure: k10667 
o|contracted procedure: k10869 
o|contracted procedure: k10670 
o|contracted procedure: k10863 
o|contracted procedure: k10673 
o|contracted procedure: k10857 
o|contracted procedure: k10676 
o|contracted procedure: k10851 
o|contracted procedure: k10679 
o|contracted procedure: k10845 
o|contracted procedure: k10682 
o|contracted procedure: k10839 
o|contracted procedure: k10685 
o|contracted procedure: k10833 
o|contracted procedure: k10688 
o|contracted procedure: k10827 
o|contracted procedure: k10691 
o|contracted procedure: k10821 
o|contracted procedure: k10694 
o|contracted procedure: k10700 
o|contracted procedure: k10712 
o|contracted procedure: k10721 
o|contracted procedure: k10730 
o|contracted procedure: k10748 
o|contracted procedure: k10762 
o|contracted procedure: k10758 
o|contracted procedure: k10773 
o|contracted procedure: k10769 
o|contracted procedure: k10779 
o|contracted procedure: k10793 
o|contracted procedure: k10789 
o|contracted procedure: k10804 
o|contracted procedure: k10808 
o|contracted procedure: k10812 
o|contracted procedure: k10800 
o|contracted procedure: k10882 
o|contracted procedure: k10894 
o|contracted procedure: k10918 
o|contracted procedure: k10931 
o|contracted procedure: k10937 
o|inlining procedure: k10928 
o|contracted procedure: k10946 
o|inlining procedure: k10928 
o|contracted procedure: k10978 
o|contracted procedure: k10991 
o|contracted procedure: k10999 
o|contracted procedure: k11142 
o|contracted procedure: k11148 
o|contracted procedure: k11309 
o|contracted procedure: k11318 
o|contracted procedure: k11327 
o|contracted procedure: k11330 
o|contracted procedure: k11333 
o|contracted procedure: k11341 
o|contracted procedure: k11349 
o|contracted procedure: k11364 
o|contracted procedure: k11380 
o|contracted procedure: k11386 
o|contracted procedure: k11395 
o|contracted procedure: k11392 
o|contracted procedure: k11422 
o|contracted procedure: k11430 
o|contracted procedure: k11438 
o|contracted procedure: k11446 
o|contracted procedure: k11462 
o|contracted procedure: k11485 
o|contracted procedure: k11481 
o|contracted procedure: k11516 
o|contracted procedure: k11523 
o|contracted procedure: k11530 
o|contracted procedure: k11655 
o|contracted procedure: k11541 
o|contracted procedure: k11553 
o|contracted procedure: k11556 
o|contracted procedure: k11605 
o|contracted procedure: k11634 
o|contracted procedure: k11648 
o|contracted procedure: k11652 
o|contracted procedure: k11644 
o|simplifications: ((if . 1) (let . 234)) 
o|removed binding forms: 667 
(o x)|known list op on rest arg sublist: ##core#rest-length as869 0 
o|inlining procedure: "(eval.scm:193) emit-syntax-trace-info201" 
o|inlining procedure: "(eval.scm:649) emit-syntax-trace-info201" 
o|contracted procedure: "(eval.scm:1032) dload-path2227" 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest23442346 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest23442346 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest23562358 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest23562358 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest24392441 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest24392441 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest24392441 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest24392441 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest24572459 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest24572459 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest26982700 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest26982700 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest26982700 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest26982700 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest14131415 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest14131415 0 
o|simplifications: ((let . 1)) 
o|replaced variables: 16 
o|removed binding forms: 2 
o|removed side-effect free assignment to unused variable: emit-syntax-trace-info201 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10139 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r10139 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10139 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r10139 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10671 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r10671 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10671 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r10671 1 
o|replaced variables: 15 
o|removed binding forms: 2 
o|inlining procedure: k9651 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10677 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r10677 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10677 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r10677 2 
o|removed binding forms: 15 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10683 3 
(o x)|known list op on rest arg sublist: ##core#rest-car r10683 3 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10683 3 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r10683 3 
o|replaced variables: 1 
o|removed binding forms: 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10689 4 
(o x)|known list op on rest arg sublist: ##core#rest-car r10689 4 
(o x)|known list op on rest arg sublist: ##core#rest-null? r10689 4 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r10689 4 
o|removed binding forms: 2 
o|removed binding forms: 2 
o|direct leaf routine/allocation: loop244 0 
o|direct leaf routine/allocation: emit-trace-info200 5 
o|direct leaf routine/allocation: g947960 0 
o|direct leaf routine/allocation: loop1051 0 
o|direct leaf routine/allocation: loop1238 0 
o|direct leaf routine/allocation: loop2162 0 
o|direct leaf routine/allocation: for-each-loop25622590 0 
o|direct leaf routine/allocation: for-each-loop26152643 0 
o|direct leaf routine/allocation: for-each-loop26682686 0 
o|direct leaf routine/allocation: store-string 0 
o|contracted procedure: k3691 
o|converted assignments to bindings: (loop244) 
o|contracted procedure: "(eval.scm:481) k6180" 
o|converted assignments to bindings: (loop1051) 
o|contracted procedure: k7347 
o|contracted procedure: "(eval.scm:683) k7367" 
o|contracted procedure: "(eval.scm:687) k7387" 
o|contracted procedure: "(eval.scm:692) k7418" 
o|contracted procedure: "(eval.scm:698) k7457" 
o|contracted procedure: "(eval.scm:705) k7504" 
o|contracted procedure: "(eval.scm:709) k7552" 
o|converted assignments to bindings: (loop1238) 
o|converted assignments to bindings: (loop2162) 
o|contracted procedure: k10503 
o|converted assignments to bindings: (for-each-loop25622590) 
o|contracted procedure: k10568 
o|converted assignments to bindings: (for-each-loop26152643) 
o|contracted procedure: k10633 
o|converted assignments to bindings: (for-each-loop26682686) 
o|simplifications: ((let . 7)) 
o|removed binding forms: 12 
o|direct leaf routine/allocation: for-each-loop946963 0 
o|contracted procedure: k6120 
o|converted assignments to bindings: (for-each-loop946963) 
o|simplifications: ((let . 1)) 
o|removed binding forms: 1 
o|customizable procedures: (doloop14831484 k11474 strip1517 k9360 k9369 complete2377 map-loop23882405 store-result run-safe test2759 loop2773 test-extensions2758 loop1720 for-each-loop26252658 for-each-loop25722605 chicken.load#any g25492550 check2512 loop2521 chicken.load#file-exists? doloop24832484 chicken.load#load-library/internal k10053 loop2424 chicken.load#make-relative-pathname chicken.load#load/internal k9895 dload2228 a9674 for-each-loop23072319 doloop22972298 chicken.load#c-toplevel loop2181 chicken.load#path-separator-index/right loop1733 foldr15231526 loop1498 err1496 g13021311 map-loop12961314 g13301339 map-loop13241342 k7203 k4086 k6934 g12111212 compile-call206 g10341043 map-loop10281058 k6681 loop1111 loop21116 map-loop9911010 map-loop918936 map-loop883901 map-loop780797 doloop12291230 decorate202 map-loop612629 map-loop639656 map-loop669687 map-loop696717 map-loop729753 map-loop553571 map-loop580601 map-loop417435 map-loop445462 chicken.eval#compile-to-closure g518527 map-loop512530 doloop539540 compile203 k3851 k3824 lookup198 rename197 loop228 g237238 k3610 find-id196) 
o|calls to known targets: 320 
o|identified direct recursive calls: f_3727 1 
o|identified direct recursive calls: f_3680 1 
o|unused rest argument: v274 f_3795 
o|unused rest argument: v295 f_3844 
o|unused rest argument: v296 f_3849 
o|unused rest argument: v315 f_4011 
o|unused rest argument: v316 f_4019 
o|unused rest argument: v317 f_4027 
o|unused rest argument: v318 f_4035 
o|unused rest argument: v319 f_4037 
o|unused rest argument: v320 f_4056 
o|unused rest argument: v321 f_4058 
o|unused rest argument: v337 f_4069 
o|unused rest argument: v354 f_4122 
o|unused rest argument: v355 f_4130 
o|unused rest argument: v356 f_4138 
o|unused rest argument: v357 f_4146 
o|unused rest argument: v358 f_4154 
o|unused rest argument: v359 f_4162 
o|unused rest argument: v360 f_4170 
o|unused rest argument: v361 f_4172 
o|unused rest argument: v363 f_4201 
o|identified direct recursive calls: f_4985 1 
o|identified direct recursive calls: f_5096 1 
o|identified direct recursive calls: f_5130 1 
o|identified direct recursive calls: f_5272 1 
o|identified direct recursive calls: f_5320 1 
o|identified direct recursive calls: f_5368 1 
o|identified direct recursive calls: f_5436 1 
o|identified direct recursive calls: f_7264 1 
o|identified direct recursive calls: f_6172 1 
o|identified direct recursive calls: f_6694 1 
o|identified direct recursive calls: f_7310 1 
o|unused rest argument: _1253 f_7660 
o|identified direct recursive calls: f_7876 1 
o|identified direct recursive calls: f_7975 1 
o|identified direct recursive calls: f_9393 1 
o|identified direct recursive calls: f_9477 4 
o|unused rest argument: rest23442346 f_9981 
o|unused rest argument: rest23562358 f_9996 
o|unused rest argument: rest24392441 f_10133 
o|unused rest argument: rest24572459 f_10181 
o|identified direct recursive calls: f_10538 1 
o|identified direct recursive calls: f_10603 1 
o|identified direct recursive calls: f_10640 1 
o|unused rest argument: rest26982700 f_10665 
o|unused rest argument: rest14131415 f_11539 
o|fast box initializations: 48 
o|fast global references: 71 
o|fast global assignments: 31 
o|dropping unused closure argument: f_10051 
o|dropping unused closure argument: f_10289 
o|dropping unused closure argument: f_10538 
o|dropping unused closure argument: f_10603 
o|dropping unused closure argument: f_10640 
o|dropping unused closure argument: f_11018 
o|dropping unused closure argument: f_11077 
o|dropping unused closure argument: f_11140 
o|dropping unused closure argument: f_3572 
o|dropping unused closure argument: f_3757 
o|dropping unused closure argument: f_3781 
o|dropping unused closure argument: f_6694 
o|dropping unused closure argument: f_7310 
o|dropping unused closure argument: f_7966 
o|dropping unused closure argument: f_8477 
o|dropping unused closure argument: f_9383 
o|dropping unused closure argument: f_9418 
o|dropping unused closure argument: f_9565 
*/
/* end of file */
