/* Generated from srfi-4.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: srfi-4.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -explicit-use -no-trace -output-file srfi-4.c -emit-import-library srfi-4
   unit: srfi-4
   uses: expand extras library
*/
#include "chicken.h"

#define C_copy_subvector(to, from, start_to, start_from, bytes)   \
  (C_memcpy((C_char *)C_data_pointer(to) + C_unfix(start_to), (C_char *)C_data_pointer(from) + C_unfix(start_from), C_unfix(bytes)), \
    C_SCHEME_UNDEFINED)

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[277];
static double C_possibly_force_alignment;
static C_char C_TLS li0[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,117,56,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li1[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,115,56,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0,0};
static C_char C_TLS li2[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,117,49,54,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li3[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,115,49,54,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li4[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,117,51,50,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li5[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,115,51,50,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li6[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,117,54,52,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li7[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,115,54,52,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li8[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,102,51,50,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li9[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,102,54,52,118,101,99,116,111,114,45,108,101,110,103,116,104,32,120,41,0,0,0,0,0};
static C_char C_TLS li10[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,117,56,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0,0};
static C_char C_TLS li11[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,115,56,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0,0};
static C_char C_TLS li12[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,117,49,54,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li13[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,115,49,54,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li14[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,117,51,50,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li15[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,115,51,50,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li16[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,117,54,52,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li17[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,115,54,52,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li18[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,102,51,50,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li19[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,102,54,52,118,101,99,116,111,114,45,115,101,116,33,32,120,32,105,32,121,41,0,0,0};
static C_char C_TLS li20[] C_aligned={C_lihdr(0,0,16),40,101,120,116,45,102,114,101,101,32,98,118,50,50,49,41};
static C_char C_TLS li21[] C_aligned={C_lihdr(0,0,32),40,97,108,108,111,99,32,108,111,99,32,101,108,101,109,45,115,105,122,101,32,101,108,101,109,115,32,101,120,116,63,41};
static C_char C_TLS li22[] C_aligned={C_lihdr(0,0,32),40,115,114,102,105,45,52,35,114,101,108,101,97,115,101,45,110,117,109,98,101,114,45,118,101,99,116,111,114,32,118,41};
static C_char C_TLS li23[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,50,54,56,41,0,0,0,0,0};
static C_char C_TLS li24[] C_aligned={C_lihdr(0,0,33),40,115,114,102,105,45,52,35,109,97,107,101,45,117,56,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li25[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,51,48,50,41,0,0,0,0,0};
static C_char C_TLS li26[] C_aligned={C_lihdr(0,0,33),40,115,114,102,105,45,52,35,109,97,107,101,45,115,56,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0,0};
static C_char C_TLS li27[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,51,51,54,41,0,0,0,0,0};
static C_char C_TLS li28[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,117,49,54,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li29[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,51,55,48,41,0,0,0,0,0};
static C_char C_TLS li30[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,115,49,54,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li31[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,52,48,52,41,0,0,0,0,0};
static C_char C_TLS li32[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,117,51,50,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li33[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,52,51,56,41,0,0,0,0,0};
static C_char C_TLS li34[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,117,54,52,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li35[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,52,55,50,41,0,0,0,0,0};
static C_char C_TLS li36[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,115,51,50,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li37[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,53,48,54,41,0,0,0,0,0};
static C_char C_TLS li38[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,115,54,52,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li39[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,53,52,49,41,0,0,0,0,0};
static C_char C_TLS li40[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,102,51,50,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li41[] C_aligned={C_lihdr(0,0,11),40,100,111,108,111,111,112,53,55,55,41,0,0,0,0,0};
static C_char C_TLS li42[] C_aligned={C_lihdr(0,0,34),40,115,114,102,105,45,52,35,109,97,107,101,45,102,54,52,118,101,99,116,111,114,32,108,101,110,32,46,32,114,101,115,116,41,0,0,0,0,0,0};
static C_char C_TLS li43[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,49,50,32,112,32,105,41,0};
static C_char C_TLS li44[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,108,105,115,116,45,62,117,56,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li45[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,50,53,32,112,32,105,41,0};
static C_char C_TLS li46[] C_aligned={C_lihdr(0,0,27),40,115,114,102,105,45,52,35,108,105,115,116,45,62,115,56,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0,0};
static C_char C_TLS li47[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,51,56,32,112,32,105,41,0};
static C_char C_TLS li48[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,117,49,54,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li49[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,53,49,32,112,32,105,41,0};
static C_char C_TLS li50[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,115,49,54,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li51[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,54,52,32,112,32,105,41,0};
static C_char C_TLS li52[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,117,51,50,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li53[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,55,55,32,112,32,105,41,0};
static C_char C_TLS li54[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,115,51,50,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li55[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,54,57,48,32,112,32,105,41,0};
static C_char C_TLS li56[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,117,54,52,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li57[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,55,48,51,32,112,32,105,41,0};
static C_char C_TLS li58[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,115,54,52,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li59[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,55,49,54,32,112,32,105,41,0};
static C_char C_TLS li60[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,102,51,50,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li61[] C_aligned={C_lihdr(0,0,15),40,100,111,108,111,111,112,55,50,57,32,112,32,105,41,0};
static C_char C_TLS li62[] C_aligned={C_lihdr(0,0,28),40,115,114,102,105,45,52,35,108,105,115,116,45,62,102,54,52,118,101,99,116,111,114,32,108,115,116,41,0,0,0,0};
static C_char C_TLS li63[] C_aligned={C_lihdr(0,0,22),40,115,114,102,105,45,52,35,117,56,118,101,99,116,111,114,32,46,32,120,115,41,0,0};
static C_char C_TLS li64[] C_aligned={C_lihdr(0,0,22),40,115,114,102,105,45,52,35,115,56,118,101,99,116,111,114,32,46,32,120,115,41,0,0};
static C_char C_TLS li65[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,117,49,54,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li66[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,115,49,54,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li67[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,117,51,50,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li68[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,115,51,50,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li69[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,117,54,52,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li70[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,115,54,52,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li71[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,102,51,50,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li72[] C_aligned={C_lihdr(0,0,23),40,115,114,102,105,45,52,35,102,54,52,118,101,99,116,111,114,32,46,32,120,115,41,0};
static C_char C_TLS li73[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li74[] C_aligned={C_lihdr(0,0,25),40,115,114,102,105,45,52,35,117,56,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li75[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li76[] C_aligned={C_lihdr(0,0,25),40,115,114,102,105,45,52,35,115,56,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0,0};
static C_char C_TLS li77[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li78[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,117,49,54,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li79[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li80[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,115,49,54,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li81[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li82[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,117,51,50,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li83[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li84[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,115,51,50,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li85[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li86[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,117,54,52,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li87[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li88[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,115,54,52,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li89[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li90[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,102,51,50,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li91[] C_aligned={C_lihdr(0,0,8),40,108,111,111,112,32,105,41};
static C_char C_TLS li92[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,102,54,52,118,101,99,116,111,114,45,62,108,105,115,116,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li93[] C_aligned={C_lihdr(0,0,20),40,115,114,102,105,45,52,35,117,56,118,101,99,116,111,114,63,32,120,41,0,0,0,0};
static C_char C_TLS li94[] C_aligned={C_lihdr(0,0,20),40,115,114,102,105,45,52,35,115,56,118,101,99,116,111,114,63,32,120,41,0,0,0,0};
static C_char C_TLS li95[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,117,49,54,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li96[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,115,49,54,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li97[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,117,51,50,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li98[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,115,51,50,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li99[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,117,54,52,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li100[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,115,54,52,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li101[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,102,51,50,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li102[] C_aligned={C_lihdr(0,0,21),40,115,114,102,105,45,52,35,102,54,52,118,101,99,116,111,114,63,32,120,41,0,0,0};
static C_char C_TLS li103[] C_aligned={C_lihdr(0,0,10),40,102,95,51,54,49,52,32,118,41,0,0,0,0,0,0};
static C_char C_TLS li104[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,112,97,99,107,45,99,111,112,121,32,116,97,103,32,108,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li105[] C_aligned={C_lihdr(0,0,12),40,102,95,51,54,51,50,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li106[] C_aligned={C_lihdr(0,0,26),40,115,114,102,105,45,52,35,117,110,112,97,99,107,32,116,97,103,32,115,122,32,108,111,99,41,0,0,0,0,0,0};
static C_char C_TLS li107[] C_aligned={C_lihdr(0,0,12),40,102,95,51,54,54,50,32,115,116,114,41,0,0,0,0};
static C_char C_TLS li108[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,117,110,112,97,99,107,45,99,111,112,121,32,116,97,103,32,115,122,32,108,111,99,41,0};
static C_char C_TLS li109[] C_aligned={C_lihdr(0,0,16),40,102,53,50,56,49,32,118,56,54,56,53,50,56,48,41};
static C_char C_TLS li110[] C_aligned={C_lihdr(0,0,16),40,102,53,50,55,52,32,118,56,54,56,53,50,55,51,41};
static C_char C_TLS li111[] C_aligned={C_lihdr(0,0,16),40,102,53,50,54,55,32,118,56,54,56,53,50,54,54,41};
static C_char C_TLS li112[] C_aligned={C_lihdr(0,0,16),40,102,53,50,54,48,32,118,56,54,56,53,50,53,57,41};
static C_char C_TLS li113[] C_aligned={C_lihdr(0,0,16),40,102,53,50,53,51,32,118,56,54,56,53,50,53,50,41};
static C_char C_TLS li114[] C_aligned={C_lihdr(0,0,16),40,102,53,50,52,54,32,118,56,54,56,53,50,52,53,41};
static C_char C_TLS li115[] C_aligned={C_lihdr(0,0,16),40,102,53,50,51,57,32,118,56,54,56,53,50,51,56,41};
static C_char C_TLS li116[] C_aligned={C_lihdr(0,0,16),40,102,53,50,51,50,32,118,56,54,56,53,50,51,49,41};
static C_char C_TLS li117[] C_aligned={C_lihdr(0,0,16),40,102,53,50,50,53,32,118,56,54,56,53,50,50,52,41};
static C_char C_TLS li118[] C_aligned={C_lihdr(0,0,16),40,102,53,50,49,56,32,118,56,54,56,53,50,49,55,41};
static C_char C_TLS li119[] C_aligned={C_lihdr(0,0,8),40,103,57,53,53,32,99,41};
static C_char C_TLS li120[] C_aligned={C_lihdr(0,0,32),40,35,35,115,121,115,35,117,115,101,114,45,114,101,97,100,45,104,111,111,107,32,99,104,97,114,32,112,111,114,116,41};
static C_char C_TLS li121[] C_aligned={C_lihdr(0,0,39),40,35,35,115,121,115,35,117,115,101,114,45,112,114,105,110,116,45,104,111,111,107,32,120,32,114,101,97,100,97,98,108,101,32,112,111,114,116,41,0};
static C_char C_TLS li122[] C_aligned={C_lihdr(0,0,38),40,115,114,102,105,45,52,35,115,117,98,110,118,101,99,116,111,114,32,118,32,116,32,101,115,32,102,114,111,109,32,116,111,32,108,111,99,41,0,0};
static C_char C_TLS li123[] C_aligned={C_lihdr(0,0,30),40,115,114,102,105,45,52,35,115,117,98,117,56,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0,0};
static C_char C_TLS li124[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,117,49,54,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li125[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,117,51,50,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li126[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,117,54,52,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li127[] C_aligned={C_lihdr(0,0,30),40,115,114,102,105,45,52,35,115,117,98,115,56,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0,0};
static C_char C_TLS li128[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,115,49,54,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li129[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,115,51,50,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li130[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,115,54,52,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li131[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,102,51,50,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li132[] C_aligned={C_lihdr(0,0,31),40,115,114,102,105,45,52,35,115,117,98,102,54,52,118,101,99,116,111,114,32,118,32,102,114,111,109,32,116,111,41,0};
static C_char C_TLS li133[] C_aligned={C_lihdr(0,0,32),40,115,114,102,105,45,52,35,119,114,105,116,101,45,117,56,118,101,99,116,111,114,32,118,32,46,32,114,101,115,116,41};
static C_char C_TLS li134[] C_aligned={C_lihdr(0,0,37),40,115,114,102,105,45,52,35,114,101,97,100,45,117,56,118,101,99,116,111,114,33,32,110,32,100,101,115,116,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li135[] C_aligned={C_lihdr(0,0,29),40,115,114,102,105,45,52,35,114,101,97,100,45,117,56,118,101,99,116,111,114,32,46,32,114,101,115,116,41,0,0,0};
static C_char C_TLS li136[] C_aligned={C_lihdr(0,0,11),40,97,52,52,52,55,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li137[] C_aligned={C_lihdr(0,0,11),40,97,52,52,53,48,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li138[] C_aligned={C_lihdr(0,0,11),40,97,52,52,53,51,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li139[] C_aligned={C_lihdr(0,0,11),40,97,52,52,53,54,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li140[] C_aligned={C_lihdr(0,0,11),40,97,52,52,53,57,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li141[] C_aligned={C_lihdr(0,0,11),40,97,52,52,54,50,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li142[] C_aligned={C_lihdr(0,0,11),40,97,52,52,54,53,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li143[] C_aligned={C_lihdr(0,0,11),40,97,52,52,54,56,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li144[] C_aligned={C_lihdr(0,0,11),40,97,52,52,55,49,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li145[] C_aligned={C_lihdr(0,0,11),40,97,52,52,55,52,32,120,32,105,41,0,0,0,0,0};
static C_char C_TLS li146[] C_aligned={C_lihdr(0,0,10),40,116,111,112,108,101,118,101,108,41,0,0,0,0,0,0};


#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub222(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
C_word bv=(C_word )(C_a0);
C_free((void *)C_block_item(bv, 1));
C_ret:
#undef return

return C_r;}

#define return(x) C_cblock C_r = (((C_word)(x))); goto C_ret; C_cblockend
C_regparm static C_word C_fcall stub216(C_word C_buf,C_word C_a0){
C_word C_r=C_SCHEME_UNDEFINED,*C_a=(C_word*)C_buf;
size_t bytes=(size_t )(size_t)C_num_to_uint64(C_a0);
if (bytes > C_HEADER_SIZE_MASK) C_return(C_SCHEME_FALSE);
C_word *buf = (C_word *)C_malloc(bytes + sizeof(C_header));
if(buf == NULL) C_return(C_SCHEME_FALSE);
C_block_header_init(buf, C_make_header(C_BYTEVECTOR_TYPE, bytes));
C_return(buf);
C_ret:
#undef return

return C_r;}

C_noret_decl(f5218)
static void C_ccall f5218(C_word c,C_word *av) C_noret;
C_noret_decl(f5225)
static void C_ccall f5225(C_word c,C_word *av) C_noret;
C_noret_decl(f5232)
static void C_ccall f5232(C_word c,C_word *av) C_noret;
C_noret_decl(f5239)
static void C_ccall f5239(C_word c,C_word *av) C_noret;
C_noret_decl(f5246)
static void C_ccall f5246(C_word c,C_word *av) C_noret;
C_noret_decl(f5253)
static void C_ccall f5253(C_word c,C_word *av) C_noret;
C_noret_decl(f5260)
static void C_ccall f5260(C_word c,C_word *av) C_noret;
C_noret_decl(f5267)
static void C_ccall f5267(C_word c,C_word *av) C_noret;
C_noret_decl(f5274)
static void C_ccall f5274(C_word c,C_word *av) C_noret;
C_noret_decl(f5281)
static void C_ccall f5281(C_word c,C_word *av) C_noret;
C_noret_decl(f_1506)
static void C_ccall f_1506(C_word c,C_word *av) C_noret;
C_noret_decl(f_1509)
static void C_ccall f_1509(C_word c,C_word *av) C_noret;
C_noret_decl(f_1512)
static void C_ccall f_1512(C_word c,C_word *av) C_noret;
C_noret_decl(f_1514)
static void C_ccall f_1514(C_word c,C_word *av) C_noret;
C_noret_decl(f_1517)
static void C_ccall f_1517(C_word c,C_word *av) C_noret;
C_noret_decl(f_1520)
static void C_ccall f_1520(C_word c,C_word *av) C_noret;
C_noret_decl(f_1523)
static void C_ccall f_1523(C_word c,C_word *av) C_noret;
C_noret_decl(f_1526)
static void C_ccall f_1526(C_word c,C_word *av) C_noret;
C_noret_decl(f_1529)
static void C_ccall f_1529(C_word c,C_word *av) C_noret;
C_noret_decl(f_1532)
static void C_ccall f_1532(C_word c,C_word *av) C_noret;
C_noret_decl(f_1535)
static void C_ccall f_1535(C_word c,C_word *av) C_noret;
C_noret_decl(f_1538)
static void C_ccall f_1538(C_word c,C_word *av) C_noret;
C_noret_decl(f_1541)
static void C_ccall f_1541(C_word c,C_word *av) C_noret;
C_noret_decl(f_1544)
static void C_ccall f_1544(C_word c,C_word *av) C_noret;
C_noret_decl(f_1547)
static void C_ccall f_1547(C_word c,C_word *av) C_noret;
C_noret_decl(f_1550)
static void C_ccall f_1550(C_word c,C_word *av) C_noret;
C_noret_decl(f_1553)
static void C_ccall f_1553(C_word c,C_word *av) C_noret;
C_noret_decl(f_1556)
static void C_ccall f_1556(C_word c,C_word *av) C_noret;
C_noret_decl(f_1559)
static void C_ccall f_1559(C_word c,C_word *av) C_noret;
C_noret_decl(f_1562)
static void C_ccall f_1562(C_word c,C_word *av) C_noret;
C_noret_decl(f_1565)
static void C_ccall f_1565(C_word c,C_word *av) C_noret;
C_noret_decl(f_1568)
static void C_ccall f_1568(C_word c,C_word *av) C_noret;
C_noret_decl(f_1571)
static void C_ccall f_1571(C_word c,C_word *av) C_noret;
C_noret_decl(f_1576)
static void C_ccall f_1576(C_word c,C_word *av) C_noret;
C_noret_decl(f_1580)
static void C_ccall f_1580(C_word c,C_word *av) C_noret;
C_noret_decl(f_1584)
static void C_ccall f_1584(C_word c,C_word *av) C_noret;
C_noret_decl(f_1588)
static void C_ccall f_1588(C_word c,C_word *av) C_noret;
C_noret_decl(f_1592)
static void C_ccall f_1592(C_word c,C_word *av) C_noret;
C_noret_decl(f_1596)
static void C_ccall f_1596(C_word c,C_word *av) C_noret;
C_noret_decl(f_1600)
static void C_ccall f_1600(C_word c,C_word *av) C_noret;
C_noret_decl(f_1604)
static void C_ccall f_1604(C_word c,C_word *av) C_noret;
C_noret_decl(f_1608)
static void C_ccall f_1608(C_word c,C_word *av) C_noret;
C_noret_decl(f_1612)
static void C_ccall f_1612(C_word c,C_word *av) C_noret;
C_noret_decl(f_1620)
static void C_ccall f_1620(C_word c,C_word *av) C_noret;
C_noret_decl(f_1622)
static void C_fcall f_1622(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_1626)
static void C_ccall f_1626(C_word c,C_word *av) C_noret;
C_noret_decl(f_1629)
static void C_ccall f_1629(C_word c,C_word *av) C_noret;
C_noret_decl(f_1635)
static void C_ccall f_1635(C_word c,C_word *av) C_noret;
C_noret_decl(f_1650)
static void C_ccall f_1650(C_word c,C_word *av) C_noret;
C_noret_decl(f_1661)
static void C_ccall f_1661(C_word c,C_word *av) C_noret;
C_noret_decl(f_1668)
static void C_ccall f_1668(C_word c,C_word *av) C_noret;
C_noret_decl(f_1676)
static void C_ccall f_1676(C_word c,C_word *av) C_noret;
C_noret_decl(f_1701)
static void C_ccall f_1701(C_word c,C_word *av) C_noret;
C_noret_decl(f_1712)
static void C_ccall f_1712(C_word c,C_word *av) C_noret;
C_noret_decl(f_1731)
static void C_ccall f_1731(C_word c,C_word *av) C_noret;
C_noret_decl(f_1736)
static C_word C_fcall f_1736(C_word t0,C_word t1);
C_noret_decl(f_1754)
static void C_ccall f_1754(C_word c,C_word *av) C_noret;
C_noret_decl(f_1792)
static void C_ccall f_1792(C_word c,C_word *av) C_noret;
C_noret_decl(f_1817)
static void C_ccall f_1817(C_word c,C_word *av) C_noret;
C_noret_decl(f_1828)
static void C_ccall f_1828(C_word c,C_word *av) C_noret;
C_noret_decl(f_1847)
static void C_ccall f_1847(C_word c,C_word *av) C_noret;
C_noret_decl(f_1852)
static C_word C_fcall f_1852(C_word t0,C_word t1);
C_noret_decl(f_1870)
static void C_ccall f_1870(C_word c,C_word *av) C_noret;
C_noret_decl(f_1908)
static void C_ccall f_1908(C_word c,C_word *av) C_noret;
C_noret_decl(f_1933)
static void C_ccall f_1933(C_word c,C_word *av) C_noret;
C_noret_decl(f_1944)
static void C_ccall f_1944(C_word c,C_word *av) C_noret;
C_noret_decl(f_1963)
static void C_ccall f_1963(C_word c,C_word *av) C_noret;
C_noret_decl(f_1968)
static C_word C_fcall f_1968(C_word t0,C_word t1);
C_noret_decl(f_1986)
static void C_ccall f_1986(C_word c,C_word *av) C_noret;
C_noret_decl(f_2024)
static void C_ccall f_2024(C_word c,C_word *av) C_noret;
C_noret_decl(f_2049)
static void C_ccall f_2049(C_word c,C_word *av) C_noret;
C_noret_decl(f_2060)
static void C_ccall f_2060(C_word c,C_word *av) C_noret;
C_noret_decl(f_2089)
static void C_ccall f_2089(C_word c,C_word *av) C_noret;
C_noret_decl(f_2094)
static C_word C_fcall f_2094(C_word t0,C_word t1);
C_noret_decl(f_2112)
static void C_ccall f_2112(C_word c,C_word *av) C_noret;
C_noret_decl(f_2150)
static void C_ccall f_2150(C_word c,C_word *av) C_noret;
C_noret_decl(f_2175)
static void C_ccall f_2175(C_word c,C_word *av) C_noret;
C_noret_decl(f_2186)
static void C_ccall f_2186(C_word c,C_word *av) C_noret;
C_noret_decl(f_2205)
static void C_ccall f_2205(C_word c,C_word *av) C_noret;
C_noret_decl(f_2210)
static C_word C_fcall f_2210(C_word t0,C_word t1);
C_noret_decl(f_2228)
static void C_ccall f_2228(C_word c,C_word *av) C_noret;
C_noret_decl(f_2266)
static void C_ccall f_2266(C_word c,C_word *av) C_noret;
C_noret_decl(f_2291)
static void C_ccall f_2291(C_word c,C_word *av) C_noret;
C_noret_decl(f_2302)
static void C_ccall f_2302(C_word c,C_word *av) C_noret;
C_noret_decl(f_2321)
static void C_ccall f_2321(C_word c,C_word *av) C_noret;
C_noret_decl(f_2326)
static C_word C_fcall f_2326(C_word t0,C_word t1);
C_noret_decl(f_2344)
static void C_ccall f_2344(C_word c,C_word *av) C_noret;
C_noret_decl(f_2382)
static void C_ccall f_2382(C_word c,C_word *av) C_noret;
C_noret_decl(f_2407)
static void C_ccall f_2407(C_word c,C_word *av) C_noret;
C_noret_decl(f_2418)
static void C_ccall f_2418(C_word c,C_word *av) C_noret;
C_noret_decl(f_2447)
static void C_ccall f_2447(C_word c,C_word *av) C_noret;
C_noret_decl(f_2452)
static C_word C_fcall f_2452(C_word t0,C_word t1);
C_noret_decl(f_2470)
static void C_ccall f_2470(C_word c,C_word *av) C_noret;
C_noret_decl(f_2508)
static void C_ccall f_2508(C_word c,C_word *av) C_noret;
C_noret_decl(f_2533)
static void C_ccall f_2533(C_word c,C_word *av) C_noret;
C_noret_decl(f_2544)
static void C_ccall f_2544(C_word c,C_word *av) C_noret;
C_noret_decl(f_2573)
static void C_ccall f_2573(C_word c,C_word *av) C_noret;
C_noret_decl(f_2578)
static C_word C_fcall f_2578(C_word t0,C_word t1);
C_noret_decl(f_2596)
static void C_ccall f_2596(C_word c,C_word *av) C_noret;
C_noret_decl(f_2634)
static void C_ccall f_2634(C_word c,C_word *av) C_noret;
C_noret_decl(f_2659)
static void C_ccall f_2659(C_word c,C_word *av) C_noret;
C_noret_decl(f_2680)
static void C_ccall f_2680(C_word c,C_word *av) C_noret;
C_noret_decl(f_2683)
static void C_fcall f_2683(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2688)
static C_word C_fcall f_2688(C_word t0,C_word t1);
C_noret_decl(f_2707)
static void C_ccall f_2707(C_word c,C_word *av) C_noret;
C_noret_decl(f_2745)
static void C_ccall f_2745(C_word c,C_word *av) C_noret;
C_noret_decl(f_2770)
static void C_ccall f_2770(C_word c,C_word *av) C_noret;
C_noret_decl(f_2791)
static void C_ccall f_2791(C_word c,C_word *av) C_noret;
C_noret_decl(f_2794)
static void C_fcall f_2794(C_word t0,C_word t1) C_noret;
C_noret_decl(f_2799)
static C_word C_fcall f_2799(C_word t0,C_word t1);
C_noret_decl(f_2818)
static void C_ccall f_2818(C_word c,C_word *av) C_noret;
C_noret_decl(f_2856)
static void C_ccall f_2856(C_word c,C_word *av) C_noret;
C_noret_decl(f_2863)
static void C_ccall f_2863(C_word c,C_word *av) C_noret;
C_noret_decl(f_2868)
static void C_fcall f_2868(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2875)
static void C_ccall f_2875(C_word c,C_word *av) C_noret;
C_noret_decl(f_2892)
static void C_ccall f_2892(C_word c,C_word *av) C_noret;
C_noret_decl(f_2899)
static void C_ccall f_2899(C_word c,C_word *av) C_noret;
C_noret_decl(f_2904)
static void C_fcall f_2904(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2911)
static void C_ccall f_2911(C_word c,C_word *av) C_noret;
C_noret_decl(f_2928)
static void C_ccall f_2928(C_word c,C_word *av) C_noret;
C_noret_decl(f_2935)
static void C_ccall f_2935(C_word c,C_word *av) C_noret;
C_noret_decl(f_2940)
static void C_fcall f_2940(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2947)
static void C_ccall f_2947(C_word c,C_word *av) C_noret;
C_noret_decl(f_2964)
static void C_ccall f_2964(C_word c,C_word *av) C_noret;
C_noret_decl(f_2971)
static void C_ccall f_2971(C_word c,C_word *av) C_noret;
C_noret_decl(f_2976)
static void C_fcall f_2976(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2983)
static void C_ccall f_2983(C_word c,C_word *av) C_noret;
C_noret_decl(f_3000)
static void C_ccall f_3000(C_word c,C_word *av) C_noret;
C_noret_decl(f_3007)
static void C_ccall f_3007(C_word c,C_word *av) C_noret;
C_noret_decl(f_3012)
static void C_fcall f_3012(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3019)
static void C_ccall f_3019(C_word c,C_word *av) C_noret;
C_noret_decl(f_3036)
static void C_ccall f_3036(C_word c,C_word *av) C_noret;
C_noret_decl(f_3043)
static void C_ccall f_3043(C_word c,C_word *av) C_noret;
C_noret_decl(f_3048)
static void C_fcall f_3048(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3055)
static void C_ccall f_3055(C_word c,C_word *av) C_noret;
C_noret_decl(f_3072)
static void C_ccall f_3072(C_word c,C_word *av) C_noret;
C_noret_decl(f_3079)
static void C_ccall f_3079(C_word c,C_word *av) C_noret;
C_noret_decl(f_3084)
static void C_fcall f_3084(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3091)
static void C_ccall f_3091(C_word c,C_word *av) C_noret;
C_noret_decl(f_3108)
static void C_ccall f_3108(C_word c,C_word *av) C_noret;
C_noret_decl(f_3115)
static void C_ccall f_3115(C_word c,C_word *av) C_noret;
C_noret_decl(f_3120)
static void C_fcall f_3120(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3127)
static void C_ccall f_3127(C_word c,C_word *av) C_noret;
C_noret_decl(f_3144)
static void C_ccall f_3144(C_word c,C_word *av) C_noret;
C_noret_decl(f_3151)
static void C_ccall f_3151(C_word c,C_word *av) C_noret;
C_noret_decl(f_3156)
static void C_fcall f_3156(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3163)
static void C_ccall f_3163(C_word c,C_word *av) C_noret;
C_noret_decl(f_3180)
static void C_ccall f_3180(C_word c,C_word *av) C_noret;
C_noret_decl(f_3187)
static void C_ccall f_3187(C_word c,C_word *av) C_noret;
C_noret_decl(f_3192)
static void C_fcall f_3192(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3199)
static void C_ccall f_3199(C_word c,C_word *av) C_noret;
C_noret_decl(f_3216)
static void C_ccall f_3216(C_word c,C_word *av) C_noret;
C_noret_decl(f_3222)
static void C_ccall f_3222(C_word c,C_word *av) C_noret;
C_noret_decl(f_3228)
static void C_ccall f_3228(C_word c,C_word *av) C_noret;
C_noret_decl(f_3234)
static void C_ccall f_3234(C_word c,C_word *av) C_noret;
C_noret_decl(f_3240)
static void C_ccall f_3240(C_word c,C_word *av) C_noret;
C_noret_decl(f_3246)
static void C_ccall f_3246(C_word c,C_word *av) C_noret;
C_noret_decl(f_3252)
static void C_ccall f_3252(C_word c,C_word *av) C_noret;
C_noret_decl(f_3258)
static void C_ccall f_3258(C_word c,C_word *av) C_noret;
C_noret_decl(f_3264)
static void C_ccall f_3264(C_word c,C_word *av) C_noret;
C_noret_decl(f_3270)
static void C_ccall f_3270(C_word c,C_word *av) C_noret;
C_noret_decl(f_3276)
static void C_ccall f_3276(C_word c,C_word *av) C_noret;
C_noret_decl(f_3285)
static void C_fcall f_3285(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3300)
static void C_ccall f_3300(C_word c,C_word *av) C_noret;
C_noret_decl(f_3306)
static void C_ccall f_3306(C_word c,C_word *av) C_noret;
C_noret_decl(f_3315)
static void C_fcall f_3315(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3330)
static void C_ccall f_3330(C_word c,C_word *av) C_noret;
C_noret_decl(f_3336)
static void C_ccall f_3336(C_word c,C_word *av) C_noret;
C_noret_decl(f_3345)
static void C_fcall f_3345(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3360)
static void C_ccall f_3360(C_word c,C_word *av) C_noret;
C_noret_decl(f_3366)
static void C_ccall f_3366(C_word c,C_word *av) C_noret;
C_noret_decl(f_3375)
static void C_fcall f_3375(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3390)
static void C_ccall f_3390(C_word c,C_word *av) C_noret;
C_noret_decl(f_3396)
static void C_ccall f_3396(C_word c,C_word *av) C_noret;
C_noret_decl(f_3405)
static void C_fcall f_3405(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3419)
static void C_ccall f_3419(C_word c,C_word *av) C_noret;
C_noret_decl(f_3425)
static void C_ccall f_3425(C_word c,C_word *av) C_noret;
C_noret_decl(f_3434)
static void C_fcall f_3434(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3448)
static void C_ccall f_3448(C_word c,C_word *av) C_noret;
C_noret_decl(f_3454)
static void C_ccall f_3454(C_word c,C_word *av) C_noret;
C_noret_decl(f_3463)
static void C_fcall f_3463(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3477)
static void C_ccall f_3477(C_word c,C_word *av) C_noret;
C_noret_decl(f_3483)
static void C_ccall f_3483(C_word c,C_word *av) C_noret;
C_noret_decl(f_3492)
static void C_fcall f_3492(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3506)
static void C_ccall f_3506(C_word c,C_word *av) C_noret;
C_noret_decl(f_3512)
static void C_ccall f_3512(C_word c,C_word *av) C_noret;
C_noret_decl(f_3521)
static void C_fcall f_3521(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3535)
static void C_ccall f_3535(C_word c,C_word *av) C_noret;
C_noret_decl(f_3541)
static void C_ccall f_3541(C_word c,C_word *av) C_noret;
C_noret_decl(f_3550)
static void C_fcall f_3550(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3564)
static void C_ccall f_3564(C_word c,C_word *av) C_noret;
C_noret_decl(f_3570)
static void C_ccall f_3570(C_word c,C_word *av) C_noret;
C_noret_decl(f_3573)
static void C_ccall f_3573(C_word c,C_word *av) C_noret;
C_noret_decl(f_3576)
static void C_ccall f_3576(C_word c,C_word *av) C_noret;
C_noret_decl(f_3579)
static void C_ccall f_3579(C_word c,C_word *av) C_noret;
C_noret_decl(f_3582)
static void C_ccall f_3582(C_word c,C_word *av) C_noret;
C_noret_decl(f_3585)
static void C_ccall f_3585(C_word c,C_word *av) C_noret;
C_noret_decl(f_3588)
static void C_ccall f_3588(C_word c,C_word *av) C_noret;
C_noret_decl(f_3591)
static void C_ccall f_3591(C_word c,C_word *av) C_noret;
C_noret_decl(f_3594)
static void C_ccall f_3594(C_word c,C_word *av) C_noret;
C_noret_decl(f_3597)
static void C_ccall f_3597(C_word c,C_word *av) C_noret;
C_noret_decl(f_3612)
static void C_fcall f_3612(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3614)
static void C_ccall f_3614(C_word c,C_word *av) C_noret;
C_noret_decl(f_3624)
static void C_ccall f_3624(C_word c,C_word *av) C_noret;
C_noret_decl(f_3630)
static void C_fcall f_3630(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3632)
static void C_ccall f_3632(C_word c,C_word *av) C_noret;
C_noret_decl(f_3660)
static void C_fcall f_3660(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3662)
static void C_ccall f_3662(C_word c,C_word *av) C_noret;
C_noret_decl(f_3672)
static void C_ccall f_3672(C_word c,C_word *av) C_noret;
C_noret_decl(f_3736)
static void C_ccall f_3736(C_word c,C_word *av) C_noret;
C_noret_decl(f_3740)
static void C_ccall f_3740(C_word c,C_word *av) C_noret;
C_noret_decl(f_3744)
static void C_ccall f_3744(C_word c,C_word *av) C_noret;
C_noret_decl(f_3748)
static void C_ccall f_3748(C_word c,C_word *av) C_noret;
C_noret_decl(f_3752)
static void C_ccall f_3752(C_word c,C_word *av) C_noret;
C_noret_decl(f_3756)
static void C_ccall f_3756(C_word c,C_word *av) C_noret;
C_noret_decl(f_3760)
static void C_ccall f_3760(C_word c,C_word *av) C_noret;
C_noret_decl(f_3764)
static void C_ccall f_3764(C_word c,C_word *av) C_noret;
C_noret_decl(f_3768)
static void C_ccall f_3768(C_word c,C_word *av) C_noret;
C_noret_decl(f_3772)
static void C_ccall f_3772(C_word c,C_word *av) C_noret;
C_noret_decl(f_3776)
static void C_ccall f_3776(C_word c,C_word *av) C_noret;
C_noret_decl(f_3780)
static void C_ccall f_3780(C_word c,C_word *av) C_noret;
C_noret_decl(f_3784)
static void C_ccall f_3784(C_word c,C_word *av) C_noret;
C_noret_decl(f_3788)
static void C_ccall f_3788(C_word c,C_word *av) C_noret;
C_noret_decl(f_3792)
static void C_ccall f_3792(C_word c,C_word *av) C_noret;
C_noret_decl(f_3796)
static void C_ccall f_3796(C_word c,C_word *av) C_noret;
C_noret_decl(f_3800)
static void C_ccall f_3800(C_word c,C_word *av) C_noret;
C_noret_decl(f_3804)
static void C_ccall f_3804(C_word c,C_word *av) C_noret;
C_noret_decl(f_3808)
static void C_ccall f_3808(C_word c,C_word *av) C_noret;
C_noret_decl(f_3812)
static void C_ccall f_3812(C_word c,C_word *av) C_noret;
C_noret_decl(f_3816)
static void C_ccall f_3816(C_word c,C_word *av) C_noret;
C_noret_decl(f_3820)
static void C_ccall f_3820(C_word c,C_word *av) C_noret;
C_noret_decl(f_3824)
static void C_ccall f_3824(C_word c,C_word *av) C_noret;
C_noret_decl(f_3828)
static void C_ccall f_3828(C_word c,C_word *av) C_noret;
C_noret_decl(f_3832)
static void C_ccall f_3832(C_word c,C_word *av) C_noret;
C_noret_decl(f_3836)
static void C_ccall f_3836(C_word c,C_word *av) C_noret;
C_noret_decl(f_3840)
static void C_ccall f_3840(C_word c,C_word *av) C_noret;
C_noret_decl(f_3844)
static void C_ccall f_3844(C_word c,C_word *av) C_noret;
C_noret_decl(f_3848)
static void C_ccall f_3848(C_word c,C_word *av) C_noret;
C_noret_decl(f_3852)
static void C_ccall f_3852(C_word c,C_word *av) C_noret;
C_noret_decl(f_3857)
static void C_ccall f_3857(C_word c,C_word *av) C_noret;
C_noret_decl(f_3866)
static void C_ccall f_3866(C_word c,C_word *av) C_noret;
C_noret_decl(f_3885)
static void C_fcall f_3885(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3896)
static void C_ccall f_3896(C_word c,C_word *av) C_noret;
C_noret_decl(f_3917)
static void C_ccall f_3917(C_word c,C_word *av) C_noret;
C_noret_decl(f_3929)
static void C_ccall f_3929(C_word c,C_word *av) C_noret;
C_noret_decl(f_3932)
static void C_ccall f_3932(C_word c,C_word *av) C_noret;
C_noret_decl(f_3942)
static void C_ccall f_3942(C_word c,C_word *av) C_noret;
C_noret_decl(f_3991)
static void C_fcall f_3991(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_4006)
static void C_ccall f_4006(C_word c,C_word *av) C_noret;
C_noret_decl(f_4025)
static void C_ccall f_4025(C_word c,C_word *av) C_noret;
C_noret_decl(f_4030)
static void C_ccall f_4030(C_word c,C_word *av) C_noret;
C_noret_decl(f_4049)
static void C_ccall f_4049(C_word c,C_word *av) C_noret;
C_noret_decl(f_4055)
static void C_ccall f_4055(C_word c,C_word *av) C_noret;
C_noret_decl(f_4076)
static void C_ccall f_4076(C_word c,C_word *av) C_noret;
C_noret_decl(f_4082)
static void C_ccall f_4082(C_word c,C_word *av) C_noret;
C_noret_decl(f_4088)
static void C_ccall f_4088(C_word c,C_word *av) C_noret;
C_noret_decl(f_4094)
static void C_ccall f_4094(C_word c,C_word *av) C_noret;
C_noret_decl(f_4100)
static void C_ccall f_4100(C_word c,C_word *av) C_noret;
C_noret_decl(f_4106)
static void C_ccall f_4106(C_word c,C_word *av) C_noret;
C_noret_decl(f_4112)
static void C_ccall f_4112(C_word c,C_word *av) C_noret;
C_noret_decl(f_4118)
static void C_ccall f_4118(C_word c,C_word *av) C_noret;
C_noret_decl(f_4124)
static void C_ccall f_4124(C_word c,C_word *av) C_noret;
C_noret_decl(f_4130)
static void C_ccall f_4130(C_word c,C_word *av) C_noret;
C_noret_decl(f_4136)
static void C_ccall f_4136(C_word c,C_word *av) C_noret;
C_noret_decl(f_4166)
static void C_ccall f_4166(C_word c,C_word *av) C_noret;
C_noret_decl(f_4185)
static void C_ccall f_4185(C_word c,C_word *av) C_noret;
C_noret_decl(f_4188)
static void C_ccall f_4188(C_word c,C_word *av) C_noret;
C_noret_decl(f_4201)
static void C_fcall f_4201(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4211)
static void C_ccall f_4211(C_word c,C_word *av) C_noret;
C_noret_decl(f_4236)
static void C_ccall f_4236(C_word c,C_word *av) C_noret;
C_noret_decl(f_4305)
static void C_ccall f_4305(C_word c,C_word *av) C_noret;
C_noret_decl(f_4324)
static void C_ccall f_4324(C_word c,C_word *av) C_noret;
C_noret_decl(f_4330)
static void C_ccall f_4330(C_word c,C_word *av) C_noret;
C_noret_decl(f_4385)
static void C_ccall f_4385(C_word c,C_word *av) C_noret;
C_noret_decl(f_4404)
static void C_ccall f_4404(C_word c,C_word *av) C_noret;
C_noret_decl(f_4407)
static void C_ccall f_4407(C_word c,C_word *av) C_noret;
C_noret_decl(f_4446)
static void C_ccall f_4446(C_word c,C_word *av) C_noret;
C_noret_decl(f_4448)
static void C_ccall f_4448(C_word c,C_word *av) C_noret;
C_noret_decl(f_4451)
static void C_ccall f_4451(C_word c,C_word *av) C_noret;
C_noret_decl(f_4454)
static void C_ccall f_4454(C_word c,C_word *av) C_noret;
C_noret_decl(f_4457)
static void C_ccall f_4457(C_word c,C_word *av) C_noret;
C_noret_decl(f_4460)
static void C_ccall f_4460(C_word c,C_word *av) C_noret;
C_noret_decl(f_4463)
static void C_ccall f_4463(C_word c,C_word *av) C_noret;
C_noret_decl(f_4466)
static void C_ccall f_4466(C_word c,C_word *av) C_noret;
C_noret_decl(f_4469)
static void C_ccall f_4469(C_word c,C_word *av) C_noret;
C_noret_decl(f_4472)
static void C_ccall f_4472(C_word c,C_word *av) C_noret;
C_noret_decl(f_4475)
static void C_ccall f_4475(C_word c,C_word *av) C_noret;
C_noret_decl(C_srfi_2d4_toplevel)
C_externexport void C_ccall C_srfi_2d4_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_1622)
static void C_ccall trf_1622(C_word c,C_word *av) C_noret;
static void C_ccall trf_1622(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_1622(t0,t1,t2,t3,t4);}

C_noret_decl(trf_2683)
static void C_ccall trf_2683(C_word c,C_word *av) C_noret;
static void C_ccall trf_2683(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2683(t0,t1);}

C_noret_decl(trf_2794)
static void C_ccall trf_2794(C_word c,C_word *av) C_noret;
static void C_ccall trf_2794(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_2794(t0,t1);}

C_noret_decl(trf_2868)
static void C_ccall trf_2868(C_word c,C_word *av) C_noret;
static void C_ccall trf_2868(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2868(t0,t1,t2,t3);}

C_noret_decl(trf_2904)
static void C_ccall trf_2904(C_word c,C_word *av) C_noret;
static void C_ccall trf_2904(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2904(t0,t1,t2,t3);}

C_noret_decl(trf_2940)
static void C_ccall trf_2940(C_word c,C_word *av) C_noret;
static void C_ccall trf_2940(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2940(t0,t1,t2,t3);}

C_noret_decl(trf_2976)
static void C_ccall trf_2976(C_word c,C_word *av) C_noret;
static void C_ccall trf_2976(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2976(t0,t1,t2,t3);}

C_noret_decl(trf_3012)
static void C_ccall trf_3012(C_word c,C_word *av) C_noret;
static void C_ccall trf_3012(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3012(t0,t1,t2,t3);}

C_noret_decl(trf_3048)
static void C_ccall trf_3048(C_word c,C_word *av) C_noret;
static void C_ccall trf_3048(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3048(t0,t1,t2,t3);}

C_noret_decl(trf_3084)
static void C_ccall trf_3084(C_word c,C_word *av) C_noret;
static void C_ccall trf_3084(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3084(t0,t1,t2,t3);}

C_noret_decl(trf_3120)
static void C_ccall trf_3120(C_word c,C_word *av) C_noret;
static void C_ccall trf_3120(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3120(t0,t1,t2,t3);}

C_noret_decl(trf_3156)
static void C_ccall trf_3156(C_word c,C_word *av) C_noret;
static void C_ccall trf_3156(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3156(t0,t1,t2,t3);}

C_noret_decl(trf_3192)
static void C_ccall trf_3192(C_word c,C_word *av) C_noret;
static void C_ccall trf_3192(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3192(t0,t1,t2,t3);}

C_noret_decl(trf_3285)
static void C_ccall trf_3285(C_word c,C_word *av) C_noret;
static void C_ccall trf_3285(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3285(t0,t1,t2);}

C_noret_decl(trf_3315)
static void C_ccall trf_3315(C_word c,C_word *av) C_noret;
static void C_ccall trf_3315(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3315(t0,t1,t2);}

C_noret_decl(trf_3345)
static void C_ccall trf_3345(C_word c,C_word *av) C_noret;
static void C_ccall trf_3345(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3345(t0,t1,t2);}

C_noret_decl(trf_3375)
static void C_ccall trf_3375(C_word c,C_word *av) C_noret;
static void C_ccall trf_3375(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3375(t0,t1,t2);}

C_noret_decl(trf_3405)
static void C_ccall trf_3405(C_word c,C_word *av) C_noret;
static void C_ccall trf_3405(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3405(t0,t1,t2);}

C_noret_decl(trf_3434)
static void C_ccall trf_3434(C_word c,C_word *av) C_noret;
static void C_ccall trf_3434(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3434(t0,t1,t2);}

C_noret_decl(trf_3463)
static void C_ccall trf_3463(C_word c,C_word *av) C_noret;
static void C_ccall trf_3463(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3463(t0,t1,t2);}

C_noret_decl(trf_3492)
static void C_ccall trf_3492(C_word c,C_word *av) C_noret;
static void C_ccall trf_3492(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3492(t0,t1,t2);}

C_noret_decl(trf_3521)
static void C_ccall trf_3521(C_word c,C_word *av) C_noret;
static void C_ccall trf_3521(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3521(t0,t1,t2);}

C_noret_decl(trf_3550)
static void C_ccall trf_3550(C_word c,C_word *av) C_noret;
static void C_ccall trf_3550(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3550(t0,t1,t2);}

C_noret_decl(trf_3612)
static void C_ccall trf_3612(C_word c,C_word *av) C_noret;
static void C_ccall trf_3612(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3612(t0,t1,t2);}

C_noret_decl(trf_3630)
static void C_ccall trf_3630(C_word c,C_word *av) C_noret;
static void C_ccall trf_3630(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3630(t0,t1,t2,t3);}

C_noret_decl(trf_3660)
static void C_ccall trf_3660(C_word c,C_word *av) C_noret;
static void C_ccall trf_3660(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3660(t0,t1,t2,t3);}

C_noret_decl(trf_3885)
static void C_ccall trf_3885(C_word c,C_word *av) C_noret;
static void C_ccall trf_3885(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3885(t0,t1,t2);}

C_noret_decl(trf_3991)
static void C_ccall trf_3991(C_word c,C_word *av) C_noret;
static void C_ccall trf_3991(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_3991(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_4201)
static void C_ccall trf_4201(C_word c,C_word *av) C_noret;
static void C_ccall trf_4201(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4201(t0,t1);}

/* f5218 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5218(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5218,c,av);}
t3=C_i_check_structure_2(t2,lf[76],lf[154]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5225 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5225,c,av);}
t3=C_i_check_structure_2(t2,lf[73],lf[152]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5232 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5232(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5232,c,av);}
t3=C_i_check_structure_2(t2,lf[70],lf[150]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5239 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5239(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5239,c,av);}
t3=C_i_check_structure_2(t2,lf[63],lf[148]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5246 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5246,c,av);}
t3=C_i_check_structure_2(t2,lf[67],lf[146]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5253 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5253,c,av);}
t3=C_i_check_structure_2(t2,lf[59],lf[144]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5260 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5260(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5260,c,av);}
t3=C_i_check_structure_2(t2,lf[55],lf[142]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5267 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5267(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5267,c,av);}
t3=C_i_check_structure_2(t2,lf[52],lf[140]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5274 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5274(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5274,c,av);}
t3=C_i_check_structure_2(t2,lf[49],lf[138]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f5281 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f5281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f5281,c,av);}
t3=C_i_check_structure_2(t2,lf[43],lf[136]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_slot(t2,C_fix(1));
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k1504 */
static void C_ccall f_1506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1506,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1509,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k1507 in k1504 */
static void C_ccall f_1509(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_1509,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1512,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

/* k1510 in k1507 in k1504 */
static void C_ccall f_1512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(82,c,5)))){
C_save_and_reclaim((void *)f_1512,c,av);}
a=C_alloc(82);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate((C_word*)lf[2]+1 /* (set! srfi-4#u8vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1514,a[2]=((C_word)li0),tmp=(C_word)a,a+=3,tmp));
t5=C_mutate((C_word*)lf[3]+1 /* (set! srfi-4#s8vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1517,a[2]=((C_word)li1),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[4]+1 /* (set! srfi-4#u16vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1520,a[2]=((C_word)li2),tmp=(C_word)a,a+=3,tmp));
t7=C_mutate((C_word*)lf[5]+1 /* (set! srfi-4#s16vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1523,a[2]=((C_word)li3),tmp=(C_word)a,a+=3,tmp));
t8=C_mutate((C_word*)lf[6]+1 /* (set! srfi-4#u32vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1526,a[2]=((C_word)li4),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[7]+1 /* (set! srfi-4#s32vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1529,a[2]=((C_word)li5),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[8]+1 /* (set! srfi-4#u64vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1532,a[2]=((C_word)li6),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[9]+1 /* (set! srfi-4#s64vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1535,a[2]=((C_word)li7),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[10]+1 /* (set! srfi-4#f32vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1538,a[2]=((C_word)li8),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[11]+1 /* (set! srfi-4#f64vector-length ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1541,a[2]=((C_word)li9),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[12]+1 /* (set! srfi-4#u8vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1544,a[2]=((C_word)li10),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[13]+1 /* (set! srfi-4#s8vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1547,a[2]=((C_word)li11),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[14]+1 /* (set! srfi-4#u16vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1550,a[2]=((C_word)li12),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[15]+1 /* (set! srfi-4#s16vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1553,a[2]=((C_word)li13),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[16]+1 /* (set! srfi-4#u32vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1556,a[2]=((C_word)li14),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[17]+1 /* (set! srfi-4#s32vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1559,a[2]=((C_word)li15),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[18]+1 /* (set! srfi-4#u64vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1562,a[2]=((C_word)li16),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[19]+1 /* (set! srfi-4#s64vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1565,a[2]=((C_word)li17),tmp=(C_word)a,a+=3,tmp));
t22=C_mutate((C_word*)lf[20]+1 /* (set! srfi-4#f32vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1568,a[2]=((C_word)li18),tmp=(C_word)a,a+=3,tmp));
t23=C_mutate((C_word*)lf[21]+1 /* (set! srfi-4#f64vector-set! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1571,a[2]=((C_word)li19),tmp=(C_word)a,a+=3,tmp));
t24=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1576,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t25=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4475,a[2]=((C_word)li145),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:187: chicken.base#getter-with-setter */
t26=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t26;
av2[1]=t24;
av2[2]=t25;
av2[3]=*((C_word*)lf[12]+1);
av2[4]=lf[276];
((C_proc)(void*)(*((C_word*)t26+1)))(5,av2);}}

/* srfi-4#u8vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1514,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u8vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s8vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1517(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1517,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s8vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u16vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1520(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1520,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u16vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s16vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1523(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1523,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s16vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u32vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1526(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1526,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u32vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s32vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1529(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1529,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s32vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u64vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1532(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1532,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u64vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s64vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1535,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s64vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#f32vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1538(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1538,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_f32vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#f64vector-length in k1510 in k1507 in k1504 */
static void C_ccall f_1541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1541,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_f64vector_length(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u8vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1544,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_u8vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#s8vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1547(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1547,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_s8vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#u16vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1550(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1550,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_u16vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#s16vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1553(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1553,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_s16vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#u32vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1556(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1556,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_u32vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#s32vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1559,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_s32vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#u64vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1562,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_u64vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#s64vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1565(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1565,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_s64vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#f32vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1568(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1568,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_f32vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* srfi-4#f64vector-set! in k1510 in k1507 in k1504 */
static void C_ccall f_1571(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1571,c,av);}
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_i_f64vector_set(t2,t3,t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1576,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[22]+1 /* (set! srfi-4#u8vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1580,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4472,a[2]=((C_word)li144),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:193: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[13]+1);
av2[4]=lf[275];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1580,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[23]+1 /* (set! srfi-4#s8vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1584,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4469,a[2]=((C_word)li143),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:199: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[14]+1);
av2[4]=lf[274];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1584(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1584,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[24]+1 /* (set! srfi-4#u16vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1588,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4466,a[2]=((C_word)li142),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:205: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[15]+1);
av2[4]=lf[273];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1588,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[25]+1 /* (set! srfi-4#s16vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1592,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4463,a[2]=((C_word)li141),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:211: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[16]+1);
av2[4]=lf[272];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1592(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1592,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[26]+1 /* (set! srfi-4#u32vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1596,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4460,a[2]=((C_word)li140),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:217: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[17]+1);
av2[4]=lf[271];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1596,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[27]+1 /* (set! srfi-4#s32vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1600,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4457,a[2]=((C_word)li139),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:223: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[18]+1);
av2[4]=lf[270];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1600(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1600,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[28]+1 /* (set! srfi-4#u64vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1604,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4454,a[2]=((C_word)li138),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:229: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[19]+1);
av2[4]=lf[269];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1604(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1604,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[29]+1 /* (set! srfi-4#s64vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1608,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4451,a[2]=((C_word)li137),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:235: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[20]+1);
av2[4]=lf[268];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1608(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_1608,c,av);}
a=C_alloc(6);
t2=C_mutate((C_word*)lf[30]+1 /* (set! srfi-4#f32vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1612,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4448,a[2]=((C_word)li136),tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:241: chicken.base#getter-with-setter */
t5=*((C_word*)lf[266]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t4;
av2[3]=*((C_word*)lf[21]+1);
av2[4]=lf[267];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1612(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word t33;
C_word t34;
C_word t35;
C_word t36;
C_word t37;
C_word t38;
C_word t39;
C_word t40;
C_word t41;
C_word t42;
C_word t43;
C_word t44;
C_word t45;
C_word t46;
C_word t47;
C_word t48;
C_word t49;
C_word t50;
C_word t51;
C_word t52;
C_word t53;
C_word t54;
C_word t55;
C_word t56;
C_word t57;
C_word t58;
C_word t59;
C_word t60;
C_word t61;
C_word t62;
C_word t63;
C_word t64;
C_word t65;
C_word t66;
C_word t67;
C_word t68;
C_word t69;
C_word t70;
C_word t71;
C_word t72;
C_word t73;
C_word t74;
C_word t75;
C_word t76;
C_word t77;
C_word t78;
C_word t79;
C_word t80;
C_word t81;
C_word t82;
C_word t83;
C_word t84;
C_word t85;
C_word t86;
C_word t87;
C_word t88;
C_word t89;
C_word t90;
C_word t91;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(231,c,6)))){
C_save_and_reclaim((void *)f_1612,c,av);}
a=C_alloc(231);
t2=C_mutate((C_word*)lf[31]+1 /* (set! srfi-4#f64vector-ref ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1620,a[2]=((C_word)li20),tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1622,a[2]=((C_word)li21),tmp=(C_word)a,a+=3,tmp);
t5=C_mutate((C_word*)lf[38]+1 /* (set! srfi-4#release-number-vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1661,a[2]=((C_word)li22),tmp=(C_word)a,a+=3,tmp));
t6=C_mutate((C_word*)lf[42]+1 /* (set! srfi-4#make-u8vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1676,a[2]=t3,a[3]=t4,a[4]=((C_word)li24),tmp=(C_word)a,a+=5,tmp));
t7=C_mutate((C_word*)lf[48]+1 /* (set! srfi-4#make-s8vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1792,a[2]=t3,a[3]=t4,a[4]=((C_word)li26),tmp=(C_word)a,a+=5,tmp));
t8=C_mutate((C_word*)lf[51]+1 /* (set! srfi-4#make-u16vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_1908,a[2]=t3,a[3]=t4,a[4]=((C_word)li28),tmp=(C_word)a,a+=5,tmp));
t9=C_mutate((C_word*)lf[54]+1 /* (set! srfi-4#make-s16vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2024,a[2]=t3,a[3]=t4,a[4]=((C_word)li30),tmp=(C_word)a,a+=5,tmp));
t10=C_mutate((C_word*)lf[58]+1 /* (set! srfi-4#make-u32vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2150,a[2]=t3,a[3]=t4,a[4]=((C_word)li32),tmp=(C_word)a,a+=5,tmp));
t11=C_mutate((C_word*)lf[62]+1 /* (set! srfi-4#make-u64vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2266,a[2]=t3,a[3]=t4,a[4]=((C_word)li34),tmp=(C_word)a,a+=5,tmp));
t12=C_mutate((C_word*)lf[66]+1 /* (set! srfi-4#make-s32vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2382,a[2]=t3,a[3]=t4,a[4]=((C_word)li36),tmp=(C_word)a,a+=5,tmp));
t13=C_mutate((C_word*)lf[69]+1 /* (set! srfi-4#make-s64vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2508,a[2]=t3,a[3]=t4,a[4]=((C_word)li38),tmp=(C_word)a,a+=5,tmp));
t14=C_mutate((C_word*)lf[72]+1 /* (set! srfi-4#make-f32vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2634,a[2]=t3,a[3]=t4,a[4]=((C_word)li40),tmp=(C_word)a,a+=5,tmp));
t15=C_mutate((C_word*)lf[75]+1 /* (set! srfi-4#make-f64vector ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_2745,a[2]=t3,a[3]=t4,a[4]=((C_word)li42),tmp=(C_word)a,a+=5,tmp));
t16=*((C_word*)lf[42]+1);
t17=C_mutate((C_word*)lf[78]+1 /* (set! srfi-4#list->u8vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2856,a[2]=t16,a[3]=((C_word)li44),tmp=(C_word)a,a+=4,tmp));
t18=*((C_word*)lf[48]+1);
t19=C_mutate((C_word*)lf[80]+1 /* (set! srfi-4#list->s8vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2892,a[2]=t18,a[3]=((C_word)li46),tmp=(C_word)a,a+=4,tmp));
t20=*((C_word*)lf[51]+1);
t21=C_mutate((C_word*)lf[81]+1 /* (set! srfi-4#list->u16vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2928,a[2]=t20,a[3]=((C_word)li48),tmp=(C_word)a,a+=4,tmp));
t22=*((C_word*)lf[54]+1);
t23=C_mutate((C_word*)lf[82]+1 /* (set! srfi-4#list->s16vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2964,a[2]=t22,a[3]=((C_word)li50),tmp=(C_word)a,a+=4,tmp));
t24=*((C_word*)lf[58]+1);
t25=C_mutate((C_word*)lf[83]+1 /* (set! srfi-4#list->u32vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3000,a[2]=t24,a[3]=((C_word)li52),tmp=(C_word)a,a+=4,tmp));
t26=*((C_word*)lf[66]+1);
t27=C_mutate((C_word*)lf[84]+1 /* (set! srfi-4#list->s32vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3036,a[2]=t26,a[3]=((C_word)li54),tmp=(C_word)a,a+=4,tmp));
t28=*((C_word*)lf[62]+1);
t29=C_mutate((C_word*)lf[85]+1 /* (set! srfi-4#list->u64vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3072,a[2]=t28,a[3]=((C_word)li56),tmp=(C_word)a,a+=4,tmp));
t30=*((C_word*)lf[69]+1);
t31=C_mutate((C_word*)lf[86]+1 /* (set! srfi-4#list->s64vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3108,a[2]=t30,a[3]=((C_word)li58),tmp=(C_word)a,a+=4,tmp));
t32=*((C_word*)lf[72]+1);
t33=C_mutate((C_word*)lf[87]+1 /* (set! srfi-4#list->f32vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3144,a[2]=t32,a[3]=((C_word)li60),tmp=(C_word)a,a+=4,tmp));
t34=*((C_word*)lf[75]+1);
t35=C_mutate((C_word*)lf[88]+1 /* (set! srfi-4#list->f64vector ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3180,a[2]=t34,a[3]=((C_word)li62),tmp=(C_word)a,a+=4,tmp));
t36=C_mutate((C_word*)lf[89]+1 /* (set! srfi-4#u8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3216,a[2]=((C_word)li63),tmp=(C_word)a,a+=3,tmp));
t37=C_mutate((C_word*)lf[90]+1 /* (set! srfi-4#s8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3222,a[2]=((C_word)li64),tmp=(C_word)a,a+=3,tmp));
t38=C_mutate((C_word*)lf[91]+1 /* (set! srfi-4#u16vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3228,a[2]=((C_word)li65),tmp=(C_word)a,a+=3,tmp));
t39=C_mutate((C_word*)lf[92]+1 /* (set! srfi-4#s16vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3234,a[2]=((C_word)li66),tmp=(C_word)a,a+=3,tmp));
t40=C_mutate((C_word*)lf[93]+1 /* (set! srfi-4#u32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3240,a[2]=((C_word)li67),tmp=(C_word)a,a+=3,tmp));
t41=C_mutate((C_word*)lf[94]+1 /* (set! srfi-4#s32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3246,a[2]=((C_word)li68),tmp=(C_word)a,a+=3,tmp));
t42=C_mutate((C_word*)lf[95]+1 /* (set! srfi-4#u64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3252,a[2]=((C_word)li69),tmp=(C_word)a,a+=3,tmp));
t43=C_mutate((C_word*)lf[96]+1 /* (set! srfi-4#s64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3258,a[2]=((C_word)li70),tmp=(C_word)a,a+=3,tmp));
t44=C_mutate((C_word*)lf[97]+1 /* (set! srfi-4#f32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3264,a[2]=((C_word)li71),tmp=(C_word)a,a+=3,tmp));
t45=C_mutate((C_word*)lf[98]+1 /* (set! srfi-4#f64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3270,a[2]=((C_word)li72),tmp=(C_word)a,a+=3,tmp));
t46=C_mutate((C_word*)lf[99]+1 /* (set! srfi-4#u8vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3276,a[2]=((C_word)li74),tmp=(C_word)a,a+=3,tmp));
t47=C_mutate((C_word*)lf[101]+1 /* (set! srfi-4#s8vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3306,a[2]=((C_word)li76),tmp=(C_word)a,a+=3,tmp));
t48=C_mutate((C_word*)lf[103]+1 /* (set! srfi-4#u16vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3336,a[2]=((C_word)li78),tmp=(C_word)a,a+=3,tmp));
t49=C_mutate((C_word*)lf[105]+1 /* (set! srfi-4#s16vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3366,a[2]=((C_word)li80),tmp=(C_word)a,a+=3,tmp));
t50=C_mutate((C_word*)lf[107]+1 /* (set! srfi-4#u32vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3396,a[2]=((C_word)li82),tmp=(C_word)a,a+=3,tmp));
t51=C_mutate((C_word*)lf[109]+1 /* (set! srfi-4#s32vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3425,a[2]=((C_word)li84),tmp=(C_word)a,a+=3,tmp));
t52=C_mutate((C_word*)lf[111]+1 /* (set! srfi-4#u64vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3454,a[2]=((C_word)li86),tmp=(C_word)a,a+=3,tmp));
t53=C_mutate((C_word*)lf[113]+1 /* (set! srfi-4#s64vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3483,a[2]=((C_word)li88),tmp=(C_word)a,a+=3,tmp));
t54=C_mutate((C_word*)lf[115]+1 /* (set! srfi-4#f32vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3512,a[2]=((C_word)li90),tmp=(C_word)a,a+=3,tmp));
t55=C_mutate((C_word*)lf[117]+1 /* (set! srfi-4#f64vector->list ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3541,a[2]=((C_word)li92),tmp=(C_word)a,a+=3,tmp));
t56=C_mutate((C_word*)lf[119]+1 /* (set! srfi-4#u8vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3570,a[2]=((C_word)li93),tmp=(C_word)a,a+=3,tmp));
t57=C_mutate((C_word*)lf[120]+1 /* (set! srfi-4#s8vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3573,a[2]=((C_word)li94),tmp=(C_word)a,a+=3,tmp));
t58=C_mutate((C_word*)lf[121]+1 /* (set! srfi-4#u16vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3576,a[2]=((C_word)li95),tmp=(C_word)a,a+=3,tmp));
t59=C_mutate((C_word*)lf[122]+1 /* (set! srfi-4#s16vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3579,a[2]=((C_word)li96),tmp=(C_word)a,a+=3,tmp));
t60=C_mutate((C_word*)lf[123]+1 /* (set! srfi-4#u32vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3582,a[2]=((C_word)li97),tmp=(C_word)a,a+=3,tmp));
t61=C_mutate((C_word*)lf[124]+1 /* (set! srfi-4#s32vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3585,a[2]=((C_word)li98),tmp=(C_word)a,a+=3,tmp));
t62=C_mutate((C_word*)lf[125]+1 /* (set! srfi-4#u64vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3588,a[2]=((C_word)li99),tmp=(C_word)a,a+=3,tmp));
t63=C_mutate((C_word*)lf[126]+1 /* (set! srfi-4#s64vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3591,a[2]=((C_word)li100),tmp=(C_word)a,a+=3,tmp));
t64=C_mutate((C_word*)lf[127]+1 /* (set! srfi-4#f32vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3594,a[2]=((C_word)li101),tmp=(C_word)a,a+=3,tmp));
t65=C_mutate((C_word*)lf[128]+1 /* (set! srfi-4#f64vector? ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3597,a[2]=((C_word)li102),tmp=(C_word)a,a+=3,tmp));
t66=C_mutate((C_word*)lf[41]+1 /* (set! srfi-4#number-vector? ...) */,*((C_word*)lf[129]+1));
t67=C_mutate(&lf[130] /* (set! srfi-4#pack-copy ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3612,a[2]=((C_word)li104),tmp=(C_word)a,a+=3,tmp));
t68=C_mutate(&lf[132] /* (set! srfi-4#unpack ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3630,a[2]=((C_word)li106),tmp=(C_word)a,a+=3,tmp));
t69=C_mutate(&lf[134] /* (set! srfi-4#unpack-copy ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3660,a[2]=((C_word)li108),tmp=(C_word)a,a+=3,tmp));
t70=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5281,a[2]=((C_word)li109),tmp=(C_word)a,a+=3,tmp);
t71=C_mutate((C_word*)lf[137]+1 /* (set! srfi-4#u8vector->blob/shared ...) */,t70);
t72=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5274,a[2]=((C_word)li110),tmp=(C_word)a,a+=3,tmp);
t73=C_mutate((C_word*)lf[139]+1 /* (set! srfi-4#s8vector->blob/shared ...) */,t72);
t74=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5267,a[2]=((C_word)li111),tmp=(C_word)a,a+=3,tmp);
t75=C_mutate((C_word*)lf[141]+1 /* (set! srfi-4#u16vector->blob/shared ...) */,t74);
t76=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5260,a[2]=((C_word)li112),tmp=(C_word)a,a+=3,tmp);
t77=C_mutate((C_word*)lf[143]+1 /* (set! srfi-4#s16vector->blob/shared ...) */,t76);
t78=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5253,a[2]=((C_word)li113),tmp=(C_word)a,a+=3,tmp);
t79=C_mutate((C_word*)lf[145]+1 /* (set! srfi-4#u32vector->blob/shared ...) */,t78);
t80=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5246,a[2]=((C_word)li114),tmp=(C_word)a,a+=3,tmp);
t81=C_mutate((C_word*)lf[147]+1 /* (set! srfi-4#s32vector->blob/shared ...) */,t80);
t82=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5239,a[2]=((C_word)li115),tmp=(C_word)a,a+=3,tmp);
t83=C_mutate((C_word*)lf[149]+1 /* (set! srfi-4#u64vector->blob/shared ...) */,t82);
t84=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5232,a[2]=((C_word)li116),tmp=(C_word)a,a+=3,tmp);
t85=C_mutate((C_word*)lf[151]+1 /* (set! srfi-4#s64vector->blob/shared ...) */,t84);
t86=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5225,a[2]=((C_word)li117),tmp=(C_word)a,a+=3,tmp);
t87=C_mutate((C_word*)lf[153]+1 /* (set! srfi-4#f32vector->blob/shared ...) */,t86);
t88=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f5218,a[2]=((C_word)li118),tmp=(C_word)a,a+=3,tmp);
t89=C_mutate((C_word*)lf[155]+1 /* (set! srfi-4#f64vector->blob/shared ...) */,t88);
t90=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3736,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:579: pack-copy */
f_3612(t90,lf[43],lf[265]);}

/* ext-free in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1620(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1620,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=stub222(C_SCHEME_UNDEFINED,t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* alloc in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_1622(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_1622,5,t1,t2,t3,t4,t5);}
a=C_alloc(7);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1626,a[2]=t4,a[3]=t3,a[4]=t5,a[5]=t1,a[6]=t2,tmp=(C_word)a,a+=7,tmp);
/* srfi-4.scm:273: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t6;
av2[2]=t4;
av2[3]=t2;
tp(4,av2);}}

/* k1624 in alloc in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1626(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_1626,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_1629,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_fixnum_lessp(((C_word*)t0)[2],C_fix(0)))){
/* srfi-4.scm:274: ##sys#error */
t3=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[36];
av2[4]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_1629(2,av2);}}}

/* k1627 in k1624 in alloc in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1629(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_1629,c,av);}
a=C_alloc(6);
t2=C_i_o_fixnum_times(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1635,a[2]=((C_word*)t0)[4],a[3]=t2,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
if(C_truep(t2)){
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1635(2,av2);}}
else{
/* srfi-4.scm:276: ##sys#error */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[35];
av2[3]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}}

/* k1633 in k1627 in k1624 in alloc in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1635(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,5)))){
C_save_and_reclaim((void *)f_1635,c,av);}
a=C_alloc(3);
if(C_truep(((C_word*)t0)[2])){
t2=C_fix((C_word)sizeof(size_t) * CHAR_BIT);
t3=C_i_foreign_unsigned_ranged_integer_argumentp(((C_word*)t0)[3],t2);
t4=stub216(C_SCHEME_UNDEFINED,t3);
if(C_truep(t4)){
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* srfi-4.scm:280: ##sys#error */
t5=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=((C_word*)t0)[4];
av2[2]=((C_word*)t0)[5];
av2[3]=lf[33];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1650,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:281: ##sys#allocate-vector */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(6,av2);}}}

/* k1648 in k1633 in k1627 in k1624 in alloc in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_1650,c,av);}
t2=C_string_to_bytevector(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#release-number-vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1661(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_1661,c,av);}
a=C_alloc(4);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1668,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:287: number-vector? */
t4=*((C_word*)lf[41]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k1666 in srfi-4#release-number-vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1668(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_1668,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=stub222(C_SCHEME_UNDEFINED,((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* srfi-4.scm:289: ##sys#error */
t2=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[39];
av2[3]=lf[40];
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}}

/* srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1676(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_1676,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1754,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:293: alloc */
f_1622(t12,lf[45],C_fix(1),t2,t7);}

/* k1699 in k1752 in srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1701(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_1701,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1731,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1712,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:109: ##sys#check-exact-uinteger */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[45];
tp(4,av2);}}}

/* k1710 in k1699 in k1752 in srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1712(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_1712,c,av);}
if(C_truep(C_fixnum_greaterp(C_i_integer_length(((C_word*)t0)[2]),C_fix(8)))){
t2=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:111: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=lf[45];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=C_fix(256);
tp(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_1731(2,av2);}}}

/* k1729 in k1699 in k1752 in srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1731,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1736,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li23),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_1736(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop268 in k1729 in k1699 in k1752 in srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_1736(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_u8vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k1752 in srfi-4#make-u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1754(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1754,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[43],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1701,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:294: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1701(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1701(2,av2);}}}

/* srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_1792,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1870,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:305: alloc */
f_1622(t12,lf[50],C_fix(1),t2,t7);}

/* k1815 in k1868 in srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1817(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_1817,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1847,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1828,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:109: ##sys#check-exact-uinteger */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[50];
tp(4,av2);}}}

/* k1826 in k1815 in k1868 in srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_1828,c,av);}
if(C_truep(C_fixnum_greaterp(C_i_integer_length(((C_word*)t0)[2]),C_fix(8)))){
t2=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:111: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=lf[50];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=C_fix(256);
tp(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_1847(2,av2);}}}

/* k1845 in k1815 in k1868 in srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1847,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1852,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li25),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_1852(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop302 in k1845 in k1815 in k1868 in srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_1852(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_s8vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k1868 in srfi-4#make-s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1870(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1870,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[49],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1817,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:306: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1817(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1817(2,av2);}}}

/* srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_1908,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_1986,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:317: alloc */
f_1622(t12,lf[53],C_fix(2),t2,t7);}

/* k1931 in k1984 in srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1933(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_1933,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1963,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_1944,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:109: ##sys#check-exact-uinteger */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[53];
tp(4,av2);}}}

/* k1942 in k1931 in k1984 in srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_1944,c,av);}
if(C_truep(C_fixnum_greaterp(C_i_integer_length(((C_word*)t0)[2]),C_fix(16)))){
t2=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:111: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=lf[53];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=C_fix(65536);
tp(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_1963(2,av2);}}}

/* k1961 in k1931 in k1984 in srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1963(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_1963,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1968,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li27),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_1968(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop336 in k1961 in k1931 in k1984 in srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_1968(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_u16vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k1984 in srfi-4#make-u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_1986(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_1986,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[52],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_1933,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:318: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1933(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_1933(2,av2);}}}

/* srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2024,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2112,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:329: alloc */
f_1622(t12,lf[56],C_fix(2),t2,t7);}

/* k2047 in k2110 in srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2049,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2089,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2060,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:115: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[57]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[57]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[56];
tp(4,av2);}}}

/* k2058 in k2047 in k2110 in srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(58,c,6)))){
C_save_and_reclaim((void *)f_2060,c,av);}
a=C_alloc(58);
t2=C_i_integer_length(((C_word*)t0)[2]);
t3=C_fixnum_difference(C_fix(16),C_fix(1));
if(C_truep(C_fixnum_greaterp(t2,t3))){
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
t5=C_s_a_i_negate(&a,1,C_fix(65536));
t6=C_s_a_i_minus(&a,2,C_fix(65536),C_fix(1));
/* srfi-4.scm:117: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t4;
av2[3]=lf[56];
av2[4]=((C_word*)t0)[2];
av2[5]=t5;
av2[6]=t6;
tp(7,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_2089(2,av2);}}}

/* k2087 in k2047 in k2110 in srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2089(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2089,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2094,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li29),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2094(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop370 in k2087 in k2047 in k2110 in srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2094(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_s16vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2110 in srfi-4#make-s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2112,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[55],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2049,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:330: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2049(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2049(2,av2);}}}

/* srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2150(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2150,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2228,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:341: alloc */
f_1622(t12,lf[60],C_fix(4),t2,t7);}

/* k2173 in k2226 in srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2175,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2205,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2186,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:109: ##sys#check-exact-uinteger */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[60];
tp(4,av2);}}}

/* k2184 in k2173 in k2226 in srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2186(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2186,c,av);}
if(C_truep(C_fixnum_greaterp(C_i_integer_length(((C_word*)t0)[2]),C_fix(32)))){
t2=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:111: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=lf[60];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=lf[61];
tp(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_2205(2,av2);}}}

/* k2203 in k2173 in k2226 in srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2205(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2205,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2210,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li31),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2210(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop404 in k2203 in k2173 in k2226 in srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2210(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_u32vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2226 in srfi-4#make-u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2228,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[59],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2175,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:342: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2175(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2175(2,av2);}}}

/* srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2266(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2266,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2344,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:353: alloc */
f_1622(t12,lf[64],C_fix(8),t2,t7);}

/* k2289 in k2342 in srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2291(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2291,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2321,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2302,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:109: ##sys#check-exact-uinteger */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[46]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[46]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[64];
tp(4,av2);}}}

/* k2300 in k2289 in k2342 in srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_2302,c,av);}
if(C_truep(C_fixnum_greaterp(C_i_integer_length(((C_word*)t0)[2]),C_fix(64)))){
t2=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:111: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=lf[64];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=lf[65];
tp(7,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_2321(2,av2);}}}

/* k2319 in k2289 in k2342 in srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2321,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2326,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li33),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2326(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop438 in k2319 in k2289 in k2342 in srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2326(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_u64vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2342 in srfi-4#make-u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2344,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[63],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2291,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:354: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2291(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2291(2,av2);}}}

/* srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2382,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2470,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:365: alloc */
f_1622(t12,lf[68],C_fix(4),t2,t7);}

/* k2405 in k2468 in srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2407,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2447,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2418,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:115: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[57]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[57]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[68];
tp(4,av2);}}}

/* k2416 in k2405 in k2468 in srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2418(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(58,c,6)))){
C_save_and_reclaim((void *)f_2418,c,av);}
a=C_alloc(58);
t2=C_i_integer_length(((C_word*)t0)[2]);
t3=C_fixnum_difference(C_fix(32),C_fix(1));
if(C_truep(C_fixnum_greaterp(t2,t3))){
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
t5=C_s_a_i_negate(&a,1,lf[61]);
t6=C_s_a_i_minus(&a,2,lf[61],C_fix(1));
/* srfi-4.scm:117: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t4;
av2[3]=lf[68];
av2[4]=((C_word*)t0)[2];
av2[5]=t5;
av2[6]=t6;
tp(7,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_2447(2,av2);}}}

/* k2445 in k2405 in k2468 in srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2447,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2452,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li35),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2452(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop472 in k2445 in k2405 in k2468 in srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2452(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_s32vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2468 in srfi-4#make-s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2470,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[67],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2407,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:366: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2407(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2407(2,av2);}}}

/* srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_2508,c,av);}
a=C_alloc(8);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2596,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t7,a[6]=t10,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:377: alloc */
f_1622(t12,lf[71],C_fix(8),t2,t7);}

/* k2531 in k2594 in srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2533(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_2533,c,av);}
a=C_alloc(10);
if(C_truep(C_i_not(((C_word*)t0)[2]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2573,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2544,a[2]=((C_word*)t0)[2],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:115: ##sys#check-exact-integer */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[57]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[57]+1);
av2[1]=t3;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[71];
tp(4,av2);}}}

/* k2542 in k2531 in k2594 in srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2544(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(58,c,6)))){
C_save_and_reclaim((void *)f_2544,c,av);}
a=C_alloc(58);
t2=C_i_integer_length(((C_word*)t0)[2]);
t3=C_fixnum_difference(C_fix(64),C_fix(1));
if(C_truep(C_fixnum_greaterp(t2,t3))){
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
t5=C_s_a_i_negate(&a,1,lf[65]);
t6=C_s_a_i_minus(&a,2,lf[65],C_fix(1));
/* srfi-4.scm:117: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[3];
av2[2]=t4;
av2[3]=lf[71];
av2[4]=((C_word*)t0)[2];
av2[5]=t5;
av2[6]=t6;
tp(7,av2);}}
else{
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_2573(2,av2);}}}

/* k2571 in k2531 in k2594 in srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_2573,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2578,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li37),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t3;
av2[1]=(
  f_2578(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop506 in k2571 in k2531 in k2594 in srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2578(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_s64vector_set(((C_word*)t0)[3],t1,((C_word*)t0)[4]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2594 in srfi-4#make-s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2596(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2596,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[70],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2533,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:378: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2533(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2533(2,av2);}}}

/* srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_2634,c,av);}
a=C_alloc(10);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_rest_nullp(c,3);
t8=C_rest_nullp(c,4);
t9=(C_truep(t8)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t10=C_rest_nullp(c,4);
t11=C_rest_nullp(c,5);
t12=(C_truep(t11)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t13=C_rest_nullp(c,5);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2707,a[2]=t6,a[3]=t1,a[4]=t2,a[5]=t9,a[6]=t12,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:389: alloc */
f_1622(t14,lf[74],C_fix(4),t2,t9);}

/* k2657 in k2705 in srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2659,c,av);}
a=C_alloc(6);
if(C_truep(C_i_not(((C_word*)((C_word*)t0)[2])[1]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2680,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)((C_word*)t0)[2])[1];
t4=C_i_exact_integerp(t3);
t5=(C_truep(t4)?t4:C_i_flonump(t3));
if(C_truep(t5)){
t6=C_SCHEME_UNDEFINED;
t7=t2;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
f_2680(2,av2);}}
else{
t6=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_FLONUM_ERROR);
/* srfi-4.scm:99: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=t2;
av2[2]=t6;
av2[3]=lf[74];
av2[4]=t3;
tp(5,av2);}}}}

/* k2678 in k2657 in k2705 in srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2680(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2680,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2683,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_flonump(((C_word*)((C_word*)t0)[4])[1]))){
t3=t2;
f_2683(t3,C_SCHEME_UNDEFINED);}
else{
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,C_a_u_i_int_to_flo(&a,1,((C_word*)((C_word*)t0)[4])[1]));
t4=t2;
f_2683(t4,t3);}}

/* k2681 in k2678 in k2657 in k2705 in srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2683(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2683,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2688,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li39),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t3;
av2[1]=(
  f_2688(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop541 in k2681 in k2678 in k2657 in k2705 in srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2688(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_f32vector_set(((C_word*)t0)[3],t1,((C_word*)((C_word*)t0)[4])[1]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2705 in srfi-4#make-f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2707,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[73],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2659,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:390: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2659(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2659(2,av2);}}}

/* srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2745(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(10,c,5)))){
C_save_and_reclaim((void *)f_2745,c,av);}
a=C_alloc(10);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?C_SCHEME_FALSE:C_get_rest_arg(c,3,av,3,t0));
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_rest_nullp(c,3);
t8=C_rest_nullp(c,4);
t9=(C_truep(t8)?C_SCHEME_FALSE:C_get_rest_arg(c,4,av,3,t0));
t10=C_rest_nullp(c,4);
t11=C_rest_nullp(c,5);
t12=(C_truep(t11)?C_SCHEME_TRUE:C_get_rest_arg(c,5,av,3,t0));
t13=C_rest_nullp(c,5);
t14=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_2818,a[2]=t6,a[3]=t1,a[4]=t2,a[5]=t9,a[6]=t12,a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:403: alloc */
f_1622(t14,lf[77],C_fix(8),t2,t9);}

/* k2768 in k2816 in srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_2770,c,av);}
a=C_alloc(6);
if(C_truep(C_i_not(((C_word*)((C_word*)t0)[2])[1]))){
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2791,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)((C_word*)t0)[2])[1];
t4=C_i_exact_integerp(t3);
t5=(C_truep(t4)?t4:C_i_flonump(t3));
if(C_truep(t5)){
t6=C_SCHEME_UNDEFINED;
t7=t2;{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
f_2791(2,av2);}}
else{
t6=C_fix((C_word)C_BAD_ARGUMENT_TYPE_NO_FLONUM_ERROR);
/* srfi-4.scm:99: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=t2;
av2[2]=t6;
av2[3]=lf[77];
av2[4]=t3;
tp(5,av2);}}}}

/* k2789 in k2768 in k2816 in srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2791(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_2791,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2794,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_flonump(((C_word*)((C_word*)t0)[4])[1]))){
t3=t2;
f_2794(t3,C_SCHEME_UNDEFINED);}
else{
t3=C_mutate(((C_word *)((C_word*)t0)[4])+1,C_a_u_i_int_to_flo(&a,1,((C_word*)((C_word*)t0)[4])[1]));
t4=t2;
f_2794(t4,t3);}}

/* k2792 in k2789 in k2768 in k2816 in srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2794(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_2794,2,t0,t1);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2799,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word)li41),tmp=(C_word)a,a+=6,tmp);
t3=((C_word*)t0)[5];{
C_word av2[2];
av2[0]=t3;
av2[1]=(
  f_2799(t2,C_fix(0))
);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* doloop577 in k2792 in k2789 in k2768 in k2816 in srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static C_word C_fcall f_2799(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_stack_overflow_check;
loop:{}
if(C_truep(C_fixnum_greater_or_equal_p(t1,((C_word*)t0)[2]))){
return(((C_word*)t0)[3]);}
else{
t2=C_u_i_f64vector_set(((C_word*)t0)[3],t1,((C_word*)((C_word*)t0)[4])[1]);
t4=C_fixnum_plus(t1,C_fix(1));
t1=t4;
goto loop;}}

/* k2816 in srfi-4#make-f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_2818,c,av);}
a=C_alloc(9);
t2=C_a_i_record2(&a,2,lf[76],t1);
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2770,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
if(C_truep(((C_word*)t0)[5])){
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:404: chicken.gc#set-finalizer! */
t4=*((C_word*)lf[47]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t2;
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2770(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_2770(2,av2);}}}

/* srfi-4#list->u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2856,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[43]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2863,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:439: make-u8vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k2861 in srfi-4#list->u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2863(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2863,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2868,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li43),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2868(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop612 in k2861 in srfi-4#list->u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2868(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2868,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2875,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_u8vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:439: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k2873 in doloop612 in k2861 in srfi-4#list->u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2875(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2875,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_2868(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2892,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[49]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2899,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:440: make-s8vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k2897 in srfi-4#list->s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2899,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2904,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li45),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2904(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop625 in k2897 in srfi-4#list->s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2904(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2904,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2911,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_s8vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:440: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k2909 in doloop625 in k2897 in srfi-4#list->s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2911(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2911,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_2904(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2928,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[52]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2935,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:441: make-u16vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k2933 in srfi-4#list->u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2935(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2935,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2940,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li47),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2940(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop638 in k2933 in srfi-4#list->u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2940(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2940,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2947,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_u16vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:441: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k2945 in doloop638 in k2933 in srfi-4#list->u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2947,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_2940(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_2964,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[55]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2971,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:442: make-s16vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k2969 in srfi-4#list->s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2971(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_2971,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2976,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li49),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_2976(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop651 in k2969 in srfi-4#list->s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_2976(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_2976,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_2983,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_s16vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:442: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k2981 in doloop651 in k2969 in srfi-4#list->s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_2983(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_2983,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_2976(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3000(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3000,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[59]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3007,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:443: make-u32vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3005 in srfi-4#list->u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3007(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3007,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3012,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li51),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3012(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop664 in k3005 in srfi-4#list->u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3012(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3012,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3019,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_u32vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:443: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3017 in doloop664 in k3005 in srfi-4#list->u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3019(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3019,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3012(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3036(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3036,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[67]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3043,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:444: make-s32vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3041 in srfi-4#list->s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3043,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3048,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li53),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3048(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop677 in k3041 in srfi-4#list->s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3048(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3048,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3055,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_s32vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:444: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3053 in doloop677 in k3041 in srfi-4#list->s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3055,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3048(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3072(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3072,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[63]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3079,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:445: make-u64vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3077 in srfi-4#list->u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3079,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3084,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li55),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3084(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop690 in k3077 in srfi-4#list->u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3084(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3084,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3091,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_u64vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:445: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3089 in doloop690 in k3077 in srfi-4#list->u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3091,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3084(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3108,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[70]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3115,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:446: make-s64vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)C_fast_retrieve_proc(t6))(3,av2);}}

/* k3113 in srfi-4#list->s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3115,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3120,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li57),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3120(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop703 in k3113 in srfi-4#list->s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3120(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3120,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3127,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_s64vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:446: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3125 in doloop703 in k3113 in srfi-4#list->s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3127(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3127,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3120(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3144,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[73]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3151,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:447: make-f32vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3149 in srfi-4#list->f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3151,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3156,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li59),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3156(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop716 in k3149 in srfi-4#list->f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3156(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3156,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3163,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_f32vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:447: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3161 in doloop716 in k3149 in srfi-4#list->f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3163(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3163,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3156(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#list->f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3180,c,av);}
a=C_alloc(4);
t3=C_i_check_list_2(t2,lf[76]);
t4=C_i_length(t2);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3187,a[2]=t2,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:448: make-f64vector */
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3185 in srfi-4#list->f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3187(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_3187,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3192,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[2],a[5]=((C_word)li61),tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_3192(t5,((C_word*)t0)[3],((C_word*)t0)[2],C_fix(0));}

/* doloop729 in k3185 in srfi-4#list->f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3192(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3192,4,t0,t1,t2,t3);}
a=C_alloc(6);
if(C_truep(C_eqp(t2,C_SCHEME_END_OF_LIST))){
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3199,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
t5=(C_truep(C_blockp(t2))?C_pairp(t2):C_SCHEME_FALSE);
if(C_truep(t5)){
t6=C_i_f64vector_set(((C_word*)t0)[2],t3,C_slot(t2,C_fix(0)));
t8=t1;
t9=C_slot(t2,C_fix(1));
t10=C_fixnum_plus(t3,C_fix(1));
t1=t8;
t2=t9;
t3=t10;
goto loop;}
else{
/* srfi-4.scm:448: ##sys#error-not-a-proper-list */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[79]+1));
C_word av2[3];
av2[0]=*((C_word*)lf[79]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
tp(3,av2);}}}}

/* k3197 in doloop729 in k3185 in srfi-4#list->f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3199,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_3192(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)),C_fixnum_plus(((C_word*)t0)[5],C_fix(1)));}

/* srfi-4#u8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3216(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3216,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:454: list->u8vector */
t3=*((C_word*)lf[78]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#s8vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3222,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:457: list->s8vector */
t3=*((C_word*)lf[80]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#u16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3228(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3228,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:460: list->u16vector */
t3=*((C_word*)lf[81]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#s16vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3234,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:463: list->s16vector */
t3=*((C_word*)lf[82]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#u32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3240(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3240,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:466: list->u32vector */
t3=*((C_word*)lf[83]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#s32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3246,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:469: list->s32vector */
t3=*((C_word*)lf[84]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#u64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3252(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3252,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:472: list->u64vector */
t3=*((C_word*)lf[85]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#s64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3258(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3258,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:475: list->s64vector */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[86]+1));
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=*((C_word*)lf[86]+1);
av2[1]=t1;
av2[2]=t2;
tp(3,av2);}}

/* srfi-4#f32vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3264(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3264,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:478: list->f32vector */
t3=*((C_word*)lf[87]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#f64vector in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3270(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-2)*C_SIZEOF_PAIR +0,c,2)))){
C_save_and_reclaim((void*)f_3270,c,av);}
a=C_alloc((c-2)*C_SIZEOF_PAIR+0);
t2=C_build_rest(&a,c,2,av);
C_word t3;
/* srfi-4.scm:481: list->f64vector */
t3=*((C_word*)lf[88]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* srfi-4#u8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3276(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3276,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[43],lf[100]);
t4=C_u_i_u8vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3285,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li73),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3285(t8,t1,C_fix(0));}

/* loop in srfi-4#u8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3285(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3285,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_u8vector_ref(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3300,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:504: loop */
t6=t4;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k3298 in loop in srfi-4#u8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3300,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#s8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3306(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3306,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[49],lf[102]);
t4=C_u_i_s8vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3315,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li75),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3315(t8,t1,C_fix(0));}

/* loop in srfi-4#s8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3315(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3315,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_s8vector_ref(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3330,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:505: loop */
t6=t4;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k3328 in loop in srfi-4#s8vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3330,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#u16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3336(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3336,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[52],lf[104]);
t4=C_u_i_u16vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3345,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li77),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3345(t8,t1,C_fix(0));}

/* loop in srfi-4#u16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3345(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3345,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_u16vector_ref(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3360,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:506: loop */
t6=t4;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k3358 in loop in srfi-4#u16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3360(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3360,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#s16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3366(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3366,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[55],lf[106]);
t4=C_u_i_s16vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3375,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li79),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3375(t8,t1,C_fix(0));}

/* loop in srfi-4#s16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3375(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3375,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_u_i_s16vector_ref(((C_word*)t0)[3],t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3390,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:507: loop */
t6=t4;
t7=C_fixnum_plus(t2,C_fix(1));
t1=t6;
t2=t7;
goto loop;}}

/* k3388 in loop in srfi-4#s16vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3390(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3390,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#u32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3396(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3396,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[59],lf[108]);
t4=C_u_i_u32vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3405,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li81),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3405(t8,t1,C_fix(0));}

/* loop in srfi-4#u32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3405(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3405,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3419,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:509: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3417 in loop in srfi-4#u32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_3419,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_u32vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#s32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3425(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3425,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[67],lf[110]);
t4=C_u_i_s32vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3434,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li83),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3434(t8,t1,C_fix(0));}

/* loop in srfi-4#s32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3434(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3434,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3448,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:510: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3446 in loop in srfi-4#s32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,1)))){
C_save_and_reclaim((void *)f_3448,c,av);}
a=C_alloc(9);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_s32vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#u64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3454,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[63],lf[112]);
t4=C_u_i_u64vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3463,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li85),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3463(t8,t1,C_fix(0));}

/* loop in srfi-4#u64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3463(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3463,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3477,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:511: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3475 in loop in srfi-4#u64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3477(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,1)))){
C_save_and_reclaim((void *)f_3477,c,av);}
a=C_alloc(10);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_u64vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#s64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3483(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3483,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[70],lf[114]);
t4=C_u_i_s64vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3492,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li87),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3492(t8,t1,C_fix(0));}

/* loop in srfi-4#s64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3492(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3492,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3506,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:512: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3504 in loop in srfi-4#s64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3506(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,1)))){
C_save_and_reclaim((void *)f_3506,c,av);}
a=C_alloc(10);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_s64vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#f32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3512(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3512,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[73],lf[116]);
t4=C_u_i_f32vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3521,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li89),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3521(t8,t1,C_fix(0));}

/* loop in srfi-4#f32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3521(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3521,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3535,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:513: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3533 in loop in srfi-4#f32vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3535(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3535,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_f32vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#f64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3541(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_3541,c,av);}
a=C_alloc(8);
t3=C_i_check_structure_2(t2,lf[76],lf[118]);
t4=C_u_i_f64vector_length(t2);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3550,a[2]=t4,a[3]=t2,a[4]=t6,a[5]=((C_word)li91),tmp=(C_word)a,a+=6,tmp));
t8=((C_word*)t6)[1];
f_3550(t8,t1,C_fix(0));}

/* loop in srfi-4#f64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3550(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3550,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_fixnum_greater_or_equal_p(t2,((C_word*)t0)[2]))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3564,a[2]=t1,a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:514: loop */
t5=t3;
t6=C_fixnum_plus(t2,C_fix(1));
t1=t5;
t2=t6;
goto loop;}}

/* k3562 in loop in srfi-4#f64vector->list in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_3564,c,av);}
a=C_alloc(7);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,C_a_u_i_f64vector_ref(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]),t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#u8vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3570,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u8vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s8vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3573,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s8vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u16vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3576(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3576,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u16vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s16vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3579(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3579,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s16vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u32vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3582(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3582,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u32vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s32vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3585(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3585,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s32vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#u64vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3588,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_u64vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#s64vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3591(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3591,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_s64vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#f32vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3594(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3594,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_f32vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#f64vector? in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3597,c,av);}
t3=t1;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_i_f64vectorp(t2);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* srfi-4#pack-copy in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3612(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_3612,3,t1,t2,t3);}
a=C_alloc(5);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3614,a[2]=t2,a[3]=t3,a[4]=((C_word)li103),tmp=(C_word)a,a+=5,tmp);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* f_3614 in srfi-4#pack-copy in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3614(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3614,c,av);}
a=C_alloc(4);
t3=C_i_check_structure_2(t2,((C_word*)t0)[2],((C_word*)t0)[3]);
t4=C_slot(t2,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3624,a[2]=t1,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:544: ##sys#make-blob */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t5;
av2[2]=C_block_size(t4);
tp(3,av2);}}

/* k3622 */
static void C_ccall f_3624(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3624,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_copy_block(((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* srfi-4#unpack in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3630(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3630,4,t1,t2,t3,t4);}
a=C_alloc(6);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3632,a[2]=t4,a[3]=t3,a[4]=t2,a[5]=((C_word)li105),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* f_3632 in srfi-4#unpack in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3632,c,av);}
a=C_alloc(3);
t3=C_i_check_bytevector_2(t2,((C_word*)t0)[2]);
t4=C_block_size(t2);
t5=C_eqp(C_SCHEME_TRUE,((C_word*)t0)[3]);
t6=(C_truep(t5)?t5:C_eqp(C_fix(0),C_fixnum_modulo(t4,((C_word*)t0)[3])));
if(C_truep(t6)){
t7=t1;{
C_word *av2=av;
av2[0]=t7;
av2[1]=C_a_i_record2(&a,2,((C_word*)t0)[4],t2);
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}
else{
/* srfi-4.scm:554: ##sys#error */
t7=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t7;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=lf[133];
av2[4]=((C_word*)t0)[4];
av2[5]=t4;
av2[6]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t7+1)))(7,av2);}}}

/* srfi-4#unpack-copy in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_fcall f_3660(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3660,4,t1,t2,t3,t4);}
a=C_alloc(6);
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3662,a[2]=t4,a[3]=t3,a[4]=t2,a[5]=((C_word)li107),tmp=(C_word)a,a+=6,tmp);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* f_3662 in srfi-4#unpack-copy in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(c!=3) C_bad_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_3662,c,av);}
a=C_alloc(8);
t3=C_i_check_bytevector_2(t2,((C_word*)t0)[2]);
t4=C_block_size(t2);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_3672,a[2]=((C_word*)t0)[3],a[3]=t4,a[4]=t2,a[5]=t1,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[2],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:560: ##sys#make-blob */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[131]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[131]+1);
av2[1]=t5;
av2[2]=t4;
tp(3,av2);}}

/* k3670 */
static void C_ccall f_3672(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,6)))){
C_save_and_reclaim((void *)f_3672,c,av);}
a=C_alloc(3);
t2=C_eqp(C_SCHEME_TRUE,((C_word*)t0)[2]);
t3=(C_truep(t2)?t2:C_eqp(C_fix(0),C_fixnum_modulo(((C_word*)t0)[3],((C_word*)t0)[2])));
if(C_truep(t3)){
t4=C_copy_block(((C_word*)t0)[4],t1);
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_a_i_record2(&a,2,((C_word*)t0)[6],t4);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
/* srfi-4.scm:566: ##sys#error */
t4=*((C_word*)lf[32]+1);{
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)t0)[7];
av2[3]=lf[135];
av2[4]=((C_word*)t0)[6];
av2[5]=((C_word*)t0)[3];
av2[6]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t4+1)))(7,av2);}}}

/* k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3736,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[156]+1 /* (set! srfi-4#u8vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3740,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:580: pack-copy */
f_3612(t3,lf[49],lf[264]);}

/* k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3740,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[157]+1 /* (set! srfi-4#s8vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3744,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:581: pack-copy */
f_3612(t3,lf[52],lf[263]);}

/* k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3744(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3744,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[158]+1 /* (set! srfi-4#u16vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3748,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:582: pack-copy */
f_3612(t3,lf[55],lf[262]);}

/* k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3748(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3748,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[159]+1 /* (set! srfi-4#s16vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3752,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:583: pack-copy */
f_3612(t3,lf[59],lf[261]);}

/* k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3752,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[160]+1 /* (set! srfi-4#u32vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3756,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:584: pack-copy */
f_3612(t3,lf[67],lf[260]);}

/* k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3756(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3756,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[161]+1 /* (set! srfi-4#s32vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3760,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:585: pack-copy */
f_3612(t3,lf[63],lf[259]);}

/* k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3760(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3760,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[162]+1 /* (set! srfi-4#u64vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3764,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:586: pack-copy */
f_3612(t3,lf[70],lf[258]);}

/* k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_3764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3764,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[163]+1 /* (set! srfi-4#s64vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3768,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:587: pack-copy */
f_3612(t3,lf[73],lf[257]);}

/* k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 in ... */
static void C_ccall f_3768(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3768,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[164]+1 /* (set! srfi-4#f32vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3772,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:588: pack-copy */
f_3612(t3,lf[76],lf[256]);}

/* k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in ... */
static void C_ccall f_3772(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3772,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[165]+1 /* (set! srfi-4#f64vector->blob ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3776,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:590: unpack */
f_3630(t3,lf[43],C_SCHEME_TRUE,lf[255]);}

/* k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in ... */
static void C_ccall f_3776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3776,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[166]+1 /* (set! srfi-4#blob->u8vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3780,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:591: unpack */
f_3630(t3,lf[49],C_SCHEME_TRUE,lf[254]);}

/* k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in ... */
static void C_ccall f_3780(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3780,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[167]+1 /* (set! srfi-4#blob->s8vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3784,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:592: unpack */
f_3630(t3,lf[52],C_fix(2),lf[253]);}

/* k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in ... */
static void C_ccall f_3784(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3784,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[168]+1 /* (set! srfi-4#blob->u16vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3788,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:593: unpack */
f_3630(t3,lf[55],C_fix(2),lf[252]);}

/* k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in ... */
static void C_ccall f_3788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3788,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[169]+1 /* (set! srfi-4#blob->s16vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3792,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:594: unpack */
f_3630(t3,lf[59],C_fix(4),lf[251]);}

/* k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in ... */
static void C_ccall f_3792(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3792,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[170]+1 /* (set! srfi-4#blob->u32vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3796,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:595: unpack */
f_3630(t3,lf[67],C_fix(4),lf[250]);}

/* k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in k1590 in ... */
static void C_ccall f_3796(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3796,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[171]+1 /* (set! srfi-4#blob->s32vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3800,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:596: unpack */
f_3630(t3,lf[63],C_fix(4),lf[249]);}

/* k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in k1594 in ... */
static void C_ccall f_3800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3800,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[172]+1 /* (set! srfi-4#blob->u64vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3804,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:597: unpack */
f_3630(t3,lf[70],C_fix(4),lf[248]);}

/* k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in k1598 in ... */
static void C_ccall f_3804(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3804,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[173]+1 /* (set! srfi-4#blob->s64vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3808,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:598: unpack */
f_3630(t3,lf[73],C_fix(4),lf[247]);}

/* k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in k1602 in ... */
static void C_ccall f_3808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3808,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[174]+1 /* (set! srfi-4#blob->f32vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3812,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:599: unpack */
f_3630(t3,lf[76],C_fix(8),lf[246]);}

/* k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in k1606 in ... */
static void C_ccall f_3812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3812,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[175]+1 /* (set! srfi-4#blob->f64vector/shared ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3816,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:601: unpack-copy */
f_3660(t3,lf[43],C_SCHEME_TRUE,lf[245]);}

/* k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in k1610 in ... */
static void C_ccall f_3816(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3816,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[176]+1 /* (set! srfi-4#blob->u8vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3820,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:602: unpack-copy */
f_3660(t3,lf[49],C_SCHEME_TRUE,lf[244]);}

/* k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in k3734 in ... */
static void C_ccall f_3820(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3820,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[177]+1 /* (set! srfi-4#blob->s8vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3824,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:603: unpack-copy */
f_3660(t3,lf[52],C_fix(2),lf[243]);}

/* k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in k3738 in ... */
static void C_ccall f_3824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3824,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[178]+1 /* (set! srfi-4#blob->u16vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3828,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:604: unpack-copy */
f_3660(t3,lf[55],C_fix(2),lf[242]);}

/* k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in k3742 in ... */
static void C_ccall f_3828(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3828,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[179]+1 /* (set! srfi-4#blob->s16vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3832,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:605: unpack-copy */
f_3660(t3,lf[59],C_fix(4),lf[241]);}

/* k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in k3746 in ... */
static void C_ccall f_3832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3832,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[180]+1 /* (set! srfi-4#blob->u32vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3836,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:606: unpack-copy */
f_3660(t3,lf[67],C_fix(4),lf[240]);}

/* k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in k3750 in ... */
static void C_ccall f_3836(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3836,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[181]+1 /* (set! srfi-4#blob->s32vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3840,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:607: unpack-copy */
f_3660(t3,lf[63],C_fix(4),lf[239]);}

/* k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in k3754 in ... */
static void C_ccall f_3840(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3840,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[182]+1 /* (set! srfi-4#blob->u64vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3844,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:608: unpack-copy */
f_3660(t3,lf[70],C_fix(4),lf[238]);}

/* k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in k3758 in ... */
static void C_ccall f_3844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3844,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[183]+1 /* (set! srfi-4#blob->s64vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3848,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:609: unpack-copy */
f_3660(t3,lf[73],C_fix(4),lf[237]);}

/* k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in k3762 in ... */
static void C_ccall f_3848(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_3848,c,av);}
a=C_alloc(3);
t2=C_mutate((C_word*)lf[184]+1 /* (set! srfi-4#blob->f32vector ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3852,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:610: unpack-copy */
f_3660(t3,lf[76],C_fix(8),lf[236]);}

/* k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in k3766 in ... */
static void C_ccall f_3852(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(114,c,8)))){
C_save_and_reclaim((void *)f_3852,c,av);}
a=C_alloc(114);
t2=C_mutate((C_word*)lf[185]+1 /* (set! srfi-4#blob->f64vector ...) */,t1);
t3=*((C_word*)lf[186]+1);
t4=C_a_i_list(&a,20,lf[187],*((C_word*)lf[78]+1),lf[188],*((C_word*)lf[80]+1),lf[189],*((C_word*)lf[81]+1),lf[190],*((C_word*)lf[82]+1),lf[191],*((C_word*)lf[83]+1),lf[192],*((C_word*)lf[84]+1),lf[193],*((C_word*)lf[85]+1),lf[194],*((C_word*)lf[86]+1),lf[195],*((C_word*)lf[87]+1),lf[196],*((C_word*)lf[88]+1));
t5=C_mutate((C_word*)lf[186]+1 /* (set! ##sys#user-read-hook ...) */,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3857,a[2]=t4,a[3]=t3,a[4]=((C_word)li120),tmp=(C_word)a,a+=5,tmp));
t6=*((C_word*)lf[202]+1);
t7=C_mutate((C_word*)lf[202]+1 /* (set! ##sys#user-print-hook ...) */,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3917,a[2]=t6,a[3]=((C_word)li121),tmp=(C_word)a,a+=4,tmp));
t8=C_mutate(&lf[204] /* (set! srfi-4#subnvector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3991,a[2]=((C_word)li122),tmp=(C_word)a,a+=3,tmp));
t9=C_mutate((C_word*)lf[205]+1 /* (set! srfi-4#subu8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4076,a[2]=((C_word)li123),tmp=(C_word)a,a+=3,tmp));
t10=C_mutate((C_word*)lf[207]+1 /* (set! srfi-4#subu16vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4082,a[2]=((C_word)li124),tmp=(C_word)a,a+=3,tmp));
t11=C_mutate((C_word*)lf[209]+1 /* (set! srfi-4#subu32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4088,a[2]=((C_word)li125),tmp=(C_word)a,a+=3,tmp));
t12=C_mutate((C_word*)lf[211]+1 /* (set! srfi-4#subu64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4094,a[2]=((C_word)li126),tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[213]+1 /* (set! srfi-4#subs8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4100,a[2]=((C_word)li127),tmp=(C_word)a,a+=3,tmp));
t14=C_mutate((C_word*)lf[215]+1 /* (set! srfi-4#subs16vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4106,a[2]=((C_word)li128),tmp=(C_word)a,a+=3,tmp));
t15=C_mutate((C_word*)lf[217]+1 /* (set! srfi-4#subs32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4112,a[2]=((C_word)li129),tmp=(C_word)a,a+=3,tmp));
t16=C_mutate((C_word*)lf[219]+1 /* (set! srfi-4#subs64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4118,a[2]=((C_word)li130),tmp=(C_word)a,a+=3,tmp));
t17=C_mutate((C_word*)lf[221]+1 /* (set! srfi-4#subf32vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4124,a[2]=((C_word)li131),tmp=(C_word)a,a+=3,tmp));
t18=C_mutate((C_word*)lf[223]+1 /* (set! srfi-4#subf64vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4130,a[2]=((C_word)li132),tmp=(C_word)a,a+=3,tmp));
t19=C_mutate((C_word*)lf[225]+1 /* (set! srfi-4#write-u8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4136,a[2]=((C_word)li133),tmp=(C_word)a,a+=3,tmp));
t20=C_mutate((C_word*)lf[228]+1 /* (set! srfi-4#read-u8vector! ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4305,a[2]=((C_word)li134),tmp=(C_word)a,a+=3,tmp));
t21=C_mutate((C_word*)lf[232]+1 /* (set! srfi-4#read-u8vector ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4385,a[2]=((C_word)li135),tmp=(C_word)a,a+=3,tmp));
t22=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4446,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:722: chicken.platform#register-feature! */
t23=*((C_word*)lf[235]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t23;
av2[1]=t22;
av2[2]=lf[0];
((C_proc)(void*)(*((C_word*)t23+1)))(3,av2);}}

/* ##sys#user-read-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_3857(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_3857,c,av);}
a=C_alloc(5);
if(C_truep((C_truep(C_eqp(t2,C_make_character(117)))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,C_make_character(115)))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,C_make_character(102)))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,C_make_character(85)))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,C_make_character(83)))?C_SCHEME_TRUE:(C_truep(C_eqp(t2,C_make_character(70)))?C_SCHEME_TRUE:C_SCHEME_FALSE)))))))){
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3866,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:630: read */
t5=*((C_word*)lf[199]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
/* srfi-4.scm:635: old-hook */
t4=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
((C_proc)C_fast_retrieve_proc(t4))(4,av2);}}}

/* k3864 in ##sys#user-read-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_3866(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_3866,c,av);}
a=C_alloc(4);
t2=C_i_symbolp(t1);
t3=(C_truep(t2)?t1:C_SCHEME_FALSE);
t4=C_eqp(t3,lf[197]);
t5=(C_truep(t4)?t4:C_eqp(t3,lf[198]));
if(C_truep(t5)){
t6=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}
else{
t6=C_i_memq(t3,((C_word*)t0)[3]);
if(C_truep(t6)){
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3885,a[2]=((C_word*)t0)[4],a[3]=((C_word)li119),tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:632: g955 */
t8=t7;
f_3885(t8,((C_word*)t0)[2],t6);}
else{
/* srfi-4.scm:634: ##sys#read-error */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[200]+1));
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=*((C_word*)lf[200]+1);
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[4];
av2[3]=lf[201];
av2[4]=t3;
tp(5,av2);}}}}

/* g955 in k3864 in ##sys#user-read-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_fcall f_3885(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3885,3,t0,t1,t2);}
a=C_alloc(4);
t3=C_slot(t2,C_fix(1));
t4=C_slot(t3,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3896,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:633: read */
t6=*((C_word*)lf[199]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}

/* k3894 in g955 in k3864 in ##sys#user-read-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in ... */
static void C_ccall f_3896(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3896,c,av);}
/* srfi-4.scm:633: g958 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=t1;
((C_proc)C_fast_retrieve_proc(t2))(3,av2);}}

/* ##sys#user-print-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_3917(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(126,c,4)))){
C_save_and_reclaim((void *)f_3917,c,av);}
a=C_alloc(126);
t5=C_slot(t2,C_fix(0));
t6=C_a_i_list(&a,3,lf[43],lf[187],*((C_word*)lf[99]+1));
t7=C_a_i_list(&a,3,lf[49],lf[188],*((C_word*)lf[101]+1));
t8=C_a_i_list(&a,3,lf[52],lf[189],*((C_word*)lf[103]+1));
t9=C_a_i_list(&a,3,lf[55],lf[190],*((C_word*)lf[105]+1));
t10=C_a_i_list(&a,3,lf[59],lf[191],*((C_word*)lf[107]+1));
t11=C_a_i_list(&a,3,lf[67],lf[192],*((C_word*)lf[109]+1));
t12=C_a_i_list(&a,3,lf[63],lf[193],*((C_word*)lf[111]+1));
t13=C_a_i_list(&a,3,lf[70],lf[194],*((C_word*)lf[113]+1));
t14=C_a_i_list(&a,3,lf[73],lf[195],*((C_word*)lf[115]+1));
t15=C_a_i_list(&a,3,lf[76],lf[196],*((C_word*)lf[117]+1));
t16=C_a_i_list(&a,10,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15);
t17=C_u_i_assq(t5,t16);
if(C_truep(t17)){
t18=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3929,a[2]=t17,a[3]=t1,a[4]=t4,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* srfi-4.scm:655: ##sys#print */
t19=*((C_word*)lf[203]+1);{
C_word *av2=av;
av2[0]=t19;
av2[1]=t18;
av2[2]=C_make_character(35);
av2[3]=C_SCHEME_FALSE;
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t19+1)))(5,av2);}}
else{
/* srfi-4.scm:658: old-hook */
t18=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t18;
av2[1]=t1;
av2[2]=t2;
av2[3]=t3;
av2[4]=t4;
((C_proc)C_fast_retrieve_proc(t18))(5,av2);}}}

/* k3927 in ##sys#user-print-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_3929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_3929,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_3932,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* srfi-4.scm:656: ##sys#print */
t3=*((C_word*)lf[203]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_cadr(((C_word*)t0)[2]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k3930 in k3927 in ##sys#user-print-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_3932(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_3932,c,av);}
a=C_alloc(4);
t2=C_i_caddr(((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3942,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* srfi-4.scm:657: g971 */
t4=t2;{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[5];
((C_proc)C_fast_retrieve_proc(t4))(3,av2);}}

/* k3940 in k3930 in k3927 in ##sys#user-print-hook in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in ... */
static void C_ccall f_3942(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_3942,c,av);}
/* srfi-4.scm:657: ##sys#print */
t2=*((C_word*)lf[203]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_fcall f_3991(C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6,C_word t7){
C_word tmp;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,3)))){
C_save_and_reclaim_args((void *)trf_3991,7,t1,t2,t3,t4,t5,t6,t7);}
a=C_alloc(16);
t8=C_i_check_structure_2(t2,t3,t7);
t9=C_slot(t2,C_fix(1));
t10=C_block_size(t9);
t11=C_u_fixnum_divide(t10,t4);
t12=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4025,a[2]=t6,a[3]=t5,a[4]=t4,a[5]=t3,a[6]=t9,a[7]=t1,a[8]=t11,a[9]=t7,tmp=(C_word)a,a+=10,tmp);
t13=C_fixnum_plus(t11,C_fix(1));
t14=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4006,a[2]=t5,a[3]=t13,a[4]=t12,a[5]=t7,tmp=(C_word)a,a+=6,tmp);
/* srfi-4.scm:102: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t14;
av2[2]=t5;
av2[3]=t7;
tp(4,av2);}}

/* k4004 in srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4006(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4006,c,av);}
t2=C_fixnum_less_or_equal_p(C_fix(0),((C_word*)t0)[2]);
t3=(C_truep(t2)?C_fixnum_lessp(((C_word*)t0)[2],((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_4025(2,av2);}}
else{
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:104: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t4;
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=((C_word*)t0)[3];
tp(7,av2);}}}

/* k4023 in srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4025(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4025,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4049,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
t3=C_fixnum_plus(((C_word*)t0)[8],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4030,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=t2,a[5]=((C_word*)t0)[9],tmp=(C_word)a,a+=6,tmp);
/* srfi-4.scm:102: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[2];
av2[3]=((C_word*)t0)[9];
tp(4,av2);}}

/* k4028 in k4023 in srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4030(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4030,c,av);}
t2=C_fixnum_less_or_equal_p(C_fix(0),((C_word*)t0)[2]);
t3=(C_truep(t2)?C_fixnum_lessp(((C_word*)t0)[2],((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_4049(2,av2);}}
else{
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:104: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t4;
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=((C_word*)t0)[3];
tp(7,av2);}}}

/* k4047 in k4023 in srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,5)))){
C_save_and_reclaim((void *)f_4049,c,av);}
a=C_alloc(8);
t2=C_fixnum_difference(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=C_fixnum_times(((C_word*)t0)[4],t2);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4055,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[6],a[6]=t3,a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* srfi-4.scm:671: ##sys#allocate-vector */
t5=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
av2[3]=C_SCHEME_TRUE;
av2[4]=C_SCHEME_FALSE;
av2[5]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t5+1)))(6,av2);}}

/* k4053 in k4047 in k4023 in srfi-4#subnvector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in ... */
static void C_ccall f_4055(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4055,c,av);}
a=C_alloc(3);
t2=C_string_to_bytevector(t1);
t3=C_a_i_record2(&a,2,((C_word*)t0)[2],t1);
t4=C_fixnum_times(((C_word*)t0)[3],((C_word*)t0)[4]);
t5=C_copy_subvector(t1,((C_word*)t0)[5],C_fix(0),t4,((C_word*)t0)[6]);
t6=((C_word*)t0)[7];{
C_word *av2=av;
av2[0]=t6;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* srfi-4#subu8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4076(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4076,c,av);}
/* srfi-4.scm:677: subnvector */
f_3991(t1,t2,lf[43],C_fix(1),t3,t4,lf[206]);}

/* srfi-4#subu16vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4082,c,av);}
/* srfi-4.scm:678: subnvector */
f_3991(t1,t2,lf[52],C_fix(2),t3,t4,lf[208]);}

/* srfi-4#subu32vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4088,c,av);}
/* srfi-4.scm:679: subnvector */
f_3991(t1,t2,lf[59],C_fix(4),t3,t4,lf[210]);}

/* srfi-4#subu64vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4094,c,av);}
/* srfi-4.scm:680: subnvector */
f_3991(t1,t2,lf[63],C_fix(8),t3,t4,lf[212]);}

/* srfi-4#subs8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4100(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4100,c,av);}
/* srfi-4.scm:681: subnvector */
f_3991(t1,t2,lf[49],C_fix(1),t3,t4,lf[214]);}

/* srfi-4#subs16vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4106(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4106,c,av);}
/* srfi-4.scm:682: subnvector */
f_3991(t1,t2,lf[55],C_fix(2),t3,t4,lf[216]);}

/* srfi-4#subs32vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4112(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4112,c,av);}
/* srfi-4.scm:683: subnvector */
f_3991(t1,t2,lf[67],C_fix(4),t3,t4,lf[218]);}

/* srfi-4#subs64vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4118(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4118,c,av);}
/* srfi-4.scm:684: subnvector */
f_3991(t1,t2,lf[70],C_fix(8),t3,t4,lf[220]);}

/* srfi-4#subf32vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4124(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4124,c,av);}
/* srfi-4.scm:685: subnvector */
f_3991(t1,t2,lf[73],C_fix(4),t3,t4,lf[222]);}

/* srfi-4#subf64vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4130(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word *a;
if(c!=5) C_bad_argc_2(c,5,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_4130,c,av);}
/* srfi-4.scm:686: subnvector */
f_3991(t1,t2,lf[76],C_fix(8),t3,t4,lf[224]);}

/* srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4136(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word *a;
if(c<3) C_bad_min_argc_2(c,3,t0);
if(C_unlikely(!C_demand(C_calculate_demand(13,c,3)))){
C_save_and_reclaim((void *)f_4136,c,av);}
a=C_alloc(13);
t3=C_rest_nullp(c,3);
t4=(C_truep(t3)?*((C_word*)lf[226]+1):C_get_rest_arg(c,3,av,3,t0));
t5=C_rest_nullp(c,3);
t6=C_rest_nullp(c,4);
t7=(C_truep(t6)?C_fix(0):C_get_rest_arg(c,4,av,3,t0));
t8=C_rest_nullp(c,4);
t9=C_rest_nullp(c,5);
t10=(C_truep(t9)?C_SCHEME_FALSE:C_get_rest_arg(c,5,av,3,t0));
t11=C_rest_nullp(c,5);
t12=C_i_check_structure_2(t2,lf[43],lf[227]);
t13=C_i_check_port_2(t4,C_fix(2),C_SCHEME_TRUE,lf[227]);
t14=C_u_i_8vector_length(t2);
t15=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4185,a[2]=t4,a[3]=t1,a[4]=t2,a[5]=t10,a[6]=t7,a[7]=t14,tmp=(C_word)a,a+=8,tmp);
t16=(C_truep(t10)?C_fixnum_plus(t10,C_fix(1)):C_fixnum_plus(t14,C_fix(1)));
t17=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4166,a[2]=t7,a[3]=t16,a[4]=t15,tmp=(C_word)a,a+=5,tmp);
/* srfi-4.scm:102: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t17;
av2[2]=t7;
av2[3]=lf[227];
tp(4,av2);}}

/* k4164 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4166(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4166,c,av);}
t2=C_fixnum_less_or_equal_p(C_fix(0),((C_word*)t0)[2]);
t3=(C_truep(t2)?C_fixnum_lessp(((C_word*)t0)[2],((C_word*)t0)[3]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[4];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_4185(2,av2);}}
else{
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:104: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[4];
av2[2]=t4;
av2[3]=lf[227];
av2[4]=((C_word*)t0)[2];
av2[5]=C_fix(0);
av2[6]=((C_word*)t0)[3];
tp(7,av2);}}}

/* k4183 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4185(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,3)))){
C_save_and_reclaim((void *)f_4185,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4188,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(((C_word*)t0)[5])){
t3=C_fixnum_plus(((C_word*)t0)[7],C_fix(1));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4236,a[2]=((C_word*)t0)[6],a[3]=((C_word*)t0)[5],a[4]=t3,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* srfi-4.scm:102: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t4;
av2[2]=((C_word*)t0)[5];
av2[3]=lf[227];
tp(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4188(2,av2);}}}

/* k4186 in k4183 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4188(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_4188,c,av);}
a=C_alloc(9);
t2=C_slot(((C_word*)t0)[2],C_fix(2));
t3=C_slot(t2,C_fix(3));
t4=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4201,a[2]=t3,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
t5=C_eqp(((C_word*)t0)[6],C_fix(0));
if(C_truep(t5)){
t6=C_i_not(((C_word*)t0)[5]);
t7=t4;
f_4201(t7,(C_truep(t6)?t6:C_eqp(((C_word*)t0)[5],((C_word*)t0)[7])));}
else{
t6=t4;
f_4201(t6,C_SCHEME_FALSE);}}

/* k4199 in k4186 in k4183 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in ... */
static void C_fcall f_4201(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_4201,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
/* srfi-4.scm:691: g1092 */
t2=((C_word*)t0)[2];{
C_word av2[4];
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4211,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[6])){
/* srfi-4.scm:700: subu8vector */
t3=*((C_word*)lf[205]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[7];
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
/* srfi-4.scm:700: subu8vector */
t3=*((C_word*)lf[205]+1);{
C_word av2[5];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[7];
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}}}

/* k4209 in k4199 in k4186 in k4183 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in ... */
static void C_ccall f_4211(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4211,c,av);}
/* srfi-4.scm:691: g1092 */
t2=((C_word*)t0)[2];{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=C_slot(t1,C_fix(1));
((C_proc)C_fast_retrieve_proc(t2))(4,av2);}}

/* k4234 in k4183 in srfi-4#write-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4236(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,6)))){
C_save_and_reclaim((void *)f_4236,c,av);}
t2=C_fixnum_less_or_equal_p(((C_word*)t0)[2],((C_word*)t0)[3]);
t3=(C_truep(t2)?C_fixnum_lessp(((C_word*)t0)[3],((C_word*)t0)[4]):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=C_SCHEME_UNDEFINED;
t5=((C_word*)t0)[5];{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
f_4188(2,av2);}}
else{
t4=C_fix((C_word)C_OUT_OF_RANGE_ERROR);
/* srfi-4.scm:104: ##sys#error-hook */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[44]+1));
C_word *av2;
if(c >= 7) {
  av2=av;
} else {
  av2=C_alloc(7);
}
av2[0]=*((C_word*)lf[44]+1);
av2[1]=((C_word*)t0)[5];
av2[2]=t4;
av2[3]=lf[227];
av2[4]=((C_word*)t0)[3];
av2[5]=((C_word*)t0)[2];
av2[6]=((C_word*)t0)[4];
tp(7,av2);}}}

/* srfi-4#read-u8vector! in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4305(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(c<4) C_bad_min_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(9,c,3)))){
C_save_and_reclaim((void *)f_4305,c,av);}
a=C_alloc(9);
t4=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t5=C_rest_nullp(c,4);
t6=(C_truep(t5)?*((C_word*)lf[229]+1):C_get_rest_arg(c,4,av,4,t0));
t7=C_rest_nullp(c,4);
t8=C_rest_nullp(c,5);
t9=(C_truep(t8)?C_fix(0):C_get_rest_arg(c,5,av,4,t0));
t10=C_rest_nullp(c,5);
t11=C_i_check_port_2(t6,C_fix(1),C_SCHEME_TRUE,lf[230]);
t12=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4324,a[2]=t3,a[3]=t4,a[4]=t9,a[5]=t1,a[6]=t6,tmp=(C_word)a,a+=7,tmp);
/* srfi-4.scm:704: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2=av;
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t12;
av2[2]=t9;
av2[3]=lf[230];
tp(4,av2);}}

/* k4322 in srfi-4#read-u8vector! in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4324(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4324,c,av);}
a=C_alloc(7);
t2=C_i_check_structure_2(((C_word*)t0)[2],lf[43],lf[230]);
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4330,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
/* srfi-4.scm:706: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t3;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=lf[230];
tp(4,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_4330(2,av2);}}}

/* k4328 in k4322 in srfi-4#read-u8vector! in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_4330,c,av);}
t2=C_slot(((C_word*)t0)[2],C_fix(1));
t3=C_block_size(t2);
t4=(C_truep(((C_word*)((C_word*)t0)[3])[1])?C_fixnum_less_or_equal_p(C_fixnum_plus(((C_word*)t0)[4],((C_word*)((C_word*)t0)[3])[1]),t3):C_SCHEME_FALSE);
if(C_truep(t4)){
/* srfi-4.scm:711: chicken.io#read-string!/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[231]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[231]+1);
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t2;
av2[4]=((C_word*)t0)[6];
av2[5]=((C_word*)t0)[4];
tp(6,av2);}}
else{
t5=C_fixnum_difference(t3,((C_word*)t0)[4]);
t6=C_set_block_item(((C_word*)t0)[3],0,t5);
/* srfi-4.scm:711: chicken.io#read-string!/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[231]+1));
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=*((C_word*)lf[231]+1);
av2[1]=((C_word*)t0)[5];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t2;
av2[4]=((C_word*)t0)[6];
av2[5]=((C_word*)t0)[4];
tp(6,av2);}}}

/* srfi-4#read-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4385(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4385,c,av);}
a=C_alloc(5);
t2=C_rest_nullp(c,2);
t3=(C_truep(t2)?C_SCHEME_FALSE:C_get_rest_arg(c,2,av,2,t0));
t4=C_rest_nullp(c,2);
t5=C_rest_nullp(c,3);
t6=(C_truep(t5)?*((C_word*)lf[229]+1):C_get_rest_arg(c,3,av,2,t0));
t7=C_rest_nullp(c,3);
t8=C_i_check_port_2(t6,C_fix(1),C_SCHEME_TRUE,lf[233]);
t9=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4404,a[2]=t1,a[3]=t3,a[4]=t6,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t3)){
/* srfi-4.scm:715: ##sys#check-fixnum */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[37]+1));
C_word av2[4];
av2[0]=*((C_word*)lf[37]+1);
av2[1]=t9;
av2[2]=t3;
av2[3]=lf[233];
tp(4,av2);}}
else{
t10=t9;{
C_word *av2=av;
av2[0]=t10;
av2[1]=C_SCHEME_UNDEFINED;
f_4404(2,av2);}}}

/* k4402 in srfi-4#read-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in ... */
static void C_ccall f_4404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_4404,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4407,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* srfi-4.scm:716: chicken.io#read-string/port */
{C_proc tp=(C_proc)C_fast_retrieve_proc(*((C_word*)lf[234]+1));
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=*((C_word*)lf[234]+1);
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
tp(4,av2);}}

/* k4405 in k4402 in srfi-4#read-u8vector in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in ... */
static void C_ccall f_4407(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_4407,c,av);}
a=C_alloc(3);
if(C_truep(C_eofp(t1))){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=C_string_to_bytevector(t1);
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_a_i_record2(&a,2,lf[43],t1);
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4444 in k3850 in k3846 in k3842 in k3838 in k3834 in k3830 in k3826 in k3822 in k3818 in k3814 in k3810 in k3806 in k3802 in k3798 in k3794 in k3790 in k3786 in k3782 in k3778 in k3774 in k3770 in ... */
static void C_ccall f_4446(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4446,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a4447 in k1606 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4448(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,1)))){
C_save_and_reclaim((void *)f_4448,c,av);}
a=C_alloc(4);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_f64vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4450 in k1602 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4451(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(4,c,1)))){
C_save_and_reclaim((void *)f_4451,c,av);}
a=C_alloc(4);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_f32vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4453 in k1598 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_4454,c,av);}
a=C_alloc(7);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_s64vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4456 in k1594 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(7,c,1)))){
C_save_and_reclaim((void *)f_4457,c,av);}
a=C_alloc(7);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_u64vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4459 in k1590 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4460(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_4460,c,av);}
a=C_alloc(5);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_s32vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4462 in k1586 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(5,c,1)))){
C_save_and_reclaim((void *)f_4463,c,av);}
a=C_alloc(5);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_a_i_u32vector_ref(&a,2,t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4465 in k1582 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4466(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4466,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_s16vector_ref(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4468 in k1578 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4469(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4469,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_u16vector_ref(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4471 in k1574 in k1510 in k1507 in k1504 */
static void C_ccall f_4472(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4472,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_s8vector_ref(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a4474 in k1510 in k1507 in k1504 */
static void C_ccall f_4475(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(c!=4) C_bad_argc_2(c,4,t0);
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4475,c,av);}
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_i_u8vector_ref(t2,t3);
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_srfi_2d4_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("srfi-4"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_srfi_2d4_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(1782))){
C_save(t1);
C_rereclaim2(1782*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,277);
lf[0]=C_h_intern(&lf[0],6, C_text("srfi-4"));
lf[1]=C_h_intern(&lf[1],7, C_text("srfi-4#"));
lf[2]=C_h_intern(&lf[2],22, C_text("srfi-4#u8vector-length"));
lf[3]=C_h_intern(&lf[3],22, C_text("srfi-4#s8vector-length"));
lf[4]=C_h_intern(&lf[4],23, C_text("srfi-4#u16vector-length"));
lf[5]=C_h_intern(&lf[5],23, C_text("srfi-4#s16vector-length"));
lf[6]=C_h_intern(&lf[6],23, C_text("srfi-4#u32vector-length"));
lf[7]=C_h_intern(&lf[7],23, C_text("srfi-4#s32vector-length"));
lf[8]=C_h_intern(&lf[8],23, C_text("srfi-4#u64vector-length"));
lf[9]=C_h_intern(&lf[9],23, C_text("srfi-4#s64vector-length"));
lf[10]=C_h_intern(&lf[10],23, C_text("srfi-4#f32vector-length"));
lf[11]=C_h_intern(&lf[11],23, C_text("srfi-4#f64vector-length"));
lf[12]=C_h_intern(&lf[12],20, C_text("srfi-4#u8vector-set!"));
lf[13]=C_h_intern(&lf[13],20, C_text("srfi-4#s8vector-set!"));
lf[14]=C_h_intern(&lf[14],21, C_text("srfi-4#u16vector-set!"));
lf[15]=C_h_intern(&lf[15],21, C_text("srfi-4#s16vector-set!"));
lf[16]=C_h_intern(&lf[16],21, C_text("srfi-4#u32vector-set!"));
lf[17]=C_h_intern(&lf[17],21, C_text("srfi-4#s32vector-set!"));
lf[18]=C_h_intern(&lf[18],21, C_text("srfi-4#u64vector-set!"));
lf[19]=C_h_intern(&lf[19],21, C_text("srfi-4#s64vector-set!"));
lf[20]=C_h_intern(&lf[20],21, C_text("srfi-4#f32vector-set!"));
lf[21]=C_h_intern(&lf[21],21, C_text("srfi-4#f64vector-set!"));
lf[22]=C_h_intern(&lf[22],19, C_text("srfi-4#u8vector-ref"));
lf[23]=C_h_intern(&lf[23],19, C_text("srfi-4#s8vector-ref"));
lf[24]=C_h_intern(&lf[24],20, C_text("srfi-4#u16vector-ref"));
lf[25]=C_h_intern(&lf[25],20, C_text("srfi-4#s16vector-ref"));
lf[26]=C_h_intern(&lf[26],20, C_text("srfi-4#u32vector-ref"));
lf[27]=C_h_intern(&lf[27],20, C_text("srfi-4#s32vector-ref"));
lf[28]=C_h_intern(&lf[28],20, C_text("srfi-4#u64vector-ref"));
lf[29]=C_h_intern(&lf[29],20, C_text("srfi-4#s64vector-ref"));
lf[30]=C_h_intern(&lf[30],20, C_text("srfi-4#f32vector-ref"));
lf[31]=C_h_intern(&lf[31],20, C_text("srfi-4#f64vector-ref"));
lf[32]=C_h_intern(&lf[32],11, C_text("##sys#error"));
lf[33]=C_decode_literal(C_heaptop,C_text("\376B\000\000:not enough memory - cannot allocate external number vector"));
lf[34]=C_h_intern(&lf[34],21, C_text("##sys#allocate-vector"));
lf[35]=C_decode_literal(C_heaptop,C_text("\376B\000\000:overflow - cannot allocate the required number of elements"));
lf[36]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020size is negative"));
lf[37]=C_h_intern(&lf[37],18, C_text("##sys#check-fixnum"));
lf[38]=C_h_intern(&lf[38],28, C_text("srfi-4#release-number-vector"));
lf[39]=C_h_intern(&lf[39],21, C_text("release-number-vector"));
lf[40]=C_decode_literal(C_heaptop,C_text("\376B\000\000\047bad argument type - not a number vector"));
lf[41]=C_h_intern(&lf[41],21, C_text("srfi-4#number-vector\077"));
lf[42]=C_h_intern(&lf[42],20, C_text("srfi-4#make-u8vector"));
lf[43]=C_h_intern(&lf[43],8, C_text("u8vector"));
lf[44]=C_h_intern(&lf[44],16, C_text("##sys#error-hook"));
lf[45]=C_h_intern(&lf[45],13, C_text("make-u8vector"));
lf[46]=C_h_intern(&lf[46],26, C_text("##sys#check-exact-uinteger"));
lf[47]=C_h_intern(&lf[47],25, C_text("chicken.gc#set-finalizer!"));
lf[48]=C_h_intern(&lf[48],20, C_text("srfi-4#make-s8vector"));
lf[49]=C_h_intern(&lf[49],8, C_text("s8vector"));
lf[50]=C_h_intern(&lf[50],13, C_text("make-s8vector"));
lf[51]=C_h_intern(&lf[51],21, C_text("srfi-4#make-u16vector"));
lf[52]=C_h_intern(&lf[52],9, C_text("u16vector"));
lf[53]=C_h_intern(&lf[53],14, C_text("make-u16vector"));
lf[54]=C_h_intern(&lf[54],21, C_text("srfi-4#make-s16vector"));
lf[55]=C_h_intern(&lf[55],9, C_text("s16vector"));
lf[56]=C_h_intern(&lf[56],14, C_text("make-s16vector"));
lf[57]=C_h_intern(&lf[57],25, C_text("##sys#check-exact-integer"));
lf[58]=C_h_intern(&lf[58],21, C_text("srfi-4#make-u32vector"));
lf[59]=C_h_intern(&lf[59],9, C_text("u32vector"));
lf[60]=C_h_intern(&lf[60],14, C_text("make-u32vector"));
lf[61]=C_decode_literal(C_heaptop,C_text("\376\302\000\000\011100000000"));
lf[62]=C_h_intern(&lf[62],21, C_text("srfi-4#make-u64vector"));
lf[63]=C_h_intern(&lf[63],9, C_text("u64vector"));
lf[64]=C_h_intern(&lf[64],14, C_text("make-u64vector"));
lf[65]=C_decode_literal(C_heaptop,C_text("\376\302\000\000\02110000000000000000"));
lf[66]=C_h_intern(&lf[66],21, C_text("srfi-4#make-s32vector"));
lf[67]=C_h_intern(&lf[67],9, C_text("s32vector"));
lf[68]=C_h_intern(&lf[68],14, C_text("make-s32vector"));
lf[69]=C_h_intern(&lf[69],21, C_text("srfi-4#make-s64vector"));
lf[70]=C_h_intern(&lf[70],9, C_text("s64vector"));
lf[71]=C_h_intern(&lf[71],14, C_text("make-s64vector"));
lf[72]=C_h_intern(&lf[72],21, C_text("srfi-4#make-f32vector"));
lf[73]=C_h_intern(&lf[73],9, C_text("f32vector"));
lf[74]=C_h_intern(&lf[74],14, C_text("make-f32vector"));
lf[75]=C_h_intern(&lf[75],21, C_text("srfi-4#make-f64vector"));
lf[76]=C_h_intern(&lf[76],9, C_text("f64vector"));
lf[77]=C_h_intern(&lf[77],14, C_text("make-f64vector"));
lf[78]=C_h_intern(&lf[78],21, C_text("srfi-4#list->u8vector"));
lf[79]=C_h_intern(&lf[79],29, C_text("##sys#error-not-a-proper-list"));
lf[80]=C_h_intern(&lf[80],21, C_text("srfi-4#list->s8vector"));
lf[81]=C_h_intern(&lf[81],22, C_text("srfi-4#list->u16vector"));
lf[82]=C_h_intern(&lf[82],22, C_text("srfi-4#list->s16vector"));
lf[83]=C_h_intern(&lf[83],22, C_text("srfi-4#list->u32vector"));
lf[84]=C_h_intern(&lf[84],22, C_text("srfi-4#list->s32vector"));
lf[85]=C_h_intern(&lf[85],22, C_text("srfi-4#list->u64vector"));
lf[86]=C_h_intern(&lf[86],22, C_text("srfi-4#list->s64vector"));
lf[87]=C_h_intern(&lf[87],22, C_text("srfi-4#list->f32vector"));
lf[88]=C_h_intern(&lf[88],22, C_text("srfi-4#list->f64vector"));
lf[89]=C_h_intern(&lf[89],15, C_text("srfi-4#u8vector"));
lf[90]=C_h_intern(&lf[90],15, C_text("srfi-4#s8vector"));
lf[91]=C_h_intern(&lf[91],16, C_text("srfi-4#u16vector"));
lf[92]=C_h_intern(&lf[92],16, C_text("srfi-4#s16vector"));
lf[93]=C_h_intern(&lf[93],16, C_text("srfi-4#u32vector"));
lf[94]=C_h_intern(&lf[94],16, C_text("srfi-4#s32vector"));
lf[95]=C_h_intern(&lf[95],16, C_text("srfi-4#u64vector"));
lf[96]=C_h_intern(&lf[96],16, C_text("srfi-4#s64vector"));
lf[97]=C_h_intern(&lf[97],16, C_text("srfi-4#f32vector"));
lf[98]=C_h_intern(&lf[98],16, C_text("srfi-4#f64vector"));
lf[99]=C_h_intern(&lf[99],21, C_text("srfi-4#u8vector->list"));
lf[100]=C_h_intern(&lf[100],14, C_text("u8vector->list"));
lf[101]=C_h_intern(&lf[101],21, C_text("srfi-4#s8vector->list"));
lf[102]=C_h_intern(&lf[102],14, C_text("s8vector->list"));
lf[103]=C_h_intern(&lf[103],22, C_text("srfi-4#u16vector->list"));
lf[104]=C_h_intern(&lf[104],15, C_text("u16vector->list"));
lf[105]=C_h_intern(&lf[105],22, C_text("srfi-4#s16vector->list"));
lf[106]=C_h_intern(&lf[106],15, C_text("s16vector->list"));
lf[107]=C_h_intern(&lf[107],22, C_text("srfi-4#u32vector->list"));
lf[108]=C_h_intern(&lf[108],15, C_text("u32vector->list"));
lf[109]=C_h_intern(&lf[109],22, C_text("srfi-4#s32vector->list"));
lf[110]=C_h_intern(&lf[110],15, C_text("s32vector->list"));
lf[111]=C_h_intern(&lf[111],22, C_text("srfi-4#u64vector->list"));
lf[112]=C_h_intern(&lf[112],15, C_text("u64vector->list"));
lf[113]=C_h_intern(&lf[113],22, C_text("srfi-4#s64vector->list"));
lf[114]=C_h_intern(&lf[114],15, C_text("s64vector->list"));
lf[115]=C_h_intern(&lf[115],22, C_text("srfi-4#f32vector->list"));
lf[116]=C_h_intern(&lf[116],15, C_text("f32vector->list"));
lf[117]=C_h_intern(&lf[117],22, C_text("srfi-4#f64vector->list"));
lf[118]=C_h_intern(&lf[118],15, C_text("f64vector->list"));
lf[119]=C_h_intern(&lf[119],16, C_text("srfi-4#u8vector\077"));
lf[120]=C_h_intern(&lf[120],16, C_text("srfi-4#s8vector\077"));
lf[121]=C_h_intern(&lf[121],17, C_text("srfi-4#u16vector\077"));
lf[122]=C_h_intern(&lf[122],17, C_text("srfi-4#s16vector\077"));
lf[123]=C_h_intern(&lf[123],17, C_text("srfi-4#u32vector\077"));
lf[124]=C_h_intern(&lf[124],17, C_text("srfi-4#s32vector\077"));
lf[125]=C_h_intern(&lf[125],17, C_text("srfi-4#u64vector\077"));
lf[126]=C_h_intern(&lf[126],17, C_text("srfi-4#s64vector\077"));
lf[127]=C_h_intern(&lf[127],17, C_text("srfi-4#f32vector\077"));
lf[128]=C_h_intern(&lf[128],17, C_text("srfi-4#f64vector\077"));
lf[129]=C_h_intern(&lf[129],20, C_text("##sys#srfi-4-vector\077"));
lf[131]=C_h_intern(&lf[131],15, C_text("##sys#make-blob"));
lf[133]=C_decode_literal(C_heaptop,C_text("\376B\000\000+blob does not have correct size for packing"));
lf[135]=C_decode_literal(C_heaptop,C_text("\376B\000\000+blob does not have correct size for packing"));
lf[136]=C_h_intern(&lf[136],21, C_text("u8vector->blob/shared"));
lf[137]=C_h_intern(&lf[137],28, C_text("srfi-4#u8vector->blob/shared"));
lf[138]=C_h_intern(&lf[138],21, C_text("s8vector->blob/shared"));
lf[139]=C_h_intern(&lf[139],28, C_text("srfi-4#s8vector->blob/shared"));
lf[140]=C_h_intern(&lf[140],22, C_text("u16vector->blob/shared"));
lf[141]=C_h_intern(&lf[141],29, C_text("srfi-4#u16vector->blob/shared"));
lf[142]=C_h_intern(&lf[142],22, C_text("s16vector->blob/shared"));
lf[143]=C_h_intern(&lf[143],29, C_text("srfi-4#s16vector->blob/shared"));
lf[144]=C_h_intern(&lf[144],22, C_text("u32vector->blob/shared"));
lf[145]=C_h_intern(&lf[145],29, C_text("srfi-4#u32vector->blob/shared"));
lf[146]=C_h_intern(&lf[146],22, C_text("s32vector->blob/shared"));
lf[147]=C_h_intern(&lf[147],29, C_text("srfi-4#s32vector->blob/shared"));
lf[148]=C_h_intern(&lf[148],22, C_text("u64vector->blob/shared"));
lf[149]=C_h_intern(&lf[149],29, C_text("srfi-4#u64vector->blob/shared"));
lf[150]=C_h_intern(&lf[150],22, C_text("s64vector->blob/shared"));
lf[151]=C_h_intern(&lf[151],29, C_text("srfi-4#s64vector->blob/shared"));
lf[152]=C_h_intern(&lf[152],22, C_text("f32vector->blob/shared"));
lf[153]=C_h_intern(&lf[153],29, C_text("srfi-4#f32vector->blob/shared"));
lf[154]=C_h_intern(&lf[154],22, C_text("f64vector->blob/shared"));
lf[155]=C_h_intern(&lf[155],29, C_text("srfi-4#f64vector->blob/shared"));
lf[156]=C_h_intern(&lf[156],21, C_text("srfi-4#u8vector->blob"));
lf[157]=C_h_intern(&lf[157],21, C_text("srfi-4#s8vector->blob"));
lf[158]=C_h_intern(&lf[158],22, C_text("srfi-4#u16vector->blob"));
lf[159]=C_h_intern(&lf[159],22, C_text("srfi-4#s16vector->blob"));
lf[160]=C_h_intern(&lf[160],22, C_text("srfi-4#u32vector->blob"));
lf[161]=C_h_intern(&lf[161],22, C_text("srfi-4#s32vector->blob"));
lf[162]=C_h_intern(&lf[162],22, C_text("srfi-4#u64vector->blob"));
lf[163]=C_h_intern(&lf[163],22, C_text("srfi-4#s64vector->blob"));
lf[164]=C_h_intern(&lf[164],22, C_text("srfi-4#f32vector->blob"));
lf[165]=C_h_intern(&lf[165],22, C_text("srfi-4#f64vector->blob"));
lf[166]=C_h_intern(&lf[166],28, C_text("srfi-4#blob->u8vector/shared"));
lf[167]=C_h_intern(&lf[167],28, C_text("srfi-4#blob->s8vector/shared"));
lf[168]=C_h_intern(&lf[168],29, C_text("srfi-4#blob->u16vector/shared"));
lf[169]=C_h_intern(&lf[169],29, C_text("srfi-4#blob->s16vector/shared"));
lf[170]=C_h_intern(&lf[170],29, C_text("srfi-4#blob->u32vector/shared"));
lf[171]=C_h_intern(&lf[171],29, C_text("srfi-4#blob->s32vector/shared"));
lf[172]=C_h_intern(&lf[172],29, C_text("srfi-4#blob->u64vector/shared"));
lf[173]=C_h_intern(&lf[173],29, C_text("srfi-4#blob->s64vector/shared"));
lf[174]=C_h_intern(&lf[174],29, C_text("srfi-4#blob->f32vector/shared"));
lf[175]=C_h_intern(&lf[175],29, C_text("srfi-4#blob->f64vector/shared"));
lf[176]=C_h_intern(&lf[176],21, C_text("srfi-4#blob->u8vector"));
lf[177]=C_h_intern(&lf[177],21, C_text("srfi-4#blob->s8vector"));
lf[178]=C_h_intern(&lf[178],22, C_text("srfi-4#blob->u16vector"));
lf[179]=C_h_intern(&lf[179],22, C_text("srfi-4#blob->s16vector"));
lf[180]=C_h_intern(&lf[180],22, C_text("srfi-4#blob->u32vector"));
lf[181]=C_h_intern(&lf[181],22, C_text("srfi-4#blob->s32vector"));
lf[182]=C_h_intern(&lf[182],22, C_text("srfi-4#blob->u64vector"));
lf[183]=C_h_intern(&lf[183],22, C_text("srfi-4#blob->s64vector"));
lf[184]=C_h_intern(&lf[184],22, C_text("srfi-4#blob->f32vector"));
lf[185]=C_h_intern(&lf[185],22, C_text("srfi-4#blob->f64vector"));
lf[186]=C_h_intern(&lf[186],20, C_text("##sys#user-read-hook"));
lf[187]=C_h_intern(&lf[187],2, C_text("u8"));
lf[188]=C_h_intern(&lf[188],2, C_text("s8"));
lf[189]=C_h_intern(&lf[189],3, C_text("u16"));
lf[190]=C_h_intern(&lf[190],3, C_text("s16"));
lf[191]=C_h_intern(&lf[191],3, C_text("u32"));
lf[192]=C_h_intern(&lf[192],3, C_text("s32"));
lf[193]=C_h_intern(&lf[193],3, C_text("u64"));
lf[194]=C_h_intern(&lf[194],3, C_text("s64"));
lf[195]=C_h_intern(&lf[195],3, C_text("f32"));
lf[196]=C_h_intern(&lf[196],3, C_text("f64"));
lf[197]=C_h_intern(&lf[197],1, C_text("f"));
lf[198]=C_h_intern(&lf[198],1, C_text("F"));
lf[199]=C_h_intern(&lf[199],11, C_text("scheme#read"));
lf[200]=C_h_intern(&lf[200],16, C_text("##sys#read-error"));
lf[201]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031illegal bytevector syntax"));
lf[202]=C_h_intern(&lf[202],21, C_text("##sys#user-print-hook"));
lf[203]=C_h_intern(&lf[203],11, C_text("##sys#print"));
lf[205]=C_h_intern(&lf[205],18, C_text("srfi-4#subu8vector"));
lf[206]=C_h_intern(&lf[206],11, C_text("subu8vector"));
lf[207]=C_h_intern(&lf[207],19, C_text("srfi-4#subu16vector"));
lf[208]=C_h_intern(&lf[208],12, C_text("subu16vector"));
lf[209]=C_h_intern(&lf[209],19, C_text("srfi-4#subu32vector"));
lf[210]=C_h_intern(&lf[210],12, C_text("subu32vector"));
lf[211]=C_h_intern(&lf[211],19, C_text("srfi-4#subu64vector"));
lf[212]=C_h_intern(&lf[212],12, C_text("subu64vector"));
lf[213]=C_h_intern(&lf[213],18, C_text("srfi-4#subs8vector"));
lf[214]=C_h_intern(&lf[214],11, C_text("subs8vector"));
lf[215]=C_h_intern(&lf[215],19, C_text("srfi-4#subs16vector"));
lf[216]=C_h_intern(&lf[216],12, C_text("subs16vector"));
lf[217]=C_h_intern(&lf[217],19, C_text("srfi-4#subs32vector"));
lf[218]=C_h_intern(&lf[218],12, C_text("subs32vector"));
lf[219]=C_h_intern(&lf[219],19, C_text("srfi-4#subs64vector"));
lf[220]=C_h_intern(&lf[220],12, C_text("subs64vector"));
lf[221]=C_h_intern(&lf[221],19, C_text("srfi-4#subf32vector"));
lf[222]=C_h_intern(&lf[222],12, C_text("subf32vector"));
lf[223]=C_h_intern(&lf[223],19, C_text("srfi-4#subf64vector"));
lf[224]=C_h_intern(&lf[224],12, C_text("subf64vector"));
lf[225]=C_h_intern(&lf[225],21, C_text("srfi-4#write-u8vector"));
lf[226]=C_h_intern(&lf[226],21, C_text("##sys#standard-output"));
lf[227]=C_h_intern(&lf[227],14, C_text("write-u8vector"));
lf[228]=C_h_intern(&lf[228],21, C_text("srfi-4#read-u8vector!"));
lf[229]=C_h_intern(&lf[229],20, C_text("##sys#standard-input"));
lf[230]=C_h_intern(&lf[230],14, C_text("read-u8vector!"));
lf[231]=C_h_intern(&lf[231],28, C_text("chicken.io#read-string!/port"));
lf[232]=C_h_intern(&lf[232],20, C_text("srfi-4#read-u8vector"));
lf[233]=C_h_intern(&lf[233],13, C_text("read-u8vector"));
lf[234]=C_h_intern(&lf[234],27, C_text("chicken.io#read-string/port"));
lf[235]=C_h_intern(&lf[235],34, C_text("chicken.platform#register-feature!"));
lf[236]=C_h_intern(&lf[236],15, C_text("blob->f64vector"));
lf[237]=C_h_intern(&lf[237],15, C_text("blob->f32vector"));
lf[238]=C_h_intern(&lf[238],15, C_text("blob->s64vector"));
lf[239]=C_h_intern(&lf[239],15, C_text("blob->u64vector"));
lf[240]=C_h_intern(&lf[240],15, C_text("blob->s32vector"));
lf[241]=C_h_intern(&lf[241],15, C_text("blob->u32vector"));
lf[242]=C_h_intern(&lf[242],15, C_text("blob->s16vector"));
lf[243]=C_h_intern(&lf[243],15, C_text("blob->u16vector"));
lf[244]=C_h_intern(&lf[244],14, C_text("blob->s8vector"));
lf[245]=C_h_intern(&lf[245],14, C_text("blob->u8vector"));
lf[246]=C_h_intern(&lf[246],22, C_text("blob->f64vector/shared"));
lf[247]=C_h_intern(&lf[247],22, C_text("blob->f32vector/shared"));
lf[248]=C_h_intern(&lf[248],22, C_text("blob->s64vector/shared"));
lf[249]=C_h_intern(&lf[249],22, C_text("blob->u64vector/shared"));
lf[250]=C_h_intern(&lf[250],22, C_text("blob->s32vector/shared"));
lf[251]=C_h_intern(&lf[251],22, C_text("blob->u32vector/shared"));
lf[252]=C_h_intern(&lf[252],22, C_text("blob->s16vector/shared"));
lf[253]=C_h_intern(&lf[253],22, C_text("blob->u16vector/shared"));
lf[254]=C_h_intern(&lf[254],21, C_text("blob->s8vector/shared"));
lf[255]=C_h_intern(&lf[255],21, C_text("blob->u8vector/shared"));
lf[256]=C_h_intern(&lf[256],15, C_text("f64vector->blob"));
lf[257]=C_h_intern(&lf[257],15, C_text("f32vector->blob"));
lf[258]=C_h_intern(&lf[258],15, C_text("s64vector->blob"));
lf[259]=C_h_intern(&lf[259],15, C_text("u64vector->blob"));
lf[260]=C_h_intern(&lf[260],15, C_text("s32vector->blob"));
lf[261]=C_h_intern(&lf[261],15, C_text("u32vector->blob"));
lf[262]=C_h_intern(&lf[262],15, C_text("s16vector->blob"));
lf[263]=C_h_intern(&lf[263],15, C_text("u16vector->blob"));
lf[264]=C_h_intern(&lf[264],14, C_text("s8vector->blob"));
lf[265]=C_h_intern(&lf[265],14, C_text("u8vector->blob"));
lf[266]=C_h_intern(&lf[266],31, C_text("chicken.base#getter-with-setter"));
lf[267]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#f64vector-ref v i)"));
lf[268]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#f32vector-ref v i)"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#s64vector-ref v i)"));
lf[270]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#u64vector-ref v i)"));
lf[271]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#s32vector-ref v i)"));
lf[272]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#u32vector-ref v i)"));
lf[273]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#s16vector-ref v i)"));
lf[274]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042(chicken.srfi-4#u16vector-ref v i)"));
lf[275]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.srfi-4#s8vector-ref v i)"));
lf[276]=C_decode_literal(C_heaptop,C_text("\376B\000\000!(chicken.srfi-4#u8vector-ref v i)"));
C_register_lf2(lf,277,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_1506,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[289] = {
{C_text("f5218:srfi_2d4_2escm"),(void*)f5218},
{C_text("f5225:srfi_2d4_2escm"),(void*)f5225},
{C_text("f5232:srfi_2d4_2escm"),(void*)f5232},
{C_text("f5239:srfi_2d4_2escm"),(void*)f5239},
{C_text("f5246:srfi_2d4_2escm"),(void*)f5246},
{C_text("f5253:srfi_2d4_2escm"),(void*)f5253},
{C_text("f5260:srfi_2d4_2escm"),(void*)f5260},
{C_text("f5267:srfi_2d4_2escm"),(void*)f5267},
{C_text("f5274:srfi_2d4_2escm"),(void*)f5274},
{C_text("f5281:srfi_2d4_2escm"),(void*)f5281},
{C_text("f_1506:srfi_2d4_2escm"),(void*)f_1506},
{C_text("f_1509:srfi_2d4_2escm"),(void*)f_1509},
{C_text("f_1512:srfi_2d4_2escm"),(void*)f_1512},
{C_text("f_1514:srfi_2d4_2escm"),(void*)f_1514},
{C_text("f_1517:srfi_2d4_2escm"),(void*)f_1517},
{C_text("f_1520:srfi_2d4_2escm"),(void*)f_1520},
{C_text("f_1523:srfi_2d4_2escm"),(void*)f_1523},
{C_text("f_1526:srfi_2d4_2escm"),(void*)f_1526},
{C_text("f_1529:srfi_2d4_2escm"),(void*)f_1529},
{C_text("f_1532:srfi_2d4_2escm"),(void*)f_1532},
{C_text("f_1535:srfi_2d4_2escm"),(void*)f_1535},
{C_text("f_1538:srfi_2d4_2escm"),(void*)f_1538},
{C_text("f_1541:srfi_2d4_2escm"),(void*)f_1541},
{C_text("f_1544:srfi_2d4_2escm"),(void*)f_1544},
{C_text("f_1547:srfi_2d4_2escm"),(void*)f_1547},
{C_text("f_1550:srfi_2d4_2escm"),(void*)f_1550},
{C_text("f_1553:srfi_2d4_2escm"),(void*)f_1553},
{C_text("f_1556:srfi_2d4_2escm"),(void*)f_1556},
{C_text("f_1559:srfi_2d4_2escm"),(void*)f_1559},
{C_text("f_1562:srfi_2d4_2escm"),(void*)f_1562},
{C_text("f_1565:srfi_2d4_2escm"),(void*)f_1565},
{C_text("f_1568:srfi_2d4_2escm"),(void*)f_1568},
{C_text("f_1571:srfi_2d4_2escm"),(void*)f_1571},
{C_text("f_1576:srfi_2d4_2escm"),(void*)f_1576},
{C_text("f_1580:srfi_2d4_2escm"),(void*)f_1580},
{C_text("f_1584:srfi_2d4_2escm"),(void*)f_1584},
{C_text("f_1588:srfi_2d4_2escm"),(void*)f_1588},
{C_text("f_1592:srfi_2d4_2escm"),(void*)f_1592},
{C_text("f_1596:srfi_2d4_2escm"),(void*)f_1596},
{C_text("f_1600:srfi_2d4_2escm"),(void*)f_1600},
{C_text("f_1604:srfi_2d4_2escm"),(void*)f_1604},
{C_text("f_1608:srfi_2d4_2escm"),(void*)f_1608},
{C_text("f_1612:srfi_2d4_2escm"),(void*)f_1612},
{C_text("f_1620:srfi_2d4_2escm"),(void*)f_1620},
{C_text("f_1622:srfi_2d4_2escm"),(void*)f_1622},
{C_text("f_1626:srfi_2d4_2escm"),(void*)f_1626},
{C_text("f_1629:srfi_2d4_2escm"),(void*)f_1629},
{C_text("f_1635:srfi_2d4_2escm"),(void*)f_1635},
{C_text("f_1650:srfi_2d4_2escm"),(void*)f_1650},
{C_text("f_1661:srfi_2d4_2escm"),(void*)f_1661},
{C_text("f_1668:srfi_2d4_2escm"),(void*)f_1668},
{C_text("f_1676:srfi_2d4_2escm"),(void*)f_1676},
{C_text("f_1701:srfi_2d4_2escm"),(void*)f_1701},
{C_text("f_1712:srfi_2d4_2escm"),(void*)f_1712},
{C_text("f_1731:srfi_2d4_2escm"),(void*)f_1731},
{C_text("f_1736:srfi_2d4_2escm"),(void*)f_1736},
{C_text("f_1754:srfi_2d4_2escm"),(void*)f_1754},
{C_text("f_1792:srfi_2d4_2escm"),(void*)f_1792},
{C_text("f_1817:srfi_2d4_2escm"),(void*)f_1817},
{C_text("f_1828:srfi_2d4_2escm"),(void*)f_1828},
{C_text("f_1847:srfi_2d4_2escm"),(void*)f_1847},
{C_text("f_1852:srfi_2d4_2escm"),(void*)f_1852},
{C_text("f_1870:srfi_2d4_2escm"),(void*)f_1870},
{C_text("f_1908:srfi_2d4_2escm"),(void*)f_1908},
{C_text("f_1933:srfi_2d4_2escm"),(void*)f_1933},
{C_text("f_1944:srfi_2d4_2escm"),(void*)f_1944},
{C_text("f_1963:srfi_2d4_2escm"),(void*)f_1963},
{C_text("f_1968:srfi_2d4_2escm"),(void*)f_1968},
{C_text("f_1986:srfi_2d4_2escm"),(void*)f_1986},
{C_text("f_2024:srfi_2d4_2escm"),(void*)f_2024},
{C_text("f_2049:srfi_2d4_2escm"),(void*)f_2049},
{C_text("f_2060:srfi_2d4_2escm"),(void*)f_2060},
{C_text("f_2089:srfi_2d4_2escm"),(void*)f_2089},
{C_text("f_2094:srfi_2d4_2escm"),(void*)f_2094},
{C_text("f_2112:srfi_2d4_2escm"),(void*)f_2112},
{C_text("f_2150:srfi_2d4_2escm"),(void*)f_2150},
{C_text("f_2175:srfi_2d4_2escm"),(void*)f_2175},
{C_text("f_2186:srfi_2d4_2escm"),(void*)f_2186},
{C_text("f_2205:srfi_2d4_2escm"),(void*)f_2205},
{C_text("f_2210:srfi_2d4_2escm"),(void*)f_2210},
{C_text("f_2228:srfi_2d4_2escm"),(void*)f_2228},
{C_text("f_2266:srfi_2d4_2escm"),(void*)f_2266},
{C_text("f_2291:srfi_2d4_2escm"),(void*)f_2291},
{C_text("f_2302:srfi_2d4_2escm"),(void*)f_2302},
{C_text("f_2321:srfi_2d4_2escm"),(void*)f_2321},
{C_text("f_2326:srfi_2d4_2escm"),(void*)f_2326},
{C_text("f_2344:srfi_2d4_2escm"),(void*)f_2344},
{C_text("f_2382:srfi_2d4_2escm"),(void*)f_2382},
{C_text("f_2407:srfi_2d4_2escm"),(void*)f_2407},
{C_text("f_2418:srfi_2d4_2escm"),(void*)f_2418},
{C_text("f_2447:srfi_2d4_2escm"),(void*)f_2447},
{C_text("f_2452:srfi_2d4_2escm"),(void*)f_2452},
{C_text("f_2470:srfi_2d4_2escm"),(void*)f_2470},
{C_text("f_2508:srfi_2d4_2escm"),(void*)f_2508},
{C_text("f_2533:srfi_2d4_2escm"),(void*)f_2533},
{C_text("f_2544:srfi_2d4_2escm"),(void*)f_2544},
{C_text("f_2573:srfi_2d4_2escm"),(void*)f_2573},
{C_text("f_2578:srfi_2d4_2escm"),(void*)f_2578},
{C_text("f_2596:srfi_2d4_2escm"),(void*)f_2596},
{C_text("f_2634:srfi_2d4_2escm"),(void*)f_2634},
{C_text("f_2659:srfi_2d4_2escm"),(void*)f_2659},
{C_text("f_2680:srfi_2d4_2escm"),(void*)f_2680},
{C_text("f_2683:srfi_2d4_2escm"),(void*)f_2683},
{C_text("f_2688:srfi_2d4_2escm"),(void*)f_2688},
{C_text("f_2707:srfi_2d4_2escm"),(void*)f_2707},
{C_text("f_2745:srfi_2d4_2escm"),(void*)f_2745},
{C_text("f_2770:srfi_2d4_2escm"),(void*)f_2770},
{C_text("f_2791:srfi_2d4_2escm"),(void*)f_2791},
{C_text("f_2794:srfi_2d4_2escm"),(void*)f_2794},
{C_text("f_2799:srfi_2d4_2escm"),(void*)f_2799},
{C_text("f_2818:srfi_2d4_2escm"),(void*)f_2818},
{C_text("f_2856:srfi_2d4_2escm"),(void*)f_2856},
{C_text("f_2863:srfi_2d4_2escm"),(void*)f_2863},
{C_text("f_2868:srfi_2d4_2escm"),(void*)f_2868},
{C_text("f_2875:srfi_2d4_2escm"),(void*)f_2875},
{C_text("f_2892:srfi_2d4_2escm"),(void*)f_2892},
{C_text("f_2899:srfi_2d4_2escm"),(void*)f_2899},
{C_text("f_2904:srfi_2d4_2escm"),(void*)f_2904},
{C_text("f_2911:srfi_2d4_2escm"),(void*)f_2911},
{C_text("f_2928:srfi_2d4_2escm"),(void*)f_2928},
{C_text("f_2935:srfi_2d4_2escm"),(void*)f_2935},
{C_text("f_2940:srfi_2d4_2escm"),(void*)f_2940},
{C_text("f_2947:srfi_2d4_2escm"),(void*)f_2947},
{C_text("f_2964:srfi_2d4_2escm"),(void*)f_2964},
{C_text("f_2971:srfi_2d4_2escm"),(void*)f_2971},
{C_text("f_2976:srfi_2d4_2escm"),(void*)f_2976},
{C_text("f_2983:srfi_2d4_2escm"),(void*)f_2983},
{C_text("f_3000:srfi_2d4_2escm"),(void*)f_3000},
{C_text("f_3007:srfi_2d4_2escm"),(void*)f_3007},
{C_text("f_3012:srfi_2d4_2escm"),(void*)f_3012},
{C_text("f_3019:srfi_2d4_2escm"),(void*)f_3019},
{C_text("f_3036:srfi_2d4_2escm"),(void*)f_3036},
{C_text("f_3043:srfi_2d4_2escm"),(void*)f_3043},
{C_text("f_3048:srfi_2d4_2escm"),(void*)f_3048},
{C_text("f_3055:srfi_2d4_2escm"),(void*)f_3055},
{C_text("f_3072:srfi_2d4_2escm"),(void*)f_3072},
{C_text("f_3079:srfi_2d4_2escm"),(void*)f_3079},
{C_text("f_3084:srfi_2d4_2escm"),(void*)f_3084},
{C_text("f_3091:srfi_2d4_2escm"),(void*)f_3091},
{C_text("f_3108:srfi_2d4_2escm"),(void*)f_3108},
{C_text("f_3115:srfi_2d4_2escm"),(void*)f_3115},
{C_text("f_3120:srfi_2d4_2escm"),(void*)f_3120},
{C_text("f_3127:srfi_2d4_2escm"),(void*)f_3127},
{C_text("f_3144:srfi_2d4_2escm"),(void*)f_3144},
{C_text("f_3151:srfi_2d4_2escm"),(void*)f_3151},
{C_text("f_3156:srfi_2d4_2escm"),(void*)f_3156},
{C_text("f_3163:srfi_2d4_2escm"),(void*)f_3163},
{C_text("f_3180:srfi_2d4_2escm"),(void*)f_3180},
{C_text("f_3187:srfi_2d4_2escm"),(void*)f_3187},
{C_text("f_3192:srfi_2d4_2escm"),(void*)f_3192},
{C_text("f_3199:srfi_2d4_2escm"),(void*)f_3199},
{C_text("f_3216:srfi_2d4_2escm"),(void*)f_3216},
{C_text("f_3222:srfi_2d4_2escm"),(void*)f_3222},
{C_text("f_3228:srfi_2d4_2escm"),(void*)f_3228},
{C_text("f_3234:srfi_2d4_2escm"),(void*)f_3234},
{C_text("f_3240:srfi_2d4_2escm"),(void*)f_3240},
{C_text("f_3246:srfi_2d4_2escm"),(void*)f_3246},
{C_text("f_3252:srfi_2d4_2escm"),(void*)f_3252},
{C_text("f_3258:srfi_2d4_2escm"),(void*)f_3258},
{C_text("f_3264:srfi_2d4_2escm"),(void*)f_3264},
{C_text("f_3270:srfi_2d4_2escm"),(void*)f_3270},
{C_text("f_3276:srfi_2d4_2escm"),(void*)f_3276},
{C_text("f_3285:srfi_2d4_2escm"),(void*)f_3285},
{C_text("f_3300:srfi_2d4_2escm"),(void*)f_3300},
{C_text("f_3306:srfi_2d4_2escm"),(void*)f_3306},
{C_text("f_3315:srfi_2d4_2escm"),(void*)f_3315},
{C_text("f_3330:srfi_2d4_2escm"),(void*)f_3330},
{C_text("f_3336:srfi_2d4_2escm"),(void*)f_3336},
{C_text("f_3345:srfi_2d4_2escm"),(void*)f_3345},
{C_text("f_3360:srfi_2d4_2escm"),(void*)f_3360},
{C_text("f_3366:srfi_2d4_2escm"),(void*)f_3366},
{C_text("f_3375:srfi_2d4_2escm"),(void*)f_3375},
{C_text("f_3390:srfi_2d4_2escm"),(void*)f_3390},
{C_text("f_3396:srfi_2d4_2escm"),(void*)f_3396},
{C_text("f_3405:srfi_2d4_2escm"),(void*)f_3405},
{C_text("f_3419:srfi_2d4_2escm"),(void*)f_3419},
{C_text("f_3425:srfi_2d4_2escm"),(void*)f_3425},
{C_text("f_3434:srfi_2d4_2escm"),(void*)f_3434},
{C_text("f_3448:srfi_2d4_2escm"),(void*)f_3448},
{C_text("f_3454:srfi_2d4_2escm"),(void*)f_3454},
{C_text("f_3463:srfi_2d4_2escm"),(void*)f_3463},
{C_text("f_3477:srfi_2d4_2escm"),(void*)f_3477},
{C_text("f_3483:srfi_2d4_2escm"),(void*)f_3483},
{C_text("f_3492:srfi_2d4_2escm"),(void*)f_3492},
{C_text("f_3506:srfi_2d4_2escm"),(void*)f_3506},
{C_text("f_3512:srfi_2d4_2escm"),(void*)f_3512},
{C_text("f_3521:srfi_2d4_2escm"),(void*)f_3521},
{C_text("f_3535:srfi_2d4_2escm"),(void*)f_3535},
{C_text("f_3541:srfi_2d4_2escm"),(void*)f_3541},
{C_text("f_3550:srfi_2d4_2escm"),(void*)f_3550},
{C_text("f_3564:srfi_2d4_2escm"),(void*)f_3564},
{C_text("f_3570:srfi_2d4_2escm"),(void*)f_3570},
{C_text("f_3573:srfi_2d4_2escm"),(void*)f_3573},
{C_text("f_3576:srfi_2d4_2escm"),(void*)f_3576},
{C_text("f_3579:srfi_2d4_2escm"),(void*)f_3579},
{C_text("f_3582:srfi_2d4_2escm"),(void*)f_3582},
{C_text("f_3585:srfi_2d4_2escm"),(void*)f_3585},
{C_text("f_3588:srfi_2d4_2escm"),(void*)f_3588},
{C_text("f_3591:srfi_2d4_2escm"),(void*)f_3591},
{C_text("f_3594:srfi_2d4_2escm"),(void*)f_3594},
{C_text("f_3597:srfi_2d4_2escm"),(void*)f_3597},
{C_text("f_3612:srfi_2d4_2escm"),(void*)f_3612},
{C_text("f_3614:srfi_2d4_2escm"),(void*)f_3614},
{C_text("f_3624:srfi_2d4_2escm"),(void*)f_3624},
{C_text("f_3630:srfi_2d4_2escm"),(void*)f_3630},
{C_text("f_3632:srfi_2d4_2escm"),(void*)f_3632},
{C_text("f_3660:srfi_2d4_2escm"),(void*)f_3660},
{C_text("f_3662:srfi_2d4_2escm"),(void*)f_3662},
{C_text("f_3672:srfi_2d4_2escm"),(void*)f_3672},
{C_text("f_3736:srfi_2d4_2escm"),(void*)f_3736},
{C_text("f_3740:srfi_2d4_2escm"),(void*)f_3740},
{C_text("f_3744:srfi_2d4_2escm"),(void*)f_3744},
{C_text("f_3748:srfi_2d4_2escm"),(void*)f_3748},
{C_text("f_3752:srfi_2d4_2escm"),(void*)f_3752},
{C_text("f_3756:srfi_2d4_2escm"),(void*)f_3756},
{C_text("f_3760:srfi_2d4_2escm"),(void*)f_3760},
{C_text("f_3764:srfi_2d4_2escm"),(void*)f_3764},
{C_text("f_3768:srfi_2d4_2escm"),(void*)f_3768},
{C_text("f_3772:srfi_2d4_2escm"),(void*)f_3772},
{C_text("f_3776:srfi_2d4_2escm"),(void*)f_3776},
{C_text("f_3780:srfi_2d4_2escm"),(void*)f_3780},
{C_text("f_3784:srfi_2d4_2escm"),(void*)f_3784},
{C_text("f_3788:srfi_2d4_2escm"),(void*)f_3788},
{C_text("f_3792:srfi_2d4_2escm"),(void*)f_3792},
{C_text("f_3796:srfi_2d4_2escm"),(void*)f_3796},
{C_text("f_3800:srfi_2d4_2escm"),(void*)f_3800},
{C_text("f_3804:srfi_2d4_2escm"),(void*)f_3804},
{C_text("f_3808:srfi_2d4_2escm"),(void*)f_3808},
{C_text("f_3812:srfi_2d4_2escm"),(void*)f_3812},
{C_text("f_3816:srfi_2d4_2escm"),(void*)f_3816},
{C_text("f_3820:srfi_2d4_2escm"),(void*)f_3820},
{C_text("f_3824:srfi_2d4_2escm"),(void*)f_3824},
{C_text("f_3828:srfi_2d4_2escm"),(void*)f_3828},
{C_text("f_3832:srfi_2d4_2escm"),(void*)f_3832},
{C_text("f_3836:srfi_2d4_2escm"),(void*)f_3836},
{C_text("f_3840:srfi_2d4_2escm"),(void*)f_3840},
{C_text("f_3844:srfi_2d4_2escm"),(void*)f_3844},
{C_text("f_3848:srfi_2d4_2escm"),(void*)f_3848},
{C_text("f_3852:srfi_2d4_2escm"),(void*)f_3852},
{C_text("f_3857:srfi_2d4_2escm"),(void*)f_3857},
{C_text("f_3866:srfi_2d4_2escm"),(void*)f_3866},
{C_text("f_3885:srfi_2d4_2escm"),(void*)f_3885},
{C_text("f_3896:srfi_2d4_2escm"),(void*)f_3896},
{C_text("f_3917:srfi_2d4_2escm"),(void*)f_3917},
{C_text("f_3929:srfi_2d4_2escm"),(void*)f_3929},
{C_text("f_3932:srfi_2d4_2escm"),(void*)f_3932},
{C_text("f_3942:srfi_2d4_2escm"),(void*)f_3942},
{C_text("f_3991:srfi_2d4_2escm"),(void*)f_3991},
{C_text("f_4006:srfi_2d4_2escm"),(void*)f_4006},
{C_text("f_4025:srfi_2d4_2escm"),(void*)f_4025},
{C_text("f_4030:srfi_2d4_2escm"),(void*)f_4030},
{C_text("f_4049:srfi_2d4_2escm"),(void*)f_4049},
{C_text("f_4055:srfi_2d4_2escm"),(void*)f_4055},
{C_text("f_4076:srfi_2d4_2escm"),(void*)f_4076},
{C_text("f_4082:srfi_2d4_2escm"),(void*)f_4082},
{C_text("f_4088:srfi_2d4_2escm"),(void*)f_4088},
{C_text("f_4094:srfi_2d4_2escm"),(void*)f_4094},
{C_text("f_4100:srfi_2d4_2escm"),(void*)f_4100},
{C_text("f_4106:srfi_2d4_2escm"),(void*)f_4106},
{C_text("f_4112:srfi_2d4_2escm"),(void*)f_4112},
{C_text("f_4118:srfi_2d4_2escm"),(void*)f_4118},
{C_text("f_4124:srfi_2d4_2escm"),(void*)f_4124},
{C_text("f_4130:srfi_2d4_2escm"),(void*)f_4130},
{C_text("f_4136:srfi_2d4_2escm"),(void*)f_4136},
{C_text("f_4166:srfi_2d4_2escm"),(void*)f_4166},
{C_text("f_4185:srfi_2d4_2escm"),(void*)f_4185},
{C_text("f_4188:srfi_2d4_2escm"),(void*)f_4188},
{C_text("f_4201:srfi_2d4_2escm"),(void*)f_4201},
{C_text("f_4211:srfi_2d4_2escm"),(void*)f_4211},
{C_text("f_4236:srfi_2d4_2escm"),(void*)f_4236},
{C_text("f_4305:srfi_2d4_2escm"),(void*)f_4305},
{C_text("f_4324:srfi_2d4_2escm"),(void*)f_4324},
{C_text("f_4330:srfi_2d4_2escm"),(void*)f_4330},
{C_text("f_4385:srfi_2d4_2escm"),(void*)f_4385},
{C_text("f_4404:srfi_2d4_2escm"),(void*)f_4404},
{C_text("f_4407:srfi_2d4_2escm"),(void*)f_4407},
{C_text("f_4446:srfi_2d4_2escm"),(void*)f_4446},
{C_text("f_4448:srfi_2d4_2escm"),(void*)f_4448},
{C_text("f_4451:srfi_2d4_2escm"),(void*)f_4451},
{C_text("f_4454:srfi_2d4_2escm"),(void*)f_4454},
{C_text("f_4457:srfi_2d4_2escm"),(void*)f_4457},
{C_text("f_4460:srfi_2d4_2escm"),(void*)f_4460},
{C_text("f_4463:srfi_2d4_2escm"),(void*)f_4463},
{C_text("f_4466:srfi_2d4_2escm"),(void*)f_4466},
{C_text("f_4469:srfi_2d4_2escm"),(void*)f_4469},
{C_text("f_4472:srfi_2d4_2escm"),(void*)f_4472},
{C_text("f_4475:srfi_2d4_2escm"),(void*)f_4475},
{C_text("toplevel:srfi_2d4_2escm"),(void*)C_srfi_2d4_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: srfi-4#d 
o|hiding unexported module binding: srfi-4#define-alias 
o|hiding unexported module binding: srfi-4#list->NNNvector 
o|hiding unexported module binding: srfi-4#NNNvector->list 
o|hiding unexported module binding: srfi-4#pack 
o|hiding unexported module binding: srfi-4#pack-copy 
o|hiding unexported module binding: srfi-4#unpack 
o|hiding unexported module binding: srfi-4#unpack-copy 
o|hiding unexported module binding: srfi-4#subnvector 
o|eliminated procedure checks: 32 
o|specializations:
o|  2 (##sys#check-input-port * * *)
o|  1 (##sys#check-output-port * * *)
o|  1 (scheme#assq * (list-of pair))
o|  1 (scheme#memq * list)
o|  3 (chicken.base#sub1 *)
o|  3 (scheme#- *)
o|  8 (chicken.bitwise#integer-length *)
(o e)|safe calls: 416 
(o e)|assignments to immediate values: 1 
o|dropping redundant toplevel assignment: srfi-4#release-number-vector 
o|dropping redundant toplevel assignment: srfi-4#make-u8vector 
o|dropping redundant toplevel assignment: srfi-4#make-s8vector 
o|dropping redundant toplevel assignment: srfi-4#make-u16vector 
o|dropping redundant toplevel assignment: srfi-4#make-s16vector 
o|dropping redundant toplevel assignment: srfi-4#make-u32vector 
o|dropping redundant toplevel assignment: srfi-4#make-u64vector 
o|dropping redundant toplevel assignment: srfi-4#make-s32vector 
o|dropping redundant toplevel assignment: srfi-4#make-s64vector 
o|dropping redundant toplevel assignment: srfi-4#make-f32vector 
o|dropping redundant toplevel assignment: srfi-4#make-f64vector 
o|safe globals: (srfi-4#f64vector-set! srfi-4#f32vector-set! srfi-4#s64vector-set! srfi-4#u64vector-set! srfi-4#s32vector-set! srfi-4#u32vector-set! srfi-4#s16vector-set! srfi-4#u16vector-set! srfi-4#s8vector-set! srfi-4#u8vector-set! srfi-4#f64vector-length srfi-4#f32vector-length srfi-4#s64vector-length srfi-4#u64vector-length srfi-4#s32vector-length srfi-4#u32vector-length srfi-4#s16vector-length srfi-4#u16vector-length srfi-4#s8vector-length srfi-4#u8vector-length) 
o|inlining procedure: k1636 
o|contracted procedure: "(srfi-4.scm:278) ext-alloc214" 
o|inlining procedure: k1636 
o|inlining procedure: k1663 
o|inlining procedure: "(srfi-4.scm:288) ext-free220" 
o|inlining procedure: k1663 
o|inlining procedure: k1702 
o|inlining procedure: k1702 
o|inlining procedure: k1738 
o|inlining procedure: k1738 
o|contracted procedure: "(srfi-4.scm:298) g261262" 
o|inlining procedure: k1713 
o|inlining procedure: k1713 
o|inlining procedure: k1818 
o|inlining procedure: k1818 
o|inlining procedure: k1854 
o|inlining procedure: k1854 
o|contracted procedure: "(srfi-4.scm:310) g295296" 
o|inlining procedure: k1829 
o|inlining procedure: k1829 
o|inlining procedure: k1934 
o|inlining procedure: k1934 
o|inlining procedure: k1970 
o|inlining procedure: k1970 
o|contracted procedure: "(srfi-4.scm:322) g329330" 
o|inlining procedure: k1945 
o|inlining procedure: k1945 
o|inlining procedure: k2050 
o|inlining procedure: k2050 
o|inlining procedure: k2096 
o|inlining procedure: k2096 
o|contracted procedure: "(srfi-4.scm:334) g363364" 
o|inlining procedure: k2061 
o|inlining procedure: k2061 
o|inlining procedure: k2176 
o|inlining procedure: k2176 
o|inlining procedure: k2212 
o|inlining procedure: k2212 
o|contracted procedure: "(srfi-4.scm:346) g397398" 
o|inlining procedure: k2187 
o|inlining procedure: k2187 
o|inlining procedure: k2292 
o|inlining procedure: k2292 
o|inlining procedure: k2328 
o|inlining procedure: k2328 
o|contracted procedure: "(srfi-4.scm:358) g431432" 
o|inlining procedure: k2303 
o|inlining procedure: k2303 
o|inlining procedure: k2408 
o|inlining procedure: k2408 
o|inlining procedure: k2454 
o|inlining procedure: k2454 
o|contracted procedure: "(srfi-4.scm:370) g465466" 
o|inlining procedure: k2419 
o|inlining procedure: k2419 
o|inlining procedure: k2534 
o|inlining procedure: k2534 
o|inlining procedure: k2580 
o|inlining procedure: k2580 
o|contracted procedure: "(srfi-4.scm:382) g499500" 
o|inlining procedure: k2545 
o|inlining procedure: k2545 
o|inlining procedure: k2660 
o|inlining procedure: k2660 
o|inlining procedure: k2690 
o|inlining procedure: k2690 
o|contracted procedure: "(srfi-4.scm:394) g533534" 
o|inlining procedure: k2668 
o|inlining procedure: k2668 
o|inlining procedure: k2771 
o|inlining procedure: k2771 
o|inlining procedure: k2801 
o|inlining procedure: k2801 
o|contracted procedure: "(srfi-4.scm:408) g569570" 
o|inlining procedure: k2779 
o|inlining procedure: k2779 
o|inlining procedure: k2870 
o|inlining procedure: k2870 
o|inlining procedure: k2906 
o|inlining procedure: k2906 
o|inlining procedure: k2942 
o|inlining procedure: k2942 
o|inlining procedure: k2978 
o|inlining procedure: k2978 
o|inlining procedure: k3014 
o|inlining procedure: k3014 
o|inlining procedure: k3050 
o|inlining procedure: k3050 
o|inlining procedure: k3086 
o|inlining procedure: k3086 
o|inlining procedure: k3122 
o|inlining procedure: k3122 
o|inlining procedure: k3158 
o|inlining procedure: k3158 
o|inlining procedure: k3194 
o|inlining procedure: k3194 
o|inlining procedure: k3287 
o|inlining procedure: k3287 
o|inlining procedure: k3317 
o|inlining procedure: k3317 
o|inlining procedure: k3347 
o|inlining procedure: k3347 
o|inlining procedure: k3377 
o|inlining procedure: k3377 
o|inlining procedure: k3407 
o|inlining procedure: k3407 
o|inlining procedure: k3436 
o|inlining procedure: k3436 
o|inlining procedure: k3465 
o|inlining procedure: k3465 
o|inlining procedure: k3494 
o|inlining procedure: k3494 
o|inlining procedure: k3523 
o|inlining procedure: k3523 
o|inlining procedure: k3552 
o|inlining procedure: k3552 
o|inlining procedure: k3640 
o|inlining procedure: k3640 
o|inlining procedure: k3673 
o|inlining procedure: k3673 
o|substituted constant variable: a3863 
o|inlining procedure: k3859 
o|inlining procedure: k3882 
o|inlining procedure: k3882 
o|inlining procedure: k3859 
o|inlining procedure: k3924 
o|inlining procedure: k3924 
o|contracted procedure: "(srfi-4.scm:669) g994995" 
o|inlining procedure: k4031 
o|inlining procedure: k4031 
o|contracted procedure: "(srfi-4.scm:668) g985986" 
o|inlining procedure: k4007 
o|inlining procedure: k4007 
o|substituted constant variable: a4160 
o|substituted constant variable: a4161 
o|inlining procedure: k4196 
o|inlining procedure: k4196 
o|inlining procedure: k4213 
o|inlining procedure: k4213 
o|inlining procedure: k4222 
o|inlining procedure: k4222 
o|contracted procedure: "(srfi-4.scm:693) g10831084" 
o|inlining procedure: k4237 
o|inlining procedure: k4237 
o|contracted procedure: "(srfi-4.scm:692) g10711072" 
o|inlining procedure: k4167 
o|inlining procedure: k4167 
o|inlining procedure: k4265 
o|inlining procedure: k4265 
o|substituted constant variable: a4320 
o|substituted constant variable: a4321 
o|substituted constant variable: a4400 
o|substituted constant variable: a4401 
o|inlining procedure: k4408 
o|inlining procedure: k4408 
o|simplifications: ((if . 1)) 
o|replaced variables: 675 
o|removed binding forms: 299 
o|substituted constant variable: loc265 
o|substituted constant variable: len264 
o|folded constant expression: (scheme#expt (quote 2) (quote 8)) 
o|substituted constant variable: len264 
o|substituted constant variable: loc265 
o|substituted constant variable: loc299 
o|substituted constant variable: len298 
o|folded constant expression: (scheme#expt (quote 2) (quote 8)) 
o|substituted constant variable: len298 
o|substituted constant variable: loc299 
o|substituted constant variable: loc333 
o|substituted constant variable: len332 
o|folded constant expression: (scheme#expt (quote 2) (quote 16)) 
o|substituted constant variable: len332 
o|substituted constant variable: loc333 
o|substituted constant variable: loc367 
o|substituted constant variable: len366 
o|folded constant expression: (scheme#expt (quote 2) (quote 16)) 
o|substituted constant variable: len366 
o|folded constant expression: (scheme#expt (quote 2) (quote 16)) 
o|substituted constant variable: len366 
o|substituted constant variable: loc367 
o|substituted constant variable: loc401 
o|substituted constant variable: len400 
o|folded constant expression: (scheme#expt (quote 2) (quote 32)) 
o|substituted constant variable: len400 
o|substituted constant variable: loc401 
o|substituted constant variable: loc435 
o|substituted constant variable: len434 
o|folded constant expression: (scheme#expt (quote 2) (quote 64)) 
o|substituted constant variable: len434 
o|substituted constant variable: loc435 
o|substituted constant variable: loc469 
o|substituted constant variable: len468 
o|folded constant expression: (scheme#expt (quote 2) (quote 32)) 
o|substituted constant variable: len468 
o|folded constant expression: (scheme#expt (quote 2) (quote 32)) 
o|substituted constant variable: len468 
o|substituted constant variable: loc469 
o|substituted constant variable: loc503 
o|substituted constant variable: len502 
o|folded constant expression: (scheme#expt (quote 2) (quote 64)) 
o|substituted constant variable: len502 
o|folded constant expression: (scheme#expt (quote 2) (quote 64)) 
o|substituted constant variable: len502 
o|substituted constant variable: loc503 
o|substituted constant variable: loc536 
o|substituted constant variable: loc572 
o|substituted constant variable: r32884563 
o|substituted constant variable: r33184565 
o|substituted constant variable: r33484567 
o|substituted constant variable: r33784569 
o|substituted constant variable: r34084571 
o|substituted constant variable: r34374573 
o|substituted constant variable: r34664575 
o|substituted constant variable: r34954577 
o|substituted constant variable: r35244579 
o|substituted constant variable: r35534581 
o|substituted constant variable: from997 
o|substituted constant variable: from997 
o|substituted constant variable: from988 
o|substituted constant variable: from988 
o|substituted constant variable: loc1088 
o|substituted constant variable: loc1088 
o|substituted constant variable: loc1076 
o|substituted constant variable: from1074 
o|substituted constant variable: from1074 
o|substituted constant variable: loc1076 
o|replaced variables: 133 
o|removed binding forms: 511 
o|inlining procedure: k1639 
o|contracted procedure: k1724 
o|inlining procedure: k1745 
o|inlining procedure: k1745 
o|contracted procedure: k1840 
o|inlining procedure: k1861 
o|inlining procedure: k1861 
o|contracted procedure: k1956 
o|inlining procedure: k1977 
o|inlining procedure: k1977 
o|contracted procedure: k2073 
o|contracted procedure: k2078 
o|inlining procedure: k2103 
o|inlining procedure: k2103 
o|contracted procedure: k2198 
o|inlining procedure: k2219 
o|inlining procedure: k2219 
o|contracted procedure: k2314 
o|inlining procedure: k2335 
o|inlining procedure: k2335 
o|contracted procedure: k2431 
o|contracted procedure: k2436 
o|inlining procedure: k2461 
o|inlining procedure: k2461 
o|contracted procedure: k2557 
o|contracted procedure: k2562 
o|inlining procedure: k2587 
o|inlining procedure: k2587 
o|inlining procedure: k2698 
o|inlining procedure: k2698 
o|inlining procedure: k2809 
o|inlining procedure: k2809 
o|inlining procedure: k4337 
o|inlining procedure: k4337 
o|removed binding forms: 123 
o|substituted constant variable: r1725 
o|substituted constant variable: r17464762 
o|substituted constant variable: r1841 
o|substituted constant variable: r18624766 
o|substituted constant variable: r1957 
o|substituted constant variable: r19784770 
o|substituted constant variable: r2074 
o|substituted constant variable: r2079 
o|substituted constant variable: r21044774 
o|substituted constant variable: r2199 
o|substituted constant variable: r22204778 
o|substituted constant variable: r2315 
o|substituted constant variable: r23364782 
o|substituted constant variable: r2432 
o|substituted constant variable: r2437 
o|substituted constant variable: r24624786 
o|substituted constant variable: r2558 
o|substituted constant variable: r2563 
o|substituted constant variable: r25884790 
o|substituted constant variable: r26994794 
o|substituted constant variable: r28104798 
o|replaced variables: 10 
o|removed binding forms: 14 
o|removed conditional forms: 10 
o|removed binding forms: 31 
o|simplifications: ((if . 96) (let . 47) (##core#call . 313)) 
o|  call simplifications:
o|    scheme#list
o|    scheme#eof-object?
o|    chicken.fixnum#fx=	2
o|    chicken.fixnum#fx<=	5
o|    chicken.fixnum#fx*	2
o|    ##sys#list	11
o|    scheme#cadr
o|    scheme#caddr
o|    scheme#symbol?
o|    scheme#memq
o|    ##sys#check-byte-vector	2
o|    scheme#eq?	6
o|    ##sys#size	5
o|    ##sys#slot	10
o|    ##sys#check-structure	15
o|    chicken.fixnum#fx>=	10
o|    chicken.fixnum#fx+	16
o|    scheme#cons	10
o|    srfi-4#f64vector-set!
o|    srfi-4#f32vector-set!
o|    srfi-4#s64vector-set!
o|    srfi-4#u64vector-set!
o|    srfi-4#s32vector-set!
o|    srfi-4#u32vector-set!
o|    srfi-4#s16vector-set!
o|    srfi-4#u16vector-set!
o|    srfi-4#s8vector-set!
o|    ##sys#check-list	10
o|    srfi-4#u8vector-set!
o|    chicken.fixnum#fx-	5
o|    scheme#car	37
o|    scheme#null?	74
o|    scheme#cdr	37
o|    ##sys#make-structure	14
o|    scheme#not	11
o|    chicken.fixnum#fx>	8
o|    chicken.fixnum#fx<	5
o|    chicken.fixnum#fx*?
o|    ##sys#foreign-unsigned-ranged-integer-argument
o|contracted procedure: k1630 
o|contracted procedure: k1616 
o|contracted procedure: k1654 
o|contracted procedure: k1785 
o|contracted procedure: k1678 
o|contracted procedure: k1779 
o|contracted procedure: k1681 
o|contracted procedure: k1773 
o|contracted procedure: k1684 
o|contracted procedure: k1767 
o|contracted procedure: k1687 
o|contracted procedure: k1761 
o|contracted procedure: k1690 
o|contracted procedure: k1755 
o|contracted procedure: k1693 
o|contracted procedure: k1696 
o|contracted procedure: k1705 
o|contracted procedure: k1716 
o|contracted procedure: k1901 
o|contracted procedure: k1794 
o|contracted procedure: k1895 
o|contracted procedure: k1797 
o|contracted procedure: k1889 
o|contracted procedure: k1800 
o|contracted procedure: k1883 
o|contracted procedure: k1803 
o|contracted procedure: k1877 
o|contracted procedure: k1806 
o|contracted procedure: k1871 
o|contracted procedure: k1809 
o|contracted procedure: k1812 
o|contracted procedure: k1821 
o|contracted procedure: k1832 
o|contracted procedure: k2017 
o|contracted procedure: k1910 
o|contracted procedure: k2011 
o|contracted procedure: k1913 
o|contracted procedure: k2005 
o|contracted procedure: k1916 
o|contracted procedure: k1999 
o|contracted procedure: k1919 
o|contracted procedure: k1993 
o|contracted procedure: k1922 
o|contracted procedure: k1987 
o|contracted procedure: k1925 
o|contracted procedure: k1928 
o|contracted procedure: k1937 
o|contracted procedure: k1948 
o|contracted procedure: k2143 
o|contracted procedure: k2026 
o|contracted procedure: k2137 
o|contracted procedure: k2029 
o|contracted procedure: k2131 
o|contracted procedure: k2032 
o|contracted procedure: k2125 
o|contracted procedure: k2035 
o|contracted procedure: k2119 
o|contracted procedure: k2038 
o|contracted procedure: k2113 
o|contracted procedure: k2041 
o|contracted procedure: k2044 
o|contracted procedure: k2053 
o|contracted procedure: k2084 
o|contracted procedure: k2064 
o|contracted procedure: k2259 
o|contracted procedure: k2152 
o|contracted procedure: k2253 
o|contracted procedure: k2155 
o|contracted procedure: k2247 
o|contracted procedure: k2158 
o|contracted procedure: k2241 
o|contracted procedure: k2161 
o|contracted procedure: k2235 
o|contracted procedure: k2164 
o|contracted procedure: k2229 
o|contracted procedure: k2167 
o|contracted procedure: k2170 
o|contracted procedure: k2179 
o|contracted procedure: k2190 
o|contracted procedure: k2375 
o|contracted procedure: k2268 
o|contracted procedure: k2369 
o|contracted procedure: k2271 
o|contracted procedure: k2363 
o|contracted procedure: k2274 
o|contracted procedure: k2357 
o|contracted procedure: k2277 
o|contracted procedure: k2351 
o|contracted procedure: k2280 
o|contracted procedure: k2345 
o|contracted procedure: k2283 
o|contracted procedure: k2286 
o|contracted procedure: k2295 
o|contracted procedure: k2306 
o|contracted procedure: k2501 
o|contracted procedure: k2384 
o|contracted procedure: k2495 
o|contracted procedure: k2387 
o|contracted procedure: k2489 
o|contracted procedure: k2390 
o|contracted procedure: k2483 
o|contracted procedure: k2393 
o|contracted procedure: k2477 
o|contracted procedure: k2396 
o|contracted procedure: k2471 
o|contracted procedure: k2399 
o|contracted procedure: k2402 
o|contracted procedure: k2411 
o|contracted procedure: k2442 
o|contracted procedure: k2422 
o|contracted procedure: k2627 
o|contracted procedure: k2510 
o|contracted procedure: k2621 
o|contracted procedure: k2513 
o|contracted procedure: k2615 
o|contracted procedure: k2516 
o|contracted procedure: k2609 
o|contracted procedure: k2519 
o|contracted procedure: k2603 
o|contracted procedure: k2522 
o|contracted procedure: k2597 
o|contracted procedure: k2525 
o|contracted procedure: k2528 
o|contracted procedure: k2537 
o|contracted procedure: k2568 
o|contracted procedure: k2548 
o|contracted procedure: k2738 
o|contracted procedure: k2636 
o|contracted procedure: k2732 
o|contracted procedure: k2639 
o|contracted procedure: k2726 
o|contracted procedure: k2642 
o|contracted procedure: k2720 
o|contracted procedure: k2645 
o|contracted procedure: k2714 
o|contracted procedure: k2648 
o|contracted procedure: k2708 
o|contracted procedure: k2651 
o|contracted procedure: k2654 
o|contracted procedure: k2663 
o|contracted procedure: k2671 
o|contracted procedure: k2849 
o|contracted procedure: k2747 
o|contracted procedure: k2843 
o|contracted procedure: k2750 
o|contracted procedure: k2837 
o|contracted procedure: k2753 
o|contracted procedure: k2831 
o|contracted procedure: k2756 
o|contracted procedure: k2825 
o|contracted procedure: k2759 
o|contracted procedure: k2819 
o|contracted procedure: k2762 
o|contracted procedure: k2765 
o|contracted procedure: k2774 
o|contracted procedure: k2782 
o|contracted procedure: k2858 
o|contracted procedure: k2881 
o|inlining procedure: k2873 
o|contracted procedure: k2894 
o|contracted procedure: k2917 
o|inlining procedure: k2909 
o|contracted procedure: k2930 
o|contracted procedure: k2953 
o|inlining procedure: k2945 
o|contracted procedure: k2966 
o|contracted procedure: k2989 
o|inlining procedure: k2981 
o|contracted procedure: k3002 
o|contracted procedure: k3025 
o|inlining procedure: k3017 
o|contracted procedure: k3038 
o|contracted procedure: k3061 
o|inlining procedure: k3053 
o|contracted procedure: k3074 
o|contracted procedure: k3097 
o|inlining procedure: k3089 
o|contracted procedure: k3110 
o|contracted procedure: k3133 
o|inlining procedure: k3125 
o|contracted procedure: k3146 
o|contracted procedure: k3169 
o|inlining procedure: k3161 
o|contracted procedure: k3182 
o|contracted procedure: k3205 
o|inlining procedure: k3197 
o|contracted procedure: k3278 
o|contracted procedure: k3290 
o|contracted procedure: k3302 
o|contracted procedure: k3308 
o|contracted procedure: k3320 
o|contracted procedure: k3332 
o|contracted procedure: k3338 
o|contracted procedure: k3350 
o|contracted procedure: k3362 
o|contracted procedure: k3368 
o|contracted procedure: k3380 
o|contracted procedure: k3392 
o|contracted procedure: k3398 
o|contracted procedure: k3410 
o|contracted procedure: k3421 
o|contracted procedure: k3427 
o|contracted procedure: k3439 
o|contracted procedure: k3450 
o|contracted procedure: k3456 
o|contracted procedure: k3468 
o|contracted procedure: k3479 
o|contracted procedure: k3485 
o|contracted procedure: k3497 
o|contracted procedure: k3508 
o|contracted procedure: k3514 
o|contracted procedure: k3526 
o|contracted procedure: k3537 
o|contracted procedure: k3543 
o|contracted procedure: k3555 
o|contracted procedure: k3566 
o|contracted procedure: k3605 
o|contracted procedure: k3616 
o|contracted procedure: k3619 
o|contracted procedure: k3626 
o|contracted procedure: k3634 
o|contracted procedure: k3637 
o|contracted procedure: k3643 
o|contracted procedure: k3646 
o|contracted procedure: k3664 
o|contracted procedure: k3667 
o|contracted procedure: k3676 
o|contracted procedure: k3679 
o|contracted procedure: k3854 
o|contracted procedure: k3910 
o|contracted procedure: k3867 
o|contracted procedure: k3873 
o|contracted procedure: k3876 
o|contracted procedure: k3879 
o|contracted procedure: k3898 
o|contracted procedure: k3887 
o|contracted procedure: k3951 
o|contracted procedure: k3955 
o|contracted procedure: k3959 
o|contracted procedure: k3963 
o|contracted procedure: k3967 
o|contracted procedure: k3971 
o|contracted procedure: k3975 
o|contracted procedure: k3979 
o|contracted procedure: k3983 
o|contracted procedure: k3987 
o|contracted procedure: k3921 
o|contracted procedure: k3937 
o|contracted procedure: k3944 
o|contracted procedure: k3993 
o|contracted procedure: k3996 
o|contracted procedure: k3999 
o|contracted procedure: k4064 
o|contracted procedure: k4050 
o|contracted procedure: k4056 
o|contracted procedure: k4060 
o|contracted procedure: k4068 
o|contracted procedure: k4041 
o|contracted procedure: k4034 
o|contracted procedure: k4072 
o|contracted procedure: k4017 
o|contracted procedure: k4010 
o|contracted procedure: k4298 
o|contracted procedure: k4138 
o|contracted procedure: k4292 
o|contracted procedure: k4141 
o|contracted procedure: k4286 
o|contracted procedure: k4144 
o|contracted procedure: k4280 
o|contracted procedure: k4147 
o|contracted procedure: k4274 
o|contracted procedure: k4150 
o|contracted procedure: k4268 
o|contracted procedure: k4153 
o|contracted procedure: k4156 
o|contracted procedure: k4229 
o|contracted procedure: k4189 
o|inlining procedure: k4196 
o|inlining procedure: k4196 
o|contracted procedure: k4216 
o|contracted procedure: k4219 
o|contracted procedure: k4257 
o|contracted procedure: k4247 
o|contracted procedure: k4240 
o|contracted procedure: k4261 
o|contracted procedure: k4177 
o|contracted procedure: k4170 
o|contracted procedure: k4378 
o|contracted procedure: k4307 
o|contracted procedure: k4372 
o|contracted procedure: k4310 
o|contracted procedure: k4366 
o|contracted procedure: k4313 
o|contracted procedure: k4360 
o|contracted procedure: k4316 
o|contracted procedure: k4325 
o|contracted procedure: k4331 
o|contracted procedure: k4334 
o|contracted procedure: k4347 
o|contracted procedure: k4354 
o|contracted procedure: k4438 
o|contracted procedure: k4387 
o|contracted procedure: k4432 
o|contracted procedure: k4390 
o|contracted procedure: k4426 
o|contracted procedure: k4393 
o|contracted procedure: k4420 
o|contracted procedure: k4396 
o|contracted procedure: k4411 
o|simplifications: ((if . 1) (let . 43)) 
o|removed binding forms: 297 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest244246 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest244246 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest244246 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest244246 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest278280 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest278280 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest278280 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest278280 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest312314 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest312314 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest312314 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest312314 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest346348 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest346348 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest346348 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest346348 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest380382 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest380382 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest380382 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest380382 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest414416 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest414416 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest414416 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest414416 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest448450 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest448450 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest448450 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest448450 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest482484 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest482484 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest482484 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest482484 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest516518 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest516518 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest516518 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest516518 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest552554 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest552554 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest552554 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest552554 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest10551057 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest10551057 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest10551057 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest10551057 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest11091112 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest11091112 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest11091112 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest11091112 0 
o|contracted procedure: k4343 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest11351136 0 
(o x)|known list op on rest arg sublist: ##core#rest-car rest11351136 0 
(o x)|known list op on rest arg sublist: ##core#rest-null? rest11351136 0 
(o x)|known list op on rest arg sublist: ##core#rest-cdr rest11351136 0 
o|inlining procedure: "(srfi-4.scm:577) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:576) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:575) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:574) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:573) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:572) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:571) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:570) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:569) srfi-4#pack" 
o|inlining procedure: "(srfi-4.scm:568) srfi-4#pack" 
o|simplifications: ((let . 2)) 
o|removed binding forms: 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1682 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1682 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1682 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1682 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1798 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1798 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1798 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1798 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1914 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r1914 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1914 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1914 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2030 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2030 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2030 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2030 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2156 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2156 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2156 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2156 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2272 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2272 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2272 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2272 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2388 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2388 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2388 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2388 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2514 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2514 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2514 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2514 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2640 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2640 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2640 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2640 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2751 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r2751 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2751 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2751 1 
o|removed side-effect free assignment to unused variable: srfi-4#pack 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4142 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r4142 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4142 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4142 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4311 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r4311 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4311 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4311 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4391 1 
(o x)|known list op on rest arg sublist: ##core#rest-car r4391 1 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4391 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4391 1 
o|substituted constant variable: tag8665214 
o|substituted constant variable: loc8675215 
o|substituted constant variable: tag8665221 
o|substituted constant variable: loc8675222 
o|substituted constant variable: tag8665228 
o|substituted constant variable: loc8675229 
o|substituted constant variable: tag8665235 
o|substituted constant variable: loc8675236 
o|substituted constant variable: tag8665242 
o|substituted constant variable: loc8675243 
o|substituted constant variable: tag8665249 
o|substituted constant variable: loc8675250 
o|substituted constant variable: tag8665256 
o|substituted constant variable: loc8675257 
o|substituted constant variable: tag8665263 
o|substituted constant variable: loc8675264 
o|substituted constant variable: tag8665270 
o|substituted constant variable: loc8675271 
o|substituted constant variable: tag8665277 
o|substituted constant variable: loc8675278 
o|replaced variables: 10 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1688 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r1688 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1688 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1688 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1804 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r1804 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1804 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1804 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1920 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r1920 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r1920 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r1920 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2036 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2036 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2036 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2036 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2162 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2162 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2162 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2162 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2278 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2278 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2278 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2278 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2394 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2394 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2394 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2394 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2520 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2520 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2520 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2520 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2646 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2646 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2646 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2646 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2757 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r2757 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r2757 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r2757 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4148 2 
(o x)|known list op on rest arg sublist: ##core#rest-car r4148 2 
(o x)|known list op on rest arg sublist: ##core#rest-null? r4148 2 
(o x)|known list op on rest arg sublist: ##core#rest-cdr r4148 2 
o|removed binding forms: 46 
o|contracted procedure: k3694 
o|contracted procedure: k3698 
o|contracted procedure: k3702 
o|contracted procedure: k3706 
o|contracted procedure: k3710 
o|contracted procedure: k3714 
o|contracted procedure: k3718 
o|contracted procedure: k3722 
o|contracted procedure: k3726 
o|contracted procedure: k3730 
o|removed binding forms: 32 
o|direct leaf routine/allocation: doloop268269 0 
o|direct leaf routine/allocation: doloop302303 0 
o|direct leaf routine/allocation: doloop336337 0 
o|direct leaf routine/allocation: doloop370371 0 
o|direct leaf routine/allocation: doloop404405 0 
o|direct leaf routine/allocation: doloop438439 0 
o|direct leaf routine/allocation: doloop472473 0 
o|direct leaf routine/allocation: doloop506507 0 
o|direct leaf routine/allocation: doloop541542 0 
o|direct leaf routine/allocation: doloop577578 0 
o|converted assignments to bindings: (doloop268269) 
o|converted assignments to bindings: (doloop302303) 
o|converted assignments to bindings: (doloop336337) 
o|converted assignments to bindings: (doloop370371) 
o|converted assignments to bindings: (doloop404405) 
o|converted assignments to bindings: (doloop438439) 
o|converted assignments to bindings: (doloop472473) 
o|converted assignments to bindings: (doloop506507) 
o|converted assignments to bindings: (doloop541542) 
o|converted assignments to bindings: (doloop577578) 
o|simplifications: ((let . 10)) 
o|customizable procedures: (srfi-4#pack-copy srfi-4#unpack srfi-4#unpack-copy k4199 srfi-4#subnvector g955956 loop840 loop833 loop826 loop819 loop812 loop805 loop798 loop791 loop784 loop777 doloop729730 doloop716717 doloop703704 doloop690691 doloop677678 doloop664665 doloop651652 doloop638639 doloop625626 doloop612613 k2792 k2681 alloc225) 
o|calls to known targets: 146 
o|identified direct recursive calls: f_1736 1 
o|unused rest argument: rest244246 f_1676 
o|identified direct recursive calls: f_1852 1 
o|unused rest argument: rest278280 f_1792 
o|identified direct recursive calls: f_1968 1 
o|unused rest argument: rest312314 f_1908 
o|identified direct recursive calls: f_2094 1 
o|unused rest argument: rest346348 f_2024 
o|identified direct recursive calls: f_2210 1 
o|unused rest argument: rest380382 f_2150 
o|identified direct recursive calls: f_2326 1 
o|unused rest argument: rest414416 f_2266 
o|identified direct recursive calls: f_2452 1 
o|unused rest argument: rest448450 f_2382 
o|identified direct recursive calls: f_2578 1 
o|unused rest argument: rest482484 f_2508 
o|identified direct recursive calls: f_2688 1 
o|unused rest argument: rest516518 f_2634 
o|identified direct recursive calls: f_2799 1 
o|unused rest argument: rest552554 f_2745 
o|identified direct recursive calls: f_2868 1 
o|identified direct recursive calls: f_2904 1 
o|identified direct recursive calls: f_2940 1 
o|identified direct recursive calls: f_2976 1 
o|identified direct recursive calls: f_3012 1 
o|identified direct recursive calls: f_3048 1 
o|identified direct recursive calls: f_3084 1 
o|identified direct recursive calls: f_3120 1 
o|identified direct recursive calls: f_3156 1 
o|identified direct recursive calls: f_3192 1 
o|identified direct recursive calls: f_3285 1 
o|identified direct recursive calls: f_3315 1 
o|identified direct recursive calls: f_3345 1 
o|identified direct recursive calls: f_3375 1 
o|identified direct recursive calls: f_3405 1 
o|identified direct recursive calls: f_3434 1 
o|identified direct recursive calls: f_3463 1 
o|identified direct recursive calls: f_3492 1 
o|identified direct recursive calls: f_3521 1 
o|identified direct recursive calls: f_3550 1 
o|unused rest argument: rest10551057 f_4136 
o|unused rest argument: rest11091112 f_4305 
o|unused rest argument: rest11351136 f_4385 
o|fast box initializations: 20 
o|fast global references: 40 
o|fast global assignments: 4 
o|dropping unused closure argument: f_1622 
o|dropping unused closure argument: f_3612 
o|dropping unused closure argument: f_3630 
o|dropping unused closure argument: f_3660 
o|dropping unused closure argument: f_3991 
*/
/* end of file */
