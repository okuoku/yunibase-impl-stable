/* Generated from batch-driver.scm by the CHICKEN compiler
   http://www.call-cc.org
   Version 5.2.0 (rev 317468e4)
   linux-unix-gnu-x86-64 [ 64bit dload ptables ]
   command line: batch-driver.scm -optimize-level 2 -include-path . -include-path ./ -inline -ignore-repository -feature chicken-bootstrap -no-warnings -specialize -consult-types-file ./types.db -no-lambda-info -no-trace -emit-import-library chicken.compiler.batch-driver -output-file batch-driver.c
   unit: batch-driver
   uses: library eval expand extras data-structures pathname support compiler-syntax compiler optimizer internal scrutinizer lfa2 c-platform c-backend user-pass
*/
#include "chicken.h"

static C_PTABLE_ENTRY *create_ptable(void);
C_noret_decl(C_library_toplevel)
C_externimport void C_ccall C_library_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_eval_toplevel)
C_externimport void C_ccall C_eval_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_expand_toplevel)
C_externimport void C_ccall C_expand_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_extras_toplevel)
C_externimport void C_ccall C_extras_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_data_2dstructures_toplevel)
C_externimport void C_ccall C_data_2dstructures_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_pathname_toplevel)
C_externimport void C_ccall C_pathname_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_support_toplevel)
C_externimport void C_ccall C_support_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_compiler_2dsyntax_toplevel)
C_externimport void C_ccall C_compiler_2dsyntax_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_compiler_toplevel)
C_externimport void C_ccall C_compiler_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_optimizer_toplevel)
C_externimport void C_ccall C_optimizer_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_internal_toplevel)
C_externimport void C_ccall C_internal_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_scrutinizer_toplevel)
C_externimport void C_ccall C_scrutinizer_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_lfa2_toplevel)
C_externimport void C_ccall C_lfa2_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_c_2dplatform_toplevel)
C_externimport void C_ccall C_c_2dplatform_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_c_2dbackend_toplevel)
C_externimport void C_ccall C_c_2dbackend_toplevel(C_word c,C_word *av) C_noret;
C_noret_decl(C_user_2dpass_toplevel)
C_externimport void C_ccall C_user_2dpass_toplevel(C_word c,C_word *av) C_noret;

static C_TLS C_word lf[499];
static double C_possibly_force_alignment;


C_noret_decl(f8805)
static void C_ccall f8805(C_word c,C_word *av) C_noret;
C_noret_decl(f9308)
static void C_ccall f9308(C_word c,C_word *av) C_noret;
C_noret_decl(f9314)
static void C_ccall f9314(C_word c,C_word *av) C_noret;
C_noret_decl(f9320)
static void C_ccall f9320(C_word c,C_word *av) C_noret;
C_noret_decl(f9326)
static void C_ccall f9326(C_word c,C_word *av) C_noret;
C_noret_decl(f9334)
static void C_ccall f9334(C_word c,C_word *av) C_noret;
C_noret_decl(f9340)
static void C_ccall f9340(C_word c,C_word *av) C_noret;
C_noret_decl(f9352)
static void C_ccall f9352(C_word c,C_word *av) C_noret;
C_noret_decl(f9360)
static void C_ccall f9360(C_word c,C_word *av) C_noret;
C_noret_decl(f9372)
static void C_ccall f9372(C_word c,C_word *av) C_noret;
C_noret_decl(f9396)
static void C_ccall f9396(C_word c,C_word *av) C_noret;
C_noret_decl(f9402)
static void C_ccall f9402(C_word c,C_word *av) C_noret;
C_noret_decl(f9416)
static void C_ccall f9416(C_word c,C_word *av) C_noret;
C_noret_decl(f9422)
static void C_ccall f9422(C_word c,C_word *av) C_noret;
C_noret_decl(f9428)
static void C_ccall f9428(C_word c,C_word *av) C_noret;
C_noret_decl(f9434)
static void C_ccall f9434(C_word c,C_word *av) C_noret;
C_noret_decl(f9440)
static void C_ccall f9440(C_word c,C_word *av) C_noret;
C_noret_decl(f9454)
static void C_ccall f9454(C_word c,C_word *av) C_noret;
C_noret_decl(f9470)
static void C_ccall f9470(C_word c,C_word *av) C_noret;
C_noret_decl(f9476)
static void C_ccall f9476(C_word c,C_word *av) C_noret;
C_noret_decl(f9482)
static void C_ccall f9482(C_word c,C_word *av) C_noret;
C_noret_decl(f9488)
static void C_ccall f9488(C_word c,C_word *av) C_noret;
C_noret_decl(f9494)
static void C_ccall f9494(C_word c,C_word *av) C_noret;
C_noret_decl(f_2725)
static void C_ccall f_2725(C_word c,C_word *av) C_noret;
C_noret_decl(f_2728)
static void C_ccall f_2728(C_word c,C_word *av) C_noret;
C_noret_decl(f_2731)
static void C_ccall f_2731(C_word c,C_word *av) C_noret;
C_noret_decl(f_2734)
static void C_ccall f_2734(C_word c,C_word *av) C_noret;
C_noret_decl(f_2737)
static void C_ccall f_2737(C_word c,C_word *av) C_noret;
C_noret_decl(f_2740)
static void C_ccall f_2740(C_word c,C_word *av) C_noret;
C_noret_decl(f_2743)
static void C_ccall f_2743(C_word c,C_word *av) C_noret;
C_noret_decl(f_2746)
static void C_ccall f_2746(C_word c,C_word *av) C_noret;
C_noret_decl(f_2749)
static void C_ccall f_2749(C_word c,C_word *av) C_noret;
C_noret_decl(f_2752)
static void C_ccall f_2752(C_word c,C_word *av) C_noret;
C_noret_decl(f_2755)
static void C_ccall f_2755(C_word c,C_word *av) C_noret;
C_noret_decl(f_2758)
static void C_ccall f_2758(C_word c,C_word *av) C_noret;
C_noret_decl(f_2761)
static void C_ccall f_2761(C_word c,C_word *av) C_noret;
C_noret_decl(f_2764)
static void C_ccall f_2764(C_word c,C_word *av) C_noret;
C_noret_decl(f_2767)
static void C_ccall f_2767(C_word c,C_word *av) C_noret;
C_noret_decl(f_2770)
static void C_ccall f_2770(C_word c,C_word *av) C_noret;
C_noret_decl(f_2971)
static void C_fcall f_2971(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_2986)
static void C_fcall f_2986(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_2994)
static void C_fcall f_2994(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3002)
static void C_ccall f_3002(C_word c,C_word *av) C_noret;
C_noret_decl(f_3013)
static void C_ccall f_3013(C_word c,C_word *av) C_noret;
C_noret_decl(f_3026)
static void C_fcall f_3026(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3040)
static void C_ccall f_3040(C_word c,C_word *av) C_noret;
C_noret_decl(f_3044)
static void C_ccall f_3044(C_word c,C_word *av) C_noret;
C_noret_decl(f_3056)
static void C_ccall f_3056(C_word c,C_word *av) C_noret;
C_noret_decl(f_3058)
static void C_fcall f_3058(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3105)
static void C_ccall f_3105(C_word c,C_word *av) C_noret;
C_noret_decl(f_3107)
static void C_fcall f_3107(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3147)
static C_word C_fcall f_3147(C_word t0,C_word t1);
C_noret_decl(f_3181)
static C_word C_fcall f_3181(C_word t0);
C_noret_decl(f_3233)
static void C_fcall f_3233(C_word t0,C_word t1) C_noret;
C_noret_decl(f_3239)
static void C_fcall f_3239(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3257)
static void C_ccall f_3257(C_word c,C_word *av) C_noret;
C_noret_decl(f_3267)
static void C_fcall f_3267(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3294)
static void C_ccall f_3294(C_word c,C_word *av) C_noret;
C_noret_decl(f_3381)
static void C_fcall f_3381(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3390)
static void C_fcall f_3390(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_3398)
static void C_fcall f_3398(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_3405)
static void C_ccall f_3405(C_word c,C_word *av) C_noret;
C_noret_decl(f_3419)
static void C_ccall f_3419(C_word c,C_word *av) C_noret;
C_noret_decl(f_3480)
static void C_ccall f_3480(C_word c,C_word *av) C_noret;
C_noret_decl(f_3488)
static void C_ccall f_3488(C_word c,C_word *av) C_noret;
C_noret_decl(f_3771)
static void C_ccall f_3771(C_word c,C_word *av) C_noret;
C_noret_decl(f_3777)
static C_word C_fcall f_3777(C_word t0,C_word t1);
C_noret_decl(f_4056)
static void C_ccall f_4056(C_word c,C_word *av) C_noret;
C_noret_decl(f_4062)
static void C_ccall f_4062(C_word c,C_word *av) C_noret;
C_noret_decl(f_4069)
static void C_ccall f_4069(C_word c,C_word *av) C_noret;
C_noret_decl(f_4075)
static void C_ccall f_4075(C_word c,C_word *av) C_noret;
C_noret_decl(f_4078)
static void C_ccall f_4078(C_word c,C_word *av) C_noret;
C_noret_decl(f_4081)
static void C_ccall f_4081(C_word c,C_word *av) C_noret;
C_noret_decl(f_4084)
static void C_ccall f_4084(C_word c,C_word *av) C_noret;
C_noret_decl(f_4087)
static void C_ccall f_4087(C_word c,C_word *av) C_noret;
C_noret_decl(f_4093)
static void C_ccall f_4093(C_word c,C_word *av) C_noret;
C_noret_decl(f_4096)
static void C_ccall f_4096(C_word c,C_word *av) C_noret;
C_noret_decl(f_4099)
static void C_ccall f_4099(C_word c,C_word *av) C_noret;
C_noret_decl(f_4105)
static void C_ccall f_4105(C_word c,C_word *av) C_noret;
C_noret_decl(f_4108)
static void C_ccall f_4108(C_word c,C_word *av) C_noret;
C_noret_decl(f_4111)
static void C_ccall f_4111(C_word c,C_word *av) C_noret;
C_noret_decl(f_4117)
static void C_ccall f_4117(C_word c,C_word *av) C_noret;
C_noret_decl(f_4120)
static void C_ccall f_4120(C_word c,C_word *av) C_noret;
C_noret_decl(f_4123)
static void C_ccall f_4123(C_word c,C_word *av) C_noret;
C_noret_decl(f_4129)
static void C_ccall f_4129(C_word c,C_word *av) C_noret;
C_noret_decl(f_4132)
static void C_ccall f_4132(C_word c,C_word *av) C_noret;
C_noret_decl(f_4135)
static void C_ccall f_4135(C_word c,C_word *av) C_noret;
C_noret_decl(f_4141)
static void C_ccall f_4141(C_word c,C_word *av) C_noret;
C_noret_decl(f_4144)
static void C_ccall f_4144(C_word c,C_word *av) C_noret;
C_noret_decl(f_4149)
static void C_fcall f_4149(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4153)
static void C_ccall f_4153(C_word c,C_word *av) C_noret;
C_noret_decl(f_4165)
static void C_ccall f_4165(C_word c,C_word *av) C_noret;
C_noret_decl(f_4176)
static void C_ccall f_4176(C_word c,C_word *av) C_noret;
C_noret_decl(f_4189)
static void C_fcall f_4189(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4199)
static void C_ccall f_4199(C_word c,C_word *av) C_noret;
C_noret_decl(f_4212)
static void C_fcall f_4212(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4222)
static void C_ccall f_4222(C_word c,C_word *av) C_noret;
C_noret_decl(f_4235)
static void C_fcall f_4235(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4245)
static void C_ccall f_4245(C_word c,C_word *av) C_noret;
C_noret_decl(f_4258)
static void C_fcall f_4258(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4262)
static void C_fcall f_4262(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4267)
static void C_ccall f_4267(C_word c,C_word *av) C_noret;
C_noret_decl(f_4277)
static void C_ccall f_4277(C_word c,C_word *av) C_noret;
C_noret_decl(f_4280)
static void C_ccall f_4280(C_word c,C_word *av) C_noret;
C_noret_decl(f_4283)
static void C_ccall f_4283(C_word c,C_word *av) C_noret;
C_noret_decl(f_4286)
static void C_ccall f_4286(C_word c,C_word *av) C_noret;
C_noret_decl(f_4289)
static void C_ccall f_4289(C_word c,C_word *av) C_noret;
C_noret_decl(f_4292)
static void C_ccall f_4292(C_word c,C_word *av) C_noret;
C_noret_decl(f_4295)
static void C_ccall f_4295(C_word c,C_word *av) C_noret;
C_noret_decl(f_4309)
static void C_ccall f_4309(C_word c,C_word *av) C_noret;
C_noret_decl(f_4320)
static void C_ccall f_4320(C_word c,C_word *av) C_noret;
C_noret_decl(f_4324)
static void C_ccall f_4324(C_word c,C_word *av) C_noret;
C_noret_decl(f_4332)
static void C_fcall f_4332(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4342)
static void C_ccall f_4342(C_word c,C_word *av) C_noret;
C_noret_decl(f_4362)
static void C_ccall f_4362(C_word c,C_word *av) C_noret;
C_noret_decl(f_4373)
static void C_ccall f_4373(C_word c,C_word *av) C_noret;
C_noret_decl(f_4377)
static void C_ccall f_4377(C_word c,C_word *av) C_noret;
C_noret_decl(f_4389)
static void C_ccall f_4389(C_word c,C_word *av) C_noret;
C_noret_decl(f_4400)
static void C_ccall f_4400(C_word c,C_word *av) C_noret;
C_noret_decl(f_4404)
static void C_ccall f_4404(C_word c,C_word *av) C_noret;
C_noret_decl(f_4427)
static void C_ccall f_4427(C_word c,C_word *av) C_noret;
C_noret_decl(f_4443)
static void C_ccall f_4443(C_word c,C_word *av) C_noret;
C_noret_decl(f_4459)
static void C_ccall f_4459(C_word c,C_word *av) C_noret;
C_noret_decl(f_4468)
static void C_fcall f_4468(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_4481)
static void C_ccall f_4481(C_word c,C_word *av) C_noret;
C_noret_decl(f_4492)
static void C_fcall f_4492(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4498)
static void C_ccall f_4498(C_word c,C_word *av) C_noret;
C_noret_decl(f_4571)
static void C_fcall f_4571(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4577)
static void C_ccall f_4577(C_word c,C_word *av) C_noret;
C_noret_decl(f_4580)
static void C_ccall f_4580(C_word c,C_word *av) C_noret;
C_noret_decl(f_4583)
static void C_ccall f_4583(C_word c,C_word *av) C_noret;
C_noret_decl(f_4885)
static void C_ccall f_4885(C_word c,C_word *av) C_noret;
C_noret_decl(f_4887)
static void C_ccall f_4887(C_word c,C_word *av) C_noret;
C_noret_decl(f_4890)
static void C_fcall f_4890(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4921)
static void C_ccall f_4921(C_word c,C_word *av) C_noret;
C_noret_decl(f_4930)
static void C_fcall f_4930(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4933)
static void C_fcall f_4933(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4936)
static void C_ccall f_4936(C_word c,C_word *av) C_noret;
C_noret_decl(f_4950)
static void C_ccall f_4950(C_word c,C_word *av) C_noret;
C_noret_decl(f_4956)
static void C_ccall f_4956(C_word c,C_word *av) C_noret;
C_noret_decl(f_4962)
static void C_ccall f_4962(C_word c,C_word *av) C_noret;
C_noret_decl(f_4965)
static void C_ccall f_4965(C_word c,C_word *av) C_noret;
C_noret_decl(f_4970)
static void C_fcall f_4970(C_word t0,C_word t1) C_noret;
C_noret_decl(f_4995)
static void C_ccall f_4995(C_word c,C_word *av) C_noret;
C_noret_decl(f_5013)
static void C_fcall f_5013(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5017)
static void C_ccall f_5017(C_word c,C_word *av) C_noret;
C_noret_decl(f_5029)
static void C_ccall f_5029(C_word c,C_word *av) C_noret;
C_noret_decl(f_5032)
static void C_ccall f_5032(C_word c,C_word *av) C_noret;
C_noret_decl(f_5035)
static void C_ccall f_5035(C_word c,C_word *av) C_noret;
C_noret_decl(f_5038)
static void C_ccall f_5038(C_word c,C_word *av) C_noret;
C_noret_decl(f_5040)
static void C_fcall f_5040(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5047)
static void C_ccall f_5047(C_word c,C_word *av) C_noret;
C_noret_decl(f_5060)
static void C_ccall f_5060(C_word c,C_word *av) C_noret;
C_noret_decl(f_5062)
static void C_fcall f_5062(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5) C_noret;
C_noret_decl(f_5069)
static void C_ccall f_5069(C_word c,C_word *av) C_noret;
C_noret_decl(f_5075)
static void C_ccall f_5075(C_word c,C_word *av) C_noret;
C_noret_decl(f_5078)
static void C_ccall f_5078(C_word c,C_word *av) C_noret;
C_noret_decl(f_5081)
static void C_ccall f_5081(C_word c,C_word *av) C_noret;
C_noret_decl(f_5084)
static void C_ccall f_5084(C_word c,C_word *av) C_noret;
C_noret_decl(f_5089)
static void C_fcall f_5089(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5096)
static void C_ccall f_5096(C_word c,C_word *av) C_noret;
C_noret_decl(f_5101)
static void C_ccall f_5101(C_word c,C_word *av) C_noret;
C_noret_decl(f_5112)
static void C_fcall f_5112(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5122)
static void C_ccall f_5122(C_word c,C_word *av) C_noret;
C_noret_decl(f_5135)
static void C_fcall f_5135(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5144)
static void C_ccall f_5144(C_word c,C_word *av) C_noret;
C_noret_decl(f_5175)
static void C_ccall f_5175(C_word c,C_word *av) C_noret;
C_noret_decl(f_5179)
static void C_ccall f_5179(C_word c,C_word *av) C_noret;
C_noret_decl(f_5195)
static void C_ccall f_5195(C_word c,C_word *av) C_noret;
C_noret_decl(f_5199)
static void C_ccall f_5199(C_word c,C_word *av) C_noret;
C_noret_decl(f_5220)
static void C_fcall f_5220(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5226)
static void C_fcall f_5226(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5234)
static void C_fcall f_5234(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5242)
static void C_ccall f_5242(C_word c,C_word *av) C_noret;
C_noret_decl(f_5246)
static void C_ccall f_5246(C_word c,C_word *av) C_noret;
C_noret_decl(f_5255)
static void C_fcall f_5255(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5263)
static void C_ccall f_5263(C_word c,C_word *av) C_noret;
C_noret_decl(f_5265)
static void C_fcall f_5265(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5275)
static void C_ccall f_5275(C_word c,C_word *av) C_noret;
C_noret_decl(f_5278)
static void C_ccall f_5278(C_word c,C_word *av) C_noret;
C_noret_decl(f_5281)
static void C_ccall f_5281(C_word c,C_word *av) C_noret;
C_noret_decl(f_5284)
static void C_ccall f_5284(C_word c,C_word *av) C_noret;
C_noret_decl(f_5291)
static void C_ccall f_5291(C_word c,C_word *av) C_noret;
C_noret_decl(f_5295)
static void C_ccall f_5295(C_word c,C_word *av) C_noret;
C_noret_decl(f_5303)
static void C_ccall f_5303(C_word c,C_word *av) C_noret;
C_noret_decl(f_5305)
static void C_fcall f_5305(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4) C_noret;
C_noret_decl(f_5307)
static void C_fcall f_5307(C_word t0,C_word t1,C_word t2,C_word t3) C_noret;
C_noret_decl(f_5311)
static void C_ccall f_5311(C_word c,C_word *av) C_noret;
C_noret_decl(f_5314)
static void C_ccall f_5314(C_word c,C_word *av) C_noret;
C_noret_decl(f_5319)
static void C_ccall f_5319(C_word c,C_word *av) C_noret;
C_noret_decl(f_5325)
static void C_ccall f_5325(C_word c,C_word *av) C_noret;
C_noret_decl(f_5330)
static void C_fcall f_5330(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5335)
static void C_fcall f_5335(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5371)
static void C_fcall f_5371(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5374)
static void C_fcall f_5374(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5377)
static void C_ccall f_5377(C_word c,C_word *av) C_noret;
C_noret_decl(f_5384)
static void C_ccall f_5384(C_word c,C_word *av) C_noret;
C_noret_decl(f_5387)
static void C_ccall f_5387(C_word c,C_word *av) C_noret;
C_noret_decl(f_5404)
static void C_ccall f_5404(C_word c,C_word *av) C_noret;
C_noret_decl(f_5408)
static void C_ccall f_5408(C_word c,C_word *av) C_noret;
C_noret_decl(f_5413)
static void C_ccall f_5413(C_word c,C_word *av) C_noret;
C_noret_decl(f_5419)
static void C_ccall f_5419(C_word c,C_word *av) C_noret;
C_noret_decl(f_5422)
static void C_fcall f_5422(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5425)
static void C_fcall f_5425(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5428)
static void C_ccall f_5428(C_word c,C_word *av) C_noret;
C_noret_decl(f_5431)
static void C_fcall f_5431(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5434)
static void C_fcall f_5434(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5437)
static void C_fcall f_5437(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5440)
static void C_fcall f_5440(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5443)
static void C_fcall f_5443(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5446)
static void C_fcall f_5446(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5449)
static void C_fcall f_5449(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5452)
static void C_fcall f_5452(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5455)
static void C_fcall f_5455(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5458)
static void C_fcall f_5458(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5461)
static void C_fcall f_5461(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5464)
static void C_fcall f_5464(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5467)
static void C_fcall f_5467(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5470)
static void C_fcall f_5470(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5473)
static void C_fcall f_5473(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5476)
static void C_fcall f_5476(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5479)
static void C_fcall f_5479(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5482)
static void C_fcall f_5482(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5487)
static void C_fcall f_5487(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5492)
static void C_fcall f_5492(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5497)
static void C_fcall f_5497(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5502)
static void C_fcall f_5502(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5507)
static void C_fcall f_5507(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5510)
static void C_ccall f_5510(C_word c,C_word *av) C_noret;
C_noret_decl(f_5513)
static void C_ccall f_5513(C_word c,C_word *av) C_noret;
C_noret_decl(f_5516)
static void C_ccall f_5516(C_word c,C_word *av) C_noret;
C_noret_decl(f_5519)
static void C_ccall f_5519(C_word c,C_word *av) C_noret;
C_noret_decl(f_5522)
static void C_ccall f_5522(C_word c,C_word *av) C_noret;
C_noret_decl(f_5528)
static void C_ccall f_5528(C_word c,C_word *av) C_noret;
C_noret_decl(f_5531)
static void C_ccall f_5531(C_word c,C_word *av) C_noret;
C_noret_decl(f_5534)
static void C_fcall f_5534(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5537)
static void C_fcall f_5537(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5540)
static void C_fcall f_5540(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5543)
static void C_fcall f_5543(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5546)
static void C_fcall f_5546(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5549)
static void C_ccall f_5549(C_word c,C_word *av) C_noret;
C_noret_decl(f_5552)
static void C_ccall f_5552(C_word c,C_word *av) C_noret;
C_noret_decl(f_5555)
static void C_ccall f_5555(C_word c,C_word *av) C_noret;
C_noret_decl(f_5561)
static void C_ccall f_5561(C_word c,C_word *av) C_noret;
C_noret_decl(f_5564)
static void C_ccall f_5564(C_word c,C_word *av) C_noret;
C_noret_decl(f_5570)
static void C_ccall f_5570(C_word c,C_word *av) C_noret;
C_noret_decl(f_5577)
static void C_ccall f_5577(C_word c,C_word *av) C_noret;
C_noret_decl(f_5580)
static void C_ccall f_5580(C_word c,C_word *av) C_noret;
C_noret_decl(f_5585)
static void C_ccall f_5585(C_word c,C_word *av) C_noret;
C_noret_decl(f_5588)
static void C_ccall f_5588(C_word c,C_word *av) C_noret;
C_noret_decl(f_5603)
static void C_ccall f_5603(C_word c,C_word *av) C_noret;
C_noret_decl(f_5607)
static void C_ccall f_5607(C_word c,C_word *av) C_noret;
C_noret_decl(f_5615)
static void C_ccall f_5615(C_word c,C_word *av) C_noret;
C_noret_decl(f_5618)
static void C_ccall f_5618(C_word c,C_word *av) C_noret;
C_noret_decl(f_5621)
static void C_fcall f_5621(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5625)
static void C_ccall f_5625(C_word c,C_word *av) C_noret;
C_noret_decl(f_5628)
static void C_fcall f_5628(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5632)
static void C_ccall f_5632(C_word c,C_word *av) C_noret;
C_noret_decl(f_5636)
static void C_ccall f_5636(C_word c,C_word *av) C_noret;
C_noret_decl(f_5647)
static void C_ccall f_5647(C_word c,C_word *av) C_noret;
C_noret_decl(f_5650)
static void C_ccall f_5650(C_word c,C_word *av) C_noret;
C_noret_decl(f_5653)
static void C_fcall f_5653(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5656)
static void C_ccall f_5656(C_word c,C_word *av) C_noret;
C_noret_decl(f_5659)
static void C_ccall f_5659(C_word c,C_word *av) C_noret;
C_noret_decl(f_5662)
static void C_ccall f_5662(C_word c,C_word *av) C_noret;
C_noret_decl(f_5670)
static void C_ccall f_5670(C_word c,C_word *av) C_noret;
C_noret_decl(f_5681)
static void C_fcall f_5681(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5692)
static void C_ccall f_5692(C_word c,C_word *av) C_noret;
C_noret_decl(f_5699)
static void C_ccall f_5699(C_word c,C_word *av) C_noret;
C_noret_decl(f_5708)
static void C_ccall f_5708(C_word c,C_word *av) C_noret;
C_noret_decl(f_5711)
static void C_ccall f_5711(C_word c,C_word *av) C_noret;
C_noret_decl(f_5714)
static void C_ccall f_5714(C_word c,C_word *av) C_noret;
C_noret_decl(f_5720)
static void C_ccall f_5720(C_word c,C_word *av) C_noret;
C_noret_decl(f_5723)
static void C_ccall f_5723(C_word c,C_word *av) C_noret;
C_noret_decl(f_5726)
static void C_ccall f_5726(C_word c,C_word *av) C_noret;
C_noret_decl(f_5729)
static void C_ccall f_5729(C_word c,C_word *av) C_noret;
C_noret_decl(f_5732)
static void C_ccall f_5732(C_word c,C_word *av) C_noret;
C_noret_decl(f_5736)
static void C_ccall f_5736(C_word c,C_word *av) C_noret;
C_noret_decl(f_5740)
static void C_ccall f_5740(C_word c,C_word *av) C_noret;
C_noret_decl(f_5743)
static void C_ccall f_5743(C_word c,C_word *av) C_noret;
C_noret_decl(f_5746)
static void C_ccall f_5746(C_word c,C_word *av) C_noret;
C_noret_decl(f_5749)
static void C_ccall f_5749(C_word c,C_word *av) C_noret;
C_noret_decl(f_5752)
static void C_ccall f_5752(C_word c,C_word *av) C_noret;
C_noret_decl(f_5755)
static void C_ccall f_5755(C_word c,C_word *av) C_noret;
C_noret_decl(f_5758)
static void C_ccall f_5758(C_word c,C_word *av) C_noret;
C_noret_decl(f_5761)
static void C_fcall f_5761(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5764)
static void C_ccall f_5764(C_word c,C_word *av) C_noret;
C_noret_decl(f_5767)
static void C_ccall f_5767(C_word c,C_word *av) C_noret;
C_noret_decl(f_5771)
static void C_fcall f_5771(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_5777)
static void C_ccall f_5777(C_word c,C_word *av) C_noret;
C_noret_decl(f_5782)
static void C_ccall f_5782(C_word c,C_word *av) C_noret;
C_noret_decl(f_5788)
static void C_ccall f_5788(C_word c,C_word *av) C_noret;
C_noret_decl(f_5794)
static void C_ccall f_5794(C_word c,C_word *av) C_noret;
C_noret_decl(f_5797)
static void C_fcall f_5797(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5803)
static void C_ccall f_5803(C_word c,C_word *av) C_noret;
C_noret_decl(f_5806)
static void C_ccall f_5806(C_word c,C_word *av) C_noret;
C_noret_decl(f_5809)
static void C_ccall f_5809(C_word c,C_word *av) C_noret;
C_noret_decl(f_5812)
static void C_ccall f_5812(C_word c,C_word *av) C_noret;
C_noret_decl(f_5815)
static void C_ccall f_5815(C_word c,C_word *av) C_noret;
C_noret_decl(f_5818)
static void C_ccall f_5818(C_word c,C_word *av) C_noret;
C_noret_decl(f_5821)
static void C_ccall f_5821(C_word c,C_word *av) C_noret;
C_noret_decl(f_5824)
static void C_ccall f_5824(C_word c,C_word *av) C_noret;
C_noret_decl(f_5829)
static void C_ccall f_5829(C_word c,C_word *av) C_noret;
C_noret_decl(f_5832)
static void C_ccall f_5832(C_word c,C_word *av) C_noret;
C_noret_decl(f_5835)
static void C_ccall f_5835(C_word c,C_word *av) C_noret;
C_noret_decl(f_5838)
static void C_ccall f_5838(C_word c,C_word *av) C_noret;
C_noret_decl(f_5841)
static void C_ccall f_5841(C_word c,C_word *av) C_noret;
C_noret_decl(f_5844)
static void C_ccall f_5844(C_word c,C_word *av) C_noret;
C_noret_decl(f_5847)
static void C_ccall f_5847(C_word c,C_word *av) C_noret;
C_noret_decl(f_5850)
static void C_ccall f_5850(C_word c,C_word *av) C_noret;
C_noret_decl(f_5853)
static void C_ccall f_5853(C_word c,C_word *av) C_noret;
C_noret_decl(f_5856)
static void C_ccall f_5856(C_word c,C_word *av) C_noret;
C_noret_decl(f_5859)
static void C_ccall f_5859(C_word c,C_word *av) C_noret;
C_noret_decl(f_5862)
static void C_ccall f_5862(C_word c,C_word *av) C_noret;
C_noret_decl(f_5865)
static void C_ccall f_5865(C_word c,C_word *av) C_noret;
C_noret_decl(f_5868)
static void C_ccall f_5868(C_word c,C_word *av) C_noret;
C_noret_decl(f_5871)
static void C_fcall f_5871(C_word t0,C_word t1) C_noret;
C_noret_decl(f_5877)
static void C_ccall f_5877(C_word c,C_word *av) C_noret;
C_noret_decl(f_5880)
static void C_ccall f_5880(C_word c,C_word *av) C_noret;
C_noret_decl(f_5883)
static void C_ccall f_5883(C_word c,C_word *av) C_noret;
C_noret_decl(f_5886)
static void C_ccall f_5886(C_word c,C_word *av) C_noret;
C_noret_decl(f_5889)
static void C_ccall f_5889(C_word c,C_word *av) C_noret;
C_noret_decl(f_5894)
static void C_fcall f_5894(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6) C_noret;
C_noret_decl(f_5898)
static void C_ccall f_5898(C_word c,C_word *av) C_noret;
C_noret_decl(f_5901)
static void C_ccall f_5901(C_word c,C_word *av) C_noret;
C_noret_decl(f_5904)
static void C_ccall f_5904(C_word c,C_word *av) C_noret;
C_noret_decl(f_5908)
static void C_ccall f_5908(C_word c,C_word *av) C_noret;
C_noret_decl(f_5911)
static void C_ccall f_5911(C_word c,C_word *av) C_noret;
C_noret_decl(f_5914)
static void C_ccall f_5914(C_word c,C_word *av) C_noret;
C_noret_decl(f_5920)
static void C_ccall f_5920(C_word c,C_word *av) C_noret;
C_noret_decl(f_5923)
static void C_ccall f_5923(C_word c,C_word *av) C_noret;
C_noret_decl(f_5928)
static void C_ccall f_5928(C_word c,C_word *av) C_noret;
C_noret_decl(f_5940)
static void C_ccall f_5940(C_word c,C_word *av) C_noret;
C_noret_decl(f_5944)
static void C_ccall f_5944(C_word c,C_word *av) C_noret;
C_noret_decl(f_5947)
static void C_ccall f_5947(C_word c,C_word *av) C_noret;
C_noret_decl(f_5964)
static void C_ccall f_5964(C_word c,C_word *av) C_noret;
C_noret_decl(f_5978)
static void C_ccall f_5978(C_word c,C_word *av) C_noret;
C_noret_decl(f_5990)
static void C_ccall f_5990(C_word c,C_word *av) C_noret;
C_noret_decl(f_5993)
static void C_ccall f_5993(C_word c,C_word *av) C_noret;
C_noret_decl(f_5996)
static void C_ccall f_5996(C_word c,C_word *av) C_noret;
C_noret_decl(f_5999)
static void C_ccall f_5999(C_word c,C_word *av) C_noret;
C_noret_decl(f_6002)
static void C_ccall f_6002(C_word c,C_word *av) C_noret;
C_noret_decl(f_6005)
static void C_ccall f_6005(C_word c,C_word *av) C_noret;
C_noret_decl(f_6021)
static void C_ccall f_6021(C_word c,C_word *av) C_noret;
C_noret_decl(f_6024)
static void C_ccall f_6024(C_word c,C_word *av) C_noret;
C_noret_decl(f_6027)
static void C_ccall f_6027(C_word c,C_word *av) C_noret;
C_noret_decl(f_6030)
static void C_ccall f_6030(C_word c,C_word *av) C_noret;
C_noret_decl(f_6034)
static void C_ccall f_6034(C_word c,C_word *av) C_noret;
C_noret_decl(f_6037)
static void C_ccall f_6037(C_word c,C_word *av) C_noret;
C_noret_decl(f_6040)
static void C_ccall f_6040(C_word c,C_word *av) C_noret;
C_noret_decl(f_6043)
static void C_ccall f_6043(C_word c,C_word *av) C_noret;
C_noret_decl(f_6046)
static void C_ccall f_6046(C_word c,C_word *av) C_noret;
C_noret_decl(f_6049)
static void C_ccall f_6049(C_word c,C_word *av) C_noret;
C_noret_decl(f_6052)
static void C_ccall f_6052(C_word c,C_word *av) C_noret;
C_noret_decl(f_6057)
static void C_ccall f_6057(C_word c,C_word *av) C_noret;
C_noret_decl(f_6063)
static void C_ccall f_6063(C_word c,C_word *av) C_noret;
C_noret_decl(f_6067)
static void C_ccall f_6067(C_word c,C_word *av) C_noret;
C_noret_decl(f_6070)
static void C_ccall f_6070(C_word c,C_word *av) C_noret;
C_noret_decl(f_6073)
static void C_ccall f_6073(C_word c,C_word *av) C_noret;
C_noret_decl(f_6076)
static void C_ccall f_6076(C_word c,C_word *av) C_noret;
C_noret_decl(f_6079)
static void C_ccall f_6079(C_word c,C_word *av) C_noret;
C_noret_decl(f_6082)
static void C_ccall f_6082(C_word c,C_word *av) C_noret;
C_noret_decl(f_6085)
static void C_ccall f_6085(C_word c,C_word *av) C_noret;
C_noret_decl(f_6088)
static void C_ccall f_6088(C_word c,C_word *av) C_noret;
C_noret_decl(f_6091)
static void C_ccall f_6091(C_word c,C_word *av) C_noret;
C_noret_decl(f_6094)
static void C_ccall f_6094(C_word c,C_word *av) C_noret;
C_noret_decl(f_6107)
static void C_ccall f_6107(C_word c,C_word *av) C_noret;
C_noret_decl(f_6116)
static void C_ccall f_6116(C_word c,C_word *av) C_noret;
C_noret_decl(f_6121)
static void C_ccall f_6121(C_word c,C_word *av) C_noret;
C_noret_decl(f_6145)
static void C_ccall f_6145(C_word c,C_word *av) C_noret;
C_noret_decl(f_6151)
static void C_ccall f_6151(C_word c,C_word *av) C_noret;
C_noret_decl(f_6164)
static void C_ccall f_6164(C_word c,C_word *av) C_noret;
C_noret_decl(f_6166)
static void C_fcall f_6166(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6191)
static void C_ccall f_6191(C_word c,C_word *av) C_noret;
C_noret_decl(f_6201)
static void C_ccall f_6201(C_word c,C_word *av) C_noret;
C_noret_decl(f_6204)
static void C_ccall f_6204(C_word c,C_word *av) C_noret;
C_noret_decl(f_6207)
static void C_ccall f_6207(C_word c,C_word *av) C_noret;
C_noret_decl(f_6210)
static void C_ccall f_6210(C_word c,C_word *av) C_noret;
C_noret_decl(f_6222)
static void C_ccall f_6222(C_word c,C_word *av) C_noret;
C_noret_decl(f_6225)
static void C_ccall f_6225(C_word c,C_word *av) C_noret;
C_noret_decl(f_6229)
static void C_ccall f_6229(C_word c,C_word *av) C_noret;
C_noret_decl(f_6238)
static void C_ccall f_6238(C_word c,C_word *av) C_noret;
C_noret_decl(f_6241)
static void C_ccall f_6241(C_word c,C_word *av) C_noret;
C_noret_decl(f_6244)
static void C_ccall f_6244(C_word c,C_word *av) C_noret;
C_noret_decl(f_6250)
static void C_ccall f_6250(C_word c,C_word *av) C_noret;
C_noret_decl(f_6282)
static void C_ccall f_6282(C_word c,C_word *av) C_noret;
C_noret_decl(f_6288)
static void C_ccall f_6288(C_word c,C_word *av) C_noret;
C_noret_decl(f_6293)
static void C_ccall f_6293(C_word c,C_word *av) C_noret;
C_noret_decl(f_6302)
static void C_ccall f_6302(C_word c,C_word *av) C_noret;
C_noret_decl(f_6308)
static void C_ccall f_6308(C_word c,C_word *av) C_noret;
C_noret_decl(f_6317)
static void C_ccall f_6317(C_word c,C_word *av) C_noret;
C_noret_decl(f_6321)
static void C_ccall f_6321(C_word c,C_word *av) C_noret;
C_noret_decl(f_6327)
static void C_ccall f_6327(C_word c,C_word *av) C_noret;
C_noret_decl(f_6330)
static void C_ccall f_6330(C_word c,C_word *av) C_noret;
C_noret_decl(f_6335)
static void C_ccall f_6335(C_word c,C_word *av) C_noret;
C_noret_decl(f_6338)
static void C_ccall f_6338(C_word c,C_word *av) C_noret;
C_noret_decl(f_6341)
static void C_ccall f_6341(C_word c,C_word *av) C_noret;
C_noret_decl(f_6344)
static void C_ccall f_6344(C_word c,C_word *av) C_noret;
C_noret_decl(f_6347)
static void C_ccall f_6347(C_word c,C_word *av) C_noret;
C_noret_decl(f_6350)
static void C_ccall f_6350(C_word c,C_word *av) C_noret;
C_noret_decl(f_6353)
static void C_ccall f_6353(C_word c,C_word *av) C_noret;
C_noret_decl(f_6356)
static void C_ccall f_6356(C_word c,C_word *av) C_noret;
C_noret_decl(f_6362)
static void C_fcall f_6362(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6372)
static void C_ccall f_6372(C_word c,C_word *av) C_noret;
C_noret_decl(f_6385)
static void C_fcall f_6385(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6395)
static void C_ccall f_6395(C_word c,C_word *av) C_noret;
C_noret_decl(f_6414)
static void C_ccall f_6414(C_word c,C_word *av) C_noret;
C_noret_decl(f_6426)
static void C_ccall f_6426(C_word c,C_word *av) C_noret;
C_noret_decl(f_6437)
static void C_fcall f_6437(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6447)
static void C_ccall f_6447(C_word c,C_word *av) C_noret;
C_noret_decl(f_6463)
static void C_ccall f_6463(C_word c,C_word *av) C_noret;
C_noret_decl(f_6469)
static void C_ccall f_6469(C_word c,C_word *av) C_noret;
C_noret_decl(f_6476)
static void C_ccall f_6476(C_word c,C_word *av) C_noret;
C_noret_decl(f_6484)
static void C_fcall f_6484(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6494)
static void C_ccall f_6494(C_word c,C_word *av) C_noret;
C_noret_decl(f_6508)
static void C_ccall f_6508(C_word c,C_word *av) C_noret;
C_noret_decl(f_6521)
static void C_ccall f_6521(C_word c,C_word *av) C_noret;
C_noret_decl(f_6523)
static void C_fcall f_6523(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6559)
static void C_ccall f_6559(C_word c,C_word *av) C_noret;
C_noret_decl(f_6563)
static void C_ccall f_6563(C_word c,C_word *av) C_noret;
C_noret_decl(f_6567)
static void C_ccall f_6567(C_word c,C_word *av) C_noret;
C_noret_decl(f_6570)
static void C_ccall f_6570(C_word c,C_word *av) C_noret;
C_noret_decl(f_6573)
static void C_ccall f_6573(C_word c,C_word *av) C_noret;
C_noret_decl(f_6583)
static void C_ccall f_6583(C_word c,C_word *av) C_noret;
C_noret_decl(f_6588)
static void C_fcall f_6588(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6613)
static void C_ccall f_6613(C_word c,C_word *av) C_noret;
C_noret_decl(f_6628)
static void C_ccall f_6628(C_word c,C_word *av) C_noret;
C_noret_decl(f_6634)
static void C_ccall f_6634(C_word c,C_word *av) C_noret;
C_noret_decl(f_6645)
static void C_ccall f_6645(C_word c,C_word *av) C_noret;
C_noret_decl(f_6649)
static void C_ccall f_6649(C_word c,C_word *av) C_noret;
C_noret_decl(f_6657)
static void C_ccall f_6657(C_word c,C_word *av) C_noret;
C_noret_decl(f_6660)
static void C_ccall f_6660(C_word c,C_word *av) C_noret;
C_noret_decl(f_6663)
static void C_ccall f_6663(C_word c,C_word *av) C_noret;
C_noret_decl(f_6666)
static void C_ccall f_6666(C_word c,C_word *av) C_noret;
C_noret_decl(f_6683)
static void C_fcall f_6683(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6693)
static void C_ccall f_6693(C_word c,C_word *av) C_noret;
C_noret_decl(f_6707)
static void C_ccall f_6707(C_word c,C_word *av) C_noret;
C_noret_decl(f_6713)
static void C_ccall f_6713(C_word c,C_word *av) C_noret;
C_noret_decl(f_6726)
static void C_ccall f_6726(C_word c,C_word *av) C_noret;
C_noret_decl(f_6732)
static void C_ccall f_6732(C_word c,C_word *av) C_noret;
C_noret_decl(f_6735)
static void C_ccall f_6735(C_word c,C_word *av) C_noret;
C_noret_decl(f_6738)
static void C_ccall f_6738(C_word c,C_word *av) C_noret;
C_noret_decl(f_6742)
static void C_ccall f_6742(C_word c,C_word *av) C_noret;
C_noret_decl(f_6749)
static void C_ccall f_6749(C_word c,C_word *av) C_noret;
C_noret_decl(f_6751)
static void C_fcall f_6751(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6776)
static void C_ccall f_6776(C_word c,C_word *av) C_noret;
C_noret_decl(f_6793)
static void C_ccall f_6793(C_word c,C_word *av) C_noret;
C_noret_decl(f_6799)
static void C_ccall f_6799(C_word c,C_word *av) C_noret;
C_noret_decl(f_6802)
static void C_ccall f_6802(C_word c,C_word *av) C_noret;
C_noret_decl(f_6805)
static void C_ccall f_6805(C_word c,C_word *av) C_noret;
C_noret_decl(f_6808)
static void C_ccall f_6808(C_word c,C_word *av) C_noret;
C_noret_decl(f_6812)
static void C_ccall f_6812(C_word c,C_word *av) C_noret;
C_noret_decl(f_6822)
static void C_ccall f_6822(C_word c,C_word *av) C_noret;
C_noret_decl(f_6824)
static void C_fcall f_6824(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6849)
static void C_ccall f_6849(C_word c,C_word *av) C_noret;
C_noret_decl(f_6867)
static void C_ccall f_6867(C_word c,C_word *av) C_noret;
C_noret_decl(f_6883)
static void C_ccall f_6883(C_word c,C_word *av) C_noret;
C_noret_decl(f_6902)
static void C_ccall f_6902(C_word c,C_word *av) C_noret;
C_noret_decl(f_6904)
static void C_fcall f_6904(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_6929)
static void C_ccall f_6929(C_word c,C_word *av) C_noret;
C_noret_decl(f_6961)
static void C_ccall f_6961(C_word c,C_word *av) C_noret;
C_noret_decl(f_6976)
static void C_ccall f_6976(C_word c,C_word *av) C_noret;
C_noret_decl(f_6980)
static void C_fcall f_6980(C_word t0,C_word t1) C_noret;
C_noret_decl(f_6984)
static void C_ccall f_6984(C_word c,C_word *av) C_noret;
C_noret_decl(f_7010)
static void C_fcall f_7010(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7044)
static void C_fcall f_7044(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7078)
static void C_fcall f_7078(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7103)
static void C_ccall f_7103(C_word c,C_word *av) C_noret;
C_noret_decl(f_7128)
static void C_ccall f_7128(C_word c,C_word *av) C_noret;
C_noret_decl(f_7135)
static void C_ccall f_7135(C_word c,C_word *av) C_noret;
C_noret_decl(f_7145)
static void C_ccall f_7145(C_word c,C_word *av) C_noret;
C_noret_decl(f_7147)
static void C_fcall f_7147(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7172)
static void C_ccall f_7172(C_word c,C_word *av) C_noret;
C_noret_decl(f_7182)
static void C_ccall f_7182(C_word c,C_word *av) C_noret;
C_noret_decl(f_7186)
static void C_ccall f_7186(C_word c,C_word *av) C_noret;
C_noret_decl(f_7191)
static void C_fcall f_7191(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7202)
static void C_ccall f_7202(C_word c,C_word *av) C_noret;
C_noret_decl(f_7212)
static void C_ccall f_7212(C_word c,C_word *av) C_noret;
C_noret_decl(f_7216)
static void C_ccall f_7216(C_word c,C_word *av) C_noret;
C_noret_decl(f_7226)
static void C_ccall f_7226(C_word c,C_word *av) C_noret;
C_noret_decl(f_7228)
static void C_fcall f_7228(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7253)
static void C_ccall f_7253(C_word c,C_word *av) C_noret;
C_noret_decl(f_7262)
static void C_fcall f_7262(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7287)
static void C_ccall f_7287(C_word c,C_word *av) C_noret;
C_noret_decl(f_7300)
static void C_ccall f_7300(C_word c,C_word *av) C_noret;
C_noret_decl(f_7303)
static void C_ccall f_7303(C_word c,C_word *av) C_noret;
C_noret_decl(f_7310)
static void C_ccall f_7310(C_word c,C_word *av) C_noret;
C_noret_decl(f_7315)
static void C_ccall f_7315(C_word c,C_word *av) C_noret;
C_noret_decl(f_7321)
static void C_fcall f_7321(C_word t0,C_word t1) C_noret;
C_noret_decl(f_7325)
static void C_ccall f_7325(C_word c,C_word *av) C_noret;
C_noret_decl(f_7343)
static void C_ccall f_7343(C_word c,C_word *av) C_noret;
C_noret_decl(f_7350)
static void C_ccall f_7350(C_word c,C_word *av) C_noret;
C_noret_decl(f_7358)
static void C_ccall f_7358(C_word c,C_word *av) C_noret;
C_noret_decl(f_7376)
static void C_ccall f_7376(C_word c,C_word *av) C_noret;
C_noret_decl(f_7382)
static void C_ccall f_7382(C_word c,C_word *av) C_noret;
C_noret_decl(f_7431)
static void C_ccall f_7431(C_word c,C_word *av) C_noret;
C_noret_decl(f_7438)
static void C_ccall f_7438(C_word c,C_word *av) C_noret;
C_noret_decl(f_7454)
static void C_ccall f_7454(C_word c,C_word *av) C_noret;
C_noret_decl(f_7457)
static void C_ccall f_7457(C_word c,C_word *av) C_noret;
C_noret_decl(f_7463)
static void C_ccall f_7463(C_word c,C_word *av) C_noret;
C_noret_decl(f_7465)
static void C_fcall f_7465(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7514)
static void C_ccall f_7514(C_word c,C_word *av) C_noret;
C_noret_decl(f_7521)
static void C_ccall f_7521(C_word c,C_word *av) C_noret;
C_noret_decl(f_7526)
static void C_fcall f_7526(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7551)
static void C_ccall f_7551(C_word c,C_word *av) C_noret;
C_noret_decl(f_7562)
static void C_ccall f_7562(C_word c,C_word *av) C_noret;
C_noret_decl(f_7564)
static void C_fcall f_7564(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7574)
static void C_ccall f_7574(C_word c,C_word *av) C_noret;
C_noret_decl(f_7587)
static void C_fcall f_7587(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7597)
static void C_ccall f_7597(C_word c,C_word *av) C_noret;
C_noret_decl(f_7610)
static void C_ccall f_7610(C_word c,C_word *av) C_noret;
C_noret_decl(f_7618)
static void C_ccall f_7618(C_word c,C_word *av) C_noret;
C_noret_decl(f_7620)
static void C_fcall f_7620(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7630)
static void C_ccall f_7630(C_word c,C_word *av) C_noret;
C_noret_decl(f_7643)
static void C_ccall f_7643(C_word c,C_word *av) C_noret;
C_noret_decl(f_7651)
static void C_ccall f_7651(C_word c,C_word *av) C_noret;
C_noret_decl(f_7664)
static void C_ccall f_7664(C_word c,C_word *av) C_noret;
C_noret_decl(f_7673)
static void C_ccall f_7673(C_word c,C_word *av) C_noret;
C_noret_decl(f_7678)
static void C_ccall f_7678(C_word c,C_word *av) C_noret;
C_noret_decl(f_7689)
static void C_fcall f_7689(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7699)
static void C_ccall f_7699(C_word c,C_word *av) C_noret;
C_noret_decl(f_7712)
static void C_fcall f_7712(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7722)
static void C_ccall f_7722(C_word c,C_word *av) C_noret;
C_noret_decl(f_7767)
static void C_ccall f_7767(C_word c,C_word *av) C_noret;
C_noret_decl(f_7773)
static void C_ccall f_7773(C_word c,C_word *av) C_noret;
C_noret_decl(f_7775)
static void C_fcall f_7775(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_7800)
static void C_ccall f_7800(C_word c,C_word *av) C_noret;
C_noret_decl(f_7812)
static void C_ccall f_7812(C_word c,C_word *av) C_noret;
C_noret_decl(f_7815)
static void C_ccall f_7815(C_word c,C_word *av) C_noret;
C_noret_decl(f_7818)
static void C_ccall f_7818(C_word c,C_word *av) C_noret;
C_noret_decl(f_7821)
static void C_ccall f_7821(C_word c,C_word *av) C_noret;
C_noret_decl(f_7829)
static void C_ccall f_7829(C_word c,C_word *av) C_noret;
C_noret_decl(f_7837)
static void C_ccall f_7837(C_word c,C_word *av) C_noret;
C_noret_decl(f_7843)
static void C_ccall f_7843(C_word c,C_word *av) C_noret;
C_noret_decl(f_7876)
static void C_ccall f_7876(C_word c,C_word *av) C_noret;
C_noret_decl(f_7879)
static void C_ccall f_7879(C_word c,C_word *av) C_noret;
C_noret_decl(f_7886)
static void C_ccall f_7886(C_word c,C_word *av) C_noret;
C_noret_decl(f_7889)
static void C_ccall f_7889(C_word c,C_word *av) C_noret;
C_noret_decl(f_7892)
static void C_ccall f_7892(C_word c,C_word *av) C_noret;
C_noret_decl(f_7899)
static void C_ccall f_7899(C_word c,C_word *av) C_noret;
C_noret_decl(f_7902)
static void C_ccall f_7902(C_word c,C_word *av) C_noret;
C_noret_decl(f_7905)
static void C_ccall f_7905(C_word c,C_word *av) C_noret;
C_noret_decl(f_7912)
static void C_ccall f_7912(C_word c,C_word *av) C_noret;
C_noret_decl(f_7918)
static void C_ccall f_7918(C_word c,C_word *av) C_noret;
C_noret_decl(f_7922)
static void C_ccall f_7922(C_word c,C_word *av) C_noret;
C_noret_decl(f_7954)
static void C_ccall f_7954(C_word c,C_word *av) C_noret;
C_noret_decl(f_8001)
static void C_fcall f_8001(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8039)
static void C_ccall f_8039(C_word c,C_word *av) C_noret;
C_noret_decl(f_8044)
static void C_ccall f_8044(C_word c,C_word *av) C_noret;
C_noret_decl(f_8060)
static void C_ccall f_8060(C_word c,C_word *av) C_noret;
C_noret_decl(f_8065)
static void C_fcall f_8065(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8090)
static void C_ccall f_8090(C_word c,C_word *av) C_noret;
C_noret_decl(f_8101)
static void C_ccall f_8101(C_word c,C_word *av) C_noret;
C_noret_decl(f_8115)
static void C_ccall f_8115(C_word c,C_word *av) C_noret;
C_noret_decl(f_8119)
static void C_ccall f_8119(C_word c,C_word *av) C_noret;
C_noret_decl(f_8136)
static void C_fcall f_8136(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8161)
static void C_ccall f_8161(C_word c,C_word *av) C_noret;
C_noret_decl(f_8172)
static void C_ccall f_8172(C_word c,C_word *av) C_noret;
C_noret_decl(f_8176)
static void C_fcall f_8176(C_word t0,C_word t1,C_word t2) C_noret;
C_noret_decl(f_8180)
static void C_ccall f_8180(C_word c,C_word *av) C_noret;
C_noret_decl(f_8204)
static void C_ccall f_8204(C_word c,C_word *av) C_noret;
C_noret_decl(f_8215)
static void C_fcall f_8215(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8234)
static void C_ccall f_8234(C_word c,C_word *av) C_noret;
C_noret_decl(f_8242)
static void C_fcall f_8242(C_word t0,C_word t1) C_noret;
C_noret_decl(f_8249)
static void C_fcall f_8249(C_word t0,C_word t1) C_noret;
C_noret_decl(C_batch_2ddriver_toplevel)
C_externexport void C_ccall C_batch_2ddriver_toplevel(C_word c,C_word *av) C_noret;

C_noret_decl(trf_2971)
static void C_ccall trf_2971(C_word c,C_word *av) C_noret;
static void C_ccall trf_2971(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2971(t0,t1,t2,t3);}

C_noret_decl(trf_2986)
static void C_ccall trf_2986(C_word c,C_word *av) C_noret;
static void C_ccall trf_2986(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_2986(t0,t1,t2);}

C_noret_decl(trf_2994)
static void C_ccall trf_2994(C_word c,C_word *av) C_noret;
static void C_ccall trf_2994(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_2994(t0,t1,t2,t3);}

C_noret_decl(trf_3026)
static void C_ccall trf_3026(C_word c,C_word *av) C_noret;
static void C_ccall trf_3026(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3026(t0,t1,t2);}

C_noret_decl(trf_3058)
static void C_ccall trf_3058(C_word c,C_word *av) C_noret;
static void C_ccall trf_3058(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3058(t0,t1,t2);}

C_noret_decl(trf_3107)
static void C_ccall trf_3107(C_word c,C_word *av) C_noret;
static void C_ccall trf_3107(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3107(t0,t1,t2);}

C_noret_decl(trf_3233)
static void C_ccall trf_3233(C_word c,C_word *av) C_noret;
static void C_ccall trf_3233(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_3233(t0,t1);}

C_noret_decl(trf_3239)
static void C_ccall trf_3239(C_word c,C_word *av) C_noret;
static void C_ccall trf_3239(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3239(t0,t1,t2);}

C_noret_decl(trf_3267)
static void C_ccall trf_3267(C_word c,C_word *av) C_noret;
static void C_ccall trf_3267(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3267(t0,t1,t2);}

C_noret_decl(trf_3381)
static void C_ccall trf_3381(C_word c,C_word *av) C_noret;
static void C_ccall trf_3381(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3381(t0,t1,t2);}

C_noret_decl(trf_3390)
static void C_ccall trf_3390(C_word c,C_word *av) C_noret;
static void C_ccall trf_3390(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_3390(t0,t1,t2);}

C_noret_decl(trf_3398)
static void C_ccall trf_3398(C_word c,C_word *av) C_noret;
static void C_ccall trf_3398(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_3398(t0,t1,t2,t3);}

C_noret_decl(trf_4149)
static void C_ccall trf_4149(C_word c,C_word *av) C_noret;
static void C_ccall trf_4149(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4149(t0,t1);}

C_noret_decl(trf_4189)
static void C_ccall trf_4189(C_word c,C_word *av) C_noret;
static void C_ccall trf_4189(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4189(t0,t1,t2);}

C_noret_decl(trf_4212)
static void C_ccall trf_4212(C_word c,C_word *av) C_noret;
static void C_ccall trf_4212(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4212(t0,t1,t2);}

C_noret_decl(trf_4235)
static void C_ccall trf_4235(C_word c,C_word *av) C_noret;
static void C_ccall trf_4235(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4235(t0,t1,t2);}

C_noret_decl(trf_4258)
static void C_ccall trf_4258(C_word c,C_word *av) C_noret;
static void C_ccall trf_4258(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4258(t0,t1,t2);}

C_noret_decl(trf_4262)
static void C_ccall trf_4262(C_word c,C_word *av) C_noret;
static void C_ccall trf_4262(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4262(t0,t1);}

C_noret_decl(trf_4332)
static void C_ccall trf_4332(C_word c,C_word *av) C_noret;
static void C_ccall trf_4332(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4332(t0,t1,t2);}

C_noret_decl(trf_4468)
static void C_ccall trf_4468(C_word c,C_word *av) C_noret;
static void C_ccall trf_4468(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_4468(t0,t1,t2);}

C_noret_decl(trf_4492)
static void C_ccall trf_4492(C_word c,C_word *av) C_noret;
static void C_ccall trf_4492(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4492(t0,t1);}

C_noret_decl(trf_4571)
static void C_ccall trf_4571(C_word c,C_word *av) C_noret;
static void C_ccall trf_4571(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4571(t0,t1);}

C_noret_decl(trf_4890)
static void C_ccall trf_4890(C_word c,C_word *av) C_noret;
static void C_ccall trf_4890(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4890(t0,t1);}

C_noret_decl(trf_4930)
static void C_ccall trf_4930(C_word c,C_word *av) C_noret;
static void C_ccall trf_4930(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4930(t0,t1);}

C_noret_decl(trf_4933)
static void C_ccall trf_4933(C_word c,C_word *av) C_noret;
static void C_ccall trf_4933(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4933(t0,t1);}

C_noret_decl(trf_4970)
static void C_ccall trf_4970(C_word c,C_word *av) C_noret;
static void C_ccall trf_4970(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_4970(t0,t1);}

C_noret_decl(trf_5013)
static void C_ccall trf_5013(C_word c,C_word *av) C_noret;
static void C_ccall trf_5013(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5013(t0,t1,t2);}

C_noret_decl(trf_5040)
static void C_ccall trf_5040(C_word c,C_word *av) C_noret;
static void C_ccall trf_5040(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5040(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5062)
static void C_ccall trf_5062(C_word c,C_word *av) C_noret;
static void C_ccall trf_5062(C_word c,C_word *av){
C_word t0=av[5];
C_word t1=av[4];
C_word t2=av[3];
C_word t3=av[2];
C_word t4=av[1];
C_word t5=av[0];
f_5062(t0,t1,t2,t3,t4,t5);}

C_noret_decl(trf_5089)
static void C_ccall trf_5089(C_word c,C_word *av) C_noret;
static void C_ccall trf_5089(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5089(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5112)
static void C_ccall trf_5112(C_word c,C_word *av) C_noret;
static void C_ccall trf_5112(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5112(t0,t1,t2);}

C_noret_decl(trf_5135)
static void C_ccall trf_5135(C_word c,C_word *av) C_noret;
static void C_ccall trf_5135(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5135(t0,t1);}

C_noret_decl(trf_5220)
static void C_ccall trf_5220(C_word c,C_word *av) C_noret;
static void C_ccall trf_5220(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5220(t0,t1,t2);}

C_noret_decl(trf_5226)
static void C_ccall trf_5226(C_word c,C_word *av) C_noret;
static void C_ccall trf_5226(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5226(t0,t1,t2);}

C_noret_decl(trf_5234)
static void C_ccall trf_5234(C_word c,C_word *av) C_noret;
static void C_ccall trf_5234(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5234(t0,t1,t2);}

C_noret_decl(trf_5255)
static void C_ccall trf_5255(C_word c,C_word *av) C_noret;
static void C_ccall trf_5255(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5255(t0,t1);}

C_noret_decl(trf_5265)
static void C_ccall trf_5265(C_word c,C_word *av) C_noret;
static void C_ccall trf_5265(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5265(t0,t1,t2);}

C_noret_decl(trf_5305)
static void C_ccall trf_5305(C_word c,C_word *av) C_noret;
static void C_ccall trf_5305(C_word c,C_word *av){
C_word t0=av[4];
C_word t1=av[3];
C_word t2=av[2];
C_word t3=av[1];
C_word t4=av[0];
f_5305(t0,t1,t2,t3,t4);}

C_noret_decl(trf_5307)
static void C_ccall trf_5307(C_word c,C_word *av) C_noret;
static void C_ccall trf_5307(C_word c,C_word *av){
C_word t0=av[3];
C_word t1=av[2];
C_word t2=av[1];
C_word t3=av[0];
f_5307(t0,t1,t2,t3);}

C_noret_decl(trf_5330)
static void C_ccall trf_5330(C_word c,C_word *av) C_noret;
static void C_ccall trf_5330(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5330(t0,t1,t2);}

C_noret_decl(trf_5335)
static void C_ccall trf_5335(C_word c,C_word *av) C_noret;
static void C_ccall trf_5335(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5335(t0,t1);}

C_noret_decl(trf_5371)
static void C_ccall trf_5371(C_word c,C_word *av) C_noret;
static void C_ccall trf_5371(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5371(t0,t1);}

C_noret_decl(trf_5374)
static void C_ccall trf_5374(C_word c,C_word *av) C_noret;
static void C_ccall trf_5374(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5374(t0,t1);}

C_noret_decl(trf_5422)
static void C_ccall trf_5422(C_word c,C_word *av) C_noret;
static void C_ccall trf_5422(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5422(t0,t1);}

C_noret_decl(trf_5425)
static void C_ccall trf_5425(C_word c,C_word *av) C_noret;
static void C_ccall trf_5425(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5425(t0,t1);}

C_noret_decl(trf_5431)
static void C_ccall trf_5431(C_word c,C_word *av) C_noret;
static void C_ccall trf_5431(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5431(t0,t1);}

C_noret_decl(trf_5434)
static void C_ccall trf_5434(C_word c,C_word *av) C_noret;
static void C_ccall trf_5434(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5434(t0,t1);}

C_noret_decl(trf_5437)
static void C_ccall trf_5437(C_word c,C_word *av) C_noret;
static void C_ccall trf_5437(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5437(t0,t1);}

C_noret_decl(trf_5440)
static void C_ccall trf_5440(C_word c,C_word *av) C_noret;
static void C_ccall trf_5440(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5440(t0,t1);}

C_noret_decl(trf_5443)
static void C_ccall trf_5443(C_word c,C_word *av) C_noret;
static void C_ccall trf_5443(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5443(t0,t1);}

C_noret_decl(trf_5446)
static void C_ccall trf_5446(C_word c,C_word *av) C_noret;
static void C_ccall trf_5446(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5446(t0,t1);}

C_noret_decl(trf_5449)
static void C_ccall trf_5449(C_word c,C_word *av) C_noret;
static void C_ccall trf_5449(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5449(t0,t1);}

C_noret_decl(trf_5452)
static void C_ccall trf_5452(C_word c,C_word *av) C_noret;
static void C_ccall trf_5452(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5452(t0,t1);}

C_noret_decl(trf_5455)
static void C_ccall trf_5455(C_word c,C_word *av) C_noret;
static void C_ccall trf_5455(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5455(t0,t1);}

C_noret_decl(trf_5458)
static void C_ccall trf_5458(C_word c,C_word *av) C_noret;
static void C_ccall trf_5458(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5458(t0,t1);}

C_noret_decl(trf_5461)
static void C_ccall trf_5461(C_word c,C_word *av) C_noret;
static void C_ccall trf_5461(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5461(t0,t1);}

C_noret_decl(trf_5464)
static void C_ccall trf_5464(C_word c,C_word *av) C_noret;
static void C_ccall trf_5464(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5464(t0,t1);}

C_noret_decl(trf_5467)
static void C_ccall trf_5467(C_word c,C_word *av) C_noret;
static void C_ccall trf_5467(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5467(t0,t1);}

C_noret_decl(trf_5470)
static void C_ccall trf_5470(C_word c,C_word *av) C_noret;
static void C_ccall trf_5470(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5470(t0,t1);}

C_noret_decl(trf_5473)
static void C_ccall trf_5473(C_word c,C_word *av) C_noret;
static void C_ccall trf_5473(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5473(t0,t1);}

C_noret_decl(trf_5476)
static void C_ccall trf_5476(C_word c,C_word *av) C_noret;
static void C_ccall trf_5476(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5476(t0,t1);}

C_noret_decl(trf_5479)
static void C_ccall trf_5479(C_word c,C_word *av) C_noret;
static void C_ccall trf_5479(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5479(t0,t1);}

C_noret_decl(trf_5482)
static void C_ccall trf_5482(C_word c,C_word *av) C_noret;
static void C_ccall trf_5482(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5482(t0,t1);}

C_noret_decl(trf_5487)
static void C_ccall trf_5487(C_word c,C_word *av) C_noret;
static void C_ccall trf_5487(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5487(t0,t1);}

C_noret_decl(trf_5492)
static void C_ccall trf_5492(C_word c,C_word *av) C_noret;
static void C_ccall trf_5492(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5492(t0,t1);}

C_noret_decl(trf_5497)
static void C_ccall trf_5497(C_word c,C_word *av) C_noret;
static void C_ccall trf_5497(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5497(t0,t1);}

C_noret_decl(trf_5502)
static void C_ccall trf_5502(C_word c,C_word *av) C_noret;
static void C_ccall trf_5502(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5502(t0,t1);}

C_noret_decl(trf_5507)
static void C_ccall trf_5507(C_word c,C_word *av) C_noret;
static void C_ccall trf_5507(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5507(t0,t1);}

C_noret_decl(trf_5534)
static void C_ccall trf_5534(C_word c,C_word *av) C_noret;
static void C_ccall trf_5534(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5534(t0,t1);}

C_noret_decl(trf_5537)
static void C_ccall trf_5537(C_word c,C_word *av) C_noret;
static void C_ccall trf_5537(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5537(t0,t1);}

C_noret_decl(trf_5540)
static void C_ccall trf_5540(C_word c,C_word *av) C_noret;
static void C_ccall trf_5540(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5540(t0,t1);}

C_noret_decl(trf_5543)
static void C_ccall trf_5543(C_word c,C_word *av) C_noret;
static void C_ccall trf_5543(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5543(t0,t1);}

C_noret_decl(trf_5546)
static void C_ccall trf_5546(C_word c,C_word *av) C_noret;
static void C_ccall trf_5546(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5546(t0,t1);}

C_noret_decl(trf_5621)
static void C_ccall trf_5621(C_word c,C_word *av) C_noret;
static void C_ccall trf_5621(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5621(t0,t1);}

C_noret_decl(trf_5628)
static void C_ccall trf_5628(C_word c,C_word *av) C_noret;
static void C_ccall trf_5628(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5628(t0,t1);}

C_noret_decl(trf_5653)
static void C_ccall trf_5653(C_word c,C_word *av) C_noret;
static void C_ccall trf_5653(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5653(t0,t1);}

C_noret_decl(trf_5681)
static void C_ccall trf_5681(C_word c,C_word *av) C_noret;
static void C_ccall trf_5681(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5681(t0,t1);}

C_noret_decl(trf_5761)
static void C_ccall trf_5761(C_word c,C_word *av) C_noret;
static void C_ccall trf_5761(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5761(t0,t1);}

C_noret_decl(trf_5771)
static void C_ccall trf_5771(C_word c,C_word *av) C_noret;
static void C_ccall trf_5771(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_5771(t0,t1,t2);}

C_noret_decl(trf_5797)
static void C_ccall trf_5797(C_word c,C_word *av) C_noret;
static void C_ccall trf_5797(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5797(t0,t1);}

C_noret_decl(trf_5871)
static void C_ccall trf_5871(C_word c,C_word *av) C_noret;
static void C_ccall trf_5871(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_5871(t0,t1);}

C_noret_decl(trf_5894)
static void C_ccall trf_5894(C_word c,C_word *av) C_noret;
static void C_ccall trf_5894(C_word c,C_word *av){
C_word t0=av[6];
C_word t1=av[5];
C_word t2=av[4];
C_word t3=av[3];
C_word t4=av[2];
C_word t5=av[1];
C_word t6=av[0];
f_5894(t0,t1,t2,t3,t4,t5,t6);}

C_noret_decl(trf_6166)
static void C_ccall trf_6166(C_word c,C_word *av) C_noret;
static void C_ccall trf_6166(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6166(t0,t1,t2);}

C_noret_decl(trf_6362)
static void C_ccall trf_6362(C_word c,C_word *av) C_noret;
static void C_ccall trf_6362(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6362(t0,t1,t2);}

C_noret_decl(trf_6385)
static void C_ccall trf_6385(C_word c,C_word *av) C_noret;
static void C_ccall trf_6385(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6385(t0,t1,t2);}

C_noret_decl(trf_6437)
static void C_ccall trf_6437(C_word c,C_word *av) C_noret;
static void C_ccall trf_6437(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6437(t0,t1,t2);}

C_noret_decl(trf_6484)
static void C_ccall trf_6484(C_word c,C_word *av) C_noret;
static void C_ccall trf_6484(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6484(t0,t1,t2);}

C_noret_decl(trf_6523)
static void C_ccall trf_6523(C_word c,C_word *av) C_noret;
static void C_ccall trf_6523(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6523(t0,t1,t2);}

C_noret_decl(trf_6588)
static void C_ccall trf_6588(C_word c,C_word *av) C_noret;
static void C_ccall trf_6588(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6588(t0,t1,t2);}

C_noret_decl(trf_6683)
static void C_ccall trf_6683(C_word c,C_word *av) C_noret;
static void C_ccall trf_6683(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6683(t0,t1,t2);}

C_noret_decl(trf_6751)
static void C_ccall trf_6751(C_word c,C_word *av) C_noret;
static void C_ccall trf_6751(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6751(t0,t1,t2);}

C_noret_decl(trf_6824)
static void C_ccall trf_6824(C_word c,C_word *av) C_noret;
static void C_ccall trf_6824(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6824(t0,t1,t2);}

C_noret_decl(trf_6904)
static void C_ccall trf_6904(C_word c,C_word *av) C_noret;
static void C_ccall trf_6904(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_6904(t0,t1,t2);}

C_noret_decl(trf_6980)
static void C_ccall trf_6980(C_word c,C_word *av) C_noret;
static void C_ccall trf_6980(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_6980(t0,t1);}

C_noret_decl(trf_7010)
static void C_ccall trf_7010(C_word c,C_word *av) C_noret;
static void C_ccall trf_7010(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7010(t0,t1,t2);}

C_noret_decl(trf_7044)
static void C_ccall trf_7044(C_word c,C_word *av) C_noret;
static void C_ccall trf_7044(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7044(t0,t1,t2);}

C_noret_decl(trf_7078)
static void C_ccall trf_7078(C_word c,C_word *av) C_noret;
static void C_ccall trf_7078(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7078(t0,t1,t2);}

C_noret_decl(trf_7147)
static void C_ccall trf_7147(C_word c,C_word *av) C_noret;
static void C_ccall trf_7147(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7147(t0,t1,t2);}

C_noret_decl(trf_7191)
static void C_ccall trf_7191(C_word c,C_word *av) C_noret;
static void C_ccall trf_7191(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7191(t0,t1,t2);}

C_noret_decl(trf_7228)
static void C_ccall trf_7228(C_word c,C_word *av) C_noret;
static void C_ccall trf_7228(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7228(t0,t1,t2);}

C_noret_decl(trf_7262)
static void C_ccall trf_7262(C_word c,C_word *av) C_noret;
static void C_ccall trf_7262(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7262(t0,t1,t2);}

C_noret_decl(trf_7321)
static void C_ccall trf_7321(C_word c,C_word *av) C_noret;
static void C_ccall trf_7321(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_7321(t0,t1);}

C_noret_decl(trf_7465)
static void C_ccall trf_7465(C_word c,C_word *av) C_noret;
static void C_ccall trf_7465(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7465(t0,t1,t2);}

C_noret_decl(trf_7526)
static void C_ccall trf_7526(C_word c,C_word *av) C_noret;
static void C_ccall trf_7526(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7526(t0,t1,t2);}

C_noret_decl(trf_7564)
static void C_ccall trf_7564(C_word c,C_word *av) C_noret;
static void C_ccall trf_7564(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7564(t0,t1,t2);}

C_noret_decl(trf_7587)
static void C_ccall trf_7587(C_word c,C_word *av) C_noret;
static void C_ccall trf_7587(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7587(t0,t1,t2);}

C_noret_decl(trf_7620)
static void C_ccall trf_7620(C_word c,C_word *av) C_noret;
static void C_ccall trf_7620(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7620(t0,t1,t2);}

C_noret_decl(trf_7689)
static void C_ccall trf_7689(C_word c,C_word *av) C_noret;
static void C_ccall trf_7689(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7689(t0,t1,t2);}

C_noret_decl(trf_7712)
static void C_ccall trf_7712(C_word c,C_word *av) C_noret;
static void C_ccall trf_7712(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7712(t0,t1,t2);}

C_noret_decl(trf_7775)
static void C_ccall trf_7775(C_word c,C_word *av) C_noret;
static void C_ccall trf_7775(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_7775(t0,t1,t2);}

C_noret_decl(trf_8001)
static void C_ccall trf_8001(C_word c,C_word *av) C_noret;
static void C_ccall trf_8001(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8001(t0,t1,t2);}

C_noret_decl(trf_8065)
static void C_ccall trf_8065(C_word c,C_word *av) C_noret;
static void C_ccall trf_8065(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8065(t0,t1,t2);}

C_noret_decl(trf_8136)
static void C_ccall trf_8136(C_word c,C_word *av) C_noret;
static void C_ccall trf_8136(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8136(t0,t1,t2);}

C_noret_decl(trf_8176)
static void C_ccall trf_8176(C_word c,C_word *av) C_noret;
static void C_ccall trf_8176(C_word c,C_word *av){
C_word t0=av[2];
C_word t1=av[1];
C_word t2=av[0];
f_8176(t0,t1,t2);}

C_noret_decl(trf_8215)
static void C_ccall trf_8215(C_word c,C_word *av) C_noret;
static void C_ccall trf_8215(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8215(t0,t1);}

C_noret_decl(trf_8242)
static void C_ccall trf_8242(C_word c,C_word *av) C_noret;
static void C_ccall trf_8242(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8242(t0,t1);}

C_noret_decl(trf_8249)
static void C_ccall trf_8249(C_word c,C_word *av) C_noret;
static void C_ccall trf_8249(C_word c,C_word *av){
C_word t0=av[1];
C_word t1=av[0];
f_8249(t0,t1);}

/* f8805 in k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in ... */
static void C_ccall f8805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f8805,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9314,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t2;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[185];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}

/* f9308 in k6092 in k6089 in k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in ... */
static void C_ccall f9308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9308,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9314 */
static void C_ccall f9314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9314,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9320 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in ... */
static void C_ccall f9320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9320,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9326 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in ... */
static void C_ccall f9326(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9326,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9334 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f9334(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9334,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9340 in k6242 in k6239 in k6236 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f9340(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9340,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9352 in for-each-loop2029 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_ccall f9352(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9352,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9360 in k6461 in for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in ... */
static void C_ccall f9360(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9360,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9372 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in ... */
static void C_ccall f9372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9372,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9396 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f9396(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9396,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9402 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in ... */
static void C_ccall f9402(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9402,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9416 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in ... */
static void C_ccall f9416(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9416,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9422 in k7380 in k7374 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in ... */
static void C_ccall f9422(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9422,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9428 in k7380 in k7374 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in ... */
static void C_ccall f9428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9428,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9434 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in ... */
static void C_ccall f9434(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9434,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9440 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in ... */
static void C_ccall f9440(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9440,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9454 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in ... */
static void C_ccall f9454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9454,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9470 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in ... */
static void C_ccall f9470(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9470,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9476 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in ... */
static void C_ccall f9476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9476,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9482 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in ... */
static void C_ccall f9482(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9482,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9488 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in ... */
static void C_ccall f9488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9488,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* f9494 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in ... */
static void C_ccall f9494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f9494,c,av);}
/* batch-driver.scm:260: chicken.compiler.support#debugging */
t2=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[106];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k2723 */
static void C_ccall f_2725(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2725,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2728,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_eval_toplevel(2,av2);}}

/* k2726 in k2723 */
static void C_ccall f_2728(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2728,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2731,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_expand_toplevel(2,av2);}}

/* k2729 in k2726 in k2723 */
static void C_ccall f_2731(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2731,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2734,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_extras_toplevel(2,av2);}}

/* k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2734(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2734,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2737,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_data_2dstructures_toplevel(2,av2);}}

/* k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2737(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2737,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2740,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_pathname_toplevel(2,av2);}}

/* k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2740,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2743,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_support_toplevel(2,av2);}}

/* k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2743,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2746,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_compiler_2dsyntax_toplevel(2,av2);}}

/* k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2746,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2749,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_compiler_toplevel(2,av2);}}

/* k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2749,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2752,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_optimizer_toplevel(2,av2);}}

/* k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2752,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2755,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_internal_toplevel(2,av2);}}

/* k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2755,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2758,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_scrutinizer_toplevel(2,av2);}}

/* k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2758,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2761,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_lfa2_toplevel(2,av2);}}

/* k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2761(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2761,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2764,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_c_2dplatform_toplevel(2,av2);}}

/* k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2764,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2767,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_c_2dbackend_toplevel(2,av2);}}

/* k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_2767,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2770,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_user_2dpass_toplevel(2,av2);}}

/* k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_2770(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,5)))){
C_save_and_reclaim((void *)f_2770,c,av);}
a=C_alloc(34);
t2=C_a_i_provide(&a,1,lf[0]);
t3=C_a_i_provide(&a,1,lf[1]);
t4=C_mutate(&lf[2] /* (set! chicken.compiler.batch-driver#append-map ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_2971,tmp=(C_word)a,a+=2,tmp));
t5=C_mutate(&lf[6] /* (set! chicken.compiler.batch-driver#concatenate ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3233,tmp=(C_word)a,a+=2,tmp));
t6=C_mutate(&lf[7] /* (set! chicken.compiler.batch-driver#filter ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3381,tmp=(C_word)a,a+=2,tmp));
t7=C_SCHEME_TRUE;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=C_mutate(&lf[8] /* (set! chicken.compiler.batch-driver#initialize-analysis-database ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4149,a[2]=t8,tmp=(C_word)a,a+=3,tmp));
t10=C_SCHEME_FALSE;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_mutate(&lf[18] /* (set! chicken.compiler.batch-driver#display-analysis-database ...) */,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4258,a[2]=t11,tmp=(C_word)a,a+=3,tmp));
t13=C_mutate((C_word*)lf[78]+1 /* (set! chicken.compiler.batch-driver#compile-source-file ...) */,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4887,tmp=(C_word)a,a+=2,tmp));
t14=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t14;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t14+1)))(2,av2);}}

/* chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_2971(C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_2971,4,t1,t2,t3,t4);}
a=C_alloc(9);
if(C_truep(C_i_nullp(t4))){
t5=C_i_check_list_2(t3,lf[3]);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_2986,a[2]=t2,a[3]=t7,tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_2986(t9,t1,t3);}
else{
t5=C_a_i_cons(&a,2,t3,t4);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3026,a[2]=t7,a[3]=t2,tmp=(C_word)a,a+=4,tmp));
t9=((C_word*)t7)[1];
f_3026(t9,t1,t5);}}

/* foldr242 in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_2986(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_2986,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2994,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3013,a[2]=t3,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g247 in foldr242 in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_2994(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_2994,4,t0,t1,t2,t3);}
a=C_alloc(4);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3002,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:72: proc */
t5=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3000 in g247 in foldr242 in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3002,c,av);}
/* mini-srfi-1.scm:72: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3011 in foldr242 in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3013(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3013,c,av);}
/* mini-srfi-1.scm:72: g247 */
t2=((C_word*)t0)[2];
f_2994(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3026(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,0,3)))){
C_save_and_reclaim_args((void *)trf_3026,3,t0,t1,t2);}
a=C_alloc(23);
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3181,tmp=(C_word)a,a+=2,tmp);
t4=(
  f_3181(t2)
);
if(C_truep(t4)){
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3040,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=C_i_check_list_2(t2,lf[5]);
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3105,a[2]=t5,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3107,a[2]=t8,a[3]=t13,a[4]=t9,tmp=(C_word)a,a+=5,tmp));
t15=((C_word*)t13)[1];
f_3107(t15,t11,t2);}}

/* k3038 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_3040,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3044,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3056,a[2]=((C_word*)t0)[3],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3058,a[2]=t5,a[3]=t9,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t11=((C_word*)t9)[1];
f_3058(t11,t7,((C_word*)t0)[4]);}

/* k3042 in k3038 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3044,c,av);}
/* mini-srfi-1.scm:76: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k3054 in k3038 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_3056,c,av);}
/* mini-srfi-1.scm:77: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_3026(t2,((C_word*)t0)[3],t1);}

/* map-loop284 in k3038 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3058(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_3058,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k3103 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3105,c,av);}{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
C_apply(4,av2);}}

/* map-loop257 in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3107(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_3107,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_car(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* loop in a3770 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static C_word C_fcall f_3147(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
t2=C_i_nullp(t1);
if(C_truep(t2)){
return(t2);}
else{
t3=(
/* mini-srfi-1.scm:82: pred */
  f_3777(((C_word*)t0)[2],C_i_car(t1))
);
if(C_truep(C_i_not(t3))){
return(C_SCHEME_FALSE);}
else{
t5=C_u_i_cdr(t1);
t1=t5;
goto loop;}}}

/* loop in loop in chicken.compiler.batch-driver#append-map in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static C_word C_fcall f_3181(C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_stack_overflow_check;
loop:{}
if(C_truep(C_i_nullp(t1))){
return(C_SCHEME_FALSE);}
else{
t2=C_i_car(t1);
t3=C_i_nullp(t2);
if(C_truep(t3)){
return(t3);}
else{
t5=C_u_i_cdr(t1);
t1=t5;
goto loop;}}}

/* chicken.compiler.batch-driver#concatenate in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3233(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_3233,2,t1,t2);}
a=C_alloc(5);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3239,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_3239(t6,t1,t2);}

/* loop in chicken.compiler.batch-driver#concatenate in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3239(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3239,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3257,a[2]=t1,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:101: loop */
t6=t4;
t7=C_u_i_cdr(t2);
t1=t6;
t2=t7;
goto loop;}}

/* k3255 in loop in chicken.compiler.batch-driver#concatenate in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3257(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3257,c,av);}
/* mini-srfi-1.scm:101: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* loop in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in ... */
static void C_fcall f_3267(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_3267,3,t0,t1,t2);}
a=C_alloc(4);
if(C_truep(C_i_nullp(t2))){
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=C_i_car(t2);
t4=C_eqp(lf[137],t3);
if(C_truep(t4)){
/* mini-srfi-1.scm:107: loop */
t8=t1;
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}
else{
t5=C_u_i_car(t2);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3294,a[2]=t1,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* mini-srfi-1.scm:109: loop */
t8=t6;
t9=C_u_i_cdr(t2);
t1=t8;
t2=t9;
goto loop;}}}

/* k3292 in loop in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in ... */
static void C_ccall f_3294(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3294,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* chicken.compiler.batch-driver#filter in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3381(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,3)))){
C_save_and_reclaim_args((void *)trf_3381,3,t1,t2,t3);}
a=C_alloc(6);
t4=C_i_check_list_2(t3,lf[3]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_3390,a[2]=t2,a[3]=t6,tmp=(C_word)a,a+=4,tmp));
t8=((C_word*)t6)[1];
f_3390(t8,t1,t3);}

/* foldr389 in chicken.compiler.batch-driver#filter in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3390(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(8,0,4)))){
C_save_and_reclaim_args((void *)trf_3390,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3398,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3419,a[2]=t3,a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
t7=t5;
t8=C_slot(t2,C_fix(1));
t1=t7;
t2=t8;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* g394 in foldr389 in chicken.compiler.batch-driver#filter in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_3398(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_3398,4,t0,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_3405,a[2]=t1,a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* mini-srfi-1.scm:131: pred */
t5=((C_word*)t0)[2];{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k3403 in g394 in foldr389 in chicken.compiler.batch-driver#filter in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3405(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_3405,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=(C_truep(t1)?C_a_i_cons(&a,2,((C_word*)t0)[3],((C_word*)t0)[4]):((C_word*)t0)[4]);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k3417 in foldr389 in chicken.compiler.batch-driver#filter in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_3419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_3419,c,av);}
/* mini-srfi-1.scm:131: g394 */
t2=((C_word*)t0)[2];
f_3398(t2,((C_word*)t0)[3],((C_word*)t0)[4],t1);}

/* a3479 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in ... */
static void C_ccall f_3480(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_3480,c,av);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3488,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=*((C_word*)lf[324]+1);
/* batch-driver.scm:633: g1835 */
t5=*((C_word*)lf[324]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t3;
av2[2]=t2;
av2[3]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}

/* k3486 in a3479 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_3488(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_3488,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_i_not(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a3770 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_3771(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_3771,c,av);}
a=C_alloc(6);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3777,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3147,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=t1;{
C_word *av2=av;
av2[0]=t5;
av2[1]=(
  f_3147(t4,((C_word*)t0)[2])
);
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}

/* a3776 in a3770 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static C_word C_fcall f_3777(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_stack_overflow_check;{}
return(C_i_memq(((C_word*)t0)[2],t1));}

/* a4055 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f_4056(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4056,c,av);}
/* batch-driver.scm:70: chicken.compiler.core#compute-database-statistics */
t2=*((C_word*)lf[224]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f_4062(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7=av[7];
C_word t8=av[8];
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_4062,c,av);}
a=C_alloc(10);
t9=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4069,a[2]=t1,a[3]=t8,a[4]=t7,a[5]=t6,a[6]=t5,a[7]=t4,a[8]=t3,a[9]=t2,tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:71: chicken.compiler.support#debugging */
t10=*((C_word*)lf[105]+1);{
C_word *av2=av;
av2[0]=t10;
av2[1]=t9;
av2[2]=lf[223];
av2[3]=lf[232];
((C_proc)(void*)(*((C_word*)t10+1)))(4,av2);}}

/* k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in ... */
static void C_ccall f_4069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_4069,c,av);}
a=C_alloc(11);
if(C_truep(t1)){
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4075,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t2,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
/* batch-driver.scm:72: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[231];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in ... */
static void C_ccall f_4075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_4075,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4078,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:72: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[10];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_4078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_4078,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4081,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:72: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[230];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_ccall f_4081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_4081,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4084,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:72: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[9];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in ... */
static void C_ccall f_4084(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4084,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4087,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:72: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_4087(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_4087,c,av);}
a=C_alloc(9);
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4093,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=t2,a[8]=((C_word*)t0)[7],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:73: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[229];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in ... */
static void C_ccall f_4093(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_4093,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4096,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:73: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[8];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in ... */
static void C_ccall f_4096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_4096,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4099,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:73: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in ... */
static void C_ccall f_4099(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,4)))){
C_save_and_reclaim((void *)f_4099,c,av);}
a=C_alloc(8);
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4105,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t2,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:74: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[228];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in ... */
static void C_ccall f_4105(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_4105,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4108,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:74: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in ... */
static void C_ccall f_4108(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_4108,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4111,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:74: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in ... */
static void C_ccall f_4111(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,4)))){
C_save_and_reclaim((void *)f_4111,c,av);}
a=C_alloc(7);
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4117,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t2,a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:75: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[227];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in ... */
static void C_ccall f_4117(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4117,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4120,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:75: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in ... */
static void C_ccall f_4120(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4120,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4123,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:75: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in ... */
static void C_ccall f_4123(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_4123,c,av);}
a=C_alloc(6);
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4129,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:76: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[226];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4127 in k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in k5896 in ... */
static void C_ccall f_4129(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4129,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4132,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:76: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4130 in k4127 in k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in k5899 in ... */
static void C_ccall f_4132(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_4132,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4135,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:76: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4133 in k4130 in k4127 in k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in k5902 in ... */
static void C_ccall f_4135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4135,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4141,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:77: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[225];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}

/* k4139 in k4133 in k4130 in k4127 in k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in k5906 in ... */
static void C_ccall f_4141(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_4141,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4144,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:77: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4142 in k4139 in k4133 in k4130 in k4127 in k4121 in k4118 in k4115 in k4109 in k4106 in k4103 in k4097 in k4094 in k4091 in k4085 in k4082 in k4079 in k4076 in k4073 in k4067 in a4061 in k5909 in ... */
static void C_ccall f_4144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_4144,c,av);}
/* batch-driver.scm:77: ##sys#write-char-0 */
t2=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4149(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,3)))){
C_save_and_reclaim_args((void *)trf_4149,2,t0,t1);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4153,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t3=*((C_word*)lf[9]+1);
t4=C_i_check_list_2(*((C_word*)lf[9]+1),lf[10]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4165,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4235,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_4235(t9,t5,*((C_word*)lf[9]+1));}
else{
t3=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4151 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4153(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4153,c,av);}
t2=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4165(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4165,c,av);}
a=C_alloc(8);
t2=*((C_word*)lf[11]+1);
t3=C_i_check_list_2(*((C_word*)lf[11]+1),lf[10]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4176,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4212,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_4212(t8,t4,*((C_word*)lf[11]+1));}

/* k4174 in k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4176(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4176,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[12]+1);
t3=C_i_check_list_2(*((C_word*)lf[12]+1),lf[10]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4189,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_4189(t7,((C_word*)t0)[2],*((C_word*)lf[12]+1));}

/* for-each-loop775 in k4174 in k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4189(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_4189,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4199,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:99: chicken.compiler.support#mark-variable */
t4=*((C_word*)lf[13]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=lf[14];
av2[4]=lf[15];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4197 in for-each-loop775 in k4174 in k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4199,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4189(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop757 in k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4212(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_4212,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4222,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:95: chicken.compiler.support#mark-variable */
t4=*((C_word*)lf[13]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=lf[14];
av2[4]=lf[16];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4220 in for-each-loop757 in k4163 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4222,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4212(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop739 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4235(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_4235,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4245,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:91: chicken.compiler.support#mark-variable */
t4=*((C_word*)lf[13]+1);{
C_word av2[5];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
av2[3]=lf[14];
av2[4]=lf[17];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4243 in for-each-loop739 in chicken.compiler.batch-driver#initialize-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4245(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4245,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4235(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4258(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,4)))){
C_save_and_reclaim_args((void *)trf_4258,3,t0,t1,t2);}
a=C_alloc(9);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4262,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t4=t3;
f_4262(t4,C_SCHEME_UNDEFINED);}
else{
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4885,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:120: scheme#append */
t5=*((C_word*)lf[4]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=*((C_word*)lf[76]+1);
av2[3]=*((C_word*)lf[77]+1);
av2[4]=*((C_word*)lf[12]+1);
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4262(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,4)))){
C_save_and_reclaim_args((void *)trf_4262,2,t0,t1);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4267,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:123: chicken.internal#hash-table-for-each */
t3=*((C_word*)lf[75]+1);{
C_word av2[4];
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4267(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,2)))){
C_save_and_reclaim((void *)f_4267,c,av);}
a=C_alloc(22);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_FALSE;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_FALSE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_END_OF_LIST;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_END_OF_LIST;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_END_OF_LIST;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
if(C_truep(C_i_memq(t2,((C_word*)((C_word*)t0)[2])[1]))){
t16=C_SCHEME_UNDEFINED;
t17=t1;{
C_word *av2=av;
av2[0]=t17;
av2[1]=t16;
((C_proc)(void*)(*((C_word*)t17+1)))(2,av2);}}
else{
t16=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4277,a[2]=t1,a[3]=t9,a[4]=t5,a[5]=t7,a[6]=t11,a[7]=t15,a[8]=t13,a[9]=t3,tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:132: scheme#write */
t17=*((C_word*)lf[74]+1);{
C_word *av2=av;
av2[0]=t17;
av2[1]=t16;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t17+1)))(3,av2);}}}

/* k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4277(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_4277,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4280,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_4468,a[2]=t4,a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[6],tmp=(C_word)a,a+=9,tmp));
t6=((C_word*)t4)[1];
f_4468(t6,t2,((C_word*)t0)[9]);}

/* k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4280(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_4280,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_4283,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[8])[1]))){
t3=*((C_word*)lf[20]+1);
t4=*((C_word*)lf[20]+1);
t5=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4459,a[2]=t2,a[3]=((C_word*)t0)[8],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:160: ##sys#print */
t7=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[31];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4283(2,av2);}}}

/* k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_ccall f_4283(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_4283,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4286,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[7])[1]))){
t3=*((C_word*)lf[20]+1);
t4=*((C_word*)lf[20]+1);
t5=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4443,a[2]=t2,a[3]=((C_word*)t0)[7],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:161: ##sys#print */
t7=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[30];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4286(2,av2);}}}

/* k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_ccall f_4286(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,4)))){
C_save_and_reclaim((void *)f_4286,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_4289,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[6])[1]))){
t3=*((C_word*)lf[20]+1);
t4=*((C_word*)lf[20]+1);
t5=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4427,a[2]=t2,a[3]=((C_word*)t0)[6],a[4]=t3,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:162: ##sys#print */
t7=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[29];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4289(2,av2);}}}

/* k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_4289(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_4289,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_4292,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(C_truep(((C_word*)((C_word*)t0)[4])[1])?C_i_not(C_eqp(((C_word*)((C_word*)t0)[4])[1],lf[26])):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=*((C_word*)lf[20]+1);
t5=*((C_word*)lf[20]+1);
t6=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4362,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:164: ##sys#print */
t8=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=lf[27];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t8+1)))(5,av2);}}
else{
t4=(C_truep(((C_word*)((C_word*)t0)[5])[1])?C_i_not(C_eqp(((C_word*)((C_word*)t0)[4])[1],lf[26])):C_SCHEME_FALSE);
if(C_truep(t4)){
t5=*((C_word*)lf[20]+1);
t6=*((C_word*)lf[20]+1);
t7=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4389,a[2]=t2,a[3]=t5,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:166: ##sys#print */
t9=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[28];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t5=C_SCHEME_UNDEFINED;
t6=t2;{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
f_4292(2,av2);}}}}

/* k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_ccall f_4292(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_4292,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4295,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_pairp(((C_word*)((C_word*)t0)[3])[1]))){
t3=((C_word*)((C_word*)t0)[3])[1];
t4=C_SCHEME_UNDEFINED;
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4332,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_4332(t8,t2,t3);}
else{
/* batch-driver.scm:172: scheme#newline */
t3=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k4293 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_4295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4295,c,av);}
/* batch-driver.scm:172: scheme#newline */
t2=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k4307 in for-each-loop958 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in ... */
static void C_ccall f_4309(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4309,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4320,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:170: chicken.compiler.support#node-class */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4318 in k4307 in for-each-loop958 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in ... */
static void C_ccall f_4320(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4320,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4324,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:170: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4322 in k4318 in k4307 in for-each-loop958 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_ccall f_4324(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4324,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
/* batch-driver.scm:170: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* for-each-loop958 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_fcall f_4332(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_4332,3,t0,t1,t2);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4342,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[20]+1);
t6=*((C_word*)lf[20]+1);
t7=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4309,a[2]=t3,a[3]=t5,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:170: ##sys#print */
t9=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[25];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4340 in for-each-loop958 in k4290 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in ... */
static void C_ccall f_4342(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4342,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_4332(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k4360 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_ccall f_4362(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4362,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4373,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:164: chicken.compiler.support#node-class */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4371 in k4360 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_4373(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4373,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4377,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:164: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4375 in k4371 in k4360 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in ... */
static void C_ccall f_4377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4377,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
/* batch-driver.scm:164: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4387 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_ccall f_4389(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4389,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4400,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:166: chicken.compiler.support#node-class */
t3=*((C_word*)lf[24]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4398 in k4387 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_4400(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_4400,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4404,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:166: chicken.compiler.support#node-parameters */
t3=*((C_word*)lf[23]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k4402 in k4398 in k4387 in k4287 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in ... */
static void C_ccall f_4404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_4404,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
/* batch-driver.scm:166: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4425 in k4284 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_4427(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4427,c,av);}
/* batch-driver.scm:162: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_length(((C_word*)((C_word*)t0)[3])[1]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4441 in k4281 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_ccall f_4443(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4443,c,av);}
/* batch-driver.scm:161: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_length(((C_word*)((C_word*)t0)[3])[1]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4457 in k4278 in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_ccall f_4459(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4459,c,av);}
/* batch-driver.scm:160: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_length(((C_word*)((C_word*)t0)[3])[1]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4468(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_4468,3,t0,t1,t2);}
a=C_alloc(18);
if(C_truep(C_i_pairp(t2))){
t3=C_i_caar(t2);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4481,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t5=C_eqp(t3,lf[32]);
t6=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4492,a[2]=t2,a[3]=t4,a[4]=t3,a[5]=((C_word*)t0)[3],a[6]=((C_word*)t0)[2],a[7]=t1,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=((C_word*)t0)[8],tmp=(C_word)a,a+=13,tmp);
if(C_truep(t5)){
t7=t6;
f_4492(t7,t5);}
else{
t7=C_eqp(t3,lf[54]);
if(C_truep(t7)){
t8=t6;
f_4492(t8,t7);}
else{
t8=C_eqp(t3,lf[55]);
if(C_truep(t8)){
t9=t6;
f_4492(t9,t8);}
else{
t9=C_eqp(t3,lf[56]);
if(C_truep(t9)){
t10=t6;
f_4492(t10,t9);}
else{
t10=C_eqp(t3,lf[57]);
if(C_truep(t10)){
t11=t6;
f_4492(t11,t10);}
else{
t11=C_eqp(t3,lf[58]);
if(C_truep(t11)){
t12=t6;
f_4492(t12,t11);}
else{
t12=C_eqp(t3,lf[59]);
if(C_truep(t12)){
t13=t6;
f_4492(t13,t12);}
else{
t13=C_eqp(t3,lf[60]);
if(C_truep(t13)){
t14=t6;
f_4492(t14,t13);}
else{
t14=C_eqp(t3,lf[61]);
if(C_truep(t14)){
t15=t6;
f_4492(t15,t14);}
else{
t15=C_eqp(t3,lf[62]);
if(C_truep(t15)){
t16=t6;
f_4492(t16,t15);}
else{
t16=C_eqp(t3,lf[63]);
if(C_truep(t16)){
t17=t6;
f_4492(t17,t16);}
else{
t17=C_eqp(t3,lf[64]);
if(C_truep(t17)){
t18=t6;
f_4492(t18,t17);}
else{
t18=C_eqp(t3,lf[65]);
if(C_truep(t18)){
t19=t6;
f_4492(t19,t18);}
else{
t19=C_eqp(t3,lf[66]);
if(C_truep(t19)){
t20=t6;
f_4492(t20,t19);}
else{
t20=C_eqp(t3,lf[67]);
if(C_truep(t20)){
t21=t6;
f_4492(t21,t20);}
else{
t21=C_eqp(t3,lf[68]);
if(C_truep(t21)){
t22=t6;
f_4492(t22,t21);}
else{
t22=C_eqp(t3,lf[69]);
if(C_truep(t22)){
t23=t6;
f_4492(t23,t22);}
else{
t23=C_eqp(t3,lf[70]);
if(C_truep(t23)){
t24=t6;
f_4492(t24,t23);}
else{
t24=C_eqp(t3,lf[71]);
if(C_truep(t24)){
t25=t6;
f_4492(t25,t24);}
else{
t25=C_eqp(t3,lf[72]);
t26=t6;
f_4492(t26,(C_truep(t25)?t25:C_eqp(t3,lf[73])));}}}}}}}}}}}}}}}}}}}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k4479 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_ccall f_4481(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_4481,c,av);}
/* batch-driver.scm:159: loop */
t2=((C_word*)((C_word*)t0)[2])[1];
f_4468(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_fcall f_4492(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,3)))){
C_save_and_reclaim_args((void *)trf_4492,2,t0,t1);}
a=C_alloc(10);
if(C_truep(t1)){
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4498,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:140: ##sys#write-char-0 */
t6=*((C_word*)lf[34]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_make_character(9);
av2[3]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[26]);
if(C_truep(t2)){
t3=C_mutate(((C_word *)((C_word*)t0)[5])+1,lf[26]);
/* batch-driver.scm:159: loop */
t4=((C_word*)((C_word*)t0)[6])[1];
f_4468(t4,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}
else{
t3=C_eqp(((C_word*)t0)[4],lf[35]);
if(C_truep(t3)){
t4=C_eqp(((C_word*)((C_word*)t0)[5])[1],lf[26]);
if(C_truep(t4)){
/* batch-driver.scm:159: loop */
t5=((C_word*)((C_word*)t0)[6])[1];
f_4468(t5,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}
else{
t5=C_i_cdar(((C_word*)t0)[2]);
t6=C_mutate(((C_word *)((C_word*)t0)[5])+1,t5);
/* batch-driver.scm:159: loop */
t7=((C_word*)((C_word*)t0)[6])[1];
f_4468(t7,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}}
else{
t4=C_eqp(((C_word*)t0)[4],lf[36]);
if(C_truep(t4)){
t5=C_eqp(((C_word*)((C_word*)t0)[5])[1],lf[26]);
if(C_truep(t5)){
/* batch-driver.scm:159: loop */
t6=((C_word*)((C_word*)t0)[6])[1];
f_4468(t6,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}
else{
t6=C_i_cdar(((C_word*)t0)[2]);
t7=C_mutate(((C_word *)((C_word*)t0)[8])+1,t6);
/* batch-driver.scm:159: loop */
t8=((C_word*)((C_word*)t0)[6])[1];
f_4468(t8,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}}
else{
t5=C_eqp(((C_word*)t0)[4],lf[37]);
if(C_truep(t5)){
t6=C_i_cdar(((C_word*)t0)[2]);
t7=C_mutate(((C_word *)((C_word*)t0)[9])+1,t6);
/* batch-driver.scm:159: loop */
t8=((C_word*)((C_word*)t0)[6])[1];
f_4468(t8,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[2]));}
else{
t6=C_eqp(((C_word*)t0)[4],lf[38]);
t7=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_4571,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[10],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],tmp=(C_word)a,a+=10,tmp);
if(C_truep(t6)){
t8=t7;
f_4571(t8,t6);}
else{
t8=C_eqp(((C_word*)t0)[4],lf[44]);
if(C_truep(t8)){
t9=t7;
f_4571(t9,t8);}
else{
t9=C_eqp(((C_word*)t0)[4],lf[45]);
if(C_truep(t9)){
t10=t7;
f_4571(t10,t9);}
else{
t10=C_eqp(((C_word*)t0)[4],lf[46]);
if(C_truep(t10)){
t11=t7;
f_4571(t11,t10);}
else{
t11=C_eqp(((C_word*)t0)[4],lf[47]);
if(C_truep(t11)){
t12=t7;
f_4571(t12,t11);}
else{
t12=C_eqp(((C_word*)t0)[4],lf[48]);
if(C_truep(t12)){
t13=t7;
f_4571(t13,t12);}
else{
t13=C_eqp(((C_word*)t0)[4],lf[49]);
if(C_truep(t13)){
t14=t7;
f_4571(t14,t13);}
else{
t14=C_eqp(((C_word*)t0)[4],lf[50]);
if(C_truep(t14)){
t15=t7;
f_4571(t15,t14);}
else{
t15=C_eqp(((C_word*)t0)[4],lf[51]);
if(C_truep(t15)){
t16=t7;
f_4571(t16,t15);}
else{
t16=C_eqp(((C_word*)t0)[4],lf[52]);
t17=t7;
f_4571(t17,(C_truep(t16)?t16:C_eqp(((C_word*)t0)[4],lf[53])));}}}}}}}}}}}}}}}

/* k4496 in k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_ccall f_4498(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4498,c,av);}
t2=C_i_caar(((C_word*)t0)[2]);
t3=C_i_assq(t2,lf[33]);
/* batch-driver.scm:140: ##sys#print */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=((C_word*)t0)[3];
av2[2]=C_i_cdr(t3);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k4569 in k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_fcall f_4571(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_4571,2,t0,t1);}
a=C_alloc(5);
if(C_truep(t1)){
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4577,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:151: ##sys#write-char-0 */
t6=*((C_word*)lf[34]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=C_make_character(9);
av2[3]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t2=C_eqp(((C_word*)t0)[4],lf[39]);
if(C_truep(t2)){
t3=C_i_cdar(((C_word*)t0)[3]);
t4=C_mutate(((C_word *)((C_word*)t0)[5])+1,t3);
/* batch-driver.scm:159: loop */
t5=((C_word*)((C_word*)t0)[6])[1];
f_4468(t5,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[3]));}
else{
t3=C_eqp(((C_word*)t0)[4],lf[40]);
if(C_truep(t3)){
t4=C_i_cdar(((C_word*)t0)[3]);
t5=C_mutate(((C_word *)((C_word*)t0)[8])+1,t4);
/* batch-driver.scm:159: loop */
t6=((C_word*)((C_word*)t0)[6])[1];
f_4468(t6,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[3]));}
else{
t4=C_eqp(((C_word*)t0)[4],lf[41]);
if(C_truep(t4)){
t5=C_i_cdar(((C_word*)t0)[3]);
t6=C_mutate(((C_word *)((C_word*)t0)[9])+1,t5);
/* batch-driver.scm:159: loop */
t7=((C_word*)((C_word*)t0)[6])[1];
f_4468(t7,((C_word*)t0)[7],C_u_i_cdr(((C_word*)t0)[3]));}
else{
/* batch-driver.scm:158: chicken.compiler.support#bomb */
t5=*((C_word*)lf[42]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[43];
av2[3]=C_u_i_car(((C_word*)t0)[3]);
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}}}}}

/* k4575 in k4569 in k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_4577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_4577,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4580,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:151: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_caar(((C_word*)t0)[3]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k4578 in k4575 in k4569 in k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_ccall f_4580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_4580,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_4583,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:151: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(61);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k4581 in k4578 in k4575 in k4569 in k4490 in loop in k4275 in a4266 in k4260 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_4583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_4583,c,av);}
/* batch-driver.scm:151: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_cdar(((C_word*)t0)[3]);
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k4883 in chicken.compiler.batch-driver#display-analysis-database in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4885(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_4885,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_4262(t3,t2);}

/* chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4887(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand((c-4)*C_SIZEOF_PAIR +9,c,3)))){
C_save_and_reclaim((void*)f_4887,c,av);}
a=C_alloc((c-4)*C_SIZEOF_PAIR+9);
t4=C_build_rest(&a,c,4,av);
C_word t5;
C_word t6;
C_word t7;
t5=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4890,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4921,a[2]=t4,a[3]=t5,a[4]=t1,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:185: chicken.compiler.core#initialize-compiler */
t7=*((C_word*)lf[498]+1);{
C_word *av2=av;
av2[0]=t7;
av2[1]=t6;
((C_proc)(void*)(*((C_word*)t7+1)))(2,av2);}}

/* option-arg in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4890(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_4890,2,t1,t2);}
t3=C_i_cdr(t2);
if(C_truep(C_i_nullp(t3))){
/* batch-driver.scm:180: chicken.compiler.support#quit-compiling */
t4=*((C_word*)lf[79]+1);{
C_word av2[4];
av2[0]=t4;
av2[1]=t1;
av2[2]=lf[80];
av2[3]=C_u_i_car(t2);
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t4=C_i_cadr(t2);
if(C_truep(C_i_symbolp(t4))){
/* batch-driver.scm:183: chicken.compiler.support#quit-compiling */
t5=*((C_word*)lf[79]+1);{
C_word av2[4];
av2[0]=t5;
av2[1]=t1;
av2[2]=lf[81];
av2[3]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
t5=t1;{
C_word av2[2];
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}}}

/* k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4921(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_4921,c,av);}
a=C_alloc(7);
t2=C_mutate((C_word*)lf[82]+1 /* (set! chicken.compiler.core#explicit-use-flag ...) */,C_u_i_memq(lf[83],((C_word*)t0)[2]));
t3=C_mutate((C_word*)lf[84]+1 /* (set! chicken.compiler.core#emit-debug-info ...) */,C_u_i_memq(lf[85],((C_word*)t0)[2]));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4930,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_u_i_memq(lf[497],((C_word*)t0)[2]))){
t5=C_mutate((C_word*)lf[490]+1 /* (set! chicken.compiler.core#compile-module-registration ...) */,lf[491]);
t6=t4;
f_4930(t6,t5);}
else{
t5=t4;
f_4930(t5,C_SCHEME_UNDEFINED);}}

/* k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4930(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_4930,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4933,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_u_i_memq(lf[495],((C_word*)t0)[2]))){
t3=C_mutate((C_word*)lf[490]+1 /* (set! chicken.compiler.core#compile-module-registration ...) */,lf[496]);
t4=t2;
f_4933(t4,t3);}
else{
t3=t2;
f_4933(t3,C_SCHEME_UNDEFINED);}}

/* k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_fcall f_4933(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_4933,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_4936,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
if(C_truep(C_u_i_memq(lf[493],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[489] /* chicken.compiler.core#static-extensions */,0,C_SCHEME_TRUE);
/* batch-driver.scm:194: chicken.platform#register-feature! */
t4=*((C_word*)lf[135]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[494];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_4936(2,av2);}}}

/* k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 */
static void C_ccall f_4936(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_4936,c,av);}
a=C_alloc(28);
t2=C_u_i_memq(lf[86],((C_word*)t0)[2]);
t3=C_u_i_memq(lf[87],((C_word*)t0)[2]);
t4=C_a_i_cons(&a,2,lf[88],*((C_word*)lf[89]+1));
t5=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_8215,a[2]=t4,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=t2,a[9]=t3,tmp=(C_word)a,a+=10,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8234,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t7=(C_truep(*((C_word*)lf[84]+1))?lf[486]:C_SCHEME_END_OF_LIST);
t8=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8242,a[2]=t6,a[3]=t7,a[4]=t2,a[5]=t3,tmp=(C_word)a,a+=6,tmp);
if(C_truep(*((C_word*)lf[82]+1))){
t9=t8;
f_8242(t9,C_SCHEME_END_OF_LIST);}
else{
t9=C_a_i_cons(&a,2,lf[388],*((C_word*)lf[492]+1));
t10=t8;
f_8242(t10,C_a_i_list(&a,1,t9));}}

/* k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_ccall f_4950(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,2)))){
C_save_and_reclaim((void *)f_4950,c,av);}
a=C_alloc(23);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=*((C_word*)lf[92]+1);
t7=(*a=C_CLOSURE_TYPE|14,a[1]=(C_word)f_4956,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=t4,a[13]=t6,a[14]=t5,tmp=(C_word)a,a+=15,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8172,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:229: chicken.process-context#get-environment-variable */
t9=*((C_word*)lf[479]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[480];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}

/* k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_4956(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,3)))){
C_save_and_reclaim((void *)f_4956,c,av);}
a=C_alloc(20);
t2=C_i_check_list_2(t1,lf[5]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_4962,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8136,a[2]=((C_word*)t0)[12],a[3]=t5,a[4]=((C_word*)t0)[13],a[5]=((C_word*)t0)[14],tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_8136(t7,t3,t1);}

/* k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_ccall f_4962(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_4962,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_4965,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=t1,a[12]=((C_word*)t0)[11],tmp=(C_word)a,a+=13,tmp);
/* batch-driver.scm:231: chicken.compiler.optimizer#default-optimization-passes */
t3=*((C_word*)lf[476]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_4965(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,2)))){
C_save_and_reclaim((void *)f_4965,c,av);}
a=C_alloc(31);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_END_OF_LIST;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_FALSE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_FALSE;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=lf[93];
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_u_i_memq(lf[94],((C_word*)t0)[2]);
t15=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_4970,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t5,a[5]=t3,a[6]=((C_word*)t0)[4],a[7]=((C_word*)t0)[5],a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=((C_word*)t0)[10],a[13]=t9,a[14]=t11,a[15]=t13,a[16]=t7,a[17]=((C_word*)t0)[11],a[18]=((C_word*)t0)[12],tmp=(C_word)a,a+=19,tmp);
if(C_truep(t14)){
t16=t15;
f_4970(t16,t14);}
else{
t16=C_u_i_memq(lf[367],((C_word*)t0)[2]);
t17=t15;
f_4970(t17,(C_truep(t16)?t16:C_u_i_memq(lf[95],((C_word*)t0)[2])));}}

/* k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in ... */
static void C_fcall f_4970(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,0,2)))){
C_save_and_reclaim_args((void *)trf_4970,2,t0,t1);}
a=C_alloc(39);
t2=C_u_i_memq(lf[95],((C_word*)t0)[2]);
t3=(C_truep(t2)?C_i_cadr(t2):C_SCHEME_FALSE);
t4=C_u_i_memq(lf[96],((C_word*)t0)[2]);
t5=C_u_i_memq(lf[97],((C_word*)t0)[2]);
t6=C_u_i_memq(lf[98],((C_word*)t0)[2]);
t7=C_u_i_memq(lf[99],((C_word*)t0)[2]);
t8=C_SCHEME_TRUE;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_u_i_memq(lf[100],((C_word*)t0)[2]);
t11=C_SCHEME_FALSE;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_SCHEME_FALSE;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_SCHEME_FALSE;
t16=(*a=C_VECTOR_TYPE|1,a[1]=t15,tmp=(C_word)a,a+=2,tmp);
t17=C_u_i_memq(lf[101],((C_word*)t0)[2]);
t18=(C_truep(t17)?t17:C_u_i_memq(lf[102],((C_word*)t0)[2]));
t19=C_u_i_memq(lf[103],((C_word*)t0)[2]);
t20=(*a=C_CLOSURE_TYPE|30,a[1]=(C_word)f_4995,a[2]=t12,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=t16,a[8]=((C_word*)t0)[6],a[9]=((C_word*)t0)[7],a[10]=((C_word*)t0)[8],a[11]=((C_word*)t0)[9],a[12]=t14,a[13]=t6,a[14]=((C_word*)t0)[10],a[15]=((C_word*)t0)[11],a[16]=((C_word*)t0)[12],a[17]=t7,a[18]=((C_word*)t0)[13],a[19]=t10,a[20]=((C_word*)t0)[14],a[21]=t9,a[22]=((C_word*)t0)[15],a[23]=t3,a[24]=((C_word*)t0)[16],a[25]=t1,a[26]=t18,a[27]=t4,a[28]=((C_word*)t0)[17],a[29]=t5,a[30]=((C_word*)t0)[18],tmp=(C_word)a,a+=31,tmp);
if(C_truep(t19)){
/* batch-driver.scm:255: option-arg */
f_4890(t20,t19);}
else{
t21=t20;{
C_word av2[2];
av2[0]=t21;
av2[1]=C_SCHEME_FALSE;
f_4995(2,av2);}}}

/* k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in ... */
static void C_ccall f_4995(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word t17;
C_word t18;
C_word t19;
C_word t20;
C_word t21;
C_word t22;
C_word t23;
C_word t24;
C_word t25;
C_word t26;
C_word t27;
C_word t28;
C_word t29;
C_word t30;
C_word t31;
C_word t32;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(91,c,6)))){
C_save_and_reclaim((void *)f_4995,c,av);}
a=C_alloc(91);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_SCHEME_UNDEFINED;
t9=(*a=C_VECTOR_TYPE|1,a[1]=t8,tmp=(C_word)a,a+=2,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_SCHEME_UNDEFINED;
t13=(*a=C_VECTOR_TYPE|1,a[1]=t12,tmp=(C_word)a,a+=2,tmp);
t14=C_SCHEME_UNDEFINED;
t15=(*a=C_VECTOR_TYPE|1,a[1]=t14,tmp=(C_word)a,a+=2,tmp);
t16=C_SCHEME_UNDEFINED;
t17=(*a=C_VECTOR_TYPE|1,a[1]=t16,tmp=(C_word)a,a+=2,tmp);
t18=C_SCHEME_UNDEFINED;
t19=(*a=C_VECTOR_TYPE|1,a[1]=t18,tmp=(C_word)a,a+=2,tmp);
t20=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5013,tmp=(C_word)a,a+=2,tmp));
t21=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5040,a[2]=((C_word*)t0)[2],a[3]=t3,tmp=(C_word)a,a+=4,tmp));
t22=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5062,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t23=C_set_block_item(t9,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5089,a[2]=t3,tmp=(C_word)a,a+=3,tmp));
t24=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_5135,tmp=(C_word)a,a+=2,tmp));
t25=C_set_block_item(t13,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5220,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp));
t26=C_set_block_item(t15,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5255,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp));
t27=C_set_block_item(t17,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5265,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],tmp=(C_word)a,a+=4,tmp));
t28=C_set_block_item(t19,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5305,a[2]=((C_word*)t0)[7],tmp=(C_word)a,a+=3,tmp));
t29=(*a=C_CLOSURE_TYPE|37,a[1]=(C_word)f_5371,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[8],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[9],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],a[10]=((C_word*)t0)[13],a[11]=t17,a[12]=t15,a[13]=t19,a[14]=t5,a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],a[17]=((C_word*)t0)[16],a[18]=((C_word*)t0)[17],a[19]=t7,a[20]=((C_word*)t0)[18],a[21]=((C_word*)t0)[19],a[22]=((C_word*)t0)[20],a[23]=((C_word*)t0)[21],a[24]=t13,a[25]=t9,a[26]=((C_word*)t0)[22],a[27]=((C_word*)t0)[23],a[28]=t1,a[29]=((C_word*)t0)[24],a[30]=((C_word*)t0)[25],a[31]=((C_word*)t0)[26],a[32]=t11,a[33]=((C_word*)t0)[3],a[34]=((C_word*)t0)[27],a[35]=((C_word*)t0)[28],a[36]=((C_word*)t0)[29],a[37]=((C_word*)t0)[5],tmp=(C_word)a,a+=38,tmp);
if(C_truep(((C_word*)t0)[30])){
t30=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8115,a[2]=t29,tmp=(C_word)a,a+=3,tmp);
t31=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8119,a[2]=t30,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:324: option-arg */
f_4890(t31,((C_word*)t0)[30]);}
else{
t30=t29;
f_5371(t30,C_SCHEME_UNDEFINED);}}

/* print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5013(C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,4)))){
C_save_and_reclaim_args((void *)trf_5013,3,t1,t2,t3);}
a=C_alloc(5);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5017,a[2]=t3,a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:263: chicken.compiler.support#debugging */
t5=*((C_word*)lf[105]+1);{
C_word av2[5];
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[106];
av2[3]=lf[107];
av2[4]=t2;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* k5015 in print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5017(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5017,c,av);}
a=C_alloc(5);
if(C_truep(C_i_memq(((C_word*)t0)[2],*((C_word*)lf[104]+1)))){
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5029,a[2]=((C_word*)t0)[3],a[3]=t2,a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:266: ##sys#write-char-0 */
t6=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=C_make_character(91);
av2[3]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k5027 in k5015 in print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5029(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_5029,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5032,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:266: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5030 in k5027 in k5015 in print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5032(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5032,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5035,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:266: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(93);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5033 in k5030 in k5027 in k5015 in print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5035(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_5035,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5038,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:266: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5036 in k5033 in k5030 in k5027 in k5015 in print-header in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in ... */
static void C_ccall f_5038(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5038,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* print-node in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5040(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_5040,5,t0,t1,t2,t3,t4);}
a=C_alloc(5);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5047,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:270: print-header */
f_5013(t5,t2,t3);}

/* k5045 in print-node in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5047(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5047,c,av);}
a=C_alloc(3);
if(C_truep(t1)){
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
/* batch-driver.scm:272: chicken.compiler.support#dump-nodes */
t2=*((C_word*)lf[108]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5060,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:273: chicken.compiler.support#build-expression-tree */
t3=*((C_word*)lf[110]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5058 in k5045 in print-node in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5060,c,av);}
/* batch-driver.scm:273: chicken.pretty-print#pretty-print */
t2=*((C_word*)lf[109]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5062(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5){
C_word tmp;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,3)))){
C_save_and_reclaim_args((void *)trf_5062,6,t0,t1,t2,t3,t4,t5);}
a=C_alloc(5);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5069,a[2]=t1,a[3]=t4,a[4]=t5,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:276: print-header */
f_5013(t6,t2,t3);}

/* k5067 in print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5069(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_5069,c,av);}
a=C_alloc(6);
if(C_truep(t1)){
t2=*((C_word*)lf[20]+1);
t3=*((C_word*)lf[20]+1);
t4=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5075,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=((C_word*)t0)[4],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:277: ##sys#print */
t6=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[111];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5073 in k5067 in print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5075(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5075,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5078,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:277: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5076 in k5073 in k5067 in print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5078(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5078,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5081,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:277: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(41);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5079 in k5076 in k5073 in k5067 in print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5081(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5081,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5084,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:277: ##sys#write-char-0 */
t3=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5082 in k5079 in k5076 in k5073 in k5067 in print-db in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in ... */
static void C_ccall f_5084(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5084,c,av);}
/* batch-driver.scm:278: display-analysis-database */
t2=lf[18];
f_4258(t2,((C_word*)t0)[2],((C_word*)t0)[3]);}

/* print-expr in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5089(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_5089,5,t0,t1,t2,t3,t4);}
a=C_alloc(4);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5096,a[2]=t4,a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:281: print-header */
f_5013(t5,t2,t3);}

/* k5094 in print-expr in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5096(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_5096,c,av);}
a=C_alloc(5);
if(C_truep(t1)){
t2=C_i_check_list_2(((C_word*)t0)[2],lf[10]);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5112,a[2]=t4,tmp=(C_word)a,a+=3,tmp));
t6=((C_word*)t4)[1];
f_5112(t6,((C_word*)t0)[3],((C_word*)t0)[2]);}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5099 in for-each-loop1165 in k5094 in print-expr in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5101(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5101,c,av);}
/* batch-driver.scm:285: scheme#newline */
t2=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* for-each-loop1165 in k5094 in print-expr in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_fcall f_5112(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_5112,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5122,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5101,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:284: chicken.pretty-print#pretty-print */
t6=*((C_word*)lf[109]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5120 in for-each-loop1165 in k5094 in print-expr in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5122(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5122,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_5112(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5135(C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,0,4)))){
C_save_and_reclaim_args((void *)trf_5135,2,t1,t2);}
a=C_alloc(16);
t3=C_i_string_length(t2);
t4=C_a_i_fixnum_difference(&a,2,t3,C_fix(1));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5144,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_fixnum_lessp(t3,C_fix(2)))){
/* batch-driver.scm:292: scheme#string->number */
t6=*((C_word*)lf[113]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t6=C_i_string_ref(t2,t4);
t7=C_eqp(t6,C_make_character(109));
t8=(C_truep(t7)?t7:C_eqp(t6,C_make_character(77)));
if(C_truep(t8)){
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5175,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5179,a[2]=t9,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:294: scheme#substring */
t11=*((C_word*)lf[114]+1);{
C_word av2[5];
av2[0]=t11;
av2[1]=t10;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t11+1)))(5,av2);}}
else{
t9=C_eqp(t6,C_make_character(107));
t10=(C_truep(t9)?t9:C_eqp(t6,C_make_character(75)));
if(C_truep(t10)){
t11=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5195,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5199,a[2]=t11,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:295: scheme#substring */
t13=*((C_word*)lf[114]+1);{
C_word av2[5];
av2[0]=t13;
av2[1]=t12;
av2[2]=t2;
av2[3]=C_fix(0);
av2[4]=t4;
((C_proc)(void*)(*((C_word*)t13+1)))(5,av2);}}
else{
/* batch-driver.scm:296: scheme#string->number */
t11=*((C_word*)lf[113]+1);{
C_word av2[3];
av2[0]=t11;
av2[1]=t5;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t11+1)))(3,av2);}}}}}

/* k5142 in arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5144(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5144,c,av);}
if(C_truep(t1)){
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
/* batch-driver.scm:297: chicken.compiler.support#quit-compiling */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[112];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k5173 in arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5175(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_5175,c,av);}
a=C_alloc(33);
t2=C_s_a_i_times(&a,2,t1,C_fix(1048576));
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* batch-driver.scm:297: chicken.compiler.support#quit-compiling */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[112];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k5177 in arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5179(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5179,c,av);}
/* batch-driver.scm:294: scheme#string->number */
t2=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5193 in arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5195(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,3)))){
C_save_and_reclaim((void *)f_5195,c,av);}
a=C_alloc(33);
t2=C_s_a_i_times(&a,2,t1,C_fix(1024));
if(C_truep(t2)){
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
/* batch-driver.scm:297: chicken.compiler.support#quit-compiling */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[112];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k5197 in arg-val in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5199(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5199,c,av);}
/* batch-driver.scm:295: scheme#string->number */
t2=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* collect-options in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5220(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,3)))){
C_save_and_reclaim_args((void *)trf_5220,3,t0,t1,t2);}
a=C_alloc(7);
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5226,a[2]=t2,a[3]=t4,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp));
t6=((C_word*)t4)[1];
f_5226(t6,t1,((C_word*)t0)[3]);}

/* loop in collect-options in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_fcall f_5226(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,3)))){
C_save_and_reclaim_args((void *)trf_5226,3,t0,t1,t2);}
a=C_alloc(4);
t3=C_i_memq(((C_word*)t0)[2],t2);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5234,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:301: g1209 */
t5=t4;
f_5234(t5,t1,t3);}
else{
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* g1209 in loop in collect-options in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_fcall f_5234(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_5234,3,t0,t1,t2);}
a=C_alloc(5);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5242,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:301: option-arg */
f_4890(t3,t2);}

/* k5240 in g1209 in loop in collect-options in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5242(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_5242,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5246,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:301: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5226(t3,t2,C_i_cddr(((C_word*)t0)[4]));}

/* k5244 in k5240 in g1209 in loop in collect-options in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5246(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,1)))){
C_save_and_reclaim((void *)f_5246,c,av);}
a=C_alloc(3);
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_a_i_cons(&a,2,((C_word*)t0)[3],t1);
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* begin-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5255(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,0,2)))){
C_save_and_reclaim_args((void *)trf_5255,2,t0,t1);}
a=C_alloc(4);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5263,a[2]=((C_word*)t0)[3],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:257: chicken.time#current-milliseconds */
t3=*((C_word*)lf[115]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5261 in begin-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5263(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5263,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5265(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,4)))){
C_save_and_reclaim_args((void *)trf_5265,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t3=*((C_word*)lf[20]+1);
t4=*((C_word*)lf[20]+1);
t5=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t6=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5275,a[2]=t1,a[3]=t3,a[4]=((C_word*)t0)[3],a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:309: ##sys#print */
t7=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[119];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_ccall f_5275(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5275,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5278,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:309: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5278(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_5278,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5281,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:309: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[118];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5279 in k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5281(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,2)))){
C_save_and_reclaim((void *)f_5281,c,av);}
a=C_alloc(15);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5284,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5291,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5295,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5303,a[2]=((C_word*)t0)[4],a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:257: chicken.time#current-milliseconds */
t6=*((C_word*)lf[115]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k5282 in k5279 in k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5284(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_5284,c,av);}
/* batch-driver.scm:309: ##sys#write-char-0 */
t2=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k5289 in k5279 in k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5291(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5291,c,av);}
/* batch-driver.scm:309: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_TRUE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k5293 in k5279 in k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5295(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5295,c,av);}
/* batch-driver.scm:311: scheme#inexact->exact */
t2=*((C_word*)lf[116]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5301 in k5279 in k5276 in k5273 in end-time in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,2)))){
C_save_and_reclaim((void *)f_5303,c,av);}
a=C_alloc(29);
t2=C_s_a_i_minus(&a,2,t1,((C_word*)((C_word*)t0)[2])[1]);
/* batch-driver.scm:311: scheme#round */
t3=*((C_word*)lf[117]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5305(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4){
C_word tmp;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,4)))){
C_save_and_reclaim_args((void *)trf_5305,5,t0,t1,t2,t3,t4);}
a=C_alloc(11);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5307,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=t3,tmp=(C_word)a,a+=5,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5330,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5335,a[2]=t6,tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_nullp(t4))){
/* batch-driver.scm:314: def-no1230 */
t8=t7;
f_5335(t8,t1);}
else{
t8=C_i_car(t4);
if(C_truep(C_mk_bool(C_unfix(C_i_length(t4)) >= 1))){
/* batch-driver.scm:314: def-contf1231 */
t9=t6;
f_5330(t9,t1,t8);}
else{
t9=C_u_i_list_ref(t4,1);
/* batch-driver.scm:314: body1228 */
t10=t5;
f_5307(t10,t1,t8,t9);}}}

/* body1228 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_fcall f_5307(C_word t0,C_word t1,C_word t2,C_word t3){
C_word tmp;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_5307,4,t0,t1,t2,t3);}
a=C_alloc(8);
t4=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_5311,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=t2,a[7]=t3,tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:315: chicken.compiler.core#analyze-expression */
t5=*((C_word*)lf[122]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5309 in body1228 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5311(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,8)))){
C_save_and_reclaim((void *)f_5311,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5314,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)((C_word*)t0)[3])[1])){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5319,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5325,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:317: upap */
t5=((C_word*)((C_word*)t0)[3])[1];{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t5;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=t1;
av2[4]=((C_word*)t0)[5];
av2[5]=t3;
av2[6]=t4;
av2[7]=((C_word*)t0)[6];
av2[8]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t5+1)))(9,av2);}}
else{
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k5312 in k5309 in body1228 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5314(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5314,c,av);}
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a5318 in k5309 in body1228 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5319(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_5319,c,av);}
t4=*((C_word*)lf[120]+1);
/* batch-driver.scm:318: g1250 */
t5=*((C_word*)lf[120]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}

/* a5324 in k5309 in body1228 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_5325,c,av);}
t5=*((C_word*)lf[121]+1);
/* batch-driver.scm:319: g1264 */
t6=*((C_word*)lf[121]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t6;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
av2[3]=t2;
av2[4]=t3;
av2[5]=t4;
((C_proc)(void*)(*((C_word*)t6+1)))(6,av2);}}

/* def-contf1231 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_fcall f_5330(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,3)))){
C_save_and_reclaim_args((void *)trf_5330,3,t0,t1,t2);}
/* batch-driver.scm:314: body1228 */
t3=((C_word*)t0)[2];
f_5307(t3,t1,t2,C_SCHEME_TRUE);}

/* def-no1230 in analyze in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_fcall f_5335(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,2)))){
C_save_and_reclaim_args((void *)trf_5335,2,t0,t1);}
/* batch-driver.scm:314: def-contf1231 */
t2=((C_word*)t0)[2];
f_5330(t2,t1,C_fix(0));}

/* k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_fcall f_5371(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,0,2)))){
C_save_and_reclaim_args((void *)trf_5371,2,t0,t1);}
a=C_alloc(38);
t2=(*a=C_CLOSURE_TYPE|37,a[1]=(C_word)f_5374,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],tmp=(C_word)a,a+=38,tmp);
t3=*((C_word*)lf[318]+1);
if(C_truep(*((C_word*)lf[318]+1))){
t4=*((C_word*)lf[318]+1);
if(C_truep(*((C_word*)lf[318]+1))){
t5=C_set_block_item(lf[329] /* chicken.compiler.core#standalone-executable */,0,C_SCHEME_FALSE);
t6=t2;
f_5374(t6,t5);}
else{
t5=t2;
f_5374(t5,C_SCHEME_UNDEFINED);}}
else{
if(C_truep(((C_word*)t0)[17])){
t4=C_set_block_item(lf[329] /* chicken.compiler.core#standalone-executable */,0,C_SCHEME_FALSE);
t5=t2;
f_5374(t5,t4);}
else{
t4=t2;
f_5374(t4,C_SCHEME_UNDEFINED);}}}

/* k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in ... */
static void C_fcall f_5374(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,0,2)))){
C_save_and_reclaim_args((void *)trf_5374,2,t0,t1);}
a=C_alloc(38);
t2=(*a=C_CLOSURE_TYPE|37,a[1]=(C_word)f_5377,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],tmp=(C_word)a,a+=38,tmp);
if(C_truep(C_u_i_memq(lf[268],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[474] /* ##sys#dload-disabled */,0,C_SCHEME_TRUE);
/* batch-driver.scm:329: chicken.platform#repository-path */
t4=*((C_word*)lf[475]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5377(2,av2);}}}

/* k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in ... */
static void C_ccall f_5377(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,3)))){
C_save_and_reclaim((void *)f_5377,c,av);}
a=C_alloc(44);
t2=C_mutate((C_word*)lf[123]+1 /* (set! chicken.compiler.core#enable-specialization ...) */,C_u_i_memq(lf[124],((C_word*)t0)[2]));
t3=(*a=C_CLOSURE_TYPE|37,a[1]=(C_word)f_5384,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],tmp=(C_word)a,a+=38,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_8044,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_8101,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:336: collect-options */
t6=((C_word*)((C_word*)t0)[24])[1];
f_5220(t6,t5,lf[473]);}

/* k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_5384(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,2)))){
C_save_and_reclaim((void *)f_5384,c,av);}
a=C_alloc(41);
t2=C_mutate((C_word*)lf[104]+1 /* (set! chicken.compiler.support#debugging-chicken ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|37,a[1]=(C_word)f_5387,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],a[37]=((C_word*)t0)[37],tmp=(C_word)a,a+=38,tmp);
if(C_truep(C_i_memq(lf[363],*((C_word*)lf[104]+1)))){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8039,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:338: chicken.compiler.support#print-debug-options */
t5=*((C_word*)lf[471]+1);{
C_word *av2=av;
av2[0]=t5;
av2[1]=t4;
((C_proc)(void*)(*((C_word*)t5+1)))(2,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5387(2,av2);}}}

/* k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_5387(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,2)))){
C_save_and_reclaim((void *)f_5387,c,av);}
a=C_alloc(44);
t2=C_i_memq(lf[125],*((C_word*)lf[104]+1));
t3=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=(*a=C_CLOSURE_TYPE|38,a[1]=(C_word)f_5413,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],a[33]=((C_word*)t0)[34],a[34]=((C_word*)t0)[35],a[35]=((C_word*)t0)[36],a[36]=((C_word*)t0)[37],a[37]=t6,a[38]=t7,tmp=(C_word)a,a+=39,tmp);
/* batch-driver.scm:345: collect-options */
t9=((C_word*)((C_word*)t0)[24])[1];
f_5220(t9,t8,lf[470]);}

/* k5402 in map-loop1309 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in ... */
static void C_ccall f_5404(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_5404,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5408,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* ##sys#string-append */
t3=*((C_word*)lf[468]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=lf[469];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5406 in k5402 in map-loop1309 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in ... */
static void C_ccall f_5408(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_5408,c,av);}
a=C_alloc(6);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[3])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[3])+1,t3);
t6=((C_word*)((C_word*)t0)[4])[1];
f_8001(t6,((C_word*)t0)[5],C_slot(((C_word*)t0)[6],C_fix(1)));}

/* k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in ... */
static void C_ccall f_5413(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,3)))){
C_save_and_reclaim((void *)f_5413,c,av);}
a=C_alloc(44);
t2=C_i_check_list_2(t1,lf[5]);
t3=(*a=C_CLOSURE_TYPE|36,a[1]=(C_word)f_5419,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],tmp=(C_word)a,a+=37,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8001,a[2]=((C_word*)t0)[37],a[3]=t5,a[4]=((C_word*)t0)[38],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_8001(t7,t3,t1);}

/* k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in ... */
static void C_ccall f_5419(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,c,2)))){
C_save_and_reclaim((void *)f_5419,c,av);}
a=C_alloc(37);
t2=C_mutate((C_word*)lf[126]+1 /* (set! chicken.compiler.core#import-libraries ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|36,a[1]=(C_word)f_5422,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],tmp=(C_word)a,a+=37,tmp);
if(C_truep(C_u_i_memq(lf[466],((C_word*)t0)[2]))){
if(C_truep(C_i_not(((C_word*)t0)[17]))){
t4=C_set_block_item(lf[467] /* chicken.compiler.core#all-import-libraries */,0,C_SCHEME_TRUE);
t5=t3;
f_5422(t5,t4);}
else{
t4=t3;
f_5422(t4,C_SCHEME_UNDEFINED);}}
else{
t4=t3;
f_5422(t4,C_SCHEME_UNDEFINED);}}

/* k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in ... */
static void C_fcall f_5422(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,0,2)))){
C_save_and_reclaim_args((void *)trf_5422,2,t0,t1);}
a=C_alloc(37);
t2=(*a=C_CLOSURE_TYPE|36,a[1]=(C_word)f_5425,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],tmp=(C_word)a,a+=37,tmp);
if(C_truep(*((C_word*)lf[123]+1))){
t3=C_set_block_item(((C_word*)t0)[22],0,C_SCHEME_TRUE);
t4=t2;
f_5425(t4,t3);}
else{
t3=t2;
f_5425(t3,C_SCHEME_UNDEFINED);}}

/* k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in ... */
static void C_fcall f_5425(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,0,2)))){
C_save_and_reclaim_args((void *)trf_5425,2,t0,t1);}
a=C_alloc(37);
t2=(*a=C_CLOSURE_TYPE|36,a[1]=(C_word)f_5428,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],a[36]=((C_word*)t0)[36],tmp=(C_word)a,a+=37,tmp);
if(C_truep(C_i_memq(lf[187],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:351: ##sys#start-timer */
t3=*((C_word*)lf[465]+1);{
C_word av2[2];
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5428(2,av2);}}}

/* k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in ... */
static void C_ccall f_5428(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,2)))){
C_save_and_reclaim((void *)f_5428,c,av);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5431,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_i_memq(lf[464],*((C_word*)lf[104]+1)))){
t3=C_set_block_item(((C_word*)t0)[36],0,C_SCHEME_TRUE);
t4=t2;
f_5431(t4,t3);}
else{
t3=t2;
f_5431(t3,C_SCHEME_UNDEFINED);}}

/* k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in ... */
static void C_fcall f_5431(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5431,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5434,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[463],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[82] /* chicken.compiler.core#explicit-use-flag */,0,C_SCHEME_TRUE);
t4=C_set_block_item(((C_word*)t0)[25],0,C_SCHEME_END_OF_LIST);
t5=C_set_block_item(((C_word*)t0)[5],0,C_SCHEME_END_OF_LIST);
t6=t2;
f_5434(t6,t5);}
else{
t3=t2;
f_5434(t3,C_SCHEME_UNDEFINED);}}

/* k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in ... */
static void C_fcall f_5434(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5434,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5437,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[461],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[462] /* chicken.compiler.core#emit-closure-info */,0,C_SCHEME_FALSE);
t4=t2;
f_5437(t4,t3);}
else{
t3=t2;
f_5437(t3,C_SCHEME_UNDEFINED);}}

/* k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in ... */
static void C_fcall f_5437(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5437,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5440,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[459],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[460] /* chicken.compiler.core#compiler-syntax-enabled */,0,C_SCHEME_FALSE);
t4=t2;
f_5440(t4,t3);}
else{
t3=t2;
f_5440(t3,C_SCHEME_UNDEFINED);}}

/* k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in ... */
static void C_fcall f_5440(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5440,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5443,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[458],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[438] /* chicken.compiler.core#local-definitions */,0,C_SCHEME_TRUE);
t4=t2;
f_5443(t4,t3);}
else{
t3=t2;
f_5443(t3,C_SCHEME_UNDEFINED);}}

/* k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in ... */
static void C_fcall f_5443(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5443,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5446,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[457],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[274] /* chicken.compiler.core#enable-inline-files */,0,C_SCHEME_TRUE);
t4=C_set_block_item(lf[168] /* chicken.compiler.core#inline-locally */,0,C_SCHEME_TRUE);
t5=t2;
f_5446(t5,t4);}
else{
t3=t2;
f_5446(t3,C_SCHEME_UNDEFINED);}}

/* k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in ... */
static void C_fcall f_5446(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5446,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5449,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(((C_word*)t0)[3])){
t3=C_set_block_item(lf[456] /* ##sys#notices-enabled */,0,C_SCHEME_TRUE);
t4=t2;
f_5449(t4,t3);}
else{
t3=t2;
f_5449(t3,C_SCHEME_UNDEFINED);}}

/* k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in ... */
static void C_fcall f_5449(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5449,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5452,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[455],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[256] /* chicken.compiler.core#strict-variable-types */,0,C_SCHEME_TRUE);
t4=C_set_block_item(lf[123] /* chicken.compiler.core#enable-specialization */,0,C_SCHEME_TRUE);
t5=t2;
f_5452(t5,t4);}
else{
t3=t2;
f_5452(t3,C_SCHEME_UNDEFINED);}}

/* k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in ... */
static void C_fcall f_5452(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(43,0,4)))){
C_save_and_reclaim_args((void *)trf_5452,2,t0,t1);}
a=C_alloc(43);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5455,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[453],((C_word*)t0)[2]))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7954,a[2]=((C_word*)t0)[22],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9494,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[454];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;
f_5455(t3,C_SCHEME_UNDEFINED);}}

/* k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in ... */
static void C_fcall f_5455(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5455,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5458,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[452],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[174] /* chicken.compiler.core#optimize-leaf-routines */,0,C_SCHEME_TRUE);
t4=t2;
f_5458(t4,t3);}
else{
t3=t2;
f_5458(t3,C_SCHEME_UNDEFINED);}}

/* k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in ... */
static void C_fcall f_5458(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5458,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5461,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[451],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[249] /* chicken.compiler.support#unsafe */,0,C_SCHEME_TRUE);
t4=t2;
f_5461(t4,t3);}
else{
t3=t2;
f_5461(t3,C_SCHEME_UNDEFINED);}}

/* k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in ... */
static void C_fcall f_5461(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5461,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5464,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[449],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[450] /* ##sys#setup-mode */,0,C_SCHEME_TRUE);
t4=t2;
f_5464(t4,t3);}
else{
t3=t2;
f_5464(t3,C_SCHEME_UNDEFINED);}}

/* k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in ... */
static void C_fcall f_5464(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5464,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5467,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[447],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[448] /* chicken.compiler.core#preserve-unchanged-import-libraries */,0,C_SCHEME_FALSE);
t4=t2;
f_5467(t4,t3);}
else{
t3=t2;
f_5467(t3,C_SCHEME_UNDEFINED);}}

/* k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in ... */
static void C_fcall f_5467(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5467,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5470,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[446],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[210] /* chicken.compiler.core#insert-timer-checks */,0,C_SCHEME_FALSE);
t4=t2;
f_5470(t4,t3);}
else{
t3=t2;
f_5470(t3,C_SCHEME_UNDEFINED);}}

/* k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in ... */
static void C_fcall f_5470(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5470,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5473,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[443],((C_word*)t0)[2]))){
t3=C_mutate((C_word*)lf[444]+1 /* (set! chicken.compiler.support#number-type ...) */,lf[445]);
t4=t2;
f_5473(t4,t3);}
else{
t3=t2;
f_5473(t3,C_SCHEME_UNDEFINED);}}

/* k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in ... */
static void C_fcall f_5473(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5473,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5476,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[442],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[167] /* chicken.compiler.core#block-compilation */,0,C_SCHEME_TRUE);
t4=t2;
f_5476(t4,t3);}
else{
t3=t2;
f_5476(t3,C_SCHEME_UNDEFINED);}}

/* k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in ... */
static void C_fcall f_5476(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5476,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5479,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[440],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[441] /* chicken.compiler.core#external-protos-first */,0,C_SCHEME_TRUE);
t4=t2;
f_5479(t4,t3);}
else{
t3=t2;
f_5479(t3,C_SCHEME_UNDEFINED);}}

/* k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in ... */
static void C_fcall f_5479(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,0,2)))){
C_save_and_reclaim_args((void *)trf_5479,2,t0,t1);}
a=C_alloc(36);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5482,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[439],((C_word*)t0)[2]))){
t3=C_set_block_item(lf[168] /* chicken.compiler.core#inline-locally */,0,C_SCHEME_TRUE);
t4=t2;
f_5482(t4,t3);}
else{
t3=t2;
f_5482(t3,C_SCHEME_UNDEFINED);}}

/* k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in ... */
static void C_fcall f_5482(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,0,2)))){
C_save_and_reclaim_args((void *)trf_5482,2,t0,t1);}
a=C_alloc(39);
t2=C_u_i_memq(lf[127],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5487,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7922,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:389: option-arg */
f_4890(t4,t2);}
else{
t4=t3;
f_5487(t4,C_SCHEME_FALSE);}}

/* k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in ... */
static void C_fcall f_5487(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,0,2)))){
C_save_and_reclaim_args((void *)trf_5487,2,t0,t1);}
a=C_alloc(40);
t2=C_u_i_memq(lf[128],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5492,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(t2)){
t4=C_set_block_item(lf[168] /* chicken.compiler.core#inline-locally */,0,C_SCHEME_TRUE);
t5=C_set_block_item(lf[438] /* chicken.compiler.core#local-definitions */,0,C_SCHEME_TRUE);
t6=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7918,a[2]=((C_word*)t0)[19],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:393: option-arg */
f_4890(t6,t2);}
else{
t4=t3;
f_5492(t4,C_SCHEME_FALSE);}}

/* k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in ... */
static void C_fcall f_5492(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,0,2)))){
C_save_and_reclaim_args((void *)trf_5492,2,t0,t1);}
a=C_alloc(40);
t2=C_u_i_memq(lf[129],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5497,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7912,a[2]=((C_word*)t0)[21],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:395: option-arg */
f_4890(t4,t2);}
else{
t4=t3;
f_5497(t4,C_SCHEME_FALSE);}}

/* k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in ... */
static void C_fcall f_5497(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,0,2)))){
C_save_and_reclaim_args((void *)trf_5497,2,t0,t1);}
a=C_alloc(39);
t2=C_u_i_memq(lf[130],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5502,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7899,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:398: option-arg */
f_4890(t4,t2);}
else{
t4=t3;
f_5502(t4,C_SCHEME_FALSE);}}

/* k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in ... */
static void C_fcall f_5502(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,0,2)))){
C_save_and_reclaim_args((void *)trf_5502,2,t0,t1);}
a=C_alloc(39);
t2=C_u_i_memq(lf[131],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5507,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[2],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(t2)){
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7886,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:404: option-arg */
f_4890(t4,t2);}
else{
t4=t3;
f_5507(t4,C_SCHEME_FALSE);}}

/* k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in ... */
static void C_fcall f_5507(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(42,0,4)))){
C_save_and_reclaim_args((void *)trf_5507,2,t0,t1);}
a=C_alloc(42);
t2=(*a=C_CLOSURE_TYPE|35,a[1]=(C_word)f_5510,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],a[35]=((C_word*)t0)[35],tmp=(C_word)a,a+=36,tmp);
if(C_truep(C_u_i_memq(lf[434],((C_word*)t0)[5]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7876,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9488,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[435];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5510(2,av2);}}}

/* k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in ... */
static void C_ccall f_5510(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,2)))){
C_save_and_reclaim((void *)f_5510,c,av);}
a=C_alloc(38);
t2=(*a=C_CLOSURE_TYPE|34,a[1]=(C_word)f_5513,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],tmp=(C_word)a,a+=35,tmp);
if(C_truep(((C_word*)t0)[35])){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7843,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:413: option-arg */
f_4890(t3,((C_word*)t0)[35]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5513(2,av2);}}}

/* k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in ... */
static void C_ccall f_5513(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,4)))){
C_save_and_reclaim((void *)f_5513,c,av);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|34,a[1]=(C_word)f_5516,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],tmp=(C_word)a,a+=35,tmp);
if(C_truep(C_u_i_memq(lf[426],((C_word*)t0)[5]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7837,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9482,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[427];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5516(2,av2);}}}

/* k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in ... */
static void C_ccall f_5516(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,4)))){
C_save_and_reclaim((void *)f_5516,c,av);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|34,a[1]=(C_word)f_5519,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],tmp=(C_word)a,a+=35,tmp);
if(C_truep(C_u_i_memq(lf[424],((C_word*)t0)[5]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7829,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9476,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[425];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5519(2,av2);}}}

/* k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in ... */
static void C_ccall f_5519(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,c,4)))){
C_save_and_reclaim((void *)f_5519,c,av);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|34,a[1]=(C_word)f_5522,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=((C_word*)t0)[33],a[34]=((C_word*)t0)[34],tmp=(C_word)a,a+=35,tmp);
if(C_truep(C_u_i_memq(lf[417],((C_word*)t0)[5]))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7812,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9470,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[423];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5522(2,av2);}}}

/* k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in ... */
static void C_ccall f_5522(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(45,c,2)))){
C_save_and_reclaim((void *)f_5522,c,av);}
a=C_alloc(45);
t2=C_mutate((C_word*)lf[132]+1 /* (set! chicken.compiler.core#verbose-mode ...) */,((C_word*)t0)[2]);
t3=C_set_block_item(lf[133] /* ##sys#read-error-with-line-number */,0,C_SCHEME_TRUE);
t4=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5528,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],tmp=(C_word)a,a+=33,tmp);
t5=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t6=t5;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=((C_word*)t7)[1];
t9=*((C_word*)lf[92]+1);
t10=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7767,a[2]=t4,a[3]=((C_word*)t0)[34],a[4]=t7,a[5]=t9,a[6]=t8,tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:434: collect-options */
t11=((C_word*)((C_word*)t0)[23])[1];
f_5220(t11,t10,lf[416]);}

/* k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in ... */
static void C_ccall f_5528(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,2)))){
C_save_and_reclaim((void *)f_5528,c,av);}
a=C_alloc(33);
t2=C_mutate((C_word*)lf[134]+1 /* (set! ##sys#include-pathnames ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5531,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(((C_word*)t0)[13])){
if(C_truep(((C_word*)t0)[6])){
if(C_truep(C_i_string_equal_p(((C_word*)t0)[13],((C_word*)t0)[6]))){
/* batch-driver.scm:438: chicken.compiler.support#quit-compiling */
t4=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[415];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5531(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5531(2,av2);}}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5531(2,av2);}}}

/* k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in ... */
static void C_ccall f_5531(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,2)))){
C_save_and_reclaim((void *)f_5531,c,av);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5534,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[413],((C_word*)t0)[4]))){
t3=C_set_block_item(lf[414] /* chicken.compiler.core#undefine-shadowed-macros */,0,C_SCHEME_FALSE);
t4=t2;
f_5534(t4,t3);}
else{
t3=t2;
f_5534(t3,C_SCHEME_UNDEFINED);}}

/* k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in ... */
static void C_fcall f_5534(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,2)))){
C_save_and_reclaim_args((void *)trf_5534,2,t0,t1);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5537,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[411],((C_word*)t0)[4]))){
t3=C_set_block_item(lf[412] /* chicken.compiler.core#no-argc-checks */,0,C_SCHEME_TRUE);
t4=t2;
f_5537(t4,t3);}
else{
t3=t2;
f_5537(t3,C_SCHEME_UNDEFINED);}}

/* k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in ... */
static void C_fcall f_5537(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,2)))){
C_save_and_reclaim_args((void *)trf_5537,2,t0,t1);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5540,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[409],((C_word*)t0)[4]))){
t3=C_set_block_item(lf[410] /* chicken.compiler.core#no-bound-checks */,0,C_SCHEME_TRUE);
t4=t2;
f_5540(t4,t3);}
else{
t3=t2;
f_5540(t3,C_SCHEME_UNDEFINED);}}

/* k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in ... */
static void C_fcall f_5540(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,2)))){
C_save_and_reclaim_args((void *)trf_5540,2,t0,t1);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5543,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[407],((C_word*)t0)[4]))){
t3=C_set_block_item(lf[408] /* chicken.compiler.core#no-procedure-checks */,0,C_SCHEME_TRUE);
t4=t2;
f_5543(t4,t3);}
else{
t3=t2;
f_5543(t3,C_SCHEME_UNDEFINED);}}

/* k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in ... */
static void C_fcall f_5543(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,2)))){
C_save_and_reclaim_args((void *)trf_5543,2,t0,t1);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5546,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[405],((C_word*)t0)[4]))){
t3=C_set_block_item(lf[406] /* chicken.compiler.core#no-global-procedure-checks */,0,C_SCHEME_TRUE);
t4=t2;
f_5546(t4,t3);}
else{
t3=t2;
f_5546(t3,C_SCHEME_UNDEFINED);}}

/* k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in ... */
static void C_fcall f_5546(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(41,0,3)))){
C_save_and_reclaim_args((void *)trf_5546,2,t0,t1);}
a=C_alloc(41);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5549,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_u_i_memq(lf[402],((C_word*)t0)[4]))){
t3=*((C_word*)lf[76]+1);
t4=C_i_check_list_2(*((C_word*)lf[76]+1),lf[10]);
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7673,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t6=C_SCHEME_UNDEFINED;
t7=(*a=C_VECTOR_TYPE|1,a[1]=t6,tmp=(C_word)a,a+=2,tmp);
t8=C_set_block_item(t7,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7712,a[2]=t7,tmp=(C_word)a,a+=3,tmp));
t9=((C_word*)t7)[1];
f_7712(t9,t5,*((C_word*)lf[76]+1));}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5549(2,av2);}}}

/* k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in ... */
static void C_ccall f_5549(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,c,2)))){
C_save_and_reclaim((void *)f_5549,c,av);}
a=C_alloc(33);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5552,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
if(C_truep(C_i_memq(lf[106],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:460: chicken.load#load-verbose */
t3=*((C_word*)lf[401]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5552(2,av2);}}}

/* k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in ... */
static void C_ccall f_5552(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,c,3)))){
C_save_and_reclaim((void *)f_5552,c,av);}
a=C_alloc(40);
t2=*((C_word*)lf[135]+1);
t3=(*a=C_CLOSURE_TYPE|33,a[1]=(C_word)f_5555,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=t2,tmp=(C_word)a,a+=34,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7643,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7651,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:465: collect-options */
t6=((C_word*)((C_word*)t0)[22])[1];
f_5220(t6,t5,lf[400]);}

/* k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in ... */
static void C_ccall f_5555(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,3)))){
C_save_and_reclaim((void *)f_5555,c,av);}
a=C_alloc(39);
t2=C_i_check_list_2(t1,lf[10]);
t3=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5561,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7620,a[2]=t5,a[3]=((C_word*)t0)[33],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_7620(t7,t3,t1);}

/* k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in ... */
static void C_ccall f_5561(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,c,3)))){
C_save_and_reclaim((void *)f_5561,c,av);}
a=C_alloc(40);
t2=*((C_word*)lf[136]+1);
t3=(*a=C_CLOSURE_TYPE|33,a[1]=(C_word)f_5564,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],a[33]=t2,tmp=(C_word)a,a+=34,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7610,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7618,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:468: collect-options */
t6=((C_word*)((C_word*)t0)[22])[1];
f_5220(t6,t5,lf[398]);}

/* k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in ... */
static void C_ccall f_5564(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,3)))){
C_save_and_reclaim((void *)f_5564,c,av);}
a=C_alloc(39);
t2=C_i_check_list_2(t1,lf[10]);
t3=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5570,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7587,a[2]=t5,a[3]=((C_word*)t0)[33],tmp=(C_word)a,a+=4,tmp));
t7=((C_word*)t5)[1];
f_7587(t7,t3,t1);}

/* k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in ... */
static void C_ccall f_5570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,2)))){
C_save_and_reclaim((void *)f_5570,c,av);}
a=C_alloc(36);
t2=C_a_i_cons(&a,2,lf[137],*((C_word*)lf[138]+1));
t3=C_mutate((C_word*)lf[138]+1 /* (set! ##sys#features ...) */,t2);
t4=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5577,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
/* batch-driver.scm:472: collect-options */
t5=((C_word*)((C_word*)t0)[22])[1];
f_5220(t5,t4,lf[396]);}

/* k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in ... */
static void C_ccall f_5577(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(37,c,4)))){
C_save_and_reclaim((void *)f_5577,c,av);}
a=C_alloc(37);
t2=(*a=C_CLOSURE_TYPE|33,a[1]=(C_word)f_5580,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],a[17]=((C_word*)t0)[16],a[18]=((C_word*)t0)[17],a[19]=((C_word*)t0)[18],a[20]=((C_word*)t0)[19],a[21]=((C_word*)t0)[20],a[22]=((C_word*)t0)[21],a[23]=((C_word*)t0)[22],a[24]=((C_word*)t0)[23],a[25]=((C_word*)t0)[24],a[26]=((C_word*)t0)[25],a[27]=((C_word*)t0)[26],a[28]=((C_word*)t0)[27],a[29]=((C_word*)t0)[28],a[30]=((C_word*)t0)[29],a[31]=((C_word*)t0)[30],a[32]=((C_word*)t0)[31],a[33]=((C_word*)t0)[32],tmp=(C_word)a,a+=34,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9454,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t3;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[395];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}

/* k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in ... */
static void C_ccall f_5580(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,3)))){
C_save_and_reclaim((void *)f_5580,c,av);}
a=C_alloc(38);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[10]);
t3=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5603,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],a[32]=((C_word*)t0)[33],tmp=(C_word)a,a+=33,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7564,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_7564(t7,t3,((C_word*)t0)[2]);}

/* k5583 in for-each-loop1474 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in ... */
static void C_ccall f_5585(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,3)))){
C_save_and_reclaim((void *)f_5585,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5588,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
if(C_truep(C_i_not(t1))){
/* batch-driver.scm:477: chicken.compiler.support#quit-compiling */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[394];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}
else{
/* batch-driver.scm:478: scheme#load */
t3=*((C_word*)lf[393]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}

/* k5586 in k5583 in for-each-loop1474 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in ... */
static void C_ccall f_5588(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5588,c,av);}
/* batch-driver.scm:478: scheme#load */
t2=*((C_word*)lf[393]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in ... */
static void C_ccall f_5603(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,3)))){
C_save_and_reclaim((void *)f_5603,c,av);}
a=C_alloc(38);
t2=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5607,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
t3=*((C_word*)lf[138]+1);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3267,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_3267(t7,t2,*((C_word*)lf[138]+1));}

/* k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in ... */
static void C_ccall f_5607(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,2)))){
C_save_and_reclaim((void *)f_5607,c,av);}
a=C_alloc(36);
t2=C_mutate((C_word*)lf[138]+1 /* (set! ##sys#features ...) */,t1);
t3=C_a_i_cons(&a,2,lf[139],*((C_word*)lf[138]+1));
t4=C_mutate((C_word*)lf[138]+1 /* (set! ##sys#features ...) */,t3);
t5=(*a=C_CLOSURE_TYPE|32,a[1]=(C_word)f_5615,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],a[32]=((C_word*)t0)[32],tmp=(C_word)a,a+=33,tmp);
/* batch-driver.scm:482: chicken.compiler.user-pass#user-post-analysis-pass */
t6=*((C_word*)lf[392]+1);{
C_word *av2=av;
av2[0]=t6;
av2[1]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(2,av2);}}

/* k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in ... */
static void C_ccall f_5615(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,3)))){
C_save_and_reclaim((void *)f_5615,c,av);}
a=C_alloc(38);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|31,a[1]=(C_word)f_5618,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[26],a[26]=((C_word*)t0)[27],a[27]=((C_word*)t0)[28],a[28]=((C_word*)t0)[29],a[29]=((C_word*)t0)[30],a[30]=((C_word*)t0)[31],a[31]=((C_word*)t0)[32],tmp=(C_word)a,a+=32,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_7514,tmp=(C_word)a,a+=2,tmp);
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7562,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:487: collect-options */
t6=((C_word*)((C_word*)t0)[22])[1];
f_5220(t6,t5,lf[388]);}

/* k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in ... */
static void C_ccall f_5618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(44,c,2)))){
C_save_and_reclaim((void *)f_5618,c,av);}
a=C_alloc(44);
t2=(*a=C_CLOSURE_TYPE|31,a[1]=(C_word)f_5621,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],tmp=(C_word)a,a+=32,tmp);
if(C_truep(C_i_nullp(t1))){
t3=t2;
f_5621(t3,C_SCHEME_UNDEFINED);}
else{
t3=C_a_i_cons(&a,2,lf[388],t1);
t4=C_a_i_list(&a,2,lf[389],t3);
t5=C_a_i_cons(&a,2,t4,((C_word*)((C_word*)t0)[26])[1]);
t6=C_mutate(((C_word *)((C_word*)t0)[26])+1,t5);
t7=t2;
f_5621(t7,t6);}}

/* k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in ... */
static void C_fcall f_5621(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(43,0,2)))){
C_save_and_reclaim_args((void *)trf_5621,2,t0,t1);}
a=C_alloc(43);
t2=(*a=C_CLOSURE_TYPE|31,a[1]=(C_word)f_5625,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],tmp=(C_word)a,a+=32,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7457,a[2]=t2,a[3]=((C_word*)t0)[2],a[4]=t5,a[5]=t6,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:497: collect-options */
t8=((C_word*)((C_word*)t0)[21])[1];
f_5220(t8,t7,lf[387]);}

/* k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in ... */
static void C_ccall f_5625(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,2)))){
C_save_and_reclaim((void *)f_5625,c,av);}
a=C_alloc(32);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|31,a[1]=(C_word)f_5628,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],a[14]=((C_word*)t0)[15],a[15]=((C_word*)t0)[16],a[16]=((C_word*)t0)[17],a[17]=((C_word*)t0)[18],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[21],a[21]=((C_word*)t0)[22],a[22]=((C_word*)t0)[23],a[23]=((C_word*)t0)[24],a[24]=((C_word*)t0)[25],a[25]=((C_word*)t0)[2],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],a[31]=((C_word*)t0)[31],tmp=(C_word)a,a+=32,tmp);
if(C_truep(C_u_i_memq(lf[384],((C_word*)t0)[3]))){
t4=C_set_block_item(lf[385] /* ##sys#enable-runtime-macros */,0,C_SCHEME_TRUE);
t5=t3;
f_5628(t5,t4);}
else{
t4=t3;
f_5628(t4,C_SCHEME_UNDEFINED);}}

/* k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_fcall f_5628(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,0,2)))){
C_save_and_reclaim_args((void *)trf_5628,2,t0,t1);}
a=C_alloc(35);
t2=(*a=C_CLOSURE_TYPE|30,a[1]=(C_word)f_5632,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=((C_word*)t0)[28],a[29]=((C_word*)t0)[29],a[30]=((C_word*)t0)[30],tmp=(C_word)a,a+=31,tmp);
if(C_truep(((C_word*)t0)[31])){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7438,a[2]=((C_word*)t0)[29],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:503: option-arg */
f_4890(t3,((C_word*)t0)[31]);}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_5632(2,av2);}}}

/* k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_5632(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,2)))){
C_save_and_reclaim((void *)f_5632,c,av);}
a=C_alloc(32);
t2=C_mutate((C_word*)lf[140]+1 /* (set! chicken.compiler.core#target-heap-size ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5636,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],tmp=(C_word)a,a+=28,tmp);
if(C_truep(((C_word*)t0)[28])){
t4=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7431,a[2]=((C_word*)t0)[29],a[3]=t3,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:506: option-arg */
f_4890(t4,((C_word*)t0)[28]);}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_FALSE;
f_5636(2,av2);}}}

/* k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_5636(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_5636,c,av);}
a=C_alloc(28);
t2=C_mutate((C_word*)lf[141]+1 /* (set! chicken.compiler.core#target-stack-size ...) */,t1);
t3=C_i_not(C_u_i_memq(lf[142],((C_word*)t0)[2]));
t4=C_set_block_item(lf[143] /* chicken.compiler.core#emit-trace-info */,0,t3);
t5=C_mutate((C_word*)lf[144]+1 /* (set! chicken.compiler.core#disable-stack-overflow-checking ...) */,C_u_i_memq(lf[145],((C_word*)t0)[2]));
t6=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5647,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],tmp=(C_word)a,a+=28,tmp);
/* batch-driver.scm:509: chicken.platform#feature? */
t7=*((C_word*)lf[382]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[383];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}

/* k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in ... */
static void C_ccall f_5647(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_5647,c,av);}
a=C_alloc(28);
t2=C_set_block_item(lf[146] /* chicken.compiler.core#bootstrap-mode */,0,t1);
t3=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5650,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],tmp=(C_word)a,a+=28,tmp);
if(C_truep(C_i_memq(lf[380],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:510: chicken.gc#set-gc-report! */
t4=*((C_word*)lf[381]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_5650(2,av2);}}}

/* k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in ... */
static void C_ccall f_5650(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_5650,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5653,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],tmp=(C_word)a,a+=28,tmp);
if(C_truep(C_u_i_memq(lf[379],((C_word*)t0)[2]))){
t3=C_set_block_item(((C_word*)t0)[19],0,C_SCHEME_FALSE);
t4=t2;
f_5653(t4,t3);}
else{
t3=C_mutate((C_word*)lf[9]+1 /* (set! chicken.compiler.core#standard-bindings ...) */,*((C_word*)lf[76]+1));
t4=C_mutate((C_word*)lf[11]+1 /* (set! chicken.compiler.core#extended-bindings ...) */,*((C_word*)lf[77]+1));
t5=t2;
f_5653(t5,t4);}}

/* k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in ... */
static void C_fcall f_5653(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,0,4)))){
C_save_and_reclaim_args((void *)trf_5653,2,t0,t1);}
a=C_alloc(34);
t2=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5656,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],tmp=(C_word)a,a+=28,tmp);
if(C_truep(*((C_word*)lf[143]+1))){
t3=C_a_i_list(&a,1,lf[376]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9434,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[377];
av2[4]=t3;
C_apply(5,av2);}}
else{
t3=C_a_i_list(&a,1,lf[378]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9440,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[377];
av2[4]=t3;
C_apply(5,av2);}}}

/* k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in ... */
static void C_ccall f_5656(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,2)))){
C_save_and_reclaim((void *)f_5656,c,av);}
a=C_alloc(32);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5659,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
if(C_truep(((C_word*)t0)[27])){
t3=C_i_car(((C_word*)t0)[27]);
t4=C_eqp(lf[367],t3);
t5=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7376,a[2]=((C_word*)t0)[25],a[3]=t4,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
if(C_truep(t4)){
if(C_truep(C_i_not(((C_word*)t0)[23]))){
/* batch-driver.scm:523: chicken.compiler.support#quit-compiling */
t6=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t6;
av2[1]=t5;
av2[2]=lf[375];
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_7376(2,av2);}}}
else{
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_7376(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5659(2,av2);}}}

/* k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in ... */
static void C_ccall f_5659(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5659,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5662,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:537: chicken.compiler.support#load-identifier-database */
t3=*((C_word*)lf[365]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[366];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in ... */
static void C_ccall f_5662(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5662,c,av);}
a=C_alloc(27);
if(C_truep(C_u_i_memq(lf[147],((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5670,a[2]=((C_word*)t0)[3],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:540: chicken.compiler.support#print-version */
t3=*((C_word*)lf[148]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=C_u_i_memq(lf[149],((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5681,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
if(C_truep(t2)){
t4=t3;
f_5681(t4,t2);}
else{
t4=C_u_i_memq(lf[362],((C_word*)t0)[2]);
if(C_truep(t4)){
t5=t3;
f_5681(t5,t4);}
else{
t5=C_u_i_memq(lf[363],((C_word*)t0)[2]);
t6=t3;
f_5681(t6,(C_truep(t5)?t5:C_u_i_memq(lf[364],((C_word*)t0)[2])));}}}}

/* k5668 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in ... */
static void C_ccall f_5670(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5670,c,av);}
/* batch-driver.scm:541: scheme#newline */
t2=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in ... */
static void C_fcall f_5681(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(33,0,4)))){
C_save_and_reclaim_args((void *)trf_5681,2,t0,t1);}
a=C_alloc(33);
if(C_truep(t1)){
/* batch-driver.scm:543: chicken.compiler.support#print-usage */
t2=*((C_word*)lf[150]+1);{
C_word av2[2];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
if(C_truep(C_u_i_memq(lf[151],((C_word*)t0)[3]))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5692,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5699,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:545: chicken.platform#chicken-version */
t4=*((C_word*)lf[153]+1);{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
if(C_truep(C_i_not(((C_word*)t0)[4]))){
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5708,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:548: chicken.compiler.support#print-version */
t3=*((C_word*)lf[148]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5720,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[12],a[11]=((C_word*)t0)[13],a[12]=((C_word*)t0)[14],a[13]=((C_word*)t0)[15],a[14]=((C_word*)t0)[16],a[15]=((C_word*)t0)[17],a[16]=((C_word*)t0)[18],a[17]=((C_word*)t0)[2],a[18]=((C_word*)t0)[19],a[19]=((C_word*)t0)[20],a[20]=((C_word*)t0)[3],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
t3=C_a_i_list(&a,1,((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9416,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[361];
av2[4]=t3;
C_apply(5,av2);}}}}}

/* k5690 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in ... */
static void C_ccall f_5692(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5692,c,av);}
/* batch-driver.scm:546: scheme#newline */
t2=*((C_word*)lf[19]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* k5697 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in ... */
static void C_ccall f_5699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5699,c,av);}
/* batch-driver.scm:545: scheme#display */
t2=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5706 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in ... */
static void C_ccall f_5708(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5708,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5711,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:549: scheme#display */
t3=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[156];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5709 in k5706 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in ... */
static void C_ccall f_5711(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_5711,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5714,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:550: scheme#display */
t3=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[155];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5712 in k5709 in k5706 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in ... */
static void C_ccall f_5714(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5714,c,av);}
/* batch-driver.scm:551: scheme#display */
t2=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[154];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in ... */
static void C_ccall f_5720(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_5720,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5723,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:556: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=lf[360];
av2[4]=((C_word*)t0)[20];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in ... */
static void C_ccall f_5723(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_5723,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5726,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:557: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=lf[359];
av2[4]=*((C_word*)lf[104]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in ... */
static void C_ccall f_5726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_5726,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5729,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:558: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=lf[358];
av2[4]=*((C_word*)lf[140]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in ... */
static void C_ccall f_5729(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,4)))){
C_save_and_reclaim((void *)f_5729,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5732,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:559: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[356];
av2[3]=lf[357];
av2[4]=*((C_word*)lf[141]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in ... */
static void C_ccall f_5732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5732,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5736,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:257: chicken.time#current-milliseconds */
t3=*((C_word*)lf[115]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in ... */
static void C_ccall f_5736(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,3)))){
C_save_and_reclaim((void *)f_5736,c,av);}
a=C_alloc(27);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5740,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[2],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:563: scheme#make-vector */
t4=*((C_word*)lf[354]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=*((C_word*)lf[355]+1);
av2[3]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in ... */
static void C_ccall f_5740(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5740,c,av);}
a=C_alloc(27);
t2=C_mutate((C_word*)lf[157]+1 /* (set! ##sys#line-number-database ...) */,t1);
t3=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5743,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:564: collect-options */
t4=((C_word*)((C_word*)t0)[19])[1];
f_5220(t4,t3,lf[353]);}

/* k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in ... */
static void C_ccall f_5743(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,2)))){
C_save_and_reclaim((void *)f_5743,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5746,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=t1,tmp=(C_word)a,a+=28,tmp);
/* batch-driver.scm:565: collect-options */
t3=((C_word*)((C_word*)t0)[19])[1];
f_5220(t3,t2,lf[352]);}

/* k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in ... */
static void C_ccall f_5746(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(34,c,2)))){
C_save_and_reclaim((void *)f_5746,c,av);}
a=C_alloc(34);
t2=(*a=C_CLOSURE_TYPE|28,a[1]=(C_word)f_5749,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=t1,tmp=(C_word)a,a+=29,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7350,a[2]=((C_word*)t0)[2],a[3]=t2,a[4]=((C_word*)t0)[19],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:567: collect-options */
t4=((C_word*)((C_word*)t0)[19])[1];
f_5220(t4,t3,lf[351]);}

/* k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in ... */
static void C_ccall f_5749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,2)))){
C_save_and_reclaim((void *)f_5749,c,av);}
a=C_alloc(30);
t2=(*a=C_CLOSURE_TYPE|29,a[1]=(C_word)f_5752,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],a[27]=((C_word*)t0)[27],a[28]=t1,a[29]=((C_word*)t0)[28],tmp=(C_word)a,a+=30,tmp);
/* batch-driver.scm:571: chicken.compiler.user-pass#user-read-pass */
t3=*((C_word*)lf[349]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in ... */
static void C_ccall f_5752(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(38,c,4)))){
C_save_and_reclaim((void *)f_5752,c,av);}
a=C_alloc(38);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5755,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_7182,a[2]=((C_word*)t0)[26],a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[27],a[6]=((C_word*)t0)[28],a[7]=((C_word*)t0)[29],tmp=(C_word)a,a+=8,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9402,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[343];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_set_block_item(t4,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7191,a[2]=((C_word*)t0)[26],a[3]=((C_word*)t0)[27],a[4]=((C_word*)t0)[29],a[5]=t4,tmp=(C_word)a,a+=6,tmp));
t6=((C_word*)t4)[1];
f_7191(t6,t2,((C_word*)t0)[28]);}}

/* k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in ... */
static void C_ccall f_5755(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5755,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5758,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:594: chicken.compiler.user-pass#user-preprocessor-pass */
t3=*((C_word*)lf[342]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_ccall f_5758(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,4)))){
C_save_and_reclaim((void *)f_5758,c,av);}
a=C_alloc(35);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5761,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7135,a[2]=((C_word*)t0)[26],a[3]=t2,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9396,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[341];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;
f_5761(t3,C_SCHEME_UNDEFINED);}}

/* k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_fcall f_5761(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,0,4)))){
C_save_and_reclaim_args((void *)trf_5761,2,t0,t1);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5764,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:599: print-expr */
t3=((C_word*)((C_word*)t0)[21])[1];
f_5089(t3,t2,lf[339],lf[340],((C_word*)((C_word*)t0)[26])[1]);}

/* k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_ccall f_5764(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5764,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5767,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
/* batch-driver.scm:600: begin-time */
t3=((C_word*)((C_word*)t0)[5])[1];
f_5255(t3,t2);}

/* k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in ... */
static void C_ccall f_5767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(36,c,3)))){
C_save_and_reclaim((void *)f_5767,c,av);}
a=C_alloc(36);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5771,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
t7=(*a=C_CLOSURE_TYPE|27,a[1]=(C_word)f_5794,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[2],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=t4,a[25]=t6,a[26]=t5,a[27]=((C_word*)t0)[24],tmp=(C_word)a,a+=28,tmp);
/* batch-driver.scm:605: scheme#append */
t8=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=((C_word*)((C_word*)t0)[25])[1];
av2[3]=((C_word*)((C_word*)t0)[26])[1];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* g1699 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in ... */
static void C_fcall f_5771(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,4)))){
C_save_and_reclaim_args((void *)trf_5771,3,t0,t1,t2);}
a=C_alloc(15);
t3=((C_word*)t0)[2];
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_SCHEME_FALSE;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5777,a[2]=t6,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_5782,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5788,a[2]=t4,a[3]=t6,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:603: ##sys#dynamic-wind */
t10=*((C_word*)lf[160]+1);{
C_word av2[5];
av2[0]=t10;
av2[1]=t1;
av2[2]=t7;
av2[3]=t8;
av2[4]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}

/* a5776 in g1699 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in ... */
static void C_ccall f_5777(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5777,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[158]+1));
t3=C_mutate((C_word*)lf[158]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a5781 in g1699 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in ... */
static void C_ccall f_5782(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_5782,c,av);}
/* batch-driver.scm:604: chicken.compiler.core#canonicalize-expression */
t2=*((C_word*)lf[159]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* a5787 in g1699 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in ... */
static void C_ccall f_5788(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_5788,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[158]+1));
t3=C_mutate((C_word*)lf[158]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in ... */
static void C_ccall f_5794(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(31,c,2)))){
C_save_and_reclaim((void *)f_5794,c,av);}
a=C_alloc(31);
t2=(*a=C_CLOSURE_TYPE|26,a[1]=(C_word)f_5797,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],a[24]=((C_word*)t0)[24],a[25]=((C_word*)t0)[25],a[26]=((C_word*)t0)[26],tmp=(C_word)a,a+=27,tmp);
if(C_truep(C_i_not(((C_word*)t0)[27]))){
t3=t2;
f_5797(t3,t1);}
else{
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7128,a[2]=t1,a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:609: scheme#string->symbol */
t4=*((C_word*)lf[338]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=((C_word*)t0)[27];
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}}

/* k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in ... */
static void C_fcall f_5797(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,0,3)))){
C_save_and_reclaim_args((void *)trf_5797,2,t0,t1);}
a=C_alloc(32);
t2=C_i_check_list_2(t1,lf[5]);
t3=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5803,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7078,a[2]=((C_word*)t0)[24],a[3]=t5,a[4]=((C_word*)t0)[25],a[5]=((C_word*)t0)[26],tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_7078(t7,t3,t1);}

/* k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in ... */
static void C_ccall f_5803(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(40,c,3)))){
C_save_and_reclaim((void *)f_5803,c,av);}
a=C_alloc(40);
t2=(*a=C_CLOSURE_TYPE|21,a[1]=(C_word)f_5806,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],tmp=(C_word)a,a+=22,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=*((C_word*)lf[328]+1);
t8=C_i_check_list_2(*((C_word*)lf[328]+1),lf[5]);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6961,a[2]=((C_word*)t0)[22],a[3]=t2,a[4]=t1,a[5]=((C_word*)t0)[23],tmp=(C_word)a,a+=6,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7044,a[2]=t5,a[3]=t11,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t13=((C_word*)t11)[1];
f_7044(t13,t9,*((C_word*)lf[328]+1));}

/* k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in ... */
static void C_ccall f_5806(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word t15;
C_word t16;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(43,c,3)))){
C_save_and_reclaim((void *)f_5806,c,av);}
a=C_alloc(43);
t2=t1;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5809,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=t3,a[22]=((C_word*)t0)[21],tmp=(C_word)a,a+=23,tmp);
if(C_truep(C_i_nullp(*((C_word*)lf[126]+1)))){
t5=t4;{
C_word *av2=av;
av2[0]=t5;
av2[1]=C_SCHEME_UNDEFINED;
f_5809(2,av2);}}
else{
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6883,a[2]=t4,tmp=(C_word)a,a+=3,tmp);
t6=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t7=t6;
t8=(*a=C_VECTOR_TYPE|1,a[1]=t7,tmp=(C_word)a,a+=2,tmp);
t9=((C_word*)t8)[1];
t10=*((C_word*)lf[126]+1);
t11=C_i_check_list_2(*((C_word*)lf[126]+1),lf[5]);
t12=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6902,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
t13=C_SCHEME_UNDEFINED;
t14=(*a=C_VECTOR_TYPE|1,a[1]=t13,tmp=(C_word)a,a+=2,tmp);
t15=C_set_block_item(t14,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6904,a[2]=t8,a[3]=t14,a[4]=t9,tmp=(C_word)a,a+=5,tmp));
t16=((C_word*)t14)[1];
f_6904(t16,t12,*((C_word*)lf[126]+1));}}

/* k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_ccall f_5809(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,3)))){
C_save_and_reclaim((void *)f_5809,c,av);}
a=C_alloc(23);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5812,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
/* batch-driver.scm:632: chicken.internal#hash-table-ref */
t3=*((C_word*)lf[325]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=*((C_word*)lf[281]+1);
av2[3]=lf[86];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in ... */
static void C_ccall f_5812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,3)))){
C_save_and_reclaim((void *)f_5812,c,av);}
a=C_alloc(30);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5815,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6707,a[2]=t2,a[3]=t1,a[4]=((C_word*)t0)[10],tmp=(C_word)a,a+=5,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_3480,tmp=(C_word)a,a+=2,tmp);
/* mini-srfi-1.scm:141: filter */
f_3381(t3,t4,t1);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_FALSE;
f_5815(2,av2);}}}

/* k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in ... */
static void C_ccall f_5815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,3)))){
C_save_and_reclaim((void *)f_5815,c,av);}
a=C_alloc(25);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5818,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
if(C_truep(C_i_pairp(*((C_word*)lf[300]+1)))){
t3=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6645,tmp=(C_word)a,a+=2,tmp);
/* batch-driver.scm:645: chicken.compiler.support#with-debugging-output */
t4=*((C_word*)lf[305]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t2;
av2[2]=lf[306];
av2[3]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5818(2,av2);}}}

/* k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_5818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_5818,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5821,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6634,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:652: chicken.compiler.support#debugging */
t4=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[298];
av2[3]=lf[299];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static void C_ccall f_5821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,3)))){
C_save_and_reclaim((void *)f_5821,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5824,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6628,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:654: chicken.compiler.support#debugging */
t4=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[295];
av2[3]=lf[296];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in ... */
static void C_ccall f_5824(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,2)))){
C_save_and_reclaim((void *)f_5824,c,av);}
a=C_alloc(23);
t2=C_mutate((C_word*)lf[157]+1 /* (set! ##sys#line-number-database ...) */,*((C_word*)lf[161]+1));
t3=C_set_block_item(lf[161] /* chicken.compiler.core#line-number-database-2 */,0,C_SCHEME_FALSE);
t4=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5829,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
/* batch-driver.scm:660: end-time */
t5=((C_word*)((C_word*)t0)[3])[1];
f_5265(t5,t4,lf[293]);}

/* k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_5829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_5829,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|21,a[1]=(C_word)f_5832,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],tmp=(C_word)a,a+=22,tmp);
/* batch-driver.scm:661: print-expr */
t3=((C_word*)((C_word*)t0)[22])[1];
f_5089(t3,t2,lf[291],lf[292],((C_word*)((C_word*)t0)[21])[1]);}

/* k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_5832(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,2)))){
C_save_and_reclaim((void *)f_5832,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|21,a[1]=(C_word)f_5835,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],tmp=(C_word)a,a+=22,tmp);
if(C_truep(C_u_i_memq(lf[290],((C_word*)t0)[20]))){
/* batch-driver.scm:663: chicken.base#exit */
t3=*((C_word*)lf[201]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5835(2,av2);}}}

/* k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in ... */
static void C_ccall f_5835(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,2)))){
C_save_and_reclaim((void *)f_5835,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|21,a[1]=(C_word)f_5838,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],tmp=(C_word)a,a+=22,tmp);
/* batch-driver.scm:666: chicken.compiler.user-pass#user-pass */
t3=*((C_word*)lf[289]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in ... */
static void C_ccall f_5838(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(32,c,4)))){
C_save_and_reclaim((void *)f_5838,c,av);}
a=C_alloc(32);
t2=(*a=C_CLOSURE_TYPE|21,a[1]=(C_word)f_5841,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],tmp=(C_word)a,a+=22,tmp);
if(C_truep(t1)){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6570,a[2]=((C_word*)t0)[21],a[3]=((C_word*)t0)[3],a[4]=t2,a[5]=t1,a[6]=((C_word*)t0)[4],tmp=(C_word)a,a+=7,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9372,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[288];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5841(2,av2);}}}

/* k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in ... */
static void C_ccall f_5841(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(27,c,2)))){
C_save_and_reclaim((void *)f_5841,c,av);}
a=C_alloc(27);
t2=(*a=C_CLOSURE_TYPE|20,a[1]=(C_word)f_5844,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],tmp=(C_word)a,a+=21,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6563,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6567,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:676: chicken.compiler.support#canonicalize-begin-body */
t5=*((C_word*)lf[286]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)((C_word*)t0)[21])[1];
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}

/* k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in ... */
static void C_ccall f_5844(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(25,c,4)))){
C_save_and_reclaim((void *)f_5844,c,av);}
a=C_alloc(25);
t2=C_SCHEME_FALSE;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5847,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=t1,a[19]=((C_word*)t0)[18],a[20]=t3,a[21]=((C_word*)t0)[19],a[22]=((C_word*)t0)[20],tmp=(C_word)a,a+=23,tmp);
/* batch-driver.scm:678: print-node */
t5=((C_word*)((C_word*)t0)[6])[1];
f_5040(t5,t4,lf[282],lf[283],t1);}

/* k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in ... */
static void C_ccall f_5847(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(23,c,2)))){
C_save_and_reclaim((void *)f_5847,c,av);}
a=C_alloc(23);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5850,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
/* batch-driver.scm:679: initialize-analysis-database */
t3=lf[8];
f_4149(t3,t2);}

/* k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in ... */
static void C_ccall f_5850(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,c,2)))){
C_save_and_reclaim((void *)f_5850,c,av);}
a=C_alloc(26);
t2=(*a=C_CLOSURE_TYPE|22,a[1]=(C_word)f_5853,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],tmp=(C_word)a,a+=23,tmp);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6559,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:682: scheme#vector->list */
t4=*((C_word*)lf[280]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=*((C_word*)lf[281]+1);
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}

/* k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in ... */
static void C_ccall f_5853(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(39,c,3)))){
C_save_and_reclaim((void *)f_5853,c,av);}
a=C_alloc(39);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5856,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=t1,tmp=(C_word)a,a+=24,tmp);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=C_i_check_list_2(t1,lf[5]);
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6521,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6523,a[2]=t5,a[3]=t10,a[4]=t6,tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_6523(t12,t8,t1);}

/* k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in ... */
static void C_ccall f_5856(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,3)))){
C_save_and_reclaim((void *)f_5856,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5859,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=t1,a[21]=((C_word*)t0)[20],a[22]=((C_word*)t0)[21],a[23]=((C_word*)t0)[22],tmp=(C_word)a,a+=24,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6508,a[2]=t2,a[3]=((C_word*)t0)[23],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:684: chicken.compiler.support#debugging */
t4=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[278];
av2[3]=lf[279];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in ... */
static void C_ccall f_5859(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,3)))){
C_save_and_reclaim((void *)f_5859,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5862,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
if(C_truep(*((C_word*)lf[274]+1))){
t3=C_i_check_list_2(((C_word*)t0)[20],lf[10]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6484,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6484(t7,t2,((C_word*)t0)[20]);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5862(2,av2);}}}

/* k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in ... */
static void C_ccall f_5862(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_5862,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5865,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
/* batch-driver.scm:694: collect-options */
t3=((C_word*)((C_word*)t0)[22])[1];
f_5220(t3,t2,lf[273]);}

/* k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in ... */
static void C_ccall f_5865(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,3)))){
C_save_and_reclaim((void *)f_5865,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5868,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
if(C_truep(C_i_nullp(t1))){
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5868(2,av2);}}
else{
t3=C_set_block_item(lf[168] /* chicken.compiler.core#inline-locally */,0,C_SCHEME_TRUE);
t4=C_i_check_list_2(t1,lf[10]);
t5=C_SCHEME_UNDEFINED;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=C_set_block_item(t6,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6437,a[2]=t6,tmp=(C_word)a,a+=3,tmp));
t8=((C_word*)t6)[1];
f_6437(t8,t2,t1);}}

/* k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in ... */
static void C_ccall f_5868(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(35,c,3)))){
C_save_and_reclaim((void *)f_5868,c,av);}
a=C_alloc(35);
t2=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_5871,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],tmp=(C_word)a,a+=19,tmp);
t3=((C_word*)((C_word*)t0)[19])[1];
t4=(C_truep(t3)?t3:*((C_word*)lf[123]+1));
if(C_truep(t4)){
t5=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6288,a[2]=((C_word*)t0)[20],a[3]=((C_word*)t0)[21],a[4]=t2,a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[18],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[19],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[13],a[11]=((C_word*)t0)[5],a[12]=((C_word*)t0)[22],tmp=(C_word)a,a+=13,tmp);
if(C_truep(C_u_i_memq(lf[268],((C_word*)t0)[23]))){
t6=t5;{
C_word *av2=av;
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_6288(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6414,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:706: chicken.compiler.scrutinizer#load-type-database */
t7=*((C_word*)lf[262]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t7;
av2[1]=t6;
av2[2]=lf[270];
av2[3]=*((C_word*)lf[123]+1);
((C_proc)(void*)(*((C_word*)t7+1)))(4,av2);}}}
else{
t5=t2;
f_5871(t5,C_SCHEME_UNDEFINED);}}

/* k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_fcall f_5871(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,2)))){
C_save_and_reclaim_args((void *)trf_5871,2,t0,t1);}
a=C_alloc(22);
t2=C_set_block_item(lf[157] /* ##sys#line-number-database */,0,C_SCHEME_FALSE);
t3=C_set_block_item(lf[162] /* chicken.compiler.core#constant-table */,0,C_SCHEME_FALSE);
t4=C_set_block_item(lf[163] /* chicken.compiler.core#inline-table */,0,C_SCHEME_FALSE);
t5=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_5877,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],tmp=(C_word)a,a+=19,tmp);
if(C_truep(*((C_word*)lf[249]+1))){
t6=t5;{
C_word av2[2];
av2[0]=t6;
av2[1]=C_SCHEME_UNDEFINED;
f_5877(2,av2);}}
else{
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6282,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:742: chicken.compiler.support#node-subexpressions */
t7=*((C_word*)lf[251]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=((C_word*)t0)[18];
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}}

/* k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in ... */
static void C_ccall f_5877(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_5877,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_5880,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],tmp=(C_word)a,a+=19,tmp);
/* batch-driver.scm:744: begin-time */
t3=((C_word*)((C_word*)t0)[4])[1];
f_5255(t3,t2);}

/* k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in ... */
static void C_ccall f_5880(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,2)))){
C_save_and_reclaim((void *)f_5880,c,av);}
a=C_alloc(18);
t2=(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_5883,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],tmp=(C_word)a,a+=18,tmp);
/* batch-driver.scm:746: chicken.compiler.core#perform-cps-conversion */
t3=*((C_word*)lf[248]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[18];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in ... */
static void C_ccall f_5883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,2)))){
C_save_and_reclaim((void *)f_5883,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_5886,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=t1,tmp=(C_word)a,a+=19,tmp);
/* batch-driver.scm:747: end-time */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5265(t3,t2,lf[247]);}

/* k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in ... */
static void C_ccall f_5886(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,4)))){
C_save_and_reclaim((void *)f_5886,c,av);}
a=C_alloc(19);
t2=(*a=C_CLOSURE_TYPE|18,a[1]=(C_word)f_5889,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],tmp=(C_word)a,a+=19,tmp);
/* batch-driver.scm:748: print-node */
t3=((C_word*)((C_word*)t0)[6])[1];
f_5040(t3,t2,lf[245],lf[246],((C_word*)t0)[18]);}

/* k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in ... */
static void C_ccall f_5889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,7)))){
C_save_and_reclaim((void *)f_5889,c,av);}
a=C_alloc(20);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|17,a[1]=(C_word)f_5894,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],a[17]=((C_word*)t0)[16],tmp=(C_word)a,a+=18,tmp));
t5=((C_word*)t3)[1];
f_5894(t5,((C_word*)t0)[17],C_fix(1),((C_word*)t0)[18],C_SCHEME_TRUE,C_SCHEME_FALSE,C_SCHEME_FALSE);}

/* loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in ... */
static void C_fcall f_5894(C_word t0,C_word t1,C_word t2,C_word t3,C_word t4,C_word t5,C_word t6){
C_word tmp;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(26,0,2)))){
C_save_and_reclaim_args((void *)trf_5894,7,t0,t1,t2,t3,t4,t5,t6);}
a=C_alloc(26);
t7=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t8=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5898,a[2]=t4,a[3]=t5,a[4]=t7,a[5]=t2,a[6]=((C_word*)t0)[2],a[7]=t6,a[8]=((C_word*)t0)[3],a[9]=((C_word*)t0)[4],a[10]=((C_word*)t0)[5],a[11]=((C_word*)t0)[6],a[12]=((C_word*)t0)[7],a[13]=t1,a[14]=((C_word*)t0)[8],a[15]=((C_word*)t0)[9],a[16]=((C_word*)t0)[10],a[17]=((C_word*)t0)[11],a[18]=((C_word*)t0)[12],a[19]=((C_word*)t0)[13],a[20]=((C_word*)t0)[14],a[21]=((C_word*)t0)[15],a[22]=((C_word*)t0)[16],a[23]=((C_word*)t0)[17],tmp=(C_word)a,a+=24,tmp);
/* batch-driver.scm:756: begin-time */
t9=((C_word*)((C_word*)t0)[5])[1];
f_5255(t9,t8);}

/* k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in ... */
static void C_ccall f_5898(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,4)))){
C_save_and_reclaim((void *)f_5898,c,av);}
a=C_alloc(30);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5901,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
/* batch-driver.scm:758: analyze */
t3=((C_word*)((C_word*)t0)[11])[1];
f_5305(t3,t2,lf[244],((C_word*)((C_word*)t0)[4])[1],C_a_i_list(&a,2,((C_word*)t0)[5],((C_word*)t0)[2]));}

/* k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in ... */
static void C_ccall f_5901(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(30,c,2)))){
C_save_and_reclaim((void *)f_5901,c,av);}
a=C_alloc(30);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5904,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],a[14]=((C_word*)t0)[13],a[15]=((C_word*)t0)[14],a[16]=((C_word*)t0)[15],a[17]=((C_word*)t0)[16],a[18]=((C_word*)t0)[17],a[19]=((C_word*)t0)[18],a[20]=((C_word*)t0)[19],a[21]=((C_word*)t0)[20],a[22]=((C_word*)t0)[21],a[23]=((C_word*)t0)[22],tmp=(C_word)a,a+=24,tmp);
if(C_truep(*((C_word*)lf[164]+1))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6238,a[2]=((C_word*)t0)[23],a[3]=t2,a[4]=((C_word*)t0)[15],a[5]=t1,tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_memq(lf[242],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:761: chicken.compiler.support#dump-undefined-globals */
t4=*((C_word*)lf[243]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t4=t3;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_6238(2,av2);}}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5904(2,av2);}}}

/* k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in ... */
static void C_ccall f_5904(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,2)))){
C_save_and_reclaim((void *)f_5904,c,av);}
a=C_alloc(24);
t2=C_set_block_item(lf[164] /* chicken.compiler.core#first-analysis */,0,C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5908,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
/* batch-driver.scm:771: end-time */
t4=((C_word*)((C_word*)t0)[10])[1];
f_5265(t4,t3,lf[235]);}

/* k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in ... */
static void C_ccall f_5908(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,5)))){
C_save_and_reclaim((void *)f_5908,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5911,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
/* batch-driver.scm:772: print-db */
t3=((C_word*)((C_word*)t0)[21])[1];
f_5062(t3,t2,lf[233],lf[234],((C_word*)t0)[5],((C_word*)t0)[6]);}

/* k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in ... */
static void C_ccall f_5911(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,9)))){
C_save_and_reclaim((void *)f_5911,c,av);}
a=C_alloc(29);
t2=(*a=C_CLOSURE_TYPE|23,a[1]=(C_word)f_5914,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],a[17]=((C_word*)t0)[17],a[18]=((C_word*)t0)[18],a[19]=((C_word*)t0)[19],a[20]=((C_word*)t0)[20],a[21]=((C_word*)t0)[21],a[22]=((C_word*)t0)[22],a[23]=((C_word*)t0)[23],tmp=(C_word)a,a+=24,tmp);
if(C_truep(C_i_memq(lf[223],*((C_word*)lf[104]+1)))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_4056,a[2]=((C_word*)t0)[5],tmp=(C_word)a,a+=3,tmp);
t4=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_4062,tmp=(C_word)a,a+=2,tmp);
/* batch-driver.scm:69: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=t2;
av2[2]=t3;
av2[3]=t4;
C_call_with_values(4,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_5914(2,av2);}}}

/* k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f_5914(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,4)))){
C_save_and_reclaim((void *)f_5914,c,av);}
a=C_alloc(24);
if(C_truep(((C_word*)t0)[2])){
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5920,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],a[11]=((C_word*)t0)[12],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[14],tmp=(C_word)a,a+=14,tmp);
/* batch-driver.scm:779: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[106];
av2[3]=lf[182];
av2[4]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}
else{
t2=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_6021,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[10],a[5]=((C_word*)t0)[15],a[6]=((C_word*)t0)[16],a[7]=((C_word*)t0)[17],a[8]=((C_word*)t0)[18],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[14],a[11]=((C_word*)t0)[19],a[12]=((C_word*)t0)[13],a[13]=((C_word*)t0)[20],a[14]=((C_word*)t0)[21],a[15]=((C_word*)t0)[6],a[16]=((C_word*)t0)[22],tmp=(C_word)a,a+=17,tmp);
if(C_truep(((C_word*)t0)[23])){
t3=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6201,a[2]=((C_word*)t0)[10],a[3]=t2,a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[11],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:819: begin-time */
t4=((C_word*)((C_word*)t0)[11])[1];
f_5255(t4,t3);}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6021(2,av2);}}}}

/* k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in ... */
static void C_ccall f_5920(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_5920,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5923,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
/* batch-driver.scm:780: begin-time */
t3=((C_word*)((C_word*)t0)[10])[1];
f_5255(t3,t2);}

/* k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in ... */
static void C_ccall f_5923(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,4)))){
C_save_and_reclaim((void *)f_5923,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_5928,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_5940,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[6],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[9],a[8]=((C_word*)t0)[10],a[9]=((C_word*)t0)[11],a[10]=((C_word*)t0)[12],tmp=(C_word)a,a+=11,tmp);
/* batch-driver.scm:781: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[13];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a5927 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_5928(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,8)))){
C_save_and_reclaim((void *)f_5928,c,av);}
if(C_truep(((C_word*)t0)[2])){
/* batch-driver.scm:783: chicken.compiler.optimizer#determine-loop-and-dispatch */
t2=*((C_word*)lf[165]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
/* batch-driver.scm:784: chicken.compiler.optimizer#perform-high-level-optimizations */
t2=*((C_word*)lf[166]+1);{
C_word *av2;
if(c >= 9) {
  av2=av;
} else {
  av2=C_alloc(9);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=((C_word*)t0)[4];
av2[4]=*((C_word*)lf[167]+1);
av2[5]=*((C_word*)lf[168]+1);
av2[6]=*((C_word*)lf[169]+1);
av2[7]=*((C_word*)lf[170]+1);
av2[8]=*((C_word*)lf[171]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(9,av2);}}}

/* a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_5940(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_5940,c,av);}
a=C_alloc(14);
t4=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_5944,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t1,a[6]=t2,a[7]=((C_word*)t0)[4],a[8]=((C_word*)t0)[5],a[9]=((C_word*)t0)[6],a[10]=((C_word*)t0)[7],a[11]=((C_word*)t0)[8],a[12]=((C_word*)t0)[9],a[13]=((C_word*)t0)[10],tmp=(C_word)a,a+=14,tmp);
/* batch-driver.scm:789: end-time */
t5=((C_word*)((C_word*)t0)[7])[1];
f_5265(t5,t4,lf[181]);}

/* k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_ccall f_5944(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,4)))){
C_save_and_reclaim((void *)f_5944,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_5947,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* batch-driver.scm:790: print-node */
t3=((C_word*)((C_word*)t0)[13])[1];
f_5040(t3,t2,lf[179],lf[180],((C_word*)t0)[6]);}

/* k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in ... */
static void C_ccall f_5947(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_5947,c,av);}
a=C_alloc(29);
if(C_truep(((C_word*)t0)[2])){
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1));
/* batch-driver.scm:792: loop */
t3=((C_word*)((C_word*)t0)[4])[1];
f_5894(t3,((C_word*)t0)[5],t2,((C_word*)t0)[6],C_SCHEME_TRUE,C_SCHEME_FALSE,((C_word*)t0)[7]);}
else{
t2=C_i_not(((C_word*)t0)[8]);
t3=(C_truep(t2)?((C_word*)t0)[9]:C_SCHEME_FALSE);
if(C_truep(t3)){
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_5964,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:794: chicken.compiler.support#debugging */
t5=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[106];
av2[3]=lf[172];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
if(C_truep(C_i_not(*((C_word*)lf[171]+1)))){
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5978,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[8],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:797: chicken.compiler.support#debugging */
t5=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=lf[106];
av2[3]=lf[173];
((C_proc)(void*)(*((C_word*)t5+1)))(4,av2);}}
else{
if(C_truep(*((C_word*)lf[174]+1))){
t4=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5990,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[10],a[8]=((C_word*)t0)[11],a[9]=((C_word*)t0)[12],tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:801: begin-time */
t5=((C_word*)((C_word*)t0)[11])[1];
f_5255(t5,t4);}
else{
t4=C_s_a_i_plus(&a,2,((C_word*)t0)[3],C_fix(1));
/* batch-driver.scm:814: loop */
t5=((C_word*)((C_word*)t0)[4])[1];
f_5894(t5,((C_word*)t0)[5],t4,((C_word*)t0)[6],C_SCHEME_FALSE,C_SCHEME_FALSE,((C_word*)t0)[8]);}}}}}

/* k5962 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_5964(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_5964,c,av);}
a=C_alloc(29);
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1));
/* batch-driver.scm:795: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5894(t3,((C_word*)t0)[4],t2,((C_word*)t0)[5],C_SCHEME_TRUE,C_SCHEME_TRUE,C_SCHEME_TRUE);}

/* k5976 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_5978(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_5978,c,av);}
a=C_alloc(29);
t2=C_set_block_item(lf[171] /* chicken.compiler.core#inline-substitutions-enabled */,0,C_SCHEME_TRUE);
t3=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1));
/* batch-driver.scm:799: loop */
t4=((C_word*)((C_word*)t0)[3])[1];
f_5894(t4,((C_word*)t0)[4],t3,((C_word*)t0)[5],C_SCHEME_TRUE,C_SCHEME_FALSE,((C_word*)t0)[6]);}

/* k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_5990(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,4)))){
C_save_and_reclaim((void *)f_5990,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5993,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:802: analyze */
t3=((C_word*)((C_word*)t0)[9])[1];
f_5305(t3,t2,lf[178],((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* k5991 in k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in ... */
static void C_ccall f_5993(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_5993,c,av);}
a=C_alloc(10);
t2=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_5996,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=t1,a[9]=((C_word*)t0)[8],tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:803: end-time */
t3=((C_word*)((C_word*)t0)[7])[1];
f_5265(t3,t2,lf[177]);}

/* k5994 in k5991 in k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in ... */
static void C_ccall f_5996(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_5996,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_5999,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:804: begin-time */
t3=((C_word*)((C_word*)t0)[9])[1];
f_5255(t3,t2);}

/* k5997 in k5994 in k5991 in k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in ... */
static void C_ccall f_5999(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_5999,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6002,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:806: chicken.compiler.optimizer#transform-direct-lambdas! */
t3=*((C_word*)lf[176]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[8];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6000 in k5997 in k5994 in k5991 in k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in ... */
static void C_ccall f_6002(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6002,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6005,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:807: end-time */
t3=((C_word*)((C_word*)t0)[7])[1];
f_5265(t3,t2,lf[175]);}

/* k6003 in k6000 in k5997 in k5994 in k5991 in k5988 in k5945 in k5942 in a5939 in k5921 in k5918 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in ... */
static void C_ccall f_6005(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,6)))){
C_save_and_reclaim((void *)f_6005,c,av);}
a=C_alloc(29);
t2=C_s_a_i_plus(&a,2,((C_word*)t0)[2],C_fix(1));
/* batch-driver.scm:808: loop */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5894(t3,((C_word*)t0)[4],t2,((C_word*)t0)[5],((C_word*)t0)[6],C_SCHEME_FALSE,((C_word*)t0)[7]);}

/* k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in ... */
static void C_ccall f_6021(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,4)))){
C_save_and_reclaim((void *)f_6021,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|16,a[1]=(C_word)f_6024,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],a[16]=((C_word*)t0)[16],tmp=(C_word)a,a+=17,tmp);
/* batch-driver.scm:828: print-node */
t3=((C_word*)((C_word*)t0)[12])[1];
f_5040(t3,t2,lf[215],lf[216],((C_word*)((C_word*)t0)[2])[1]);}

/* k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in ... */
static void C_ccall f_6024(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(28,c,4)))){
C_save_and_reclaim((void *)f_6024,c,av);}
a=C_alloc(28);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_6027,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
t3=(C_truep(((C_word*)((C_word*)t0)[16])[1])?*((C_word*)lf[210]+1):C_SCHEME_FALSE);
if(C_truep(t3)){
t4=((C_word*)((C_word*)t0)[16])[1];
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6151,a[2]=t2,a[3]=((C_word*)t0)[6],a[4]=t4,a[5]=((C_word*)t0)[3],tmp=(C_word)a,a+=6,tmp);
t6=C_a_i_list(&a,1,t4);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9334,a[2]=t5,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t7;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[214];
av2[4]=t6;
C_apply(5,av2);}}
else{
t4=t2;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
f_6027(2,av2);}}}

/* k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_6027(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_6027,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_6030,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
/* batch-driver.scm:838: begin-time */
t3=((C_word*)((C_word*)t0)[9])[1];
f_5255(t3,t2);}

/* k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_ccall f_6030(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_6030,c,av);}
a=C_alloc(16);
t2=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_6034,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
/* batch-driver.scm:840: chicken.compiler.core#perform-closure-conversion */
t3=*((C_word*)lf[209]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in ... */
static void C_ccall f_6034(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,2)))){
C_save_and_reclaim((void *)f_6034,c,av);}
a=C_alloc(16);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|15,a[1]=(C_word)f_6037,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],a[14]=((C_word*)t0)[14],a[15]=((C_word*)t0)[15],tmp=(C_word)a,a+=16,tmp);
/* batch-driver.scm:841: end-time */
t4=((C_word*)((C_word*)t0)[4])[1];
f_5265(t4,t3,lf[208]);}

/* k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_6037(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,5)))){
C_save_and_reclaim((void *)f_6037,c,av);}
a=C_alloc(14);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6040,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],a[13]=((C_word*)t0)[13],tmp=(C_word)a,a+=14,tmp);
/* batch-driver.scm:842: print-db */
t3=((C_word*)((C_word*)t0)[14])[1];
f_5062(t3,t2,lf[206],lf[207],((C_word*)t0)[3],((C_word*)t0)[15]);}

/* k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in ... */
static void C_ccall f_6040(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_6040,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6043,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
if(C_truep(*((C_word*)lf[204]+1))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6145,a[2]=((C_word*)t0)[13],a[3]=t2,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:257: chicken.time#current-milliseconds */
t4=*((C_word*)lf[115]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6043(2,av2);}}}

/* k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in ... */
static void C_ccall f_6043(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6043,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6046,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* batch-driver.scm:846: print-node */
t3=((C_word*)((C_word*)t0)[12])[1];
f_5040(t3,t2,lf[202],lf[203],((C_word*)((C_word*)t0)[2])[1]);}

/* k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in ... */
static void C_ccall f_6046(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6046,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6049,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
if(C_truep(((C_word*)t0)[11])){
/* batch-driver.scm:847: chicken.base#exit */
t3=*((C_word*)lf[201]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_fix(0);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6049(2,av2);}}}

/* k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in ... */
static void C_ccall f_6049(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6049,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6052,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
/* batch-driver.scm:848: begin-time */
t3=((C_word*)((C_word*)t0)[9])[1];
f_5255(t3,t2);}

/* k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in ... */
static void C_ccall f_6052(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,7)))){
C_save_and_reclaim((void *)f_6052,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6057,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6063,a[2]=((C_word*)t0)[4],a[3]=((C_word*)t0)[5],a[4]=((C_word*)t0)[6],a[5]=((C_word*)t0)[7],a[6]=((C_word*)t0)[8],a[7]=((C_word*)t0)[3],a[8]=((C_word*)t0)[9],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:850: ##sys#call-with-values */{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=0;
av2[1]=((C_word*)t0)[10];
av2[2]=t2;
av2[3]=t3;
C_call_with_values(4,av2);}}

/* a6056 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in ... */
static void C_ccall f_6057(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6057,c,av);}
/* batch-driver.scm:851: chicken.compiler.core#prepare-for-code-generation */
t2=*((C_word*)lf[183]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=t1;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in ... */
static void C_ccall f_6063(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3=av[3];
C_word t4=av[4];
C_word t5=av[5];
C_word t6=av[6];
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,c,2)))){
C_save_and_reclaim((void *)f_6063,c,av);}
a=C_alloc(14);
t7=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6067,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=t3,a[6]=t4,a[7]=t5,a[8]=((C_word*)t0)[4],a[9]=((C_word*)t0)[5],a[10]=((C_word*)t0)[6],a[11]=((C_word*)t0)[7],a[12]=t6,a[13]=((C_word*)t0)[8],tmp=(C_word)a,a+=14,tmp);
/* batch-driver.scm:852: end-time */
t8=((C_word*)((C_word*)t0)[2])[1];
f_5265(t8,t7,lf[200]);}

/* k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in ... */
static void C_ccall f_6067(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6067,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6070,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
/* batch-driver.scm:853: begin-time */
t3=((C_word*)((C_word*)t0)[13])[1];
f_5255(t3,t2);}

/* k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in ... */
static void C_ccall f_6070(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_6070,c,av);}
a=C_alloc(22);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6073,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
if(C_truep(*((C_word*)lf[195]+1))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6116,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
t4=C_a_i_list(&a,1,*((C_word*)lf[195]+1));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9326,a[2]=t3,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t5;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[199];
av2[4]=t4;
C_apply(5,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6073(2,av2);}}}

/* k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in ... */
static void C_ccall f_6073(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(13,c,2)))){
C_save_and_reclaim((void *)f_6073,c,av);}
a=C_alloc(13);
t2=(*a=C_CLOSURE_TYPE|12,a[1]=(C_word)f_6076,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],a[12]=((C_word*)t0)[12],tmp=(C_word)a,a+=13,tmp);
if(C_truep(((C_word*)t0)[4])){
/* batch-driver.scm:863: scheme#open-output-file */
t3=*((C_word*)lf[194]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=*((C_word*)lf[20]+1);
f_6076(2,av2);}}}

/* k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in ... */
static void C_ccall f_6076(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(20,c,4)))){
C_save_and_reclaim((void *)f_6076,c,av);}
a=C_alloc(20);
t2=(*a=C_CLOSURE_TYPE|13,a[1]=(C_word)f_6079,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],a[11]=((C_word*)t0)[10],a[12]=((C_word*)t0)[11],a[13]=((C_word*)t0)[12],tmp=(C_word)a,a+=14,tmp);
t3=C_a_i_list(&a,1,((C_word*)t0)[4]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9320,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[193];
av2[4]=t3;
C_apply(5,av2);}}

/* k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in ... */
static void C_ccall f_6079(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,10)))){
C_save_and_reclaim((void *)f_6079,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6082,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:865: chicken.compiler.c-backend#generate-code */
t3=*((C_word*)lf[192]+1);{
C_word *av2;
if(c >= 11) {
  av2=av;
} else {
  av2=C_alloc(11);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[6];
av2[3]=((C_word*)t0)[7];
av2[4]=((C_word*)t0)[8];
av2[5]=((C_word*)t0)[5];
av2[6]=((C_word*)t0)[9];
av2[7]=((C_word*)t0)[10];
av2[8]=((C_word*)t0)[11];
av2[9]=((C_word*)t0)[12];
av2[10]=((C_word*)t0)[13];
((C_proc)(void*)(*((C_word*)t3+1)))(11,av2);}}

/* k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in ... */
static void C_ccall f_6082(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6082,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6085,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
if(C_truep(((C_word*)t0)[4])){
/* batch-driver.scm:868: scheme#close-output-port */
t3=*((C_word*)lf[191]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6085(2,av2);}}}

/* k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in ... */
static void C_ccall f_6085(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6085,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6088,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:869: end-time */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5265(t3,t2,lf[190]);}

/* k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in ... */
static void C_ccall f_6088(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6088,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6091,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(C_i_memq(lf[187],*((C_word*)lf[104]+1)))){
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6107,a[2]=t2,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:871: ##sys#stop-timer */
t4=*((C_word*)lf[189]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}
else{
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f8805,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:872: chicken.compiler.support#compiler-cleanup-hook */
t4=*((C_word*)lf[186]+1);{
C_word *av2=av;
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6089 in k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in ... */
static void C_ccall f_6091(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6091,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6094,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:872: chicken.compiler.support#compiler-cleanup-hook */
t3=*((C_word*)lf[186]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k6092 in k6089 in k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in ... */
static void C_ccall f_6094(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_6094,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9308,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t2;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[185];
av2[4]=C_SCHEME_END_OF_LIST;
C_apply(5,av2);}}

/* k6105 in k6086 in k6083 in k6080 in k6077 in k6074 in k6071 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in ... */
static void C_ccall f_6107(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6107,c,av);}
/* batch-driver.scm:871: ##sys#display-times */
t2=*((C_word*)lf[188]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6114 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in ... */
static void C_ccall f_6116(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(2,c,3)))){
C_save_and_reclaim((void *)f_6116,c,av);}
a=C_alloc(2);
t2=(*a=C_CLOSURE_TYPE|1,a[1]=(C_word)f_6121,tmp=(C_word)a,a+=2,tmp);
/* batch-driver.scm:858: scheme#with-output-to-file */
t3=*((C_word*)lf[198]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[195]+1);
av2[3]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* a6120 in k6114 in k6068 in k6065 in a6062 in k6050 in k6047 in k6044 in k6041 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in ... */
static void C_ccall f_6121(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6121,c,av);}
t2=*((C_word*)lf[196]+1);
/* batch-driver.scm:860: g2185 */
t3=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t1;
av2[2]=*((C_word*)lf[197]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6143 in k6038 in k6035 in k6032 in k6028 in k6025 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in ... */
static void C_ccall f_6145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(29,c,2)))){
C_save_and_reclaim((void *)f_6145,c,av);}
a=C_alloc(29);
t2=C_s_a_i_minus(&a,2,t1,((C_word*)((C_word*)t0)[2])[1]);
if(C_truep(C_i_greaterp(t2,C_fix(60000)))){
/* batch-driver.scm:845: scheme#display */
t3=*((C_word*)lf[152]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=lf[205];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6043(2,av2);}}}

/* k6149 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_6151(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_6151,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=*((C_word*)lf[211]+1);
t7=*((C_word*)lf[212]+1);
t8=C_i_check_list_2(*((C_word*)lf[212]+1),lf[5]);
t9=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6164,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6166,a[2]=t4,a[3]=t11,a[4]=t6,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_6166(t13,t9,*((C_word*)lf[212]+1));}

/* k6162 in k6149 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_ccall f_6164(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6164,c,av);}
/* batch-driver.scm:834: chicken.compiler.support#emit-global-inline-file */
t2=*((C_word*)lf[213]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=((C_word*)t0)[5];
av2[5]=*((C_word*)lf[167]+1);
av2[6]=*((C_word*)lf[169]+1);
av2[7]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}

/* map-loop2151 in k6149 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_fcall f_6166(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6166,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6191,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:837: g2157 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6189 in map-loop2151 in k6149 in k6022 in k6019 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in ... */
static void C_ccall f_6191(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6191,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6166(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in ... */
static void C_ccall f_6201(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_6201,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6204,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:820: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[106];
av2[3]=lf[222];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in ... */
static void C_ccall f_6204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6204,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6207,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:821: chicken.compiler.lfa2#perform-secondary-flow-analysis */
t3=*((C_word*)lf[221]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6205 in k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in ... */
static void C_ccall f_6207(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,2)))){
C_save_and_reclaim((void *)f_6207,c,av);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6210,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:822: end-time */
t3=((C_word*)((C_word*)t0)[2])[1];
f_5265(t3,t2,lf[220]);}

/* k6208 in k6205 in k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in ... */
static void C_ccall f_6210(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6210,c,av);}
a=C_alloc(6);
if(C_truep(C_i_nullp(((C_word*)t0)[2]))){
/* batch-driver.scm:827: end-time */
t2=((C_word*)((C_word*)t0)[3])[1];
f_5265(t2,((C_word*)t0)[4],lf[217]);}
else{
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6222,a[2]=((C_word*)t0)[5],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[2],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:824: begin-time */
t3=((C_word*)((C_word*)t0)[6])[1];
f_5255(t3,t2);}}

/* k6220 in k6208 in k6205 in k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in ... */
static void C_ccall f_6222(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,3)))){
C_save_and_reclaim((void *)f_6222,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6225,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:825: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[106];
av2[3]=lf[219];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6223 in k6220 in k6208 in k6205 in k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in ... */
static void C_ccall f_6225(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6225,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6229,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:826: chicken.compiler.lfa2#perform-unboxing */
t3=*((C_word*)lf[218]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6227 in k6223 in k6220 in k6208 in k6205 in k6202 in k6199 in k5912 in k5909 in k5906 in k5902 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in ... */
static void C_ccall f_6229(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6229,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* batch-driver.scm:827: end-time */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5265(t3,((C_word*)t0)[4],lf[217]);}

/* k6236 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in ... */
static void C_ccall f_6238(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6238,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6241,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_memq(lf[240],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:763: chicken.compiler.support#dump-defined-globals */
t3=*((C_word*)lf[241]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6241(2,av2);}}}

/* k6239 in k6236 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in ... */
static void C_ccall f_6241(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6241,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6244,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
if(C_truep(C_i_memq(lf[238],*((C_word*)lf[104]+1)))){
/* batch-driver.scm:765: chicken.compiler.support#dump-global-refs */
t3=*((C_word*)lf[239]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
t3=t2;{
C_word *av2=av;
av2[0]=t3;
av2[1]=C_SCHEME_UNDEFINED;
f_6244(2,av2);}}}

/* k6242 in k6239 in k6236 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in ... */
static void C_ccall f_6244(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,4)))){
C_save_and_reclaim((void *)f_6244,c,av);}
a=C_alloc(12);
if(C_truep(((C_word*)((C_word*)t0)[2])[1])){
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6250,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[2],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
t3=C_a_i_list(&a,1,((C_word*)((C_word*)t0)[2])[1]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9340,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[237];
av2[4]=t3;
C_apply(5,av2);}}
else{
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_5904(2,av2);}}}

/* k6248 in k6242 in k6239 in k6236 in k5899 in k5896 in loop in k5887 in k5884 in k5881 in k5878 in k5875 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f_6250(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6250,c,av);}
/* batch-driver.scm:769: chicken.compiler.scrutinizer#emit-types-file */
t2=*((C_word*)lf[236]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)((C_word*)t0)[4])[1];
av2[4]=((C_word*)t0)[5];
av2[5]=*((C_word*)lf[167]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* k6280 in k5869 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in ... */
static void C_ccall f_6282(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6282,c,av);}
/* batch-driver.scm:742: chicken.compiler.optimizer#scan-toplevel-assignments */
t2=*((C_word*)lf[250]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_i_car(t1);
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_ccall f_6288(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,2)))){
C_save_and_reclaim((void *)f_6288,c,av);}
a=C_alloc(12);
t2=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6302,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
/* batch-driver.scm:714: collect-options */
t3=((C_word*)((C_word*)t0)[12])[1];
f_5220(t3,t2,lf[267]);}

/* k6291 in for-each-loop2052 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in ... */
static void C_ccall f_6293(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6293,c,av);}
if(C_truep(t1)){
t2=((C_word*)((C_word*)t0)[2])[1];
f_6385(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}
else{
/* batch-driver.scm:713: chicken.compiler.support#quit-compiling */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[5];
av2[2]=lf[266];
av2[3]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}}

/* k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in ... */
static void C_ccall f_6302(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_6302,c,av);}
a=C_alloc(17);
t2=C_i_check_list_2(t1,lf[10]);
t3=(*a=C_CLOSURE_TYPE|11,a[1]=(C_word)f_6308,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],a[11]=((C_word*)t0)[11],tmp=(C_word)a,a+=12,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6385,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6385(t7,t3,t1);}

/* k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in ... */
static void C_ccall f_6308(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(16,c,3)))){
C_save_and_reclaim((void *)f_6308,c,av);}
a=C_alloc(16);
t2=C_i_check_list_2(((C_word*)t0)[2],lf[10]);
t3=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6327,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[7],a[7]=((C_word*)t0)[8],a[8]=((C_word*)t0)[9],a[9]=((C_word*)t0)[10],a[10]=((C_word*)t0)[11],tmp=(C_word)a,a+=11,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6362,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6362(t7,t3,((C_word*)t0)[2]);}

/* k6315 in for-each-loop2073 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in ... */
static void C_ccall f_6317(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6317,c,av);}
/* batch-driver.scm:717: chicken.compiler.scrutinizer#load-type-database */
t2=*((C_word*)lf[262]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=*((C_word*)lf[123]+1);
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6319 in for-each-loop2073 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in ... */
static void C_ccall f_6321(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6321,c,av);}
/* batch-driver.scm:718: chicken.pathname#make-pathname */
t2=*((C_word*)lf[263]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
av2[4]=lf[264];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in ... */
static void C_ccall f_6327(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,2)))){
C_save_and_reclaim((void *)f_6327,c,av);}
a=C_alloc(11);
t2=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_6330,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],a[10]=((C_word*)t0)[10],tmp=(C_word)a,a+=11,tmp);
/* batch-driver.scm:721: begin-time */
t3=((C_word*)((C_word*)t0)[8])[1];
f_5255(t3,t2);}

/* k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in ... */
static void C_ccall f_6330(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6330,c,av);}
a=C_alloc(10);
t2=C_set_block_item(lf[164] /* chicken.compiler.core#first-analysis */,0,C_SCHEME_FALSE);
t3=(*a=C_CLOSURE_TYPE|9,a[1]=(C_word)f_6335,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],a[9]=((C_word*)t0)[9],tmp=(C_word)a,a+=10,tmp);
/* batch-driver.scm:723: analyze */
t4=((C_word*)((C_word*)t0)[10])[1];
f_5305(t4,t3,lf[261],((C_word*)t0)[5],C_SCHEME_END_OF_LIST);}

/* k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in ... */
static void C_ccall f_6335(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,5)))){
C_save_and_reclaim((void *)f_6335,c,av);}
a=C_alloc(9);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6338,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],a[6]=((C_word*)t0)[2],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:724: print-db */
t4=((C_word*)((C_word*)t0)[9])[1];
f_5062(t4,t3,lf[259],lf[260],((C_word*)((C_word*)t0)[2])[1],C_fix(0));}

/* k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in ... */
static void C_ccall f_6338(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_6338,c,av);}
a=C_alloc(9);
t2=(*a=C_CLOSURE_TYPE|8,a[1]=(C_word)f_6341,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],a[8]=((C_word*)t0)[8],tmp=(C_word)a,a+=9,tmp);
/* batch-driver.scm:725: end-time */
t3=((C_word*)((C_word*)t0)[5])[1];
f_5265(t3,t2,lf[258]);}

/* k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in ... */
static void C_ccall f_6341(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_6341,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6344,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:726: begin-time */
t3=((C_word*)((C_word*)t0)[8])[1];
f_5255(t3,t2);}

/* k6342 in k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in ... */
static void C_ccall f_6344(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,3)))){
C_save_and_reclaim((void *)f_6344,c,av);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6347,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=((C_word*)t0)[6],a[7]=((C_word*)t0)[7],tmp=(C_word)a,a+=8,tmp);
/* batch-driver.scm:727: chicken.compiler.support#debugging */
t3=*((C_word*)lf[105]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[106];
av2[3]=lf[257];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}

/* k6345 in k6342 in k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in ... */
static void C_ccall f_6347(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,7)))){
C_save_and_reclaim((void *)f_6347,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6350,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:728: chicken.compiler.scrutinizer#scrutinize */
t3=*((C_word*)lf[255]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)((C_word*)t0)[6])[1];
av2[4]=((C_word*)((C_word*)t0)[7])[1];
av2[5]=*((C_word*)lf[123]+1);
av2[6]=*((C_word*)lf[256]+1);
av2[7]=*((C_word*)lf[167]+1);
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}

/* k6348 in k6345 in k6342 in k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in ... */
static void C_ccall f_6350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6350,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6353,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:731: end-time */
t3=((C_word*)((C_word*)t0)[5])[1];
f_5265(t3,t2,lf[254]);}

/* k6351 in k6348 in k6345 in k6342 in k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in ... */
static void C_ccall f_6353(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,4)))){
C_save_and_reclaim((void *)f_6353,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6356,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(*((C_word*)lf[123]+1))){
/* batch-driver.scm:733: print-node */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5040(t3,t2,lf[252],lf[253],((C_word*)t0)[4]);}
else{
t3=C_set_block_item(lf[164] /* chicken.compiler.core#first-analysis */,0,C_SCHEME_TRUE);
t4=((C_word*)t0)[2];
f_5871(t4,t3);}}

/* k6354 in k6351 in k6348 in k6345 in k6342 in k6339 in k6336 in k6333 in k6328 in k6325 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in ... */
static void C_ccall f_6356(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6356,c,av);}
t2=C_set_block_item(lf[164] /* chicken.compiler.core#first-analysis */,0,C_SCHEME_TRUE);
t3=((C_word*)t0)[2];
f_5871(t3,t2);}

/* for-each-loop2073 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in ... */
static void C_fcall f_6362(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,0,2)))){
C_save_and_reclaim_args((void *)trf_6362,3,t0,t1,t2);}
a=C_alloc(11);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6372,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6317,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6321,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:718: scheme#symbol->string */
t7=*((C_word*)lf[265]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6370 in for-each-loop2073 in k6306 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in ... */
static void C_ccall f_6372(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6372,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6362(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop2052 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in ... */
static void C_fcall f_6385(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,0,4)))){
C_save_and_reclaim_args((void *)trf_6385,3,t0,t1,t2);}
a=C_alloc(12);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6395,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6293,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,a[5]=t3,a[6]=t4,tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:712: chicken.compiler.scrutinizer#load-type-database */
t6=*((C_word*)lf[262]+1);{
C_word av2[5];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=*((C_word*)lf[123]+1);
av2[4]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6393 in for-each-loop2052 in k6300 in k6286 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in ... */
static void C_ccall f_6395(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6395,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6385(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6412 in k5866 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_ccall f_6414(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6414,c,av);}
if(C_truep(t1)){
t2=C_SCHEME_UNDEFINED;
t3=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_6288(2,av2);}}
else{
/* batch-driver.scm:708: chicken.compiler.support#quit-compiling */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[269];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* k6424 in for-each-loop2029 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_ccall f_6426(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6426,c,av);}
/* batch-driver.scm:700: chicken.compiler.support#load-inline-file */
t2=*((C_word*)lf[271]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* for-each-loop2029 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in ... */
static void C_fcall f_6437(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,0,4)))){
C_save_and_reclaim_args((void *)trf_6437,3,t0,t1,t2);}
a=C_alloc(15);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6447,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6426,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
t6=C_a_i_list(&a,1,t4);
t7=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9352,a[2]=t5,tmp=(C_word)a,a+=3,tmp);{
C_word av2[5];
av2[0]=0;
av2[1]=t7;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[272];
av2[4]=t6;
C_apply(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6445 in for-each-loop2029 in k5863 in k5860 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in ... */
static void C_ccall f_6447(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6447,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6437(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6461 in for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in ... */
static void C_ccall f_6463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,4)))){
C_save_and_reclaim((void *)f_6463,c,av);}
a=C_alloc(10);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6469,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t3=C_a_i_list(&a,1,t1);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9360,a[2]=t2,tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[275];
av2[4]=t3;
C_apply(5,av2);}}
else{
t2=((C_word*)((C_word*)t0)[3])[1];
f_6484(t2,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}}

/* k6467 in k6461 in for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in ... */
static void C_ccall f_6469(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6469,c,av);}
/* batch-driver.scm:692: chicken.compiler.support#load-inline-file */
t2=*((C_word*)lf[271]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6474 in for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in ... */
static void C_ccall f_6476(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,5)))){
C_save_and_reclaim((void *)f_6476,c,av);}
/* batch-driver.scm:689: ##sys#resolve-include-filename */
t2=*((C_word*)lf[276]+1);{
C_word *av2;
if(c >= 6) {
  av2=av;
} else {
  av2=C_alloc(6);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[277];
av2[4]=C_SCHEME_TRUE;
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}

/* for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in ... */
static void C_fcall f_6484(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(14,0,2)))){
C_save_and_reclaim_args((void *)trf_6484,3,t0,t1,t2);}
a=C_alloc(14);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6494,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6463,a[2]=t3,a[3]=((C_word*)t0)[2],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6476,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:690: scheme#symbol->string */
t7=*((C_word*)lf[265]+1);{
C_word av2[3];
av2[0]=t7;
av2[1]=t6;
av2[2]=t4;
((C_proc)(void*)(*((C_word*)t7+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6492 in for-each-loop2007 in k5857 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in ... */
static void C_ccall f_6494(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6494,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6484(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6506 in k5854 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in ... */
static void C_ccall f_6508(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6508,c,av);}
if(C_truep(t1)){
/* batch-driver.scm:685: chicken.pretty-print#pp */
t2=*((C_word*)lf[196]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5859(2,av2);}}}

/* k6519 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in ... */
static void C_ccall f_6521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6521,c,av);}
/* batch-driver.scm:683: concatenate */
f_3233(((C_word*)t0)[2],t1);}

/* map-loop1981 in k5851 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in ... */
static void C_fcall f_6523(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_6523,3,t0,t1,t2);}
a=C_alloc(3);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6557 in k5848 in k5845 in k5842 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in ... */
static void C_ccall f_6559(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6559,c,av);}
/* batch-driver.scm:682: concatenate */
f_3233(((C_word*)t0)[2],t1);}

/* k6561 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in ... */
static void C_ccall f_6563(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6563,c,av);}
/* batch-driver.scm:674: chicken.compiler.core#build-toplevel-procedure */
t2=*((C_word*)lf[284]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6565 in k5839 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in ... */
static void C_ccall f_6567(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6567,c,av);}
/* batch-driver.scm:675: chicken.compiler.support#build-node-graph */
t2=*((C_word*)lf[285]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6568 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in ... */
static void C_ccall f_6570(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,2)))){
C_save_and_reclaim((void *)f_6570,c,av);}
a=C_alloc(6);
t2=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6573,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:669: begin-time */
t3=((C_word*)((C_word*)t0)[6])[1];
f_5255(t3,t2);}

/* k6571 in k6568 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in ... */
static void C_ccall f_6573(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_6573,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=((C_word*)((C_word*)t0)[2])[1];
t7=C_i_check_list_2(t6,lf[5]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6583,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6588,a[2]=t4,a[3]=t10,a[4]=((C_word*)t0)[5],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_6588(t12,t8,t6);}

/* k6581 in k6571 in k6568 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in ... */
static void C_ccall f_6583(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6583,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
/* batch-driver.scm:671: end-time */
t3=((C_word*)((C_word*)t0)[3])[1];
f_5265(t3,((C_word*)t0)[4],lf[287]);}

/* map-loop1948 in k6571 in k6568 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in ... */
static void C_fcall f_6588(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6588,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6613,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:670: g1954 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6611 in map-loop1948 in k6571 in k6568 in k5836 in k5833 in k5830 in k5827 in k5822 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in ... */
static void C_ccall f_6613(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6613,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6588(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6626 in k5819 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in ... */
static void C_ccall f_6628(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6628,c,av);}
if(C_truep(t1)){
/* batch-driver.scm:655: chicken.compiler.support#display-line-number-database */
t2=*((C_word*)lf[294]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5824(2,av2);}}}

/* k6632 in k5816 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static void C_ccall f_6634(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_6634,c,av);}
if(C_truep(t1)){
/* batch-driver.scm:653: chicken.compiler.support#display-real-name-table */
t2=*((C_word*)lf[297]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5821(2,av2);}}}

/* a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_6645(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6645,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6649,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:648: chicken.base#print */
t3=*((C_word*)lf[303]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[304];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static void C_ccall f_6649(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_6649,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[300]+1);
t3=C_i_check_list_2(*((C_word*)lf[300]+1),lf[10]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6683,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_6683(t7,((C_word*)t0)[2],*((C_word*)lf[300]+1));}

/* k6655 in for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6657(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6657,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6660,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:650: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_i_car(((C_word*)t0)[4]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6658 in k6655 in for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6660(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6660,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6663,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:650: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[301];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6661 in k6658 in k6655 in for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in ... */
static void C_ccall f_6663(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_6663,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6666,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:650: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_u_i_cdr(((C_word*)t0)[4]);
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6664 in k6661 in k6658 in k6655 in for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in ... */
static void C_ccall f_6666(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6666,c,av);}
/* batch-driver.scm:650: ##sys#write-char-0 */
t2=*((C_word*)lf[34]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_make_character(10);
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in ... */
static void C_fcall f_6683(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,0,4)))){
C_save_and_reclaim_args((void *)trf_6683,3,t0,t1,t2);}
a=C_alloc(10);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6693,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=*((C_word*)lf[20]+1);
t6=*((C_word*)lf[20]+1);
t7=C_i_check_port_2(*((C_word*)lf[20]+1),C_fix(2),C_SCHEME_TRUE,lf[21]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6657,a[2]=t3,a[3]=t5,a[4]=t4,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:650: ##sys#print */
t9=*((C_word*)lf[22]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t8;
av2[2]=lf[302];
av2[3]=C_SCHEME_FALSE;
av2[4]=*((C_word*)lf[20]+1);
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k6691 in for-each-loop1919 in k6647 in a6644 in k5813 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6693(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6693,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_6683(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in ... */
static void C_ccall f_6707(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(15,c,3)))){
C_save_and_reclaim((void *)f_6707,c,av);}
a=C_alloc(15);
if(C_truep(t1)){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6713,a[2]=t1,a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6867,a[2]=t2,a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
t4=*((C_word*)lf[322]+1);
t5=C_a_i_list(&a,1,*((C_word*)lf[322]+1));
t6=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_3771,a[2]=t5,tmp=(C_word)a,a+=3,tmp);
/* mini-srfi-1.scm:182: filter */
f_3381(t3,t6,lf[323]);}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_FALSE;
f_5815(2,av2);}}}

/* k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_6713(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_6713,c,av);}
a=C_alloc(4);
if(C_truep(C_i_pairp(((C_word*)t0)[2]))){
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6726,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[2],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:641: chicken.base#open-output-string */
t3=*((C_word*)lf[314]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_5815(2,av2);}}}

/* k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static void C_ccall f_6726(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6726,c,av);}
a=C_alloc(5);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[307]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6732,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:641: ##sys#print */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[313];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}

/* k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in ... */
static void C_ccall f_6732(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_6732,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6735,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6742,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=*((C_word*)lf[310]+1);
t9=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6749,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t10=C_SCHEME_UNDEFINED;
t11=(*a=C_VECTOR_TYPE|1,a[1]=t10,tmp=(C_word)a,a+=2,tmp);
t12=C_set_block_item(t11,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6751,a[2]=t6,a[3]=t11,a[4]=t8,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t13=((C_word*)t11)[1];
f_6751(t13,t9,((C_word*)t0)[4]);}

/* k6733 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6735(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6735,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6738,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:641: chicken.base#get-output-string */
t3=*((C_word*)lf[309]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6736 in k6733 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6738(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6738,c,av);}
/* batch-driver.scm:640: chicken.base#warning */
t2=*((C_word*)lf[308]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6740 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6742(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6742,c,av);}
/* batch-driver.scm:641: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6747 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6749(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6749,c,av);}
/* batch-driver.scm:642: chicken.string#string-intersperse */
t2=*((C_word*)lf[311]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[312];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1888 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_fcall f_6751(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6751,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6776,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:642: g1894 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6774 in map-loop1888 in k6730 in k6724 in k6711 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6776(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6776,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6751(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in ... */
static void C_ccall f_6793(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6793,c,av);}
a=C_alloc(5);
t2=C_i_check_port_2(t1,C_fix(2),C_SCHEME_TRUE,lf[307]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6799,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[318]+1))){
/* batch-driver.scm:636: ##sys#print */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[319];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
if(C_truep(((C_word*)t0)[4])){
/* batch-driver.scm:636: ##sys#print */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[320];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}
else{
/* batch-driver.scm:636: ##sys#print */
t4=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t4;
av2[1]=t3;
av2[2]=lf[321];
av2[3]=C_SCHEME_FALSE;
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t4+1)))(5,av2);}}}}

/* k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in ... */
static void C_ccall f_6799(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_6799,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6802,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:636: ##sys#print */
t3=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[317];
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in ... */
static void C_ccall f_6802(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(24,c,3)))){
C_save_and_reclaim((void *)f_6802,c,av);}
a=C_alloc(24);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6805,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_6812,a[2]=t2,a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=*((C_word*)lf[310]+1);
t9=C_i_check_list_2(((C_word*)t0)[4],lf[5]);
t10=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6822,a[2]=t3,tmp=(C_word)a,a+=3,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6824,a[2]=t6,a[3]=t12,a[4]=t8,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_6824(t14,t10,((C_word*)t0)[4]);}

/* k6803 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6805(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6805,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_6808,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:636: chicken.base#get-output-string */
t3=*((C_word*)lf[309]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k6806 in k6803 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in ... */
static void C_ccall f_6808(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_6808,c,av);}
/* batch-driver.scm:635: chicken.base#notice */
t2=*((C_word*)lf[315]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k6810 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_6812,c,av);}
/* batch-driver.scm:636: ##sys#print */
t2=*((C_word*)lf[22]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=C_SCHEME_FALSE;
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k6820 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_ccall f_6822(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6822,c,av);}
/* batch-driver.scm:638: chicken.string#string-intersperse */
t2=*((C_word*)lf[311]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[316];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1850 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in ... */
static void C_fcall f_6824(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6824,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6849,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:638: g1856 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6847 in map-loop1850 in k6800 in k6797 in k6791 in k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in ... */
static void C_ccall f_6849(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6849,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6824(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6865 in k6705 in k5810 in k5807 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in ... */
static void C_ccall f_6867(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_6867,c,av);}
a=C_alloc(5);
if(C_truep(C_i_nullp(t1))){
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_6793,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:636: chicken.base#open-output-string */
t3=*((C_word*)lf[314]+1);{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=C_SCHEME_UNDEFINED;
f_6713(2,av2);}}}

/* k6881 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_ccall f_6883(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6883,c,av);}
/* batch-driver.scm:625: chicken.compiler.support#quit-compiling */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[326];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k6900 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_ccall f_6902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_6902,c,av);}
/* batch-driver.scm:628: chicken.string#string-intersperse */
t2=*((C_word*)lf[311]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=lf[327];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1802 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_fcall f_6904(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_6904,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_6929,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
/* batch-driver.scm:629: chicken.string#->string */
t5=*((C_word*)lf[310]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t3;
av2[2]=C_i_car(t4);
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k6927 in map-loop1802 in k5804 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in ... */
static void C_ccall f_6929(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_6929,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_6904(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k6959 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in ... */
static void C_ccall f_6961(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,c,3)))){
C_save_and_reclaim((void *)f_6961,c,av);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=*((C_word*)lf[322]+1);
t7=C_i_check_list_2(*((C_word*)lf[322]+1),lf[5]);
t8=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_6976,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7010,a[2]=t4,a[3]=t10,a[4]=t5,tmp=(C_word)a,a+=5,tmp));
t12=((C_word*)t10)[1];
f_7010(t12,t8,*((C_word*)lf[322]+1));}

/* k6974 in k6959 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_ccall f_6976(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,2)))){
C_save_and_reclaim((void *)f_6976,c,av);}
a=C_alloc(17);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6980,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t1,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(*((C_word*)lf[318]+1))){
t3=C_a_i_list(&a,2,lf[333],*((C_word*)lf[318]+1));
t4=t2;
f_6980(t4,C_a_i_list(&a,1,t3));}
else{
t3=t2;
f_6980(t3,C_SCHEME_END_OF_LIST);}}

/* k6978 in k6974 in k6959 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in ... */
static void C_fcall f_6980(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_6980,2,t0,t1);}
a=C_alloc(8);
t2=(*a=C_CLOSURE_TYPE|7,a[1]=(C_word)f_6984,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=((C_word*)t0)[5],a[6]=t1,a[7]=((C_word*)t0)[6],tmp=(C_word)a,a+=8,tmp);
if(C_truep(*((C_word*)lf[331]+1))){
if(C_truep(C_i_not(*((C_word*)lf[318]+1)))){
if(C_truep(((C_word*)t0)[7])){
/* batch-driver.scm:616: chicken.compiler.support#profiling-prelude-exps */
t3=*((C_word*)lf[332]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}
else{
/* batch-driver.scm:616: chicken.compiler.support#profiling-prelude-exps */
t3=*((C_word*)lf[332]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_TRUE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
/* batch-driver.scm:616: chicken.compiler.support#profiling-prelude-exps */
t3=*((C_word*)lf[332]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}}
else{
t3=t2;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_SCHEME_END_OF_LIST;
f_6984(2,av2);}}}

/* k6982 in k6978 in k6974 in k6959 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in ... */
static void C_ccall f_6984(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,7)))){
C_save_and_reclaim((void *)f_6984,c,av);}
if(C_truep(*((C_word*)lf[329]+1))){
t2=((C_word*)((C_word*)t0)[2])[1];
/* batch-driver.scm:611: scheme#append */
t3=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t3;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=t1;
av2[6]=((C_word*)t0)[7];
av2[7]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(8,av2);}}
else{
/* batch-driver.scm:611: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 8) {
  av2=av;
} else {
  av2=C_alloc(8);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[3];
av2[2]=((C_word*)t0)[4];
av2[3]=((C_word*)t0)[5];
av2[4]=((C_word*)t0)[6];
av2[5]=t1;
av2[6]=((C_word*)t0)[7];
av2[7]=lf[330];
((C_proc)(void*)(*((C_word*)t2+1)))(8,av2);}}}

/* map-loop1765 in k6959 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in ... */
static void C_fcall f_7010(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(9,0,2)))){
C_save_and_reclaim_args((void *)trf_7010,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_a_i_list(&a,2,lf[334],t3);
t5=C_a_i_cons(&a,2,t4,C_SCHEME_END_OF_LIST);
t6=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t5);
t7=C_mutate(((C_word *)((C_word*)t0)[2])+1,t5);
t9=t1;
t10=C_slot(t2,C_fix(1));
t1=t9;
t2=t10;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop1735 in k5801 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in ... */
static void C_fcall f_7044(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word *a;
loop:
if(C_unlikely(!C_demand(C_calculate_demand(18,0,2)))){
C_save_and_reclaim_args((void *)trf_7044,3,t0,t1,t2);}
a=C_alloc(18);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=C_i_cdr(t3);
t5=C_u_i_car(t3);
t6=C_a_i_list(&a,2,lf[335],t5);
t7=C_a_i_list(&a,3,lf[336],t4,t6);
t8=C_a_i_cons(&a,2,t7,C_SCHEME_END_OF_LIST);
t9=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t8);
t10=C_mutate(((C_word *)((C_word*)t0)[2])+1,t8);
t12=t1;
t13=C_slot(t2,C_fix(1));
t1=t12;
t2=t13;
goto loop;}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* map-loop1693 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in ... */
static void C_fcall f_7078(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7078,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7103,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:602: g1699 */
t4=((C_word*)t0)[4];
f_5771(t4,t3,C_slot(t2,C_fix(0)));}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7101 in map-loop1693 in k5795 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in ... */
static void C_ccall f_7103(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7103,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7078(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7126 in k5792 in k5765 in k5762 in k5759 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in ... */
static void C_ccall f_7128(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_7128,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,C_SCHEME_END_OF_LIST,((C_word*)t0)[2]);
t3=C_a_i_cons(&a,2,t1,t2);
t4=C_a_i_cons(&a,2,lf[337],t3);
t5=((C_word*)t0)[3];
f_5797(t5,C_a_i_list(&a,1,t4));}

/* k7133 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7135(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(17,c,3)))){
C_save_and_reclaim((void *)f_7135,c,av);}
a=C_alloc(17);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=((C_word*)((C_word*)t0)[2])[1];
t7=C_i_check_list_2(t6,lf[5]);
t8=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7145,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7147,a[2]=t4,a[3]=t10,a[4]=((C_word*)t0)[4],a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_7147(t12,t8,t6);}

/* k7143 in k7133 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_ccall f_7145(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7145,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5761(t3,t2);}

/* map-loop1665 in k7133 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_fcall f_7147(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7147,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7172,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:597: g1671 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7170 in map-loop1665 in k7133 in k5756 in k5753 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in ... */
static void C_ccall f_7172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7172,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7147(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7180 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in ... */
static void C_ccall f_7182(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,4)))){
C_save_and_reclaim((void *)f_7182,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7186,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:574: proc */
t3=((C_word*)t0)[4];{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[5];
av2[3]=((C_word*)t0)[6];
av2[4]=((C_word*)t0)[7];
((C_proc)(void*)(*((C_word*)t3+1)))(5,av2);}}

/* k7184 in k7180 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_ccall f_7186(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7186,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
f_5755(2,av2);}}

/* doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in ... */
static void C_fcall f_7191(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word t13;
C_word t14;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,0,3)))){
C_save_and_reclaim_args((void *)trf_7191,3,t0,t1,t2);}
a=C_alloc(22);
if(C_truep(C_i_nullp(t2))){
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7202,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
t4=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t5=t4;
t6=(*a=C_VECTOR_TYPE|1,a[1]=t5,tmp=(C_word)a,a+=2,tmp);
t7=((C_word*)t6)[1];
t8=*((C_word*)lf[344]+1);
t9=C_i_check_list_2(((C_word*)t0)[3],lf[5]);
t10=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7212,a[2]=((C_word*)t0)[4],a[3]=t3,a[4]=((C_word*)t0)[2],tmp=(C_word)a,a+=5,tmp);
t11=C_SCHEME_UNDEFINED;
t12=(*a=C_VECTOR_TYPE|1,a[1]=t11,tmp=(C_word)a,a+=2,tmp);
t13=C_set_block_item(t12,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7262,a[2]=t6,a[3]=t12,a[4]=t8,a[5]=t7,tmp=(C_word)a,a+=6,tmp));
t14=((C_word*)t12)[1];
f_7262(t14,t10,((C_word*)t0)[3]);}
else{
t3=C_i_car(t2);
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7300,a[2]=t3,a[3]=((C_word*)t0)[5],a[4]=t1,a[5]=t2,a[6]=((C_word*)t0)[2],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:583: chicken.compiler.support#check-and-open-input-file */
t5=*((C_word*)lf[348]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}}

/* k7200 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_ccall f_7202(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7202,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];{
C_word *av2=av;
av2[0]=t3;
av2[1]=t2;
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}

/* k7210 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_ccall f_7212(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,2)))){
C_save_and_reclaim((void *)f_7212,c,av);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7216,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:580: scheme#reverse */
t3=*((C_word*)lf[345]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)((C_word*)t0)[4])[1];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7214 in k7210 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7216(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word t11;
C_word t12;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(18,c,3)))){
C_save_and_reclaim((void *)f_7216,c,av);}
a=C_alloc(18);
t2=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=((C_word*)t4)[1];
t6=*((C_word*)lf[344]+1);
t7=C_i_check_list_2(((C_word*)t0)[2],lf[5]);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7226,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
t9=C_SCHEME_UNDEFINED;
t10=(*a=C_VECTOR_TYPE|1,a[1]=t9,tmp=(C_word)a,a+=2,tmp);
t11=C_set_block_item(t10,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7228,a[2]=t4,a[3]=t10,a[4]=t6,a[5]=t5,tmp=(C_word)a,a+=6,tmp));
t12=((C_word*)t10)[1];
f_7228(t12,t8,((C_word*)t0)[2]);}

/* k7224 in k7214 in k7210 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_ccall f_7226(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7226,c,av);}
/* batch-driver.scm:579: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1616 in k7214 in k7210 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_fcall f_7228(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7228,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7253,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:581: g1622 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7251 in map-loop1616 in k7214 in k7210 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in ... */
static void C_ccall f_7253(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7253,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7228(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* map-loop1590 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_fcall f_7262(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7262,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7287,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:579: g1596 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7285 in map-loop1590 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7287(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7287,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7262(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in ... */
static void C_ccall f_7300(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word t10;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(22,c,4)))){
C_save_and_reclaim((void *)f_7300,c,av);}
a=C_alloc(22);
t2=((C_word*)t0)[2];
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_SCHEME_FALSE;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7303,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp);
t7=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7310,a[2]=t5,a[3]=t3,tmp=(C_word)a,a+=4,tmp);
t8=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7315,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[6],tmp=(C_word)a,a+=5,tmp);
t9=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7343,a[2]=t3,a[3]=t5,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:584: ##sys#dynamic-wind */
t10=*((C_word*)lf[160]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t10;
av2[1]=t6;
av2[2]=t7;
av2[3]=t8;
av2[4]=t9;
((C_proc)(void*)(*((C_word*)t10+1)))(5,av2);}}

/* k7301 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7303(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7303,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7191(t2,((C_word*)t0)[3],C_u_i_cdr(((C_word*)t0)[4]));}

/* a7309 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7310(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7310,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[158]+1));
t3=C_mutate((C_word*)lf[158]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* a7314 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7315(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7315,c,av);}
a=C_alloc(8);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7321,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],a[5]=t3,tmp=(C_word)a,a+=6,tmp));
t5=((C_word*)t3)[1];
f_7321(t5,t1);}

/* loop in a7314 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in ... */
static void C_fcall f_7321(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_7321,2,t0,t1);}
a=C_alloc(7);
t2=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_7325,a[2]=t1,a[3]=((C_word*)t0)[2],a[4]=((C_word*)t0)[3],a[5]=((C_word*)t0)[4],a[6]=((C_word*)t0)[5],tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:586: chicken.compiler.support#read/source-info */
t3=*((C_word*)lf[347]+1);{
C_word av2[3];
av2[0]=t3;
av2[1]=t2;
av2[2]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7323 in loop in a7314 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in ... */
static void C_ccall f_7325(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7325,c,av);}
a=C_alloc(3);
if(C_truep(C_eofp(t1))){
/* batch-driver.scm:588: chicken.compiler.support#close-checked-input-file */
t2=*((C_word*)lf[346]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}
else{
t2=C_a_i_cons(&a,2,t1,((C_word*)((C_word*)t0)[5])[1]);
t3=C_mutate(((C_word *)((C_word*)t0)[5])+1,t2);
/* batch-driver.scm:591: loop */
t4=((C_word*)((C_word*)t0)[6])[1];
f_7321(t4,((C_word*)t0)[2]);}}

/* a7342 in k7298 in doloop1585 in k5750 in k5747 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in ... */
static void C_ccall f_7343(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7343,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,*((C_word*)lf[158]+1));
t3=C_mutate((C_word*)lf[158]+1 /* (set! ##sys#current-source-filename ...) */,((C_word*)((C_word*)t0)[3])[1]);
t4=t1;{
C_word *av2=av;
av2[0]=t4;
av2[1]=C_SCHEME_UNDEFINED;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}

/* k7348 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in ... */
static void C_ccall f_7350(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,c,2)))){
C_save_and_reclaim((void *)f_7350,c,av);}
a=C_alloc(8);
t2=C_a_i_list1(&a,1,((C_word*)t0)[2]);
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7358,a[2]=((C_word*)t0)[3],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:569: collect-options */
t4=((C_word*)((C_word*)t0)[4])[1];
f_5220(t4,t3,lf[350]);}

/* k7356 in k7348 in k5744 in k5741 in k5738 in k5734 in k5730 in k5727 in k5724 in k5721 in k5718 in k5679 in k5660 in k5657 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in ... */
static void C_ccall f_7358(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7358,c,av);}
/* batch-driver.scm:566: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=((C_word*)t0)[4];
av2[4]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k7374 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in ... */
static void C_ccall f_7376(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,4)))){
C_save_and_reclaim((void *)f_7376,c,av);}
a=C_alloc(5);
t2=C_set_block_item(lf[331] /* chicken.compiler.core#emit-profile */,0,C_SCHEME_TRUE);
t3=C_mutate((C_word*)lf[368]+1 /* (set! chicken.compiler.core#profiled-procedures ...) */,lf[369]);
t4=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7382,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=((C_word*)t0)[4],tmp=(C_word)a,a+=5,tmp);
if(C_truep(((C_word*)t0)[3])){
/* batch-driver.scm:528: scheme#append */
t5=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=*((C_word*)lf[373]+1);
av2[4]=lf[374];
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}
else{
/* batch-driver.scm:528: scheme#append */
t5=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t5;
av2[1]=t4;
av2[2]=((C_word*)((C_word*)t0)[2])[1];
av2[3]=*((C_word*)lf[373]+1);
av2[4]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t5+1)))(5,av2);}}}

/* k7380 in k7374 in k5654 in k5651 in k5648 in k5645 in k5634 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in ... */
static void C_ccall f_7382(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,c,4)))){
C_save_and_reclaim((void *)f_7382,c,av);}
a=C_alloc(6);
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
if(C_truep(((C_word*)t0)[3])){
t3=C_a_i_list(&a,1,lf[370]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9422,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[371];
av2[4]=t3;
C_apply(5,av2);}}
else{
t3=C_a_i_list(&a,1,lf[372]);
t4=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f9428,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=0;
av2[1]=t4;
av2[2]=*((C_word*)lf[184]+1);
av2[3]=lf[371];
av2[4]=t3;
C_apply(5,av2);}}}

/* k7429 in k5630 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in ... */
static void C_ccall f_7431(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7431,c,av);}
/* batch-driver.scm:506: arg-val */
f_5135(((C_word*)t0)[3],t1);}

/* k7436 in k5626 in k5623 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_7438(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7438,c,av);}
/* batch-driver.scm:503: arg-val */
f_5135(((C_word*)t0)[3],t1);}

/* k7452 in map-loop1526 in k7455 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in ... */
static void C_ccall f_7454(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,c,2)))){
C_save_and_reclaim((void *)f_7454,c,av);}
a=C_alloc(9);
t2=C_a_i_list(&a,2,lf[386],t1);
t3=C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST);
t4=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t3);
t5=C_mutate(((C_word *)((C_word*)t0)[2])+1,t3);
t6=((C_word*)((C_word*)t0)[3])[1];
f_7465(t6,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7455 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in ... */
static void C_ccall f_7457(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(11,c,3)))){
C_save_and_reclaim((void *)f_7457,c,av);}
a=C_alloc(11);
t2=C_i_check_list_2(t1,lf[5]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7463,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7465,a[2]=((C_word*)t0)[4],a[3]=t5,a[4]=((C_word*)t0)[5],tmp=(C_word)a,a+=5,tmp));
t7=((C_word*)t5)[1];
f_7465(t7,t3,t1);}

/* k7461 in k7455 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_7463(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7463,c,av);}
/* batch-driver.scm:494: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)((C_word*)t0)[3])[1];
av2[3]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* map-loop1526 in k7455 in k5619 in k5616 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_fcall f_7465(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7465,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7454,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:496: scheme#string->symbol */
t5=*((C_word*)lf[338]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* a7513 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in ... */
static void C_ccall f_7514(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,3)))){
C_save_and_reclaim((void *)f_7514,c,av);}
a=C_alloc(10);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7521,a[2]=t5,a[3]=t6,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:486: chicken.string#string-split */
t8=*((C_word*)lf[390]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
av2[3]=lf[391];
((C_proc)(void*)(*((C_word*)t8+1)))(4,av2);}}

/* k7519 in a7513 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in ... */
static void C_ccall f_7521(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_7521,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7526,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_7526(t5,((C_word*)t0)[4],t1);}

/* map-loop1497 in k7519 in a7513 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in ... */
static void C_fcall f_7526(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7526,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7551,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:486: g1503 */
t4=*((C_word*)lf[338]+1);{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7549 in map-loop1497 in k7519 in a7513 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in ... */
static void C_ccall f_7551(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7551,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7526(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7560 in k5613 in k5605 in k5601 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in ... */
static void C_ccall f_7562(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7562,c,av);}
/* batch-driver.scm:485: append-map */
f_2971(((C_word*)t0)[2],((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* for-each-loop1474 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in ... */
static void C_fcall f_7564(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,5)))){
C_save_and_reclaim_args((void *)trf_7564,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7574,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_5585,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:476: ##sys#resolve-include-filename */
t6=*((C_word*)lf[276]+1);{
C_word av2[6];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=C_SCHEME_FALSE;
av2[4]=C_SCHEME_TRUE;
av2[5]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t6+1)))(6,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7572 in for-each-loop1474 in k5578 in k5575 in k5568 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in ... */
static void C_ccall f_7574(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7574,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7564(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1113 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in ... */
static void C_fcall f_7587(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7587,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7597,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:466: g1114 */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7595 in for-each-loop1113 in k5562 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in ... */
static void C_ccall f_7597(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7597,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7587(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* a7609 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in ... */
static void C_ccall f_7610(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7610,c,av);}
t3=*((C_word*)lf[390]+1);
/* batch-driver.scm:468: g1464 */
t4=*((C_word*)lf[390]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[397];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7616 in k5559 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in ... */
static void C_ccall f_7618(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7618,c,av);}
/* batch-driver.scm:468: append-map */
f_2971(((C_word*)t0)[2],((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* for-each-loop1103 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in ... */
static void C_fcall f_7620(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_7620,3,t0,t1,t2);}
a=C_alloc(5);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7630,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
/* batch-driver.scm:463: g1104 */
t4=((C_word*)t0)[3];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7628 in for-each-loop1103 in k5553 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in ... */
static void C_ccall f_7630(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7630,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7620(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* a7642 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in ... */
static void C_ccall f_7643(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7643,c,av);}
t3=*((C_word*)lf[390]+1);
/* batch-driver.scm:465: g1449 */
t4=*((C_word*)lf[390]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t4;
av2[1]=t1;
av2[2]=t2;
av2[3]=lf[399];
((C_proc)(void*)(*((C_word*)t4+1)))(4,av2);}}

/* k7649 in k5550 in k5547 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in ... */
static void C_ccall f_7651(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7651,c,av);}
/* batch-driver.scm:465: append-map */
f_2971(((C_word*)t0)[2],((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* k7662 in for-each-loop1404 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in ... */
static void C_ccall f_7664(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7664,c,av);}
/* batch-driver.scm:453: chicken.compiler.support#mark-variable */
t2=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[403];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* k7671 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in ... */
static void C_ccall f_7673(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,c,3)))){
C_save_and_reclaim((void *)f_7673,c,av);}
a=C_alloc(5);
t2=*((C_word*)lf[77]+1);
t3=C_i_check_list_2(*((C_word*)lf[77]+1),lf[10]);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7689,a[2]=t5,tmp=(C_word)a,a+=3,tmp));
t7=((C_word*)t5)[1];
f_7689(t7,((C_word*)t0)[2],*((C_word*)lf[77]+1));}

/* k7676 in for-each-loop1423 in k7671 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in ... */
static void C_ccall f_7678(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,3)))){
C_save_and_reclaim((void *)f_7678,c,av);}
/* batch-driver.scm:458: chicken.compiler.support#mark-variable */
t2=*((C_word*)lf[13]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=((C_word*)t0)[3];
av2[3]=lf[403];
((C_proc)(void*)(*((C_word*)t2+1)))(4,av2);}}

/* for-each-loop1423 in k7671 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in ... */
static void C_fcall f_7689(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7689,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7699,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7678,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:457: chicken.compiler.support#mark-variable */
t6=*((C_word*)lf[13]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=lf[404];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7697 in for-each-loop1423 in k7671 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in ... */
static void C_ccall f_7699(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7699,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7689(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* for-each-loop1404 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in ... */
static void C_fcall f_7712(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(9,0,3)))){
C_save_and_reclaim_args((void *)trf_7712,3,t0,t1,t2);}
a=C_alloc(9);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_7722,a[2]=((C_word*)t0)[2],a[3]=t1,a[4]=t2,tmp=(C_word)a,a+=5,tmp);
t4=C_slot(t2,C_fix(0));
t5=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7664,a[2]=t3,a[3]=t4,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:452: chicken.compiler.support#mark-variable */
t6=*((C_word*)lf[13]+1);{
C_word av2[4];
av2[0]=t6;
av2[1]=t5;
av2[2]=t4;
av2[3]=lf[404];
((C_proc)(void*)(*((C_word*)t6+1)))(4,av2);}}
else{
t3=C_SCHEME_UNDEFINED;
t4=t1;{
C_word av2[2];
av2[0]=t4;
av2[1]=t3;
((C_proc)(void*)(*((C_word*)t4+1)))(2,av2);}}}

/* k7720 in for-each-loop1404 in k5544 in k5541 in k5538 in k5535 in k5532 in k5529 in k5526 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in ... */
static void C_ccall f_7722(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7722,c,av);}
t2=((C_word*)((C_word*)t0)[2])[1];
f_7712(t2,((C_word*)t0)[3],C_slot(((C_word*)t0)[4],C_fix(1)));}

/* k7765 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in ... */
static void C_ccall f_7767(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,3)))){
C_save_and_reclaim((void *)f_7767,c,av);}
a=C_alloc(12);
t2=C_i_check_list_2(t1,lf[5]);
t3=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7773,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],tmp=(C_word)a,a+=4,tmp);
t4=C_SCHEME_UNDEFINED;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=C_set_block_item(t5,0,(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7775,a[2]=((C_word*)t0)[4],a[3]=t5,a[4]=((C_word*)t0)[5],a[5]=((C_word*)t0)[6],tmp=(C_word)a,a+=6,tmp));
t7=((C_word*)t5)[1];
f_7775(t7,t3,t1);}

/* k7771 in k7765 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in ... */
static void C_ccall f_7773(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_7773,c,av);}
/* batch-driver.scm:434: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
av2[3]=*((C_word*)lf[134]+1);
av2[4]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* map-loop1376 in k7765 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in ... */
static void C_fcall f_7775(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_7775,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_7800,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:434: g1382 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k7798 in map-loop1376 in k7765 in k5520 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in ... */
static void C_ccall f_7800(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7800,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_7775(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k7810 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in ... */
static void C_ccall f_7812(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7812,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7815,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:427: chicken.base#case-sensitive */
t3=*((C_word*)lf[422]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7813 in k7810 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in ... */
static void C_ccall f_7815(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7815,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7818,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:428: chicken.base#keyword-style */
t3=*((C_word*)lf[420]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[421];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7816 in k7813 in k7810 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in ... */
static void C_ccall f_7818(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7818,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7821,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:429: chicken.base#parentheses-synonyms */
t3=*((C_word*)lf[419]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7819 in k7816 in k7813 in k7810 in k5517 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in ... */
static void C_ccall f_7821(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7821,c,av);}
/* batch-driver.scm:430: chicken.base#symbol-escape */
t2=*((C_word*)lf[418]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7827 in k5514 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in ... */
static void C_ccall f_7829(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7829,c,av);}
/* batch-driver.scm:424: chicken.base#symbol-escape */
t2=*((C_word*)lf[418]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7835 in k5511 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in ... */
static void C_ccall f_7837(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7837,c,av);}
/* batch-driver.scm:421: chicken.base#parentheses-synonyms */
t2=*((C_word*)lf[419]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7841 in k5508 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in ... */
static void C_ccall f_7843(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7843,c,av);}
if(C_truep(C_i_string_equal_p(lf[428],t1))){
/* batch-driver.scm:414: chicken.base#keyword-style */
t2=*((C_word*)lf[420]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[429];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
if(C_truep(C_u_i_string_equal_p(lf[430],t1))){
/* batch-driver.scm:415: chicken.base#keyword-style */
t2=*((C_word*)lf[420]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[421];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
if(C_truep(C_u_i_string_equal_p(lf[431],t1))){
/* batch-driver.scm:416: chicken.base#keyword-style */
t2=*((C_word*)lf[420]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[432];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
/* batch-driver.scm:417: chicken.compiler.support#quit-compiling */
t2=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[433];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}}}

/* k7874 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in ... */
static void C_ccall f_7876(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_7876,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7879,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:410: chicken.platform#register-feature! */
t3=*((C_word*)lf[135]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[434];
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7877 in k7874 in k5505 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in ... */
static void C_ccall f_7879(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_7879,c,av);}
/* batch-driver.scm:411: chicken.base#case-sensitive */
t2=*((C_word*)lf[422]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* k7884 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in ... */
static void C_ccall f_7886(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7886,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7889,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:405: scheme#string->number */
t3=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7887 in k7884 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in ... */
static void C_ccall f_7889(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7889,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7892,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=C_mutate((C_word*)lf[170]+1 /* (set! chicken.compiler.core#unroll-limit ...) */,t1);
t4=((C_word*)t0)[2];
f_5507(t4,t3);}
else{
/* batch-driver.scm:406: chicken.compiler.support#quit-compiling */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[436];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k7890 in k7887 in k7884 in k5500 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in ... */
static void C_ccall f_7892(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7892,c,av);}
t2=C_mutate((C_word*)lf[170]+1 /* (set! chicken.compiler.core#unroll-limit ...) */,t1);
t3=((C_word*)t0)[2];
f_5507(t3,t2);}

/* k7897 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in ... */
static void C_ccall f_7899(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(4,c,2)))){
C_save_and_reclaim((void *)f_7899,c,av);}
a=C_alloc(4);
t2=(*a=C_CLOSURE_TYPE|3,a[1]=(C_word)f_7902,a[2]=((C_word*)t0)[2],a[3]=t1,tmp=(C_word)a,a+=4,tmp);
/* batch-driver.scm:399: scheme#string->number */
t3=*((C_word*)lf[113]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t3+1)))(3,av2);}}

/* k7900 in k7897 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in ... */
static void C_ccall f_7902(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,3)))){
C_save_and_reclaim((void *)f_7902,c,av);}
a=C_alloc(3);
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_7905,a[2]=((C_word*)t0)[2],tmp=(C_word)a,a+=3,tmp);
if(C_truep(t1)){
t3=C_mutate((C_word*)lf[169]+1 /* (set! chicken.compiler.core#inline-max-size ...) */,t1);
t4=((C_word*)t0)[2];
f_5502(t4,t3);}
else{
/* batch-driver.scm:400: chicken.compiler.support#quit-compiling */
t3=*((C_word*)lf[79]+1);{
C_word *av2;
if(c >= 4) {
  av2=av;
} else {
  av2=C_alloc(4);
}
av2[0]=t3;
av2[1]=t2;
av2[2]=lf[437];
av2[3]=((C_word*)t0)[3];
((C_proc)(void*)(*((C_word*)t3+1)))(4,av2);}}}

/* k7903 in k7900 in k7897 in k5495 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in ... */
static void C_ccall f_7905(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7905,c,av);}
t2=C_mutate((C_word*)lf[169]+1 /* (set! chicken.compiler.core#inline-max-size ...) */,t1);
t3=((C_word*)t0)[2];
f_5502(t3,t2);}

/* k7910 in k5490 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in ... */
static void C_ccall f_7912(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7912,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5497(t3,t2);}

/* k7916 in k5485 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in ... */
static void C_ccall f_7918(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7918,c,av);}
t2=C_mutate(((C_word *)((C_word*)t0)[2])+1,t1);
t3=((C_word*)t0)[3];
f_5492(t3,t2);}

/* k7920 in k5480 in k5477 in k5474 in k5471 in k5468 in k5465 in k5462 in k5459 in k5456 in k5453 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in ... */
static void C_ccall f_7922(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7922,c,av);}
t2=C_mutate((C_word*)lf[195]+1 /* (set! chicken.compiler.core#emit-link-file ...) */,t1);
t3=((C_word*)t0)[2];
f_5487(t3,t2);}

/* k7952 in k5450 in k5447 in k5444 in k5441 in k5438 in k5435 in k5432 in k5429 in k5426 in k5423 in k5420 in k5417 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in ... */
static void C_ccall f_7954(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_7954,c,av);}
t2=C_set_block_item(lf[204] /* ##sys#warnings-enabled */,0,C_SCHEME_FALSE);
t3=C_set_block_item(((C_word*)t0)[2],0,C_SCHEME_FALSE);
t4=((C_word*)t0)[3];
f_5455(t4,t3);}

/* map-loop1309 in k5411 in k5385 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in ... */
static void C_fcall f_8001(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,0,2)))){
C_save_and_reclaim_args((void *)trf_8001,3,t0,t1,t2);}
a=C_alloc(7);
if(C_truep(C_i_pairp(t2))){
t3=C_slot(t2,C_fix(0));
t4=(*a=C_CLOSURE_TYPE|6,a[1]=(C_word)f_5404,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,a[6]=t3,tmp=(C_word)a,a+=7,tmp);
/* batch-driver.scm:343: scheme#string->symbol */
t5=*((C_word*)lf[338]+1);{
C_word av2[3];
av2[0]=t5;
av2[1]=t4;
av2[2]=t3;
((C_proc)(void*)(*((C_word*)t5+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8037 in k5382 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_8039(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8039,c,av);}
/* batch-driver.scm:339: chicken.base#exit */
t2=*((C_word*)lf[201]+1);{
C_word *av2=av;
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}

/* a8043 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_8044(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2=av[2];
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(10,c,2)))){
C_save_and_reclaim((void *)f_8044,c,av);}
a=C_alloc(10);
t3=C_a_i_cons(&a,2,C_SCHEME_UNDEFINED,C_SCHEME_END_OF_LIST);
t4=t3;
t5=(*a=C_VECTOR_TYPE|1,a[1]=t4,tmp=(C_word)a,a+=2,tmp);
t6=((C_word*)t5)[1];
t7=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8060,a[2]=t5,a[3]=t6,a[4]=t1,tmp=(C_word)a,a+=5,tmp);
/* ##sys#string->list */
t8=*((C_word*)lf[472]+1);{
C_word *av2=av;
av2[0]=t8;
av2[1]=t7;
av2[2]=t2;
((C_proc)(void*)(*((C_word*)t8+1)))(3,av2);}}

/* k8058 in a8043 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in ... */
static void C_ccall f_8060(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(7,c,3)))){
C_save_and_reclaim((void *)f_8060,c,av);}
a=C_alloc(7);
t2=C_SCHEME_UNDEFINED;
t3=(*a=C_VECTOR_TYPE|1,a[1]=t2,tmp=(C_word)a,a+=2,tmp);
t4=C_set_block_item(t3,0,(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8065,a[2]=((C_word*)t0)[2],a[3]=t3,a[4]=((C_word*)t0)[3],tmp=(C_word)a,a+=5,tmp));
t5=((C_word*)t3)[1];
f_8065(t5,((C_word*)t0)[4],t1);}

/* map-loop1281 in k8058 in a8043 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in ... */
static void C_fcall f_8065(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(8,0,2)))){
C_save_and_reclaim_args((void *)trf_8065,3,t0,t1,t2);}
a=C_alloc(8);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8090,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
t4=C_slot(t2,C_fix(0));
t5=C_a_i_string(&a,1,t4);
/* batch-driver.scm:334: scheme#string->symbol */
t6=*((C_word*)lf[338]+1);{
C_word av2[3];
av2[0]=t6;
av2[1]=t3;
av2[2]=t5;
((C_proc)(void*)(*((C_word*)t6+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[4],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8088 in map-loop1281 in k8058 in a8043 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in ... */
static void C_ccall f_8090(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8090,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8065(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8099 in k5375 in k5372 in k5369 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in ... */
static void C_ccall f_8101(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8101,c,av);}
/* batch-driver.scm:332: append-map */
f_2971(((C_word*)t0)[2],((C_word*)t0)[3],t1,C_SCHEME_END_OF_LIST);}

/* k8113 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_ccall f_8115(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,1)))){
C_save_and_reclaim((void *)f_8115,c,av);}
t2=C_mutate((C_word*)lf[318]+1 /* (set! chicken.compiler.core#unit-name ...) */,t1);
t3=((C_word*)t0)[2];
f_5371(t3,t2);}

/* k8117 in k4993 in k4968 in k4963 in k4960 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in ... */
static void C_ccall f_8119(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8119,c,av);}
/* batch-driver.scm:324: scheme#string->symbol */
t2=*((C_word*)lf[338]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}

/* map-loop1030 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in ... */
static void C_fcall f_8136(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(6,0,2)))){
C_save_and_reclaim_args((void *)trf_8136,3,t0,t1,t2);}
a=C_alloc(6);
if(C_truep(C_i_pairp(t2))){
t3=(*a=C_CLOSURE_TYPE|5,a[1]=(C_word)f_8161,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,a[5]=t2,tmp=(C_word)a,a+=6,tmp);
/* batch-driver.scm:227: g1036 */
t4=((C_word*)t0)[4];{
C_word av2[3];
av2[0]=t4;
av2[1]=t3;
av2[2]=C_slot(t2,C_fix(0));
((C_proc)(void*)(*((C_word*)t4+1)))(3,av2);}}
else{
t3=t1;{
C_word av2[2];
av2[0]=t3;
av2[1]=C_slot(((C_word*)t0)[5],C_fix(1));
((C_proc)(void*)(*((C_word*)t3+1)))(2,av2);}}}

/* k8159 in map-loop1030 in k4954 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in ... */
static void C_ccall f_8161(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void *)f_8161,c,av);}
a=C_alloc(3);
t2=C_a_i_cons(&a,2,t1,C_SCHEME_END_OF_LIST);
t3=C_i_setslot(((C_word*)((C_word*)t0)[2])[1],C_fix(1),t2);
t4=C_mutate(((C_word *)((C_word*)t0)[2])+1,t2);
t5=((C_word*)((C_word*)t0)[3])[1];
f_8136(t5,((C_word*)t0)[4],C_slot(((C_word*)t0)[5],C_fix(1)));}

/* k8170 in k4948 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_8172(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8172,c,av);}
if(C_truep(t1)){
/* batch-driver.scm:228: ##sys#split-path */
t2=*((C_word*)lf[477]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
/* batch-driver.scm:228: ##sys#split-path */
t2=*((C_word*)lf[477]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=lf[478];
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}}

/* g1023 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_fcall f_8176(C_word t0,C_word t1,C_word t2){
C_word tmp;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(3,0,2)))){
C_save_and_reclaim_args((void *)trf_8176,3,t0,t1,t2);}
a=C_alloc(3);
t3=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8180,a[2]=t1,tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:221: option-arg */
f_4890(t3,t2);}

/* k8178 in g1023 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in ... */
static void C_ccall f_8180(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,2)))){
C_save_and_reclaim((void *)f_8180,c,av);}
if(C_truep(C_i_symbolp(t1))){
/* batch-driver.scm:223: scheme#symbol->string */
t2=*((C_word*)lf[265]+1);{
C_word *av2;
if(c >= 3) {
  av2=av;
} else {
  av2=C_alloc(3);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(3,av2);}}
else{
t2=((C_word*)t0)[2];{
C_word *av2=av;
av2[0]=t2;
av2[1]=t1;
((C_proc)(void*)(*((C_word*)t2+1)))(2,av2);}}}

/* k8202 in k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_ccall f_8204(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,c,4)))){
C_save_and_reclaim((void *)f_8204,c,av);}
/* batch-driver.scm:226: chicken.pathname#make-pathname */
t2=*((C_word*)lf[263]+1);{
C_word *av2;
if(c >= 5) {
  av2=av;
} else {
  av2=C_alloc(5);
}
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=C_SCHEME_FALSE;
av2[3]=t1;
av2[4]=lf[482];
((C_proc)(void*)(*((C_word*)t2+1)))(5,av2);}}

/* k8213 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_fcall f_8215(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word t6;
C_word t7;
C_word t8;
C_word t9;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(19,0,4)))){
C_save_and_reclaim_args((void *)trf_8215,2,t0,t1);}
a=C_alloc(19);
t2=C_a_i_cons(&a,2,((C_word*)t0)[2],t1);
t3=t2;
t4=(*a=C_VECTOR_TYPE|1,a[1]=t3,tmp=(C_word)a,a+=2,tmp);
t5=C_u_i_memq(lf[90],((C_word*)t0)[3]);
t6=C_u_i_memq(lf[91],((C_word*)t0)[3]);
t7=(*a=C_CLOSURE_TYPE|10,a[1]=(C_word)f_4950,a[2]=((C_word*)t0)[3],a[3]=((C_word*)t0)[4],a[4]=t5,a[5]=t4,a[6]=((C_word*)t0)[5],a[7]=((C_word*)t0)[6],a[8]=((C_word*)t0)[7],a[9]=((C_word*)t0)[8],a[10]=((C_word*)t0)[9],tmp=(C_word)a,a+=11,tmp);
if(C_truep(t6)){
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8176,a[2]=((C_word*)t0)[4],tmp=(C_word)a,a+=3,tmp);
/* batch-driver.scm:219: g1023 */
t9=t8;
f_8176(t9,t7,t6);}
else{
if(C_truep(C_u_i_memq(lf[481],((C_word*)t0)[3]))){
t8=t7;{
C_word av2[2];
av2[0]=t8;
av2[1]=C_SCHEME_FALSE;
f_4950(2,av2);}}
else{
t8=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_8204,a[2]=t7,tmp=(C_word)a,a+=3,tmp);
if(C_truep(((C_word*)t0)[6])){
/* batch-driver.scm:226: chicken.pathname#pathname-file */
t9=*((C_word*)lf[483]+1);{
C_word av2[3];
av2[0]=t9;
av2[1]=t8;
av2[2]=((C_word*)t0)[6];
((C_proc)(void*)(*((C_word*)t9+1)))(3,av2);}}
else{
/* batch-driver.scm:226: chicken.pathname#make-pathname */
t9=*((C_word*)lf[263]+1);{
C_word av2[5];
av2[0]=t9;
av2[1]=t7;
av2[2]=C_SCHEME_FALSE;
av2[3]=lf[484];
av2[4]=lf[482];
((C_proc)(void*)(*((C_word*)t9+1)))(5,av2);}}}}}

/* k8232 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_ccall f_8234(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word t4;
C_word t5;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(12,c,1)))){
C_save_and_reclaim((void *)f_8234,c,av);}
a=C_alloc(12);
t2=C_a_i_cons(&a,2,lf[389],t1);
if(C_truep(*((C_word*)lf[82]+1))){
t3=((C_word*)t0)[2];
f_8215(t3,C_a_i_cons(&a,2,t2,C_SCHEME_END_OF_LIST));}
else{
t3=C_a_i_cons(&a,2,lf[386],*((C_word*)lf[485]+1));
t4=C_a_i_list(&a,1,t3);
t5=((C_word*)t0)[2];
f_8215(t5,C_a_i_cons(&a,2,t2,t4));}}

/* k8240 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in k2723 in ... */
static void C_fcall f_8242(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word t3;
C_word t4;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(5,0,2)))){
C_save_and_reclaim_args((void *)trf_8242,2,t0,t1);}
a=C_alloc(5);
t2=(*a=C_CLOSURE_TYPE|4,a[1]=(C_word)f_8249,a[2]=((C_word*)t0)[2],a[3]=((C_word*)t0)[3],a[4]=t1,tmp=(C_word)a,a+=5,tmp);
if(C_truep(*((C_word*)lf[489]+1))){
if(C_truep(C_i_not(((C_word*)t0)[4]))){
if(C_truep(C_i_not(((C_word*)t0)[5]))){
if(C_truep(C_i_not(*((C_word*)lf[82]+1)))){
t3=C_i_not(*((C_word*)lf[490]+1));
t4=t2;
f_8249(t4,(C_truep(t3)?t3:C_eqp(*((C_word*)lf[490]+1),lf[491])));}
else{
t3=t2;
f_8249(t3,C_SCHEME_FALSE);}}
else{
t3=t2;
f_8249(t3,C_SCHEME_FALSE);}}
else{
t3=t2;
f_8249(t3,C_SCHEME_FALSE);}}
else{
t3=t2;
f_8249(t3,C_SCHEME_FALSE);}}

/* k8247 in k8240 in k4934 in k4931 in k4928 in k4919 in chicken.compiler.batch-driver#compile-source-file in k2768 in k2765 in k2762 in k2759 in k2756 in k2753 in k2750 in k2747 in k2744 in k2741 in k2738 in k2735 in k2732 in k2729 in k2726 in ... */
static void C_fcall f_8249(C_word t0,C_word t1){
C_word tmp;
C_word t2;
C_word *a;
if(C_unlikely(!C_demand(C_calculate_demand(0,0,5)))){
C_save_and_reclaim_args((void *)trf_8249,2,t0,t1);}
if(C_truep(t1)){
/* batch-driver.scm:199: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[487]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=lf[488];
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}
else{
/* batch-driver.scm:199: scheme#append */
t2=*((C_word*)lf[4]+1);{
C_word av2[6];
av2[0]=t2;
av2[1]=((C_word*)t0)[2];
av2[2]=*((C_word*)lf[487]+1);
av2[3]=((C_word*)t0)[3];
av2[4]=((C_word*)t0)[4];
av2[5]=C_SCHEME_END_OF_LIST;
((C_proc)(void*)(*((C_word*)t2+1)))(6,av2);}}}

/* toplevel */
static C_TLS int toplevel_initialized=0;

void C_ccall C_batch_2ddriver_toplevel(C_word c,C_word *av){
C_word tmp;
C_word t0=av[0];
C_word t1=av[1];
C_word t2;
C_word t3;
C_word *a;
if(toplevel_initialized) {C_kontinue(t1,C_SCHEME_UNDEFINED);}
else C_toplevel_entry(C_text("batch-driver"));
C_check_nursery_minimum(C_calculate_demand(3,c,2));
if(C_unlikely(!C_demand(C_calculate_demand(3,c,2)))){
C_save_and_reclaim((void*)C_batch_2ddriver_toplevel,c,av);}
toplevel_initialized=1;
if(C_unlikely(!C_demand_2(3159))){
C_save(t1);
C_rereclaim2(3159*sizeof(C_word),1);
t1=C_restore;}
a=C_alloc(3);
C_initialize_lf(lf,499);
lf[0]=C_h_intern(&lf[0],12, C_text("batch-driver"));
lf[1]=C_h_intern(&lf[1],30, C_text("chicken.compiler.batch-driver#"));
lf[3]=C_h_intern(&lf[3],5, C_text("foldr"));
lf[4]=C_h_intern(&lf[4],13, C_text("scheme#append"));
lf[5]=C_h_intern(&lf[5],3, C_text("map"));
lf[9]=C_h_intern(&lf[9],39, C_text("chicken.compiler.core#standard-bindings"));
lf[10]=C_h_intern(&lf[10],8, C_text("for-each"));
lf[11]=C_h_intern(&lf[11],39, C_text("chicken.compiler.core#extended-bindings"));
lf[12]=C_h_intern(&lf[12],39, C_text("chicken.compiler.core#internal-bindings"));
lf[13]=C_h_intern(&lf[13],38, C_text("chicken.compiler.support#mark-variable"));
lf[14]=C_h_intern(&lf[14],20, C_text("##compiler#intrinsic"));
lf[15]=C_h_intern(&lf[15],8, C_text("internal"));
lf[16]=C_h_intern(&lf[16],8, C_text("extended"));
lf[17]=C_h_intern(&lf[17],8, C_text("standard"));
lf[19]=C_h_intern(&lf[19],14, C_text("scheme#newline"));
lf[20]=C_h_intern(&lf[20],21, C_text("##sys#standard-output"));
lf[21]=C_h_intern(&lf[21],6, C_text("printf"));
lf[22]=C_h_intern(&lf[22],11, C_text("##sys#print"));
lf[23]=C_h_intern(&lf[23],40, C_text("chicken.compiler.support#node-parameters"));
lf[24]=C_h_intern(&lf[24],35, C_text("chicken.compiler.support#node-class"));
lf[25]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006\011pval="));
lf[26]=C_h_intern(&lf[26],7, C_text("unknown"));
lf[27]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005\011val="));
lf[28]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006\011lval="));
lf[29]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005\011css="));
lf[30]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010\011drvars="));
lf[31]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006\011refs="));
lf[32]=C_h_intern(&lf[32],8, C_text("captured"));
lf[33]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001captured\376\001\000\000\003\001cpt\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001assigned\376\001\000\000\003\001set\376\003\000\000\002\376\003\000\000\002\376\001\000\000"
"\005\001boxed\376\001\000\000\003\001box\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001global\376\001\000\000\003\001glo\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001assigned-locally\376"
"\001\000\000\003\001stl\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001contractable\376\001\000\000\003\001con\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001standard-binding\376\001\000"
"\000\003\001stb\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001simple\376\001\000\000\003\001sim\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001inlinable\376\001\000\000\003\001inl\376\003\000\000\002\376\003\000\000"
"\002\376\001\000\000\013\001collapsable\376\001\000\000\003\001col\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001removable\376\001\000\000\003\001rem\376\003\000\000\002\376\003\000\000\002\376\001\000\000\010\001con"
"stant\376\001\000\000\003\001con\376\003\000\000\002\376\003\000\000\002\376\001\000\000\015\001inline-target\376\001\000\000\003\001ilt\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001inline-trans"
"ient\376\001\000\000\003\001itr\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001undefined\376\001\000\000\003\001und\376\003\000\000\002\376\003\000\000\002\376\001\000\000\011\001replacing\376\001\000\000\003\001rp"
"g\376\003\000\000\002\376\003\000\000\002\376\001\000\000\006\001unused\376\001\000\000\003\001uud\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001extended-binding\376\001\000\000\003\001xtb\376\003\000\000\002\376\003"
"\000\000\002\376\001\000\000\015\001inline-export\376\001\000\000\003\001ilx\376\003\000\000\002\376\003\000\000\002\376\001\000\000\013\001hidden-refs\376\001\000\000\003\001hrf\376\003\000\000\002\376\003\000\000\002\376\001\000"
"\000\011\001value-ref\376\001\000\000\003\001vvf\376\003\000\000\002\376\003\000\000\002\376\001\000\000\014\001customizable\376\001\000\000\003\001cst\376\003\000\000\002\376\003\000\000\002\376\001\000\000\025\001has-un"
"used-parameters\376\001\000\000\003\001hup\376\003\000\000\002\376\003\000\000\002\376\001\000\000\012\001boxed-rest\376\001\000\000\003\001bxr\376\377\016"));
lf[34]=C_h_intern(&lf[34],18, C_text("##sys#write-char-0"));
lf[35]=C_h_intern(&lf[35],5, C_text("value"));
lf[36]=C_h_intern(&lf[36],11, C_text("local-value"));
lf[37]=C_h_intern(&lf[37],16, C_text("potential-values"));
lf[38]=C_h_intern(&lf[38],10, C_text("replacable"));
lf[39]=C_h_intern(&lf[39],17, C_text("derived-rest-vars"));
lf[40]=C_h_intern(&lf[40],10, C_text("references"));
lf[41]=C_h_intern(&lf[41],10, C_text("call-sites"));
lf[42]=C_h_intern(&lf[42],29, C_text("chicken.compiler.support#bomb"));
lf[43]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020Illegal property"));
lf[44]=C_h_intern(&lf[44],4, C_text("home"));
lf[45]=C_h_intern(&lf[45],8, C_text("contains"));
lf[46]=C_h_intern(&lf[46],12, C_text("contained-in"));
lf[47]=C_h_intern(&lf[47],8, C_text("use-expr"));
lf[48]=C_h_intern(&lf[48],12, C_text("closure-size"));
lf[49]=C_h_intern(&lf[49],14, C_text("rest-parameter"));
lf[50]=C_h_intern(&lf[50],18, C_text("captured-variables"));
lf[51]=C_h_intern(&lf[51],13, C_text("explicit-rest"));
lf[52]=C_h_intern(&lf[52],8, C_text("rest-cdr"));
lf[53]=C_h_intern(&lf[53],10, C_text("rest-null\077"));
lf[54]=C_h_intern(&lf[54],8, C_text("assigned"));
lf[55]=C_h_intern(&lf[55],5, C_text("boxed"));
lf[56]=C_h_intern(&lf[56],6, C_text("global"));
lf[57]=C_h_intern(&lf[57],12, C_text("contractable"));
lf[58]=C_h_intern(&lf[58],16, C_text("standard-binding"));
lf[59]=C_h_intern(&lf[59],16, C_text("assigned-locally"));
lf[60]=C_h_intern(&lf[60],11, C_text("collapsable"));
lf[61]=C_h_intern(&lf[61],9, C_text("removable"));
lf[62]=C_h_intern(&lf[62],9, C_text("undefined"));
lf[63]=C_h_intern(&lf[63],9, C_text("replacing"));
lf[64]=C_h_intern(&lf[64],6, C_text("unused"));
lf[65]=C_h_intern(&lf[65],6, C_text("simple"));
lf[66]=C_h_intern(&lf[66],9, C_text("inlinable"));
lf[67]=C_h_intern(&lf[67],13, C_text("inline-export"));
lf[68]=C_h_intern(&lf[68],21, C_text("has-unused-parameters"));
lf[69]=C_h_intern(&lf[69],16, C_text("extended-binding"));
lf[70]=C_h_intern(&lf[70],12, C_text("customizable"));
lf[71]=C_h_intern(&lf[71],8, C_text("constant"));
lf[72]=C_h_intern(&lf[72],10, C_text("boxed-rest"));
lf[73]=C_h_intern(&lf[73],11, C_text("hidden-refs"));
lf[74]=C_h_intern(&lf[74],12, C_text("scheme#write"));
lf[75]=C_h_intern(&lf[75],36, C_text("chicken.internal#hash-table-for-each"));
lf[76]=C_h_intern(&lf[76],47, C_text("chicken.compiler.core#default-standard-bindings"));
lf[77]=C_h_intern(&lf[77],47, C_text("chicken.compiler.core#default-extended-bindings"));
lf[78]=C_h_intern(&lf[78],49, C_text("chicken.compiler.batch-driver#compile-source-file"));
lf[79]=C_h_intern(&lf[79],39, C_text("chicken.compiler.support#quit-compiling"));
lf[80]=C_decode_literal(C_heaptop,C_text("\376B\000\000 missing argument to `-~A\047 option"));
lf[81]=C_decode_literal(C_heaptop,C_text("\376B\000\000\037invalid argument to `~A\047 option"));
lf[82]=C_h_intern(&lf[82],39, C_text("chicken.compiler.core#explicit-use-flag"));
lf[83]=C_h_intern(&lf[83],12, C_text("explicit-use"));
lf[84]=C_h_intern(&lf[84],37, C_text("chicken.compiler.core#emit-debug-info"));
lf[85]=C_h_intern(&lf[85],10, C_text("debug-info"));
lf[86]=C_h_intern(&lf[86],7, C_text("dynamic"));
lf[87]=C_h_intern(&lf[87],4, C_text("unit"));
lf[88]=C_h_intern(&lf[88],17, C_text("import-for-syntax"));
lf[89]=C_h_intern(&lf[89],39, C_text("chicken.internal#default-syntax-imports"));
lf[90]=C_h_intern(&lf[90],7, C_text("verbose"));
lf[91]=C_h_intern(&lf[91],11, C_text("output-file"));
lf[92]=C_h_intern(&lf[92],39, C_text("chicken.compiler.support#chop-separator"));
lf[93]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\003\000\000\002\376\001\000\000\042\001chicken.base#implicit-exit-handler\376\377\016\376\377\016\376\377\016"));
lf[94]=C_h_intern(&lf[94],7, C_text("profile"));
lf[95]=C_h_intern(&lf[95],12, C_text("profile-name"));
lf[96]=C_h_intern(&lf[96],9, C_text("heap-size"));
lf[97]=C_h_intern(&lf[97],13, C_text("keyword-style"));
lf[98]=C_h_intern(&lf[98],10, C_text("clustering"));
lf[99]=C_h_intern(&lf[99],12, C_text("analyze-only"));
lf[100]=C_h_intern(&lf[100],4, C_text("lfa2"));
lf[101]=C_h_intern(&lf[101],7, C_text("nursery"));
lf[102]=C_h_intern(&lf[102],10, C_text("stack-size"));
lf[103]=C_h_intern(&lf[103],6, C_text("module"));
lf[104]=C_h_intern(&lf[104],42, C_text("chicken.compiler.support#debugging-chicken"));
lf[105]=C_h_intern(&lf[105],34, C_text("chicken.compiler.support#debugging"));
lf[106]=C_h_intern(&lf[106],1, C_text("p"));
lf[107]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004pass"));
lf[108]=C_h_intern(&lf[108],35, C_text("chicken.compiler.support#dump-nodes"));
lf[109]=C_h_intern(&lf[109],33, C_text("chicken.pretty-print#pretty-print"));
lf[110]=C_h_intern(&lf[110],46, C_text("chicken.compiler.support#build-expression-tree"));
lf[111]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013(iteration "));
lf[112]=C_decode_literal(C_heaptop,C_text("\376B\000\000\033invalid numeric argument ~S"));
lf[113]=C_h_intern(&lf[113],21, C_text("scheme#string->number"));
lf[114]=C_h_intern(&lf[114],16, C_text("scheme#substring"));
lf[115]=C_h_intern(&lf[115],33, C_text("chicken.time#current-milliseconds"));
lf[116]=C_h_intern(&lf[116],21, C_text("scheme#inexact->exact"));
lf[117]=C_h_intern(&lf[117],12, C_text("scheme#round"));
lf[118]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003: \011"));
lf[119]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030milliseconds needed for "));
lf[120]=C_h_intern(&lf[120],31, C_text("chicken.compiler.support#db-get"));
lf[121]=C_h_intern(&lf[121],32, C_text("chicken.compiler.support#db-put!"));
lf[122]=C_h_intern(&lf[122],40, C_text("chicken.compiler.core#analyze-expression"));
lf[123]=C_h_intern(&lf[123],43, C_text("chicken.compiler.core#enable-specialization"));
lf[124]=C_h_intern(&lf[124],10, C_text("specialize"));
lf[125]=C_h_intern(&lf[125],1, C_text("D"));
lf[126]=C_h_intern(&lf[126],38, C_text("chicken.compiler.core#import-libraries"));
lf[127]=C_h_intern(&lf[127],14, C_text("emit-link-file"));
lf[128]=C_h_intern(&lf[128],16, C_text("emit-inline-file"));
lf[129]=C_h_intern(&lf[129],15, C_text("emit-types-file"));
lf[130]=C_h_intern(&lf[130],12, C_text("inline-limit"));
lf[131]=C_h_intern(&lf[131],12, C_text("unroll-limit"));
lf[132]=C_h_intern(&lf[132],34, C_text("chicken.compiler.core#verbose-mode"));
lf[133]=C_h_intern(&lf[133],33, C_text("##sys#read-error-with-line-number"));
lf[134]=C_h_intern(&lf[134],23, C_text("##sys#include-pathnames"));
lf[135]=C_h_intern(&lf[135],34, C_text("chicken.platform#register-feature!"));
lf[136]=C_h_intern(&lf[136],36, C_text("chicken.platform#unregister-feature!"));
lf[137]=C_h_intern_kw(&lf[137],18, C_text("compiler-extension"));
lf[138]=C_h_intern(&lf[138],14, C_text("##sys#features"));
lf[139]=C_h_intern_kw(&lf[139],9, C_text("compiling"));
lf[140]=C_h_intern(&lf[140],38, C_text("chicken.compiler.core#target-heap-size"));
lf[141]=C_h_intern(&lf[141],39, C_text("chicken.compiler.core#target-stack-size"));
lf[142]=C_h_intern(&lf[142],8, C_text("no-trace"));
lf[143]=C_h_intern(&lf[143],37, C_text("chicken.compiler.core#emit-trace-info"));
lf[144]=C_h_intern(&lf[144],53, C_text("chicken.compiler.core#disable-stack-overflow-checking"));
lf[145]=C_h_intern(&lf[145],29, C_text("disable-stack-overflow-checks"));
lf[146]=C_h_intern(&lf[146],36, C_text("chicken.compiler.core#bootstrap-mode"));
lf[147]=C_h_intern(&lf[147],7, C_text("version"));
lf[148]=C_h_intern(&lf[148],38, C_text("chicken.compiler.support#print-version"));
lf[149]=C_h_intern(&lf[149],4, C_text("help"));
lf[150]=C_h_intern(&lf[150],36, C_text("chicken.compiler.support#print-usage"));
lf[151]=C_h_intern(&lf[151],7, C_text("release"));
lf[152]=C_h_intern(&lf[152],14, C_text("scheme#display"));
lf[153]=C_h_intern(&lf[153],32, C_text("chicken.platform#chicken-version"));
lf[154]=C_decode_literal(C_heaptop,C_text("\376B\000\0001\012Run `csi\047 to start the interactive interpreter.\012"));
lf[155]=C_decode_literal(C_heaptop,C_text("\376B\000\000.or try `csc\047 for a more convenient interface.\012"));
lf[156]=C_decode_literal(C_heaptop,C_text("\376B\000\000C\012Enter `chicken -help\047 for information on how to use the compiler,\012"));
lf[157]=C_h_intern(&lf[157],26, C_text("##sys#line-number-database"));
lf[158]=C_h_intern(&lf[158],29, C_text("##sys#current-source-filename"));
lf[159]=C_h_intern(&lf[159],45, C_text("chicken.compiler.core#canonicalize-expression"));
lf[160]=C_h_intern(&lf[160],18, C_text("##sys#dynamic-wind"));
lf[161]=C_h_intern(&lf[161],44, C_text("chicken.compiler.core#line-number-database-2"));
lf[162]=C_h_intern(&lf[162],36, C_text("chicken.compiler.core#constant-table"));
lf[163]=C_h_intern(&lf[163],34, C_text("chicken.compiler.core#inline-table"));
lf[164]=C_h_intern(&lf[164],36, C_text("chicken.compiler.core#first-analysis"));
lf[165]=C_h_intern(&lf[165],54, C_text("chicken.compiler.optimizer#determine-loop-and-dispatch"));
lf[166]=C_h_intern(&lf[166],59, C_text("chicken.compiler.optimizer#perform-high-level-optimizations"));
lf[167]=C_h_intern(&lf[167],39, C_text("chicken.compiler.core#block-compilation"));
lf[168]=C_h_intern(&lf[168],36, C_text("chicken.compiler.core#inline-locally"));
lf[169]=C_h_intern(&lf[169],37, C_text("chicken.compiler.core#inline-max-size"));
lf[170]=C_h_intern(&lf[170],34, C_text("chicken.compiler.core#unroll-limit"));
lf[171]=C_h_intern(&lf[171],50, C_text("chicken.compiler.core#inline-substitutions-enabled"));
lf[172]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022clustering enabled"));
lf[173]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022rewritings enabled"));
lf[174]=C_h_intern(&lf[174],44, C_text("chicken.compiler.core#optimize-leaf-routines"));
lf[175]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031leaf routine optimization"));
lf[176]=C_h_intern(&lf[176],52, C_text("chicken.compiler.optimizer#transform-direct-lambdas!"));
lf[177]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010analysis"));
lf[178]=C_h_intern(&lf[178],4, C_text("leaf"));
lf[179]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023optimized-iteration"));
lf[180]=C_h_intern(&lf[180],1, C_text("5"));
lf[181]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014optimization"));
lf[182]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021optimization pass"));
lf[183]=C_h_intern(&lf[183],49, C_text("chicken.compiler.core#prepare-for-code-generation"));
lf[184]=C_h_intern(&lf[184],22, C_text("chicken.format#sprintf"));
lf[185]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025compilation finished."));
lf[186]=C_h_intern(&lf[186],46, C_text("chicken.compiler.support#compiler-cleanup-hook"));
lf[187]=C_h_intern(&lf[187],1, C_text("t"));
lf[188]=C_h_intern(&lf[188],19, C_text("##sys#display-times"));
lf[189]=C_h_intern(&lf[189],16, C_text("##sys#stop-timer"));
lf[190]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017code generation"));
lf[191]=C_h_intern(&lf[191],24, C_text("scheme#close-output-port"));
lf[192]=C_h_intern(&lf[192],40, C_text("chicken.compiler.c-backend#generate-code"));
lf[193]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023generating `~A\047 ..."));
lf[194]=C_h_intern(&lf[194],23, C_text("scheme#open-output-file"));
lf[195]=C_h_intern(&lf[195],36, C_text("chicken.compiler.core#emit-link-file"));
lf[196]=C_h_intern(&lf[196],23, C_text("chicken.pretty-print#pp"));
lf[197]=C_h_intern(&lf[197],46, C_text("chicken.compiler.core#linked-static-extensions"));
lf[198]=C_h_intern(&lf[198],26, C_text("scheme#with-output-to-file"));
lf[199]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035generating link file `~a\047 ..."));
lf[200]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013preparation"));
lf[201]=C_h_intern(&lf[201],17, C_text("chicken.base#exit"));
lf[202]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021closure-converted"));
lf[203]=C_h_intern(&lf[203],1, C_text("9"));
lf[204]=C_h_intern(&lf[204],22, C_text("##sys#warnings-enabled"));
lf[205]=C_decode_literal(C_heaptop,C_text("\376B\000\000#(don\047t worry - still compiling...)\012"));
lf[206]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016final-analysis"));
lf[207]=C_h_intern(&lf[207],1, C_text("8"));
lf[208]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022closure conversion"));
lf[209]=C_h_intern(&lf[209],48, C_text("chicken.compiler.core#perform-closure-conversion"));
lf[210]=C_h_intern(&lf[210],41, C_text("chicken.compiler.core#insert-timer-checks"));
lf[211]=C_h_intern(&lf[211],37, C_text("chicken.compiler.core#foreign-stub-id"));
lf[212]=C_h_intern(&lf[212],42, C_text("chicken.compiler.core#foreign-lambda-stubs"));
lf[213]=C_h_intern(&lf[213],48, C_text("chicken.compiler.support#emit-global-inline-file"));
lf[214]=C_decode_literal(C_heaptop,C_text("\376B\000\000&generating global inline file `~a\047 ..."));
lf[215]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011optimized"));
lf[216]=C_h_intern(&lf[216],1, C_text("7"));
lf[217]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010unboxing"));
lf[218]=C_h_intern(&lf[218],38, C_text("chicken.compiler.lfa2#perform-unboxing"));
lf[219]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016doing unboxing"));
lf[220]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027secondary flow analysis"));
lf[221]=C_h_intern(&lf[221],53, C_text("chicken.compiler.lfa2#perform-secondary-flow-analysis"));
lf[222]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012doing lfa2"));
lf[223]=C_h_intern(&lf[223],1, C_text("s"));
lf[224]=C_h_intern(&lf[224],49, C_text("chicken.compiler.core#compute-database-statistics"));
lf[225]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027;   database entries: \011"));
lf[226]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027;   known call sites: \011"));
lf[227]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027;   global variables: \011"));
lf[228]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027;   known procedures: \011"));
lf[229]=C_decode_literal(C_heaptop,C_text("\376B\000\000\042;   variables with known values: \011"));
lf[230]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032 \011original program size: \011"));
lf[231]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023;   program size: \011"));
lf[232]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023program statistics:"));
lf[233]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010analysis"));
lf[234]=C_h_intern(&lf[234],1, C_text("4"));
lf[235]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010analysis"));
lf[236]=C_h_intern(&lf[236],44, C_text("chicken.compiler.scrutinizer#emit-types-file"));
lf[237]=C_decode_literal(C_heaptop,C_text("\376B\000\000\035generating type file `~a\047 ..."));
lf[238]=C_h_intern(&lf[238],1, C_text("v"));
lf[239]=C_h_intern(&lf[239],41, C_text("chicken.compiler.support#dump-global-refs"));
lf[240]=C_h_intern(&lf[240],1, C_text("d"));
lf[241]=C_h_intern(&lf[241],45, C_text("chicken.compiler.support#dump-defined-globals"));
lf[242]=C_h_intern(&lf[242],1, C_text("u"));
lf[243]=C_h_intern(&lf[243],47, C_text("chicken.compiler.support#dump-undefined-globals"));
lf[244]=C_h_intern(&lf[244],3, C_text("opt"));
lf[245]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003cps"));
lf[246]=C_h_intern(&lf[246],1, C_text("3"));
lf[247]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016cps conversion"));
lf[248]=C_h_intern(&lf[248],44, C_text("chicken.compiler.core#perform-cps-conversion"));
lf[249]=C_h_intern(&lf[249],31, C_text("chicken.compiler.support#unsafe"));
lf[250]=C_h_intern(&lf[250],52, C_text("chicken.compiler.optimizer#scan-toplevel-assignments"));
lf[251]=C_h_intern(&lf[251],44, C_text("chicken.compiler.support#node-subexpressions"));
lf[252]=C_decode_literal(C_heaptop,C_text("\376B\000\000\016specialization"));
lf[253]=C_h_intern(&lf[253],1, C_text("P"));
lf[254]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010scrutiny"));
lf[255]=C_h_intern(&lf[255],39, C_text("chicken.compiler.scrutinizer#scrutinize"));
lf[256]=C_h_intern(&lf[256],43, C_text("chicken.compiler.core#strict-variable-types"));
lf[257]=C_decode_literal(C_heaptop,C_text("\376B\000\000\023performing scrutiny"));
lf[258]=C_decode_literal(C_heaptop,C_text("\376B\000\000\027pre-analysis (scrutiny)"));
lf[259]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010analysis"));
lf[260]=C_h_intern(&lf[260],1, C_text("0"));
lf[261]=C_h_intern(&lf[261],8, C_text("scrutiny"));
lf[262]=C_h_intern(&lf[262],47, C_text("chicken.compiler.scrutinizer#load-type-database"));
lf[263]=C_h_intern(&lf[263],30, C_text("chicken.pathname#make-pathname"));
lf[264]=C_decode_literal(C_heaptop,C_text("\376B\000\000\005types"));
lf[265]=C_h_intern(&lf[265],21, C_text("scheme#symbol->string"));
lf[266]=C_decode_literal(C_heaptop,C_text("\376B\000\000\034type-database `~a\047 not found"));
lf[267]=C_h_intern(&lf[267],18, C_text("consult-types-file"));
lf[268]=C_h_intern(&lf[268],17, C_text("ignore-repository"));
lf[269]=C_decode_literal(C_heaptop,C_text("\376B\000\000\052default type-database `types.db\047 not found"));
lf[270]=C_decode_literal(C_heaptop,C_text("\376B\000\000\010types.db"));
lf[271]=C_h_intern(&lf[271],41, C_text("chicken.compiler.support#load-inline-file"));
lf[272]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032Loading inline file ~a ..."));
lf[273]=C_h_intern(&lf[273],19, C_text("consult-inline-file"));
lf[274]=C_h_intern(&lf[274],41, C_text("chicken.compiler.core#enable-inline-files"));
lf[275]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032Loading inline file ~a ..."));
lf[276]=C_h_intern(&lf[276],30, C_text("##sys#resolve-include-filename"));
lf[277]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376B\000\000\007.inline\376\377\016"));
lf[278]=C_h_intern(&lf[278],1, C_text("M"));
lf[279]=C_decode_literal(C_heaptop,C_text("\376B\000\000\017; requirements:"));
lf[280]=C_h_intern(&lf[280],19, C_text("scheme#vector->list"));
lf[281]=C_h_intern(&lf[281],39, C_text("chicken.compiler.core#file-requirements"));
lf[282]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021initial node tree"));
lf[283]=C_h_intern(&lf[283],1, C_text("T"));
lf[284]=C_h_intern(&lf[284],46, C_text("chicken.compiler.core#build-toplevel-procedure"));
lf[285]=C_h_intern(&lf[285],41, C_text("chicken.compiler.support#build-node-graph"));
lf[286]=C_h_intern(&lf[286],48, C_text("chicken.compiler.support#canonicalize-begin-body"));
lf[287]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011user pass"));
lf[288]=C_decode_literal(C_heaptop,C_text("\376B\000\000\014User pass..."));
lf[289]=C_h_intern(&lf[289],36, C_text("chicken.compiler.user-pass#user-pass"));
lf[290]=C_h_intern(&lf[290],12, C_text("check-syntax"));
lf[291]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015canonicalized"));
lf[292]=C_h_intern(&lf[292],1, C_text("2"));
lf[293]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020canonicalization"));
lf[294]=C_h_intern(&lf[294],53, C_text("chicken.compiler.support#display-line-number-database"));
lf[295]=C_h_intern(&lf[295],1, C_text("n"));
lf[296]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025line number database:"));
lf[297]=C_h_intern(&lf[297],48, C_text("chicken.compiler.support#display-real-name-table"));
lf[298]=C_h_intern(&lf[298],1, C_text("N"));
lf[299]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020real name table:"));
lf[300]=C_h_intern(&lf[300],59, C_text("chicken.compiler.compiler-syntax#compiler-syntax-statistics"));
lf[301]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002\011\011"));
lf[302]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002  "));
lf[303]=C_h_intern(&lf[303],18, C_text("chicken.base#print"));
lf[304]=C_decode_literal(C_heaptop,C_text("\376B\000\000\030applied compiler syntax:"));
lf[305]=C_h_intern(&lf[305],46, C_text("chicken.compiler.support#with-debugging-output"));
lf[306]=C_h_intern(&lf[306],1, C_text("S"));
lf[307]=C_h_intern(&lf[307],6, C_text("format"));
lf[308]=C_h_intern(&lf[308],20, C_text("chicken.base#warning"));
lf[309]=C_h_intern(&lf[309],30, C_text("chicken.base#get-output-string"));
lf[310]=C_h_intern(&lf[310],23, C_text("chicken.string#->string"));
lf[311]=C_h_intern(&lf[311],33, C_text("chicken.string#string-intersperse"));
lf[312]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[313]=C_decode_literal(C_heaptop,C_text("\376B\000\0006the following extensions are not currently installed: "));
lf[314]=C_h_intern(&lf[314],31, C_text("chicken.base#open-output-string"));
lf[315]=C_h_intern(&lf[315],19, C_text("chicken.base#notice"));
lf[316]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[317]=C_decode_literal(C_heaptop,C_text("\376B\000\000; has dynamic requirements but doesn\047t load (chicken eval): "));
lf[318]=C_h_intern(&lf[318],31, C_text("chicken.compiler.core#unit-name"));
lf[319]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004unit"));
lf[320]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007library"));
lf[321]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007program"));
lf[322]=C_h_intern(&lf[322],32, C_text("chicken.compiler.core#used-units"));
lf[323]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\001\000\000\004\001eval\376\003\000\000\002\376\001\000\000\004\001repl\376\377\016"));
lf[324]=C_h_intern(&lf[324],35, C_text("chicken.load#find-dynamic-extension"));
lf[325]=C_h_intern(&lf[325],31, C_text("chicken.internal#hash-table-ref"));
lf[326]=C_decode_literal(C_heaptop,C_text("\376B\000\000;No module definition found for import libraries to emit: ~A"));
lf[327]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[328]=C_h_intern(&lf[328],41, C_text("chicken.compiler.core#immutable-constants"));
lf[329]=C_h_intern(&lf[329],43, C_text("chicken.compiler.core#standalone-executable"));
lf[330]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\020\001##core#undefined\376\377\016\376\377\016"));
lf[331]=C_h_intern(&lf[331],34, C_text("chicken.compiler.core#emit-profile"));
lf[332]=C_h_intern(&lf[332],47, C_text("chicken.compiler.support#profiling-prelude-exps"));
lf[333]=C_h_intern(&lf[333],14, C_text("##core#provide"));
lf[334]=C_h_intern(&lf[334],15, C_text("##core#callunit"));
lf[335]=C_h_intern(&lf[335],5, C_text("quote"));
lf[336]=C_h_intern(&lf[336],4, C_text("set!"));
lf[337]=C_h_intern(&lf[337],13, C_text("##core#module"));
lf[338]=C_h_intern(&lf[338],21, C_text("scheme#string->symbol"));
lf[339]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006source"));
lf[340]=C_h_intern(&lf[340],1, C_text("1"));
lf[341]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032User preprocessing pass..."));
lf[342]=C_h_intern(&lf[342],49, C_text("chicken.compiler.user-pass#user-preprocessor-pass"));
lf[343]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021User read pass..."));
lf[344]=C_h_intern(&lf[344],37, C_text("chicken.compiler.support#string->expr"));
lf[345]=C_h_intern(&lf[345],14, C_text("scheme#reverse"));
lf[346]=C_h_intern(&lf[346],49, C_text("chicken.compiler.support#close-checked-input-file"));
lf[347]=C_h_intern(&lf[347],41, C_text("chicken.compiler.support#read/source-info"));
lf[348]=C_h_intern(&lf[348],50, C_text("chicken.compiler.support#check-and-open-input-file"));
lf[349]=C_h_intern(&lf[349],41, C_text("chicken.compiler.user-pass#user-read-pass"));
lf[350]=C_h_intern(&lf[350],8, C_text("epilogue"));
lf[351]=C_h_intern(&lf[351],8, C_text("prologue"));
lf[352]=C_h_intern(&lf[352],8, C_text("postlude"));
lf[353]=C_h_intern(&lf[353],7, C_text("prelude"));
lf[354]=C_h_intern(&lf[354],18, C_text("scheme#make-vector"));
lf[355]=C_h_intern(&lf[355],47, C_text("chicken.compiler.core#line-number-database-size"));
lf[356]=C_h_intern(&lf[356],1, C_text("r"));
lf[357]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021target stack size"));
lf[358]=C_decode_literal(C_heaptop,C_text("\376B\000\000\020target heap size"));
lf[359]=C_decode_literal(C_heaptop,C_text("\376B\000\000\021debugging options"));
lf[360]=C_decode_literal(C_heaptop,C_text("\376B\000\000\007options"));
lf[361]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022compiling `~a\047 ..."));
lf[362]=C_h_intern(&lf[362],5, C_text("-help"));
lf[363]=C_h_intern(&lf[363],1, C_text("h"));
lf[364]=C_h_intern(&lf[364],2, C_text("-h"));
lf[365]=C_h_intern(&lf[365],49, C_text("chicken.compiler.support#load-identifier-database"));
lf[366]=C_decode_literal(C_heaptop,C_text("\376B\000\000\012modules.db"));
lf[367]=C_h_intern(&lf[367],18, C_text("accumulate-profile"));
lf[368]=C_h_intern(&lf[368],41, C_text("chicken.compiler.core#profiled-procedures"));
lf[369]=C_h_intern(&lf[369],3, C_text("all"));
lf[370]=C_decode_literal(C_heaptop,C_text("\376B\000\000\015accumulative "));
lf[371]=C_decode_literal(C_heaptop,C_text("\376B\000\000\032generating ~aprofiled code"));
lf[372]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[373]=C_h_intern(&lf[373],58, C_text("chicken.compiler.c-platform#default-profiling-declarations"));
lf[374]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001set!\376\003\000\000\002\376\001\000\000\031\001##sys#profile-append-mode\376\003\000\000\002\376\377\006\001\376\377\016\376\377\016"));
lf[375]=C_decode_literal(C_heaptop,C_text("\376B\000\000Eyou need to specify -profile-name if using accumulated profiling runs"));
lf[376]=C_decode_literal(C_heaptop,C_text("\376B\000\000\011calltrace"));
lf[377]=C_decode_literal(C_heaptop,C_text("\376B\000\000\022debugging info: ~A"));
lf[378]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004none"));
lf[379]=C_h_intern(&lf[379],21, C_text("no-usual-integrations"));
lf[380]=C_h_intern(&lf[380],1, C_text("m"));
lf[381]=C_h_intern(&lf[381],25, C_text("chicken.gc#set-gc-report!"));
lf[382]=C_h_intern(&lf[382],25, C_text("chicken.platform#feature\077"));
lf[383]=C_h_intern_kw(&lf[383],17, C_text("chicken-bootstrap"));
lf[384]=C_h_intern(&lf[384],14, C_text("compile-syntax"));
lf[385]=C_h_intern(&lf[385],27, C_text("##sys#enable-runtime-macros"));
lf[386]=C_h_intern(&lf[386],6, C_text("import"));
lf[387]=C_h_intern(&lf[387],17, C_text("require-extension"));
lf[388]=C_h_intern(&lf[388],4, C_text("uses"));
lf[389]=C_h_intern(&lf[389],14, C_text("##core#declare"));
lf[390]=C_h_intern(&lf[390],27, C_text("chicken.string#string-split"));
lf[391]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[392]=C_h_intern(&lf[392],50, C_text("chicken.compiler.user-pass#user-post-analysis-pass"));
lf[393]=C_h_intern(&lf[393],11, C_text("scheme#load"));
lf[394]=C_decode_literal(C_heaptop,C_text("\376B\000\000\031cannot load extension: ~a"));
lf[395]=C_decode_literal(C_heaptop,C_text("\376B\000\000\036Loading compiler extensions..."));
lf[396]=C_h_intern(&lf[396],6, C_text("extend"));
lf[397]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001,"));
lf[398]=C_h_intern(&lf[398],10, C_text("no-feature"));
lf[399]=C_decode_literal(C_heaptop,C_text("\376B\000\000\002, "));
lf[400]=C_h_intern(&lf[400],7, C_text("feature"));
lf[401]=C_h_intern(&lf[401],25, C_text("chicken.load#load-verbose"));
lf[402]=C_h_intern(&lf[402],38, C_text("no-procedure-checks-for-usual-bindings"));
lf[403]=C_h_intern(&lf[403],23, C_text("##compiler#always-bound"));
lf[404]=C_h_intern(&lf[404],36, C_text("##compiler#always-bound-to-procedure"));
lf[405]=C_h_intern(&lf[405],41, C_text("no-procedure-checks-for-toplevel-bindings"));
lf[406]=C_h_intern(&lf[406],48, C_text("chicken.compiler.core#no-global-procedure-checks"));
lf[407]=C_h_intern(&lf[407],19, C_text("no-procedure-checks"));
lf[408]=C_h_intern(&lf[408],41, C_text("chicken.compiler.core#no-procedure-checks"));
lf[409]=C_h_intern(&lf[409],15, C_text("no-bound-checks"));
lf[410]=C_h_intern(&lf[410],37, C_text("chicken.compiler.core#no-bound-checks"));
lf[411]=C_h_intern(&lf[411],14, C_text("no-argc-checks"));
lf[412]=C_h_intern(&lf[412],36, C_text("chicken.compiler.core#no-argc-checks"));
lf[413]=C_h_intern(&lf[413],20, C_text("keep-shadowed-macros"));
lf[414]=C_h_intern(&lf[414],46, C_text("chicken.compiler.core#undefine-shadowed-macros"));
lf[415]=C_decode_literal(C_heaptop,C_text("\376B\000\000(source- and output-filename are the same"));
lf[416]=C_h_intern(&lf[416],12, C_text("include-path"));
lf[417]=C_h_intern(&lf[417],11, C_text("r5rs-syntax"));
lf[418]=C_h_intern(&lf[418],26, C_text("chicken.base#symbol-escape"));
lf[419]=C_h_intern(&lf[419],33, C_text("chicken.base#parentheses-synonyms"));
lf[420]=C_h_intern(&lf[420],26, C_text("chicken.base#keyword-style"));
lf[421]=C_h_intern_kw(&lf[421],4, C_text("none"));
lf[422]=C_h_intern(&lf[422],27, C_text("chicken.base#case-sensitive"));
lf[423]=C_decode_literal(C_heaptop,C_text("\376B\000\000.Disabled the CHICKEN extensions to R5RS syntax"));
lf[424]=C_h_intern(&lf[424],16, C_text("no-symbol-escape"));
lf[425]=C_decode_literal(C_heaptop,C_text("\376B\000\000$Disabled support for escaped symbols"));
lf[426]=C_h_intern(&lf[426],23, C_text("no-parentheses-synonyms"));
lf[427]=C_decode_literal(C_heaptop,C_text("\376B\000\000)Disabled support for parentheses synonyms"));
lf[428]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006prefix"));
lf[429]=C_h_intern_kw(&lf[429],6, C_text("prefix"));
lf[430]=C_decode_literal(C_heaptop,C_text("\376B\000\000\004none"));
lf[431]=C_decode_literal(C_heaptop,C_text("\376B\000\000\006suffix"));
lf[432]=C_h_intern_kw(&lf[432],6, C_text("suffix"));
lf[433]=C_decode_literal(C_heaptop,C_text("\376B\000\000+invalid argument to `-keyword-style\047 option"));
lf[434]=C_h_intern(&lf[434],16, C_text("case-insensitive"));
lf[435]=C_decode_literal(C_heaptop,C_text("\376B\000\000,Identifiers and symbols are case insensitive"));
lf[436]=C_decode_literal(C_heaptop,C_text("\376B\000\0000invalid argument to `-unroll-limit\047 option: `~A\047"));
lf[437]=C_decode_literal(C_heaptop,C_text("\376B\000\0000invalid argument to `-inline-limit\047 option: `~A\047"));
lf[438]=C_h_intern(&lf[438],39, C_text("chicken.compiler.core#local-definitions"));
lf[439]=C_h_intern(&lf[439],6, C_text("inline"));
lf[440]=C_h_intern(&lf[440],30, C_text("emit-external-prototypes-first"));
lf[441]=C_h_intern(&lf[441],43, C_text("chicken.compiler.core#external-protos-first"));
lf[442]=C_h_intern(&lf[442],5, C_text("block"));
lf[443]=C_h_intern(&lf[443],17, C_text("fixnum-arithmetic"));
lf[444]=C_h_intern(&lf[444],36, C_text("chicken.compiler.support#number-type"));
lf[445]=C_h_intern(&lf[445],6, C_text("fixnum"));
lf[446]=C_h_intern(&lf[446],18, C_text("disable-interrupts"));
lf[447]=C_h_intern(&lf[447],27, C_text("regenerate-import-libraries"));
lf[448]=C_h_intern(&lf[448],57, C_text("chicken.compiler.core#preserve-unchanged-import-libraries"));
lf[449]=C_h_intern(&lf[449],10, C_text("setup-mode"));
lf[450]=C_h_intern(&lf[450],16, C_text("##sys#setup-mode"));
lf[451]=C_h_intern(&lf[451],6, C_text("unsafe"));
lf[452]=C_h_intern(&lf[452],22, C_text("optimize-leaf-routines"));
lf[453]=C_h_intern(&lf[453],11, C_text("no-warnings"));
lf[454]=C_decode_literal(C_heaptop,C_text("\376B\000\000\025Warnings are disabled"));
lf[455]=C_h_intern(&lf[455],12, C_text("strict-types"));
lf[456]=C_h_intern(&lf[456],21, C_text("##sys#notices-enabled"));
lf[457]=C_h_intern(&lf[457],13, C_text("inline-global"));
lf[458]=C_h_intern(&lf[458],5, C_text("local"));
lf[459]=C_h_intern(&lf[459],18, C_text("no-compiler-syntax"));
lf[460]=C_h_intern(&lf[460],45, C_text("chicken.compiler.core#compiler-syntax-enabled"));
lf[461]=C_h_intern(&lf[461],14, C_text("no-lambda-info"));
lf[462]=C_h_intern(&lf[462],39, C_text("chicken.compiler.core#emit-closure-info"));
lf[463]=C_h_intern(&lf[463],3, C_text("raw"));
lf[464]=C_h_intern(&lf[464],1, C_text("b"));
lf[465]=C_h_intern(&lf[465],17, C_text("##sys#start-timer"));
lf[466]=C_h_intern(&lf[466],25, C_text("emit-all-import-libraries"));
lf[467]=C_h_intern(&lf[467],42, C_text("chicken.compiler.core#all-import-libraries"));
lf[468]=C_h_intern(&lf[468],19, C_text("##sys#string-append"));
lf[469]=C_decode_literal(C_heaptop,C_text("\376B\000\000\013.import.scm"));
lf[470]=C_h_intern(&lf[470],19, C_text("emit-import-library"));
lf[471]=C_h_intern(&lf[471],44, C_text("chicken.compiler.support#print-debug-options"));
lf[472]=C_h_intern(&lf[472],18, C_text("##sys#string->list"));
lf[473]=C_h_intern(&lf[473],5, C_text("debug"));
lf[474]=C_h_intern(&lf[474],20, C_text("##sys#dload-disabled"));
lf[475]=C_h_intern(&lf[475],32, C_text("chicken.platform#repository-path"));
lf[476]=C_h_intern(&lf[476],54, C_text("chicken.compiler.optimizer#default-optimization-passes"));
lf[477]=C_h_intern(&lf[477],16, C_text("##sys#split-path"));
lf[478]=C_decode_literal(C_heaptop,C_text("\376B\000\000\000"));
lf[479]=C_h_intern(&lf[479],48, C_text("chicken.process-context#get-environment-variable"));
lf[480]=C_decode_literal(C_heaptop,C_text("\376B\000\000\024CHICKEN_INCLUDE_PATH"));
lf[481]=C_h_intern(&lf[481],9, C_text("to-stdout"));
lf[482]=C_decode_literal(C_heaptop,C_text("\376B\000\000\001c"));
lf[483]=C_h_intern(&lf[483],30, C_text("chicken.pathname#pathname-file"));
lf[484]=C_decode_literal(C_heaptop,C_text("\376B\000\000\003out"));
lf[485]=C_h_intern(&lf[485],32, C_text("chicken.internal#default-imports"));
lf[486]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001uses\376\003\000\000\002\376\001\000\000\017\001debugger-client\376\377\016\376\377\016"));
lf[487]=C_h_intern(&lf[487],48, C_text("chicken.compiler.c-platform#default-declarations"));
lf[488]=C_decode_literal(C_heaptop,C_text("\376\003\000\000\002\376\003\000\000\002\376\001\000\000\004\001uses\376\003\000\000\002\376\001\000\000\014\001eval-modules\376\377\016\376\377\016"));
lf[489]=C_h_intern(&lf[489],39, C_text("chicken.compiler.core#static-extensions"));
lf[490]=C_h_intern(&lf[490],49, C_text("chicken.compiler.core#compile-module-registration"));
lf[491]=C_h_intern(&lf[491],3, C_text("yes"));
lf[492]=C_h_intern(&lf[492],41, C_text("chicken.compiler.c-platform#default-units"));
lf[493]=C_h_intern(&lf[493],6, C_text("static"));
lf[494]=C_h_intern(&lf[494],22, C_text("chicken-compile-static"));
lf[495]=C_h_intern(&lf[495],22, C_text("no-module-registration"));
lf[496]=C_h_intern(&lf[496],2, C_text("no"));
lf[497]=C_h_intern(&lf[497],19, C_text("module-registration"));
lf[498]=C_h_intern(&lf[498],41, C_text("chicken.compiler.core#initialize-compiler"));
C_register_lf2(lf,499,create_ptable());{}
t2=(*a=C_CLOSURE_TYPE|2,a[1]=(C_word)f_2725,a[2]=t1,tmp=(C_word)a,a+=3,tmp);{
C_word *av2=av;
av2[0]=C_SCHEME_UNDEFINED;
av2[1]=t2;
C_library_toplevel(2,av2);}}

#ifdef C_ENABLE_PTABLES
static C_PTABLE_ENTRY ptable[567] = {
{C_text("f8805:batch_2ddriver_2escm"),(void*)f8805},
{C_text("f9308:batch_2ddriver_2escm"),(void*)f9308},
{C_text("f9314:batch_2ddriver_2escm"),(void*)f9314},
{C_text("f9320:batch_2ddriver_2escm"),(void*)f9320},
{C_text("f9326:batch_2ddriver_2escm"),(void*)f9326},
{C_text("f9334:batch_2ddriver_2escm"),(void*)f9334},
{C_text("f9340:batch_2ddriver_2escm"),(void*)f9340},
{C_text("f9352:batch_2ddriver_2escm"),(void*)f9352},
{C_text("f9360:batch_2ddriver_2escm"),(void*)f9360},
{C_text("f9372:batch_2ddriver_2escm"),(void*)f9372},
{C_text("f9396:batch_2ddriver_2escm"),(void*)f9396},
{C_text("f9402:batch_2ddriver_2escm"),(void*)f9402},
{C_text("f9416:batch_2ddriver_2escm"),(void*)f9416},
{C_text("f9422:batch_2ddriver_2escm"),(void*)f9422},
{C_text("f9428:batch_2ddriver_2escm"),(void*)f9428},
{C_text("f9434:batch_2ddriver_2escm"),(void*)f9434},
{C_text("f9440:batch_2ddriver_2escm"),(void*)f9440},
{C_text("f9454:batch_2ddriver_2escm"),(void*)f9454},
{C_text("f9470:batch_2ddriver_2escm"),(void*)f9470},
{C_text("f9476:batch_2ddriver_2escm"),(void*)f9476},
{C_text("f9482:batch_2ddriver_2escm"),(void*)f9482},
{C_text("f9488:batch_2ddriver_2escm"),(void*)f9488},
{C_text("f9494:batch_2ddriver_2escm"),(void*)f9494},
{C_text("f_2725:batch_2ddriver_2escm"),(void*)f_2725},
{C_text("f_2728:batch_2ddriver_2escm"),(void*)f_2728},
{C_text("f_2731:batch_2ddriver_2escm"),(void*)f_2731},
{C_text("f_2734:batch_2ddriver_2escm"),(void*)f_2734},
{C_text("f_2737:batch_2ddriver_2escm"),(void*)f_2737},
{C_text("f_2740:batch_2ddriver_2escm"),(void*)f_2740},
{C_text("f_2743:batch_2ddriver_2escm"),(void*)f_2743},
{C_text("f_2746:batch_2ddriver_2escm"),(void*)f_2746},
{C_text("f_2749:batch_2ddriver_2escm"),(void*)f_2749},
{C_text("f_2752:batch_2ddriver_2escm"),(void*)f_2752},
{C_text("f_2755:batch_2ddriver_2escm"),(void*)f_2755},
{C_text("f_2758:batch_2ddriver_2escm"),(void*)f_2758},
{C_text("f_2761:batch_2ddriver_2escm"),(void*)f_2761},
{C_text("f_2764:batch_2ddriver_2escm"),(void*)f_2764},
{C_text("f_2767:batch_2ddriver_2escm"),(void*)f_2767},
{C_text("f_2770:batch_2ddriver_2escm"),(void*)f_2770},
{C_text("f_2971:batch_2ddriver_2escm"),(void*)f_2971},
{C_text("f_2986:batch_2ddriver_2escm"),(void*)f_2986},
{C_text("f_2994:batch_2ddriver_2escm"),(void*)f_2994},
{C_text("f_3002:batch_2ddriver_2escm"),(void*)f_3002},
{C_text("f_3013:batch_2ddriver_2escm"),(void*)f_3013},
{C_text("f_3026:batch_2ddriver_2escm"),(void*)f_3026},
{C_text("f_3040:batch_2ddriver_2escm"),(void*)f_3040},
{C_text("f_3044:batch_2ddriver_2escm"),(void*)f_3044},
{C_text("f_3056:batch_2ddriver_2escm"),(void*)f_3056},
{C_text("f_3058:batch_2ddriver_2escm"),(void*)f_3058},
{C_text("f_3105:batch_2ddriver_2escm"),(void*)f_3105},
{C_text("f_3107:batch_2ddriver_2escm"),(void*)f_3107},
{C_text("f_3147:batch_2ddriver_2escm"),(void*)f_3147},
{C_text("f_3181:batch_2ddriver_2escm"),(void*)f_3181},
{C_text("f_3233:batch_2ddriver_2escm"),(void*)f_3233},
{C_text("f_3239:batch_2ddriver_2escm"),(void*)f_3239},
{C_text("f_3257:batch_2ddriver_2escm"),(void*)f_3257},
{C_text("f_3267:batch_2ddriver_2escm"),(void*)f_3267},
{C_text("f_3294:batch_2ddriver_2escm"),(void*)f_3294},
{C_text("f_3381:batch_2ddriver_2escm"),(void*)f_3381},
{C_text("f_3390:batch_2ddriver_2escm"),(void*)f_3390},
{C_text("f_3398:batch_2ddriver_2escm"),(void*)f_3398},
{C_text("f_3405:batch_2ddriver_2escm"),(void*)f_3405},
{C_text("f_3419:batch_2ddriver_2escm"),(void*)f_3419},
{C_text("f_3480:batch_2ddriver_2escm"),(void*)f_3480},
{C_text("f_3488:batch_2ddriver_2escm"),(void*)f_3488},
{C_text("f_3771:batch_2ddriver_2escm"),(void*)f_3771},
{C_text("f_3777:batch_2ddriver_2escm"),(void*)f_3777},
{C_text("f_4056:batch_2ddriver_2escm"),(void*)f_4056},
{C_text("f_4062:batch_2ddriver_2escm"),(void*)f_4062},
{C_text("f_4069:batch_2ddriver_2escm"),(void*)f_4069},
{C_text("f_4075:batch_2ddriver_2escm"),(void*)f_4075},
{C_text("f_4078:batch_2ddriver_2escm"),(void*)f_4078},
{C_text("f_4081:batch_2ddriver_2escm"),(void*)f_4081},
{C_text("f_4084:batch_2ddriver_2escm"),(void*)f_4084},
{C_text("f_4087:batch_2ddriver_2escm"),(void*)f_4087},
{C_text("f_4093:batch_2ddriver_2escm"),(void*)f_4093},
{C_text("f_4096:batch_2ddriver_2escm"),(void*)f_4096},
{C_text("f_4099:batch_2ddriver_2escm"),(void*)f_4099},
{C_text("f_4105:batch_2ddriver_2escm"),(void*)f_4105},
{C_text("f_4108:batch_2ddriver_2escm"),(void*)f_4108},
{C_text("f_4111:batch_2ddriver_2escm"),(void*)f_4111},
{C_text("f_4117:batch_2ddriver_2escm"),(void*)f_4117},
{C_text("f_4120:batch_2ddriver_2escm"),(void*)f_4120},
{C_text("f_4123:batch_2ddriver_2escm"),(void*)f_4123},
{C_text("f_4129:batch_2ddriver_2escm"),(void*)f_4129},
{C_text("f_4132:batch_2ddriver_2escm"),(void*)f_4132},
{C_text("f_4135:batch_2ddriver_2escm"),(void*)f_4135},
{C_text("f_4141:batch_2ddriver_2escm"),(void*)f_4141},
{C_text("f_4144:batch_2ddriver_2escm"),(void*)f_4144},
{C_text("f_4149:batch_2ddriver_2escm"),(void*)f_4149},
{C_text("f_4153:batch_2ddriver_2escm"),(void*)f_4153},
{C_text("f_4165:batch_2ddriver_2escm"),(void*)f_4165},
{C_text("f_4176:batch_2ddriver_2escm"),(void*)f_4176},
{C_text("f_4189:batch_2ddriver_2escm"),(void*)f_4189},
{C_text("f_4199:batch_2ddriver_2escm"),(void*)f_4199},
{C_text("f_4212:batch_2ddriver_2escm"),(void*)f_4212},
{C_text("f_4222:batch_2ddriver_2escm"),(void*)f_4222},
{C_text("f_4235:batch_2ddriver_2escm"),(void*)f_4235},
{C_text("f_4245:batch_2ddriver_2escm"),(void*)f_4245},
{C_text("f_4258:batch_2ddriver_2escm"),(void*)f_4258},
{C_text("f_4262:batch_2ddriver_2escm"),(void*)f_4262},
{C_text("f_4267:batch_2ddriver_2escm"),(void*)f_4267},
{C_text("f_4277:batch_2ddriver_2escm"),(void*)f_4277},
{C_text("f_4280:batch_2ddriver_2escm"),(void*)f_4280},
{C_text("f_4283:batch_2ddriver_2escm"),(void*)f_4283},
{C_text("f_4286:batch_2ddriver_2escm"),(void*)f_4286},
{C_text("f_4289:batch_2ddriver_2escm"),(void*)f_4289},
{C_text("f_4292:batch_2ddriver_2escm"),(void*)f_4292},
{C_text("f_4295:batch_2ddriver_2escm"),(void*)f_4295},
{C_text("f_4309:batch_2ddriver_2escm"),(void*)f_4309},
{C_text("f_4320:batch_2ddriver_2escm"),(void*)f_4320},
{C_text("f_4324:batch_2ddriver_2escm"),(void*)f_4324},
{C_text("f_4332:batch_2ddriver_2escm"),(void*)f_4332},
{C_text("f_4342:batch_2ddriver_2escm"),(void*)f_4342},
{C_text("f_4362:batch_2ddriver_2escm"),(void*)f_4362},
{C_text("f_4373:batch_2ddriver_2escm"),(void*)f_4373},
{C_text("f_4377:batch_2ddriver_2escm"),(void*)f_4377},
{C_text("f_4389:batch_2ddriver_2escm"),(void*)f_4389},
{C_text("f_4400:batch_2ddriver_2escm"),(void*)f_4400},
{C_text("f_4404:batch_2ddriver_2escm"),(void*)f_4404},
{C_text("f_4427:batch_2ddriver_2escm"),(void*)f_4427},
{C_text("f_4443:batch_2ddriver_2escm"),(void*)f_4443},
{C_text("f_4459:batch_2ddriver_2escm"),(void*)f_4459},
{C_text("f_4468:batch_2ddriver_2escm"),(void*)f_4468},
{C_text("f_4481:batch_2ddriver_2escm"),(void*)f_4481},
{C_text("f_4492:batch_2ddriver_2escm"),(void*)f_4492},
{C_text("f_4498:batch_2ddriver_2escm"),(void*)f_4498},
{C_text("f_4571:batch_2ddriver_2escm"),(void*)f_4571},
{C_text("f_4577:batch_2ddriver_2escm"),(void*)f_4577},
{C_text("f_4580:batch_2ddriver_2escm"),(void*)f_4580},
{C_text("f_4583:batch_2ddriver_2escm"),(void*)f_4583},
{C_text("f_4885:batch_2ddriver_2escm"),(void*)f_4885},
{C_text("f_4887:batch_2ddriver_2escm"),(void*)f_4887},
{C_text("f_4890:batch_2ddriver_2escm"),(void*)f_4890},
{C_text("f_4921:batch_2ddriver_2escm"),(void*)f_4921},
{C_text("f_4930:batch_2ddriver_2escm"),(void*)f_4930},
{C_text("f_4933:batch_2ddriver_2escm"),(void*)f_4933},
{C_text("f_4936:batch_2ddriver_2escm"),(void*)f_4936},
{C_text("f_4950:batch_2ddriver_2escm"),(void*)f_4950},
{C_text("f_4956:batch_2ddriver_2escm"),(void*)f_4956},
{C_text("f_4962:batch_2ddriver_2escm"),(void*)f_4962},
{C_text("f_4965:batch_2ddriver_2escm"),(void*)f_4965},
{C_text("f_4970:batch_2ddriver_2escm"),(void*)f_4970},
{C_text("f_4995:batch_2ddriver_2escm"),(void*)f_4995},
{C_text("f_5013:batch_2ddriver_2escm"),(void*)f_5013},
{C_text("f_5017:batch_2ddriver_2escm"),(void*)f_5017},
{C_text("f_5029:batch_2ddriver_2escm"),(void*)f_5029},
{C_text("f_5032:batch_2ddriver_2escm"),(void*)f_5032},
{C_text("f_5035:batch_2ddriver_2escm"),(void*)f_5035},
{C_text("f_5038:batch_2ddriver_2escm"),(void*)f_5038},
{C_text("f_5040:batch_2ddriver_2escm"),(void*)f_5040},
{C_text("f_5047:batch_2ddriver_2escm"),(void*)f_5047},
{C_text("f_5060:batch_2ddriver_2escm"),(void*)f_5060},
{C_text("f_5062:batch_2ddriver_2escm"),(void*)f_5062},
{C_text("f_5069:batch_2ddriver_2escm"),(void*)f_5069},
{C_text("f_5075:batch_2ddriver_2escm"),(void*)f_5075},
{C_text("f_5078:batch_2ddriver_2escm"),(void*)f_5078},
{C_text("f_5081:batch_2ddriver_2escm"),(void*)f_5081},
{C_text("f_5084:batch_2ddriver_2escm"),(void*)f_5084},
{C_text("f_5089:batch_2ddriver_2escm"),(void*)f_5089},
{C_text("f_5096:batch_2ddriver_2escm"),(void*)f_5096},
{C_text("f_5101:batch_2ddriver_2escm"),(void*)f_5101},
{C_text("f_5112:batch_2ddriver_2escm"),(void*)f_5112},
{C_text("f_5122:batch_2ddriver_2escm"),(void*)f_5122},
{C_text("f_5135:batch_2ddriver_2escm"),(void*)f_5135},
{C_text("f_5144:batch_2ddriver_2escm"),(void*)f_5144},
{C_text("f_5175:batch_2ddriver_2escm"),(void*)f_5175},
{C_text("f_5179:batch_2ddriver_2escm"),(void*)f_5179},
{C_text("f_5195:batch_2ddriver_2escm"),(void*)f_5195},
{C_text("f_5199:batch_2ddriver_2escm"),(void*)f_5199},
{C_text("f_5220:batch_2ddriver_2escm"),(void*)f_5220},
{C_text("f_5226:batch_2ddriver_2escm"),(void*)f_5226},
{C_text("f_5234:batch_2ddriver_2escm"),(void*)f_5234},
{C_text("f_5242:batch_2ddriver_2escm"),(void*)f_5242},
{C_text("f_5246:batch_2ddriver_2escm"),(void*)f_5246},
{C_text("f_5255:batch_2ddriver_2escm"),(void*)f_5255},
{C_text("f_5263:batch_2ddriver_2escm"),(void*)f_5263},
{C_text("f_5265:batch_2ddriver_2escm"),(void*)f_5265},
{C_text("f_5275:batch_2ddriver_2escm"),(void*)f_5275},
{C_text("f_5278:batch_2ddriver_2escm"),(void*)f_5278},
{C_text("f_5281:batch_2ddriver_2escm"),(void*)f_5281},
{C_text("f_5284:batch_2ddriver_2escm"),(void*)f_5284},
{C_text("f_5291:batch_2ddriver_2escm"),(void*)f_5291},
{C_text("f_5295:batch_2ddriver_2escm"),(void*)f_5295},
{C_text("f_5303:batch_2ddriver_2escm"),(void*)f_5303},
{C_text("f_5305:batch_2ddriver_2escm"),(void*)f_5305},
{C_text("f_5307:batch_2ddriver_2escm"),(void*)f_5307},
{C_text("f_5311:batch_2ddriver_2escm"),(void*)f_5311},
{C_text("f_5314:batch_2ddriver_2escm"),(void*)f_5314},
{C_text("f_5319:batch_2ddriver_2escm"),(void*)f_5319},
{C_text("f_5325:batch_2ddriver_2escm"),(void*)f_5325},
{C_text("f_5330:batch_2ddriver_2escm"),(void*)f_5330},
{C_text("f_5335:batch_2ddriver_2escm"),(void*)f_5335},
{C_text("f_5371:batch_2ddriver_2escm"),(void*)f_5371},
{C_text("f_5374:batch_2ddriver_2escm"),(void*)f_5374},
{C_text("f_5377:batch_2ddriver_2escm"),(void*)f_5377},
{C_text("f_5384:batch_2ddriver_2escm"),(void*)f_5384},
{C_text("f_5387:batch_2ddriver_2escm"),(void*)f_5387},
{C_text("f_5404:batch_2ddriver_2escm"),(void*)f_5404},
{C_text("f_5408:batch_2ddriver_2escm"),(void*)f_5408},
{C_text("f_5413:batch_2ddriver_2escm"),(void*)f_5413},
{C_text("f_5419:batch_2ddriver_2escm"),(void*)f_5419},
{C_text("f_5422:batch_2ddriver_2escm"),(void*)f_5422},
{C_text("f_5425:batch_2ddriver_2escm"),(void*)f_5425},
{C_text("f_5428:batch_2ddriver_2escm"),(void*)f_5428},
{C_text("f_5431:batch_2ddriver_2escm"),(void*)f_5431},
{C_text("f_5434:batch_2ddriver_2escm"),(void*)f_5434},
{C_text("f_5437:batch_2ddriver_2escm"),(void*)f_5437},
{C_text("f_5440:batch_2ddriver_2escm"),(void*)f_5440},
{C_text("f_5443:batch_2ddriver_2escm"),(void*)f_5443},
{C_text("f_5446:batch_2ddriver_2escm"),(void*)f_5446},
{C_text("f_5449:batch_2ddriver_2escm"),(void*)f_5449},
{C_text("f_5452:batch_2ddriver_2escm"),(void*)f_5452},
{C_text("f_5455:batch_2ddriver_2escm"),(void*)f_5455},
{C_text("f_5458:batch_2ddriver_2escm"),(void*)f_5458},
{C_text("f_5461:batch_2ddriver_2escm"),(void*)f_5461},
{C_text("f_5464:batch_2ddriver_2escm"),(void*)f_5464},
{C_text("f_5467:batch_2ddriver_2escm"),(void*)f_5467},
{C_text("f_5470:batch_2ddriver_2escm"),(void*)f_5470},
{C_text("f_5473:batch_2ddriver_2escm"),(void*)f_5473},
{C_text("f_5476:batch_2ddriver_2escm"),(void*)f_5476},
{C_text("f_5479:batch_2ddriver_2escm"),(void*)f_5479},
{C_text("f_5482:batch_2ddriver_2escm"),(void*)f_5482},
{C_text("f_5487:batch_2ddriver_2escm"),(void*)f_5487},
{C_text("f_5492:batch_2ddriver_2escm"),(void*)f_5492},
{C_text("f_5497:batch_2ddriver_2escm"),(void*)f_5497},
{C_text("f_5502:batch_2ddriver_2escm"),(void*)f_5502},
{C_text("f_5507:batch_2ddriver_2escm"),(void*)f_5507},
{C_text("f_5510:batch_2ddriver_2escm"),(void*)f_5510},
{C_text("f_5513:batch_2ddriver_2escm"),(void*)f_5513},
{C_text("f_5516:batch_2ddriver_2escm"),(void*)f_5516},
{C_text("f_5519:batch_2ddriver_2escm"),(void*)f_5519},
{C_text("f_5522:batch_2ddriver_2escm"),(void*)f_5522},
{C_text("f_5528:batch_2ddriver_2escm"),(void*)f_5528},
{C_text("f_5531:batch_2ddriver_2escm"),(void*)f_5531},
{C_text("f_5534:batch_2ddriver_2escm"),(void*)f_5534},
{C_text("f_5537:batch_2ddriver_2escm"),(void*)f_5537},
{C_text("f_5540:batch_2ddriver_2escm"),(void*)f_5540},
{C_text("f_5543:batch_2ddriver_2escm"),(void*)f_5543},
{C_text("f_5546:batch_2ddriver_2escm"),(void*)f_5546},
{C_text("f_5549:batch_2ddriver_2escm"),(void*)f_5549},
{C_text("f_5552:batch_2ddriver_2escm"),(void*)f_5552},
{C_text("f_5555:batch_2ddriver_2escm"),(void*)f_5555},
{C_text("f_5561:batch_2ddriver_2escm"),(void*)f_5561},
{C_text("f_5564:batch_2ddriver_2escm"),(void*)f_5564},
{C_text("f_5570:batch_2ddriver_2escm"),(void*)f_5570},
{C_text("f_5577:batch_2ddriver_2escm"),(void*)f_5577},
{C_text("f_5580:batch_2ddriver_2escm"),(void*)f_5580},
{C_text("f_5585:batch_2ddriver_2escm"),(void*)f_5585},
{C_text("f_5588:batch_2ddriver_2escm"),(void*)f_5588},
{C_text("f_5603:batch_2ddriver_2escm"),(void*)f_5603},
{C_text("f_5607:batch_2ddriver_2escm"),(void*)f_5607},
{C_text("f_5615:batch_2ddriver_2escm"),(void*)f_5615},
{C_text("f_5618:batch_2ddriver_2escm"),(void*)f_5618},
{C_text("f_5621:batch_2ddriver_2escm"),(void*)f_5621},
{C_text("f_5625:batch_2ddriver_2escm"),(void*)f_5625},
{C_text("f_5628:batch_2ddriver_2escm"),(void*)f_5628},
{C_text("f_5632:batch_2ddriver_2escm"),(void*)f_5632},
{C_text("f_5636:batch_2ddriver_2escm"),(void*)f_5636},
{C_text("f_5647:batch_2ddriver_2escm"),(void*)f_5647},
{C_text("f_5650:batch_2ddriver_2escm"),(void*)f_5650},
{C_text("f_5653:batch_2ddriver_2escm"),(void*)f_5653},
{C_text("f_5656:batch_2ddriver_2escm"),(void*)f_5656},
{C_text("f_5659:batch_2ddriver_2escm"),(void*)f_5659},
{C_text("f_5662:batch_2ddriver_2escm"),(void*)f_5662},
{C_text("f_5670:batch_2ddriver_2escm"),(void*)f_5670},
{C_text("f_5681:batch_2ddriver_2escm"),(void*)f_5681},
{C_text("f_5692:batch_2ddriver_2escm"),(void*)f_5692},
{C_text("f_5699:batch_2ddriver_2escm"),(void*)f_5699},
{C_text("f_5708:batch_2ddriver_2escm"),(void*)f_5708},
{C_text("f_5711:batch_2ddriver_2escm"),(void*)f_5711},
{C_text("f_5714:batch_2ddriver_2escm"),(void*)f_5714},
{C_text("f_5720:batch_2ddriver_2escm"),(void*)f_5720},
{C_text("f_5723:batch_2ddriver_2escm"),(void*)f_5723},
{C_text("f_5726:batch_2ddriver_2escm"),(void*)f_5726},
{C_text("f_5729:batch_2ddriver_2escm"),(void*)f_5729},
{C_text("f_5732:batch_2ddriver_2escm"),(void*)f_5732},
{C_text("f_5736:batch_2ddriver_2escm"),(void*)f_5736},
{C_text("f_5740:batch_2ddriver_2escm"),(void*)f_5740},
{C_text("f_5743:batch_2ddriver_2escm"),(void*)f_5743},
{C_text("f_5746:batch_2ddriver_2escm"),(void*)f_5746},
{C_text("f_5749:batch_2ddriver_2escm"),(void*)f_5749},
{C_text("f_5752:batch_2ddriver_2escm"),(void*)f_5752},
{C_text("f_5755:batch_2ddriver_2escm"),(void*)f_5755},
{C_text("f_5758:batch_2ddriver_2escm"),(void*)f_5758},
{C_text("f_5761:batch_2ddriver_2escm"),(void*)f_5761},
{C_text("f_5764:batch_2ddriver_2escm"),(void*)f_5764},
{C_text("f_5767:batch_2ddriver_2escm"),(void*)f_5767},
{C_text("f_5771:batch_2ddriver_2escm"),(void*)f_5771},
{C_text("f_5777:batch_2ddriver_2escm"),(void*)f_5777},
{C_text("f_5782:batch_2ddriver_2escm"),(void*)f_5782},
{C_text("f_5788:batch_2ddriver_2escm"),(void*)f_5788},
{C_text("f_5794:batch_2ddriver_2escm"),(void*)f_5794},
{C_text("f_5797:batch_2ddriver_2escm"),(void*)f_5797},
{C_text("f_5803:batch_2ddriver_2escm"),(void*)f_5803},
{C_text("f_5806:batch_2ddriver_2escm"),(void*)f_5806},
{C_text("f_5809:batch_2ddriver_2escm"),(void*)f_5809},
{C_text("f_5812:batch_2ddriver_2escm"),(void*)f_5812},
{C_text("f_5815:batch_2ddriver_2escm"),(void*)f_5815},
{C_text("f_5818:batch_2ddriver_2escm"),(void*)f_5818},
{C_text("f_5821:batch_2ddriver_2escm"),(void*)f_5821},
{C_text("f_5824:batch_2ddriver_2escm"),(void*)f_5824},
{C_text("f_5829:batch_2ddriver_2escm"),(void*)f_5829},
{C_text("f_5832:batch_2ddriver_2escm"),(void*)f_5832},
{C_text("f_5835:batch_2ddriver_2escm"),(void*)f_5835},
{C_text("f_5838:batch_2ddriver_2escm"),(void*)f_5838},
{C_text("f_5841:batch_2ddriver_2escm"),(void*)f_5841},
{C_text("f_5844:batch_2ddriver_2escm"),(void*)f_5844},
{C_text("f_5847:batch_2ddriver_2escm"),(void*)f_5847},
{C_text("f_5850:batch_2ddriver_2escm"),(void*)f_5850},
{C_text("f_5853:batch_2ddriver_2escm"),(void*)f_5853},
{C_text("f_5856:batch_2ddriver_2escm"),(void*)f_5856},
{C_text("f_5859:batch_2ddriver_2escm"),(void*)f_5859},
{C_text("f_5862:batch_2ddriver_2escm"),(void*)f_5862},
{C_text("f_5865:batch_2ddriver_2escm"),(void*)f_5865},
{C_text("f_5868:batch_2ddriver_2escm"),(void*)f_5868},
{C_text("f_5871:batch_2ddriver_2escm"),(void*)f_5871},
{C_text("f_5877:batch_2ddriver_2escm"),(void*)f_5877},
{C_text("f_5880:batch_2ddriver_2escm"),(void*)f_5880},
{C_text("f_5883:batch_2ddriver_2escm"),(void*)f_5883},
{C_text("f_5886:batch_2ddriver_2escm"),(void*)f_5886},
{C_text("f_5889:batch_2ddriver_2escm"),(void*)f_5889},
{C_text("f_5894:batch_2ddriver_2escm"),(void*)f_5894},
{C_text("f_5898:batch_2ddriver_2escm"),(void*)f_5898},
{C_text("f_5901:batch_2ddriver_2escm"),(void*)f_5901},
{C_text("f_5904:batch_2ddriver_2escm"),(void*)f_5904},
{C_text("f_5908:batch_2ddriver_2escm"),(void*)f_5908},
{C_text("f_5911:batch_2ddriver_2escm"),(void*)f_5911},
{C_text("f_5914:batch_2ddriver_2escm"),(void*)f_5914},
{C_text("f_5920:batch_2ddriver_2escm"),(void*)f_5920},
{C_text("f_5923:batch_2ddriver_2escm"),(void*)f_5923},
{C_text("f_5928:batch_2ddriver_2escm"),(void*)f_5928},
{C_text("f_5940:batch_2ddriver_2escm"),(void*)f_5940},
{C_text("f_5944:batch_2ddriver_2escm"),(void*)f_5944},
{C_text("f_5947:batch_2ddriver_2escm"),(void*)f_5947},
{C_text("f_5964:batch_2ddriver_2escm"),(void*)f_5964},
{C_text("f_5978:batch_2ddriver_2escm"),(void*)f_5978},
{C_text("f_5990:batch_2ddriver_2escm"),(void*)f_5990},
{C_text("f_5993:batch_2ddriver_2escm"),(void*)f_5993},
{C_text("f_5996:batch_2ddriver_2escm"),(void*)f_5996},
{C_text("f_5999:batch_2ddriver_2escm"),(void*)f_5999},
{C_text("f_6002:batch_2ddriver_2escm"),(void*)f_6002},
{C_text("f_6005:batch_2ddriver_2escm"),(void*)f_6005},
{C_text("f_6021:batch_2ddriver_2escm"),(void*)f_6021},
{C_text("f_6024:batch_2ddriver_2escm"),(void*)f_6024},
{C_text("f_6027:batch_2ddriver_2escm"),(void*)f_6027},
{C_text("f_6030:batch_2ddriver_2escm"),(void*)f_6030},
{C_text("f_6034:batch_2ddriver_2escm"),(void*)f_6034},
{C_text("f_6037:batch_2ddriver_2escm"),(void*)f_6037},
{C_text("f_6040:batch_2ddriver_2escm"),(void*)f_6040},
{C_text("f_6043:batch_2ddriver_2escm"),(void*)f_6043},
{C_text("f_6046:batch_2ddriver_2escm"),(void*)f_6046},
{C_text("f_6049:batch_2ddriver_2escm"),(void*)f_6049},
{C_text("f_6052:batch_2ddriver_2escm"),(void*)f_6052},
{C_text("f_6057:batch_2ddriver_2escm"),(void*)f_6057},
{C_text("f_6063:batch_2ddriver_2escm"),(void*)f_6063},
{C_text("f_6067:batch_2ddriver_2escm"),(void*)f_6067},
{C_text("f_6070:batch_2ddriver_2escm"),(void*)f_6070},
{C_text("f_6073:batch_2ddriver_2escm"),(void*)f_6073},
{C_text("f_6076:batch_2ddriver_2escm"),(void*)f_6076},
{C_text("f_6079:batch_2ddriver_2escm"),(void*)f_6079},
{C_text("f_6082:batch_2ddriver_2escm"),(void*)f_6082},
{C_text("f_6085:batch_2ddriver_2escm"),(void*)f_6085},
{C_text("f_6088:batch_2ddriver_2escm"),(void*)f_6088},
{C_text("f_6091:batch_2ddriver_2escm"),(void*)f_6091},
{C_text("f_6094:batch_2ddriver_2escm"),(void*)f_6094},
{C_text("f_6107:batch_2ddriver_2escm"),(void*)f_6107},
{C_text("f_6116:batch_2ddriver_2escm"),(void*)f_6116},
{C_text("f_6121:batch_2ddriver_2escm"),(void*)f_6121},
{C_text("f_6145:batch_2ddriver_2escm"),(void*)f_6145},
{C_text("f_6151:batch_2ddriver_2escm"),(void*)f_6151},
{C_text("f_6164:batch_2ddriver_2escm"),(void*)f_6164},
{C_text("f_6166:batch_2ddriver_2escm"),(void*)f_6166},
{C_text("f_6191:batch_2ddriver_2escm"),(void*)f_6191},
{C_text("f_6201:batch_2ddriver_2escm"),(void*)f_6201},
{C_text("f_6204:batch_2ddriver_2escm"),(void*)f_6204},
{C_text("f_6207:batch_2ddriver_2escm"),(void*)f_6207},
{C_text("f_6210:batch_2ddriver_2escm"),(void*)f_6210},
{C_text("f_6222:batch_2ddriver_2escm"),(void*)f_6222},
{C_text("f_6225:batch_2ddriver_2escm"),(void*)f_6225},
{C_text("f_6229:batch_2ddriver_2escm"),(void*)f_6229},
{C_text("f_6238:batch_2ddriver_2escm"),(void*)f_6238},
{C_text("f_6241:batch_2ddriver_2escm"),(void*)f_6241},
{C_text("f_6244:batch_2ddriver_2escm"),(void*)f_6244},
{C_text("f_6250:batch_2ddriver_2escm"),(void*)f_6250},
{C_text("f_6282:batch_2ddriver_2escm"),(void*)f_6282},
{C_text("f_6288:batch_2ddriver_2escm"),(void*)f_6288},
{C_text("f_6293:batch_2ddriver_2escm"),(void*)f_6293},
{C_text("f_6302:batch_2ddriver_2escm"),(void*)f_6302},
{C_text("f_6308:batch_2ddriver_2escm"),(void*)f_6308},
{C_text("f_6317:batch_2ddriver_2escm"),(void*)f_6317},
{C_text("f_6321:batch_2ddriver_2escm"),(void*)f_6321},
{C_text("f_6327:batch_2ddriver_2escm"),(void*)f_6327},
{C_text("f_6330:batch_2ddriver_2escm"),(void*)f_6330},
{C_text("f_6335:batch_2ddriver_2escm"),(void*)f_6335},
{C_text("f_6338:batch_2ddriver_2escm"),(void*)f_6338},
{C_text("f_6341:batch_2ddriver_2escm"),(void*)f_6341},
{C_text("f_6344:batch_2ddriver_2escm"),(void*)f_6344},
{C_text("f_6347:batch_2ddriver_2escm"),(void*)f_6347},
{C_text("f_6350:batch_2ddriver_2escm"),(void*)f_6350},
{C_text("f_6353:batch_2ddriver_2escm"),(void*)f_6353},
{C_text("f_6356:batch_2ddriver_2escm"),(void*)f_6356},
{C_text("f_6362:batch_2ddriver_2escm"),(void*)f_6362},
{C_text("f_6372:batch_2ddriver_2escm"),(void*)f_6372},
{C_text("f_6385:batch_2ddriver_2escm"),(void*)f_6385},
{C_text("f_6395:batch_2ddriver_2escm"),(void*)f_6395},
{C_text("f_6414:batch_2ddriver_2escm"),(void*)f_6414},
{C_text("f_6426:batch_2ddriver_2escm"),(void*)f_6426},
{C_text("f_6437:batch_2ddriver_2escm"),(void*)f_6437},
{C_text("f_6447:batch_2ddriver_2escm"),(void*)f_6447},
{C_text("f_6463:batch_2ddriver_2escm"),(void*)f_6463},
{C_text("f_6469:batch_2ddriver_2escm"),(void*)f_6469},
{C_text("f_6476:batch_2ddriver_2escm"),(void*)f_6476},
{C_text("f_6484:batch_2ddriver_2escm"),(void*)f_6484},
{C_text("f_6494:batch_2ddriver_2escm"),(void*)f_6494},
{C_text("f_6508:batch_2ddriver_2escm"),(void*)f_6508},
{C_text("f_6521:batch_2ddriver_2escm"),(void*)f_6521},
{C_text("f_6523:batch_2ddriver_2escm"),(void*)f_6523},
{C_text("f_6559:batch_2ddriver_2escm"),(void*)f_6559},
{C_text("f_6563:batch_2ddriver_2escm"),(void*)f_6563},
{C_text("f_6567:batch_2ddriver_2escm"),(void*)f_6567},
{C_text("f_6570:batch_2ddriver_2escm"),(void*)f_6570},
{C_text("f_6573:batch_2ddriver_2escm"),(void*)f_6573},
{C_text("f_6583:batch_2ddriver_2escm"),(void*)f_6583},
{C_text("f_6588:batch_2ddriver_2escm"),(void*)f_6588},
{C_text("f_6613:batch_2ddriver_2escm"),(void*)f_6613},
{C_text("f_6628:batch_2ddriver_2escm"),(void*)f_6628},
{C_text("f_6634:batch_2ddriver_2escm"),(void*)f_6634},
{C_text("f_6645:batch_2ddriver_2escm"),(void*)f_6645},
{C_text("f_6649:batch_2ddriver_2escm"),(void*)f_6649},
{C_text("f_6657:batch_2ddriver_2escm"),(void*)f_6657},
{C_text("f_6660:batch_2ddriver_2escm"),(void*)f_6660},
{C_text("f_6663:batch_2ddriver_2escm"),(void*)f_6663},
{C_text("f_6666:batch_2ddriver_2escm"),(void*)f_6666},
{C_text("f_6683:batch_2ddriver_2escm"),(void*)f_6683},
{C_text("f_6693:batch_2ddriver_2escm"),(void*)f_6693},
{C_text("f_6707:batch_2ddriver_2escm"),(void*)f_6707},
{C_text("f_6713:batch_2ddriver_2escm"),(void*)f_6713},
{C_text("f_6726:batch_2ddriver_2escm"),(void*)f_6726},
{C_text("f_6732:batch_2ddriver_2escm"),(void*)f_6732},
{C_text("f_6735:batch_2ddriver_2escm"),(void*)f_6735},
{C_text("f_6738:batch_2ddriver_2escm"),(void*)f_6738},
{C_text("f_6742:batch_2ddriver_2escm"),(void*)f_6742},
{C_text("f_6749:batch_2ddriver_2escm"),(void*)f_6749},
{C_text("f_6751:batch_2ddriver_2escm"),(void*)f_6751},
{C_text("f_6776:batch_2ddriver_2escm"),(void*)f_6776},
{C_text("f_6793:batch_2ddriver_2escm"),(void*)f_6793},
{C_text("f_6799:batch_2ddriver_2escm"),(void*)f_6799},
{C_text("f_6802:batch_2ddriver_2escm"),(void*)f_6802},
{C_text("f_6805:batch_2ddriver_2escm"),(void*)f_6805},
{C_text("f_6808:batch_2ddriver_2escm"),(void*)f_6808},
{C_text("f_6812:batch_2ddriver_2escm"),(void*)f_6812},
{C_text("f_6822:batch_2ddriver_2escm"),(void*)f_6822},
{C_text("f_6824:batch_2ddriver_2escm"),(void*)f_6824},
{C_text("f_6849:batch_2ddriver_2escm"),(void*)f_6849},
{C_text("f_6867:batch_2ddriver_2escm"),(void*)f_6867},
{C_text("f_6883:batch_2ddriver_2escm"),(void*)f_6883},
{C_text("f_6902:batch_2ddriver_2escm"),(void*)f_6902},
{C_text("f_6904:batch_2ddriver_2escm"),(void*)f_6904},
{C_text("f_6929:batch_2ddriver_2escm"),(void*)f_6929},
{C_text("f_6961:batch_2ddriver_2escm"),(void*)f_6961},
{C_text("f_6976:batch_2ddriver_2escm"),(void*)f_6976},
{C_text("f_6980:batch_2ddriver_2escm"),(void*)f_6980},
{C_text("f_6984:batch_2ddriver_2escm"),(void*)f_6984},
{C_text("f_7010:batch_2ddriver_2escm"),(void*)f_7010},
{C_text("f_7044:batch_2ddriver_2escm"),(void*)f_7044},
{C_text("f_7078:batch_2ddriver_2escm"),(void*)f_7078},
{C_text("f_7103:batch_2ddriver_2escm"),(void*)f_7103},
{C_text("f_7128:batch_2ddriver_2escm"),(void*)f_7128},
{C_text("f_7135:batch_2ddriver_2escm"),(void*)f_7135},
{C_text("f_7145:batch_2ddriver_2escm"),(void*)f_7145},
{C_text("f_7147:batch_2ddriver_2escm"),(void*)f_7147},
{C_text("f_7172:batch_2ddriver_2escm"),(void*)f_7172},
{C_text("f_7182:batch_2ddriver_2escm"),(void*)f_7182},
{C_text("f_7186:batch_2ddriver_2escm"),(void*)f_7186},
{C_text("f_7191:batch_2ddriver_2escm"),(void*)f_7191},
{C_text("f_7202:batch_2ddriver_2escm"),(void*)f_7202},
{C_text("f_7212:batch_2ddriver_2escm"),(void*)f_7212},
{C_text("f_7216:batch_2ddriver_2escm"),(void*)f_7216},
{C_text("f_7226:batch_2ddriver_2escm"),(void*)f_7226},
{C_text("f_7228:batch_2ddriver_2escm"),(void*)f_7228},
{C_text("f_7253:batch_2ddriver_2escm"),(void*)f_7253},
{C_text("f_7262:batch_2ddriver_2escm"),(void*)f_7262},
{C_text("f_7287:batch_2ddriver_2escm"),(void*)f_7287},
{C_text("f_7300:batch_2ddriver_2escm"),(void*)f_7300},
{C_text("f_7303:batch_2ddriver_2escm"),(void*)f_7303},
{C_text("f_7310:batch_2ddriver_2escm"),(void*)f_7310},
{C_text("f_7315:batch_2ddriver_2escm"),(void*)f_7315},
{C_text("f_7321:batch_2ddriver_2escm"),(void*)f_7321},
{C_text("f_7325:batch_2ddriver_2escm"),(void*)f_7325},
{C_text("f_7343:batch_2ddriver_2escm"),(void*)f_7343},
{C_text("f_7350:batch_2ddriver_2escm"),(void*)f_7350},
{C_text("f_7358:batch_2ddriver_2escm"),(void*)f_7358},
{C_text("f_7376:batch_2ddriver_2escm"),(void*)f_7376},
{C_text("f_7382:batch_2ddriver_2escm"),(void*)f_7382},
{C_text("f_7431:batch_2ddriver_2escm"),(void*)f_7431},
{C_text("f_7438:batch_2ddriver_2escm"),(void*)f_7438},
{C_text("f_7454:batch_2ddriver_2escm"),(void*)f_7454},
{C_text("f_7457:batch_2ddriver_2escm"),(void*)f_7457},
{C_text("f_7463:batch_2ddriver_2escm"),(void*)f_7463},
{C_text("f_7465:batch_2ddriver_2escm"),(void*)f_7465},
{C_text("f_7514:batch_2ddriver_2escm"),(void*)f_7514},
{C_text("f_7521:batch_2ddriver_2escm"),(void*)f_7521},
{C_text("f_7526:batch_2ddriver_2escm"),(void*)f_7526},
{C_text("f_7551:batch_2ddriver_2escm"),(void*)f_7551},
{C_text("f_7562:batch_2ddriver_2escm"),(void*)f_7562},
{C_text("f_7564:batch_2ddriver_2escm"),(void*)f_7564},
{C_text("f_7574:batch_2ddriver_2escm"),(void*)f_7574},
{C_text("f_7587:batch_2ddriver_2escm"),(void*)f_7587},
{C_text("f_7597:batch_2ddriver_2escm"),(void*)f_7597},
{C_text("f_7610:batch_2ddriver_2escm"),(void*)f_7610},
{C_text("f_7618:batch_2ddriver_2escm"),(void*)f_7618},
{C_text("f_7620:batch_2ddriver_2escm"),(void*)f_7620},
{C_text("f_7630:batch_2ddriver_2escm"),(void*)f_7630},
{C_text("f_7643:batch_2ddriver_2escm"),(void*)f_7643},
{C_text("f_7651:batch_2ddriver_2escm"),(void*)f_7651},
{C_text("f_7664:batch_2ddriver_2escm"),(void*)f_7664},
{C_text("f_7673:batch_2ddriver_2escm"),(void*)f_7673},
{C_text("f_7678:batch_2ddriver_2escm"),(void*)f_7678},
{C_text("f_7689:batch_2ddriver_2escm"),(void*)f_7689},
{C_text("f_7699:batch_2ddriver_2escm"),(void*)f_7699},
{C_text("f_7712:batch_2ddriver_2escm"),(void*)f_7712},
{C_text("f_7722:batch_2ddriver_2escm"),(void*)f_7722},
{C_text("f_7767:batch_2ddriver_2escm"),(void*)f_7767},
{C_text("f_7773:batch_2ddriver_2escm"),(void*)f_7773},
{C_text("f_7775:batch_2ddriver_2escm"),(void*)f_7775},
{C_text("f_7800:batch_2ddriver_2escm"),(void*)f_7800},
{C_text("f_7812:batch_2ddriver_2escm"),(void*)f_7812},
{C_text("f_7815:batch_2ddriver_2escm"),(void*)f_7815},
{C_text("f_7818:batch_2ddriver_2escm"),(void*)f_7818},
{C_text("f_7821:batch_2ddriver_2escm"),(void*)f_7821},
{C_text("f_7829:batch_2ddriver_2escm"),(void*)f_7829},
{C_text("f_7837:batch_2ddriver_2escm"),(void*)f_7837},
{C_text("f_7843:batch_2ddriver_2escm"),(void*)f_7843},
{C_text("f_7876:batch_2ddriver_2escm"),(void*)f_7876},
{C_text("f_7879:batch_2ddriver_2escm"),(void*)f_7879},
{C_text("f_7886:batch_2ddriver_2escm"),(void*)f_7886},
{C_text("f_7889:batch_2ddriver_2escm"),(void*)f_7889},
{C_text("f_7892:batch_2ddriver_2escm"),(void*)f_7892},
{C_text("f_7899:batch_2ddriver_2escm"),(void*)f_7899},
{C_text("f_7902:batch_2ddriver_2escm"),(void*)f_7902},
{C_text("f_7905:batch_2ddriver_2escm"),(void*)f_7905},
{C_text("f_7912:batch_2ddriver_2escm"),(void*)f_7912},
{C_text("f_7918:batch_2ddriver_2escm"),(void*)f_7918},
{C_text("f_7922:batch_2ddriver_2escm"),(void*)f_7922},
{C_text("f_7954:batch_2ddriver_2escm"),(void*)f_7954},
{C_text("f_8001:batch_2ddriver_2escm"),(void*)f_8001},
{C_text("f_8039:batch_2ddriver_2escm"),(void*)f_8039},
{C_text("f_8044:batch_2ddriver_2escm"),(void*)f_8044},
{C_text("f_8060:batch_2ddriver_2escm"),(void*)f_8060},
{C_text("f_8065:batch_2ddriver_2escm"),(void*)f_8065},
{C_text("f_8090:batch_2ddriver_2escm"),(void*)f_8090},
{C_text("f_8101:batch_2ddriver_2escm"),(void*)f_8101},
{C_text("f_8115:batch_2ddriver_2escm"),(void*)f_8115},
{C_text("f_8119:batch_2ddriver_2escm"),(void*)f_8119},
{C_text("f_8136:batch_2ddriver_2escm"),(void*)f_8136},
{C_text("f_8161:batch_2ddriver_2escm"),(void*)f_8161},
{C_text("f_8172:batch_2ddriver_2escm"),(void*)f_8172},
{C_text("f_8176:batch_2ddriver_2escm"),(void*)f_8176},
{C_text("f_8180:batch_2ddriver_2escm"),(void*)f_8180},
{C_text("f_8204:batch_2ddriver_2escm"),(void*)f_8204},
{C_text("f_8215:batch_2ddriver_2escm"),(void*)f_8215},
{C_text("f_8234:batch_2ddriver_2escm"),(void*)f_8234},
{C_text("f_8242:batch_2ddriver_2escm"),(void*)f_8242},
{C_text("f_8249:batch_2ddriver_2escm"),(void*)f_8249},
{C_text("toplevel:batch_2ddriver_2escm"),(void*)C_batch_2ddriver_toplevel},
{NULL,NULL}};
#endif

static C_PTABLE_ENTRY *create_ptable(void){
#ifdef C_ENABLE_PTABLES
return ptable;
#else
return NULL;
#endif
}

/*
o|hiding unexported module binding: chicken.compiler.batch-driver#partition 
o|hiding unexported module binding: chicken.compiler.batch-driver#span 
o|hiding unexported module binding: chicken.compiler.batch-driver#take 
o|hiding unexported module binding: chicken.compiler.batch-driver#drop 
o|hiding unexported module binding: chicken.compiler.batch-driver#split-at 
o|hiding unexported module binding: chicken.compiler.batch-driver#append-map 
o|hiding unexported module binding: chicken.compiler.batch-driver#every 
o|hiding unexported module binding: chicken.compiler.batch-driver#any 
o|hiding unexported module binding: chicken.compiler.batch-driver#cons* 
o|hiding unexported module binding: chicken.compiler.batch-driver#concatenate 
o|hiding unexported module binding: chicken.compiler.batch-driver#delete 
o|hiding unexported module binding: chicken.compiler.batch-driver#first 
o|hiding unexported module binding: chicken.compiler.batch-driver#second 
o|hiding unexported module binding: chicken.compiler.batch-driver#third 
o|hiding unexported module binding: chicken.compiler.batch-driver#fourth 
o|hiding unexported module binding: chicken.compiler.batch-driver#fifth 
o|hiding unexported module binding: chicken.compiler.batch-driver#delete-duplicates 
o|hiding unexported module binding: chicken.compiler.batch-driver#alist-cons 
o|hiding unexported module binding: chicken.compiler.batch-driver#filter 
o|hiding unexported module binding: chicken.compiler.batch-driver#filter-map 
o|hiding unexported module binding: chicken.compiler.batch-driver#remove 
o|hiding unexported module binding: chicken.compiler.batch-driver#unzip1 
o|hiding unexported module binding: chicken.compiler.batch-driver#last 
o|hiding unexported module binding: chicken.compiler.batch-driver#list-index 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset-adjoin/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset-difference/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset-union/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset-intersection/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#list-tabulate 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset<=/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#lset=/eq? 
o|hiding unexported module binding: chicken.compiler.batch-driver#length+ 
o|hiding unexported module binding: chicken.compiler.batch-driver#find 
o|hiding unexported module binding: chicken.compiler.batch-driver#find-tail 
o|hiding unexported module binding: chicken.compiler.batch-driver#iota 
o|hiding unexported module binding: chicken.compiler.batch-driver#make-list 
o|hiding unexported module binding: chicken.compiler.batch-driver#posq 
o|hiding unexported module binding: chicken.compiler.batch-driver#posv 
o|hiding unexported module binding: chicken.compiler.batch-driver#print-program-statistics 
o|hiding unexported module binding: chicken.compiler.batch-driver#initialize-analysis-database 
o|hiding unexported module binding: chicken.compiler.batch-driver#display-analysis-database 
S|applied compiler syntax:
S|  chicken.format#sprintf		2
S|  scheme#for-each		15
S|  chicken.format#printf		18
S|  chicken.base#foldl		3
S|  scheme#map		21
S|  chicken.base#foldr		3
o|eliminated procedure checks: 198 
o|folded constant expression: (scheme#* (quote 1024) (quote 1024)) 
o|specializations:
o|  1 (scheme#current-output-port)
o|  5 (chicken.base#add1 *)
o|  2 (scheme#string=? string string)
o|  1 (scheme#string-append string string)
o|  4 (scheme#eqv? (or eof null fixnum char boolean symbol keyword) *)
o|  1 (scheme#< fixnum fixnum)
o|  1 (scheme#- fixnum fixnum)
o|  68 (scheme#memq * list)
o|  39 (scheme#eqv? * (or eof null fixnum char boolean symbol keyword))
o|  20 (##sys#check-output-port * * *)
o|  1 (scheme#eqv? * *)
o|  6 (##sys#check-list (or pair list) *)
o|  27 (scheme#cdr pair)
o|  10 (scheme#car pair)
(o e)|safe calls: 692 
(o e)|assignments to immediate values: 4 
o|safe globals: (chicken.compiler.batch-driver#compile-source-file chicken.compiler.batch-driver#display-analysis-database chicken.compiler.batch-driver#initialize-analysis-database chicken.compiler.batch-driver#print-program-statistics chicken.compiler.batch-driver#posv chicken.compiler.batch-driver#posq chicken.compiler.batch-driver#make-list chicken.compiler.batch-driver#iota chicken.compiler.batch-driver#find-tail chicken.compiler.batch-driver#find chicken.compiler.batch-driver#length+ chicken.compiler.batch-driver#lset=/eq? chicken.compiler.batch-driver#lset<=/eq? chicken.compiler.batch-driver#list-tabulate chicken.compiler.batch-driver#lset-intersection/eq? chicken.compiler.batch-driver#lset-union/eq? chicken.compiler.batch-driver#lset-difference/eq? chicken.compiler.batch-driver#lset-adjoin/eq? chicken.compiler.batch-driver#list-index chicken.compiler.batch-driver#last chicken.compiler.batch-driver#unzip1 chicken.compiler.batch-driver#remove chicken.compiler.batch-driver#filter-map chicken.compiler.batch-driver#filter chicken.compiler.batch-driver#alist-cons chicken.compiler.batch-driver#delete-duplicates chicken.compiler.batch-driver#fifth chicken.compiler.batch-driver#fourth chicken.compiler.batch-driver#third chicken.compiler.batch-driver#second chicken.compiler.batch-driver#first chicken.compiler.batch-driver#delete chicken.compiler.batch-driver#concatenate chicken.compiler.batch-driver#cons* chicken.compiler.batch-driver#any chicken.compiler.batch-driver#every chicken.compiler.batch-driver#append-map chicken.compiler.batch-driver#split-at chicken.compiler.batch-driver#drop chicken.compiler.batch-driver#take chicken.compiler.batch-driver#span chicken.compiler.batch-driver#partition) 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#partition 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#span 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#drop 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#split-at 
o|merged explicitly consed rest parameter: lsts237 
o|inlining procedure: k2973 
o|inlining procedure: k2988 
o|inlining procedure: k2988 
o|inlining procedure: k2973 
o|inlining procedure: k3028 
o|inlining procedure: k3028 
o|inlining procedure: k3060 
o|contracted procedure: "(mini-srfi-1.scm:77) g290299" 
o|inlining procedure: k3060 
o|inlining procedure: k3109 
o|contracted procedure: "(mini-srfi-1.scm:76) g263272" 
o|inlining procedure: k3109 
o|inlining procedure: k3152 
o|inlining procedure: k3152 
o|inlining procedure: k3183 
o|inlining procedure: k3183 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#cons* 
o|inlining procedure: k3241 
o|inlining procedure: k3241 
o|inlining procedure: k3269 
o|inlining procedure: k3269 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#second 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#third 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#fourth 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#fifth 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#delete-duplicates 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#alist-cons 
o|inlining procedure: k3400 
o|inlining procedure: k3400 
o|inlining procedure: k3392 
o|inlining procedure: k3392 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#filter-map 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#unzip1 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#last 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#list-index 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#lset-adjoin/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#lset-difference/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#lset-union/eq? 
o|inlining procedure: k3791 
o|inlining procedure: k3791 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#lset<=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#lset=/eq? 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#length+ 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#find 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#find-tail 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#iota 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#make-list 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#posq 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#posv 
o|inlining procedure: k4151 
o|inlining procedure: k4191 
o|contracted procedure: "(batch-driver.scm:97) g776783" 
o|inlining procedure: k4191 
o|propagated global variable: g782784 chicken.compiler.core#internal-bindings 
o|inlining procedure: k4214 
o|contracted procedure: "(batch-driver.scm:93) g758765" 
o|inlining procedure: k4214 
o|propagated global variable: g764766 chicken.compiler.core#extended-bindings 
o|inlining procedure: k4237 
o|contracted procedure: "(batch-driver.scm:89) g740747" 
o|inlining procedure: k4237 
o|propagated global variable: g746748 chicken.compiler.core#standard-bindings 
o|inlining procedure: k4151 
o|inlining procedure: k4269 
o|inlining procedure: k4269 
o|inlining procedure: k4334 
o|contracted procedure: "(batch-driver.scm:168) g959966" 
o|propagated global variable: out969972 ##sys#standard-output 
o|substituted constant variable: a4305 
o|substituted constant variable: a4306 
o|propagated global variable: out969972 ##sys#standard-output 
o|inlining procedure: k4334 
o|propagated global variable: out943946 ##sys#standard-output 
o|substituted constant variable: a4358 
o|substituted constant variable: a4359 
o|propagated global variable: out943946 ##sys#standard-output 
o|propagated global variable: out950953 ##sys#standard-output 
o|substituted constant variable: a4385 
o|substituted constant variable: a4386 
o|inlining procedure: k4378 
o|propagated global variable: out950953 ##sys#standard-output 
o|inlining procedure: k4378 
o|propagated global variable: out932935 ##sys#standard-output 
o|substituted constant variable: a4423 
o|substituted constant variable: a4424 
o|propagated global variable: out932935 ##sys#standard-output 
o|propagated global variable: out926929 ##sys#standard-output 
o|substituted constant variable: a4439 
o|substituted constant variable: a4440 
o|propagated global variable: out926929 ##sys#standard-output 
o|propagated global variable: out920923 ##sys#standard-output 
o|substituted constant variable: a4455 
o|substituted constant variable: a4456 
o|propagated global variable: out920923 ##sys#standard-output 
o|inlining procedure: k4470 
o|propagated global variable: out874877 ##sys#standard-output 
o|substituted constant variable: a4494 
o|substituted constant variable: a4495 
o|substituted constant variable: names795 
o|propagated global variable: out874877 ##sys#standard-output 
o|inlining procedure: k4514 
o|inlining procedure: k4514 
o|inlining procedure: k4527 
o|inlining procedure: k4527 
o|inlining procedure: k4537 
o|inlining procedure: k4537 
o|propagated global variable: out910913 ##sys#standard-output 
o|substituted constant variable: a4573 
o|substituted constant variable: a4574 
o|inlining procedure: k4563 
o|propagated global variable: out910913 ##sys#standard-output 
o|inlining procedure: k4563 
o|inlining procedure: k4605 
o|inlining procedure: k4605 
o|substituted constant variable: a4631 
o|substituted constant variable: a4633 
o|substituted constant variable: a4635 
o|inlining procedure: k4639 
o|inlining procedure: k4639 
o|inlining procedure: k4651 
o|inlining procedure: k4651 
o|inlining procedure: k4663 
o|inlining procedure: k4663 
o|inlining procedure: k4675 
o|inlining procedure: k4675 
o|inlining procedure: k4687 
o|inlining procedure: k4687 
o|substituted constant variable: a4694 
o|substituted constant variable: a4696 
o|substituted constant variable: a4698 
o|substituted constant variable: a4700 
o|substituted constant variable: a4702 
o|substituted constant variable: a4704 
o|substituted constant variable: a4706 
o|substituted constant variable: a4708 
o|substituted constant variable: a4710 
o|substituted constant variable: a4712 
o|substituted constant variable: a4714 
o|substituted constant variable: a4716 
o|substituted constant variable: a4718 
o|substituted constant variable: a4720 
o|substituted constant variable: a4722 
o|inlining procedure: k4726 
o|inlining procedure: k4726 
o|inlining procedure: k4738 
o|inlining procedure: k4738 
o|inlining procedure: k4750 
o|inlining procedure: k4750 
o|inlining procedure: k4762 
o|inlining procedure: k4762 
o|inlining procedure: k4774 
o|inlining procedure: k4774 
o|inlining procedure: k4786 
o|inlining procedure: k4786 
o|inlining procedure: k4798 
o|inlining procedure: k4798 
o|inlining procedure: k4810 
o|inlining procedure: k4810 
o|inlining procedure: k4822 
o|inlining procedure: k4822 
o|inlining procedure: k4834 
o|inlining procedure: k4834 
o|substituted constant variable: a4841 
o|substituted constant variable: a4843 
o|substituted constant variable: a4845 
o|substituted constant variable: a4847 
o|substituted constant variable: a4849 
o|substituted constant variable: a4851 
o|substituted constant variable: a4853 
o|substituted constant variable: a4855 
o|substituted constant variable: a4857 
o|substituted constant variable: a4859 
o|substituted constant variable: a4861 
o|substituted constant variable: a4863 
o|substituted constant variable: a4865 
o|substituted constant variable: a4867 
o|substituted constant variable: a4869 
o|substituted constant variable: a4871 
o|substituted constant variable: a4873 
o|substituted constant variable: a4875 
o|substituted constant variable: a4877 
o|substituted constant variable: a4879 
o|substituted constant variable: a4881 
o|inlining procedure: k4470 
o|inlining procedure: k4892 
o|inlining procedure: k4892 
o|substituted constant variable: a4923 
o|substituted constant variable: a4926 
o|substituted constant variable: a4937 
o|substituted constant variable: a4939 
o|substituted constant variable: a4944 
o|substituted constant variable: a4946 
o|substituted constant variable: a4966 
o|substituted constant variable: a4971 
o|substituted constant variable: a4976 
o|substituted constant variable: a4978 
o|substituted constant variable: a4980 
o|substituted constant variable: a4982 
o|substituted constant variable: a4984 
o|substituted constant variable: a4986 
o|substituted constant variable: a4991 
o|merged explicitly consed rest parameter: args1130 
o|propagated global variable: out11341137 ##sys#standard-output 
o|substituted constant variable: a5025 
o|substituted constant variable: a5026 
o|inlining procedure: k5018 
o|propagated global variable: out11341137 ##sys#standard-output 
o|inlining procedure: k5018 
o|inlining procedure: k5042 
o|inlining procedure: k5042 
o|propagated global variable: out11511154 ##sys#standard-output 
o|substituted constant variable: a5071 
o|substituted constant variable: a5072 
o|inlining procedure: k5064 
o|propagated global variable: out11511154 ##sys#standard-output 
o|inlining procedure: k5064 
o|inlining procedure: k5091 
o|inlining procedure: k5114 
o|contracted procedure: "(batch-driver.scm:282) g11661173" 
o|inlining procedure: k5114 
o|inlining procedure: k5091 
o|substituted constant variable: a5141 
o|inlining procedure: k5145 
o|inlining procedure: k5145 
o|inlining procedure: k5160 
o|inlining procedure: k5160 
o|substituted constant variable: a5207 
o|substituted constant variable: a5209 
o|substituted constant variable: a5214 
o|substituted constant variable: a5216 
o|substituted constant variable: a5218 
o|inlining procedure: k5231 
o|inlining procedure: k5231 
o|inlining procedure: k5257 
o|inlining procedure: "(batch-driver.scm:305) cputime1090" 
o|inlining procedure: k5257 
o|propagated global variable: out12141217 ##sys#standard-output 
o|substituted constant variable: a5271 
o|substituted constant variable: a5272 
o|inlining procedure: k5267 
o|inlining procedure: "(batch-driver.scm:311) cputime1090" 
o|propagated global variable: out12141217 ##sys#standard-output 
o|inlining procedure: k5267 
o|merged explicitly consed rest parameter: args1225 
o|inlining procedure: k5312 
o|propagated global variable: g12501251 chicken.compiler.support#db-get 
o|propagated global variable: g12641265 chicken.compiler.support#db-put! 
o|inlining procedure: k5312 
o|inlining procedure: k5340 
(o x)|known list op on rest arg sublist: ##core#rest-cdr args1225 0 
o|inlining procedure: k5340 
o|substituted constant variable: a5379 
o|substituted constant variable: a5483 
o|substituted constant variable: a5488 
o|substituted constant variable: a5493 
o|substituted constant variable: a5498 
o|substituted constant variable: a5503 
o|substituted constant variable: a5642 
o|substituted constant variable: a5666 
o|inlining procedure: k5663 
o|inlining procedure: k5663 
o|substituted constant variable: a5677 
o|substituted constant variable: a5688 
o|inlining procedure: k5685 
o|inlining procedure: k5685 
o|inlining procedure: k5915 
o|inlining procedure: k5930 
o|inlining procedure: k5930 
o|inlining procedure: k5948 
o|inlining procedure: k5948 
o|inlining procedure: k5970 
o|inlining procedure: k5970 
o|consed rest parameter at call site: "(batch-driver.scm:802) analyze1100" 3 
o|inlining procedure: k5915 
o|consed rest parameter at call site: "(batch-driver.scm:873) dribble1091" 2 
o|consed rest parameter at call site: "(batch-driver.scm:864) dribble1091" 2 
o|propagated global variable: g21852186 chicken.pretty-print#pp 
o|consed rest parameter at call site: "(batch-driver.scm:857) dribble1091" 2 
o|inlining procedure: "(batch-driver.scm:844) cputime1090" 
o|inlining procedure: k6168 
o|inlining procedure: k6168 
o|propagated global variable: g21632167 chicken.compiler.core#foreign-lambda-stubs 
o|consed rest parameter at call site: "(batch-driver.scm:833) dribble1091" 2 
o|inlining procedure: k6211 
o|inlining procedure: k6211 
o|contracted procedure: "(batch-driver.scm:775) chicken.compiler.batch-driver#print-program-statistics" 
o|propagated global variable: out686689 ##sys#standard-output 
o|substituted constant variable: a4071 
o|substituted constant variable: a4072 
o|propagated global variable: out695698 ##sys#standard-output 
o|substituted constant variable: a4089 
o|substituted constant variable: a4090 
o|propagated global variable: out702705 ##sys#standard-output 
o|substituted constant variable: a4101 
o|substituted constant variable: a4102 
o|propagated global variable: out709712 ##sys#standard-output 
o|substituted constant variable: a4113 
o|substituted constant variable: a4114 
o|propagated global variable: out716719 ##sys#standard-output 
o|substituted constant variable: a4125 
o|substituted constant variable: a4126 
o|propagated global variable: out723726 ##sys#standard-output 
o|substituted constant variable: a4137 
o|substituted constant variable: a4138 
o|inlining procedure: k4064 
o|propagated global variable: out723726 ##sys#standard-output 
o|propagated global variable: out716719 ##sys#standard-output 
o|propagated global variable: out709712 ##sys#standard-output 
o|propagated global variable: out702705 ##sys#standard-output 
o|propagated global variable: out695698 ##sys#standard-output 
o|propagated global variable: out686689 ##sys#standard-output 
o|inlining procedure: k4064 
o|inlining procedure: k6245 
o|consed rest parameter at call site: "(batch-driver.scm:768) dribble1091" 2 
o|inlining procedure: k6245 
o|consed rest parameter at call site: "(batch-driver.scm:758) analyze1100" 3 
o|contracted procedure: "(batch-driver.scm:742) chicken.compiler.batch-driver#first" 
o|inlining procedure: k6354 
o|inlining procedure: k6354 
o|consed rest parameter at call site: "(batch-driver.scm:723) analyze1100" 3 
o|inlining procedure: k6364 
o|contracted procedure: "(batch-driver.scm:715) g20742081" 
o|inlining procedure: k6364 
o|inlining procedure: k6387 
o|contracted procedure: "(batch-driver.scm:710) g20532060" 
o|inlining procedure: k6294 
o|inlining procedure: k6294 
o|inlining procedure: k6387 
o|substituted constant variable: a6407 
o|inlining procedure: k6409 
o|inlining procedure: k6409 
o|consed rest parameter at call site: "(batch-driver.scm:699) dribble1091" 2 
o|inlining procedure: k6439 
o|inlining procedure: k6439 
o|inlining procedure: k6464 
o|consed rest parameter at call site: "(batch-driver.scm:691) dribble1091" 2 
o|inlining procedure: k6464 
o|inlining procedure: k6486 
o|inlining procedure: k6486 
o|inlining procedure: k6525 
o|inlining procedure: k6525 
o|inlining procedure: k6590 
o|inlining procedure: k6590 
o|consed rest parameter at call site: "(batch-driver.scm:668) dribble1091" 2 
o|substituted constant variable: a6621 
o|inlining procedure: k6685 
o|contracted procedure: "(batch-driver.scm:649) g19201927" 
o|propagated global variable: out19301933 ##sys#standard-output 
o|substituted constant variable: a6653 
o|substituted constant variable: a6654 
o|propagated global variable: out19301933 ##sys#standard-output 
o|inlining procedure: k6685 
o|propagated global variable: g19261928 chicken.compiler.compiler-syntax#compiler-syntax-statistics 
o|inlining procedure: k6708 
o|substituted constant variable: a6728 
o|substituted constant variable: a6729 
o|inlining procedure: k6753 
o|inlining procedure: k6753 
o|substituted constant variable: a6795 
o|substituted constant variable: a6796 
o|inlining procedure: k6826 
o|inlining procedure: k6826 
o|inlining procedure: k6858 
o|inlining procedure: k6858 
o|contracted procedure: "(batch-driver.scm:634) chicken.compiler.batch-driver#lset-intersection/eq?" 
o|inlining procedure: k6708 
o|propagated global variable: g18351836 chicken.load#find-dynamic-extension 
o|contracted procedure: "(batch-driver.scm:633) chicken.compiler.batch-driver#remove" 
o|inlining procedure: k6906 
o|contracted procedure: "(batch-driver.scm:629) g18081817" 
o|inlining procedure: k6906 
o|propagated global variable: g18141818 chicken.compiler.core#import-libraries 
o|inlining procedure: k6986 
o|inlining procedure: k6986 
o|inlining procedure: k6993 
o|inlining procedure: k6993 
o|inlining procedure: k7012 
o|contracted procedure: "(batch-driver.scm:613) g17711780" 
o|inlining procedure: k7012 
o|propagated global variable: g17771781 chicken.compiler.core#used-units 
o|inlining procedure: k7046 
o|contracted procedure: "(batch-driver.scm:612) g17411750" 
o|inlining procedure: k7046 
o|propagated global variable: g17471751 chicken.compiler.core#immutable-constants 
o|inlining procedure: k7080 
o|inlining procedure: k7080 
o|inlining procedure: k7149 
o|inlining procedure: k7149 
o|consed rest parameter at call site: "(batch-driver.scm:596) dribble1091" 2 
o|consed rest parameter at call site: "(batch-driver.scm:573) dribble1091" 2 
o|inlining procedure: k7193 
o|inlining procedure: k7230 
o|inlining procedure: k7230 
o|inlining procedure: k7264 
o|inlining procedure: k7264 
o|inlining procedure: k7193 
o|inlining procedure: k7326 
o|inlining procedure: k7326 
o|inlining procedure: "(batch-driver.scm:560) cputime1090" 
o|consed rest parameter at call site: "(batch-driver.scm:555) dribble1091" 2 
o|substituted constant variable: a7359 
o|inlining procedure: k7361 
o|substituted constant variable: a7364 
o|inlining procedure: k7361 
o|substituted constant variable: a7369 
o|consed rest parameter at call site: "(batch-driver.scm:534) dribble1091" 2 
o|inlining procedure: k7387 
o|consed rest parameter at call site: "(batch-driver.scm:534) dribble1091" 2 
o|inlining procedure: k7387 
o|consed rest parameter at call site: "(batch-driver.scm:534) dribble1091" 2 
o|inlining procedure: k7391 
o|inlining procedure: k7391 
o|consed rest parameter at call site: "(batch-driver.scm:516) dribble1091" 2 
o|inlining procedure: k7408 
o|consed rest parameter at call site: "(batch-driver.scm:516) dribble1091" 2 
o|inlining procedure: k7408 
o|consed rest parameter at call site: "(batch-driver.scm:516) dribble1091" 2 
o|substituted constant variable: a7411 
o|substituted constant variable: a7423 
o|substituted constant variable: a7439 
o|inlining procedure: k7467 
o|contracted procedure: "(batch-driver.scm:496) g15321541" 
o|inlining procedure: k7467 
o|inlining procedure: k7528 
o|inlining procedure: k7528 
o|consed rest parameter at call site: "(batch-driver.scm:485) chicken.compiler.batch-driver#append-map" 3 
o|inlining procedure: k7566 
o|contracted procedure: "(batch-driver.scm:474) g14751482" 
o|inlining procedure: k5586 
o|inlining procedure: k5586 
o|inlining procedure: k7566 
o|consed rest parameter at call site: "(batch-driver.scm:473) dribble1091" 2 
o|inlining procedure: k7589 
o|inlining procedure: k7589 
o|propagated global variable: g14641465 chicken.string#string-split 
o|consed rest parameter at call site: "(batch-driver.scm:468) chicken.compiler.batch-driver#append-map" 3 
o|inlining procedure: k7622 
o|inlining procedure: k7622 
o|propagated global variable: g14491450 chicken.string#string-split 
o|consed rest parameter at call site: "(batch-driver.scm:465) chicken.compiler.batch-driver#append-map" 3 
o|substituted constant variable: a7658 
o|inlining procedure: k7691 
o|contracted procedure: "(batch-driver.scm:455) g14241431" 
o|inlining procedure: k7691 
o|propagated global variable: g14301432 chicken.compiler.core#default-extended-bindings 
o|inlining procedure: k7714 
o|contracted procedure: "(batch-driver.scm:450) g14051412" 
o|inlining procedure: k7714 
o|propagated global variable: g14111413 chicken.compiler.core#default-standard-bindings 
o|substituted constant variable: a7734 
o|substituted constant variable: a7737 
o|substituted constant variable: a7740 
o|substituted constant variable: a7743 
o|substituted constant variable: a7746 
o|inlining procedure: k7755 
o|inlining procedure: k7755 
o|inlining procedure: k7777 
o|inlining procedure: k7777 
o|substituted constant variable: a7808 
o|consed rest parameter at call site: "(batch-driver.scm:426) dribble1091" 2 
o|substituted constant variable: a7825 
o|consed rest parameter at call site: "(batch-driver.scm:423) dribble1091" 2 
o|substituted constant variable: a7833 
o|consed rest parameter at call site: "(batch-driver.scm:420) dribble1091" 2 
o|inlining procedure: k7844 
o|inlining procedure: k7844 
o|substituted constant variable: a7856 
o|substituted constant variable: a7864 
o|inlining procedure: k7861 
o|inlining procedure: k7861 
o|substituted constant variable: a7872 
o|consed rest parameter at call site: "(batch-driver.scm:409) dribble1091" 2 
o|inlining procedure: k7890 
o|inlining procedure: k7890 
o|inlining procedure: k7903 
o|inlining procedure: k7903 
o|substituted constant variable: a7923 
o|substituted constant variable: a7926 
o|substituted constant variable: a7929 
o|substituted constant variable: a7932 
o|substituted constant variable: a7935 
o|substituted constant variable: a7938 
o|substituted constant variable: a7941 
o|substituted constant variable: a7944 
o|substituted constant variable: a7947 
o|substituted constant variable: a7950 
o|consed rest parameter at call site: "(batch-driver.scm:372) dribble1091" 2 
o|substituted constant variable: a7957 
o|substituted constant variable: a7962 
o|substituted constant variable: a7966 
o|substituted constant variable: a7969 
o|substituted constant variable: a7972 
o|substituted constant variable: a7975 
o|substituted constant variable: a7995 
o|inlining procedure: k7991 
o|inlining procedure: k7991 
o|inlining procedure: k8003 
o|contracted procedure: "(batch-driver.scm:342) g13151324" 
o|substituted constant variable: a5410 
o|inlining procedure: k8003 
o|inlining procedure: k8067 
o|contracted procedure: "(batch-driver.scm:334) g12871296" 
o|inlining procedure: k8067 
o|consed rest parameter at call site: "(batch-driver.scm:332) chicken.compiler.batch-driver#append-map" 3 
o|substituted constant variable: a8102 
o|propagated global variable: tmp12741276 chicken.compiler.core#unit-name 
o|inlining procedure: k8108 
o|propagated global variable: tmp12741276 chicken.compiler.core#unit-name 
o|inlining procedure: k8108 
o|substituted constant variable: a8123 
o|substituted constant variable: a8128 
o|inlining procedure: k8130 
o|inlining procedure: k8130 
o|substituted constant variable: a8133 
o|inlining procedure: k8138 
o|inlining procedure: k8138 
o|inlining procedure: k8173 
o|inlining procedure: k8173 
o|inlining procedure: k8181 
o|inlining procedure: k8181 
o|substituted constant variable: a8196 
o|inlining procedure: k8193 
o|inlining procedure: k8193 
o|inlining procedure: k8202 
o|inlining procedure: k8202 
o|inlining procedure: k8221 
o|inlining procedure: k8221 
o|inlining procedure: k8244 
o|inlining procedure: k8244 
o|inlining procedure: k8250 
o|inlining procedure: k8262 
o|inlining procedure: k8262 
o|inlining procedure: k8250 
o|substituted constant variable: a8284 
o|substituted constant variable: a8290 
o|substituted constant variable: a8293 
o|replaced variables: 1100 
o|removed binding forms: 535 
o|substituted constant variable: r29898298 
o|substituted constant variable: r30298300 
o|contracted procedure: "(mini-srfi-1.scm:74) chicken.compiler.batch-driver#any" 
o|substituted constant variable: r31848308 
o|substituted constant variable: r32428310 
o|substituted constant variable: r33938317 
o|removed side-effect free assignment to unused variable: chicken.compiler.batch-driver#list-tabulate 
o|propagated global variable: out969972 ##sys#standard-output 
o|inlining procedure: k4293 
o|propagated global variable: out943946 ##sys#standard-output 
o|propagated global variable: out950953 ##sys#standard-output 
o|propagated global variable: out932935 ##sys#standard-output 
o|propagated global variable: out926929 ##sys#standard-output 
o|propagated global variable: out920923 ##sys#standard-output 
o|propagated global variable: out874877 ##sys#standard-output 
o|propagated global variable: out910913 ##sys#standard-output 
o|removed side-effect free assignment to unused variable: cputime1090 
o|propagated global variable: out11341137 ##sys#standard-output 
o|substituted constant variable: r50198382 
o|substituted constant variable: r50198383 
o|propagated global variable: out11511154 ##sys#standard-output 
o|substituted constant variable: r52328397 
o|propagated global variable: out12141217 ##sys#standard-output 
o|propagated global variable: out686689 ##sys#standard-output 
o|propagated global variable: out695698 ##sys#standard-output 
o|propagated global variable: out702705 ##sys#standard-output 
o|propagated global variable: out709712 ##sys#standard-output 
o|propagated global variable: out716719 ##sys#standard-output 
o|propagated global variable: out723726 ##sys#standard-output 
o|substituted constant variable: r64658465 
o|propagated global variable: out19301933 ##sys#standard-output 
o|substituted constant variable: r68598479 
o|substituted constant variable: r68598479 
o|inlining procedure: k6858 
o|contracted procedure: "(mini-srfi-1.scm:183) chicken.compiler.batch-driver#every" 
o|substituted constant variable: ls1550 
o|substituted constant variable: r67098484 
o|substituted constant variable: r69878489 
o|substituted constant variable: r69878489 
o|inlining procedure: k6993 
o|substituted constant variable: r69948493 
o|substituted constant variable: r69948493 
o|substituted constant variable: r73888517 
o|substituted constant variable: r73888517 
o|substituted constant variable: r73888519 
o|substituted constant variable: r73888519 
o|substituted constant variable: r73928521 
o|substituted constant variable: r73928521 
o|substituted constant variable: r73928523 
o|substituted constant variable: r73928523 
o|substituted constant variable: r74098525 
o|substituted constant variable: r74098525 
o|substituted constant variable: r74098527 
o|substituted constant variable: r74098527 
o|contracted procedure: "(batch-driver.scm:480) chicken.compiler.batch-driver#delete" 
o|propagated global variable: lst350 ##sys#features 
o|substituted constant variable: r77568554 
o|substituted constant variable: r79928571 
o|propagated global variable: r81098577 chicken.compiler.core#unit-name 
o|substituted constant variable: r81748587 
o|substituted constant variable: r81748587 
o|substituted constant variable: r81948591 
o|substituted constant variable: r82038595 
o|substituted constant variable: r82038595 
o|substituted constant variable: r82228597 
o|substituted constant variable: r82228597 
o|substituted constant variable: r82458601 
o|substituted constant variable: r82458601 
o|substituted constant variable: r82458603 
o|substituted constant variable: r82458603 
o|substituted constant variable: r82638607 
o|substituted constant variable: r82518608 
o|converted assignments to bindings: (option-arg992) 
o|simplifications: ((let . 1)) 
o|replaced variables: 101 
o|removed binding forms: 1070 
o|removed conditional forms: 1 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k4479 
o|inlining procedure: k6089 
o|inlining procedure: k6129 
o|inlining procedure: k6393 
o|contracted procedure: k6858 
o|inlining procedure: "(mini-srfi-1.scm:141) a6868" 
o|contracted procedure: k6993 
o|inlining procedure: k7394 
o|substituted constant variable: x349 
o|inlining procedure: k7749 
o|inlining procedure: k7749 
o|replaced variables: 7 
o|removed binding forms: 153 
o|substituted constant variable: r61308807 
o|substituted constant variable: r68598690 
o|substituted constant variable: r6859 
o|substituted constant variable: r6994 
o|substituted constant variable: r73958857 
o|substituted constant variable: r77508874 
o|substituted constant variable: r77508875 
o|replaced variables: 2 
o|removed binding forms: 22 
o|removed conditional forms: 4 
o|removed binding forms: 10 
o|simplifications: ((if . 14) (let . 21) (##core#call . 417)) 
o|  call simplifications:
o|    scheme#string->list
o|    scheme#string
o|    scheme#string=?	2
o|    scheme#list
o|    scheme#eof-object?
o|    ##sys#cons	11
o|    ##sys#list	10
o|    scheme#>
o|    ##sys#call-with-values	3
o|    scheme#-	2
o|    scheme#cddr
o|    scheme#string-length
o|    chicken.fixnum#fx<
o|    scheme#string-ref
o|    scheme#*	2
o|    scheme#cadr	2
o|    scheme#symbol?	2
o|    scheme#memq	15
o|    scheme#cdar	7
o|    scheme#caar	3
o|    scheme#assq
o|    scheme#length	3
o|    scheme#eq?	50
o|    scheme#not	17
o|    scheme#null?	15
o|    scheme#car	12
o|    scheme#apply	2
o|    scheme#cdr	5
o|    scheme#cons	52
o|    ##sys#setslot	20
o|    ##sys#check-list	33
o|    scheme#pair?	44
o|    ##sys#slot	95
o|contracted procedure: k2976 
o|contracted procedure: k2979 
o|contracted procedure: k2991 
o|contracted procedure: k3007 
o|contracted procedure: k3015 
o|contracted procedure: k3022 
o|contracted procedure: k3046 
o|contracted procedure: k3063 
o|contracted procedure: k3085 
o|contracted procedure: k3081 
o|contracted procedure: k3066 
o|contracted procedure: k3069 
o|contracted procedure: k3077 
o|contracted procedure: k3092 
o|contracted procedure: k3100 
o|contracted procedure: k3112 
o|contracted procedure: k3134 
o|contracted procedure: k3130 
o|contracted procedure: k3115 
o|contracted procedure: k3118 
o|contracted procedure: k3126 
o|contracted procedure: k3186 
o|contracted procedure: k3201 
o|contracted procedure: k3189 
o|contracted procedure: k3244 
o|contracted procedure: k3251 
o|contracted procedure: k3383 
o|contracted procedure: k3395 
o|contracted procedure: k3413 
o|contracted procedure: k3421 
o|contracted procedure: k4160 
o|contracted procedure: k4171 
o|contracted procedure: k4182 
o|contracted procedure: k4194 
o|contracted procedure: k4204 
o|contracted procedure: k4208 
o|propagated global variable: g782784 chicken.compiler.core#internal-bindings 
o|contracted procedure: k4217 
o|contracted procedure: k4227 
o|contracted procedure: k4231 
o|propagated global variable: g764766 chicken.compiler.core#extended-bindings 
o|contracted procedure: k4240 
o|contracted procedure: k4250 
o|contracted procedure: k4254 
o|propagated global variable: g746748 chicken.compiler.core#standard-bindings 
o|contracted procedure: k4272 
o|contracted procedure: k4299 
o|contracted procedure: k4325 
o|contracted procedure: k4337 
o|contracted procedure: k4347 
o|contracted procedure: k4351 
o|contracted procedure: k4314 
o|contracted procedure: k4367 
o|contracted procedure: k4394 
o|contracted procedure: k4409 
o|contracted procedure: k4416 
o|contracted procedure: k4419 
o|contracted procedure: k4432 
o|contracted procedure: k4435 
o|contracted procedure: k4448 
o|contracted procedure: k4451 
o|contracted procedure: k4464 
o|contracted procedure: k4473 
o|contracted procedure: k4476 
o|contracted procedure: k4487 
o|contracted procedure: k4511 
o|contracted procedure: k4507 
o|contracted procedure: k4503 
o|contracted procedure: k4517 
o|contracted procedure: k4524 
o|contracted procedure: k4530 
o|contracted procedure: k4534 
o|contracted procedure: k4540 
o|contracted procedure: k4546 
o|contracted procedure: k4550 
o|contracted procedure: k4556 
o|contracted procedure: k4560 
o|contracted procedure: k4566 
o|contracted procedure: k4588 
o|contracted procedure: k4592 
o|contracted procedure: k4598 
o|contracted procedure: k4602 
o|contracted procedure: k4608 
o|contracted procedure: k4612 
o|contracted procedure: k4618 
o|contracted procedure: k4622 
o|contracted procedure: k4636 
o|contracted procedure: k4642 
o|contracted procedure: k4648 
o|contracted procedure: k4654 
o|contracted procedure: k4660 
o|contracted procedure: k4666 
o|contracted procedure: k4672 
o|contracted procedure: k4678 
o|contracted procedure: k4684 
o|contracted procedure: k4723 
o|contracted procedure: k4729 
o|contracted procedure: k4735 
o|contracted procedure: k4741 
o|contracted procedure: k4747 
o|contracted procedure: k4753 
o|contracted procedure: k4759 
o|contracted procedure: k4765 
o|contracted procedure: k4771 
o|contracted procedure: k4777 
o|contracted procedure: k4783 
o|contracted procedure: k4789 
o|contracted procedure: k4795 
o|contracted procedure: k4801 
o|contracted procedure: k4807 
o|contracted procedure: k4813 
o|contracted procedure: k4819 
o|contracted procedure: k4825 
o|contracted procedure: k4831 
o|contracted procedure: k4916 
o|contracted procedure: k4895 
o|contracted procedure: k4903 
o|contracted procedure: k4909 
o|contracted procedure: k8209 
o|contracted procedure: k4941 
o|contracted procedure: k4951 
o|contracted procedure: k4957 
o|contracted procedure: k4973 
o|contracted procedure: k4988 
o|contracted procedure: k5021 
o|contracted procedure: k5105 
o|contracted procedure: k5117 
o|contracted procedure: k5127 
o|contracted procedure: k5131 
o|contracted procedure: k5137 
o|contracted procedure: k5151 
o|contracted procedure: k5157 
o|contracted procedure: k5163 
o|contracted procedure: k5166 
o|inlining procedure: k5142 
o|contracted procedure: k5183 
o|contracted procedure: k5186 
o|inlining procedure: k5142 
o|contracted procedure: k5228 
o|contracted procedure: k5248 
o|contracted procedure: k5297 
o|contracted procedure: k5343 
o|contracted procedure: k5349 
o|contracted procedure: k5356 
o|contracted procedure: k5362 
o|contracted procedure: k5389 
o|contracted procedure: k5393 
o|contracted procedure: k5414 
o|contracted procedure: k5556 
o|contracted procedure: k5565 
o|contracted procedure: k5572 
o|contracted procedure: k5598 
o|contracted procedure: k5609 
o|contracted procedure: k5638 
o|contracted procedure: k5703 
o|contracted procedure: k5768 
o|contracted procedure: k5798 
o|contracted procedure: k6016 
o|contracted procedure: k5959 
o|contracted procedure: k5973 
o|inlining procedure: "(batch-driver.scm:873) dribble1091" 
o|contracted procedure: k6098 
o|inlining procedure: "(batch-driver.scm:873) dribble1091" 
o|inlining procedure: "(batch-driver.scm:864) dribble1091" 
o|inlining procedure: "(batch-driver.scm:857) dribble1091" 
o|contracted procedure: k6139 
o|contracted procedure: k6129 
o|contracted procedure: k6146 
o|contracted procedure: k6156 
o|contracted procedure: k6159 
o|contracted procedure: k6171 
o|contracted procedure: k6174 
o|contracted procedure: k6177 
o|contracted procedure: k6185 
o|contracted procedure: k6193 
o|propagated global variable: g21632167 chicken.compiler.core#foreign-lambda-stubs 
o|inlining procedure: "(batch-driver.scm:833) dribble1091" 
o|contracted procedure: k6217 
o|contracted procedure: k6230 
o|inlining procedure: "(batch-driver.scm:768) dribble1091" 
o|contracted procedure: k6254 
o|contracted procedure: k6260 
o|contracted procedure: k6266 
o|contracted procedure: k6276 
o|contracted procedure: k6283 
o|contracted procedure: k6303 
o|contracted procedure: k6322 
o|contracted procedure: k6367 
o|contracted procedure: k6377 
o|contracted procedure: k6381 
o|contracted procedure: k6390 
o|contracted procedure: k6400 
o|contracted procedure: k6404 
o|contracted procedure: k64008817 
o|contracted procedure: k6418 
o|inlining procedure: "(batch-driver.scm:699) dribble1091" 
o|contracted procedure: k6430 
o|contracted procedure: k6442 
o|contracted procedure: k6452 
o|contracted procedure: k6456 
o|inlining procedure: "(batch-driver.scm:691) dribble1091" 
o|contracted procedure: k6477 
o|contracted procedure: k6489 
o|contracted procedure: k6499 
o|contracted procedure: k6503 
o|contracted procedure: k6513 
o|contracted procedure: k6516 
o|contracted procedure: k6528 
o|contracted procedure: k6550 
o|contracted procedure: k6546 
o|contracted procedure: k6531 
o|contracted procedure: k6534 
o|contracted procedure: k6542 
o|contracted procedure: k6575 
o|contracted procedure: k6578 
o|contracted procedure: k6593 
o|contracted procedure: k6596 
o|contracted procedure: k6599 
o|contracted procedure: k6607 
o|contracted procedure: k6615 
o|inlining procedure: "(batch-driver.scm:668) dribble1091" 
o|contracted procedure: k6638 
o|contracted procedure: k6676 
o|contracted procedure: k6688 
o|contracted procedure: k6698 
o|contracted procedure: k6702 
o|contracted procedure: k6673 
o|propagated global variable: g19261928 chicken.compiler.compiler-syntax#compiler-syntax-statistics 
o|contracted procedure: k6717 
o|contracted procedure: k6744 
o|contracted procedure: k6756 
o|contracted procedure: k6759 
o|contracted procedure: k6762 
o|contracted procedure: k6770 
o|contracted procedure: k6778 
o|contracted procedure: k6784 
o|contracted procedure: k6814 
o|contracted procedure: k6817 
o|contracted procedure: k6829 
o|contracted procedure: k6832 
o|contracted procedure: k6835 
o|contracted procedure: k6843 
o|contracted procedure: k6851 
o|contracted procedure: k3149 
o|contracted procedure: k3158 
o|contracted procedure: k3171 
o|contracted procedure: k6874 
o|contracted procedure: k6885 
o|contracted procedure: k6897 
o|contracted procedure: k6909 
o|contracted procedure: k6912 
o|contracted procedure: k6915 
o|contracted procedure: k6923 
o|contracted procedure: k6931 
o|contracted procedure: k6894 
o|propagated global variable: g18141818 chicken.compiler.core#import-libraries 
o|contracted procedure: k6938 
o|contracted procedure: k6956 
o|contracted procedure: k6963 
o|contracted procedure: k6971 
o|contracted procedure: k6996 
o|contracted procedure: k7006 
o|contracted procedure: k7015 
o|contracted procedure: k7037 
o|contracted procedure: k7033 
o|contracted procedure: k7018 
o|contracted procedure: k7021 
o|contracted procedure: k7029 
o|propagated global variable: g17771781 chicken.compiler.core#used-units 
o|contracted procedure: k7049 
o|contracted procedure: k7071 
o|contracted procedure: k6947 
o|contracted procedure: k6951 
o|contracted procedure: k7067 
o|contracted procedure: k7052 
o|contracted procedure: k7055 
o|contracted procedure: k7063 
o|propagated global variable: g17471751 chicken.compiler.core#immutable-constants 
o|contracted procedure: k7083 
o|contracted procedure: k7086 
o|contracted procedure: k7089 
o|contracted procedure: k7097 
o|contracted procedure: k7105 
o|contracted procedure: k7111 
o|contracted procedure: k7130 
o|contracted procedure: k7122 
o|contracted procedure: k7118 
o|contracted procedure: k7137 
o|contracted procedure: k7140 
o|contracted procedure: k7152 
o|contracted procedure: k7155 
o|contracted procedure: k7158 
o|contracted procedure: k7166 
o|contracted procedure: k7174 
o|inlining procedure: "(batch-driver.scm:596) dribble1091" 
o|inlining procedure: "(batch-driver.scm:573) dribble1091" 
o|contracted procedure: k7196 
o|contracted procedure: k7204 
o|contracted procedure: k7207 
o|contracted procedure: k7218 
o|contracted procedure: k7221 
o|contracted procedure: k7233 
o|contracted procedure: k7236 
o|contracted procedure: k7239 
o|contracted procedure: k7247 
o|contracted procedure: k7255 
o|contracted procedure: k7267 
o|contracted procedure: k7270 
o|contracted procedure: k7273 
o|contracted procedure: k7281 
o|contracted procedure: k7289 
o|contracted procedure: k7295 
o|contracted procedure: k7329 
o|contracted procedure: k7336 
o|contracted procedure: k7352 
o|inlining procedure: "(batch-driver.scm:555) dribble1091" 
o|contracted procedure: k7404 
o|contracted procedure: k7371 
o|inlining procedure: "(batch-driver.scm:534) dribble1091" 
o|inlining procedure: "(batch-driver.scm:534) dribble1091" 
o|contracted procedure: k7394 
o|inlining procedure: "(batch-driver.scm:516) dribble1091" 
o|inlining procedure: "(batch-driver.scm:516) dribble1091" 
o|contracted procedure: k7416 
o|contracted procedure: k7443 
o|contracted procedure: k7458 
o|contracted procedure: k7470 
o|contracted procedure: k7492 
o|contracted procedure: k7488 
o|contracted procedure: k7473 
o|contracted procedure: k7476 
o|contracted procedure: k7484 
o|contracted procedure: k7498 
o|contracted procedure: k7510 
o|contracted procedure: k7506 
o|contracted procedure: k7502 
o|contracted procedure: k7516 
o|contracted procedure: k7531 
o|contracted procedure: k7534 
o|contracted procedure: k7537 
o|contracted procedure: k7545 
o|contracted procedure: k7553 
o|contracted procedure: k3272 
o|contracted procedure: k3298 
o|contracted procedure: k3278 
o|contracted procedure: k7569 
o|contracted procedure: k7579 
o|contracted procedure: k7583 
o|contracted procedure: k5592 
o|inlining procedure: "(batch-driver.scm:473) dribble1091" 
o|contracted procedure: k7592 
o|contracted procedure: k7602 
o|contracted procedure: k7606 
o|contracted procedure: k7625 
o|contracted procedure: k7635 
o|contracted procedure: k7639 
o|contracted procedure: k7652 
o|contracted procedure: k7668 
o|contracted procedure: k7682 
o|contracted procedure: k7694 
o|contracted procedure: k7704 
o|contracted procedure: k7708 
o|propagated global variable: g14301432 chicken.compiler.core#default-extended-bindings 
o|contracted procedure: k7717 
o|contracted procedure: k7727 
o|contracted procedure: k7731 
o|propagated global variable: g14111413 chicken.compiler.core#default-standard-bindings 
o|contracted procedure: k7749 
o|contracted procedure: k7762 
o|contracted procedure: k7768 
o|contracted procedure: k7780 
o|contracted procedure: k7783 
o|contracted procedure: k7786 
o|contracted procedure: k7794 
o|contracted procedure: k7802 
o|inlining procedure: "(batch-driver.scm:426) dribble1091" 
o|inlining procedure: "(batch-driver.scm:423) dribble1091" 
o|inlining procedure: "(batch-driver.scm:420) dribble1091" 
o|contracted procedure: k7847 
o|inlining procedure: "(batch-driver.scm:409) dribble1091" 
o|inlining procedure: "(batch-driver.scm:372) dribble1091" 
o|contracted procedure: k7980 
o|contracted procedure: k7984 
o|contracted procedure: k7991 
o|contracted procedure: k8006 
o|contracted procedure: k8028 
o|contracted procedure: k8024 
o|contracted procedure: k8009 
o|contracted procedure: k8012 
o|contracted procedure: k8020 
o|contracted procedure: k8034 
o|contracted procedure: k8046 
o|contracted procedure: k8070 
o|contracted procedure: k8073 
o|contracted procedure: k8076 
o|contracted procedure: k8084 
o|contracted procedure: k8092 
o|contracted procedure: k8055 
o|contracted procedure: k8141 
o|contracted procedure: k8144 
o|contracted procedure: k8147 
o|contracted procedure: k8155 
o|contracted procedure: k8163 
o|contracted procedure: k8184 
o|contracted procedure: k8217 
o|contracted procedure: k8228 
o|contracted procedure: k8221 
o|contracted procedure: k8236 
o|contracted procedure: k8253 
o|contracted procedure: k8259 
o|contracted procedure: k8265 
o|contracted procedure: k8268 
o|contracted procedure: k8281 
o|simplifications: ((if . 2) (let . 156)) 
o|removed binding forms: 379 
o|contracted procedure: k4354 
o|contracted procedure: k4381 
o|removed side-effect free assignment to unused variable: dribble1091 
(o x)|known list op on rest arg sublist: ##core#rest-null? _%rest12291271 1 
(o x)|known list op on rest arg sublist: ##core#rest-car _%rest12291271 1 
(o x)|known list op on rest arg sublist: ##core#rest-cdr _%rest12291271 1 
o|substituted constant variable: fstr11299305 
o|substituted constant variable: args11309306 
o|substituted constant variable: fstr11299311 
o|substituted constant variable: args11309312 
o|substituted constant variable: fstr11299317 
o|substituted constant variable: fstr11299323 
o|substituted constant variable: fstr11299331 
o|substituted constant variable: fstr11299337 
o|contracted procedure: "(batch-driver.scm:697) g20302037" 
o|substituted constant variable: fstr11299349 
o|contracted procedure: "(batch-driver.scm:687) g20082015" 
o|substituted constant variable: fstr11299357 
o|substituted constant variable: fstr11299369 
o|substituted constant variable: args11309370 
o|substituted constant variable: fstr11299393 
o|substituted constant variable: args11309394 
o|substituted constant variable: fstr11299399 
o|substituted constant variable: args11309400 
o|substituted constant variable: fstr11299413 
o|substituted constant variable: fstr11299419 
o|substituted constant variable: fstr11299425 
o|substituted constant variable: fstr11299431 
o|substituted constant variable: fstr11299437 
o|substituted constant variable: fstr11299451 
o|substituted constant variable: args11309452 
o|substituted constant variable: fstr11299467 
o|substituted constant variable: args11309468 
o|substituted constant variable: fstr11299473 
o|substituted constant variable: args11309474 
o|substituted constant variable: fstr11299479 
o|substituted constant variable: args11309480 
o|substituted constant variable: fstr11299485 
o|substituted constant variable: args11309486 
o|substituted constant variable: fstr11299491 
o|substituted constant variable: args11309492 
o|replaced variables: 22 
o|removed binding forms: 4 
o|replaced variables: 3 
o|removed binding forms: 59 
o|inlining procedure: k6492 
o|removed binding forms: 2 
o|removed binding forms: 1 
o|direct leaf routine/allocation: loop326 0 
o|direct leaf routine/allocation: a3776 0 
o|contracted procedure: k3031 
o|converted assignments to bindings: (loop326) 
o|contracted procedure: "(mini-srfi-1.scm:82) k3167" 
o|simplifications: ((let . 1)) 
o|removed binding forms: 2 
o|direct leaf routine/allocation: loop313 0 
o|converted assignments to bindings: (loop313) 
o|simplifications: ((let . 1)) 
x|eliminated type checks:
x|  C_i_check_list_2:	1
o|customizable procedures: (k4928 k4931 k8240 k8247 k8213 g10231024 map-loop10301050 k4968 k5369 k5372 map-loop12811299 map-loop13091327 k5420 k5423 k5429 k5432 k5435 k5438 k5441 k5444 k5447 k5450 k5453 k5456 k5459 k5462 k5465 k5468 k5471 k5474 k5477 k5480 k5485 k5490 k5495 k5500 k5505 map-loop13761393 k5532 k5535 k5538 k5541 k5544 for-each-loop14041416 for-each-loop14231435 for-each-loop11031451 for-each-loop11131466 for-each-loop14741487 loop352 chicken.compiler.batch-driver#append-map map-loop14971514 k5619 map-loop15261547 k5626 arg-val1096 k5651 k5679 loop1650 doloop15851586 map-loop15901607 map-loop16161633 map-loop16651682 k5759 k5795 g16991708 map-loop16931725 map-loop17351756 map-loop17651786 k6978 map-loop18021820 chicken.compiler.batch-driver#filter map-loop18501867 map-loop18881905 for-each-loop19191939 print-expr1095 map-loop19481965 chicken.compiler.batch-driver#initialize-analysis-database map-loop19811998 chicken.compiler.batch-driver#concatenate for-each-loop20072021 for-each-loop20292041 collect-options1097 for-each-loop20522066 for-each-loop20732084 k5869 map-loop21512168 print-db1094 print-node1093 analyze1100 begin-time1098 end-time1099 loop2106 def-no12301269 def-contf12311267 body12281237 g12091210 option-arg992 loop1201 for-each-loop11651177 chicken.compiler.batch-driver#display-analysis-database print-header1092 k4260 k4490 k4569 loop806 for-each-loop958975 for-each-loop739750 for-each-loop757768 for-each-loop775786 foldr389392 g394395 loop345 map-loop257275 map-loop284302 loop253 foldr242245 g247248) 
o|calls to known targets: 392 
o|identified direct recursive calls: f_2986 1 
o|identified direct recursive calls: f_3181 1 
o|identified direct recursive calls: f_3058 1 
o|identified direct recursive calls: f_3107 1 
o|identified direct recursive calls: f_3239 1 
o|identified direct recursive calls: f_3390 1 
o|identified direct recursive calls: f_6523 1 
o|identified direct recursive calls: f_3147 1 
o|identified direct recursive calls: f_7010 1 
o|identified direct recursive calls: f_7044 1 
o|identified direct recursive calls: f_3267 2 
o|fast box initializations: 54 
o|fast global references: 10 
o|fast global assignments: 5 
o|dropping unused closure argument: f_2971 
o|dropping unused closure argument: f_3181 
o|dropping unused closure argument: f_3233 
o|dropping unused closure argument: f_3381 
o|dropping unused closure argument: f_4890 
o|dropping unused closure argument: f_5013 
o|dropping unused closure argument: f_5135 
*/
/* end of file */
