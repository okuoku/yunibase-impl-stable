# Stable sources
register_source(CHICKEN STABLE
    "chicken" # Path
    STATIC    # Say "STATIC here.
    "4.13.0"  # Tag (free-format for STAIC sources)
    )

register_source(CHICKEN5 STABLE
    "chicken5"
    STATIC
    "5.2.0")

register_source(GAUCHE STABLE
    "gauche"
    STATIC
    "0.9.15")

register_source(MOSH STABLE
    "mosh"
    STATIC
    "0.2.8-rc5-0c912605fab6abac4cc0245f950bab944cc5bb32")

register_source(SAGITTARIUS STABLE
    "sagittarius"
    STATIC
    "0.9.9")

register_source(GAMBIT STABLE
    "gambit"
    STATIC
    "4.9.5")

register_source(MIT_SCHEME STABLE
    "mit-scheme-amd64"
    STATIC
    "11.0.90")

register_source(CYCLONE STABLE
    "cyclone"
    STATIC
    "8e5071f1f3feba7a8b09b04cda0046547e41d839" # @@CYCLONE_HEAD@@
    )

register_source(SCM STABLE
    "scmslib"
    STATIC
    "5f4+3c1")

register_source(BIGLOO STABLE
    "bigloo"
    STATIC
    "4.5b-dev")

register_source(RACKET STABLE
    "racket"
    STATIC
    "v8.1-minimal-snapshot0626"
    )
