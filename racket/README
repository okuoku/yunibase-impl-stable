The Racket Programming Language
===============================

This is the
  Minimal Racket | All Platforms | Source
distribution for version 8.1.0.7 (20210625-492daf6e8f).
 
This distribution provides source for the Racket run-time system;
for build and installation instructions, see "src/README.txt".
(The distribution also includes the core Racket collections and any
installed packages in source form.)

The distribution has been configured so that when you install or
update packages, the package catalog at
  https://plt.cs.northwestern.edu/snapshots/20210625-492daf6e8f/catalog/
is consulted first.

The distribution has been configured so that the installation
name is
  snapshot
Multiple installations with this name share `user'-scoped packages,
which makes it easier to upgrade from such an installation to this one.
To avoid sharing (which is better for keeping multiple installations
active) use `raco pkg config -i --set name ...' to choose a different
name for this installation.

Visit http://racket-lang.org/ for more Racket resources.


License
-------

Racket is distributed under the MIT license and the Apache version 2.0
license, at your option.

The Racket runtime system includes components distributed under
other licenses. See "src/LICENSE.txt" for more information.

Racket packages that are included in the distribution have their own
licenses. See the package files in "pkgs" within "share" for more
information.
