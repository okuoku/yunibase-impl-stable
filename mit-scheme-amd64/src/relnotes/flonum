New flonum-related definitions:

. flo:radix - Floating-point radix as an integer.  This is essentially
  always 2 but useful for assertions if you want to future-proof code
  that assumes it with a noisy failure in case we ever violate the
  assumption.
. flo:radix. - Floating-point radix as a flonum.
. flo:precision - Number of digits in a significand, including the hidden
  one bit for normal binary floating-point numbers.  For binary64
  floating-point, this is 53.
. flo:error-bound - Greatest possible relative error in rounding to nearest.
. flo:log-error-bound = (log flo:error-bound)
. flo:ulp-of-one - Distance from 1 to next greater floating-point number.
  Equal to twice flo:error-bound.
. flo:log-ulp-of-one = (log flo:ulp-of-one)
. (flo:ulp x) - Distance from x to the next floating-point number
  larger in magnitude with the same sign.
. flo:normal-exponent-max - Exponent of greatest integer power of
  flo:radix that is a finite floating-point number, as an exact
  integer.  Note that there are floating-point numbers greater than
  (expt flo:radix. flo:normal-exponent-max) -- the greatest one is just
  below flo:radix times that.  But there are none that are a greater
  _integer_ power of flo:radix.
. flo:normal-exponent-min - Exponent of least positive integer power of
  flo:radix that is a normal floating-point number, as an exact
  integer.  (expt flo:radix. flo:normal-exponent-min) is also named
  flo:smallest-positive-normal.
. flo:subnormal-exponent-min - Exponent of least positive integer power
  of flo:radix that is a nonzero, subnormal floating-point number, as
  an exact integer.  (expt flo:radix. flo:subnormal-exponent-min) is
  the least positive floating-point number, also named
  flo:smallest-positive-subnormal.
. flo:smallest-positive-subnormal - Smallest positive subnormal.
. flo:smallest-positive-normal - Smallest positive normal.
. flo:largest-positive-normal - Largest positive normal.
. flo:least-subnormal-exponent-base-2 - Least flonum input x for which
  (expt 2. x) gives a nonzero result.
. flo:least-subnormal-exponent-base-e - Least flonum input x for which
  (exp x) gives a nonzero result.
. flo:least-subnormal-exponent-base-10 - Least flonum input x for which
  (expt 10. x) gives a nonzero result.
. flo:least-normal-exponent-base-2 - Least flonum input x for which
  (expt 2. x) gives a normal result.
. flo:least-normal-exponent-base-e - Least flonum input x for which
  (exp x) gives a normal result.
. flo:least-normal-exponent-base-10 - Least flonum input x for which
  (expt 10. x) gives a normal result.
. flo:greatest-normal-exponent-base-2 - Greatest flonum input x for
  which (expt 2. x) gives a finite result.
. flo:greatest-normal-exponent-base-e - Greatest flonum input x for
  which (exp x) gives a finite result.
. flo:greatest-normal-exponent-base-10 - Greatest flonum input x for
  which (expt 10. x) gives a finite result.
. (flo:ldexp x e) = x * 2^e
. (flo:scalbn x e) = x * b^e, where b is flo:radix and e is an integer
. (flo:logb y) = e such that y = x * b^e for x = (flo:scalbn y (- e)),
  and 1 <= x < b, and e is an integer
. (flo:classify x) returns one of the symbols
  - zero
  - subnormal
  - normal
  - infinite
  - nan
. (flo:subnormal? x) is true if x is subnormal; false if zero, normal,
  infinite, or NaN
- (flo:safe-zero? x) is true if x is zero; false if subnormal, normal,
  infinite, or NaN.  flo:zero? raises an invalid-operation exception on
  NaN; flo:safe-zero? never does even on signalling NaN.
- (flo:sign-negative? x) returns true if x has negative sign, false if
  x has positive sign.  Note that (flo:negative? 0.) and (flo:negative?
  -0.) both return false, while (flo:sign-negative? -0.) returns true.
  Also, flo:negative? raises invalid-operation exception on NaN, while
  flo:sign-negative? never does even on signalling NaN.
- (flo:safe< x y), (flo:safe<= x y), (flo:safe> x y), (flo:safe>= x y),
  (flo:safe= x y), (flo:safe<> x y), (flo:unordered? x y) perform
  unordered floating-point comparisons and, unlike flo:< &c., do not
  raise invalid-operation exceptions on quiet NaNs.  (However, they do
  raise invalid-operation exceptions on signalling NaNs.)
- (flo:<> x y) returns true if x is less or greater than y, false if
  equal or unordered, and raises invalid-operation exceptions on any
  NaNs.
- (flo:total< x y) is true if x < y in the total ordering defined in
  IEEE 754-2008 Sec. 5.10; (flo:total-order x y) is a three-way
  comparison, -1 if x < y, 0 if x = y, +1 if x > y.
- (flo:total-mag< x y) = (flo:total< (flo:abs x) (flo:abs y))
- (flo:total-order-mag x y) = (flo:total-order (flo:abs x) (flo:abs y))
- The eight min/max operations of IEEE 754-2019 are now supported:
  . flo:min
  . flo:max
  . flo:min-mag
  . flo:max-mag
  . flo:min-num
  . flo:max-num
  . flo:min-mag-num
  . flo:max-mag-num
  The `-mag' variants compare magnitudes.  The `-num' variants treat
  NaN as missing data -- if one operand is numeric and the other is
  NaN, they return the numeric operand and ignore the NaN, as the
  minNum/maxNum operations from IEEE 754-2008 did.
- (flo:make-nan negative? quiet? payload) returns a NaN with the
  specified payload, which must not exceed (- (expt 2
  flo:significand-digits-base-2 2) 1).
- (flo:nan-quiet? nan) returns true for quiet NaNs, false for
  signalling NaNs.
- (flo:nan-payload nan) returns the payload of NaN, an integer between
  0 (inclusive) and 2^51 (exclusive).
