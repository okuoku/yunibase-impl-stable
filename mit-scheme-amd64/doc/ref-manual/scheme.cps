\initial {!}
\entry{! in mutation procedure names}{10}
\initial {"}
\entry{" as external representation}{109}
\initial {#}
\entry{# as format parameter}{235}
\entry{# in external representation of number}{66}
\entry{#( as external representation}{149}
\entry{#* as external representation}{153}
\entry{#[ as external representation}{236}
\entry{#\backslashchar {} as external representation}{101}
\entry{#| as external representation}{11}
\entry{#b as external representation}{66}
\entry{#d as external representation}{66}
\entry{#e as external representation}{66}
\entry{#f as external representation}{157}
\entry{#i as external representation}{66}
\entry{#o as external representation}{66}
\entry{#t as external representation}{157}
\entry{#x as external representation}{66}
\initial {'}
\entry{' as external representation}{24}
\initial {(}
\entry{( as external representation}{131}
\initial {)}
\entry{) as external representation}{131}
\initial {,}
\entry{, as external representation}{25}
\entry{,\@ as external representation}{25}
\initial {-}
\entry{-| notational convention}{4}
\entry{-ci, in string procedure name}{109}
\initial {.}
\entry{. as external representation}{131}
\entry{... in entries}{5}
\initial {;}
\entry{; as external representation}{11}
\initial {=}
\entry{=> in cond clause}{27}
\entry{=> notational convention}{4}
\initial {?}
\entry{? in predicate names}{10}
\initial {[}
\entry{[ in entries}{5}
\initial {]}
\entry{] in entries}{5}
\initial {`}
\entry{` as external representation}{25}
\initial {{\indexbackslash}}
\entry{\backslashchar {} as escape character in string}{109}
\initial {1}
\entry{1D table (defn)}{173}
\initial {A}
\entry{absolute pathname (defn)}{274}
\entry{absolute value, of number}{68}
\entry{access time, of file}{279}
\entry{access, used with set!}{24}
\entry{addition, of numbers}{68}
\entry{address hashing}{185}
\entry{alias}{42, 47}
\entry{alist (defn)}{171}
\entry{alphabetic case, of interned symbol}{158}
\entry{alphabetic case-insensitivity of programs (defn)}{10}
\entry{anonymous syntactic keyword}{37}
\entry{apostrophe, as external representation}{24}
\entry{appending, of bit strings}{154}
\entry{appending, of lists}{138}
\entry{appending, of symbols}{160}
\entry{appending, to output file}{220}
\entry{application hook (defn)}{205, 210}
\entry{application, of procedure}{205}
\entry{apply hook (defn)}{210}
\entry{argument evaluation order}{14}
\entry{arity}{206}
\entry{ASCII character}{104}
\entry{assignment}{24}
\entry{association list (defn)}{171}
\entry{association table (defn)}{174}
\entry{asterisk, as external representation}{153}
\entry{attribute, of file}{280}
\initial {B}
\entry{backquote, as external representation}{25}
\entry{backslash, as escape character in string}{109}
\entry{Backtracking, in parser language}{247}
\entry{balanced binary trees}{187, 191}
\entry{barrier, reference}{170}
\entry{bell, ringing on console}{230}
\entry{binary port (defn)}{217}
\entry{binary trees}{187, 191}
\entry{binary trees, as discrete maps}{191}
\entry{binary trees, as sets}{191}
\entry{binding expression (defn)}{7}
\entry{binding expression, dynamic}{20}
\entry{binding expression, lexical}{17}
\entry{binding, of variable}{6}
\entry{binding, syntactic keyword}{213}
\entry{binding, unassigned}{213}
\entry{binding, variable}{213}
\entry{bit string (defn)}{153}
\entry{bit string index (defn)}{153}
\entry{bit string length (defn)}{153}
\entry{bitless character}{105}
\entry{bitmaps}{336}
\entry{bitmaps, graphics}{328}
\entry{bitwise-logical operations, on fixnums}{82}
\entry{block structure}{17}
\entry{blocking mode, of port}{232}
\entry{BOA constructor}{31}
\entry{BOA constructor (defn)}{33}
\entry{body, of special form (defn)}{5}
\entry{boolean object}{8}
\entry{boolean object (defn)}{157}
\entry{boolean object, equivalence predicate}{157}
\entry{bound variable (defn)}{6}
\entry{bound-restarts}{311, 313}
\entry{bracket, in entries}{5}
\entry{broken ephemeron}{169}
\entry{bucky bit, of character (defn)}{104}
\entry{bucky bit, prefix (defn)}{101}
\entry{buffering, of graphics output}{327}
\entry{buffering, of output}{228}
\entry{built-in procedure}{205}
\entry{bytevector, input and output ports}{223}
\initial {C}
\entry{call by need evaluation (defn)}{164}
\entry{car field, of pair (defn)}{131}
\entry{case clause}{27}
\entry{case conversion, of character}{103}
\entry{case folding, of character}{103}
\entry{case sensitivity, of string operations}{109}
\entry{case, of interned symbol}{158}
\entry{case-insensitivity of programs (defn)}{10}
\entry{cdr field, of pair (defn)}{131}
\entry{cell (defn)}{162}
\entry{character (defn)}{101}
\entry{character bits (defn)}{104}
\entry{character code (defn)}{104}
\entry{character set}{107}
\entry{character, bitless}{105}
\entry{character, input from port}{224}
\entry{character, input from textual port}{241}
\entry{character, output to textual port}{243}
\entry{character, searching string for}{121}
\entry{characters, special, in programs}{11}
\entry{child, of environment (defn)}{6}
\entry{circle, graphics}{335}
\entry{circles, drawing}{331, 332}
\entry{circular list}{136, 146}
\entry{circular structure}{61}
\entry{clause, of case expression}{27}
\entry{clause, of cond expression}{26}
\entry{clearing the console screen}{230}
\entry{client socket}{295}
\entry{clip rectangle, graphics (defn)}{328}
\entry{clipping, of graphics}{328}
\entry{closing environment, of procedure (defn)}{15}
\entry{closing, of file port}{221}
\entry{closing, of port}{219}
\entry{code point}{105}
\entry{code, of character (defn)}{104}
\entry{code-point list}{107}
\entry{code-point range}{107}
\entry{color}{335}
\entry{combination (defn)}{14}
\entry{comma, as external representation}{25}
\entry{comment, extended, in programs (defn)}{11}
\entry{comment, in programs (defn)}{11}
\entry{comparison predicate}{50}
\entry{comparison, for equivalence}{57}
\entry{comparison, of bit strings}{155}
\entry{comparison, of boolean objects}{157}
\entry{comparison, of characters}{102}
\entry{comparison, of numbers}{67}
\entry{comparison, of XML names}{259}
\entry{compiled, procedure type}{205}
\entry{component selection, of bit string}{154}
\entry{component selection, of cell}{162}
\entry{component selection, of character}{105}
\entry{component selection, of ephemeron}{169}
\entry{component selection, of list}{136}
\entry{component selection, of pair}{132}
\entry{component selection, of stream}{167}
\entry{component selection, of vector}{150}
\entry{component selection, of weak pair}{169}
\entry{components, of pathname}{267}
\entry{compound procedure}{205}
\entry{cond clause}{26}
\entry{condition (defn)}{310}
\entry{condition handler (defn)}{303}
\entry{condition instance (defn)}{310}
\entry{condition signalling (defn)}{300}
\entry{condition type}{300, 313}
\entry{conditional expression (defn)}{26}
\entry{console, clearing}{230}
\entry{console, port}{219}
\entry{console, ringing the bell}{230}
\entry{constant}{9}
\entry{constant expression (defn)}{13}
\entry{constant, and quasiquote}{25}
\entry{constant, and quote}{24}
\entry{construction, of bit string}{153}
\entry{construction, of cell}{162}
\entry{construction, of character}{104}
\entry{construction, of circular list}{146}
\entry{construction, of continuation}{208}
\entry{construction, of EOF object}{225}
\entry{construction, of ephemeron}{169}
\entry{construction, of hash table}{175}
\entry{construction, of list}{134}
\entry{construction, of pair}{132}
\entry{construction, of pathname}{266, 269}
\entry{construction, of procedure}{15}
\entry{construction, of promise}{164}
\entry{construction, of stream}{166}
\entry{construction, of symbols}{159}
\entry{construction, of textual port type}{240}
\entry{construction, of vector}{149}
\entry{construction, of weak pair}{168}
\entry{continuation}{208}
\entry{continuation, alternate invocation}{209}
\entry{continuation, and dynamic binding}{21}
\entry{control, bucky bit prefix (defn)}{101}
\entry{conventions for error messages}{302}
\entry{conventions, lexical}{9}
\entry{conventions, naming}{10}
\entry{conventions, notational}{4}
\entry{conversion, pathname to string}{266, 272}
\entry{cooked mode, of terminal port}{233}
\entry{coordinates, graphics}{324}
\entry{copying, of alist}{173}
\entry{copying, of bit string}{153}
\entry{copying, of file}{276}
\entry{copying, of tree}{134}
\entry{copying, of vector}{149}
\entry{current environment}{216}
\entry{current environment (defn)}{7}
\entry{current error port (defn)}{218}
\entry{current input port (defn)}{218}
\entry{current input port, rebinding}{220}
\entry{current interaction port (defn)}{218}
\entry{current notification port (defn)}{218}
\entry{current output port (defn)}{218}
\entry{current output port, rebinding}{220}
\entry{current tracing output port (defn)}{218}
\entry{current working directory}{265}
\entry{current working directory (defn)}{274}
\entry{cursor, graphics (defn)}{325}
\entry{custom operations, on graphics device}{328}
\entry{custom operations, on textual port}{239}
\entry{cutting, of bit string}{154}
\entry{cutting, of list}{137}
\entry{cutting, of vector}{151}
\initial {D}
\entry{d, as exponent marker in number}{66}
\entry{decoded time}{282}
\entry{default environment, floating-point}{91}
\entry{default object (defn)}{15}
\entry{defaulting, of pathname}{271}
\entry{define, procedure (defn)}{22}
\entry{defining foreign procedures}{341}
\entry{defining foreign types}{338}
\entry{definition}{22}
\entry{definition, internal}{23}
\entry{definition, internal (defn)}{22}
\entry{definition, top-level}{23}
\entry{definition, top-level (defn)}{22}
\entry{deletion, of alist element}{172}
\entry{deletion, of file}{276}
\entry{deletion, of list element}{139}
\entry{delimiter, in programs (defn)}{10}
\entry{denormal}{84}
\entry{device coordinates, graphics (defn)}{324}
\entry{device, pathname component}{267}
\entry{difference, of numbers}{68}
\entry{directive, format (defn)}{234}
\entry{directory path (defn)}{268}
\entry{directory, converting pathname to}{273}
\entry{directory, current working (defn)}{274}
\entry{directory, pathname component}{267}
\entry{directory, predicate for}{277}
\entry{directory, reading}{273, 281}
\entry{discrete maps, using binary trees}{191}
\entry{discretionary flushing, of buffered output}{228}
\entry{disembodied property list}{158}
\entry{display, clearing}{230}
\entry{display, X graphics}{330}
\entry{divide-by-zero exception}{93}
\entry{division, of integers}{68}
\entry{division, of numbers}{68}
\entry{DLL, DIBUTILS.DLL}{343}
\entry{DLL, exports}{345}
\entry{DLL, GDI32.DLL}{341}
\entry{DLL, KERNEL32.DLL}{341}
\entry{DLL, loading}{340}
\entry{DLL, USER32.DLL}{341}
\entry{dot, as external representation}{131}
\entry{dotted notation, for pair (defn)}{131}
\entry{dotted pair (see pair)}{131}
\entry{double precision, of inexact number}{66}
\entry{double quote, as external representation}{109}
\entry{drawing arcs and circles, graphics}{331, 332}
\entry{drawing mode, graphics (defn)}{326}
\entry{dynamic binding}{20, 304, 305}
\entry{dynamic binding, and continuations}{21}
\entry{dynamic binding, versus static scoping}{7}
\entry{dynamic environment}{20}
\entry{dynamic extent}{20}
\entry{dynamic parameter (defn)}{161}
\entry{dynamic types (defn)}{3}
\initial {E}
\entry{e, as exponent marker in number}{66}
\entry{effector, restart (defn)}{305}
\entry{element, of list (defn)}{131}
\entry{ellipse, graphics}{335}
\entry{ellipsis, in entries}{5}
\entry{else clause, of case expression (defn)}{27}
\entry{else clause, of cond expression (defn)}{26}
\entry{empty list (defn)}{131}
\entry{empty list, external representation}{131}
\entry{empty list, predicate for}{136}
\entry{empty stream, predicate for}{167}
\entry{empty string, predicate for}{116}
\entry{end of file object (see EOF object)}{225}
\entry{end, of subvector (defn)}{149}
\entry{entity (defn)}{210}
\entry{entry format}{5}
\entry{environment (defn)}{6}
\entry{environment, current}{216}
\entry{environment, current (defn)}{7}
\entry{environment, extension (defn)}{6}
\entry{environment, initial (defn)}{7}
\entry{environment, interpreter}{216}
\entry{environment, of procedure}{15}
\entry{environment, procedure closing (defn)}{15}
\entry{environment, procedure invocation (defn)}{15}
\entry{environment, top-level}{216}
\entry{EOF object, construction}{225}
\entry{EOF object, predicate for}{225}
\entry{ephemerally held data, of hash table}{176}
\entry{ephemerally held keys, of hash table}{176}
\entry{ephemeron (defn)}{169}
\entry{ephemeron, broken}{169}
\entry{equality, of XML names}{259}
\entry{equivalence predicate (defn)}{57}
\entry{equivalence predicate, for bit strings}{155}
\entry{equivalence predicate, for boolean objects}{157}
\entry{equivalence predicate, for characters}{102}
\entry{equivalence predicate, for fixnums}{82}
\entry{equivalence predicate, for flonums}{84}
\entry{equivalence predicate, for numbers}{67}
\entry{equivalence predicate, for pathname host}{273}
\entry{equivalence predicate, for pathnames}{270}
\entry{equivalence predicate, of hash table}{176}
\entry{error messages, conventions}{302}
\entry{error port, current (defn)}{218}
\entry{error, in examples}{4}
\entry{error, unassigned variable}{6}
\entry{error, unbound variable (defn)}{6}
\entry{error--> notational convention}{4}
\entry{errors, notational conventions}{4}
\entry{escape character, for string}{109}
\entry{escape procedure (defn)}{208}
\entry{escape procedure, alternate invocation}{209}
\entry{evaluation order, of arguments}{14}
\entry{evaluation, call by need (defn)}{164}
\entry{evaluation, in examples}{4}
\entry{evaluation, lazy (defn)}{164}
\entry{evaluation, of s-expression}{215}
\entry{even number}{67}
\entry{exactness}{64}
\entry{examples}{4}
\entry{existence, testing of file}{276}
\entry{exit, non-local}{208}
\entry{explicit renaming}{49}
\entry{exponent marker (defn)}{66}
\entry{expression (defn)}{12}
\entry{expression, binding (defn)}{7}
\entry{expression, conditional (defn)}{26}
\entry{expression, constant (defn)}{13}
\entry{expression, input from port}{223}
\entry{expression, iteration (defn)}{29}
\entry{expression, literal (defn)}{13}
\entry{expression, procedure call (defn)}{13}
\entry{expression, special form (defn)}{13}
\entry{extended comment, in programs (defn)}{11}
\entry{extended real line}{84}
\entry{extension, of environment (defn)}{6}
\entry{extent, of dynamic binding (defn)}{21}
\entry{extent, of objects}{3}
\entry{external representation (defn)}{8}
\entry{external representation, and quasiquote}{25}
\entry{external representation, and quote}{24}
\entry{external representation, for bit string}{153}
\entry{external representation, for empty list}{131}
\entry{external representation, for list}{131}
\entry{external representation, for number}{66}
\entry{external representation, for pair}{131}
\entry{external representation, for procedure}{205}
\entry{external representation, for string}{109}
\entry{external representation, for symbol}{158}
\entry{external representation, for vector}{149}
\entry{external representation, parsing}{223}
\entry{extra object, of application hook}{210}
\initial {F}
\entry{f, as exponent marker in number}{66}
\entry{false, boolean object}{8}
\entry{false, boolean object (defn)}{157}
\entry{false, in conditional expression (defn)}{26}
\entry{false, predicate for}{157}
\entry{FDL, GNU Free Documentation License}{361}
\entry{file (regular), predicate for}{277}
\entry{file name}{265}
\entry{file time}{282}
\entry{file type, procedure for}{278}
\entry{file, converting pathname directory to}{273}
\entry{file, end-of-file marker (see EOF object)}{225}
\entry{file, input and output ports}{219}
\entry{filename (defn)}{265}
\entry{filling, of bit string}{155}
\entry{filling, of vector}{152}
\entry{filtering, of list}{139}
\entry{fixnum (defn)}{81}
\entry{floating-point comparison, ordered}{84}
\entry{floating-point comparison, unordered}{84}
\entry{floating-point environment}{91}
\entry{floating-point environment, default}{91}
\entry{floating-point number, infinite}{84}
\entry{floating-point number, normal}{83}
\entry{floating-point number, not a number}{84}
\entry{floating-point number, subnormal}{84}
\entry{floating-point number, zero}{84}
\entry{flonum (defn)}{83}
\entry{flushing, of buffered output}{228}
\entry{folding, of list}{144}
\entry{forcing, of promise}{164}
\entry{foreign type declarations}{338}
\entry{form}{42}
\entry{form, special (defn)}{13}
\entry{formal parameter list, of lambda (defn)}{15}
\entry{format directive (defn)}{234}
\entry{format, entry}{5}
\initial {G}
\entry{generalization, of condition types}{300, 301, 313, 314}
\entry{generalization, of condition types (defn)}{299}
\entry{gensym (see uninterned symbol)}{160}
\entry{geometry string, X graphics}{331}
\entry{grapheme cluster}{113}
\entry{graphics}{323}
\entry{graphics, bitmaps}{328}
\entry{graphics, buffering of output}{327}
\entry{graphics, circle}{335}
\entry{graphics, clipping}{328}
\entry{graphics, coordinate systems}{324}
\entry{graphics, cursor (defn)}{325}
\entry{graphics, custom operations}{328}
\entry{graphics, device coordinates (defn)}{324}
\entry{graphics, drawing}{324}
\entry{graphics, drawing arcs and circles}{331, 332}
\entry{graphics, drawing mode (defn)}{326}
\entry{graphics, ellipse}{335}
\entry{graphics, images}{328}
\entry{graphics, line style (defn)}{326}
\entry{graphics, opening and closing devices}{323}
\entry{graphics, output characteristics}{326}
\entry{graphics, virtual coordinates (defn)}{324}
\entry{greatest common divisor, of numbers}{70}
\entry{growing, of vector}{150}
\initial {H}
\entry{handler, condition (defn)}{303}
\entry{hard linking, of file}{276}
\entry{hash table}{175}
\entry{hashing, of key in hash table}{178}
\entry{hashing, of object}{186}
\entry{hashing, of string}{116}
\entry{hashing, of symbol}{161}
\entry{home directory, as pathname}{273}
\entry{hook, application (defn)}{205}
\entry{host, in filename}{265}
\entry{host, pathname component}{267}
\entry{hostname, TCP}{295}
\entry{hygienic}{37}
\entry{hyper, bucky bit prefix (defn)}{101}
\initial {I}
\entry{I/O, to bytevectors}{223}
\entry{I/O, to files}{219}
\entry{I/O, to strings}{221}
\entry{identifier}{42}
\entry{identifier (defn)}{10}
\entry{identity, additive}{68}
\entry{identity, multiplicative}{68}
\entry{images, graphics}{328}
\entry{immutable}{9}
\entry{immutable string}{110}
\entry{implementation restriction}{64}
\entry{implicit begin}{29}
\entry{improper list (defn)}{131}
\entry{index, of bit string (defn)}{153}
\entry{index, of list (defn)}{137}
\entry{index, of string (defn)}{109}
\entry{index, of subvector (defn)}{149}
\entry{index, of vector (defn)}{149}
\entry{inexact-result exception}{92}
\entry{infinity (\code {+inf.0}, \code {-inf.0})}{84}
\entry{inheritance, of environment bindings (defn)}{6}
\entry{initial environment (defn)}{7}
\entry{initial size, of hash table}{184}
\entry{input}{217}
\entry{input form}{43}
\entry{input form, to macro}{49}
\entry{input operations}{223}
\entry{input port (defn)}{217}
\entry{input port, bytevector}{223}
\entry{input port, console}{219}
\entry{input port, current (defn)}{218}
\entry{input port, file}{219}
\entry{input port, string}{221}
\entry{input, XML}{256}
\entry{insensitivity, to case in programs (defn)}{10}
\entry{installed, as pathname component}{269}
\entry{instance, of condition (defn)}{310}
\entry{integer division}{68}
\entry{integer, converting to bit string}{156}
\entry{interaction port, current (defn)}{218}
\entry{interactive input ports (defn)}{223}
\entry{internal definition}{23}
\entry{internal definition (defn)}{22}
\entry{internal representation, for character}{104}
\entry{internal representation, for inexact number}{66}
\entry{interned symbol (defn)}{158}
\entry{interning, of symbols}{159}
\entry{interpreted, procedure type}{205}
\entry{interpreter environment}{216}
\entry{invalid-operation exception}{84, 93}
\entry{inverse, additive, of number}{68}
\entry{inverse, multiplicative, of number}{68}
\entry{inverse, of bit string}{155}
\entry{inverse, of boolean object}{157}
\entry{invocation environment, of procedure (defn)}{15}
\entry{iteration expression (defn)}{29}
\initial {J}
\entry{joiner procedure, of strings}{117}
\entry{joining, of strings}{117}
\initial {K}
\entry{key, of association list element (defn)}{171}
\entry{keyword}{37}
\entry{keyword binding}{213}
\entry{keyword constructor}{31}
\entry{keyword constructor (defn)}{34}
\entry{keyword, of special form (defn)}{13}
\initial {L}
\entry{l, as exponent marker in number}{66}
\entry{lambda expression (defn)}{15}
\entry{lambda list (defn)}{15}
\entry{lambda, implicit in define}{22}
\entry{lambda, implicit in let}{17}
\entry{latent types (defn)}{3}
\entry{lazy evaluation (defn)}{164}
\entry{least common multiple, of numbers}{70}
\entry{length, of bit string}{154}
\entry{length, of bit string (defn)}{153}
\entry{length, of list (defn)}{131}
\entry{length, of stream}{167}
\entry{length, of string (defn)}{109}
\entry{length, of vector (defn)}{149}
\entry{letrec, implicit in define}{23}
\entry{lexical binding expression}{17}
\entry{lexical conventions}{9}
\entry{lexical scoping (defn)}{7}
\entry{library, system pathname}{274}
\entry{limitations}{337}
\entry{line style, graphics (defn)}{326}
\entry{linking (hard), of file}{276}
\entry{linking (soft), of file}{276}
\entry{list (defn)}{131}
\entry{list index (defn)}{137}
\entry{list, association (defn)}{171}
\entry{list, converting to stream}{166}
\entry{list, converting to vector}{149}
\entry{list, external representation}{131}
\entry{list, improper (defn)}{131}
\entry{literal expression (defn)}{13}
\entry{literal, and quasiquote}{25}
\entry{literal, and quote}{24}
\entry{literal, identifier as}{10}
\entry{loading DLLs}{340}
\entry{local part, of XML name}{259}
\entry{location}{9}
\entry{location, of variable}{6}
\entry{log-odds}{76}
\entry{log-probability}{76}
\entry{logical operations, on fixnums}{82}
\entry{long precision, of inexact number}{66}
\entry{loopback interface}{298}
\entry{looping (see iteration expressions)}{29}
\entry{lowercase}{10}
\entry{lowercase, character conversion}{103}
\initial {M}
\entry{macro}{36}
\entry{macro keyword}{37}
\entry{macro transformer}{37, 43, 49}
\entry{macro use}{37}
\entry{magnitude, of real number}{68}
\entry{manifest types (defn)}{3}
\entry{mapping, of list}{141}
\entry{mapping, of stream}{167}
\entry{mapping, of vector}{150}
\entry{Matcher language}{249}
\entry{Matcher procedure}{249}
\entry{matching, of strings}{121}
\entry{maximum, of numbers}{68}
\entry{memoization, of promise}{164}
\entry{merging, of pathnames}{271}
\entry{meta, bucky bit prefix (defn)}{101}
\entry{minimum, of numbers}{68}
\entry{modification time, of file}{279}
\entry{modification, of bit string}{155}
\entry{modification, of vector}{152}
\entry{modulus, of hashing procedure}{178}
\entry{modulus, of integers}{68}
\entry{moving, of bit string elements}{155}
\entry{moving, of vector elements}{152}
\entry{multiple values, from procedure}{210}
\entry{multiplication, of numbers}{68}
\entry{must be, notational convention}{4}
\entry{mutable}{9}
\entry{mutable string}{110}
\entry{mutation procedure (defn)}{10}
\initial {N}
\entry{name, of file}{276}
\entry{name, of symbol}{159}
\entry{name, of value (defn)}{6}
\entry{name, pathname component}{267}
\entry{named lambda (defn)}{16}
\entry{named let (defn)}{29}
\entry{names, XML}{258}
\entry{naming conventions}{10, 342}
\entry{NaN}{84}
\entry{negative infinity (\code {-inf.0})}{84}
\entry{negative number}{67}
\entry{nesting, of quasiquote expressions}{25}
\entry{newest, as pathname component}{269}
\entry{NFC}{114}
\entry{NFD}{114}
\entry{non-local exit}{208}
\entry{normal floating-point number}{83}
\entry{Normalization Form C (NFC)}{114}
\entry{Normalization Form D (NFD)}{114}
\entry{not a number (NaN, \code {+nan.0})}{84}
\entry{notation, dotted (defn)}{131}
\entry{notational conventions}{4}
\entry{notification port, current (defn)}{218}
\entry{null string, predicate for}{116}
\entry{number}{63}
\entry{number, external representation}{66}
\entry{number, pseudorandom generation}{96}
\entry{numeric precision, inexact}{66}
\entry{numerical input and output}{78}
\entry{numerical operations}{66}
\entry{numerical types}{63}
\initial {O}
\entry{object hashing}{186}
\entry{odd number}{67}
\entry{oldest, as pathname component}{269}
\entry{one-dimensional table (defn)}{173}
\entry{operand, of procedure call (defn)}{13}
\entry{Operating-System Interface}{265}
\entry{operator, of procedure call (defn)}{13}
\entry{option, run-time-loadable}{234, 249, 256}
\entry{optional component, in entries}{5}
\entry{optional parameter (defn)}{15}
\entry{order, of argument evaluation}{14}
\entry{ordered comparison}{84}
\entry{ordering, of characters}{102}
\entry{ordering, of numbers}{67}
\entry{output}{217}
\entry{output form}{43}
\entry{output port (defn)}{217}
\entry{output port, bytevector}{223}
\entry{output port, console}{219}
\entry{output port, current (defn)}{218}
\entry{output port, file}{219}
\entry{output port, string}{221}
\entry{output procedures}{228}
\entry{output, XML}{257}
\entry{overflow exception}{93}
\initial {P}
\entry{padder procedure}{118}
\entry{padding, of string}{118}
\entry{pair (defn)}{131}
\entry{pair, external representation}{131}
\entry{pair, weak (defn)}{168}
\entry{parameter list, of lambda (defn)}{15}
\entry{parameter, dynamic (defn)}{161}
\entry{parameter, entry category}{5}
\entry{parameter, optional (defn)}{15}
\entry{parameter, required (defn)}{15}
\entry{parameter, rest (defn)}{15}
\entry{parent, of directory}{268}
\entry{parent, of environment (defn)}{6}
\entry{parenthesis, as external representation}{131, 149}
\entry{Parser buffer}{244}
\entry{Parser language}{247, 252}
\entry{Parser procedure}{252}
\entry{parser, XML}{256}
\entry{Parser-buffer pointer}{244}
\entry{parsing, of external representation}{223}
\entry{pasting, of bit strings}{154}
\entry{pasting, of lists}{137}
\entry{pasting, of symbols}{160}
\entry{path, directory (defn)}{268}
\entry{pathname}{265}
\entry{pathname (defn)}{265}
\entry{pathname components}{267}
\entry{pathname, absolute (defn)}{274}
\entry{pathname, relative (defn)}{274}
\entry{period, as external representation}{131}
\entry{physical size, of hash table (defn)}{183}
\entry{port}{217}
\entry{port (defn)}{217}
\entry{port number, TCP}{295}
\entry{port, bytevector}{223}
\entry{port, console}{219}
\entry{port, current}{218}
\entry{port, file}{219}
\entry{port, string}{221}
\entry{positive infinity (\code {+inf.0})}{84}
\entry{positive number}{67}
\entry{precision, of inexact number}{66}
\entry{predicate (defn)}{10, 57}
\entry{predicate, equivalence (defn)}{57}
\entry{prefix, of string}{124}
\entry{prefix, of XML name}{259}
\entry{pretty printer}{230}
\entry{primitive procedure (defn)}{205}
\entry{primitive, procedure type}{205}
\entry{print name, of symbol}{159}
\entry{printed output, in examples}{4}
\entry{printing graphics output}{336}
\entry{procedure}{205}
\entry{procedure call (defn)}{13}
\entry{procedure define (defn)}{22}
\entry{procedure, closing environment (defn)}{15}
\entry{procedure, compiled}{205}
\entry{procedure, compound}{205}
\entry{procedure, construction}{15}
\entry{procedure, entry format}{5}
\entry{procedure, escape (defn)}{208}
\entry{procedure, interpreted}{205}
\entry{procedure, invocation environment (defn)}{15}
\entry{procedure, of application hook}{210}
\entry{procedure, primitive}{205}
\entry{procedure, type}{205}
\entry{product, of numbers}{68}
\entry{promise (defn)}{164}
\entry{promise, construction}{164}
\entry{promise, forcing}{164}
\entry{prompting}{237}
\entry{proper tail recursion (defn)}{3}
\entry{property list}{171, 173, 174}
\entry{property list, of symbol}{158}
\entry{protocol, restart (defn)}{305}
\entry{pseudorandom number generation}{96}
\initial {Q}
\entry{qname, of XML name}{258}
\entry{quiet NaN}{84, 93}
\entry{quote, as external representation}{24}
\entry{quotient, of integers}{68}
\entry{quotient, of numbers}{68}
\entry{quoting}{24}
\initial {R}
\entry{R4RS}{3}
\entry{random number generation}{96}
\entry{rational, simplest (defn)}{71}
\entry{raw mode, of terminal port}{233}
\entry{record-type descriptor (defn)}{162}
\entry{recursion (see tail recursion)}{3}
\entry{red-black binary trees}{187}
\entry{reference barrier}{170}
\entry{reference, strong (defn)}{167}
\entry{reference, variable (defn)}{13}
\entry{reference, weak (defn)}{167}
\entry{referentially transparent}{37}
\entry{region of variable binding, do}{30}
\entry{region of variable binding, internal definition}{23}
\entry{region of variable binding, lambda}{15}
\entry{region of variable binding, let}{17}
\entry{region of variable binding, let*}{17}
\entry{region of variable binding, letrec}{18}
\entry{region, of variable binding (defn)}{7}
\entry{regular file, predicate for}{277}
\entry{rehash size, of hash table (defn)}{184}
\entry{rehash threshold, of hash table (defn)}{185}
\entry{relative pathname (defn)}{274}
\entry{remainder, of integers}{68}
\entry{renaming procedure}{49}
\entry{renaming, of file}{276}
\entry{REP loop}{301, 303, 304, 308}
\entry{REP loop (defn)}{7}
\entry{REP loop, environment of}{7}
\entry{representation, external (defn)}{8}
\entry{required parameter (defn)}{15}
\entry{resizing, of hash table}{183}
\entry{resources, X graphics}{330}
\entry{rest parameter (defn)}{15}
\entry{restart (defn)}{305}
\entry{restart effector (defn)}{305}
\entry{restart protocol}{305}
\entry{restarts, bound}{311, 313}
\entry{result of evaluation, in examples}{4}
\entry{result, unspecified (defn)}{4}
\entry{reversal, of list}{146}
\entry{ringing the console bell}{230}
\entry{root, as pathname component}{268}
\entry{run-time-loadable option}{234, 249, 256}
\entry{runtime system}{3}
\initial {S}
\entry{s, as exponent marker in number}{66}
\entry{s-expression}{215}
\entry{scalar value}{105}
\entry{scheme concepts}{6}
\entry{Scheme standard}{3}
\entry{scope (see region)}{3}
\entry{scoping, lexical (defn)}{7}
\entry{scoping, static}{7}
\entry{screen, clearing}{230}
\entry{searching, of alist}{172}
\entry{searching, of bit string}{154}
\entry{searching, of list}{140}
\entry{searching, of string}{121}
\entry{searching, of vector}{151}
\entry{selecting, of stream component}{167}
\entry{selection, components of pathname}{272}
\entry{selection, of bit string component}{154}
\entry{selection, of cell component}{162}
\entry{selection, of character component}{105}
\entry{selection, of ephemeron component}{169}
\entry{selection, of list component}{136}
\entry{selection, of pair component}{132}
\entry{selection, of vector component}{150}
\entry{selection, of weak pair component}{169}
\entry{semicolon, as external representation}{11}
\entry{sensitivity, to case in programs (defn)}{10}
\entry{sequencing expressions}{28}
\entry{server socket}{295, 296}
\entry{service, TCP}{295}
\entry{set, of characters}{107}
\entry{sets, using binary trees}{191}
\entry{shadowing, of variable binding (defn)}{6}
\entry{short precision, of inexact number}{66}
\entry{signal an error (defn)}{4}
\entry{signalling NaN}{84, 93}
\entry{signalling, of condition (defn)}{300}
\entry{signed zero}{84}
\entry{simplest rational (defn)}{71}
\entry{simplification, of pathname}{266}
\entry{single precision, of inexact number}{66}
\entry{size, of hash table (defn)}{183}
\entry{slice, of string}{112}
\entry{socket}{295}
\entry{soft linking, of file}{276}
\entry{special characters, in programs}{11}
\entry{special form}{15}
\entry{special form (defn)}{13}
\entry{special form, entry category}{5}
\entry{specialization, of condition types}{300, 301, 311, 312, 314}
\entry{specialization, of condition types (defn)}{299}
\entry{specified result, in examples}{4}
\entry{splitter procedure}{118}
\entry{splitting, of string}{118}
\entry{SRFI 0}{52}
\entry{SRFI 2}{54}
\entry{SRFI 8}{53}
\entry{SRFI 9}{54}
\entry{SRFI syntax}{51}
\entry{standard operations, on textual port}{239}
\entry{standard Scheme (defn)}{3}
\entry{start, of subvector (defn)}{149}
\entry{static scoping}{7}
\entry{static scoping (defn)}{3}
\entry{static types (defn)}{3}
\entry{stream (defn)}{166}
\entry{stream, converting to list}{166}
\entry{string builder procedure}{116}
\entry{string index (defn)}{109}
\entry{string length (defn)}{109}
\entry{string slice}{112}
\entry{string, character (defn)}{109}
\entry{string, input and output ports}{221}
\entry{string, input from port}{227}
\entry{string, input from textual port}{241}
\entry{string, interning as symbol}{159}
\entry{string, of bits (defn)}{153}
\entry{string, searching string for}{121}
\entry{strong reference (defn)}{167}
\entry{strong types (defn)}{3}
\entry{strongly held data, of hash table}{176}
\entry{strongly held keys, of hash table}{176}
\entry{subnormal floating-point number}{84}
\entry{subnormal-operand exception}{93}
\entry{subprocess}{291}
\entry{substring, of bit string}{154}
\entry{substring, output to textual port}{243}
\entry{subtraction, of numbers}{68}
\entry{subvector (defn)}{149}
\entry{suffix, of string}{124}
\entry{sum, of numbers}{68}
\entry{super, bucky bit prefix (defn)}{101}
\entry{symbol (defn)}{158}
\entry{symbolic link, predicate for}{278}
\entry{symbolic linking, of file}{276}
\entry{synchronous subprocess}{291}
\entry{syntactic closure}{43}
\entry{syntactic closures}{42}
\entry{syntactic environment}{42}
\entry{syntactic keyword}{14, 37}
\entry{syntactic keyword (defn)}{13}
\entry{syntactic keyword binding}{213}
\entry{syntactic keyword, identifier as}{10}
\entry{synthetic identifier}{42}
\initial {T}
\entry{table, association (defn)}{174}
\entry{table, one-dimensional (defn)}{173}
\entry{tail recursion (defn)}{3}
\entry{tail recursion, vs. iteration expression}{29}
\entry{taxonomical link, of condition type (defn)}{299}
\entry{terminal mode, of port}{233}
\entry{terminal screen, clearing}{230}
\entry{tetrachotomy}{84}
\entry{textual input port operations}{241}
\entry{textual output port operations}{243}
\entry{textual port (defn)}{217}
\entry{textual port primitives}{239}
\entry{textual port type}{239}
\entry{tick}{289}
\entry{time, decoded}{282}
\entry{time, file}{282}
\entry{time, string}{282}
\entry{time, universal}{281}
\entry{token, in programs (defn)}{9}
\entry{top-level definition}{23}
\entry{top-level definition (defn)}{22}
\entry{top-level environment}{216}
\entry{total ordering (defn)}{146}
\entry{tracing output port, current (defn)}{218}
\entry{transformer environment}{43}
\entry{tree, copying}{134}
\entry{trees, balanced binary}{187, 191}
\entry{trichotomy}{84}
\entry{trimmer procedure}{120}
\entry{trimming, of string}{120}
\entry{true, boolean object}{8}
\entry{true, boolean object (defn)}{157}
\entry{true, in conditional expression (defn)}{26}
\entry{truename, of input file}{277}
\entry{type predicate, for 1D table}{174}
\entry{type predicate, for alist}{172}
\entry{type predicate, for apply hook}{211}
\entry{type predicate, for bit string}{154}
\entry{type predicate, for boolean}{157}
\entry{type predicate, for cell}{162}
\entry{type predicate, for character}{102}
\entry{type predicate, for character set}{107}
\entry{type predicate, for compiled procedure}{206}
\entry{type predicate, for compound procedure}{206}
\entry{type predicate, for continuation}{209}
\entry{type predicate, for empty list}{136}
\entry{type predicate, for entity}{211}
\entry{type predicate, for environment}{213}
\entry{type predicate, for EOF object}{225}
\entry{type predicate, for ephemeron}{169}
\entry{type predicate, for fixnum}{81}
\entry{type predicate, for flonum}{84}
\entry{type predicate, for hash table}{181}
\entry{type predicate, for list}{136}
\entry{type predicate, for number}{66}
\entry{type predicate, for pair}{132}
\entry{type predicate, for pathname}{270}
\entry{type predicate, for pathname host}{273}
\entry{type predicate, for port}{218}
\entry{type predicate, for primitive procedure}{206}
\entry{type predicate, for procedure}{205}
\entry{type predicate, for promise}{165}
\entry{type predicate, for record}{163}
\entry{type predicate, for record type}{164}
\entry{type predicate, for stream pair}{166}
\entry{type predicate, for symbol}{159}
\entry{type predicate, for top-level environment}{216}
\entry{type predicate, for vector}{150}
\entry{type predicate, for weak pair}{168}
\entry{type, condition}{300}
\entry{type, of condition}{313}
\entry{type, of procedure}{205}
\entry{type, pathname component}{267}
\entry{types, latent (defn)}{3}
\entry{types, manifest (defn)}{3}
\entry{types, Windows}{338}
\initial {U}
\entry{unassigned binding}{213}
\entry{unassigned variable}{13}
\entry{unassigned variable (defn)}{6}
\entry{unassigned variable, and assignment}{24}
\entry{unassigned variable, and definition}{23}
\entry{unassigned variable, and dynamic bindings}{22}
\entry{unassigned variable, and named let}{29}
\entry{unbound variable}{13}
\entry{unbound variable (defn)}{6}
\entry{underflow exception}{92}
\entry{Unicode}{105}
\entry{Unicode code point}{105}
\entry{Unicode normalization forms}{114}
\entry{Unicode scalar value}{105}
\entry{Uniform Resource Identifier}{258}
\entry{uninterned symbol (defn)}{158}
\entry{universal time}{281}
\entry{unordered comparison}{84}
\entry{unspecified result (defn)}{4}
\entry{up, as pathname component}{268}
\entry{uppercase}{10}
\entry{uppercase, character conversion}{103}
\entry{URI, of XML name}{258}
\entry{usable size, of hash table (defn)}{183}
\entry{usage environment}{43}
\initial {V}
\entry{V as format parameter}{235}
\entry{valid index, of bit string (defn)}{153}
\entry{valid index, of list (defn)}{137}
\entry{valid index, of string (defn)}{109}
\entry{valid index, of subvector (defn)}{149}
\entry{valid index, of vector (defn)}{149}
\entry{value, of variable (defn)}{6}
\entry{values, multiple}{210}
\entry{variable binding}{6, 213}
\entry{variable binding, do}{30}
\entry{variable binding, fluid-let}{22}
\entry{variable binding, internal definition}{23}
\entry{variable binding, lambda}{15}
\entry{variable binding, let}{17}
\entry{variable binding, let*}{17}
\entry{variable binding, letrec}{18}
\entry{variable binding, top-level definition}{23}
\entry{variable reference (defn)}{13}
\entry{variable, adding to environment}{22}
\entry{variable, assigning values to}{24}
\entry{variable, binding region (defn)}{7}
\entry{variable, entry category}{5}
\entry{variable, identifier as}{10}
\entry{vector (defn)}{149}
\entry{vector index (defn)}{149}
\entry{vector length (defn)}{149}
\entry{vector, converting to list}{135}
\entry{version, pathname component}{267}
\entry{virtual coordinates, graphics (defn)}{324}
\initial {W}
\entry{warning}{337}
\entry{weak pair (defn)}{168}
\entry{weak pair, and 1D table}{173}
\entry{weak reference (defn)}{167}
\entry{weak types (defn)}{3}
\entry{weakly held data, of hash table}{176}
\entry{weakly held keys, of hash table}{176}
\entry{weight-balanced binary trees}{191}
\entry{whitespace, in programs (defn)}{9}
\entry{Win32 API names}{342}
\entry{Win32 graphics}{334}
\entry{Windows types}{338}
\entry{working directory (see current working directory)}{274}
\initial {X}
\entry{X display, graphics}{330}
\entry{X geometry string, graphics}{331}
\entry{X graphics}{329}
\entry{X resources, graphics}{330}
\entry{X window system}{329}
\entry{XML input}{256}
\entry{XML names}{258}
\entry{XML output}{257}
\entry{XML parser}{256}
\initial {Z}
\entry{zero}{67, 84}
