@unnchapentry{Acknowledgements}{10001}{Acknowledgements}{1}
@numchapentry{Overview}{1}{Overview}{3}
@numsecentry{Notational Conventions}{1.1}{Notational Conventions}{4}
@numsubsecentry{Errors}{1.1.1}{Errors}{4}
@numsubsecentry{Examples}{1.1.2}{Examples}{4}
@numsubsecentry{Entry Format}{1.1.3}{Entry Format}{5}
@numsecentry{Scheme Concepts}{1.2}{Scheme Concepts}{6}
@numsubsecentry{Variable Bindings}{1.2.1}{Variable Bindings}{6}
@numsubsecentry{Environment Concepts}{1.2.2}{Environment Concepts}{6}
@numsubsecentry{Initial and Current Environments}{1.2.3}{Initial and Current Environments}{7}
@numsubsecentry{Static Scoping}{1.2.4}{Static Scoping}{7}
@numsubsecentry{True and False}{1.2.5}{True and False}{8}
@numsubsecentry{External Representations}{1.2.6}{External Representations}{8}
@numsubsecentry{Disjointness of Types}{1.2.7}{Disjointness of Types}{9}
@numsubsecentry{Storage Model}{1.2.8}{Storage Model}{9}
@numsecentry{Lexical Conventions}{1.3}{Lexical Conventions}{9}
@numsubsecentry{Whitespace}{1.3.1}{Whitespace}{9}
@numsubsecentry{Delimiters}{1.3.2}{Delimiters}{10}
@numsubsecentry{Identifiers}{1.3.3}{Identifiers}{10}
@numsubsecentry{Uppercase and Lowercase}{1.3.4}{Uppercase and Lowercase}{10}
@numsubsecentry{Naming Conventions}{1.3.5}{Naming Conventions}{10}
@numsubsecentry{Comments}{1.3.6}{Comments}{11}
@numsubsecentry{Additional Notations}{1.3.7}{Additional Notations}{11}
@numsecentry{Expressions}{1.4}{Expressions}{12}
@numsubsecentry{Literal Expressions}{1.4.1}{Literal Expressions}{13}
@numsubsecentry{Variable References}{1.4.2}{Variable References}{13}
@numsubsecentry{Special Form Syntax}{1.4.3}{Special Form Syntax}{13}
@numsubsecentry{Procedure Call Syntax}{1.4.4}{Procedure Call Syntax}{13}
@numchapentry{Special Forms}{2}{Special Forms}{15}
@numsecentry{Lambda Expressions}{2.1}{Lambda Expressions}{15}
@numsecentry{Lexical Binding}{2.2}{Lexical Binding}{17}
@numsecentry{Dynamic Binding}{2.3}{Dynamic Binding}{20}
@numsubsecentry{Fluid-Let}{2.3.1}{}{22}
@numsecentry{Definitions}{2.4}{Definitions}{22}
@numsubsecentry{Top-Level Definitions}{2.4.1}{Top-Level Definitions}{23}
@numsubsecentry{Internal Definitions}{2.4.2}{Internal Definitions}{23}
@numsecentry{Assignments}{2.5}{Assignments}{24}
@numsecentry{Quoting}{2.6}{Quoting}{24}
@numsecentry{Conditionals}{2.7}{Conditionals}{26}
@numsecentry{Sequencing}{2.8}{Sequencing}{28}
@numsecentry{Iteration}{2.9}{Iteration}{29}
@numsecentry{Structure Definitions}{2.10}{Structure Definitions}{31}
@numsecentry{Macros}{2.11}{Macros}{36}
@numsubsecentry{Binding Constructs for Syntactic Keywords}{2.11.1}{Syntactic Binding Constructs}{37}
@numsubsecentry{Pattern Language}{2.11.2}{Pattern Language}{40}
@numsubsecentry{Syntactic Closures}{2.11.3}{Syntactic Closures}{42}
@numsubsubsecentry{Syntax Terminology}{2.11.3.1}{Syntax Terminology}{42}
@numsubsubsecentry{Transformer Definition}{2.11.3.2}{SC Transformer Definition}{43}
@numsubsubsecentry{Identifiers}{2.11.3.3}{SC Identifiers}{47}
@numsubsecentry{Explicit Renaming}{2.11.4}{Explicit Renaming}{49}
@numsecentry{SRFI syntax}{2.12}{SRFI syntax}{51}
@numsubsecentry{cond-expand (SRFI 0)}{2.12.1}{cond-expand (SRFI 0)}{52}
@numsubsecentry{receive (SRFI 8)}{2.12.2}{receive (SRFI 8)}{53}
@numsubsecentry{and-let* (SRFI 2)}{2.12.3}{and-let* (SRFI 2)}{54}
@numsubsecentry{define-record-type (SRFI 9)}{2.12.4}{define-record-type (SRFI 9)}{54}
@numchapentry{Equivalence Predicates}{3}{Equivalence Predicates}{57}
@numchapentry{Numbers}{4}{Numbers}{63}
@numsecentry{Numerical types}{4.1}{Numerical types}{63}
@numsecentry{Exactness}{4.2}{Exactness}{64}
@numsecentry{Implementation restrictions}{4.3}{Implementation restrictions}{64}
@numsecentry{Syntax of numerical constants}{4.4}{Syntax of numerical constants}{66}
@numsecentry{Numerical operations}{4.5}{Numerical operations}{66}
@numsecentry{Numerical input and output}{4.6}{Numerical input and output}{78}
@numsecentry{Fixnum and Flonum Operations}{4.7}{Fixnum and Flonum Operations}{81}
@numsubsecentry{Fixnum Operations}{4.7.1}{Fixnum Operations}{81}
@numsubsecentry{Flonum Operations}{4.7.2}{Flonum Operations}{83}
@numsubsecentry{Floating-Point Environment}{4.7.3}{Floating-Point Environment}{91}
@numsubsecentry{Floating-Point Exceptions}{4.7.4}{Floating-Point Exceptions}{92}
@numsubsecentry{Floating-Point Rounding Mode}{4.7.5}{Floating-Point Rounding Mode}{95}
@numsecentry{Random Number Generation}{4.8}{Random Number Generation}{96}
@numchapentry{Characters}{5}{Characters}{101}
@numsecentry{Character implementation}{5.1}{Character implementation}{104}
@numsecentry{Unicode}{5.2}{Unicode}{105}
@numsecentry{Character Sets}{5.3}{Character Sets}{107}
@numchapentry{Strings}{6}{Strings}{109}
@numsecentry{Searching and Matching Strings}{6.1}{Searching and Matching Strings}{121}
@numsecentry{Regular Expressions}{6.2}{Regular Expressions}{124}
@numsubsecentry{Regular S-Expressions}{6.2.1}{Regular S-Expressions}{124}
@numsubsecentry{Regsexp Procedures}{6.2.2}{Regsexp Procedures}{129}
@numchapentry{Lists}{7}{Lists}{131}
@numsecentry{Pairs}{7.1}{Pairs}{132}
@numsecentry{Construction of Lists}{7.2}{Construction of Lists}{134}
@numsecentry{Selecting List Components}{7.3}{Selecting List Components}{136}
@numsecentry{Cutting and Pasting Lists}{7.4}{Cutting and Pasting Lists}{137}
@numsecentry{Filtering Lists}{7.5}{Filtering Lists}{139}
@numsecentry{Searching Lists}{7.6}{Searching Lists}{140}
@numsecentry{Mapping of Lists}{7.7}{Mapping of Lists}{141}
@numsecentry{Folding of Lists}{7.8}{Folding of Lists}{144}
@numsecentry{Miscellaneous List Operations}{7.9}{Miscellaneous List Operations}{146}
@numchapentry{Vectors}{8}{Vectors}{149}
@numsecentry{Construction of Vectors}{8.1}{Construction of Vectors}{149}
@numsecentry{Selecting Vector Components}{8.2}{Selecting Vector Components}{150}
@numsecentry{Cutting Vectors}{8.3}{Cutting Vectors}{151}
@numsecentry{Modifying Vectors}{8.4}{Modifying Vectors}{152}
@numchapentry{Bit Strings}{9}{Bit Strings}{153}
@numsecentry{Construction of Bit Strings}{9.1}{Construction of Bit Strings}{153}
@numsecentry{Selecting Bit String Components}{9.2}{Selecting Bit String Components}{154}
@numsecentry{Cutting and Pasting Bit Strings}{9.3}{Cutting and Pasting Bit Strings}{154}
@numsecentry{Bitwise Operations on Bit Strings}{9.4}{Bitwise Operations on Bit Strings}{155}
@numsecentry{Modification of Bit Strings}{9.5}{Modification of Bit Strings}{155}
@numsecentry{Integer Conversions of Bit Strings}{9.6}{Integer Conversions of Bit Strings}{156}
@numchapentry{Miscellaneous Datatypes}{10}{Miscellaneous Datatypes}{157}
@numsecentry{Booleans}{10.1}{Booleans}{157}
@numsecentry{Symbols}{10.2}{Symbols}{158}
@numsecentry{Parameters}{10.3}{Parameters}{161}
@numsubsecentry{Cells}{10.3.1}{}{162}
@numsecentry{Records}{10.4}{Records}{162}
@numsecentry{Promises}{10.5}{Promises}{164}
@numsecentry{Streams}{10.6}{Streams}{166}
@numsecentry{Weak References}{10.7}{Weak References}{167}
@numsubsecentry{Weak Pairs}{10.7.1}{Weak Pairs}{168}
@numsubsecentry{Ephemerons}{10.7.2}{Ephemerons}{169}
@numsubsecentry{Reference barriers}{10.7.3}{Reference barriers}{170}
@numchapentry{Associations}{11}{Associations}{171}
@numsecentry{Association Lists}{11.1}{Association Lists}{171}
@numsecentry{1D Tables}{11.2}{1D Tables}{173}
@numsecentry{The Association Table}{11.3}{The Association Table}{174}
@numsecentry{Hash Tables}{11.4}{Hash Tables}{175}
@numsubsecentry{Construction of Hash Tables}{11.4.1}{Construction of Hash Tables}{175}
@numsubsecentry{Basic Hash Table Operations}{11.4.2}{Basic Hash Table Operations}{181}
@numsubsecentry{Resizing of Hash Tables}{11.4.3}{Resizing of Hash Tables}{183}
@numsubsecentry{Address Hashing}{11.4.4}{Address Hashing}{185}
@numsecentry{Object Hashing}{11.5}{Object Hashing}{186}
@numsecentry{Red-Black Trees}{11.6}{Red-Black Trees}{187}
@numsecentry{Weight-Balanced Trees}{11.7}{Weight-Balanced Trees}{191}
@numsubsecentry{Construction of Weight-Balanced Trees}{11.7.1}{Construction of Weight-Balanced Trees}{191}
@numsubsecentry{Basic Operations on Weight-Balanced Trees}{11.7.2}{Basic Operations on Weight-Balanced Trees}{193}
@numsubsecentry{Advanced Operations on Weight-Balanced Trees}{11.7.3}{Advanced Operations on Weight-Balanced Trees}{194}
@numsubsecentry{Indexing Operations on Weight-Balanced Trees}{11.7.4}{Indexing Operations on Weight-Balanced Trees}{196}
@numsecentry{Associative Maps}{11.8}{Associative Maps}{198}
@numsubsecentry{Amap constructors}{11.8.1}{Amap constructors}{198}
@numsubsecentry{Amap predicates}{11.8.2}{Amap predicates}{199}
@numsubsecentry{Amap accessors}{11.8.3}{Amap accessors}{200}
@numsubsecentry{Amap mutators}{11.8.4}{Amap mutators}{200}
@numsubsecentry{Amap mapping and folding}{11.8.5}{Amap mapping and folding}{201}
@numsubsecentry{Amap contents}{11.8.6}{Amap contents}{202}
@numsubsecentry{Amap copying and conversion}{11.8.7}{Amap copying and conversion}{202}
@numsubsecentry{Amaps as sets}{11.8.8}{Amaps as sets}{203}
@numchapentry{Procedures}{12}{Procedures}{205}
@numsecentry{Procedure Operations}{12.1}{Procedure Operations}{205}
@numsecentry{Arity}{12.2}{Arity}{206}
@numsecentry{Primitive Procedures}{12.3}{Primitive Procedures}{207}
@numsecentry{Continuations}{12.4}{Continuations}{208}
@numsecentry{Application Hooks}{12.5}{Application Hooks}{210}
@numchapentry{Environments}{13}{Environments}{213}
@numsecentry{Environment Operations}{13.1}{Environment Operations}{213}
@numsecentry{Environment Variables}{13.2}{Environment Variables}{215}
@numsecentry{REPL Environment}{13.3}{REPL Environment}{215}
@numsecentry{Top-level Environments}{13.4}{Top-level Environments}{216}
@numchapentry{Input/Output}{14}{Input/Output}{217}
@numsecentry{Ports}{14.1}{Ports}{217}
@numsecentry{File Ports}{14.2}{File Ports}{219}
@numsecentry{String Ports}{14.3}{String Ports}{221}
@numsecentry{Bytevector Ports}{14.4}{Bytevector Ports}{223}
@numsecentry{Input Procedures}{14.5}{Input Procedures}{223}
@numsubsecentry{Reader Controls}{14.5.1}{}{227}
@numsecentry{Output Procedures}{14.6}{Output Procedures}{228}
@numsecentry{Blocking Mode}{14.7}{Blocking Mode}{232}
@numsecentry{Terminal Mode}{14.8}{Terminal Mode}{233}
@numsecentry{Format}{14.9}{Format}{234}
@numsecentry{Custom Output}{14.10}{Custom Output}{236}
@numsecentry{Prompting}{14.11}{Prompting}{237}
@numsecentry{Textual Port Primitives}{14.12}{Textual Port Primitives}{239}
@numsubsecentry{Textual Port Types}{14.12.1}{Textual Port Types}{239}
@numsubsecentry{Constructors and Accessors for Textual Ports}{14.12.2}{Constructors and Accessors for Textual Ports}{240}
@numsubsecentry{Textual Input Port Operations}{14.12.3}{Textual Input Port Operations}{241}
@numsubsecentry{Textual Output Port Operations}{14.12.4}{Textual Output Port Operations}{243}
@numsecentry{Parser Buffers}{14.13}{Parser Buffers}{244}
@numsecentry{Parser Language}{14.14}{Parser Language}{247}
@numsubsecentry{*Matcher}{14.14.1}{*Matcher}{249}
@numsubsecentry{*Parser}{14.14.2}{*Parser}{252}
@numsubsecentry{Parser-language Macros}{14.14.3}{Parser-language Macros}{255}
@numsecentry{XML Support}{14.15}{XML Support}{256}
@numsubsecentry{XML Input}{14.15.1}{XML Input}{256}
@numsubsecentry{XML Output}{14.15.2}{XML Output}{257}
@numsubsecentry{XML Names}{14.15.3}{XML Names}{258}
@numsubsecentry{XML Structure}{14.15.4}{XML Structure}{260}
@numchapentry{Operating-System Interface}{15}{Operating-System Interface}{265}
@numsecentry{Pathnames}{15.1}{Pathnames}{265}
@numsubsecentry{Filenames and Pathnames}{15.1.1}{Filenames and Pathnames}{266}
@numsubsecentry{Components of Pathnames}{15.1.2}{Components of Pathnames}{267}
@numsubsecentry{Operations on Pathnames}{15.1.3}{Operations on Pathnames}{270}
@numsubsecentry{Miscellaneous Pathname Procedures}{15.1.4}{Miscellaneous Pathnames}{273}
@numsecentry{Working Directory}{15.2}{Working Directory}{274}
@numsecentry{File Manipulation}{15.3}{File Manipulation}{275}
@numsecentry{Directory Reader}{15.4}{Directory Reader}{281}
@numsecentry{Date and Time}{15.5}{Date and Time}{281}
@numsubsecentry{Universal Time}{15.5.1}{Universal Time}{282}
@numsubsecentry{Decoded Time}{15.5.2}{Decoded Time}{282}
@numsubsecentry{File Time}{15.5.3}{File Time}{285}
@numsubsecentry{Time-Format Conversion}{15.5.4}{Time-Format Conversion}{285}
@numsubsecentry{External Representation of Time}{15.5.5}{External Representation of Time}{288}
@numsecentry{Machine Time}{15.6}{Machine Time}{289}
@numsecentry{Subprocesses}{15.7}{Subprocesses}{291}
@numsubsecentry{Subprocess Procedures}{15.7.1}{Subprocess Procedures}{291}
@numsubsecentry{Subprocess Conditions}{15.7.2}{Subprocess Conditions}{292}
@numsubsecentry{Subprocess Options}{15.7.3}{Subprocess Options}{292}
@numsecentry{TCP Sockets}{15.8}{TCP Sockets}{295}
@numsecentry{Miscellaneous OS Facilities}{15.9}{Miscellaneous OS Facilities}{297}
@numchapentry{Error System}{16}{Error System}{299}
@numsecentry{Condition Signalling}{16.1}{Condition Signalling}{300}
@numsecentry{Error Messages}{16.2}{Error Messages}{302}
@numsecentry{Condition Handling}{16.3}{Condition Handling}{303}
@numsecentry{Restarts}{16.4}{Restarts}{305}
@numsubsecentry{Establishing Restart Code}{16.4.1}{Establishing Restart Code}{306}
@numsubsecentry{Invoking Standard Restart Code}{16.4.2}{Invoking Standard Restart Code}{307}
@numsubsecentry{Finding and Invoking General Restart Code}{16.4.3}{Finding and Invoking General Restart Code}{309}
@numsubsecentry{The Named Restart Abstraction}{16.4.4}{The Named Restart Abstraction}{310}
@numsecentry{Condition Instances}{16.5}{Condition Instances}{310}
@numsubsecentry{Generating Operations on Conditions}{16.5.1}{Generating Operations on Conditions}{311}
@numsubsecentry{Condition Abstraction}{16.5.2}{Condition State}{312}
@numsubsecentry{Simple Operations on Condition Instances}{16.5.3}{Simple Condition Instance Operations}{313}
@numsecentry{Condition Types}{16.6}{Condition Types}{313}
@numsecentry{Condition-Type Taxonomy}{16.7}{Taxonomy}{314}
@numchapentry{Graphics}{17}{Graphics}{323}
@numsecentry{Opening and Closing of Graphics Devices}{17.1}{Opening and Closing of Graphics Devices}{323}
@numsecentry{Coordinates for Graphics}{17.2}{Coordinates for Graphics}{324}
@numsecentry{Drawing Graphics}{17.3}{Drawing Graphics}{324}
@numsecentry{Characteristics of Graphics Output}{17.4}{Characteristics of Graphics Output}{326}
@numsecentry{Buffering of Graphics Output}{17.5}{Buffering of Graphics Output}{327}
@numsecentry{Clipping of Graphics Output}{17.6}{Clipping of Graphics Output}{328}
@numsecentry{Custom Graphics Operations}{17.7}{Custom Graphics Operations}{328}
@numsecentry{Images}{17.8}{Images}{328}
@numsecentry{X Graphics}{17.9}{X Graphics}{329}
@numsubsecentry{X Graphics Type}{17.9.1}{X Graphics Type}{330}
@numsubsecentry{Utilities for X Graphics}{17.9.2}{Utilities for X Graphics}{330}
@numsubsecentry{Custom Operations on X Graphics Devices}{17.9.3}{Custom Operations on X Graphics Devices}{331}
@numsecentry{Win32 Graphics}{17.10}{Win32 Graphics}{334}
@numsubsecentry{Win32 Graphics Type}{17.10.1}{Win32 Graphics Type}{334}
@numsubsecentry{Custom Operations for Win32 Graphics}{17.10.2}{Custom Operations for Win32 Graphics}{335}
@numchapentry{Win32 Package Reference}{18}{Win32 Package Reference}{337}
@numsecentry{Overview}{18.1}{Win32 Package Overview}{337}
@numsecentry{Foreign Function Interface}{18.2}{Foreign function interface}{337}
@numsubsecentry{Windows Types}{18.2.1}{Windows Types}{338}
@numsubsecentry{Windows Foreign Procedures}{18.2.2}{Windows Foreign Procedures}{340}
@numsubsecentry{Win32 API names and procedures}{18.2.3}{Win32 API names and procedures}{342}
@numsecentry{Device Independent Bitmap Utilities}{18.3}{Device Independent Bitmap Utilities}{343}
@numsubsecentry{DIB procedures}{18.3.1}{DIB procedures}{344}
@numsubsecentry{Other parts of the DIB Utilities implementation}{18.3.2}{Other parts of the DIB Utilities implementation}{345}
@numchapentry{Standards Support}{19}{Standards Support}{347}
@numsecentry{Revised@sup {7} Report on the Algorithmic Language Scheme}{19.1}{R7RS}{347}
@numsecentry{SRFI 1: List Library}{19.2}{SRFI 1}{350}
@numsecentry{SRFI 2: @code {and-let*}}{19.3}{SRFI 2}{351}
@numsecentry{SRFI 8: @code {receive}}{19.4}{SRFI 8}{351}
@numsecentry{SRFI 9: Record Types}{19.5}{SRFI 9}{352}
@numsecentry{SRFI 14: Character-set Library}{19.6}{SRFI 14}{352}
@numsecentry{SRFI 23: Error Reporting Mechanism}{19.7}{SRFI 23}{353}
@numsecentry{SRFI 27: Sources of Random Bits}{19.8}{SRFI 27}{353}
@numsecentry{SRFI 39: Parameter Objects}{19.9}{SRFI 39}{354}
@numsecentry{SRFI 69: Basic Hash Tables}{19.10}{SRFI 69}{354}
@numsecentry{SRFI 115: Scheme Regular Expressions}{19.11}{SRFI 115}{354}
@numsecentry{SRFI 124: Ephemerons}{19.12}{SRFI 124}{355}
@numsecentry{SRFI 125: Intermediate Hash Tables}{19.13}{SRFI 125}{355}
@numsecentry{SRFI 128: Comparators (reduced)}{19.14}{SRFI 128}{356}
@numsecentry{SRFI 131: ERRR5RS Record Syntax (reduced)}{19.15}{SRFI 131}{357}
@numsecentry{SRFI 133: Vector Library (R7RS-compatible)}{19.16}{SRFI 133}{357}
@numsecentry{SRFI 140: Immutable Strings}{19.17}{SRFI 140}{358}
@numsecentry{SRFI 143: Fixnums}{19.18}{SRFI 143}{359}
@numsecentry{SRFI 158: Generators and Accumulators}{19.19}{SRFI 158}{360}
@appentry{GNU Free Documentation License}{A}{GNU Free Documentation License}{361}
@appsecentry{ADDENDUM: How to use this License for your documents}{A.1}{}{367}
@appentry{Binding Index}{B}{Binding Index}{369}
@appentry{Concept Index}{C}{Concept Index}{391}
