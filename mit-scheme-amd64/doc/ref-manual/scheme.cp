\entry{runtime system}{3}{runtime system}
\entry{standard Scheme (defn)}{3}{standard Scheme (defn)}
\entry{Scheme standard}{3}{Scheme standard}
\entry{R4RS}{3}{R4RS}
\entry{static scoping (defn)}{3}{static scoping (defn)}
\entry{scope (see region)}{3}{scope (see region)}
\entry{latent types (defn)}{3}{latent types (defn)}
\entry{manifest types (defn)}{3}{manifest types (defn)}
\entry{weak types (defn)}{3}{weak types (defn)}
\entry{strong types (defn)}{3}{strong types (defn)}
\entry{dynamic types (defn)}{3}{dynamic types (defn)}
\entry{static types (defn)}{3}{static types (defn)}
\entry{types, latent (defn)}{3}{types, latent (defn)}
\entry{types, manifest (defn)}{3}{types, manifest (defn)}
\entry{extent, of objects}{3}{extent, of objects}
\entry{proper tail recursion (defn)}{3}{proper tail recursion (defn)}
\entry{tail recursion (defn)}{3}{tail recursion (defn)}
\entry{recursion (see tail recursion)}{3}{recursion (see tail recursion)}
\entry{notational conventions}{4}{notational conventions}
\entry{conventions, notational}{4}{conventions, notational}
\entry{errors, notational conventions}{4}{errors, notational conventions}
\entry{signal an error (defn)}{4}{signal an error (defn)}
\entry{must be, notational convention}{4}{must be, notational convention}
\entry{examples}{4}{examples}
\entry{=> notational convention}{4}{=> notational convention}
\entry{result of evaluation, in examples}{4}{result of evaluation, in examples}
\entry{evaluation, in examples}{4}{evaluation, in examples}
\entry{specified result, in examples}{4}{specified result, in examples}
\entry{error--> notational convention}{4}{error--> notational convention}
\entry{error, in examples}{4}{error, in examples}
\entry{-| notational convention}{4}{-| notational convention}
\entry{printed output, in examples}{4}{printed output, in examples}
\entry{unspecified result (defn)}{4}{unspecified result (defn)}
\entry{result, unspecified (defn)}{4}{result, unspecified (defn)}
\entry{entry format}{5}{entry format}
\entry{format, entry}{5}{format, entry}
\entry{variable, entry category}{5}{variable, entry category}
\entry{parameter, entry category}{5}{parameter, entry category}
\entry{special form, entry category}{5}{special form, entry category}
\entry{ellipsis, in entries}{5}{ellipsis, in entries}
\entry{... in entries}{5}{... in entries}
\entry{bracket, in entries}{5}{bracket, in entries}
\entry{[ in entries}{5}{[ in entries}
\entry{] in entries}{5}{] in entries}
\entry{optional component, in entries}{5}{optional component, in entries}
\entry{body, of special form (defn)}{5}{body, of special form (defn)}
\entry{procedure, entry format}{5}{procedure, entry format}
\entry{scheme concepts}{6}{scheme concepts}
\entry{variable binding}{6}{variable binding}
\entry{binding, of variable}{6}{binding, of variable}
\entry{bound variable (defn)}{6}{bound variable (defn)}
\entry{value, of variable (defn)}{6}{value, of variable (defn)}
\entry{name, of value (defn)}{6}{name, of value (defn)}
\entry{location, of variable}{6}{location, of variable}
\entry{unassigned variable (defn)}{6}{unassigned variable (defn)}
\entry{error, unassigned variable}{6}{error, unassigned variable}
\entry{environment (defn)}{6}{environment (defn)}
\entry{unbound variable (defn)}{6}{unbound variable (defn)}
\entry{error, unbound variable (defn)}{6}{error, unbound variable (defn)}
\entry{extension, of environment (defn)}{6}{extension, of environment (defn)}
\entry{environment, extension (defn)}{6}{environment, extension (defn)}
\entry{shadowing, of variable binding (defn)}{6}{shadowing, of variable binding (defn)}
\entry{parent, of environment (defn)}{6}{parent, of environment (defn)}
\entry{child, of environment (defn)}{6}{child, of environment (defn)}
\entry{inheritance, of environment bindings (defn)}{6}{inheritance, of environment bindings (defn)}
\entry{initial environment (defn)}{7}{initial environment (defn)}
\entry{environment, initial (defn)}{7}{environment, initial (defn)}
\entry{current environment (defn)}{7}{current environment (defn)}
\entry{environment, current (defn)}{7}{environment, current (defn)}
\entry{REP loop (defn)}{7}{REP loop (defn)}
\entry{REP loop, environment of}{7}{REP loop, environment of}
\entry{scoping, static}{7}{scoping, static}
\entry{static scoping}{7}{static scoping}
\entry{dynamic binding, versus static scoping}{7}{dynamic binding, versus static scoping}
\entry{binding expression (defn)}{7}{binding expression (defn)}
\entry{expression, binding (defn)}{7}{expression, binding (defn)}
\entry{lexical scoping (defn)}{7}{lexical scoping (defn)}
\entry{scoping, lexical (defn)}{7}{scoping, lexical (defn)}
\entry{region, of variable binding (defn)}{7}{region, of variable binding (defn)}
\entry{variable, binding region (defn)}{7}{variable, binding region (defn)}
\entry{boolean object}{8}{boolean object}
\entry{true, boolean object}{8}{true, boolean object}
\entry{false, boolean object}{8}{false, boolean object}
\entry{external representation (defn)}{8}{external representation (defn)}
\entry{representation, external (defn)}{8}{representation, external (defn)}
\entry{location}{9}{location}
\entry{constant}{9}{constant}
\entry{mutable}{9}{mutable}
\entry{immutable}{9}{immutable}
\entry{lexical conventions}{9}{lexical conventions}
\entry{conventions, lexical}{9}{conventions, lexical}
\entry{whitespace, in programs (defn)}{9}{whitespace, in programs (defn)}
\entry{token, in programs (defn)}{9}{token, in programs (defn)}
\entry{delimiter, in programs (defn)}{10}{delimiter, in programs (defn)}
\entry{identifier (defn)}{10}{identifier (defn)}
\entry{variable, identifier as}{10}{variable, identifier as}
\entry{syntactic keyword, identifier as}{10}{syntactic keyword, identifier as}
\entry{literal, identifier as}{10}{literal, identifier as}
\entry{uppercase}{10}{uppercase}
\entry{lowercase}{10}{lowercase}
\entry{alphabetic case-insensitivity of programs (defn)}{10}{alphabetic case-insensitivity of programs (defn)}
\entry{case-insensitivity of programs (defn)}{10}{case-insensitivity of programs (defn)}
\entry{sensitivity, to case in programs (defn)}{10}{sensitivity, to case in programs (defn)}
\entry{insensitivity, to case in programs (defn)}{10}{insensitivity, to case in programs (defn)}
\entry{naming conventions}{10}{naming conventions}
\entry{conventions, naming}{10}{conventions, naming}
\entry{predicate (defn)}{10}{predicate (defn)}
\entry{? in predicate names}{10}{? in predicate names}
\entry{mutation procedure (defn)}{10}{mutation procedure (defn)}
\entry{! in mutation procedure names}{10}{! in mutation procedure names}
\entry{comment, in programs (defn)}{11}{comment, in programs (defn)}
\entry{semicolon, as external representation}{11}{semicolon, as external representation}
\entry{; as external representation}{11}{; as external representation}
\entry{extended comment, in programs (defn)}{11}{extended comment, in programs (defn)}
\entry{comment, extended, in programs (defn)}{11}{comment, extended, in programs (defn)}
\entry{#| as external representation}{11}{#| as external representation}
\entry{characters, special, in programs}{11}{characters, special, in programs}
\entry{special characters, in programs}{11}{special characters, in programs}
\entry{expression (defn)}{12}{expression (defn)}
\entry{literal expression (defn)}{13}{literal expression (defn)}
\entry{constant expression (defn)}{13}{constant expression (defn)}
\entry{expression, literal (defn)}{13}{expression, literal (defn)}
\entry{expression, constant (defn)}{13}{expression, constant (defn)}
\entry{variable reference (defn)}{13}{variable reference (defn)}
\entry{reference, variable (defn)}{13}{reference, variable (defn)}
\entry{unbound variable}{13}{unbound variable}
\entry{unassigned variable}{13}{unassigned variable}
\entry{expression, special form (defn)}{13}{expression, special form (defn)}
\entry{special form (defn)}{13}{special form (defn)}
\entry{form, special (defn)}{13}{form, special (defn)}
\entry{keyword, of special form (defn)}{13}{keyword, of special form (defn)}
\entry{syntactic keyword (defn)}{13}{syntactic keyword (defn)}
\entry{expression, procedure call (defn)}{13}{expression, procedure call (defn)}
\entry{procedure call (defn)}{13}{procedure call (defn)}
\entry{operator, of procedure call (defn)}{13}{operator, of procedure call (defn)}
\entry{operand, of procedure call (defn)}{13}{operand, of procedure call (defn)}
\entry{combination (defn)}{14}{combination (defn)}
\entry{order, of argument evaluation}{14}{order, of argument evaluation}
\entry{evaluation order, of arguments}{14}{evaluation order, of arguments}
\entry{argument evaluation order}{14}{argument evaluation order}
\entry{syntactic keyword}{14}{syntactic keyword}
\entry{special form}{15}{special form}
\entry{lambda expression (defn)}{15}{lambda expression (defn)}
\entry{procedure, construction}{15}{procedure, construction}
\entry{procedure, closing environment (defn)}{15}{procedure, closing environment (defn)}
\entry{procedure, invocation environment (defn)}{15}{procedure, invocation environment (defn)}
\entry{construction, of procedure}{15}{construction, of procedure}
\entry{closing environment, of procedure (defn)}{15}{closing environment, of procedure (defn)}
\entry{invocation environment, of procedure (defn)}{15}{invocation environment, of procedure (defn)}
\entry{environment, of procedure}{15}{environment, of procedure}
\entry{environment, procedure closing (defn)}{15}{environment, procedure closing (defn)}
\entry{environment, procedure invocation (defn)}{15}{environment, procedure invocation (defn)}
\entry{region of variable binding, lambda}{15}{region of variable binding, lambda}
\entry{variable binding, lambda}{15}{variable binding, lambda}
\entry{lambda list (defn)}{15}{lambda list (defn)}
\entry{parameter list, of lambda (defn)}{15}{parameter list, of lambda (defn)}
\entry{formal parameter list, of lambda (defn)}{15}{formal parameter list, of lambda (defn)}
\entry{required parameter (defn)}{15}{required parameter (defn)}
\entry{parameter, required (defn)}{15}{parameter, required (defn)}
\entry{optional parameter (defn)}{15}{optional parameter (defn)}
\entry{parameter, optional (defn)}{15}{parameter, optional (defn)}
\entry{default object (defn)}{15}{default object (defn)}
\entry{rest parameter (defn)}{15}{rest parameter (defn)}
\entry{parameter, rest (defn)}{15}{parameter, rest (defn)}
\entry{named lambda (defn)}{16}{named lambda (defn)}
\entry{lexical binding expression}{17}{lexical binding expression}
\entry{binding expression, lexical}{17}{binding expression, lexical}
\entry{block structure}{17}{block structure}
\entry{region of variable binding, let}{17}{region of variable binding, let}
\entry{variable binding, let}{17}{variable binding, let}
\entry{lambda, implicit in let}{17}{lambda, implicit in let}
\entry{region of variable binding, let*}{17}{region of variable binding, let*}
\entry{variable binding, let*}{17}{variable binding, let*}
\entry{region of variable binding, letrec}{18}{region of variable binding, letrec}
\entry{variable binding, letrec}{18}{variable binding, letrec}
\entry{binding expression, dynamic}{20}{binding expression, dynamic}
\entry{dynamic binding}{20}{dynamic binding}
\entry{dynamic environment}{20}{dynamic environment}
\entry{dynamic extent}{20}{dynamic extent}
\entry{extent, of dynamic binding (defn)}{21}{extent, of dynamic binding (defn)}
\entry{dynamic binding, and continuations}{21}{dynamic binding, and continuations}
\entry{continuation, and dynamic binding}{21}{continuation, and dynamic binding}
\entry{variable binding, fluid-let}{22}{variable binding, fluid-let}
\entry{unassigned variable, and dynamic bindings}{22}{unassigned variable, and dynamic bindings}
\entry{definition}{22}{definition}
\entry{variable, adding to environment}{22}{variable, adding to environment}
\entry{definition, top-level (defn)}{22}{definition, top-level (defn)}
\entry{definition, internal (defn)}{22}{definition, internal (defn)}
\entry{top-level definition (defn)}{22}{top-level definition (defn)}
\entry{internal definition (defn)}{22}{internal definition (defn)}
\entry{lambda, implicit in define}{22}{lambda, implicit in define}
\entry{procedure define (defn)}{22}{procedure define (defn)}
\entry{define, procedure (defn)}{22}{define, procedure (defn)}
\entry{top-level definition}{23}{top-level definition}
\entry{definition, top-level}{23}{definition, top-level}
\entry{variable binding, top-level definition}{23}{variable binding, top-level definition}
\entry{unassigned variable, and definition}{23}{unassigned variable, and definition}
\entry{internal definition}{23}{internal definition}
\entry{definition, internal}{23}{definition, internal}
\entry{region of variable binding, internal definition}{23}{region of variable binding, internal definition}
\entry{variable binding, internal definition}{23}{variable binding, internal definition}
\entry{letrec, implicit in define}{23}{letrec, implicit in define}
\entry{assignment}{24}{assignment}
\entry{variable, assigning values to}{24}{variable, assigning values to}
\entry{unassigned variable, and assignment}{24}{unassigned variable, and assignment}
\entry{access, used with set!}{24}{access, used with set!}
\entry{quoting}{24}{quoting}
\entry{external representation, and quote}{24}{external representation, and quote}
\entry{literal, and quote}{24}{literal, and quote}
\entry{constant, and quote}{24}{constant, and quote}
\entry{' as external representation}{24}{' as external representation}
\entry{apostrophe, as external representation}{24}{apostrophe, as external representation}
\entry{quote, as external representation}{24}{quote, as external representation}
\entry{external representation, and quasiquote}{25}{external representation, and quasiquote}
\entry{literal, and quasiquote}{25}{literal, and quasiquote}
\entry{constant, and quasiquote}{25}{constant, and quasiquote}
\entry{nesting, of quasiquote expressions}{25}{nesting, of quasiquote expressions}
\entry{backquote, as external representation}{25}{backquote, as external representation}
\entry{` as external representation}{25}{` as external representation}
\entry{comma, as external representation}{25}{comma, as external representation}
\entry{, as external representation}{25}{, as external representation}
\entry{,{\indexatchar} as external representation}{25}{,\@ as external representation}
\entry{expression, conditional (defn)}{26}{expression, conditional (defn)}
\entry{conditional expression (defn)}{26}{conditional expression (defn)}
\entry{true, in conditional expression (defn)}{26}{true, in conditional expression (defn)}
\entry{false, in conditional expression (defn)}{26}{false, in conditional expression (defn)}
\entry{cond clause}{26}{cond clause}
\entry{clause, of cond expression}{26}{clause, of cond expression}
\entry{else clause, of cond expression (defn)}{26}{else clause, of cond expression (defn)}
\entry{=> in cond clause}{27}{=> in cond clause}
\entry{case clause}{27}{case clause}
\entry{clause, of case expression}{27}{clause, of case expression}
\entry{else clause, of case expression (defn)}{27}{else clause, of case expression (defn)}
\entry{sequencing expressions}{28}{sequencing expressions}
\entry{implicit begin}{29}{implicit begin}
\entry{expression, iteration (defn)}{29}{expression, iteration (defn)}
\entry{iteration expression (defn)}{29}{iteration expression (defn)}
\entry{looping (see iteration expressions)}{29}{looping (see iteration expressions)}
\entry{tail recursion, vs. iteration expression}{29}{tail recursion, vs. iteration expression}
\entry{named let (defn)}{29}{named let (defn)}
\entry{unassigned variable, and named let}{29}{unassigned variable, and named let}
\entry{region of variable binding, do}{30}{region of variable binding, do}
\entry{variable binding, do}{30}{variable binding, do}
\entry{keyword constructor}{31}{keyword constructor}
\entry{BOA constructor}{31}{BOA constructor}
\entry{BOA constructor (defn)}{33}{BOA constructor (defn)}
\entry{keyword constructor (defn)}{34}{keyword constructor (defn)}
\entry{macro}{36}{macro}
\entry{syntactic keyword}{37}{syntactic keyword}
\entry{keyword}{37}{keyword}
\entry{macro keyword}{37}{macro keyword}
\entry{macro use}{37}{macro use}
\entry{macro transformer}{37}{macro transformer}
\entry{anonymous syntactic keyword}{37}{anonymous syntactic keyword}
\entry{hygienic}{37}{hygienic}
\entry{referentially transparent}{37}{referentially transparent}
\entry{syntactic closures}{42}{syntactic closures}
\entry{form}{42}{form}
\entry{alias}{42}{alias}
\entry{identifier}{42}{identifier}
\entry{synthetic identifier}{42}{synthetic identifier}
\entry{syntactic environment}{42}{syntactic environment}
\entry{syntactic closure}{43}{syntactic closure}
\entry{macro transformer}{43}{macro transformer}
\entry{input form}{43}{input form}
\entry{usage environment}{43}{usage environment}
\entry{output form}{43}{output form}
\entry{transformer environment}{43}{transformer environment}
\entry{alias}{47}{alias}
\entry{explicit renaming}{49}{explicit renaming}
\entry{macro transformer}{49}{macro transformer}
\entry{input form, to macro}{49}{input form, to macro}
\entry{renaming procedure}{49}{renaming procedure}
\entry{comparison predicate}{50}{comparison predicate}
\entry{SRFI syntax}{51}{SRFI syntax}
\entry{SRFI 0}{52}{SRFI 0}
\entry{SRFI 8}{53}{SRFI 8}
\entry{SRFI 2}{54}{SRFI 2}
\entry{SRFI 9}{54}{SRFI 9}
\entry{predicate (defn)}{57}{predicate (defn)}
\entry{predicate, equivalence (defn)}{57}{predicate, equivalence (defn)}
\entry{equivalence predicate (defn)}{57}{equivalence predicate (defn)}
\entry{comparison, for equivalence}{57}{comparison, for equivalence}
\entry{circular structure}{61}{circular structure}
\entry{number}{63}{number}
\entry{numerical types}{63}{numerical types}
\entry{exactness}{64}{exactness}
\entry{implementation restriction}{64}{implementation restriction}
\entry{number, external representation}{66}{number, external representation}
\entry{external representation, for number}{66}{external representation, for number}
\entry{#b as external representation}{66}{#b as external representation}
\entry{#o as external representation}{66}{#o as external representation}
\entry{#d as external representation}{66}{#d as external representation}
\entry{#x as external representation}{66}{#x as external representation}
\entry{#e as external representation}{66}{#e as external representation}
\entry{#i as external representation}{66}{#i as external representation}
\entry{# in external representation of number}{66}{# in external representation of number}
\entry{s, as exponent marker in number}{66}{s, as exponent marker in number}
\entry{f, as exponent marker in number}{66}{f, as exponent marker in number}
\entry{d, as exponent marker in number}{66}{d, as exponent marker in number}
\entry{l, as exponent marker in number}{66}{l, as exponent marker in number}
\entry{e, as exponent marker in number}{66}{e, as exponent marker in number}
\entry{exponent marker (defn)}{66}{exponent marker (defn)}
\entry{precision, of inexact number}{66}{precision, of inexact number}
\entry{numeric precision, inexact}{66}{numeric precision, inexact}
\entry{internal representation, for inexact number}{66}{internal representation, for inexact number}
\entry{short precision, of inexact number}{66}{short precision, of inexact number}
\entry{single precision, of inexact number}{66}{single precision, of inexact number}
\entry{double precision, of inexact number}{66}{double precision, of inexact number}
\entry{long precision, of inexact number}{66}{long precision, of inexact number}
\entry{numerical operations}{66}{numerical operations}
\entry{type predicate, for number}{66}{type predicate, for number}
\entry{ordering, of numbers}{67}{ordering, of numbers}
\entry{comparison, of numbers}{67}{comparison, of numbers}
\entry{equivalence predicate, for numbers}{67}{equivalence predicate, for numbers}
\entry{zero}{67}{zero}
\entry{positive number}{67}{positive number}
\entry{negative number}{67}{negative number}
\entry{odd number}{67}{odd number}
\entry{even number}{67}{even number}
\entry{minimum, of numbers}{68}{minimum, of numbers}
\entry{maximum, of numbers}{68}{maximum, of numbers}
\entry{addition, of numbers}{68}{addition, of numbers}
\entry{sum, of numbers}{68}{sum, of numbers}
\entry{identity, additive}{68}{identity, additive}
\entry{multiplication, of numbers}{68}{multiplication, of numbers}
\entry{product, of numbers}{68}{product, of numbers}
\entry{identity, multiplicative}{68}{identity, multiplicative}
\entry{subtraction, of numbers}{68}{subtraction, of numbers}
\entry{difference, of numbers}{68}{difference, of numbers}
\entry{inverse, additive, of number}{68}{inverse, additive, of number}
\entry{division, of numbers}{68}{division, of numbers}
\entry{quotient, of numbers}{68}{quotient, of numbers}
\entry{inverse, multiplicative, of number}{68}{inverse, multiplicative, of number}
\entry{absolute value, of number}{68}{absolute value, of number}
\entry{magnitude, of real number}{68}{magnitude, of real number}
\entry{division, of integers}{68}{division, of integers}
\entry{quotient, of integers}{68}{quotient, of integers}
\entry{remainder, of integers}{68}{remainder, of integers}
\entry{modulus, of integers}{68}{modulus, of integers}
\entry{integer division}{68}{integer division}
\entry{greatest common divisor, of numbers}{70}{greatest common divisor, of numbers}
\entry{least common multiple, of numbers}{70}{least common multiple, of numbers}
\entry{simplest rational (defn)}{71}{simplest rational (defn)}
\entry{rational, simplest (defn)}{71}{rational, simplest (defn)}
\entry{log-probability}{76}{log-probability}
\entry{log-odds}{76}{log-odds}
\entry{log-probability}{76}{log-probability}
\entry{log-odds}{76}{log-odds}
\entry{numerical input and output}{78}{numerical input and output}
\entry{fixnum (defn)}{81}{fixnum (defn)}
\entry{type predicate, for fixnum}{81}{type predicate, for fixnum}
\entry{equivalence predicate, for fixnums}{82}{equivalence predicate, for fixnums}
\entry{logical operations, on fixnums}{82}{logical operations, on fixnums}
\entry{bitwise-logical operations, on fixnums}{82}{bitwise-logical operations, on fixnums}
\entry{flonum (defn)}{83}{flonum (defn)}
\entry{floating-point number, normal}{83}{floating-point number, normal}
\entry{normal floating-point number}{83}{normal floating-point number}
\entry{floating-point number, subnormal}{84}{floating-point number, subnormal}
\entry{subnormal floating-point number}{84}{subnormal floating-point number}
\entry{denormal}{84}{denormal}
\entry{floating-point number, zero}{84}{floating-point number, zero}
\entry{zero}{84}{zero}
\entry{signed zero}{84}{signed zero}
\entry{positive infinity (+inf.0)}{84}{positive infinity (\code {+inf.0})}
\entry{negative infinity (-inf.0)}{84}{negative infinity (\code {-inf.0})}
\entry{floating-point number, infinite}{84}{floating-point number, infinite}
\entry{infinity (+inf.0, -inf.0)}{84}{infinity (\code {+inf.0}, \code {-inf.0})}
\entry{extended real line}{84}{extended real line}
\entry{floating-point number, not a number}{84}{floating-point number, not a number}
\entry{not a number (NaN, +nan.0)}{84}{not a number (NaN, \code {+nan.0})}
\entry{NaN}{84}{NaN}
\entry{quiet NaN}{84}{quiet NaN}
\entry{signalling NaN}{84}{signalling NaN}
\entry{invalid-operation exception}{84}{invalid-operation exception}
\entry{type predicate, for flonum}{84}{type predicate, for flonum}
\entry{equivalence predicate, for flonums}{84}{equivalence predicate, for flonums}
\entry{ordered comparison}{84}{ordered comparison}
\entry{floating-point comparison, ordered}{84}{floating-point comparison, ordered}
\entry{trichotomy}{84}{trichotomy}
\entry{equivalence predicate, for flonums}{84}{equivalence predicate, for flonums}
\entry{unordered comparison}{84}{unordered comparison}
\entry{floating-point comparison, unordered}{84}{floating-point comparison, unordered}
\entry{tetrachotomy}{84}{tetrachotomy}
\entry{floating-point environment}{91}{floating-point environment}
\entry{floating-point environment, default}{91}{floating-point environment, default}
\entry{default environment, floating-point}{91}{default environment, floating-point}
\entry{inexact-result exception}{92}{inexact-result exception}
\entry{underflow exception}{92}{underflow exception}
\entry{overflow exception}{93}{overflow exception}
\entry{divide-by-zero exception}{93}{divide-by-zero exception}
\entry{invalid-operation exception}{93}{invalid-operation exception}
\entry{signalling NaN}{93}{signalling NaN}
\entry{quiet NaN}{93}{quiet NaN}
\entry{subnormal-operand exception}{93}{subnormal-operand exception}
\entry{random number generation}{96}{random number generation}
\entry{pseudorandom number generation}{96}{pseudorandom number generation}
\entry{number, pseudorandom generation}{96}{number, pseudorandom generation}
\entry{character (defn)}{101}{character (defn)}
\entry{#{\indexbackslash} as external representation}{101}{#\backslashchar {} as external representation}
\entry{bucky bit, prefix (defn)}{101}{bucky bit, prefix (defn)}
\entry{control, bucky bit prefix (defn)}{101}{control, bucky bit prefix (defn)}
\entry{meta, bucky bit prefix (defn)}{101}{meta, bucky bit prefix (defn)}
\entry{super, bucky bit prefix (defn)}{101}{super, bucky bit prefix (defn)}
\entry{hyper, bucky bit prefix (defn)}{101}{hyper, bucky bit prefix (defn)}
\entry{type predicate, for character}{102}{type predicate, for character}
\entry{equivalence predicate, for characters}{102}{equivalence predicate, for characters}
\entry{ordering, of characters}{102}{ordering, of characters}
\entry{comparison, of characters}{102}{comparison, of characters}
\entry{uppercase, character conversion}{103}{uppercase, character conversion}
\entry{lowercase, character conversion}{103}{lowercase, character conversion}
\entry{case conversion, of character}{103}{case conversion, of character}
\entry{case folding, of character}{103}{case folding, of character}
\entry{internal representation, for character}{104}{internal representation, for character}
\entry{character code (defn)}{104}{character code (defn)}
\entry{character bits (defn)}{104}{character bits (defn)}
\entry{code, of character (defn)}{104}{code, of character (defn)}
\entry{bucky bit, of character (defn)}{104}{bucky bit, of character (defn)}
\entry{ASCII character}{104}{ASCII character}
\entry{construction, of character}{104}{construction, of character}
\entry{selection, of character component}{105}{selection, of character component}
\entry{component selection, of character}{105}{component selection, of character}
\entry{bitless character}{105}{bitless character}
\entry{character, bitless}{105}{character, bitless}
\entry{Unicode}{105}{Unicode}
\entry{Unicode code point}{105}{Unicode code point}
\entry{Unicode scalar value}{105}{Unicode scalar value}
\entry{code point}{105}{code point}
\entry{scalar value}{105}{scalar value}
\entry{character set}{107}{character set}
\entry{set, of characters}{107}{set, of characters}
\entry{type predicate, for character set}{107}{type predicate, for character set}
\entry{code-point list}{107}{code-point list}
\entry{code-point range}{107}{code-point range}
\entry{code-point range}{107}{code-point range}
\entry{string, character (defn)}{109}{string, character (defn)}
\entry{external representation, for string}{109}{external representation, for string}
\entry{" as external representation}{109}{" as external representation}
\entry{double quote, as external representation}{109}{double quote, as external representation}
\entry{{\indexbackslash} as escape character in string}{109}{\backslashchar {} as escape character in string}
\entry{backslash, as escape character in string}{109}{backslash, as escape character in string}
\entry{escape character, for string}{109}{escape character, for string}
\entry{length, of string (defn)}{109}{length, of string (defn)}
\entry{index, of string (defn)}{109}{index, of string (defn)}
\entry{valid index, of string (defn)}{109}{valid index, of string (defn)}
\entry{string length (defn)}{109}{string length (defn)}
\entry{string index (defn)}{109}{string index (defn)}
\entry{case sensitivity, of string operations}{109}{case sensitivity, of string operations}
\entry{-ci, in string procedure name}{109}{-ci, in string procedure name}
\entry{mutable string}{110}{mutable string}
\entry{immutable string}{110}{immutable string}
\entry{slice, of string}{112}{slice, of string}
\entry{string slice}{112}{string slice}
\entry{grapheme cluster}{113}{grapheme cluster}
\entry{NFC}{114}{NFC}
\entry{Normalization Form C (NFC)}{114}{Normalization Form C (NFC)}
\entry{NFD}{114}{NFD}
\entry{Normalization Form D (NFD)}{114}{Normalization Form D (NFD)}
\entry{Unicode normalization forms}{114}{Unicode normalization forms}
\entry{empty string, predicate for}{116}{empty string, predicate for}
\entry{null string, predicate for}{116}{null string, predicate for}
\entry{hashing, of string}{116}{hashing, of string}
\entry{string builder procedure}{116}{string builder procedure}
\entry{joining, of strings}{117}{joining, of strings}
\entry{joiner procedure, of strings}{117}{joiner procedure, of strings}
\entry{splitting, of string}{118}{splitting, of string}
\entry{splitter procedure}{118}{splitter procedure}
\entry{padding, of string}{118}{padding, of string}
\entry{padder procedure}{118}{padder procedure}
\entry{trimming, of string}{120}{trimming, of string}
\entry{trimmer procedure}{120}{trimmer procedure}
\entry{searching, of string}{121}{searching, of string}
\entry{matching, of strings}{121}{matching, of strings}
\entry{character, searching string for}{121}{character, searching string for}
\entry{string, searching string for}{121}{string, searching string for}
\entry{prefix, of string}{124}{prefix, of string}
\entry{suffix, of string}{124}{suffix, of string}
\entry{pair (defn)}{131}{pair (defn)}
\entry{dotted pair (see pair)}{131}{dotted pair (see pair)}
\entry{car field, of pair (defn)}{131}{car field, of pair (defn)}
\entry{cdr field, of pair (defn)}{131}{cdr field, of pair (defn)}
\entry{list (defn)}{131}{list (defn)}
\entry{element, of list (defn)}{131}{element, of list (defn)}
\entry{length, of list (defn)}{131}{length, of list (defn)}
\entry{empty list (defn)}{131}{empty list (defn)}
\entry{dotted notation, for pair (defn)}{131}{dotted notation, for pair (defn)}
\entry{notation, dotted (defn)}{131}{notation, dotted (defn)}
\entry{external representation, for pair}{131}{external representation, for pair}
\entry{pair, external representation}{131}{pair, external representation}
\entry{( as external representation}{131}{( as external representation}
\entry{) as external representation}{131}{) as external representation}
\entry{. as external representation}{131}{. as external representation}
\entry{parenthesis, as external representation}{131}{parenthesis, as external representation}
\entry{dot, as external representation}{131}{dot, as external representation}
\entry{period, as external representation}{131}{period, as external representation}
\entry{external representation, for list}{131}{external representation, for list}
\entry{list, external representation}{131}{list, external representation}
\entry{external representation, for empty list}{131}{external representation, for empty list}
\entry{empty list, external representation}{131}{empty list, external representation}
\entry{improper list (defn)}{131}{improper list (defn)}
\entry{list, improper (defn)}{131}{list, improper (defn)}
\entry{type predicate, for pair}{132}{type predicate, for pair}
\entry{construction, of pair}{132}{construction, of pair}
\entry{selection, of pair component}{132}{selection, of pair component}
\entry{component selection, of pair}{132}{component selection, of pair}
\entry{copying, of tree}{134}{copying, of tree}
\entry{tree, copying}{134}{tree, copying}
\entry{construction, of list}{134}{construction, of list}
\entry{vector, converting to list}{135}{vector, converting to list}
\entry{selection, of list component}{136}{selection, of list component}
\entry{component selection, of list}{136}{component selection, of list}
\entry{type predicate, for list}{136}{type predicate, for list}
\entry{circular list}{136}{circular list}
\entry{type predicate, for empty list}{136}{type predicate, for empty list}
\entry{empty list, predicate for}{136}{empty list, predicate for}
\entry{index, of list (defn)}{137}{index, of list (defn)}
\entry{valid index, of list (defn)}{137}{valid index, of list (defn)}
\entry{list index (defn)}{137}{list index (defn)}
\entry{cutting, of list}{137}{cutting, of list}
\entry{pasting, of lists}{137}{pasting, of lists}
\entry{appending, of lists}{138}{appending, of lists}
\entry{filtering, of list}{139}{filtering, of list}
\entry{deletion, of list element}{139}{deletion, of list element}
\entry{searching, of list}{140}{searching, of list}
\entry{mapping, of list}{141}{mapping, of list}
\entry{folding, of list}{144}{folding, of list}
\entry{circular list}{146}{circular list}
\entry{construction, of circular list}{146}{construction, of circular list}
\entry{reversal, of list}{146}{reversal, of list}
\entry{total ordering (defn)}{146}{total ordering (defn)}
\entry{vector (defn)}{149}{vector (defn)}
\entry{length, of vector (defn)}{149}{length, of vector (defn)}
\entry{index, of vector (defn)}{149}{index, of vector (defn)}
\entry{valid index, of vector (defn)}{149}{valid index, of vector (defn)}
\entry{vector length (defn)}{149}{vector length (defn)}
\entry{vector index (defn)}{149}{vector index (defn)}
\entry{external representation, for vector}{149}{external representation, for vector}
\entry{#( as external representation}{149}{#( as external representation}
\entry{parenthesis, as external representation}{149}{parenthesis, as external representation}
\entry{subvector (defn)}{149}{subvector (defn)}
\entry{start, of subvector (defn)}{149}{start, of subvector (defn)}
\entry{end, of subvector (defn)}{149}{end, of subvector (defn)}
\entry{index, of subvector (defn)}{149}{index, of subvector (defn)}
\entry{valid index, of subvector (defn)}{149}{valid index, of subvector (defn)}
\entry{construction, of vector}{149}{construction, of vector}
\entry{copying, of vector}{149}{copying, of vector}
\entry{list, converting to vector}{149}{list, converting to vector}
\entry{growing, of vector}{150}{growing, of vector}
\entry{mapping, of vector}{150}{mapping, of vector}
\entry{selection, of vector component}{150}{selection, of vector component}
\entry{component selection, of vector}{150}{component selection, of vector}
\entry{type predicate, for vector}{150}{type predicate, for vector}
\entry{searching, of vector}{151}{searching, of vector}
\entry{cutting, of vector}{151}{cutting, of vector}
\entry{modification, of vector}{152}{modification, of vector}
\entry{filling, of vector}{152}{filling, of vector}
\entry{moving, of vector elements}{152}{moving, of vector elements}
\entry{bit string (defn)}{153}{bit string (defn)}
\entry{string, of bits (defn)}{153}{string, of bits (defn)}
\entry{length, of bit string (defn)}{153}{length, of bit string (defn)}
\entry{index, of bit string (defn)}{153}{index, of bit string (defn)}
\entry{valid index, of bit string (defn)}{153}{valid index, of bit string (defn)}
\entry{bit string length (defn)}{153}{bit string length (defn)}
\entry{bit string index (defn)}{153}{bit string index (defn)}
\entry{external representation, for bit string}{153}{external representation, for bit string}
\entry{#* as external representation}{153}{#* as external representation}
\entry{asterisk, as external representation}{153}{asterisk, as external representation}
\entry{construction, of bit string}{153}{construction, of bit string}
\entry{copying, of bit string}{153}{copying, of bit string}
\entry{type predicate, for bit string}{154}{type predicate, for bit string}
\entry{length, of bit string}{154}{length, of bit string}
\entry{selection, of bit string component}{154}{selection, of bit string component}
\entry{component selection, of bit string}{154}{component selection, of bit string}
\entry{searching, of bit string}{154}{searching, of bit string}
\entry{cutting, of bit string}{154}{cutting, of bit string}
\entry{pasting, of bit strings}{154}{pasting, of bit strings}
\entry{appending, of bit strings}{154}{appending, of bit strings}
\entry{substring, of bit string}{154}{substring, of bit string}
\entry{equivalence predicate, for bit strings}{155}{equivalence predicate, for bit strings}
\entry{comparison, of bit strings}{155}{comparison, of bit strings}
\entry{inverse, of bit string}{155}{inverse, of bit string}
\entry{modification, of bit string}{155}{modification, of bit string}
\entry{filling, of bit string}{155}{filling, of bit string}
\entry{moving, of bit string elements}{155}{moving, of bit string elements}
\entry{integer, converting to bit string}{156}{integer, converting to bit string}
\entry{#t as external representation}{157}{#t as external representation}
\entry{#f as external representation}{157}{#f as external representation}
\entry{boolean object (defn)}{157}{boolean object (defn)}
\entry{true, boolean object (defn)}{157}{true, boolean object (defn)}
\entry{false, boolean object (defn)}{157}{false, boolean object (defn)}
\entry{type predicate, for boolean}{157}{type predicate, for boolean}
\entry{false, predicate for}{157}{false, predicate for}
\entry{inverse, of boolean object}{157}{inverse, of boolean object}
\entry{boolean object, equivalence predicate}{157}{boolean object, equivalence predicate}
\entry{equivalence predicate, for boolean objects}{157}{equivalence predicate, for boolean objects}
\entry{comparison, of boolean objects}{157}{comparison, of boolean objects}
\entry{symbol (defn)}{158}{symbol (defn)}
\entry{interned symbol (defn)}{158}{interned symbol (defn)}
\entry{uninterned symbol (defn)}{158}{uninterned symbol (defn)}
\entry{property list, of symbol}{158}{property list, of symbol}
\entry{disembodied property list}{158}{disembodied property list}
\entry{case, of interned symbol}{158}{case, of interned symbol}
\entry{alphabetic case, of interned symbol}{158}{alphabetic case, of interned symbol}
\entry{external representation, for symbol}{158}{external representation, for symbol}
\entry{type predicate, for symbol}{159}{type predicate, for symbol}
\entry{name, of symbol}{159}{name, of symbol}
\entry{print name, of symbol}{159}{print name, of symbol}
\entry{interning, of symbols}{159}{interning, of symbols}
\entry{construction, of symbols}{159}{construction, of symbols}
\entry{string, interning as symbol}{159}{string, interning as symbol}
\entry{gensym (see uninterned symbol)}{160}{gensym (see uninterned symbol)}
\entry{appending, of symbols}{160}{appending, of symbols}
\entry{pasting, of symbols}{160}{pasting, of symbols}
\entry{hashing, of symbol}{161}{hashing, of symbol}
\entry{parameter, dynamic (defn)}{161}{parameter, dynamic (defn)}
\entry{dynamic parameter (defn)}{161}{dynamic parameter (defn)}
\entry{cell (defn)}{162}{cell (defn)}
\entry{type predicate, for cell}{162}{type predicate, for cell}
\entry{construction, of cell}{162}{construction, of cell}
\entry{selection, of cell component}{162}{selection, of cell component}
\entry{component selection, of cell}{162}{component selection, of cell}
\entry{record-type descriptor (defn)}{162}{record-type descriptor (defn)}
\entry{type predicate, for record}{163}{type predicate, for record}
\entry{type predicate, for record type}{164}{type predicate, for record type}
\entry{promise (defn)}{164}{promise (defn)}
\entry{promise, construction}{164}{promise, construction}
\entry{construction, of promise}{164}{construction, of promise}
\entry{lazy evaluation (defn)}{164}{lazy evaluation (defn)}
\entry{call by need evaluation (defn)}{164}{call by need evaluation (defn)}
\entry{evaluation, lazy (defn)}{164}{evaluation, lazy (defn)}
\entry{evaluation, call by need (defn)}{164}{evaluation, call by need (defn)}
\entry{promise, forcing}{164}{promise, forcing}
\entry{forcing, of promise}{164}{forcing, of promise}
\entry{memoization, of promise}{164}{memoization, of promise}
\entry{type predicate, for promise}{165}{type predicate, for promise}
\entry{stream (defn)}{166}{stream (defn)}
\entry{construction, of stream}{166}{construction, of stream}
\entry{list, converting to stream}{166}{list, converting to stream}
\entry{stream, converting to list}{166}{stream, converting to list}
\entry{type predicate, for stream pair}{166}{type predicate, for stream pair}
\entry{empty stream, predicate for}{167}{empty stream, predicate for}
\entry{length, of stream}{167}{length, of stream}
\entry{selecting, of stream component}{167}{selecting, of stream component}
\entry{component selection, of stream}{167}{component selection, of stream}
\entry{mapping, of stream}{167}{mapping, of stream}
\entry{weak reference (defn)}{167}{weak reference (defn)}
\entry{strong reference (defn)}{167}{strong reference (defn)}
\entry{reference, weak (defn)}{167}{reference, weak (defn)}
\entry{reference, strong (defn)}{167}{reference, strong (defn)}
\entry{weak pair (defn)}{168}{weak pair (defn)}
\entry{pair, weak (defn)}{168}{pair, weak (defn)}
\entry{type predicate, for weak pair}{168}{type predicate, for weak pair}
\entry{construction, of weak pair}{168}{construction, of weak pair}
\entry{selection, of weak pair component}{169}{selection, of weak pair component}
\entry{component selection, of weak pair}{169}{component selection, of weak pair}
\entry{ephemeron (defn)}{169}{ephemeron (defn)}
\entry{ephemeron, broken}{169}{ephemeron, broken}
\entry{broken ephemeron}{169}{broken ephemeron}
\entry{type predicate, for ephemeron}{169}{type predicate, for ephemeron}
\entry{construction, of ephemeron}{169}{construction, of ephemeron}
\entry{selection, of ephemeron component}{169}{selection, of ephemeron component}
\entry{component selection, of ephemeron}{169}{component selection, of ephemeron}
\entry{reference barrier}{170}{reference barrier}
\entry{barrier, reference}{170}{barrier, reference}
\entry{property list}{171}{property list}
\entry{association list (defn)}{171}{association list (defn)}
\entry{list, association (defn)}{171}{list, association (defn)}
\entry{alist (defn)}{171}{alist (defn)}
\entry{key, of association list element (defn)}{171}{key, of association list element (defn)}
\entry{type predicate, for alist}{172}{type predicate, for alist}
\entry{searching, of alist}{172}{searching, of alist}
\entry{deletion, of alist element}{172}{deletion, of alist element}
\entry{copying, of alist}{173}{copying, of alist}
\entry{1D table (defn)}{173}{1D table (defn)}
\entry{one-dimensional table (defn)}{173}{one-dimensional table (defn)}
\entry{table, one-dimensional (defn)}{173}{table, one-dimensional (defn)}
\entry{weak pair, and 1D table}{173}{weak pair, and 1D table}
\entry{property list}{173}{property list}
\entry{type predicate, for 1D table}{174}{type predicate, for 1D table}
\entry{association table (defn)}{174}{association table (defn)}
\entry{table, association (defn)}{174}{table, association (defn)}
\entry{property list}{174}{property list}
\entry{hash table}{175}{hash table}
\entry{construction, of hash table}{175}{construction, of hash table}
\entry{equivalence predicate, of hash table}{176}{equivalence predicate, of hash table}
\entry{strongly held keys, of hash table}{176}{strongly held keys, of hash table}
\entry{weakly held keys, of hash table}{176}{weakly held keys, of hash table}
\entry{ephemerally held keys, of hash table}{176}{ephemerally held keys, of hash table}
\entry{strongly held data, of hash table}{176}{strongly held data, of hash table}
\entry{weakly held data, of hash table}{176}{weakly held data, of hash table}
\entry{ephemerally held data, of hash table}{176}{ephemerally held data, of hash table}
\entry{hashing, of key in hash table}{178}{hashing, of key in hash table}
\entry{modulus, of hashing procedure}{178}{modulus, of hashing procedure}
\entry{type predicate, for hash table}{181}{type predicate, for hash table}
\entry{resizing, of hash table}{183}{resizing, of hash table}
\entry{size, of hash table (defn)}{183}{size, of hash table (defn)}
\entry{usable size, of hash table (defn)}{183}{usable size, of hash table (defn)}
\entry{physical size, of hash table (defn)}{183}{physical size, of hash table (defn)}
\entry{initial size, of hash table}{184}{initial size, of hash table}
\entry{rehash size, of hash table (defn)}{184}{rehash size, of hash table (defn)}
\entry{rehash threshold, of hash table (defn)}{185}{rehash threshold, of hash table (defn)}
\entry{address hashing}{185}{address hashing}
\entry{object hashing}{186}{object hashing}
\entry{hashing, of object}{186}{hashing, of object}
\entry{trees, balanced binary}{187}{trees, balanced binary}
\entry{balanced binary trees}{187}{balanced binary trees}
\entry{binary trees}{187}{binary trees}
\entry{red-black binary trees}{187}{red-black binary trees}
\entry{trees, balanced binary}{191}{trees, balanced binary}
\entry{balanced binary trees}{191}{balanced binary trees}
\entry{binary trees}{191}{binary trees}
\entry{weight-balanced binary trees}{191}{weight-balanced binary trees}
\entry{binary trees, as sets}{191}{binary trees, as sets}
\entry{binary trees, as discrete maps}{191}{binary trees, as discrete maps}
\entry{sets, using binary trees}{191}{sets, using binary trees}
\entry{discrete maps, using binary trees}{191}{discrete maps, using binary trees}
\entry{procedure}{205}{procedure}
\entry{primitive procedure (defn)}{205}{primitive procedure (defn)}
\entry{built-in procedure}{205}{built-in procedure}
\entry{application hook (defn)}{205}{application hook (defn)}
\entry{hook, application (defn)}{205}{hook, application (defn)}
\entry{procedure, type}{205}{procedure, type}
\entry{procedure, primitive}{205}{procedure, primitive}
\entry{procedure, interpreted}{205}{procedure, interpreted}
\entry{procedure, compiled}{205}{procedure, compiled}
\entry{type, of procedure}{205}{type, of procedure}
\entry{primitive, procedure type}{205}{primitive, procedure type}
\entry{interpreted, procedure type}{205}{interpreted, procedure type}
\entry{compiled, procedure type}{205}{compiled, procedure type}
\entry{external representation, for procedure}{205}{external representation, for procedure}
\entry{compound procedure}{205}{compound procedure}
\entry{procedure, compound}{205}{procedure, compound}
\entry{application, of procedure}{205}{application, of procedure}
\entry{type predicate, for procedure}{205}{type predicate, for procedure}
\entry{type predicate, for compiled procedure}{206}{type predicate, for compiled procedure}
\entry{type predicate, for compound procedure}{206}{type predicate, for compound procedure}
\entry{type predicate, for primitive procedure}{206}{type predicate, for primitive procedure}
\entry{arity}{206}{arity}
\entry{continuation}{208}{continuation}
\entry{construction, of continuation}{208}{construction, of continuation}
\entry{procedure, escape (defn)}{208}{procedure, escape (defn)}
\entry{escape procedure (defn)}{208}{escape procedure (defn)}
\entry{non-local exit}{208}{non-local exit}
\entry{exit, non-local}{208}{exit, non-local}
\entry{type predicate, for continuation}{209}{type predicate, for continuation}
\entry{continuation, alternate invocation}{209}{continuation, alternate invocation}
\entry{escape procedure, alternate invocation}{209}{escape procedure, alternate invocation}
\entry{multiple values, from procedure}{210}{multiple values, from procedure}
\entry{values, multiple}{210}{values, multiple}
\entry{application hook (defn)}{210}{application hook (defn)}
\entry{procedure, of application hook}{210}{procedure, of application hook}
\entry{extra object, of application hook}{210}{extra object, of application hook}
\entry{apply hook (defn)}{210}{apply hook (defn)}
\entry{entity (defn)}{210}{entity (defn)}
\entry{type predicate, for apply hook}{211}{type predicate, for apply hook}
\entry{type predicate, for entity}{211}{type predicate, for entity}
\entry{variable binding}{213}{variable binding}
\entry{binding, variable}{213}{binding, variable}
\entry{unassigned binding}{213}{unassigned binding}
\entry{binding, unassigned}{213}{binding, unassigned}
\entry{keyword binding}{213}{keyword binding}
\entry{syntactic keyword binding}{213}{syntactic keyword binding}
\entry{binding, syntactic keyword}{213}{binding, syntactic keyword}
\entry{type predicate, for environment}{213}{type predicate, for environment}
\entry{s-expression}{215}{s-expression}
\entry{evaluation, of s-expression}{215}{evaluation, of s-expression}
\entry{top-level environment}{216}{top-level environment}
\entry{interpreter environment}{216}{interpreter environment}
\entry{environment, top-level}{216}{environment, top-level}
\entry{environment, interpreter}{216}{environment, interpreter}
\entry{current environment}{216}{current environment}
\entry{environment, current}{216}{environment, current}
\entry{type predicate, for top-level environment}{216}{type predicate, for top-level environment}
\entry{input}{217}{input}
\entry{output}{217}{output}
\entry{port}{217}{port}
\entry{port (defn)}{217}{port (defn)}
\entry{input port (defn)}{217}{input port (defn)}
\entry{output port (defn)}{217}{output port (defn)}
\entry{textual port (defn)}{217}{textual port (defn)}
\entry{binary port (defn)}{217}{binary port (defn)}
\entry{type predicate, for port}{218}{type predicate, for port}
\entry{current input port (defn)}{218}{current input port (defn)}
\entry{input port, current (defn)}{218}{input port, current (defn)}
\entry{current output port (defn)}{218}{current output port (defn)}
\entry{output port, current (defn)}{218}{output port, current (defn)}
\entry{current error port (defn)}{218}{current error port (defn)}
\entry{error port, current (defn)}{218}{error port, current (defn)}
\entry{port, current}{218}{port, current}
\entry{current notification port (defn)}{218}{current notification port (defn)}
\entry{notification port, current (defn)}{218}{notification port, current (defn)}
\entry{current tracing output port (defn)}{218}{current tracing output port (defn)}
\entry{tracing output port, current (defn)}{218}{tracing output port, current (defn)}
\entry{current interaction port (defn)}{218}{current interaction port (defn)}
\entry{interaction port, current (defn)}{218}{interaction port, current (defn)}
\entry{closing, of port}{219}{closing, of port}
\entry{port, console}{219}{port, console}
\entry{console, port}{219}{console, port}
\entry{input port, console}{219}{input port, console}
\entry{output port, console}{219}{output port, console}
\entry{file, input and output ports}{219}{file, input and output ports}
\entry{port, file}{219}{port, file}
\entry{input port, file}{219}{input port, file}
\entry{output port, file}{219}{output port, file}
\entry{I/O, to files}{219}{I/O, to files}
\entry{current input port, rebinding}{220}{current input port, rebinding}
\entry{current output port, rebinding}{220}{current output port, rebinding}
\entry{appending, to output file}{220}{appending, to output file}
\entry{closing, of file port}{221}{closing, of file port}
\entry{string, input and output ports}{221}{string, input and output ports}
\entry{port, string}{221}{port, string}
\entry{input port, string}{221}{input port, string}
\entry{output port, string}{221}{output port, string}
\entry{I/O, to strings}{221}{I/O, to strings}
\entry{bytevector, input and output ports}{223}{bytevector, input and output ports}
\entry{port, bytevector}{223}{port, bytevector}
\entry{input port, bytevector}{223}{input port, bytevector}
\entry{output port, bytevector}{223}{output port, bytevector}
\entry{I/O, to bytevectors}{223}{I/O, to bytevectors}
\entry{input operations}{223}{input operations}
\entry{interactive input ports (defn)}{223}{interactive input ports (defn)}
\entry{expression, input from port}{223}{expression, input from port}
\entry{external representation, parsing}{223}{external representation, parsing}
\entry{parsing, of external representation}{223}{parsing, of external representation}
\entry{character, input from port}{224}{character, input from port}
\entry{type predicate, for EOF object}{225}{type predicate, for EOF object}
\entry{EOF object, predicate for}{225}{EOF object, predicate for}
\entry{end of file object (see EOF object)}{225}{end of file object (see EOF object)}
\entry{file, end-of-file marker (see EOF object)}{225}{file, end-of-file marker (see EOF object)}
\entry{EOF object, construction}{225}{EOF object, construction}
\entry{construction, of EOF object}{225}{construction, of EOF object}
\entry{string, input from port}{227}{string, input from port}
\entry{output procedures}{228}{output procedures}
\entry{buffering, of output}{228}{buffering, of output}
\entry{flushing, of buffered output}{228}{flushing, of buffered output}
\entry{discretionary flushing, of buffered output}{228}{discretionary flushing, of buffered output}
\entry{console, ringing the bell}{230}{console, ringing the bell}
\entry{ringing the console bell}{230}{ringing the console bell}
\entry{bell, ringing on console}{230}{bell, ringing on console}
\entry{console, clearing}{230}{console, clearing}
\entry{display, clearing}{230}{display, clearing}
\entry{screen, clearing}{230}{screen, clearing}
\entry{terminal screen, clearing}{230}{terminal screen, clearing}
\entry{clearing the console screen}{230}{clearing the console screen}
\entry{pretty printer}{230}{pretty printer}
\entry{blocking mode, of port}{232}{blocking mode, of port}
\entry{terminal mode, of port}{233}{terminal mode, of port}
\entry{cooked mode, of terminal port}{233}{cooked mode, of terminal port}
\entry{raw mode, of terminal port}{233}{raw mode, of terminal port}
\entry{run-time-loadable option}{234}{run-time-loadable option}
\entry{option, run-time-loadable}{234}{option, run-time-loadable}
\entry{format directive (defn)}{234}{format directive (defn)}
\entry{directive, format (defn)}{234}{directive, format (defn)}
\entry{V as format parameter}{235}{V as format parameter}
\entry{# as format parameter}{235}{# as format parameter}
\entry{#[ as external representation}{236}{#[ as external representation}
\entry{prompting}{237}{prompting}
\entry{textual port primitives}{239}{textual port primitives}
\entry{textual port type}{239}{textual port type}
\entry{standard operations, on textual port}{239}{standard operations, on textual port}
\entry{custom operations, on textual port}{239}{custom operations, on textual port}
\entry{construction, of textual port type}{240}{construction, of textual port type}
\entry{textual input port operations}{241}{textual input port operations}
\entry{character, input from textual port}{241}{character, input from textual port}
\entry{string, input from textual port}{241}{string, input from textual port}
\entry{textual output port operations}{243}{textual output port operations}
\entry{character, output to textual port}{243}{character, output to textual port}
\entry{substring, output to textual port}{243}{substring, output to textual port}
\entry{Parser buffer}{244}{Parser buffer}
\entry{Parser-buffer pointer}{244}{Parser-buffer pointer}
\entry{Parser language}{247}{Parser language}
\entry{Backtracking, in parser language}{247}{Backtracking, in parser language}
\entry{run-time-loadable option}{249}{run-time-loadable option}
\entry{option, run-time-loadable}{249}{option, run-time-loadable}
\entry{Matcher language}{249}{Matcher language}
\entry{Matcher procedure}{249}{Matcher procedure}
\entry{Parser language}{252}{Parser language}
\entry{Parser procedure}{252}{Parser procedure}
\entry{run-time-loadable option}{256}{run-time-loadable option}
\entry{option, run-time-loadable}{256}{option, run-time-loadable}
\entry{XML parser}{256}{XML parser}
\entry{parser, XML}{256}{parser, XML}
\entry{XML input}{256}{XML input}
\entry{input, XML}{256}{input, XML}
\entry{XML output}{257}{XML output}
\entry{output, XML}{257}{output, XML}
\entry{XML names}{258}{XML names}
\entry{names, XML}{258}{names, XML}
\entry{Uniform Resource Identifier}{258}{Uniform Resource Identifier}
\entry{URI, of XML name}{258}{URI, of XML name}
\entry{qname, of XML name}{258}{qname, of XML name}
\entry{prefix, of XML name}{259}{prefix, of XML name}
\entry{local part, of XML name}{259}{local part, of XML name}
\entry{comparison, of XML names}{259}{comparison, of XML names}
\entry{equality, of XML names}{259}{equality, of XML names}
\entry{Operating-System Interface}{265}{Operating-System Interface}
\entry{pathname}{265}{pathname}
\entry{current working directory}{265}{current working directory}
\entry{file name}{265}{file name}
\entry{filename (defn)}{265}{filename (defn)}
\entry{pathname (defn)}{265}{pathname (defn)}
\entry{host, in filename}{265}{host, in filename}
\entry{construction, of pathname}{266}{construction, of pathname}
\entry{construction, of pathname}{266}{construction, of pathname}
\entry{conversion, pathname to string}{266}{conversion, pathname to string}
\entry{simplification, of pathname}{266}{simplification, of pathname}
\entry{components, of pathname}{267}{components, of pathname}
\entry{pathname components}{267}{pathname components}
\entry{host, pathname component}{267}{host, pathname component}
\entry{device, pathname component}{267}{device, pathname component}
\entry{directory, pathname component}{267}{directory, pathname component}
\entry{name, pathname component}{267}{name, pathname component}
\entry{type, pathname component}{267}{type, pathname component}
\entry{version, pathname component}{267}{version, pathname component}
\entry{root, as pathname component}{268}{root, as pathname component}
\entry{directory path (defn)}{268}{directory path (defn)}
\entry{path, directory (defn)}{268}{path, directory (defn)}
\entry{up, as pathname component}{268}{up, as pathname component}
\entry{parent, of directory}{268}{parent, of directory}
\entry{newest, as pathname component}{269}{newest, as pathname component}
\entry{oldest, as pathname component}{269}{oldest, as pathname component}
\entry{installed, as pathname component}{269}{installed, as pathname component}
\entry{construction, of pathname}{269}{construction, of pathname}
\entry{type predicate, for pathname}{270}{type predicate, for pathname}
\entry{equivalence predicate, for pathnames}{270}{equivalence predicate, for pathnames}
\entry{merging, of pathnames}{271}{merging, of pathnames}
\entry{defaulting, of pathname}{271}{defaulting, of pathname}
\entry{defaulting, of pathname}{271}{defaulting, of pathname}
\entry{conversion, pathname to string}{272}{conversion, pathname to string}
\entry{selection, components of pathname}{272}{selection, components of pathname}
\entry{file, converting pathname directory to}{273}{file, converting pathname directory to}
\entry{directory, converting pathname to}{273}{directory, converting pathname to}
\entry{directory, reading}{273}{directory, reading}
\entry{type predicate, for pathname host}{273}{type predicate, for pathname host}
\entry{equivalence predicate, for pathname host}{273}{equivalence predicate, for pathname host}
\entry{home directory, as pathname}{273}{home directory, as pathname}
\entry{home directory, as pathname}{273}{home directory, as pathname}
\entry{library, system pathname}{274}{library, system pathname}
\entry{library, system pathname}{274}{library, system pathname}
\entry{absolute pathname (defn)}{274}{absolute pathname (defn)}
\entry{pathname, absolute (defn)}{274}{pathname, absolute (defn)}
\entry{relative pathname (defn)}{274}{relative pathname (defn)}
\entry{pathname, relative (defn)}{274}{pathname, relative (defn)}
\entry{directory, current working (defn)}{274}{directory, current working (defn)}
\entry{current working directory (defn)}{274}{current working directory (defn)}
\entry{working directory (see current working directory)}{274}{working directory (see current working directory)}
\entry{existence, testing of file}{276}{existence, testing of file}
\entry{copying, of file}{276}{copying, of file}
\entry{renaming, of file}{276}{renaming, of file}
\entry{name, of file}{276}{name, of file}
\entry{deletion, of file}{276}{deletion, of file}
\entry{linking (hard), of file}{276}{linking (hard), of file}
\entry{hard linking, of file}{276}{hard linking, of file}
\entry{linking (soft), of file}{276}{linking (soft), of file}
\entry{soft linking, of file}{276}{soft linking, of file}
\entry{symbolic linking, of file}{276}{symbolic linking, of file}
\entry{truename, of input file}{277}{truename, of input file}
\entry{directory, predicate for}{277}{directory, predicate for}
\entry{file (regular), predicate for}{277}{file (regular), predicate for}
\entry{regular file, predicate for}{277}{regular file, predicate for}
\entry{symbolic link, predicate for}{278}{symbolic link, predicate for}
\entry{file type, procedure for}{278}{file type, procedure for}
\entry{modification time, of file}{279}{modification time, of file}
\entry{access time, of file}{279}{access time, of file}
\entry{attribute, of file}{280}{attribute, of file}
\entry{directory, reading}{281}{directory, reading}
\entry{universal time}{281}{universal time}
\entry{time, universal}{281}{time, universal}
\entry{decoded time}{282}{decoded time}
\entry{time, decoded}{282}{time, decoded}
\entry{file time}{282}{file time}
\entry{time, file}{282}{time, file}
\entry{time, string}{282}{time, string}
\entry{tick}{289}{tick}
\entry{subprocess}{291}{subprocess}
\entry{synchronous subprocess}{291}{synchronous subprocess}
\entry{socket}{295}{socket}
\entry{client socket}{295}{client socket}
\entry{server socket}{295}{server socket}
\entry{hostname, TCP}{295}{hostname, TCP}
\entry{service, TCP}{295}{service, TCP}
\entry{port number, TCP}{295}{port number, TCP}
\entry{server socket}{296}{server socket}
\entry{loopback interface}{298}{loopback interface}
\entry{taxonomical link, of condition type (defn)}{299}{taxonomical link, of condition type (defn)}
\entry{specialization, of condition types (defn)}{299}{specialization, of condition types (defn)}
\entry{generalization, of condition types (defn)}{299}{generalization, of condition types (defn)}
\entry{condition type}{300}{condition type}
\entry{type, condition}{300}{type, condition}
\entry{specialization, of condition types}{300}{specialization, of condition types}
\entry{generalization, of condition types}{300}{generalization, of condition types}
\entry{condition signalling (defn)}{300}{condition signalling (defn)}
\entry{signalling, of condition (defn)}{300}{signalling, of condition (defn)}
\entry{REP loop}{301}{REP loop}
\entry{generalization, of condition types}{301}{generalization, of condition types}
\entry{specialization, of condition types}{301}{specialization, of condition types}
\entry{REP loop}{301}{REP loop}
\entry{error messages, conventions}{302}{error messages, conventions}
\entry{conventions for error messages}{302}{conventions for error messages}
\entry{handler, condition (defn)}{303}{handler, condition (defn)}
\entry{condition handler (defn)}{303}{condition handler (defn)}
\entry{REP loop}{303}{REP loop}
\entry{REP loop}{304}{REP loop}
\entry{REP loop}{304}{REP loop}
\entry{dynamic binding}{304}{dynamic binding}
\entry{REP loop}{304}{REP loop}
\entry{dynamic binding}{305}{dynamic binding}
\entry{restart effector (defn)}{305}{restart effector (defn)}
\entry{effector, restart (defn)}{305}{effector, restart (defn)}
\entry{restart (defn)}{305}{restart (defn)}
\entry{protocol, restart (defn)}{305}{protocol, restart (defn)}
\entry{restart protocol}{305}{restart protocol}
\entry{REP loop}{308}{REP loop}
\entry{condition (defn)}{310}{condition (defn)}
\entry{condition instance (defn)}{310}{condition instance (defn)}
\entry{instance, of condition (defn)}{310}{instance, of condition (defn)}
\entry{bound-restarts}{311}{bound-restarts}
\entry{restarts, bound}{311}{restarts, bound}
\entry{specialization, of condition types}{311}{specialization, of condition types}
\entry{specialization, of condition types}{312}{specialization, of condition types}
\entry{specialization, of condition types}{312}{specialization, of condition types}
\entry{bound-restarts}{313}{bound-restarts}
\entry{restarts, bound}{313}{restarts, bound}
\entry{condition type}{313}{condition type}
\entry{type, of condition}{313}{type, of condition}
\entry{generalization, of condition types}{313}{generalization, of condition types}
\entry{specialization, of condition types}{314}{specialization, of condition types}
\entry{generalization, of condition types}{314}{generalization, of condition types}
\entry{generalization, of condition types}{314}{generalization, of condition types}
\entry{graphics}{323}{graphics}
\entry{graphics, opening and closing devices}{323}{graphics, opening and closing devices}
\entry{graphics, coordinate systems}{324}{graphics, coordinate systems}
\entry{coordinates, graphics}{324}{coordinates, graphics}
\entry{device coordinates, graphics (defn)}{324}{device coordinates, graphics (defn)}
\entry{graphics, device coordinates (defn)}{324}{graphics, device coordinates (defn)}
\entry{virtual coordinates, graphics (defn)}{324}{virtual coordinates, graphics (defn)}
\entry{graphics, virtual coordinates (defn)}{324}{graphics, virtual coordinates (defn)}
\entry{graphics, drawing}{324}{graphics, drawing}
\entry{graphics, cursor (defn)}{325}{graphics, cursor (defn)}
\entry{cursor, graphics (defn)}{325}{cursor, graphics (defn)}
\entry{graphics, output characteristics}{326}{graphics, output characteristics}
\entry{drawing mode, graphics (defn)}{326}{drawing mode, graphics (defn)}
\entry{graphics, drawing mode (defn)}{326}{graphics, drawing mode (defn)}
\entry{line style, graphics (defn)}{326}{line style, graphics (defn)}
\entry{graphics, line style (defn)}{326}{graphics, line style (defn)}
\entry{buffering, of graphics output}{327}{buffering, of graphics output}
\entry{graphics, buffering of output}{327}{graphics, buffering of output}
\entry{graphics, clipping}{328}{graphics, clipping}
\entry{clipping, of graphics}{328}{clipping, of graphics}
\entry{clip rectangle, graphics (defn)}{328}{clip rectangle, graphics (defn)}
\entry{custom operations, on graphics device}{328}{custom operations, on graphics device}
\entry{graphics, custom operations}{328}{graphics, custom operations}
\entry{graphics, images}{328}{graphics, images}
\entry{images, graphics}{328}{images, graphics}
\entry{graphics, bitmaps}{328}{graphics, bitmaps}
\entry{bitmaps, graphics}{328}{bitmaps, graphics}
\entry{X graphics}{329}{X graphics}
\entry{X window system}{329}{X window system}
\entry{resources, X graphics}{330}{resources, X graphics}
\entry{X resources, graphics}{330}{X resources, graphics}
\entry{display, X graphics}{330}{display, X graphics}
\entry{X display, graphics}{330}{X display, graphics}
\entry{geometry string, X graphics}{331}{geometry string, X graphics}
\entry{X geometry string, graphics}{331}{X geometry string, graphics}
\entry{drawing arcs and circles, graphics}{331}{drawing arcs and circles, graphics}
\entry{graphics, drawing arcs and circles}{331}{graphics, drawing arcs and circles}
\entry{circles, drawing}{331}{circles, drawing}
\entry{drawing arcs and circles, graphics}{332}{drawing arcs and circles, graphics}
\entry{graphics, drawing arcs and circles}{332}{graphics, drawing arcs and circles}
\entry{circles, drawing}{332}{circles, drawing}
\entry{Win32 graphics}{334}{Win32 graphics}
\entry{color}{335}{color}
\entry{ellipse, graphics}{335}{ellipse, graphics}
\entry{circle, graphics}{335}{circle, graphics}
\entry{graphics, ellipse}{335}{graphics, ellipse}
\entry{graphics, circle}{335}{graphics, circle}
\entry{bitmaps}{336}{bitmaps}
\entry{printing graphics output}{336}{printing graphics output}
\entry{limitations}{337}{limitations}
\entry{limitations}{337}{limitations}
\entry{warning}{337}{warning}
\entry{Windows types}{338}{Windows types}
\entry{foreign type declarations}{338}{foreign type declarations}
\entry{types, Windows}{338}{types, Windows}
\entry{defining foreign types}{338}{defining foreign types}
\entry{defining foreign types}{338}{defining foreign types}
\entry{loading DLLs}{340}{loading DLLs}
\entry{DLL, loading}{340}{DLL, loading}
\entry{DLL, GDI32.DLL}{341}{DLL, GDI32.DLL}
\entry{DLL, KERNEL32.DLL}{341}{DLL, KERNEL32.DLL}
\entry{DLL, USER32.DLL}{341}{DLL, USER32.DLL}
\entry{defining foreign procedures}{341}{defining foreign procedures}
\entry{Win32 API names}{342}{Win32 API names}
\entry{naming conventions}{342}{naming conventions}
\entry{DLL, DIBUTILS.DLL}{343}{DLL, DIBUTILS.DLL}
\entry{DLL, exports}{345}{DLL, exports}
\entry{FDL, GNU Free Documentation License}{361}{FDL, GNU Free Documentation License}
