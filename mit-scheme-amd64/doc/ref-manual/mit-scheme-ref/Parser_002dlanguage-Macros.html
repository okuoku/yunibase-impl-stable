<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Parser-language Macros (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Parser-language Macros (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Parser-language Macros (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parser-Language.html" rel="up" title="Parser Language">
<link href="XML-Support.html" rel="next" title="XML Support">
<link href="_002aParser.html" rel="prev" title="*Parser">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Parser_002dlanguage-Macros"></span><div class="header">
<p>
Previous: <a href="_002aParser.html" accesskey="p" rel="prev">*Parser</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Parser_002dlanguage-Macros-1"></span><h4 class="subsection">14.14.3 Parser-language Macros</h4>

<p>The parser and matcher languages provide a macro facility so that
common patterns can be abstracted.  The macro facility allows new
expression types to be independently defined in the two languages.
The macros are defined in hierarchically organized tables, so that
different applications can have private macro bindings.
</p>
<dl>
<dt id="index-define_002d_002amatcher_002dmacro">special form: <strong>define-*matcher-macro</strong> <em>formals expression</em></dt>
<dt id="index-define_002d_002aparser_002dmacro">special form: <strong>define-*parser-macro</strong> <em>formals expression</em></dt>
<dd><p>These special forms are used to define macros in the matcher and
parser language, respectively.  <var>Formals</var> is like the
<var>formals</var> list of a <code>define</code> special form, and
<var>expression</var> is a Scheme expression.
</p>
<p>If <var>formals</var> is a list (or improper list) of symbols, the first
symbol in the list is the name of the macro, and the remaining symbols
are interpreted as the <var>formals</var> of a lambda expression.  A lambda
expression is formed by combining the latter <var>formals</var> with the
<var>expression</var>, and this lambda expression, when evaluated, becomes
the <em>expander</em>.  The defined macro accepts the same number of
operands as the expander.  A macro instance is expanded by applying
the expander to the list of operands; the result of the application is
interpreted as a replacement expression for the macro instance.
</p>
<p>If <var>formals</var> is a symbol, it is the name of the macro.  In this
case, the expander is a procedure of no arguments whose body is
<var>expression</var>.  When the <var>formals</var> symbol appears by itself as
an expression in the language, the expander is called with no
arguments, and the result is interpreted as a replacement expression
for the symbol.
</p></dd></dl>

<dl>
<dt id="index-define_002d_002amatcher_002dexpander">procedure: <strong>define-*matcher-expander</strong> <em>identifier expander</em></dt>
<dt id="index-define_002d_002aparser_002dexpander">procedure: <strong>define-*parser-expander</strong> <em>identifier expander</em></dt>
<dd><p>These procedures provide a procedural interface to the
macro-definition mechanism.  <var>Identifier</var> must be a symbol, and
<var>expander</var> must be an expander procedure, as defined above.
Instances of the <code>define-*matcher-macro</code> and
<code>define-*parser-macro</code> special forms expand into calls to these
procedures.
</p></dd></dl>

<p>The remaining procedures define the interface to the parser-macros
table abstraction.  Each parser-macro table has a separate binding
space for macros in the matcher and parser languages.  However, the
table inherits bindings from one specified table; it&rsquo;s not possible to
inherit matcher-language bindings from one table and parser-language
bindings from another.
</p>
<dl>
<dt id="index-make_002dparser_002dmacros">procedure: <strong>make-parser-macros</strong> <em>parent-table</em></dt>
<dd><p>Create and return a new parser-macro table that inherits from
<var>parent-table</var>.  <var>Parent-table</var> must be either a parser-macro
table, or <code>#f</code>; usually it is specified as the value of
<code>global-parser-macros</code>.
</p></dd></dl>

<dl>
<dt id="index-parser_002dmacros_003f">procedure: <strong>parser-macros?</strong> <em>object</em></dt>
<dd><p>This is a predicate for parser-macro tables.
</p></dd></dl>

<dl>
<dt id="index-global_002dparser_002dmacros">procedure: <strong>global-parser-macros</strong></dt>
<dd><p>Return the global parser-macro table.  This table is predefined and
contains all of the bindings documented here.
</p></dd></dl>

<p>There is a &ldquo;current&rdquo; table at all times, and macro definitions are
always placed in this table.  By default, the current table is the
global macro table, but the following procedures allow this to be
changed.
</p>
<dl>
<dt id="index-current_002dparser_002dmacros">procedure: <strong>current-parser-macros</strong></dt>
<dd><p>Return the current parser-macro table.
</p></dd></dl>

<dl>
<dt id="index-set_002dcurrent_002dparser_002dmacros_0021">procedure: <strong>set-current-parser-macros!</strong> <em>table</em></dt>
<dd><p>Change the current parser-macro table to <var>table</var>, which must
satisfy <code>parser-macros?</code>.
</p></dd></dl>

<dl>
<dt id="index-with_002dcurrent_002dparser_002dmacros">procedure: <strong>with-current-parser-macros</strong> <em>table thunk</em></dt>
<dd><p>Bind the current parser-macro table to <var>table</var>, call <var>thunk</var>
with no arguments, then restore the original table binding.  The value
returned by <var>thunk</var> is the returned as the value of this
procedure.  <var>Table</var> must satisfy <code>parser-macros?</code>, and
<var>thunk</var> must be a procedure of no arguments.
</p></dd></dl>


<hr>
<div class="header">
<p>
Previous: <a href="_002aParser.html" accesskey="p" rel="prev">*Parser</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
