<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Operations on Pathnames (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Operations on Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Operations on Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pathnames.html" rel="up" title="Pathnames">
<link href="Miscellaneous-Pathnames.html" rel="next" title="Miscellaneous Pathnames">
<link href="Components-of-Pathnames.html" rel="prev" title="Components of Pathnames">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Operations-on-Pathnames"></span><div class="header">
<p>
Next: <a href="Miscellaneous-Pathnames.html" accesskey="n" rel="next">Miscellaneous Pathnames</a>, Previous: <a href="Components-of-Pathnames.html" accesskey="p" rel="prev">Components of Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Operations-on-Pathnames-1"></span><h4 class="subsection">15.1.3 Operations on Pathnames</h4>

<dl>
<dt id="index-pathname_003f">procedure: <strong>pathname?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-pathname"></span>
<p>Returns <code>#t</code> if <var>object</var> is a pathname; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-pathname_003d_003f">procedure: <strong>pathname=?</strong> <em>pathname1 pathname2</em></dt>
<dd><span id="index-equivalence-predicate_002c-for-pathnames"></span>
<p>Returns <code>#t</code> if <var>pathname1</var> is equivalent to <var>pathname2</var>;
otherwise returns <code>#f</code>.
Pathnames are equivalent if all of their components are equivalent,
hence two pathnames that are equivalent must identify the same file or
equivalent partial pathnames.
However, the converse is not true: non-equivalent pathnames may specify
the same file (e.g. via absolute and relative directory components),
and pathnames that specify no file at all (e.g. name and directory
components unspecified) may be equivalent.
</p></dd></dl>

<dl>
<dt id="index-pathname_002dabsolute_003f">procedure: <strong>pathname-absolute?</strong> <em>pathname</em></dt>
<dd><p>Returns <code>#t</code> if <var>pathname</var> is an absolute rather than relative
pathname object; otherwise returns <code>#f</code>.  Specifically, this
procedure returns <code>#t</code> when the directory component of
<var>pathname</var> is a list starting with the symbol <code>absolute</code>, and
returns <code>#f</code> in all other cases.  All pathnames are either absolute
or relative, so if this procedure returns <code>#f</code>, the argument is a
relative pathname.
</p></dd></dl>

<dl>
<dt id="index-directory_002dpathname_003f">procedure: <strong>directory-pathname?</strong> <em>pathname</em></dt>
<dd><p>Returns <code>#t</code> if <var>pathname</var> has only directory components and no
file components.  This is roughly equivalent to
</p>
<div class="example">
<pre class="example">(define (directory-pathname? pathname)
  (string-null? (file-namestring pathname)))
</pre></div>

<p>except that it is faster.
</p></dd></dl>

<dl>
<dt id="index-pathname_002dwild_003f">procedure: <strong>pathname-wild?</strong> <em>pathname</em></dt>
<dd><p>Returns <code>#t</code> if <var>pathname</var> contains any wildcard components;
otherwise returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-merge_002dpathnames-1">procedure: <strong>merge-pathnames</strong> <em>pathname [defaults [default-version]]</em></dt>
<dd><span id="index-merging_002c-of-pathnames"></span>
<span id="index-defaulting_002c-of-pathname"></span>
<p>Returns a pathname whose components are obtained by combining those of
<var>pathname</var> and <var>defaults</var>.  <var>Defaults</var> defaults to the value
of <code>param:default-pathname-defaults</code> and <var>default-version</var> defaults
to <code>newest</code>.
</p>
<p>The pathnames are combined by components: if <var>pathname</var> has a
non-missing component, that is the resulting component, otherwise the
component from <var>defaults</var> is used.
The default version can be <code>#f</code> to preserve the information that
the component was missing from <var>pathname</var>.
The directory component is handled specially: if both pathnames have
directory components that are lists, and the directory component from
<var>pathname</var> is relative (i.e. starts with <code>relative</code>), then the
resulting directory component is formed by appending <var>pathname</var>&rsquo;s
component to <var>defaults</var>&rsquo;s component.
For example:
</p>
<div class="example">
<pre class="example">(define path1 (-&gt;pathname &quot;scheme/foo.scm&quot;))
(define path2 (-&gt;pathname &quot;/usr/morris&quot;))
path1
     &rArr;  #[pathname 74 &quot;scheme/foo.scm&quot;]
path2
     &rArr;  #[pathname 75 &quot;/usr/morris&quot;]
(merge-pathnames path1 path2)
     &rArr;  #[pathname 76 &quot;/usr/scheme/foo.scm&quot;]
(merge-pathnames path2 path1)
     &rArr;  #[pathname 77 &quot;/usr/morris.scm&quot;]
</pre></div>

<p>The merging rules for the version are more complex and depend on whether
<var>pathname</var> specifies a name.  If <var>pathname</var> does not specify a
name, then the version, if not provided, will come from <var>defaults</var>.
However, if <var>pathname</var> does specify a name then the version is not
affected by <var>defaults</var>.  The reason is that the version &ldquo;belongs
to&rdquo; some other file name and is unlikely to have anything to do with
the new one.  Finally, if this process leaves the version missing, then
<var>default-version</var> is used.
</p>
<p>The net effect is that if the user supplies just a name, then the host,
device, directory and type will come from <var>defaults</var>, but the
version will come from <var>default-version</var>.  If the user supplies
nothing, or just a directory, the name, type and version will come over
from <var>defaults</var> together.
</p></dd></dl>

<dl>
<dt id="index-param_003adefault_002dpathname_002ddefaults">parameter: <strong>param:default-pathname-defaults</strong></dt>
<dd><span id="index-defaulting_002c-of-pathname-1"></span>
<p>The value of this parameter (see <a href="Parameters.html">Parameters</a>) is the default
pathname-defaults pathname; if any pathname primitive that needs a set
of defaults is not given one, it uses this one.  Modifying the
<code>working-directory-pathname</code> parameter also changes this
parameter to the same value, computed by merging the new working
directory with the parameter&rsquo;s old value.
</p></dd></dl>

<dl>
<dt id="index-_002adefault_002dpathname_002ddefaults_002a">variable: <strong>*default-pathname-defaults*</strong></dt>
<dd><p>This variable is <strong>deprecated</strong>; use
<code>param:default-pathname-defaults</code> instead.
</p></dd></dl>

<dl>
<dt id="index-pathname_002ddefault">procedure: <strong>pathname-default</strong> <em>pathname device directory name type version</em></dt>
<dd><p>This procedure defaults all of the components of <var>pathname</var>
simultaneously.  It could have been defined by:
</p>
<div class="example">
<pre class="example">(define (pathname-default pathname
                          device directory name type version)
  (make-pathname (pathname-host pathname)
                 (or (pathname-device pathname) device)
                 (or (pathname-directory pathname) directory)
                 (or (pathname-name pathname) name)
                 (or (pathname-type pathname) type)
                 (or (pathname-version pathname) version)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-file_002dnamestring">procedure: <strong>file-namestring</strong> <em>pathname</em></dt>
<dt id="index-directory_002dnamestring">procedure: <strong>directory-namestring</strong> <em>pathname</em></dt>
<dt id="index-host_002dnamestring">procedure: <strong>host-namestring</strong> <em>pathname</em></dt>
<dt id="index-enough_002dnamestring">procedure: <strong>enough-namestring</strong> <em>pathname [defaults]</em></dt>
<dd><span id="index-conversion_002c-pathname-to-string-1"></span>
<p>These procedures return a string corresponding to a subset of the
<var>pathname</var> information.  <code>file-namestring</code> returns a string
representing just the <var>name</var>, <var>type</var> and <var>version</var>
components of <var>pathname</var>; the result of <code>directory-namestring</code>
represents just the <var>host</var>, <var>device</var>, and <var>directory</var>
components; and <code>host-namestring</code> returns a string for just the
<var>host</var> portion.
</p>
<p><code>enough-namestring</code> takes another argument, <var>defaults</var>.  It
returns an abbreviated namestring that is just sufficient to identify
the file named by <var>pathname</var> when considered relative to the
<var>defaults</var> (which defaults to the value of
<code>param:default-pathname-defaults</code>).
</p>
<div class="example">
<pre class="example">(file-namestring &quot;/usr/morris/minor.van&quot;)
     &rArr;  &quot;minor.van&quot;
(directory-namestring &quot;/usr/morris/minor.van&quot;)
     &rArr;  &quot;/usr/morris/&quot;
(enough-namestring &quot;/usr/morris/men&quot;)
     &rArr;  &quot;men&quot;      <span class="roman">;perhaps</span>
</pre></div>
</dd></dl>

<dl>
<dt id="index-file_002dpathname">procedure: <strong>file-pathname</strong> <em>pathname</em></dt>
<dt id="index-directory_002dpathname">procedure: <strong>directory-pathname</strong> <em>pathname</em></dt>
<dt id="index-enough_002dpathname">procedure: <strong>enough-pathname</strong> <em>pathname [defaults]</em></dt>
<dd><span id="index-selection_002c-components-of-pathname"></span>
<p>These procedures return a pathname corresponding to a subset of the
<var>pathname</var> information.
<code>file-pathname</code> returns a pathname with just the
<var>name</var>, <var>type</var> and <var>version</var> components of <var>pathname</var>.
The result of <code>directory-pathname</code> is a pathname containing the
<var>host</var>, <var>device</var> and <var>directory</var> components of <var>pathname</var>.
</p>
<p><code>enough-pathname</code> takes another argument, <var>defaults</var>.  It
returns an abbreviated pathname that is just sufficient to identify
the file named by <var>pathname</var> when considered relative to the
<var>defaults</var> (which defaults to the value of
<code>param:default-pathname-defaults</code>).
</p>
<p>These procedures are similar to <code>file-namestring</code>,
<code>directory-namestring</code> and <code>enough-namestring</code>, but they
return pathnames instead of strings.
</p></dd></dl>

<dl>
<dt id="index-directory_002dpathname_002das_002dfile">procedure: <strong>directory-pathname-as-file</strong> <em>pathname</em></dt>
<dd><span id="index-file_002c-converting-pathname-directory-to"></span>
<p>Returns a pathname that is equivalent to <var>pathname</var>, but in which
the directory component is represented as a file.
The last directory is removed from the directory component and converted
into name and type components.
This is the inverse operation to <code>pathname-as-directory</code>.
</p>
<div class="example">
<pre class="example">(directory-pathname-as-file (-&gt;pathname &quot;/usr/blisp/&quot;))
     &rArr;  #[pathname &quot;/usr/blisp&quot;]
</pre></div>
</dd></dl>

<dl>
<dt id="index-pathname_002das_002ddirectory">procedure: <strong>pathname-as-directory</strong> <em>pathname</em></dt>
<dd><span id="index-directory_002c-converting-pathname-to"></span>
<p>Returns a pathname that is equivalent to <var>pathname</var>, but in which
any file components have been converted to a directory component.  If
<var>pathname</var> does not have name, type, or version components, it is
returned without modification.  Otherwise, these file components are
converted into a string, and the string is added to the end of the list
of directory components.  This is the inverse operation to
<code>directory-pathname-as-file</code>.
</p>
<div class="example">
<pre class="example">(pathname-as-directory (-&gt;pathname &quot;/usr/blisp/rel5&quot;))
     &rArr;  #[pathname &quot;/usr/blisp/rel5/&quot;]
</pre></div>
</dd></dl>


<hr>
<div class="header">
<p>
Next: <a href="Miscellaneous-Pathnames.html" accesskey="n" rel="next">Miscellaneous Pathnames</a>, Previous: <a href="Components-of-Pathnames.html" accesskey="p" rel="prev">Components of Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
