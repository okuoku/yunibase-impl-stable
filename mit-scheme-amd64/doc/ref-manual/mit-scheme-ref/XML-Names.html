<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>XML Names (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="XML Names (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="XML Names (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="XML-Support.html" rel="up" title="XML Support">
<link href="XML-Structure.html" rel="next" title="XML Structure">
<link href="XML-Output.html" rel="prev" title="XML Output">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="XML-Names"></span><div class="header">
<p>
Next: <a href="XML-Structure.html" accesskey="n" rel="next">XML Structure</a>, Previous: <a href="XML-Output.html" accesskey="p" rel="prev">XML Output</a>, Up: <a href="XML-Support.html" accesskey="u" rel="up">XML Support</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="XML-Names-1"></span><h4 class="subsection">14.15.3 XML Names</h4>

<span id="index-XML-names"></span>
<span id="index-names_002c-XML"></span>
<p>MIT/GNU Scheme implements <acronym>XML</acronym> names in a slightly complex way.
Unfortunately, this complexity is a direct consequence of the definition
of <acronym>XML</acronym> names rather than a mis-feature of this implementation.
</p>
<p>The reason that <acronym>XML</acronym> names are complex is that <acronym>XML</acronym>
namespace support, which was added after <acronym>XML</acronym> was standardized,
is not very well integrated with the core <acronym>XML</acronym> definition.  The
most obvious problem is that names can&rsquo;t have associated namespaces when
they appear in the <acronym>DTD</acronym> of a document, even if the body of the
document uses them.  Consequently, it must be possible to compare
non-associated names with associated names.
</p>
<span id="index-Uniform-Resource-Identifier"></span>
<span id="index-URI_002c-of-XML-name"></span>
<span id="index-qname_002c-of-XML-name"></span>
<p>An <acronym>XML</acronym> name consists of two parts: the <em>qname</em>, which is a
symbol, possibly including a namespace prefix; and the
<em>Uniform Resource Identifier</em> (<acronym>URI</acronym>), which
identifies an optional namespace.
</p>
<dl>
<dt id="index-make_002dxml_002dname">procedure: <strong>make-xml-name</strong> <em>qname uri</em></dt>
<dd><p>Creates and returns an <acronym>XML</acronym> name.  <var>Qname</var> must be a symbol
whose name satisfies <code>string-is-xml-name?</code>.  <var>Uri</var> must satisfy
either <code>absolute-uri?</code> or <code>null-xml-namespace-uri?</code>.  The
returned value is an <acronym>XML</acronym> name that satisfies <code>xml-name?</code>.
</p>
<p>If <var>uri</var> is the <em>null</em> namespace (satisfies
<code>null-xml-namespace-uri?</code>), the returned value is a symbol
equivalent to <var>qname</var>.  This means that an ordinary symbol can be
used as an <acronym>XML</acronym> name when there is no namespace associated with
the name.
</p>
<p>For convenience, <var>qname</var> may be a string, in which case it is
converted to a symbol using <code>make-xml-qname</code>.
</p>
<p>For convenience, <var>uri</var> may be any object that <code>-&gt;uri</code> is able
to convert to a <acronym>URI</acronym> record, provided the resulting
<acronym>URI</acronym> meets the above restrictions.
</p></dd></dl>

<dl>
<dt id="index-xml_002dname_003f">procedure: <strong>xml-name?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is an <acronym>XML</acronym> name, and
<code>#f</code> otherwise.
</p></dd></dl>

<dl>
<dt id="index-xml_002dname_002d_003esymbol">procedure: <strong>xml-name-&gt;symbol</strong> <em>xml-name</em></dt>
<dd><p>Returns the symbol part of <var>xml-name</var>.
</p></dd></dl>

<dl>
<dt id="index-xml_002dname_002duri">procedure: <strong>xml-name-uri</strong> <em>xml-name</em></dt>
<dd><p>Returns the <em>URI</em> of <var>xml-name</var>.  The result always satisfies
<code>absolute-uri?</code> or <code>null-xml-namespace-uri?</code>.
</p></dd></dl>

<dl>
<dt id="index-xml_002dname_002dstring">procedure: <strong>xml-name-string</strong> <em>xml-name</em></dt>
<dd><p>Returns the <em>qname</em> of <var>xml-name</var> as a string.  Equivalent to
</p>
<div class="example">
<pre class="example">(symbol-&gt;string (xml-name-&gt;symbol <var>xml-name</var>))
</pre></div>
</dd></dl>

<span id="index-prefix_002c-of-XML-name"></span>
<span id="index-local-part_002c-of-XML-name"></span>
<p>The next two procedures get the <em>prefix</em> and <em>local part</em> of an
<acronym>XML</acronym> name, respectively.  The prefix of an <acronym>XML</acronym> name
is the part of the qname to the left of the colon, while the local part
is the part of the qname to the right of the colon.  If there is no
colon in the qname, the local part is the entire qname, and the prefix
is the null symbol (i.e. &lsquo;<samp>||</samp>&rsquo;).
</p>
<dl>
<dt id="index-xml_002dname_002dprefix">procedure: <strong>xml-name-prefix</strong> <em>xml-name</em></dt>
<dd><p>Returns the <em>prefix</em> of <var>xml-name</var> as a symbol.
</p></dd></dl>

<dl>
<dt id="index-xml_002dname_002dlocal">procedure: <strong>xml-name-local</strong> <em>xml-name</em></dt>
<dd><p>Returns the <em>local part</em> of <var>xml-name</var> as a symbol.
</p></dd></dl>

<span id="index-comparison_002c-of-XML-names"></span>
<span id="index-equality_002c-of-XML-names"></span>
<p>The next procedure compares two <acronym>XML</acronym> names for equality.  The
rules for equality are slightly complex, in order to permit comparing
names in the <acronym>DTD</acronym> with names in the document body.  So, if both
of the names have non-null namespace <acronym>URI</acronym>s, then the names are
equal if and only if their local parts are equal and their
<acronym>URI</acronym>s are equal.  (The prefixes of the names are not considered
in this case.)  Otherwise, the names are equal if and only if their
qnames are equal.
</p>
<dl>
<dt id="index-xml_002dname_003d_003f">procedure: <strong>xml-name=?</strong> <em>xml-name-1 xml-name-2</em></dt>
<dd><p>Returns <code>#t</code> if <var>xml-name-1</var> and <var>xml-name-2</var> are the same
name, and <code>#f</code> otherwise.
</p></dd></dl>

<p>These next procedures define the data abstraction for qnames.  While
qnames are represented as symbols, only symbols whose names satisfy
<code>string-is-xml-name?</code> are qnames.
</p>
<dl>
<dt id="index-make_002dxml_002dqname">procedure: <strong>make-xml-qname</strong> <em>string</em></dt>
<dd><p><var>String</var> must satisfy <code>string-is-xml-name?</code>.  Returns the qname
corresponding to <var>string</var> (the symbol whose name is <var>string</var>).
</p></dd></dl>

<dl>
<dt id="index-xml_002dqname_003f">procedure: <strong>xml-qname?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is a qname, otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-xml_002dqname_002dprefix">procedure: <strong>xml-qname-prefix</strong> <em>qname</em></dt>
<dd><p>Returns the prefix of <var>qname</var> as a symbol.
</p></dd></dl>

<dl>
<dt id="index-xml_002dqname_002dlocal">procedure: <strong>xml-qname-local</strong> <em>qname</em></dt>
<dd><p>Returns the local part of <var>qname</var> as a symbol.
</p></dd></dl>

<p>The prefix of a qname or <acronym>XML</acronym> name may be absent if there is no
colon in the name.  The absent, or null, prefix is abstracted by the
next two procedures.  Note that the null prefix is a symbol, just like
non-null prefixes.
</p>
<dl>
<dt id="index-null_002dxml_002dname_002dprefix">procedure: <strong>null-xml-name-prefix</strong></dt>
<dd><p>Returns the null prefix.
</p></dd></dl>

<dl>
<dt id="index-null_002dxml_002dname_002dprefix_003f">procedure: <strong>null-xml-name-prefix?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is the null prefix, otherwise returns
<code>#f</code>.
</p></dd></dl>

<p>The namespace <acronym>URI</acronym> of an <acronym>XML</acronym> name may be null,
meaning that there is no namespace associated with the name.  This
namespace is represented by a relative <acronym>URI</acronym> record whose string
representation is the null string.
</p>
<dl>
<dt id="index-null_002dxml_002dnamespace_002duri">procedure: <strong>null-xml-namespace-uri</strong></dt>
<dd><p>Returns the null namespace <acronym>URI</acronym> record.
</p></dd></dl>

<dl>
<dt id="index-null_002dxml_002dnamespace_002duri_003f">procedure: <strong>null-xml-namespace-uri?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is the null namespace <acronym>URI</acronym>
record, otherwise returns <code>#f</code>.
</p></dd></dl>

<p>The following values are two distinguished <acronym>URI</acronym> records.
</p>
<dl>
<dt id="index-xml_002duri">variable: <strong>xml-uri</strong></dt>
<dd><p><code>xml-uri</code> is the <acronym>URI</acronym> reserved for use by the
<acronym>XML</acronym> recommendation.  This <acronym>URI</acronym> must be used with the
&lsquo;<samp>xml</samp>&rsquo; prefix.
</p></dd></dl>

<dl>
<dt id="index-xmlns_002duri">variable: <strong>xmlns-uri</strong></dt>
<dd><p><code>xmlns-uri</code> is the <acronym>URI</acronym> reserved for use by the
<acronym>XML</acronym> namespace recommendation.  This <acronym>URI</acronym> must be used
with the &lsquo;<samp>xmlns</samp>&rsquo; prefix.
</p></dd></dl>


<dl>
<dt id="index-make_002dxml_002dnmtoken">procedure: <strong>make-xml-nmtoken</strong> <em>string</em></dt>
</dl>

<dl>
<dt id="index-xml_002dnmtoken_003f">procedure: <strong>xml-nmtoken?</strong> <em>object</em></dt>
</dl>


<dl>
<dt id="index-string_002dis_002dxml_002dname_003f">procedure: <strong>string-is-xml-name?</strong> <em>string</em></dt>
</dl>

<dl>
<dt id="index-string_002dis_002dxml_002dnmtoken_003f">procedure: <strong>string-is-xml-nmtoken?</strong> <em>string</em></dt>
</dl>



<hr>
<div class="header">
<p>
Next: <a href="XML-Structure.html" accesskey="n" rel="next">XML Structure</a>, Previous: <a href="XML-Output.html" accesskey="p" rel="prev">XML Output</a>, Up: <a href="XML-Support.html" accesskey="u" rel="up">XML Support</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
