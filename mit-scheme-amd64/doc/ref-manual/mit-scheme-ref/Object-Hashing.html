<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Object Hashing (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Object Hashing (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Object Hashing (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="Red_002dBlack-Trees.html" rel="next" title="Red-Black Trees">
<link href="Address-Hashing.html" rel="prev" title="Address Hashing">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Object-Hashing"></span><div class="header">
<p>
Next: <a href="Red_002dBlack-Trees.html" accesskey="n" rel="next">Red-Black Trees</a>, Previous: <a href="Hash-Tables.html" accesskey="p" rel="prev">Hash Tables</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Object-Hashing-1"></span><h3 class="section">11.5 Object Hashing</h3>

<span id="index-object-hashing"></span>
<span id="index-hashing_002c-of-object"></span>
<p>The MIT/GNU Scheme object-hashing facility provides a mechanism for
generating a unique hash number for an arbitrary object.  This hash
number, unlike an object&rsquo;s address, is unchanged by garbage
collection.  The object-hashing facility is used in the generation of
the written representation for many objects (see <a href="Custom-Output.html">Custom Output</a>),
but it can be used for anything that needs a stable identifier for an
arbitrary object.
</p>
<p>All of these procedures accept an optional argument called
<var>hasher</var> which contains the object-integer associations.  If
given, this argument must be an object hasher as constructed by
<code>make-object-hasher</code> (see below).  If not given, a default hasher
is used.
</p>
<dl>
<dt id="index-hash_002dobject">procedure: <strong>hash-object</strong> <em>object [hasher]</em></dt>
<dt id="index-hash-1">obsolete procedure: <strong>hash</strong> <em>object [hasher]</em></dt>
<dt id="index-object_002dhash">obsolete procedure: <strong>object-hash</strong> <em>object [hasher]</em></dt>
<dd><span id="index-eq_003f-13"></span>
<p><code>hash-object</code> associates an exact non-negative integer with
<var>object</var> and returns that integer.  If <code>hash-object</code> was
previously called with <var>object</var> as its argument, the integer
returned is the same as was returned by the previous call.
<code>hash-object</code> guarantees that distinct objects (in the sense of
<code>eqv?</code>) are associated with distinct integers.
</p></dd></dl>

<dl>
<dt id="index-unhash_002dobject">procedure: <strong>unhash-object</strong> <em>k [hasher]</em></dt>
<dt id="index-unhash">obsolete procedure: <strong>unhash</strong> <em>k [hasher]</em></dt>
<dt id="index-object_002dunhash">obsolete procedure: <strong>object-unhash</strong> <em>k [hasher]</em></dt>
<dd><p><code>unhash-object</code> takes an exact non-negative integer <var>k</var> and
returns the object associated with that integer.  If there is no
object associated with <var>k</var>, or if the object previously associated
with <var>k</var> has been reclaimed by the garbage collector, an error of
type <code>condition-type:bad-range-argument</code> is signalled.  In other
words, if <code>hash-object</code> previously returned <var>k</var> for some
object, and that object has not been reclaimed, it is the value of the
call to <code>unhash-object</code>.
<span id="index-condition_002dtype_003abad_002drange_002dargument-3"></span>
</p></dd></dl>

<p>An object that is passed to <code>hash-object</code> as an argument is not
protected from being reclaimed by the garbage collector.  If all other
references to that object are eliminated, the object will be
reclaimed.  Subsequently calling <code>unhash-object</code> with the hash
number of the (now reclaimed) object will signal an error.
</p>
<div class="example">
<pre class="example">(define x (cons 0 0))           &rArr;  <span class="roman">unspecified</span>
(hash-object x)                 &rArr;  77
(eqv? (hash-object x)
      (hash-object x))          &rArr;  #t
(define x 0)                    &rArr;  <span class="roman">unspecified</span>
(gc-flip)                       <span class="roman">;force a garbage collection</span>
(unhash-object 77)              error&rarr;
</pre></div>

<dl>
<dt id="index-object_002dhashed_003f">procedure: <strong>object-hashed?</strong> <em>object [hasher]</em></dt>
<dd><p>This predicate is true iff <var>object</var> has an associated hash number.
</p></dd></dl>

<dl>
<dt id="index-valid_002dobject_002dhash_003f">procedure: <strong>valid-object-hash?</strong> <em>k [hasher]</em></dt>
<dt id="index-valid_002dhash_002dnumber_003f">obsolete procedure: <strong>valid-hash-number?</strong> <em>k [hasher]</em></dt>
<dd><p>This predicate is true iff <var>k</var> is the hash number associated with
some object.
</p></dd></dl>

<p>Finally, this procedure makes new object hashers:
</p>
<dl>
<dt id="index-make_002dobject_002dhasher">procedure: <strong>make-object-hasher</strong></dt>
<dt id="index-hash_002dtable_002fmake">obsolete procedure: <strong>hash-table/make</strong></dt>
<dd><p>This procedure creates and returns a new, empty object hasher that
is suitable for use as the optional <var>hasher</var> argument to the above
procedures.  The returned hasher contains no associations.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Red_002dBlack-Trees.html" accesskey="n" rel="next">Red-Black Trees</a>, Previous: <a href="Hash-Tables.html" accesskey="p" rel="prev">Hash Tables</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
