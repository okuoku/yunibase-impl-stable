<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Bit Strings (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Bit Strings (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Bit Strings (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Construction-of-Bit-Strings.html" rel="next" title="Construction of Bit Strings">
<link href="Modifying-Vectors.html" rel="prev" title="Modifying Vectors">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Bit-Strings"></span><div class="header">
<p>
Next: <a href="Miscellaneous-Datatypes.html" accesskey="n" rel="next">Miscellaneous Datatypes</a>, Previous: <a href="Vectors.html" accesskey="p" rel="prev">Vectors</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Bit-Strings-1"></span><h2 class="chapter">9 Bit Strings</h2>

<span id="index-bit-string-_0028defn_0029"></span>
<span id="index-string_002c-of-bits-_0028defn_0029"></span>
<p>A <em>bit string</em> is a sequence of bits.  Bit strings can be used to
represent sets or to manipulate binary data.  The elements of a bit
string are numbered from zero up to the number of bits in the string
less one, in <em>right to left order</em>, (the rightmost bit is numbered
zero).  When you convert from a bit string to an integer, the zero-th
bit is associated with the zero-th power of two, the first bit is
associated with the first power, and so on.
</p>
<p>Bit strings are encoded very densely in memory.  Each bit occupies
exactly one bit of storage, and the overhead for the entire bit string
is bounded by a small constant.  However, accessing a bit in a bit
string is slow compared to accessing an element of a vector or character
string.  If performance is of overriding concern, it is better to use
character strings to store sets of boolean values even though they
occupy more space.
</p>
<span id="index-length_002c-of-bit-string-_0028defn_0029"></span>
<span id="index-index_002c-of-bit-string-_0028defn_0029"></span>
<span id="index-valid-index_002c-of-bit-string-_0028defn_0029"></span>
<span id="index-bit-string-length-_0028defn_0029"></span>
<span id="index-bit-string-index-_0028defn_0029"></span>
<p>The <em>length</em> of a bit string is the number of bits that it contains.
This number is an exact non-negative integer that is fixed when the bit
string is created.  The <em>valid indexes</em> of a bit string are the
exact non-negative integers less than the length of the bit string.
</p>
<span id="index-external-representation_002c-for-bit-string"></span>
<span id="index-_0023_002a-as-external-representation"></span>
<span id="index-asterisk_002c-as-external-representation"></span>
<p>Bit strings may contain zero or more bits.  They are not limited by the
length of a machine word.  In the printed representation of a bit
string, the contents of the bit string are preceded by &lsquo;<samp>#*</samp>&rsquo;.  The
contents are printed starting with the most significant bit (highest
index).
</p>
<p>Note that the external representation of bit strings uses a bit ordering
that is the reverse of the representation for bit strings in Common
Lisp.  It is likely that MIT/GNU Scheme&rsquo;s representation will be
changed in the future, to be compatible with Common Lisp.  For the time
being this representation should be considered a convenience for viewing
bit strings rather than a means of entering them as data.
</p>
<div class="example">
<pre class="example">#*11111
#*1010
#*00000000
#*
</pre></div>

<p>All of the bit-string procedures are MIT/GNU Scheme extensions.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Construction-of-Bit-Strings.html" accesskey="1">Construction of Bit Strings</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Selecting-Bit-String-Components.html" accesskey="2">Selecting Bit String Components</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Cutting-and-Pasting-Bit-Strings.html" accesskey="3">Cutting and Pasting Bit Strings</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Bitwise-Operations-on-Bit-Strings.html" accesskey="4">Bitwise Operations on Bit Strings</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Modification-of-Bit-Strings.html" accesskey="5">Modification of Bit Strings</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Integer-Conversions-of-Bit-Strings.html" accesskey="6">Integer Conversions of Bit Strings</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Miscellaneous-Datatypes.html" accesskey="n" rel="next">Miscellaneous Datatypes</a>, Previous: <a href="Vectors.html" accesskey="p" rel="prev">Vectors</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
