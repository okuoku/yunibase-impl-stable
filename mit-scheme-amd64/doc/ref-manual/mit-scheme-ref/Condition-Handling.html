<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Condition Handling (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Condition Handling (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Condition Handling (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-System.html" rel="up" title="Error System">
<link href="Restarts.html" rel="next" title="Restarts">
<link href="Error-Messages.html" rel="prev" title="Error Messages">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Condition-Handling"></span><div class="header">
<p>
Next: <a href="Restarts.html" accesskey="n" rel="next">Restarts</a>, Previous: <a href="Error-Messages.html" accesskey="p" rel="prev">Error Messages</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Condition-Handling-1"></span><h3 class="section">16.3 Condition Handling</h3>

<span id="index-handler_002c-condition-_0028defn_0029"></span>
<span id="index-condition-handler-_0028defn_0029"></span>
<span id="index-bind_002dcondition_002dhandler-2"></span>
<span id="index-bind_002ddefault_002dcondition_002dhandler-1"></span>
<p>The occurrence of a condition is signalled using
<code>signal-condition</code>.  <code>signal-condition</code> attempts to locate and
invoke a <em>condition handler</em> that is prepared to deal with the type
of condition that has occurred.  A condition handler is a procedure of
one parameter, the condition that is being signalled.  A procedure is
installed as a condition handler by calling
<code>bind-condition-handler</code> (to establish a handler that is in effect
only while a particular thunk is executing) or
<code>bind-default-condition-handler</code> (to establish a handler that is in
effect permanently).  As implied by the name, handlers created by
<code>bind-default-condition-handler</code> are invoked only after all other
applicable handlers have been invoked.
</p>
<p>A <var>handler</var> may process a signal in any way it deems appropriate,
but the common patterns are:
</p>
<dl compact="compact">
<dt>Ignore the condition.</dt>
<dd><p>By returning from the handler in the usual manner.
</p>
</dd>
<dt>Handle the condition.</dt>
<dd><p>By doing some processing and then invoking a restart (or, less
preferably, a continuation) that was established at some point prior to
the call to <code>signal-condition</code>.
</p>
</dd>
<dt>Resignal a condition.</dt>
<dd><p>By doing some processing and calling <code>signal-condition</code> with either
the same condition or a newly created one.  In order to support this,
<code>signal-condition</code> runs <var>handler</var> in such a way that a
subsequent call to <code>signal-condition</code> sees only the handlers that
were established prior to this one.
</p></dd>
</dl>

<span id="index-REP-loop-2"></span>
<span id="index-break_002don_002dsignals-1"></span>
<p>As an aid to debugging condition handlers, Scheme maintains a set of
condition types that will cause an interactive breakpoint to occur prior
to normal condition signalling.  That is, <code>signal-condition</code>
creates a new <small>REPL</small> prior to its normal operation when its argument
is a condition that is a specialization of any of these types.  The
procedure <code>break-on-signals</code> establishes this set of condition
types.
</p>
<dl>
<dt id="index-ignore_002derrors">procedure: <strong>ignore-errors</strong> <em>thunk</em></dt>
<dd><span id="index-error-4"></span>
<span id="index-condition_002dtype_003aerror-1"></span>
<p>Executes <var>thunk</var> with a condition handler that intercepts the
signalling of any specialization of <code>condition-type:error</code>
(including those produced by calls to <code>error</code>) and immediately
terminates the execution of <var>thunk</var> and returns from the call to
<code>ignore-errors</code> with the signalled condition as its value.  If
<var>thunk</var> returns normally, its value is returned from
<code>ignore-errors</code>.
</p>
<p>Notice that <code>ignore-errors</code> does not &ldquo;turn off signalling&rdquo; or
condition handling.  Condition handling takes place in the normal manner
but conditions specialized from <code>condition-type:error</code> are trapped
rather than propogated as they would be by default.
</p></dd></dl>

<dl>
<dt id="index-bind_002dcondition_002dhandler-3">procedure: <strong>bind-condition-handler</strong> <em>condition-types handler thunk</em></dt>
<dd><span id="index-signal_002dcondition-4"></span>
<p>Invokes <var>thunk</var> after adding <var>handler</var> as a condition handler
for the conditions specified by <var>condition-types</var>.
<var>Condition-types</var> must be a list of condition types; signalling a
condition whose type is a specialization of any of these types will
cause the <var>handler</var> to be invoked.  See <code>signal-condition</code> for
a description of the mechanism used to invoke handlers.
</p>
<p>By special extension, if <var>condition-types</var> is the empty list then
the <var>handler</var> is called for all conditions.
</p></dd></dl>

<dl>
<dt id="index-bind_002ddefault_002dcondition_002dhandler-2">procedure: <strong>bind-default-condition-handler</strong> <em>condition-types handler</em></dt>
<dd><span id="index-signal_002dcondition-5"></span>
<p>Installs <var>handler</var> as a (permanent) condition handler for the
conditions specified by <var>condition-types</var>.  <var>Condition-types</var>
must be a list of condition types; signalling a condition whose type is
a specialization of any of these types will cause the <var>handler</var> to
be invoked.  See <code>signal-condition</code> for a description of the
mechanism used to invoke handlers.
</p>
<p>By special extension, if <var>condition-types</var> is the empty list then
the <var>handler</var> is called for all conditions.
</p></dd></dl>

<dl>
<dt id="index-break_002don_002dsignals-2">procedure: <strong>break-on-signals</strong> <em>condition-types</em></dt>
<dd><span id="index-signal_002dcondition-6"></span>
<span id="index-REP-loop-3"></span>
<p>Arranges for <code>signal-condition</code> to create an interactive <small>REPL</small>
before it signals a condition that is a specialization of any of the
types in the list of <var>condition-types</var>.  This can be extremely
helpful when trying to debug code that uses custom condition handlers.
In order to create a <small>REPL</small> when <em>any</em> condition type is
signalled it is best to actually put a breakpoint on entry to
<code>signal-condition</code>.
</p></dd></dl>

<dl>
<dt id="index-standard_002derror_002dhandler">procedure: <strong>standard-error-handler</strong> <em>condition</em></dt>
<dd><span id="index-error-5"></span>
<span id="index-ignore_002derror"></span>
<span id="index-REP-loop-4"></span>
<p>Called internally by <code>error</code> after it calls
<code>signal-condition</code>.  Normally creates a new <small>REPL</small> with
the prompt <code>&quot;error&gt;&quot;</code> (but see <code>standard-error-hook</code>).  In
order to simulate the effect of calling <code>error</code>, code may call
<code>signal-condition</code> directly and then call
<code>standard-error-handler</code> if <code>signal-condition</code> returns.
</p></dd></dl>

<dl>
<dt id="index-standard_002derror_002dhook">parameter: <strong>standard-error-hook</strong></dt>
<dd><span id="index-standard_002derror_002dhandler-1"></span>
<span id="index-dynamic-binding-1"></span>
<span id="index-REP-loop-5"></span>
<p>This parameter controls the behavior of the procedure
<code>standard-error-handler</code>, and hence <code>error</code>.  It is intended
to be bound with <code>parameterize</code> and is normally <code>#f</code>.  It may be
changed to a procedure of one argument and will then be invoked (with
<code>standard-error-hook</code> rebound to <code>#f</code>) by
<code>standard-error-handler</code> just prior to starting the error
<small>REPL</small>.  It is passed one argument, the condition being signalled.
</p></dd></dl>

<dl>
<dt id="index-standard_002dwarning_002dhandler">procedure: <strong>standard-warning-handler</strong> <em>condition</em></dt>
<dd><span id="index-signal_002dcondition-7"></span>
<span id="index-notification_002doutput_002dport-1"></span>
<span id="index-write_002dcondition_002dreport"></span>
<p>This is the procedure called internally by <code>warn</code> after it calls
<code>signal-condition</code>.  The normal behavior of
<code>standard-warning-handler</code> is to print a message (but see
<code>standard-warning-hook</code>).  More precisely, the message is printed
to the port returned by <code>notification-output-port</code>.  The message is
formed by first printing the string <code>&quot;Warning: &quot;</code> to this port, and
then calling <code>write-condition-report</code> on <var>condition</var> and the port.
</p>
<span id="index-muffle_002dwarning-1"></span>
<p>In order to simulate the effect of calling <code>warn</code>, code may call
<code>signal-condition</code> directly and then call
<code>standard-warning-handler</code> if <code>signal-condition</code> returns.
(This is not sufficient to implement the <code>muffle-warning</code> protocol,
however.  For that purpose an explicit restart must be provided.)
</p></dd></dl>

<dl>
<dt id="index-standard_002dwarning_002dhook">parameter: <strong>standard-warning-hook</strong></dt>
<dd><span id="index-standard_002dwarning_002dhandler-1"></span>
<span id="index-dynamic-binding-2"></span>
<p>This parameter controls the behavior of the procedure
<code>standard-warning-handler</code>, and hence <code>warn</code>.  It is intended
to be bound with <code>parameterize</code> and is normally <code>#f</code>.  It may be
changed to a procedure of one argument and will then be invoked (with
<code>standard-warning-hook</code> rebound to <code>#f</code>) by
<code>standard-warning-handler</code> in lieu of writing the warning message.
It is passed one argument, the condition being signalled.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Restarts.html" accesskey="n" rel="next">Restarts</a>, Previous: <a href="Error-Messages.html" accesskey="p" rel="prev">Error Messages</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
