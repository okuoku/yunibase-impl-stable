<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Floating-Point Environment (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Floating-Point Environment (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Floating-Point Environment (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Fixnum-and-Flonum-Operations.html" rel="up" title="Fixnum and Flonum Operations">
<link href="Floating_002dPoint-Exceptions.html" rel="next" title="Floating-Point Exceptions">
<link href="Flonum-Operations.html" rel="prev" title="Flonum Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Floating_002dPoint-Environment"></span><div class="header">
<p>
Next: <a href="Floating_002dPoint-Exceptions.html" accesskey="n" rel="next">Floating-Point Exceptions</a>, Previous: <a href="Flonum-Operations.html" accesskey="p" rel="prev">Flonum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Floating_002dPoint-Environment-1"></span><h4 class="subsection">4.7.3 Floating-Point Environment</h4>

<span id="index-floating_002dpoint-environment"></span>
<p>The <acronym>IEEE 754-2008</acronym> computation model includes a persistent
rounding mode, exception flags, and exception-handling modes.
In MIT/GNU Scheme, the floating-point environment is per-thread.
However, because saving and restoring the floating-point environment
is expensive, it is maintained only for those threads that have
touched the floating-point environment explicitly, either:
</p>
<ul>
<li> during a procedure such as <code>flo:with-exceptions-trapped</code> that
establishes a change to the floating-point environment for a dynamic
extent, or

</li><li> after <code>flo:set-environment!</code> to a non-default environment (but
not after <code>flo:set-environment!</code> to the default environment), or

</li><li> after various other procedures such as <code>flo:clear-exceptions!</code>
that explicitly change the floating-point environment.
</li></ul>

<p><span id="index-floating_002dpoint-environment_002c-default"></span>
<span id="index-default-environment_002c-floating_002dpoint"></span>
The default environment is as in <acronym>IEEE 754-2008</acronym>: no
exceptions are trapped, and rounding is to nearest with ties broken to
even.
The set of exception flags in the default environment is indeterminate
&mdash; callers must enter a per-thread environment, e.g. by calling
<code>flo:clear-exceptions!</code>, before acting on the exception flags.
Like the default environment, a per-thread environment initially has
no exceptions trapped and rounds to nearest with ties to even.
</p>
<p>A <strong>floating-point environment</strong> descriptor is a
machine-dependent object representing the <acronym>IEEE 754-2008</acronym>
floating-point rounding mode, exception flags, and exception-handling
mode.
Users should not inspect a floating-point environment descriptor other
than to use it with the procedures here; its representation may vary
from system to system.
</p>
<dl>
<dt id="index-flo_003adefault_002denvironment">procedure: <strong>flo:default-environment</strong></dt>
<dd><p>Returns a descriptor for the default environment, with no exceptions
trapped and round-to-nearest/ties-to-even.
</p></dd></dl>

<dl>
<dt id="index-flo_003aenvironment">procedure: <strong>flo:environment</strong></dt>
<dt id="index-flo_003aset_002denvironment_0021">procedure: <strong>flo:set-environment!</strong> <em>floenv</em></dt>
<dt id="index-flo_003aupdate_002denvironment_0021">procedure: <strong>flo:update-environment!</strong> <em>floenv</em></dt>
<dd><p><code>Flo:environment</code> returns a descriptor for the current
floating-point environment.
<code>Flo:set-environment!</code> replaces the current floating-point
environment by <var>floenv</var>.
<code>Flo:update-environment!</code> does likewise, but re-raises any
exceptions that were already raised in the current floating-point
environment, which may cause a trap if <var>floenv</var> also traps them.
</p>
<p><code>Flo:update-environment!</code> is usually used together with
<code>flo:defer-exception-traps!</code> to defer potentially trapping on
exceptions in a large intermediate computation until the end.
</p></dd></dl>

<dl>
<dt id="index-flo_003apreserving_002denvironment">procedure: <strong>flo:preserving-environment</strong> <em>thunk</em></dt>
<dd><p>Saves the current floating-point environment if any and calls
<var>thunk</var>.
On exit from <var>thunk</var>, including non-local exit, saves
<var>thunk</var>&rsquo;s floating-point environment and restores the original
floating-point environment as if with <code>flo:set-environment!</code>.
On re-entry into <var>thunk</var>, restores <var>thunk</var>&rsquo;s floating-point
environment.
</p>
<p><strong>Note:</strong> <code>Flo:preserving-environment</code> <em>does not</em> enter
a per-thread environment.
If the current thread is in the default environment, the exception
flags are indeterminate, and remain so inside
<code>flo:preserving-environment</code>.
Callers interested in using the exception flags should start inside
<code>flo:preserving-environment</code> by clearing them with
<code>flo:clear-exceptions!</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Floating_002dPoint-Exceptions.html" accesskey="n" rel="next">Floating-Point Exceptions</a>, Previous: <a href="Flonum-Operations.html" accesskey="p" rel="prev">Flonum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
