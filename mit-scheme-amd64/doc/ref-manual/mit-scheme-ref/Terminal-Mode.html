<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Terminal Mode (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Terminal Mode (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Terminal Mode (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Format.html" rel="next" title="Format">
<link href="Blocking-Mode.html" rel="prev" title="Blocking Mode">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Terminal-Mode"></span><div class="header">
<p>
Next: <a href="Format.html" accesskey="n" rel="next">Format</a>, Previous: <a href="Blocking-Mode.html" accesskey="p" rel="prev">Blocking Mode</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Terminal-Mode-1"></span><h3 class="section">14.8 Terminal Mode</h3>

<span id="index-terminal-mode_002c-of-port"></span>
<p>A port that reads from or writes to a terminal has a <em>terminal
mode</em>; this is either <em>cooked</em> or <em>raw</em>.  This mode is
independent of the blocking mode: each can be changed independently of
the other.  Furthermore, a terminal <acronym>I/O</acronym> port has independent
terminal modes both for input and for output.
</p>
<span id="index-cooked-mode_002c-of-terminal-port"></span>
<p>A terminal port in cooked mode provides some standard processing to make
the terminal easy to communicate with.  For example, under unix, cooked
mode on input reads from the terminal a line at a time and provides
editing within the line, while cooked mode on output might
translate linefeeds to carriage-return/linefeed pairs.  In general, the
precise meaning of cooked mode is operating-system dependent, and
furthermore might be customizable by means of operating-system
utilities.  The basic idea is that cooked mode does whatever is
necessary to make the terminal handle all of the usual user-interface
conventions for the operating system, while keeping the program&rsquo;s
interaction with the port as normal as possible.
</p>
<span id="index-raw-mode_002c-of-terminal-port"></span>
<p>A terminal port in raw mode disables all of that processing.  In raw
mode, characters are directly read from and written to the device
without any translation or interpretation by the operating system.  On
input, characters are available as soon as they are typed, and are not
echoed on the terminal by the operating system.  In general, programs
that put ports in raw mode have to know the details of interacting with
the terminal.  In particular, raw mode is used for writing programs such
as text editors.
</p>
<p>Terminal ports are initially in cooked mode; this can be changed at any
time with the procedures defined in this section.
</p>
<p>These procedures represent cooked mode by the symbol <code>cooked</code>,
and raw mode by the symbol <code>raw</code>.  An argument called <var>mode</var>
must be one of these symbols.  A <var>port</var> argument to any of these
procedures may be any port, even if that port does not support
terminal mode; in that case, the port is not modified in any way.
</p>
<dl>
<dt id="index-input_002dport_002dterminal_002dmode">procedure: <strong>input-port-terminal-mode</strong> <em>input-port</em></dt>
<dt id="index-output_002dport_002dterminal_002dmode">procedure: <strong>output-port-terminal-mode</strong> <em>output-port</em></dt>
<dd><p>Returns the terminal mode of <var>input-port</var> or <var>output-port</var>.
Returns <code>#f</code> if the given port is not a terminal port.
</p></dd></dl>

<dl>
<dt id="index-set_002dinput_002dport_002dterminal_002dmode_0021">procedure: <strong>set-input-port-terminal-mode!</strong> <em>input-port mode</em></dt>
<dt id="index-set_002doutput_002dport_002dterminal_002dmode_0021">procedure: <strong>set-output-port-terminal-mode!</strong> <em>output-port mode</em></dt>
<dd><p>Changes the terminal mode of <var>input-port</var> or <var>output-port</var> to
be <var>mode</var> and returns an unspecified value.
</p></dd></dl>

<dl>
<dt id="index-with_002dinput_002dport_002dterminal_002dmode">procedure: <strong>with-input-port-terminal-mode</strong> <em>input-port mode thunk</em></dt>
<dt id="index-with_002doutput_002dport_002dterminal_002dmode">procedure: <strong>with-output-port-terminal-mode</strong> <em>output-port mode thunk</em></dt>
<dd><p><var>Thunk</var> must be a procedure of no arguments.
</p>
<p>Binds the terminal mode of <var>input-port</var> or <var>output-port</var> to be
<var>mode</var>, and calls <var>thunk</var>.  When <var>thunk</var> returns, the
original terminal mode is restored and the values yielded by
<var>thunk</var> are returned.
</p></dd></dl>

<dl>
<dt id="index-port_002finput_002dterminal_002dmode">obsolete procedure: <strong>port/input-terminal-mode</strong> <em>input-port</em></dt>
<dt id="index-port_002fset_002dinput_002dterminal_002dmode">obsolete procedure: <strong>port/set-input-terminal-mode</strong> <em>input-port mode</em></dt>
<dt id="index-port_002fwith_002dinput_002dterminal_002dmode">obsolete procedure: <strong>port/with-input-terminal-mode</strong> <em>input-port mode thunk</em></dt>
<dt id="index-port_002foutput_002dterminal_002dmode">obsolete procedure: <strong>port/output-terminal-mode</strong> <em>output-port</em></dt>
<dt id="index-port_002fset_002doutput_002dterminal_002dmode">obsolete procedure: <strong>port/set-output-terminal-mode</strong> <em>output-port mode</em></dt>
<dt id="index-port_002fwith_002doutput_002dterminal_002dmode">obsolete procedure: <strong>port/with-output-terminal-mode</strong> <em>output-port mode thunk</em></dt>
<dd><p>These procedures are <strong>deprecated</strong>; instead use the
corresponding procedures above.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Format.html" accesskey="n" rel="next">Format</a>, Previous: <a href="Blocking-Mode.html" accesskey="p" rel="prev">Blocking Mode</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
