<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Taxonomy (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Taxonomy (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Taxonomy (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-System.html" rel="up" title="Error System">
<link href="Graphics.html" rel="next" title="Graphics">
<link href="Condition-Types.html" rel="prev" title="Condition Types">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Taxonomy"></span><div class="header">
<p>
Previous: <a href="Condition-Types.html" accesskey="p" rel="prev">Condition Types</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Condition_002dType-Taxonomy"></span><h3 class="section">16.7 Condition-Type Taxonomy</h3>

<p>The MIT/GNU Scheme error system provides a rich set of predefined condition
types.  These are organized into a forest through taxonomic links
providing the relationships for &ldquo;specializes&rdquo; and &ldquo;generalizes&rdquo;.
The chart appearing below shows these relationships by indenting all the
specializations of a given type relative to the type.  Note that the
variables that are bound to these condition types are prefixed by
&lsquo;<samp>condition-type:</samp>&rsquo;; for example, the type appearing in the following
table as &lsquo;<samp>simple-error</samp>&rsquo; is stored in the variable
<code>condition-type:simple-error</code>.  Users are encouraged to add new
condition types by creating specializations of existing ones.
</p>
<p>Following the chart are detailed descriptions of the predefined
condition types.  Some of these types are marked as <em>abstract</em>
types.  Abstract types are not intended to be used directly as the type
of a condition; they are to be used as generalizations of other types,
and for binding condition handlers.  Types that are not marked as
abstract are <em>concrete</em>; they are intended to be explicitly used as
a condition&rsquo;s type.
</p>
<div class="example">
<pre class="example">serious-condition 
    error 
        simple-error
        illegal-datum
            wrong-type-datum
                wrong-type-argument
                wrong-number-of-arguments
            datum-out-of-range 
                bad-range-argument
            inapplicable-object
        file-error
            file-operation-error
            derived-file-error
        port-error
            derived-port-error
        variable-error
            unbound-variable
            unassigned-variable
        arithmetic-error
            divide-by-zero
            floating-point-overflow
            floating-point-underflow
        control-error
            no-such-restart
        not-loading 
        primitive-procedure-error
            system-call-error
warning
    simple-warning
simple-condition
breakpoint
</pre></div>

<dl>
<dt id="index-condition_002dtype_003aserious_002dcondition">condition type: <strong>condition-type:serious-condition</strong></dt>
<dd><p>This is an abstract type.  All serious conditions that require some form
of intervention should inherit from this type.  In particular, all
errors inherit from this type.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aerror">condition type: <strong>condition-type:error</strong></dt>
<dd><p>This is an abstract type.  All errors should inherit from this type.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003asimple_002derror">condition type: <strong>condition-type:simple-error</strong> <em>message irritants</em></dt>
<dd><p>This is the condition generated by the <code>error</code> procedure when its
first argument is not a condition or condition type.  The fields
<var>message</var> and <var>irritants</var> are taken directly from the arguments
to <code>error</code>; <var>message</var> contains an object (usually a string) and
<var>irritants</var> contains a list of objects.  The reporter for this type
uses <code>format-error-message</code> to generate its output from
<var>message</var> and <var>irritants</var>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aillegal_002ddatum">condition type: <strong>condition-type:illegal-datum</strong> <em>datum</em></dt>
<dd><p>This is an abstract type.  This type indicates the class of errors in
which a program discovers an object that lacks specific required
properties.  Most commonly, the object is of the wrong type or is
outside a specific range.  The <var>datum</var> field contains the offending
object.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003awrong_002dtype_002ddatum">condition type: <strong>condition-type:wrong-type-datum</strong> <em>datum type</em></dt>
<dd><p>This type indicates the class of errors in which a program discovers an
object that is of the wrong type.  The <var>type</var> field contains a
string describing the type that was expected, and the <var>datum</var> field
contains the object that is of the wrong type.
</p></dd></dl>

<div class="example">
<pre class="example">(error:wrong-type-datum 3.4 &quot;integer&quot;)  error&rarr;
;The object 3.4 is not an integer.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003awrong_002dtype_002ddatum">procedure: <strong>error:wrong-type-datum</strong> <em>datum type</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:wrong-type-datum</code>.  The <var>datum</var> and <var>type</var>
fields of the condition are filled in from the corresponding arguments
to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003awrong_002dtype_002dargument-1">condition type: <strong>condition-type:wrong-type-argument</strong> <em>datum type operator operand</em></dt>
<dd><p>This type indicates that a procedure was passed an argument of the wrong
type.  The <var>operator</var> field contains the procedure (or a symbol
naming the procedure), the <var>operand</var> field indicates the argument
position that was involved (this field contains either a symbol, a
non-negative integer, or <code>#f</code>), the <var>type</var> field contains a
string describing the type that was expected, and the <var>datum</var> field
contains the offending argument.
</p></dd></dl>

<div class="example">
<pre class="example">(+ 'a 3)                                error&rarr;
;The object a, passed as the first argument to integer-add,
; is not the correct type.
;To continue, call RESTART with an option number:
; (RESTART 2) =&gt; Specify an argument to use in its place.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre><pre class="example">

</pre><pre class="example">(list-copy 3)
;The object 3, passed as an argument to list-copy, is not a list.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003awrong_002dtype_002dargument">procedure: <strong>error:wrong-type-argument</strong> <em>datum type operator</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:wrong-type-argument</code>.  The <var>datum</var>, <var>type</var>
and <var>operator</var> fields of the condition are filled in from the
corresponding arguments to the procedure; the <var>operand</var> field of the
condition is set to <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003awrong_002dnumber_002dof_002darguments-3">condition type: <strong>condition-type:wrong-number-of-arguments</strong> <em>datum type operands</em></dt>
<dd><p>This type indicates that a procedure was called with the wrong number of
arguments.  The <var>datum</var> field contains the procedure being called,
the <var>type</var> field contains the number of arguments that the procedure
accepts, and the <var>operands</var> field contains a list of the arguments
that were passed to the procedure.
</p></dd></dl>

<div class="example">
<pre class="example">(car 3 4)                               error&rarr;
;The procedure car has been called with 2 arguments;
; it requires exactly 1 argument.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003awrong_002dnumber_002dof_002darguments">procedure: <strong>error:wrong-number-of-arguments</strong> <em>datum type operands</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:wrong-number-of-arguments</code>.  The <var>datum</var>,
<var>type</var> and <var>operands</var> fields of the condition are filled in from
the corresponding arguments to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003adatum_002dout_002dof_002drange">condition type: <strong>condition-type:datum-out-of-range</strong> <em>datum</em></dt>
<dd><p>This type indicates the class of errors in which a program discovers an
object that is of the correct type but is otherwise out of range.  Most
often, this type indicates that an index to some data structure is
outside of the range of indices for that structure.  The <var>datum</var>
field contains the offending object.
</p></dd></dl>

<div class="example">
<pre class="example">(error:datum-out-of-range 3)            error&rarr;
;The object 3 is not in the correct range.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003adatum_002dout_002dof_002drange">procedure: <strong>error:datum-out-of-range</strong> <em>datum</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:datum-out-of-range</code>.  The <var>datum</var> field of the
condition is filled in from the corresponding argument to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003abad_002drange_002dargument-4">condition type: <strong>condition-type:bad-range-argument</strong> <em>datum operator operand</em></dt>
<dd><p>This type indicates that a procedure was passed an argument that is of
the correct type but is otherwise out of range.  Most often, this type
indicates that an index to some data structure is outside of the range
of indices for that structure.  The <var>operator</var> field contains the
procedure (or a symbol naming the procedure), the <var>operand</var> field
indicates the argument position that was involved (this field contains
either a symbol, a non-negative integer, or <code>#f</code>), and the
<var>datum</var> field is the offending argument.
</p></dd></dl>

<div class="example">
<pre class="example">(string-ref &quot;abc&quot; 3)                    error&rarr;
;The object 3, passed as the second argument to string-ref,
; is not in the correct range.
;To continue, call RESTART with an option number:
; (RESTART 2) =&gt; Specify an argument to use in its place.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003abad_002drange_002dargument">procedure: <strong>error:bad-range-argument</strong> <em>datum operator</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:bad-range-argument</code>.  The <var>datum</var> and
<var>operator</var> fields of the condition are filled in from the
corresponding arguments to the procedure; the <var>operand</var> field of the
condition is set to <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003ainapplicable_002dobject">condition type: <strong>condition-type:inapplicable-object</strong> <em>datum operands</em></dt>
<dd><p>This type indicates an error in which a program attempted to apply an
object that is not a procedure.  The object being applied is saved in
the <var>datum</var> field, and the arguments being passed to the object are
saved as a list in the <var>operands</var> field.
</p></dd></dl>

<div class="example">
<pre class="example">(3 4)                                   error&rarr;
;The object 3 is not applicable.
;To continue, call RESTART with an option number:
; (RESTART 2) =&gt; Specify a procedure to use in its place.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-condition_002dtype_003afile_002derror">condition type: <strong>condition-type:file-error</strong> <em>filename</em></dt>
<dd><p>This is an abstract type.  It indicates that an error associated with a
file has occurred.  For example, attempting to delete a nonexistent file
will signal an error.  The <var>filename</var> field contains a filename or
pathname associated with the operation that failed.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003afile_002doperation_002derror-3">condition type: <strong>condition-type:file-operation-error</strong> <em>filename verb noun reason operator operands</em></dt>
<dd><p>This is the most common condition type for file system errors.  The
<var>filename</var> field contains the filename or pathname that was being
operated on.  The <var>verb</var> field contains a string which is the verb
or verb phrase describing the operation being performed, and the
<var>noun</var> field contains a string which is a noun or noun phrase
describing the object being operated on.  The <var>reason</var> field
contains a string describing the error that occurred.  The
<var>operator</var> field contains the procedure performing the operation (or
a symbol naming that procedure), and the <var>operands</var> field contains a
list of the arguments that were passed to that procedure.  For example,
an attempt to delete a nonexistent file would have the following field
values:
</p>
<div class="example">
<pre class="example">filename        &quot;/zu/cph/tmp/no-such-file&quot;
verb            &quot;delete&quot;
noun            &quot;file&quot;
reason          &quot;no such file or directory&quot;
operator        file-remove
operands        (&quot;/zu/cph/tmp/no-such-file&quot;)
</pre></div>

<p>and would generate a message like this:
</p>
<div class="example">
<pre class="example">(delete-file &quot;/zu/cph/tmp/no-such-file&quot;) error&rarr;
;Unable to delete file &quot;/zu/cph/tmp/no-such-file&quot; because:
; No such file or directory.
;To continue, call RESTART with an option number:
; (RESTART 3) =&gt; Try to delete the same file again.
; (RESTART 2) =&gt; Try to delete a different file.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>
</dd></dl>

<dl>
<dt id="index-error_003afile_002doperation">procedure: <strong>error:file-operation</strong> <em>index verb noun reason operator operands</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:file-operation-error</code>.  The fields of the
condition are filled in from the corresponding arguments to the
procedure, except that the filename is taken as the <var>index</var>th
element of <var>operands</var>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aderived_002dfile_002derror">condition type: <strong>condition-type:derived-file-error</strong> <em>filename condition</em></dt>
<dd><p>This is another kind of file error, which is generated by obscure
file-system errors that do not fit into the standard categories.  The
<var>filename</var> field contains the filename or pathname that was being
operated on, and the <var>condition</var> field contains a condition
describing the error in more detail.  Usually the <var>condition</var> field
contains a condition of type <code>condition-type:system-call-error</code>.
</p></dd></dl>

<dl>
<dt id="index-error_003aderived_002dfile">procedure: <strong>error:derived-file</strong> <em>filename condition</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:derived-file-error</code>.  The <var>filename</var> and
<var>condition</var> fields of the condition are filled in from the
corresponding arguments to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aport_002derror">condition type: <strong>condition-type:port-error</strong> <em>port</em></dt>
<dd><p>This is an abstract type.  It indicates that an error associated with a
I/O port has occurred.  For example, writing output to a file port can
signal an error if the disk containing the file is full; that error
would be signalled as a port error.  The <var>port</var> field contains the
associated port.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aderived_002dport_002derror">condition type: <strong>condition-type:derived-port-error</strong> <em>port condition</em></dt>
<dd><p>This is a concrete type that is signalled when port errors occur.  The
<var>port</var> field contains the port associated with the error, and the
<var>condition</var> field contains a condition object that describes the
error in more detail.  Usually the <var>condition</var> field contains a
condition of type <code>condition-type:system-call-error</code>.
</p></dd></dl>

<dl>
<dt id="index-error_003aderived_002dport">procedure: <strong>error:derived-port</strong> <em>port condition</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:derived-port-error</code>.  The <var>port</var> and
<var>condition</var> fields of the condition are filled in from the
corresponding arguments to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003avariable_002derror">condition type: <strong>condition-type:variable-error</strong> <em>location environment</em></dt>
<dd><p>This is an abstract type.  It indicates that an error associated with a
variable has occurred.  The <var>location</var> field contains the name of
the variable, and the <var>environment</var> field contains the environment
in which the variable was referenced.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aunbound_002dvariable-2">condition type: <strong>condition-type:unbound-variable</strong> <em>location environment</em></dt>
<dd><p>This type is generated when a program attempts to access or modify a
variable that is not bound.  The <var>location</var> field contains the name
of the variable, and the <var>environment</var> field contains the
environment in which the reference occurred.
</p></dd></dl>

<div class="example">
<pre class="example">foo                                     error&rarr;
;Unbound variable: foo
;To continue, call RESTART with an option number:
; (RESTART 3) =&gt; Specify a value to use instead of foo.
; (RESTART 2) =&gt; Define foo to a given value.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-condition_002dtype_003aunassigned_002dvariable-2">condition type: <strong>condition-type:unassigned-variable</strong> <em>location environment</em></dt>
<dd><p>This type is generated when a program attempts to access a variable that
is not assigned.  The <var>location</var> field contains the name of the
variable, and the <var>environment</var> field contains the environment in
which the reference occurred.
</p></dd></dl>

<div class="example">
<pre class="example">foo                                     error&rarr;
;Unassigned variable: foo
;To continue, call RESTART with an option number:
; (RESTART 3) =&gt; Specify a value to use instead of foo.
; (RESTART 2) =&gt; Set foo to a given value.
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-condition_002dtype_003aarithmetic_002derror">condition type: <strong>condition-type:arithmetic-error</strong> <em>operator operands</em></dt>
<dd><p>This is an abstract type.  It indicates that a numerical operation was
unable to complete because of an arithmetic error.  (For example,
division by zero.)  The <var>operator</var> field contains the procedure that
implements the operation (or a symbol naming the procedure), and the
<var>operands</var> field contains a list of the arguments that were passed
to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003adivide_002dby_002dzero">condition type: <strong>condition-type:divide-by-zero</strong> <em>operator operands</em></dt>
<dd><p>This type is generated when a program attempts to divide by zero.  The
<var>operator</var> field contains the procedure that implements the failing
operation (or a symbol naming the procedure), and the <var>operands</var>
field contains a list of the arguments that were passed to the
procedure.
</p></dd></dl>

<div class="example">
<pre class="example">(/ 1 0)
;Division by zero signalled by /.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003adivide_002dby_002dzero">procedure: <strong>error:divide-by-zero</strong> <em>operator operands</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:divide-by-zero</code>.  The <var>operator</var> and
<var>operands</var> fields of the condition are filled in from the
corresponding arguments to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003afloating_002dpoint_002doverflow">condition type: <strong>condition-type:floating-point-overflow</strong> <em>operator operands</em></dt>
<dd><p>This type is generated when a program performs an arithmetic operation
that results in a floating-point overflow.  The <var>operator</var> field
contains the procedure that implements the operation (or a symbol naming
the procedure), and the <var>operands</var> field contains a list of the
arguments that were passed to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003afloating_002dpoint_002dunderflow">condition type: <strong>condition-type:floating-point-underflow</strong> <em>operator operands</em></dt>
<dd><p>This type is generated when a program performs an arithmetic operation
that results in a floating-point underflow.  The <var>operator</var> field
contains the procedure that implements the operation (or a symbol naming
the procedure), and the <var>operands</var> field contains a list of the
arguments that were passed to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003aprimitive_002dprocedure_002derror">condition type: <strong>condition-type:primitive-procedure-error</strong> <em>operator operands</em></dt>
<dd><p>This is an abstract type.  It indicates that an error was generated by a
primitive procedure call.  Primitive procedures are distinguished from
ordinary procedures in that they are not written in Scheme but instead
in the underlying language of the Scheme implementation.  The
<var>operator</var> field contains the procedure that implements the
operation (or a symbol naming the procedure), and the <var>operands</var>
field contains a list of the arguments that were passed to the
procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003asystem_002dcall_002derror">condition type: <strong>condition-type:system-call-error</strong> <em>operator operands system-call error-type</em></dt>
<dd><p>This is the most common condition type generated by primitive
procedures.  A condition of this type indicates that the primitive made
a system call to the operating system, and that the system call
signalled an error.  The system-call error is reflected back to Scheme
as a condition of this type, except that many common system-call errors
are automatically translated by the Scheme implementation into more
useful forms; for example, a system-call error that occurs while trying
to delete a file will be translated into a condition of type
<code>condition-type:file-operation-error</code>.  The <var>operator</var> field
contains the procedure that implements the operation (or a symbol naming
the procedure), and the <var>operands</var> field contains a list of the
arguments that were passed to the procedure.  The <var>system-call</var> and
<var>error-type</var> fields contain symbols that describe the specific
system call that was being made and the error that occurred,
respectively; these symbols are completely operating-system dependent.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003acontrol_002derror">condition type: <strong>condition-type:control-error</strong></dt>
<dd><p>This is an abstract type.  It describes a class of errors relating to
program control flow.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003ano_002dsuch_002drestart">condition type: <strong>condition-type:no-such-restart</strong> <em>name</em></dt>
<dd><p>This type indicates that a named restart was not active when it was
expected to be.  Conditions of this type are signalled by several
procedures that look for particular named restarts, for example
<code>muffle-warning</code>.  The <var>name</var> field contains the name that was
being searched for.
</p></dd></dl>

<div class="example">
<pre class="example">(muffle-warning)                        error&rarr;
;The restart named muffle-warning is not bound.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-error_003ano_002dsuch_002drestart">procedure: <strong>error:no-such-restart</strong> <em>name</em></dt>
<dd><p>This procedure signals a condition of type
<code>condition-type:no-such-restart</code>.  The <var>name</var> field of the
condition is filled in from the corresponding argument to the procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003anot_002dloading">condition type: <strong>condition-type:not-loading</strong></dt>
<dd><p>A condition of this type is generated when the procedure
<code>current-load-pathname</code> is called from somewhere other than inside
a file being loaded.
</p></dd></dl>

<div class="example">
<pre class="example">(current-load-pathname)                 error&rarr;
;No file being loaded.
;To continue, call RESTART with an option number:
; (RESTART 1) =&gt; Return to read-eval-print level 1.
</pre></div>

<dl>
<dt id="index-condition_002dtype_003awarning">condition type: <strong>condition-type:warning</strong></dt>
<dd><p>This is an abstract type.  All warnings should inherit from this type.
Warnings are a class of conditions that are usually handled by informing
the user of the condition and proceeding the computation normally.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003asimple_002dwarning">condition type: <strong>condition-type:simple-warning</strong> <em>message irritants</em></dt>
<dd><p>This is the condition generated by the <code>warn</code> procedure.  The
fields <var>message</var> and <var>irritants</var> are taken directly from the
arguments to <code>warn</code>; <var>message</var> contains an object (usually a
string) and <var>irritants</var> contains a list of objects.  The reporter
for this type uses <code>format-error-message</code> to generate its output
from <var>message</var> and <var>irritants</var>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003asimple_002dcondition">condition type: <strong>condition-type:simple-condition</strong> <em>message irritants</em></dt>
<dd><p>This is an unspecialized condition that does not fall into any of the
standard condition classes.  The <var>message</var> field contains an object
(usually a string) and <var>irritants</var> contains a list of objects.  The
reporter for this type uses <code>format-error-message</code> to generate its
output from <var>message</var> and <var>irritants</var>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003abreakpoint">condition type: <strong>condition-type:breakpoint</strong> <em>environment message prompt</em></dt>
<dd><p>A condition of this type is generated by the breakpoint mechanism.  The
contents of its fields are beyond the scope of this document.
</p></dd></dl>
<hr>
<div class="header">
<p>
Previous: <a href="Condition-Types.html" accesskey="p" rel="prev">Condition Types</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
