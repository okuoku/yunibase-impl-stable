<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Address Hashing (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Address Hashing (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Address Hashing (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Hash-Tables.html" rel="up" title="Hash Tables">
<link href="Object-Hashing.html" rel="next" title="Object Hashing">
<link href="Resizing-of-Hash-Tables.html" rel="prev" title="Resizing of Hash Tables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Address-Hashing"></span><div class="header">
<p>
Previous: <a href="Resizing-of-Hash-Tables.html" accesskey="p" rel="prev">Resizing of Hash Tables</a>, Up: <a href="Hash-Tables.html" accesskey="u" rel="up">Hash Tables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Address-Hashing-1"></span><h4 class="subsection">11.4.4 Address Hashing</h4>
<span id="index-address-hashing"></span>

<p>The procedures described in this section may be used to make very
efficient key-hashing procedures for arbitrary objects.  All of these
procedures are based on <em>address hashing</em>, which uses the address of
an object as its hash number.  The great advantage of address hashing is
that converting an arbitrary object to a hash number is extremely fast
and takes the same amount of time for any object.
</p>
<p>The disadvantage of address hashing is that the garbage collector
changes the addresses of most objects.  The hash-table implementation
compensates for this disadvantage by automatically rehashing tables
that use address hashing when garbage collections occur.  Thus, in
order to use these procedures for key hashing, it is necessary to tell
the hash-table implementation (by means of the <var>rehash-after-gc?</var>
argument to the hash-table type constructors) that the hash numbers
computed by your key-hashing procedure must be recomputed after a
garbage collection.
</p>
<dl>
<dt id="index-eq_002dhash-1">procedure: <strong>eq-hash</strong> <em>object</em></dt>
<dt id="index-eqv_002dhash">procedure: <strong>eqv-hash</strong> <em>object</em></dt>
<dt id="index-equal_002dhash-1">procedure: <strong>equal-hash</strong> <em>object</em></dt>
<dd><p>These procedures return a hash number for <var>object</var>.  The result is
always a non-negative integer, and in the case of <code>eq-hash</code>, a
non-negative fixnum.  Two objects that are equivalent according to
<code>eq?</code>, <code>eqv?</code>, or <code>equal?</code>, respectively, will produce the
same hash number when passed as arguments to these procedures, provided
that the garbage collector does not run during or between the two calls.
</p></dd></dl>

<dl>
<dt id="index-hash_002dby_002didentity">procedure: <strong>hash-by-identity</strong> <em>key [modulus]</em></dt>
<dd><p>This <acronym>SRFI</acronym> 69 procedure returns the same value as <code>eq-hash</code>,
optionally limited by <var>modulus</var>.
</p></dd></dl>

<dl>
<dt id="index-hash">procedure: <strong>hash</strong> <em>key [modulus]</em></dt>
<dd><p>This <acronym>SRFI</acronym> 69 procedure returns the same value as <code>equal-hash</code>,
optionally limited by <var>modulus</var>.
</p></dd></dl>

<dl>
<dt id="index-hash_002dby_002deqv">obsolete procedure: <strong>hash-by-eqv</strong> <em>key [modulus]</em></dt>
<dd><p>This procedure returns the same value as <code>eqv-hash</code>, optionally
limited by <var>modulus</var>.
</p></dd></dl>

<dl>
<dt id="index-eq_002dhash_002dmod">obsolete procedure: <strong>eq-hash-mod</strong> <em>object modulus</em></dt>
<dd><p>This procedure is the key-hashing procedure used by
<code>make-strong-eq-hash-table</code>.
</p></dd></dl>

<dl>
<dt id="index-eqv_002dhash_002dmod">obsolete procedure: <strong>eqv-hash-mod</strong> <em>object modulus</em></dt>
<dd><p>This procedure is the key-hashing procedure used by
<code>make-strong-eqv-hash-table</code>.
</p></dd></dl>

<dl>
<dt id="index-equal_002dhash_002dmod">obsolete procedure: <strong>equal-hash-mod</strong> <em>object modulus</em></dt>
<dd><p>This procedure is the key-hashing procedure used by
<code>make-equal-hash-table</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Resizing-of-Hash-Tables.html" accesskey="p" rel="prev">Resizing of Hash Tables</a>, Up: <a href="Hash-Tables.html" accesskey="u" rel="up">Hash Tables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
