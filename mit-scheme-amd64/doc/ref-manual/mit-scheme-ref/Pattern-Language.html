<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Pattern Language (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Pattern Language (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Pattern Language (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Macros.html" rel="up" title="Macros">
<link href="Syntactic-Closures.html" rel="next" title="Syntactic Closures">
<link href="Syntactic-Binding-Constructs.html" rel="prev" title="Syntactic Binding Constructs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Pattern-Language"></span><div class="header">
<p>
Next: <a href="Syntactic-Closures.html" accesskey="n" rel="next">Syntactic Closures</a>, Previous: <a href="Syntactic-Binding-Constructs.html" accesskey="p" rel="prev">Syntactic Binding Constructs</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Pattern-Language-1"></span><h4 class="subsection">2.11.2 Pattern Language</h4>

<p>MIT/GNU Scheme supports a high-level pattern language for specifying macro
transformers.  This pattern language is defined by the Revised^4 Report
and is portable to other conforming Scheme implementations.  To use the
pattern language, specify a <var>transformer-spec</var> as a
<code>syntax-rules</code> form:
</p>
<dl>
<dt id="index-syntax_002drules">standard special form: <strong>syntax-rules</strong> <em>[ellipsis] literals syntax-rule &hellip;</em></dt>
<dd><p><var>Ellipsis</var> is an identifier, and if omitted defaults to
<code>...</code>.  <var>Literals</var> is a list of identifiers and each
<var>syntax-rule</var> should be of the form
</p>
<div class="example">
<pre class="example">(<var>pattern</var> <var>template</var>)
</pre></div>

<p>The <var>pattern</var> in a <var>syntax-rule</var> is a list <var>pattern</var> that
begins with the keyword for the macro.
</p>
<p>A <var>pattern</var> is either an identifier, a constant, or one of the
following
</p>
<div class="example">
<pre class="example">(<var>pattern</var> &hellip;)
(<var>pattern</var> <var>pattern</var> &hellip; . <var>pattern</var>)
(<var>pattern</var> &hellip; <var>pattern</var> <var>ellipsis</var>)
</pre></div>

<p>and a template is either an identifier, a constant, or one of the
following
</p>
<div class="example">
<pre class="example">(<var>element</var> &hellip;)
(<var>element</var> <var>element</var> &hellip; . <var>template</var>)
</pre></div>

<span id="index-_002e_002e_002e-1"></span>
<p>where an <var>element</var> is a <var>template</var> optionally followed by an
<var>ellipsis</var> and an <var>ellipsis</var> is the identifier &lsquo;<samp>...</samp>&rsquo; (which
cannot be used as an identifier in either a template or a pattern).
</p>
<p>An instance of <code>syntax-rules</code> produces a new macro transformer by
specifying a sequence of hygienic rewrite rules.  A use of a macro whose
keyword is associated with a transformer specified by
<code>syntax-rules</code> is matched against the patterns contained in the
<var>syntax-rule</var>s, beginning with the leftmost <var>syntax-rule</var>.  When
a match is found, the macro use is transcribed hygienically according to
the template.
</p>
<p>An identifier that appears in the pattern of a <var>syntax-rule</var> is a
<em>pattern-variable</em>, unless it is the keyword that begins the
pattern, is listed in <var>literals</var>, or is the identifier &lsquo;<samp>...</samp>&rsquo;.
Pattern variables match arbitrary input elements and are used to refer
to elements of the input in the template.  It is an error for the same
pattern variable to appear more than once in a <var>pattern</var>.
</p>
<p>The keyword at the beginning of the pattern in a <var>syntax-rule</var> is
not involved in the matching and is not considered a pattern variable or
literal identifier.
</p>
<p>Identifiers that appear in <var>literals</var> are interpreted as literal
identifiers to be matched against corresponding subforms of the input.
A subform in the input matches a literal identifier if and only if it is
an identifier and either both its occurrence in the macro expression and
its occurrence in the macro definition have the same lexical binding, or
the two identifiers are equal and both have no lexical binding.
</p>
<p>A subpattern followed by &lsquo;<samp>...</samp>&rsquo; can match zero or more elements of
the input.  It is an error for &lsquo;<samp>...</samp>&rsquo; to appear in <var>literals</var>.
Within a pattern the identifier &lsquo;<samp>...</samp>&rsquo; must follow the last element
of a nonempty sequence of subpatterns.
</p>
<p>More formally, an input form <var>F</var> matches a pattern <var>P</var> if and
only if:
</p>
<ul>
<li> <var>P</var> is a non-literal identifier; or

</li><li> <var>P</var> is a literal identifier and <var>F</var> is an identifier with the
same binding; or

</li><li> <var>P</var> is a list <code>(<var>P_1</var> &hellip; <var>P_n</var>)</code> and <var>F</var> is a
list of <var>n</var> forms that match <var>P_1</var> through <var>P_n</var>,
respectively; or

</li><li> <var>P</var> is an improper list <code>(<var>P_1</var> <var>P_2</var> &hellip; <var>P_n</var>
. <var>P_n+1</var>)</code> and <var>F</var> is a list or improper list of <var>n</var> or
more forms that match <var>P_1</var> through <var>P_n</var>, respectively, and
whose <var>n</var>th &ldquo;cdr&rdquo; matches <var>P_n+1</var>; or

</li><li> <var>P</var> is of the form <code>(<var>P_1</var> &hellip; <var>P_n</var> <var>P_n+1</var>
<var>ellipsis</var>)</code> where <var>ellipsis</var> is the identifier &lsquo;<samp>...</samp>&rsquo; and
<var>F</var> is a proper list of at least <var>n</var> forms, the first <var>n</var> of
which match <var>P_1</var> through <var>P_n</var>, respectively, and each
remaining element of <var>F</var> matches <var>P_n+1</var>; or

</li><li> <var>P</var> is a datum and <var>F</var> is equal to <var>P</var> in the sense of the
<code>equal?</code> procedure.
</li></ul>

<p>It is an error to use a macro keyword, within the scope of its
binding, in an expression that does not match any of the patterns.
</p>
<p>When a macro use is transcribed according to the template of the
matching <var>syntax rule</var>, pattern variables that occur in the template
are replaced by the subforms they match in the input.  Pattern variables
that occur in subpatterns followed by one or more instances of the
identifier &lsquo;<samp>...</samp>&rsquo; are allowed only in subtemplates that are followed
by as many instances of &lsquo;<samp>...</samp>&rsquo;.  They are replaced in the output by
all of the subforms they match in the input, distributed as indicated.
It is an error if the output cannot be built up as specified.
</p>
<p>Identifiers that appear in the template but are not pattern variables or
the identifier &lsquo;<samp>...</samp>&rsquo; are inserted into the output as literal
identifiers.  If a literal identifier is inserted as a free identifier
then it refers to the binding of that identifier within whose scope the
instance of <code>syntax-rules</code> appears.  If a literal identifier is
inserted as a bound identifier then it is in effect renamed to prevent
inadvertent captures of free identifiers.
</p>
<div class="example">
<pre class="example">(let ((=&gt; #f))
  (cond (#t =&gt; 'ok)))           &rArr; ok
</pre></div>

<p>The macro transformer for <code>cond</code> recognizes <code>=&gt;</code>
as a local variable, and hence an expression, and not as the
top-level identifier <code>=&gt;</code>, which the macro transformer treats
as a syntactic keyword.  Thus the example expands into
</p>
<div class="example">
<pre class="example">(let ((=&gt; #f))
  (if #t (begin =&gt; 'ok)))
</pre></div>

<p>instead of
</p>
<div class="example">
<pre class="example">(let ((=&gt; #f))
  (let ((temp #t))
    (if temp 
        ('ok temp))))
</pre></div>

<p>which would result in an invalid procedure call.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Syntactic-Closures.html" accesskey="n" rel="next">Syntactic Closures</a>, Previous: <a href="Syntactic-Binding-Constructs.html" accesskey="p" rel="prev">Syntactic Binding Constructs</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
