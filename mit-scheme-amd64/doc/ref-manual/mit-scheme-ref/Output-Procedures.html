<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Output Procedures (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Output Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Output Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Blocking-Mode.html" rel="next" title="Blocking Mode">
<link href="Input-Procedures.html" rel="prev" title="Input Procedures">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Output-Procedures"></span><div class="header">
<p>
Next: <a href="Blocking-Mode.html" accesskey="n" rel="next">Blocking Mode</a>, Previous: <a href="Input-Procedures.html" accesskey="p" rel="prev">Input Procedures</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Output-Procedures-1"></span><h3 class="section">14.6 Output Procedures</h3>
<span id="index-output-procedures"></span>

<span id="index-buffering_002c-of-output"></span>
<span id="index-flushing_002c-of-buffered-output"></span>
<p>Output ports may or may not support <em>buffering</em> of output, in which
output characters are collected together in a buffer and then sent to
the output device all at once.  (Most of the output ports implemented by
the runtime system support buffering.)  Sending all of the characters in
the buffer to the output device is called <em>flushing</em> the buffer.  In
general, output procedures do not flush the buffer of an output port
unless the buffer is full.
</p>
<span id="index-discretionary-flushing_002c-of-buffered-output"></span>
<span id="index-discretionary_002dflush_002doutput"></span>
<span id="index-flush_002doutput_002dport"></span>
<p>However, the standard output procedures described in this section
perform what is called <em>discretionary</em> flushing of the buffer.
Discretionary output flushing works as follows.  After a procedure
performs its output (writing characters to the output buffer), it checks
to see if the port implements an operation called
<code>discretionary-flush-output</code>.  If so, then that operation is
invoked to flush the buffer.  At present, only the console port defines
<code>discretionary-flush-output</code>; this is used to guarantee that output
to the console appears immediately after it is written, without
requiring calls to <code>flush-output-port</code>.
</p>
<p>In this section, all optional arguments called <var>port</var> default to
the current output port.
</p>
<p><strong>Note</strong>: MIT/GNU Scheme doesn&rsquo;t support datum labels, so any
behavior in <code>write</code>, <code>write-shared</code>, or <code>write-simple</code>
that depends on datum labels is not implemented.  At present all three
of these procedures are equivalent.  This will be remedied in a future
release.
</p>
<dl>
<dt id="index-write-6">standard procedure: <strong>write</strong> <em>object [port]</em></dt>
<dd><p>Writes a representation of <var>object</var> to the given textual output
<var>port</var>.  Strings that appear in the written representation are
enclosed in quotation marks, and within those strings backslash and
quotation mark characters are escaped by backslashes.  Symbols that
contain non-<acronym>ASCII</acronym> characters are escaped with vertical lines.
Character objects are written using the <code>#\</code> notation.
</p>
<p>If <var>object</var> contains cycles which would cause an infinite loop
using the normal written representation, then at least the objects
that form part of the cycle must be represented using datum labels.
Datum labels must not be used if there are no cycles.
</p>
<p>Implementations may support extended syntax to represent record types
or other types that do not have datum representations.
</p>
<p>The <code>write</code> procedure returns an unspecified value.
</p>
<p>On MIT/GNU Scheme <code>write</code> performs discretionary output flushing.
</p></dd></dl>

<dl>
<dt id="index-write_002dshared">standard procedure: <strong>write-shared</strong> <em>object [port]</em></dt>
<dd><p>The <code>write-shared</code> procedure is the same as <code>write</code>, except
that shared structure must be represented using datum labels for all
pairs and vectors that appear more than once in the output.
</p></dd></dl>

<dl>
<dt id="index-write_002dsimple">standard procedure: <strong>write-simple</strong> <em>object [port]</em></dt>
<dd><p>The <code>write-simple</code> procedure is the same as <code>write</code>, except
that shared structure is never represented using datum labels.  This
can cause <code>write-simple</code> not to terminate if <var>object</var>
contains circular structure.
</p></dd></dl>

<dl>
<dt id="index-display">standard procedure: <strong>display</strong> <em>object [port]</em></dt>
<dd><p>Writes a representation of <var>object</var> to the given textual output <var>port</var>.
Strings that appear in the written representation are output as if by
write-string instead of by write.  Symbols are not escaped.  Character
objects appear in the representation as if written by <code>write-char</code>
instead of by <code>write</code>.  The display representation of other objects is
unspecified.  However, <code>display</code> must not loop forever on
self-referencing pairs, vectors, or records.  Thus if the normal <code>write</code>
representation is used, datum labels are needed to represent cycles as
in <code>write</code>.
</p>
<p>Implementations may support extended syntax to represent record types
or other types that do not have datum representations.
</p>
<p>The <code>display</code> procedure returns an unspecified value.
</p>
<p><em>Rationale</em>: The <code>write</code> procedure is intended for producing
machine-readable output and <code>display</code> for producing
human-readable output.
</p></dd></dl>

<dl>
<dt id="index-newline">standard procedure: <strong>newline</strong> <em>[port]</em></dt>
<dd><p>Writes an end of line to textual output <var>port</var>.  Exactly how this
is done differs from one operating system to another.  Returns an
unspecified value.
</p></dd></dl>

<dl>
<dt id="index-write_002dchar-2">standard procedure: <strong>write-char</strong> <em>char [port]</em></dt>
<dd><p>Writes the character <var>char</var> (not an external representation of the
character) to the given textual output <var>port</var> and returns an
unspecified value.
</p></dd></dl>

<dl>
<dt id="index-write_002dstring">standard procedure: <strong>write-string</strong> <em>string [port [start [end]]]</em></dt>
<dd><p>Writes the characters of <var>string</var> from <var>start</var> to <var>end</var> in
left-to-right order to the textual output <var>port</var>.
</p></dd></dl>

<dl>
<dt id="index-write_002dsubstring">obsolete procedure: <strong>write-substring</strong> <em>string start end [port]</em></dt>
<dd><p>This procedure is <strong>deprecated</strong>; use <code>write-string</code> instead.
</p></dd></dl>

<dl>
<dt id="index-write_002du8-1">standard procedure: <strong>write-u8</strong> <em>byte [port]</em></dt>
<dd><p>Writes the <var>byte</var> to the given binary output <var>port</var> and
returns an unspecified value.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive output port in
non-blocking mode and writing a byte would block, <code>write-u8</code>
immediately returns <code>#f</code> without writing anything.  Otherwise
<var>byte</var> is written and <code>1</code> is returned.
</p></dd></dl>

<dl>
<dt id="index-write_002dbytevector">standard procedure: <strong>write-bytevector</strong> <em>bytevector [port [start [end]]]</em></dt>
<dd><p>Writes the bytes of <var>bytevector</var> from <var>start</var> to <var>end</var> in
left-to-right order to the binary output <var>port</var>.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive output port in
non-blocking mode <code>write-bytevector</code> will write as many bytes as
it can without blocking, then returns the number of bytes written; if
no bytes can be written without blocking, returns <code>#f</code> without
writing anything.  Otherwise <code>write-bytevector</code> returns the
number of bytes actually written, which may be less than the number
requested if unable to write all the bytes.  (For example, if writing
to a file and the file system is full.)
</p></dd></dl>

<dl>
<dt id="index-flush_002doutput_002dport-1">standard procedure: <strong>flush-output-port</strong> <em>[port]</em></dt>
<dd><p>Flushes any buffered output from the buffer of <var>port</var> to the
underlying file or device and returns an unspecified value.
</p></dd></dl>

<dl>
<dt id="index-flush_002doutput">obsolete procedure: <strong>flush-output</strong> <em>[port]</em></dt>
<dd><p>This procedure is <strong>deprecated</strong>; use <code>flush-output-port</code>
instead.
</p></dd></dl>

<dl>
<dt id="index-fresh_002dline">procedure: <strong>fresh-line</strong> <em>[port]</em></dt>
<dd><p>Most output ports are able to tell whether or not they are at the
beginning of a line of output.  If <var>port</var> is such a port,
this procedure writes an end-of-line to the port only if the port is not
already at the beginning of a line.  If <var>port</var> is not such a
port, this procedure is identical to <code>newline</code>.  In either case,
<code>fresh-line</code> performs discretionary output flushing and returns an
unspecified value.
</p></dd></dl>

<dl>
<dt id="index-write_002dline">procedure: <strong>write-line</strong> <em>object [port]</em></dt>
<dd><p>Like <code>write</code>, except that it writes an end-of-line to
<var>port</var> after writing <var>object</var>&rsquo;s representation.  This
procedure performs discretionary output flushing and returns an
unspecified value.
</p></dd></dl>

<dl>
<dt id="index-beep">procedure: <strong>beep</strong> <em>[port]</em></dt>
<dd><span id="index-console_002c-ringing-the-bell"></span>
<span id="index-ringing-the-console-bell"></span>
<span id="index-bell_002c-ringing-on-console"></span>
<p>Performs a &ldquo;beep&rdquo; operation on <var>port</var>, performs
discretionary output flushing, and returns an unspecified value.  On the
console port, this usually causes the console bell to beep, but more
sophisticated interactive ports may take other actions, such as flashing
the screen.  On most output ports, e.g. file and string output ports,
this does nothing.
</p></dd></dl>

<dl>
<dt id="index-clear">procedure: <strong>clear</strong> <em>[port]</em></dt>
<dd><span id="index-console_002c-clearing"></span>
<span id="index-display_002c-clearing"></span>
<span id="index-screen_002c-clearing"></span>
<span id="index-terminal-screen_002c-clearing"></span>
<span id="index-clearing-the-console-screen"></span>
<p>&ldquo;Clears the screen&rdquo; of <var>port</var>, performs discretionary
output flushing, and returns an unspecified value.  On a terminal or
window, this has a well-defined effect.  On other output ports, e.g.
file and string output ports, this does nothing.
</p></dd></dl>

<dl>
<dt id="index-pp">procedure: <strong>pp</strong> <em>object [port [as-code?]]</em></dt>
<dd><span id="index-pretty-printer"></span>
<p><code>pp</code> prints <var>object</var> in a visually appealing and structurally
revealing manner on <var>port</var>.  If object is a procedure,
<code>pp</code> attempts to print the source text.  If the optional argument
<var>as-code?</var> is true, <code>pp</code> prints lists as Scheme code, providing
appropriate indentation; by default this argument is false.  <code>pp</code>
performs discretionary output flushing and returns an unspecified value.
</p></dd></dl>

<span id="index-parameterize-7"></span>
<p>The following parameters may be used with <code>parameterize</code> to
change the behavior of the <code>write</code> and <code>display</code> procedures.
</p>
<dl>
<dt id="index-param_003aprinter_002dradix">parameter: <strong>param:printer-radix</strong></dt>
<dd><p>This parameter specifies the default radix used to print numbers.  Its
value must be one of the exact integers <code>2</code>, <code>8</code>, <code>10</code>,
or <code>16</code>; the default is <code>10</code>.  For values other than
<code>10</code>, numbers are prefixed to indicate their radix.
</p></dd></dl>

<dl>
<dt id="index-param_003aprinter_002dlist_002dbreadth_002dlimit">parameter: <strong>param:printer-list-breadth-limit</strong></dt>
<dd><p>This parameter specifies a limit on the length of the printed
representation of a list or vector; for example, if the limit is
<code>4</code>, only the first four elements of any list are printed, followed
by ellipses to indicate any additional elements.  The value of this
parameter must be an exact non-negative integer, or <code>#f</code> meaning no
limit; the default is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(parameterize ((param:printer-list-breadth-limit 4))
  (write-to-string '(a b c d)))
                                &rArr; &quot;(a b c d)&quot;
(parameterize ((param:printer-list-breadth-limit 4))
  (write-to-string '(a b c d e)))
                                &rArr; &quot;(a b c d ...)&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-param_003aprinter_002dlist_002ddepth_002dlimit">parameter: <strong>param:printer-list-depth-limit</strong></dt>
<dd><p>This parameter specifies a limit on the nesting of lists and vectors in
the printed representation.  If lists (or vectors) are more deeply
nested than the limit, the part of the representation that exceeds the
limit is replaced by ellipses.  The value of this parameter must be an
exact non-negative integer, or <code>#f</code> meaning no limit; the default
is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(parameterize ((param:printer-list-depth-limit 4))
  (write-to-string '((((a))) b c d)))
                                &rArr; &quot;((((a))) b c d)&quot;
(parameterize ((param:printer-list-depth-limit 4))
  (write-to-string '(((((a)))) b c d)))
                                &rArr; &quot;((((...))) b c d)&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-param_003aprinter_002dstring_002dlength_002dlimit">parameter: <strong>param:printer-string-length-limit</strong></dt>
<dd><p>This parameter specifies a limit on the length of the printed
representation of strings.  If a string&rsquo;s length exceeds this limit, the
part of the printed representation for the characters exceeding the
limit is replaced by ellipses.  The value of this parameter must be an
exact non-negative integer, or <code>#f</code> meaning no limit; the default
is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(parameterize ((param:printer-string-length-limit 4))
  (write-to-string &quot;abcd&quot;))
                                &rArr; &quot;\&quot;abcd\&quot;&quot;
(parameterize ((param:printer-string-length-limit 4))
  (write-to-string &quot;abcde&quot;))
                                &rArr; &quot;\&quot;abcd...\&quot;&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-param_003aprint_002dwith_002dmaximum_002dreadability_003f">parameter: <strong>param:print-with-maximum-readability?</strong></dt>
<dd><p>This parameter, which takes a boolean value, tells the printer to use a
special printed representation for objects that normally print in a form
that cannot be recognized by <code>read</code>.  These objects are printed
using the representation <code>#@<var>n</var></code>, where <var>n</var> is the result
of calling <code>hash</code> on the object to be printed.  The reader
recognizes this syntax, calling <code>unhash</code> on <var>n</var> to get back the
original object.  Note that this printed representation can only be
recognized by the Scheme program in which it was generated, because
these hash numbers are different for each invocation of Scheme.
</p></dd></dl>

<dl>
<dt id="index-_002aunparser_002dradix_002a">obsolete variable: <strong>*unparser-radix*</strong></dt>
<dt id="index-_002aunparser_002dlist_002dbreadth_002dlimit_002a">obsolete variable: <strong>*unparser-list-breadth-limit*</strong></dt>
<dt id="index-_002aunparser_002dlist_002ddepth_002dlimit_002a">obsolete variable: <strong>*unparser-list-depth-limit*</strong></dt>
<dt id="index-_002aunparser_002dstring_002dlength_002dlimit_002a">obsolete variable: <strong>*unparser-string-length-limit*</strong></dt>
<dt id="index-_002aunparse_002dwith_002dmaximum_002dreadability_003f_002a">obsolete variable: <strong>*unparse-with-maximum-readability?*</strong></dt>
<dd><p>These variables are <strong>deprecated</strong>; instead use the corresponding
parameter objects.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Blocking-Mode.html" accesskey="n" rel="next">Blocking Mode</a>, Previous: <a href="Input-Procedures.html" accesskey="p" rel="prev">Input Procedures</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
