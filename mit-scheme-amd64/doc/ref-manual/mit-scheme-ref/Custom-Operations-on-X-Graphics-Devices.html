<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Custom Operations on X Graphics Devices (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Custom Operations on X Graphics Devices (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Custom Operations on X Graphics Devices (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="X-Graphics.html" rel="up" title="X Graphics">
<link href="Win32-Graphics.html" rel="next" title="Win32 Graphics">
<link href="Utilities-for-X-Graphics.html" rel="prev" title="Utilities for X Graphics">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Custom-Operations-on-X-Graphics-Devices"></span><div class="header">
<p>
Previous: <a href="Utilities-for-X-Graphics.html" accesskey="p" rel="prev">Utilities for X Graphics</a>, Up: <a href="X-Graphics.html" accesskey="u" rel="up">X Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Custom-Operations-on-X-Graphics-Devices-1"></span><h4 class="subsection">17.9.3 Custom Operations on X Graphics Devices</h4>

<p>Custom operations are invoked using the procedure
<code>graphics-operation</code>.  For example,
</p>
<div class="example">
<pre class="example">(graphics-operation device 'set-foreground-color &quot;blue&quot;)
</pre></div>

<dl>
<dt id="index-set_002dbackground_002dcolor-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-background-color</strong> <em>color-name</em></dt>
<dt id="index-set_002dforeground_002dcolor-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-foreground-color</strong> <em>color-name</em></dt>
<dt id="index-set_002dborder_002dcolor-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-border-color</strong> <em>color-name</em></dt>
<dt id="index-set_002dmouse_002dcolor-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-mouse-color</strong> <em>color-name</em></dt>
<dd><span id="index-graphics_002dclear-1"></span>
<p>These operations change the colors associated with a window.
<var>Color-name</var> must be a string, which is the X server&rsquo;s name for the
desired color.  <code>set-border-color</code> and <code>set-mouse-color</code>
immediately change the border and mouse-cursor colors.
<code>set-background-color</code> and <code>set-foreground-color</code> change the
colors to be used when drawing, but have no effect on anything drawn
prior to their invocation.  Because changing the background color
affects the entire window, we recommend calling <code>graphics-clear</code> on
the window&rsquo;s device afterwards.  Color names include both mnemonic
names, like <code>&quot;red&quot;</code>, and intensity names specified in the
<code>&quot;#<var>rrggbb</var>&quot;</code> notation.
</p></dd></dl>

<dl>
<dt id="index-draw_002darc-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>draw-arc</strong> <em>x y radius-x radius-y angle-start angle-sweep fill?</em></dt>
<dd><span id="index-drawing-arcs-and-circles_002c-graphics"></span>
<span id="index-graphics_002c-drawing-arcs-and-circles"></span>
<span id="index-circles_002c-drawing"></span>
<span id="index-draw_002darc"></span>

<p>Operation <code>draw-arc</code> draws or fills an arc.  An arc is a segment of
a circle, which may have been stretched along the x- or y- axis to form
an ellipse.
</p>
<p>The parameters <var>x</var>, <var>y</var>, <var>radius-x</var> and <var>radius-y</var>
describe the circle and <var>angle-start</var> and <var>angle-sweep</var> choose
which part of the circle is drawn.  The arc is drawn on the graphics
device with the center of the circle at the virtual coordinates given by
<var>x</var> and <var>y</var>.  <var>radius-x</var> and <var>radius-y</var> determine the
size of the circle in virtual coordinate units.
</p>
<p>The parameter <var>angle-start</var> determines where the arc starts.  It is
measured in degrees in an anti-clockwise direction, starting at 3
o&rsquo;clock.  <var>angle-sweep</var> determines how much of the circle is drawn.
It too is measured anti-clockwise in degrees.  A negative value means
the measurement is in a clockwise direction.
</p>
<p>Note that the angles are determined on a unit circle before it is
stretched into an ellipse, so the actual angles that you will see on the
computer screen depends on all of: <var>radius-x</var> and <var>radius-y</var>,
the window size, and the virtual coordinates.
</p>
<p>If <var>fill?</var> is <code>#f</code> then just the segment of the circle is
drawn, otherwise the arc is filled in a pie-slice fashion.
</p>
<p>This draws a quarter circle pie slice, standing on its point, with point
at virtual coordinates (3,5):
</p>
<div class="example">
<pre class="example">(graphics-operation g 'draw-arc 3 5 .5 .5 45 90 #t)
</pre></div>

</dd></dl>

<dl>
<dt id="index-draw_002dcircle-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>draw-circle</strong> <em>x y radius</em></dt>
<dt id="index-fill_002dcircle-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>fill-circle</strong> <em>x y radius</em></dt>
<dd><span id="index-drawing-arcs-and-circles_002c-graphics-1"></span>
<span id="index-graphics_002c-drawing-arcs-and-circles-1"></span>
<span id="index-circles_002c-drawing-1"></span>
<span id="index-draw_002dcircle"></span>
<span id="index-fill_002dcircle"></span>
<p>These operations draw a circle (outline) or a filled circle (solid) at
on the graphics device at the virtual coordinates given by <var>x</var> and
<var>y</var>.  These operations could be implemented trivially interms of the
<code>draw-arc</code> operation.
</p></dd></dl>

<dl>
<dt id="index-set_002dborder_002dwidth-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-border-width</strong> <em>width</em></dt>
<dt id="index-set_002dinternal_002dborder_002dwidth-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-internal-border-width</strong> <em>width</em></dt>
<dd><span id="index-graphics_002dclear-2"></span>
<p>These operations change the external and internal border widths of a
window.  <var>Width</var> must be an exact non-negative integer, specified in
pixels.  The change takes place immediately.  Note that changing the
internal border width can cause displayed graphics to be garbled; we
recommend calling <code>graphics-clear</code> on the window&rsquo;s device after
doing so.
</p></dd></dl>

<dl>
<dt id="index-set_002dfont-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-font</strong> <em>font-name</em></dt>
<dd><p>Changes the font used when drawing text in a window.  <var>Font-name</var>
must be a string that is a font name known to the X server.  This
operation does not affect text drawn prior to its invocation.
</p></dd></dl>

<dl>
<dt id="index-set_002dmouse_002dshape-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>set-mouse-shape</strong> <em>shape-number</em></dt>
<dd><p>Changes the shape of the mouse cursor.  <var>Shape-number</var> is an exact
non-negative integer that is used as an index into the mouse-shape font;
when multiplied by 2 this number corresponds to an index in the file<br>
<samp>/usr/include/X11/cursorfont.h</samp>.
</p></dd></dl>

<dl>
<dt id="index-map_002dwindow-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>map-window</strong></dt>
<dt id="index-withdraw_002dwindow-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>withdraw-window</strong></dt>
<dd><p>These operations control the mapping of windows.  They correspond
directly to Xlib&rsquo;s <code>XMapWindow</code> and <code>XWithdrawWindow</code>.
</p></dd></dl>

<dl>
<dt id="index-resize_002dwindow-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>resize-window</strong> <em>width height</em></dt>
<dd><p>Changes the size of a window.  <var>Width</var> and <var>height</var> must be
exact non-negative integers.  The operation corresponds directly to
Xlib&rsquo;s <code>XResizeWindow</code>.
</p>
<p>This operation resets the virtual coordinate system and the clip
rectangle.
</p></dd></dl>

<dl>
<dt id="index-move_002dwindow-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>move-window</strong> <em>x y</em></dt>
<dd><p>Changes the position of a window on the display.  <var>X</var> and <var>y</var>
must be exact integers.  The operation corresponds directly to Xlib&rsquo;s
<code>XMoveWindow</code>.  Note that the coordinates <var>x</var> and <var>y</var> do
not take the external border into account, and therefore will not
position the window as you might like.  The only reliable way to
position a window is to ask a window manager to do it for you.
</p></dd></dl>

<dl>
<dt id="index-get_002ddefault-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>get-default</strong> <em>resource property</em></dt>
<dd><p>This operation corresponds directly to Xlib&rsquo;s <code>XGetDefault</code>.
<var>Resource</var> and <var>property</var> must be strings.  The operation
returns the character string corresponding to the association of
<var>resource</var> and <var>property</var>; if no such association exists,
<code>#f</code> is returned.
</p></dd></dl>

<dl>
<dt id="index-copy_002darea-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>copy-area</strong> <em>source-x-left source-y-top width height destination-x-left destination-y-top</em></dt>
<dd><p>This operation copies the contents of the rectangle specified by
<var>source-x-left</var>, <var>source-y-top</var>, <var>width</var>, and <var>height</var> to
the rectangle of the same dimensions at <var>destination-x-left</var> and
<var>destination-y-top</var>.
</p></dd></dl>

<dl>
<dt id="index-font_002dstructure-on-x_002dgraphics_002ddevice">operation on x-graphics-device: <strong>font-structure</strong> <em>font-name</em></dt>
<dd><p>Returns a Scheme equivalent of the X font structure for the font named
<var>font-name</var>.  If the string <var>font-name</var> does not name a font
known to the X server, or names a 16-bit font, <code>#f</code> is returned.
</p></dd></dl>

<dl>
<dt id="index-x_002dfont_002dstructure_002fname">procedure: <strong>x-font-structure/name</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fdirection">procedure: <strong>x-font-structure/direction</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fall_002dchars_002dexist">procedure: <strong>x-font-structure/all-chars-exist</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fdefault_002dchar">procedure: <strong>x-font-structure/default-char</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fmin_002dbounds">procedure: <strong>x-font-structure/min-bounds</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fmax_002dbounds">procedure: <strong>x-font-structure/max-bounds</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fstart_002dindex">procedure: <strong>x-font-structure/start-index</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fcharacter_002dbounds">procedure: <strong>x-font-structure/character-bounds</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fmax_002dascent">procedure: <strong>x-font-structure/max-ascent</strong> <em>font-structure</em></dt>
<dt id="index-x_002dfont_002dstructure_002fmax_002ddescent">procedure: <strong>x-font-structure/max-descent</strong> <em>font-structure</em></dt>
<dd><p>These procedures extract the components of the font description
structure returned by the X graphics operation <code>font-structure</code>.  A
more complete description of these components appears in documentation
of the <code>XLoadQueryFont</code> Xlib call.  <code>start-index</code> is the index
of the first character available in the font.  The <code>min-bounds</code> and
<code>max-bounds</code> components are structures of type
<code>x-character-bounds</code>, and the <code>character-bounds</code> component is
a vector of the same type.
</p></dd></dl>

<dl>
<dt id="index-x_002dcharacter_002dbounds_002flbearing">procedure: <strong>x-character-bounds/lbearing</strong> <em>character-bounds</em></dt>
<dt id="index-x_002dcharacter_002dbounds_002frbearing">procedure: <strong>x-character-bounds/rbearing</strong> <em>character-bounds</em></dt>
<dt id="index-x_002dcharacter_002dbounds_002fwidth">procedure: <strong>x-character-bounds/width</strong> <em>character-bounds</em></dt>
<dt id="index-x_002dcharacter_002dbounds_002fascent">procedure: <strong>x-character-bounds/ascent</strong> <em>character-bounds</em></dt>
<dt id="index-x_002dcharacter_002dbounds_002fdescent">procedure: <strong>x-character-bounds/descent</strong> <em>character-bounds</em></dt>
<dd><p>These procedures extract components of objects of type
<code>x-character-bounds</code>.  A more complete description of them appears
in documentation of the<br> <code>XLoadQueryFont</code> Xlib call.
</p></dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Utilities-for-X-Graphics.html" accesskey="p" rel="prev">Utilities for X Graphics</a>, Up: <a href="X-Graphics.html" accesskey="u" rel="up">X Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
