<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Association Lists (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Association Lists (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Association Lists (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="1D-Tables.html" rel="next" title="1D Tables">
<link href="Associations.html" rel="prev" title="Associations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Association-Lists"></span><div class="header">
<p>
Next: <a href="1D-Tables.html" accesskey="n" rel="next">1D Tables</a>, Previous: <a href="Associations.html" accesskey="p" rel="prev">Associations</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Association-Lists-1"></span><h3 class="section">11.1 Association Lists</h3>

<span id="index-association-list-_0028defn_0029"></span>
<span id="index-list_002c-association-_0028defn_0029"></span>
<span id="index-alist-_0028defn_0029"></span>
<span id="index-key_002c-of-association-list-element-_0028defn_0029"></span>
<p>An <em>association list</em>, or <em>alist</em>, is a data structure used very
frequently in Scheme.  An alist is a list of pairs, each of which is
called an <em>association</em>.  The car of an association is called the
<em>key</em>.
</p>
<p>An advantage of the alist representation is that an alist can be
incrementally augmented simply by adding new entries to the front.
Moreover, because the searching procedures <code>assv</code> et al. search the
alist in order, new entries can &ldquo;shadow&rdquo; old entries.  If an alist is
viewed as a mapping from keys to data, then the mapping can be not only
augmented but also altered in a non-destructive manner by adding new
entries to the front of the alist.<a id="DOCF11" href="#FOOT11"><sup>11</sup></a>
</p>
<dl>
<dt id="index-alist_003f">procedure: <strong>alist?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-alist"></span>
<span id="index-list_003f-1"></span>
<p>Returns <code>#t</code> if <var>object</var> is an association list (including the
empty list); otherwise returns <code>#f</code>.  Any <var>object</var> satisfying this
predicate also satisfies <code>list?</code>.
</p></dd></dl>

<dl>
<dt id="index-assq">procedure: <strong>assq</strong> <em>object alist</em></dt>
<dt id="index-assv">procedure: <strong>assv</strong> <em>object alist</em></dt>
<dt id="index-assoc">procedure: <strong>assoc</strong> <em>object alist</em></dt>
<dd><span id="index-searching_002c-of-alist"></span>
<span id="index-eq_003f-4"></span>
<span id="index-eqv_003f-5"></span>
<span id="index-equal_003f-3"></span>
<p>These procedures find the first pair in <var>alist</var> whose car field is
<var>object</var>, and return that pair; the returned pair is always an
<em>element</em> of <var>alist</var>, <em>not</em> one of the pairs from which
<var>alist</var> is composed.  If no pair in <var>alist</var> has <var>object</var> as
its car, <code>#f</code> (n.b.: not the empty list) is returned.  <code>assq</code>
uses <code>eq?</code> to compare <var>object</var> with the car fields of the pairs
in <var>alist</var>, while <code>assv</code> uses <code>eqv?</code> and <code>assoc</code> uses
<code>equal?</code>.<a id="DOCF12" href="#FOOT12"><sup>12</sup></a>
</p>
<div class="example">
<pre class="example">(define e '((a 1) (b 2) (c 3)))
(assq 'a e)                             &rArr;  (a 1)
(assq 'b e)                             &rArr;  (b 2)
(assq 'd e)                             &rArr;  #f
(assq (list 'a) '(((a)) ((b)) ((c))))   &rArr;  #f
(assoc (list 'a) '(((a)) ((b)) ((c))))  &rArr;  ((a))
(assq 5 '((2 3) (5 7) (11 13)))         &rArr;  <span class="roman">unspecified</span>
(assv 5 '((2 3) (5 7) (11 13)))         &rArr;  (5 7)
</pre></div>
</dd></dl>

<dl>
<dt id="index-association_002dprocedure">procedure: <strong>association-procedure</strong> <em>predicate selector</em></dt>
<dd><p>Returns an association procedure that is similar to <code>assv</code>, except
that <var>selector</var> (a procedure of one argument) is used to select the
key from the association, and <var>predicate</var> (an equivalence predicate)
is used to compare the key to the given item.  This can be used to make
association lists whose elements are, say, vectors instead of pairs
(also see <a href="Searching-Lists.html">Searching Lists</a>).
</p>
<p>For example, here is how <code>assv</code> could be implemented:
</p>
<div class="example">
<pre class="example">(define assv (association-procedure eqv? car))
</pre></div>

<p>Another example is a &ldquo;reverse association&rdquo; procedure:
</p>
<div class="example">
<pre class="example">(define rassv (association-procedure eqv? cdr))
</pre></div>
</dd></dl>

<dl>
<dt id="index-del_002dassq">procedure: <strong>del-assq</strong> <em>object alist</em></dt>
<dt id="index-del_002dassv">procedure: <strong>del-assv</strong> <em>object alist</em></dt>
<dt id="index-del_002dassoc">procedure: <strong>del-assoc</strong> <em>object alist</em></dt>
<dd><span id="index-deletion_002c-of-alist-element"></span>
<span id="index-eq_003f-5"></span>
<span id="index-eqv_003f-6"></span>
<span id="index-equal_003f-4"></span>
<p>These procedures return a newly allocated copy of <var>alist</var> in which
all associations with keys equal to <var>object</var> have been removed.
Note that while the returned copy is a newly allocated list, the
association pairs that are the elements of the list are shared with
<var>alist</var>, not copied.  <code>del-assq</code> uses <code>eq?</code> to compare
<var>object</var> with the keys, while <code>del-assv</code> uses <code>eqv?</code> and
<code>del-assoc</code> uses <code>equal?</code>.
</p>
<div class="example">
<pre class="example">(define a
  '((butcher . &quot;231 e22nd St.&quot;)
    (baker . &quot;515 w23rd St.&quot;)
    (hardware . &quot;988 Lexington Ave.&quot;)))

(del-assq 'baker a)
     &rArr;
     ((butcher . &quot;231 e22nd St.&quot;)
      (hardware . &quot;988 Lexington Ave.&quot;))
</pre></div>
</dd></dl>

<dl>
<dt id="index-del_002dassq_0021">procedure: <strong>del-assq!</strong> <em>object alist</em></dt>
<dt id="index-del_002dassv_0021">procedure: <strong>del-assv!</strong> <em>object alist</em></dt>
<dt id="index-del_002dassoc_0021">procedure: <strong>del-assoc!</strong> <em>object alist</em></dt>
<dd><span id="index-eq_003f-6"></span>
<span id="index-eqv_003f-7"></span>
<span id="index-equal_003f-5"></span>
<p>These procedures remove from <var>alist</var> all associations with keys
equal to <var>object</var>.  They return the resulting list.
<code>del-assq!</code> uses <code>eq?</code> to compare <var>object</var> with the keys,
while <code>del-assv!</code> uses <code>eqv?</code> and <code>del-assoc!</code> uses
<code>equal?</code>.  These procedures are like <code>del-assq</code>,
<code>del-assv</code>, and <code>del-assoc</code>, respectively, except that they
destructively modify <var>alist</var>.
</p></dd></dl>

<dl>
<dt id="index-delete_002dassociation_002dprocedure">procedure: <strong>delete-association-procedure</strong> <em>deletor predicate selector</em></dt>
<dd><span id="index-list_002ddeletor-2"></span>
<span id="index-list_002ddeletor_0021-2"></span>
<p>This returns a deletion procedure similar to <code>del-assv</code> or
<code>del-assq!</code>.  The <var>predicate</var> and <var>selector</var> arguments are
the same as those for <code>association-procedure</code>, while the
<var>deletor</var> argument should be either the procedure
<code>list-deletor</code> (for non-destructive deletions), or the procedure
<code>list-deletor!</code> (for destructive deletions).
</p>
<p>For example, here is a possible implementation of <code>del-assv</code>:
</p>
<div class="example">
<pre class="example">(define del-assv 
  (delete-association-procedure list-deletor eqv? car))
</pre></div>
</dd></dl>

<dl>
<dt id="index-alist_002dcopy">procedure: <strong>alist-copy</strong> <em>alist</em></dt>
<dd><span id="index-copying_002c-of-alist"></span>
<span id="index-list_002dcopy-1"></span>
<p>Returns a newly allocated copy of <var>alist</var>.  This is similar to
<code>list-copy</code> except that the &ldquo;association&rdquo; pairs, i.e. the
elements of the list <var>alist</var>, are also copied.  <code>alist-copy</code>
could have been implemented like this:
</p>
<div class="example">
<pre class="example">(define (alist-copy alist)
  (if (null? alist)
      '()
      (cons (cons (car (car alist)) (cdr (car alist)))
            (alist-copy (cdr alist)))))
</pre></div>
</dd></dl>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT11" href="#DOCF11">(11)</a></h3>
<p>This introduction is taken
from <cite>Common Lisp, The Language</cite>, second edition, p. 431.</p>
<h5><a id="FOOT12" href="#DOCF12">(12)</a></h3>
<p>Although they are often used as predicates,
<code>assq</code>, <code>assv</code>, and <code>assoc</code> do not have question marks in
their names because they return useful values rather than just <code>#t</code>
or <code>#f</code>.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="1D-Tables.html" accesskey="n" rel="next">1D Tables</a>, Previous: <a href="Associations.html" accesskey="p" rel="prev">Associations</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
