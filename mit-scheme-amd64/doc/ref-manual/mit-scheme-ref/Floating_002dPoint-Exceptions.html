<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Floating-Point Exceptions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Floating-Point Exceptions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Floating-Point Exceptions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Fixnum-and-Flonum-Operations.html" rel="up" title="Fixnum and Flonum Operations">
<link href="Floating_002dPoint-Rounding-Mode.html" rel="next" title="Floating-Point Rounding Mode">
<link href="Floating_002dPoint-Environment.html" rel="prev" title="Floating-Point Environment">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Floating_002dPoint-Exceptions"></span><div class="header">
<p>
Next: <a href="Floating_002dPoint-Rounding-Mode.html" accesskey="n" rel="next">Floating-Point Rounding Mode</a>, Previous: <a href="Floating_002dPoint-Environment.html" accesskey="p" rel="prev">Floating-Point Environment</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Floating_002dPoint-Exceptions-1"></span><h4 class="subsection">4.7.4 Floating-Point Exceptions</h4>

<p>In <acronym>IEEE 754-2008</acronym>, floating-point operations such as
arithmetic may raise exceptions.
This sets a flag in the floating-point environment that is maintained
until it is cleared.
Many machines can also be configured to trap on exceptions, which in
Scheme leads to signalling a condition.
(Not all CPUs support trapping exceptions &mdash; for example, most ARMv8
CPUs do not.)
In the default environment, no exceptions are trapped.
</p>
<p>Floating-point exceptions and sets of floating-point exceptions are
represented by small integers, whose interpretation is
machine-dependent &mdash; for example, the invalid-operation exception
may be represented differently on PowerPC and AMD x86-64 CPUs.
The number for a floating-point exception is the same as the number
for a set of exceptions containing only that one; the bitwise-AND of
two sets is their intersection, the bitwise-IOR is their union, etc.
The procedures <code>flo:exceptions-&gt;names</code> and
<code>flo:names-&gt;exceptions</code> convert between machine-dependent integer
representations and machine-independent lists of human-readable
symbols.
</p>
<p>The following exceptions are recognized by MIT/GNU Scheme:
</p>
<dl compact="compact">
<dt>inexact-result</dt>
<dd><span id="index-inexact_002dresult-exception"></span>
<p>Raised when the result of a floating-point computation is not a
floating-point number and therefore must be rounded.
</p>
<p>The inexact-result exception is never trappable in MIT/GNU Scheme.
</p>
</dd>
<dt>underflow</dt>
<dd><span id="index-underflow-exception"></span>
<p>Raised when the result of a floating-point computation is too small in
magnitude to be represented by a normal floating-point number, and is
therefore rounded to a subnormal or zero.
</p>
</dd>
<dt>overflow</dt>
<dd><span id="index-overflow-exception"></span>
<p>Raised when the result of a floating-point computation is too large in
magnitude to be represented by a floating-point number, and is
therefore rounded to infinity.
</p>
</dd>
<dt>divide-by-zero</dt>
<dd><span id="index-divide_002dby_002dzero-exception"></span>
<p>Raised on division of a nonzero finite real number by a zero real
number, or logarithm of zero, or other operation that has an unbounded
limit at a point like division by a divisor approaching zero.
</p>
</dd>
<dt>invalid-operation</dt>
<dd><span id="index-invalid_002doperation-exception-1"></span>
<span id="index-signalling-NaN-1"></span>
<span id="index-quiet-NaN-1"></span>
<p>Raised when the input to a floating-point computation is nonsensical,
such as division of zero by zero, or real logarithm of a negative
number.
The result of an invalid-operation is a NaN.
Also raised when the input to a floating-point operation is a
signalling NaN, but not for a quiet NaN.
</p>
</dd>
<dt>subnormal-operand</dt>
<dd><span id="index-subnormal_002doperand-exception"></span>
<p>Raised when an operand in a floating-point operation is subnormal.
</p>
<p>(This is not a standard <acronym>IEEE 754-2008</acronym> exception.
It is supported by Intel CPUs.)
</p></dd>
</dl>

<dl>
<dt id="index-flo_003asupported_002dexceptions">procedure: <strong>flo:supported-exceptions</strong></dt>
<dd><p>Returns the set of exceptions that are supported on the current
machine.
</p></dd></dl>

<dl>
<dt id="index-flo_003aexception_003adivide_002dby_002dzero">procedure: <strong>flo:exception:divide-by-zero</strong></dt>
<dt id="index-flo_003aexception_003ainexact_002dresult">procedure: <strong>flo:exception:inexact-result</strong></dt>
<dt id="index-flo_003aexception_003ainvalid_002doperation">procedure: <strong>flo:exception:invalid-operation</strong></dt>
<dt id="index-flo_003aexception_003aoverflow">procedure: <strong>flo:exception:overflow</strong></dt>
<dt id="index-flo_003aexception_003asubnormal_002doperand">procedure: <strong>flo:exception:subnormal-operand</strong></dt>
<dt id="index-flo_003aexception_003aunderflow">procedure: <strong>flo:exception:underflow</strong></dt>
<dd><p>Returns the specified floating-point exception number.
On machines that do not support a particular exception, the
corresponding procedure simply returns <code>0</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003aexceptions_002d_003enames">procedure: <strong>flo:exceptions-&gt;names</strong> <em>excepts</em></dt>
<dt id="index-flo_003anames_002d_003eexceptions">procedure: <strong>flo:names-&gt;exceptions</strong> <em>list</em></dt>
<dd><p>These procedures convert between a machine-dependent small integer
representation of a set of exceptions, and a representation of a set
of exceptions by a list of human-readable symbols naming them.
</p>
<div class="example">
<pre class="example">(flo:preserving-environment
 (lambda ()
   (flo:clear-exceptions! (flo:supported-exceptions))
   (flo:/ (identity-procedure 1.) 0.)
   (flo:exceptions-&gt;names
    (flo:test-exceptions (flo:supported-exceptions)))))
                               &rArr; (divide-by-zero)
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003atest_002dexceptions">procedure: <strong>flo:test-exceptions</strong> <em>excepts</em></dt>
<dd><p>Returns the set of exceptions in <var>excepts</var> that are currently
raised.
</p>
<p>In the default environment, the result is indeterminate, and may
be affected by floating-point operations in other threads.
</p></dd></dl>

<dl>
<dt id="index-flo_003aclear_002dexceptions_0021">procedure: <strong>flo:clear-exceptions!</strong> <em>excepts</em></dt>
<dt id="index-flo_003araise_002dexceptions_0021">procedure: <strong>flo:raise-exceptions!</strong> <em>excepts</em></dt>
<dd><p>Clears or raises the exceptions in <var>excepts</var>, entering a
per-thread environment.
Other exceptions are unaffected.
</p></dd></dl>

<dl>
<dt id="index-flo_003asave_002dexception_002dflags">procedure: <strong>flo:save-exception-flags</strong></dt>
<dt id="index-flo_003arestore_002dexception_002dflags_0021">procedure: <strong>flo:restore-exception-flags!</strong> <em>exceptflags</em></dt>
<dt id="index-flo_003atest_002dexception_002dflags">procedure: <strong>flo:test-exception-flags</strong> <em>exceptflags excepts</em></dt>
<dd><p><code>Flo:save-exception-flags</code> returns a machine-dependent
representation of the currently trapped and raised exceptions.
<code>Flo:restore-exception-flags!</code> restores it, entering a per-thread
environment.
<code>Flo:test-exception-flags</code> returns the set of exceptions in
<var>excepts</var> that are raised in <var>exceptflags</var>.
</p>
<p><var>Exceptflags</var> is <em>not</em> the same as a set of exceptions.
It is opaque and machine-dependent and should not be used except with
<code>flo:restore-exception-flags!</code> and
<code>flo:test-exception-flags</code>.
</p>
<p><strong>Bug:</strong> <code>Flo:test-exception-flags</code> is unimplemented.
</p></dd></dl>

<dl>
<dt id="index-flo_003ahave_002dtrap_002denable_002fdisable_003f">procedure: <strong>flo:have-trap-enable/disable?</strong></dt>
<dd><p>Returns true if trapping floating-point exceptions is supported on
this machine.
</p></dd></dl>

<dl>
<dt id="index-flo_003adefault_002dtrapped_002dexceptions">procedure: <strong>flo:default-trapped-exceptions</strong></dt>
<dd><p>Returns the set of exceptions that are trapped in the default
floating-point environment.
Equivalent to <code>(flo:names-&gt;exceptions '())</code>, or simply <code>0</code>,
since by default, no exceptions are trapped.
</p></dd></dl>

<dl>
<dt id="index-flo_003atrapped_002dexceptions">procedure: <strong>flo:trapped-exceptions</strong></dt>
<dd><p>Returns the set of exceptions that are currently trapped.
</p></dd></dl>

<dl>
<dt id="index-flo_003atrap_002dexceptions_0021">procedure: <strong>flo:trap-exceptions!</strong> <em>excepts</em></dt>
<dt id="index-flo_003auntrap_002dexceptions_0021">procedure: <strong>flo:untrap-exceptions!</strong> <em>excepts</em></dt>
<dt id="index-flo_003aset_002dtrapped_002dexceptions_0021">procedure: <strong>flo:set-trapped-exceptions!</strong> <em>excepts</em></dt>
<dd><p><code>Flo:trap-exceptions!</code> requests that any exceptions in the set
<var>excepts</var> be trapped, in addition to all of the ones that are
currently trapped.
<code>Flo:untrap-exceptions!</code> requests that any exceptions in the set
<var>excepts</var> not be trapped.
<code>Flo:set-trapped-exceptions!</code> replaces the set of trapped
exceptions altogether by <var>excepts</var>.
All three procedures enter a per-thread environment.
</p>
<div class="example">
<pre class="example">(define (flo:trap-exceptions! excepts)
  (flo:set-trapped-exceptions!
   (fix:or (flo:trapped-exceptions) excepts)))

(define (flo:untrap-exceptions! excepts)
  (flo:set-trapped-exceptions!
   (fix:andc (flo:trapped-exceptions) excepts)))

(define (flo:set-trapped-exceptions! excepts)
  (flo:trap-exceptions! excepts)
  (flo:untrap-exceptions!
   (fix:andc (flo:supported-exceptions) excepts)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003awith_002dexceptions_002dtrapped">procedure: <strong>flo:with-exceptions-trapped</strong> <em>excepts thunk</em></dt>
<dt id="index-flo_003awith_002dexceptions_002duntrapped">procedure: <strong>flo:with-exceptions-untrapped</strong> <em>excepts thunk</em></dt>
<dt id="index-flo_003awith_002dtrapped_002dexceptions">procedure: <strong>flo:with-trapped-exceptions</strong> <em>excepts thunk</em></dt>
<dd><p>Dynamic-extent analogues of <code>flo:trap-exceptions!</code>,
<code>flo:untrap-exceptions!</code>, and <code>flo:set-trapped-exceptions!</code>.
These call <var>thunk</var> with their respective changes to the set of
trapped exceptions in a per-thread environment, and restore the
environment on return or non-local exit.
</p></dd></dl>

<dl>
<dt id="index-flo_003adefer_002dexception_002dtraps_0021">procedure: <strong>flo:defer-exception-traps!</strong></dt>
<dd><p>Saves the current floating-point environment, clears all raised
exceptions, disables all exception traps, and returns a descriptor for
the saved floating-point environment.
</p>
<p><code>Flo:defer-exception-traps!</code> is typically used together with
<code>flo:update-environment!</code>, to trap any exceptions that the caller
had wanted trapped only after a long intermediate computation.
This pattern is captured in <code>flo:deferring-exception-traps</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003adeferring_002dexception_002dtraps">procedure: <strong>flo:deferring-exception-traps</strong> <em>thunk</em></dt>
<dd><p>Calls <var>thunk</var>, but defers trapping on any exceptions it raises
until it returns.
Equivalent to:
</p>
<div class="example">
<pre class="example">(flo:preserving-environment
 (lambda ()
   (let ((environment (flo:defer-exception-traps!)))
     (begin0 (<var>thunk</var>)
       (flo:update-environment! environment)))))
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003aignoring_002dexception_002dtraps">procedure: <strong>flo:ignoring-exception-traps</strong> <em>thunk</em></dt>
<dd><p>Calls <var>thunk</var> with all exceptions untrapped and unraised.
Equivalent to:
</p>
<div class="example">
<pre class="example">(flo:preserving-environment
 (lambda ()
   (flo:defer-exception-traps!)
   (<var>thunk</var>)))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Floating_002dPoint-Rounding-Mode.html" accesskey="n" rel="next">Floating-Point Rounding Mode</a>, Previous: <a href="Floating_002dPoint-Environment.html" accesskey="p" rel="prev">Floating-Point Environment</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
