<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>receive (SRFI 8) (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="receive (SRFI 8) (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="receive (SRFI 8) (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="SRFI-syntax.html" rel="up" title="SRFI syntax">
<link href="and_002dlet_002a-_0028SRFI-2_0029.html" rel="next" title="and-let* (SRFI 2)">
<link href="cond_002dexpand-_0028SRFI-0_0029.html" rel="prev" title="cond-expand (SRFI 0)">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="receive-_0028SRFI-8_0029"></span><div class="header">
<p>
Next: <a href="and_002dlet_002a-_0028SRFI-2_0029.html" accesskey="n" rel="next">and-let* (SRFI 2)</a>, Previous: <a href="cond_002dexpand-_0028SRFI-0_0029.html" accesskey="p" rel="prev">cond-expand (SRFI 0)</a>, Up: <a href="SRFI-syntax.html" accesskey="u" rel="up">SRFI syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="receive-_0028SRFI-8_0029-1"></span><h4 class="subsection">2.12.2 receive (SRFI 8)</h4>

<span id="index-SRFI-8"></span>
<p><a href="https://srfi.schemers.org/srfi-8/srfi-8.html"><acronym>SRFI</acronym> 8</a> defines a convenient syntax to bind an identifier to each of
the values of a multiple-valued expression and then evaluate an
expression in the scope of the bindings.  As an instance of this
pattern, consider the following excerpt from a &lsquo;<samp>quicksort</samp>&rsquo;
procedure:
</p>
<div class="example">
<pre class="example">(call-with-values
  (lambda ()
    (partition (precedes pivot) others))
  (lambda (fore aft)
    (append (qsort fore) (cons pivot (qsort aft)))))
</pre></div>

<p>Here &lsquo;<samp>partition</samp>&rsquo; is a multiple-valued procedure that takes two
arguments, a predicate and a list, and returns two lists, one comprising
the list elements that satisfy the predicate, the other those that do
not.  The purpose of the expression shown is to partition the list
&lsquo;<samp>others</samp>&rsquo;, sort each of the sublists, and recombine the results into
a sorted list.
</p>
<p>For our purposes, the important step is the binding of the identifiers
&lsquo;<samp>fore</samp>&rsquo; and &lsquo;<samp>aft</samp>&rsquo; to the values returned by &lsquo;<samp>partition</samp>&rsquo;.
Expressing the construction and use of these bindings with the
call-by-values primitive is cumbersome: One must explicitly embed the
expression that provides the values for the bindings in a parameterless
procedure, and one must explicitly embed the expression to be evaluated
in the scope of those bindings in another procedure, writing as its
parameters the identifiers that are to be bound to the values received.
</p>
<p>These embeddings are boilerplate, exposing the underlying binding
mechanism but not revealing anything relevant to the particular program
in which it occurs.  So the use of a syntactic abstraction that exposes
only the interesting parts &ndash; the identifiers to be bound, the
multiple-valued expression that supplies the values, and the body of the
receiving procedure &ndash; makes the code more concise and more readable:
</p>
<div class="example">
<pre class="example">(receive (fore aft) (partition (precedes pivot) others)
  (append (qsort fore) (cons pivot (qsort aft))))
</pre></div>

<p>The advantages are similar to those of a &lsquo;<samp>let</samp>&rsquo; expression over a
procedure call with a &lsquo;<samp>lambda</samp>&rsquo; expression as its operator.  In both
cases, cleanly separating a &ldquo;header&rdquo; in which the bindings are
established from a &ldquo;body&rdquo; in which they are used makes it easier to
follow the code.
</p>
<dl>
<dt id="index-receive">special form: <strong>receive</strong> <em>formals expression body</em></dt>
<dd><p><var>Formals</var> and <var>body</var> are defined as for &lsquo;<samp>lambda</samp>&rsquo;
(see <a href="Lambda-Expressions.html">Lambda Expressions</a>).  Specifically, <var>formals</var> can have the
following forms (the use of &lsquo;<samp>#!optional</samp>&rsquo; and &lsquo;<samp>#!rest</samp>&rsquo; is also
allowed in <var>formals</var> but is omitted for brevity):
</p>
<dl compact="compact">
<dt>&lsquo;<samp>(<var>ident1</var> &hellip; <var>identN</var>)</samp>&rsquo;</dt>
<dd><p>The environment in which the &lsquo;<samp>receive</samp>&rsquo; expression is evaluated is
extended by binding <var>ident1</var>, &hellip;, <var>identN</var> to fresh
locations.  The <var>expression</var> is evaluated, and its values are stored
into those locations.  (It is an error if <var>expression</var> does not have
exactly <var>N</var> values.)
</p>
</dd>
<dt>&lsquo;<samp><var>ident</var></samp>&rsquo;</dt>
<dd><p>The environment in which the &lsquo;<samp>receive</samp>&rsquo; expression is evaluated is
extended by binding <var>ident</var> to a fresh location.  The
<var>expression</var> is evaluated, its values are converted into a newly
allocated list, and the list is stored in the location bound to
<var>ident</var>.
</p>
</dd>
<dt>&lsquo;<samp>(<var>ident1</var> &hellip; <var>identN</var> . <var>identN+1</var>)</samp>&rsquo;</dt>
<dd><p>The environment in which the &lsquo;<samp>receive</samp>&rsquo; expression is evaluated is
extended by binding <var>ident1</var>, &hellip;, <var>identN+1</var> to fresh
locations.  The <var>expression</var> is evaluated.  Its first <var>N</var> values
are stored into the locations bound to <var>ident1</var> &hellip; <var>identN</var>.
Any remaining values are converted into a newly allocated list, which is
stored into the location bound to <var>identN+1</var>.  (It is an error if
<var>expression</var> does not have at least <var>N</var> values.)
</p></dd>
</dl>

<p>In any case, the expressions in <var>body</var> are evaluated sequentially in
the extended environment.  The results of the last expression in the
body are the values of the &lsquo;<samp>receive</samp>&rsquo; expression.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="and_002dlet_002a-_0028SRFI-2_0029.html" accesskey="n" rel="next">and-let* (SRFI 2)</a>, Previous: <a href="cond_002dexpand-_0028SRFI-0_0029.html" accesskey="p" rel="prev">cond-expand (SRFI 0)</a>, Up: <a href="SRFI-syntax.html" accesskey="u" rel="up">SRFI syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
