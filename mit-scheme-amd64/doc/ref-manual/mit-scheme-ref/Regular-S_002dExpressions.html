<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Regular S-Expressions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Regular S-Expressions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Regular S-Expressions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Regular-Expressions.html" rel="up" title="Regular Expressions">
<link href="Regsexp-Procedures.html" rel="next" title="Regsexp Procedures">
<link href="Regular-Expressions.html" rel="prev" title="Regular Expressions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Regular-S_002dExpressions"></span><div class="header">
<p>
Next: <a href="Regsexp-Procedures.html" accesskey="n" rel="next">Regsexp Procedures</a>, Previous: <a href="Regular-Expressions.html" accesskey="p" rel="prev">Regular Expressions</a>, Up: <a href="Regular-Expressions.html" accesskey="u" rel="up">Regular Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Regular-S_002dExpressions-1"></span><h4 class="subsection">6.2.1 Regular S-Expressions</h4>

<p>A regular s-expression is either a character or a string, which
matches itself, or one of the following forms.
</p>
<p>Examples in this section use the following definitions for brevity:
</p>
<div class="example">
<pre class="example">(define (try-match pattern string)
  (regsexp-match-string (compile-regsexp pattern) string))

(define (try-search pattern string)
  (regsexp-search-string-forward (compile-regsexp pattern) string))
</pre></div>

<p>These forms match one or more characters literally:
</p>
<dl>
<dt id="index-char_002dci">regsexp: <strong>char-ci</strong> <em>char</em></dt>
<dd><p>Matches <var>char</var> without considering case.
</p></dd></dl>

<dl>
<dt id="index-string_002dci">regsexp: <strong>string-ci</strong> <em>string</em></dt>
<dd><p>Matches <var>string</var> without considering case.
</p></dd></dl>

<dl>
<dt id="index-any_002dchar">regsexp: <strong>any-char</strong></dt>
<dd><p>Matches one character other than <code>#\newline</code>.
</p>
<div class="example">
<pre class="example">(try-match '(any-char) &quot;&quot;) &rArr; #f
(try-match '(any-char) &quot;a&quot;) &rArr; (0 1)
(try-match '(any-char) &quot;\n&quot;) &rArr; #f
(try-search '(any-char) &quot;&quot;) &rArr; #f
(try-search '(any-char) &quot;ab&quot;) &rArr; (0 1)
(try-search '(any-char) &quot;\na&quot;) &rArr; (1 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_002din">regsexp: <strong>char-in</strong> <em>datum &hellip;</em></dt>
<dt id="index-char_002dnot_002din">regsexp: <strong>char-not-in</strong> <em>datum &hellip;</em></dt>
<dd><p>Matches one character in (not in) the character set specified by
<code>(char-set <var>datum &hellip;</var>)</code>.
</p>
<div class="example">
<pre class="example">(try-match '(seq &quot;a&quot; (char-in &quot;ab&quot;) &quot;c&quot;) &quot;abc&quot;) &rArr; (0 3)
(try-match '(seq &quot;a&quot; (char-not-in &quot;ab&quot;) &quot;c&quot;) &quot;abc&quot;) &rArr; #f
(try-match '(seq &quot;a&quot; (char-not-in &quot;ab&quot;) &quot;c&quot;) &quot;adc&quot;) &rArr; (0 3)
(try-match '(seq &quot;a&quot; (+ (char-in numeric)) &quot;c&quot;) &quot;a019c&quot;) &rArr; (0 5)
</pre></div>
</dd></dl>

<p>These forms match no characters, but only at specific locations in the
input string:
</p>
<dl>
<dt id="index-line_002dstart">regsexp: <strong>line-start</strong></dt>
<dt id="index-line_002dend">regsexp: <strong>line-end</strong></dt>
<dd><p>Matches no characters at the start (end) of a line.
</p>
<div class="example">
<pre class="example">(try-match '(seq (line-start)
                 (* (any-char))
                 (line-end))
           &quot;abc&quot;) &rArr; (0 3)
</pre><pre class="example">(try-match '(seq (line-start)
                 (* (any-char))
                 (line-end))
           &quot;ab\nc&quot;) &rArr; (0 2)
</pre><pre class="example">(try-search '(seq (line-start)
                  (* (char-in alphabetic))
                  (line-end))
            &quot;1abc&quot;) &rArr; #f
</pre><pre class="example">(try-search '(seq (line-start)
                  (* (char-in alphabetic))
                  (line-end))
            &quot;1\nabc&quot;) &rArr; (2 5)
</pre></div>
</dd></dl>

<dl>
<dt id="index-string_002dstart">regsexp: <strong>string-start</strong></dt>
<dt id="index-string_002dend">regsexp: <strong>string-end</strong></dt>
<dd><p>Matches no characters at the start (end) of the string.
</p>
<div class="example">
<pre class="example">(try-match '(seq (string-start)
                 (* (any-char))
                 (string-end))
           &quot;abc&quot;) &rArr; (0 3)
</pre><pre class="example">(try-match '(seq (string-start)
                 (* (any-char))
                 (string-end))
           &quot;ab\nc&quot;) &rArr; #f
</pre><pre class="example">(try-search '(seq (string-start)
                  (* (char-in alphabetic))
                  (string-end))
            &quot;1abc&quot;) &rArr; #f
</pre><pre class="example">(try-search '(seq (string-start)
                  (* (char-in alphabetic))
                  (string-end))
            &quot;1\nabc&quot;) &rArr; #f
</pre></div>
</dd></dl>

<p>These forms match repetitions of a given regsexp.  Most of them come
in two forms, one of which is <em>greedy</em> and the other <em>shy</em>.
The greedy form matches as many repetitions as it can, then uses
failure backtracking to reduce the number of repetitions one at a
time.  The shy form matches the minimum number of repetitions, then
uses failure backtracking to increase the number of repetitions one at
a time.  The shy form is similar to the greedy form except that a
<code>?</code> is added at the end of the form&rsquo;s keyword.
</p>
<dl>
<dt id="index-_003f">regsexp: <strong>?</strong> <em>regsexp</em></dt>
<dt id="index-_003f_003f">regsexp: <strong>??</strong> <em>regsexp</em></dt>
<dd><p>Matches <var>regsexp</var> zero or one time.
</p>
<div class="example">
<pre class="example">(try-search '(seq (char-in alphabetic)
                  (? (char-in numeric)))
            &quot;a&quot;) &rArr; (0 1)
</pre><pre class="example">(try-search '(seq (char-in alphabetic)
                  (?? (char-in numeric)))
            &quot;a&quot;) &rArr; (0 1)
</pre><pre class="example">(try-search '(seq (char-in alphabetic)
                  (? (char-in numeric)))
            &quot;a1&quot;) &rArr; (0 2)
</pre><pre class="example">(try-search '(seq (char-in alphabetic)
                  (?? (char-in numeric)))
            &quot;a1&quot;) &rArr; (0 1)
</pre><pre class="example">(try-search '(seq (char-in alphabetic)
                  (? (char-in numeric)))
            &quot;1a2&quot;) &rArr; (1 3)
</pre><pre class="example">(try-search '(seq (char-in alphabetic)
                  (?? (char-in numeric)))
            &quot;1a2&quot;) &rArr; (1 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-_002a-2">regsexp: <strong>*</strong> <em>regsexp</em></dt>
<dt id="index-_002a_003f">regsexp: <strong>*?</strong> <em>regsexp</em></dt>
<dd><p>Matches <var>regsexp</var> zero or more times.
</p>
<div class="example">
<pre class="example">(try-match '(seq (char-in alphabetic)
                 (* (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; (0 2)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (*? (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; (0 2)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (* (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 5)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (*? (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-_002b-3">regsexp: <strong>+</strong> <em>regsexp</em></dt>
<dt id="index-_002b_003f">regsexp: <strong>+?</strong> <em>regsexp</em></dt>
<dd><p>Matches <var>regsexp</var> one or more times.
</p>
<div class="example">
<pre class="example">(try-match '(seq (char-in alphabetic)
                 (+ (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; #f
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (+? (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; #f
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (+ (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 5)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (+? (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 3)
</pre></div>
</dd></dl>

<dl>
<dt id="index-_002a_002a">regsexp: <strong>**</strong> <em>n m regsexp</em></dt>
<dt id="index-_002a_002a_003f">regsexp: <strong>**?</strong> <em>n m regsexp</em></dt>
<dd><p>The <var>n</var> argument must be an exact nonnegative integer.  The
<var>m</var> argument must be either an exact integer greater than or equal
to <var>n</var>, or else <code>#f</code>.
</p>
<p>Matches <var>regsexp</var> at least <var>n</var> times and at most <var>m</var>
times; if <var>m</var> is <code>#f</code> then there is no upper limit.
</p>
<div class="example">
<pre class="example">(try-match '(seq (char-in alphabetic)
                 (** 0 2 (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; (0 2)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (**? 0 2 (char-in numeric))
                 (any-char))
           &quot;aa&quot;) &rArr; (0 2)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (** 0 2 (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 4)
</pre><pre class="example">(try-match '(seq (char-in alphabetic)
                 (**? 0 2 (char-in numeric))
                 (any-char))
           &quot;a123a&quot;) &rArr; (0 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-_002a_002a-1">regsexp: <strong>**</strong> <em>n regsexp</em></dt>
<dd><p>This is an abbreviation for <code>(** <var>n</var> <var>n</var>
<var>regsexp</var>)</code>.  This matcher is neither greedy nor shy since it
matches a fixed number of repetitions.
</p></dd></dl>

<p>These forms implement alternatives and sequencing:
</p>
<dl>
<dt id="index-alt">regsexp: <strong>alt</strong> <em>regsexp &hellip;</em></dt>
<dd><p>Matches one of the <var>regsexp</var> arguments, trying each in order from
left to right.
</p>
<div class="example">
<pre class="example">(try-match '(alt #\a (char-in numeric)) &quot;a&quot;) &rArr; (0 1)
(try-match '(alt #\a (char-in numeric)) &quot;b&quot;) &rArr; #f
(try-match '(alt #\a (char-in numeric)) &quot;1&quot;) &rArr; (0 1)
</pre></div>
</dd></dl>

<dl>
<dt id="index-seq">regsexp: <strong>seq</strong> <em>regsexp &hellip;</em></dt>
<dd><p>Matches the first <var>regsexp</var>, then continues the match with the
next <var>regsexp</var>, and so on until all of the arguments are matched.
</p>
<div class="example">
<pre class="example">(try-match '(seq #\a #\b) &quot;a&quot;) &rArr; #f
(try-match '(seq #\a #\b) &quot;aa&quot;) &rArr; #f
(try-match '(seq #\a #\b) &quot;ab&quot;) &rArr; (0 2)
</pre></div>
</dd></dl>

<p>These forms implement named <em>registers</em>, which store matched
segments of the input string:
</p>
<dl>
<dt id="index-group">regsexp: <strong>group</strong> <em>key regsexp</em></dt>
<dd><p>The <var>key</var> argument must be a fixnum, a character, or a symbol.
</p>
<p>Matches <var>regsexp</var>.  If the match succeeds, the matched segment is
stored in the register named <var>key</var>.
</p>
<div class="example">
<pre class="example">(try-match '(seq (group a (any-char))
                 (group b (any-char))
                 (any-char))
           &quot;radar&quot;) &rArr; (0 3 (a . &quot;r&quot;) (b . &quot;a&quot;))
</pre></div>
</dd></dl>

<dl>
<dt id="index-group_002dref">regsexp: <strong>group-ref</strong> <em>key</em></dt>
<dd><p>The <var>key</var> argument must be a fixnum, a character, or a symbol.
</p>
<p>Matches the characters stored in the register named <var>key</var>.  It is
an error if that register has not been initialized with a
corresponding <code>group</code> expression.
</p>
<div class="example">
<pre class="example">(try-match '(seq (group a (any-char))
                 (group b (any-char))
                 (any-char)
                 (group-ref b)
                 (group-ref a))
           &quot;radar&quot;) &rArr; (0 5 (a . &quot;r&quot;) (b . &quot;a&quot;))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Regsexp-Procedures.html" accesskey="n" rel="next">Regsexp Procedures</a>, Previous: <a href="Regular-Expressions.html" accesskey="p" rel="prev">Regular Expressions</a>, Up: <a href="Regular-Expressions.html" accesskey="u" rel="up">Regular Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
