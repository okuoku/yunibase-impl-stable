<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Random Number Generation (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Random Number Generation (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Random Number Generation (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Numbers.html" rel="up" title="Numbers">
<link href="Characters.html" rel="next" title="Characters">
<link href="Floating_002dPoint-Rounding-Mode.html" rel="prev" title="Floating-Point Rounding Mode">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Random-Number-Generation"></span><div class="header">
<p>
Previous: <a href="Fixnum-and-Flonum-Operations.html" accesskey="p" rel="prev">Fixnum and Flonum Operations</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Random-Number-Generation-1"></span><h3 class="section">4.8 Random Number Generation</h3>
<span id="index-random-number-generation"></span>
<span id="index-pseudorandom-number-generation"></span>
<span id="index-number_002c-pseudorandom-generation"></span>

<p>MIT/GNU Scheme provides a facility for random number generation.
The current implementation uses the ChaCha stream cipher, reseeding
itself at each request so that past outputs cannot be distinguished
from uniform random even if the state of memory is compromised in the
future.
</p>
<p>The interface described here is a mixture of the Common Lisp and SRFI
27 systems.
</p>
<dl>
<dt id="index-random">procedure: <strong>random</strong> <em>m [state]</em></dt>
<dd><p>The argument <var>m</var> must be either an exact positive integer, or an
inexact positive real.
</p>
<ul>
<li> If <var>m</var> is an exact positive integer, then <code>random</code> returns an
exact nonnegative integer below <var>m</var> with uniform distribution.

</li><li> If <var>m</var> is an inexact positive real, then <code>random</code> returns an
inexact real in the interval <em>[0, m)</em> with uniform distribution.
</li></ul>

<p>If <var>state</var> is given and not <code>#f</code>, it must be a random-state
object; otherwise, it defaults to the <code>default-random-source</code>.
This object is used to maintain the state of the pseudorandom number
generator and is altered as a side effect of the <code>random</code>
procedure.
</p>
<p>Use of the default random state requires synchronization between
threads, so it is better for multithreaded programs to use explicit
states.
</p>
<div class="example">
<pre class="example">(random 1.0)    &rArr; .32744744667719056
(random 1.0)    &rArr; .01668326768172354
(random 10)     &rArr; 3
(random 10)     &rArr; 8
(random 100)    &rArr; 38
(random 100)    &rArr; 63
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003arandom_002dunit_002dclosed">procedure: <strong>flo:random-unit-closed</strong> <em>state</em></dt>
<dt id="index-flo_003arandom_002dunit_002dopen">procedure: <strong>flo:random-unit-open</strong> <em>state</em></dt>
<dd><p><var>State</var> must be a random-state object.
<code>Flo:random-unit-closed</code> returns a flonum in the closed interval
<em>[0,1]</em> with uniform distribution.
In practical terms, the result is in the half-closed interval
<em>(0,1]</em> because the probability of returning 0 is
<em>2^{-1075}</em>, far below the standard probability <em>2^{-128}</em>
that means &ldquo;never&rdquo; in cryptographic engineering terms.
</p>
<p><code>Flo:random-unit-open</code> is like <code>flo:random-unit-closed</code>, but
it explicitly rejects <code>0.0</code> and <code>1.0</code> as outputs, so that
the result is a floating-point number in the open interval
<em>(0,1)</em>.
<code>(flo:random-unit-open)</code> is equivalent <code>(random 1.)</code>, except
that it is faster.
</p>
<p>Callers should generally use <code>flo:random-unit-closed</code>, because
for the uniform distribution on the interval <em>[0,1]</em> of real
numbers, the probability of a real number that is rounded to the
floating-point <code>1.0</code> is the small but nonnegligible
<em>2^{-54}</em>, and arithmetic downstream should be prepared to handle
results that are rounded to <code>1.0</code> much more readily than results
that are rounded to <code>0.0</code> &mdash; in other words, a requirement to
use <code>flo:random-unit-open</code> is evidence of bad numerics
downstream.
</p></dd></dl>

<dl>
<dt id="index-flo_003arandom_002dunit">procedure: <strong>flo:random-unit</strong> <em>state</em></dt>
<dd><p><strong>Deprecated</strong> alias for <code>flo:random-unit-open</code>.
</p></dd></dl>

<dl>
<dt id="index-random_002dbytevector">procedure: <strong>random-bytevector</strong> <em>n [state]</em></dt>
<dd><p>Returns a bytevector of <var>n</var> bytes drawn independently uniformly at
random from <var>state</var>.
</p></dd></dl>

<dl>
<dt id="index-random_002dbytevector_0021">procedure: <strong>random-bytevector!</strong> <em>bytevector [start end state]</em></dt>
<dd><p>Replaces the bytes in <var>bytevector</var> from <var>start</var> to <var>end</var>
by bytes drawn independently uniformly at random from <var>state</var>.
</p></dd></dl>

<p>The next three definitions concern random-state objects.  In addition to
these definitions, it is important to know that random-state objects are
specifically designed so that they can be saved to disk using the
<code>fasdump</code> procedure, and later restored using the <code>fasload</code>
procedure.  This allows a particular random-state object to be saved in
order to replay a particular pseudorandom sequence.
</p>
<dl>
<dt id="index-_002arandom_002dstate_002a">variable: <strong>*random-state*</strong></dt>
<dd><p>This variable is <strong>deprecated</strong>; pass an explicit state instead.
</p></dd></dl>

<dl>
<dt id="index-make_002drandom_002dstate">procedure: <strong>make-random-state</strong> <em>[state]</em></dt>
<dd><p>This procedure returns a new random-state object, suitable for use as
as the <var>state</var> argument to <code>random</code>.  If <var>state</var> is not
given or <code>#f</code>, <code>make-random-state</code> returns a <em>copy</em> of
<code>default-random-source</code>.  If <var>state</var> is a random-state
object, a copy of that object is returned.  If <var>state</var> is
<code>#t</code>, then a new random-state object is returned that has been
&ldquo;randomly&rdquo; initialized by some means (such as by a time-of-day
clock).
</p></dd></dl>

<dl>
<dt id="index-random_002dstate_003f">procedure: <strong>random-state?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is a random-state object, otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-export_002drandom_002dstate">procedure: <strong>export-random-state</strong> <em>state</em></dt>
<dt id="index-import_002drandom_002dstate">procedure: <strong>import-random-state</strong> <em>state</em></dt>
<dd><p><code>Export-random-state</code> returns an external representation of a
random state &mdash; an object that can be safely read and written with
<code>read</code> and <code>write</code>, consisting only of nested lists,
vectors, symbols, and small exact integers.
<code>Import-random-state</code> creates a random state from its external
representation.
</p></dd></dl>

<p>In the MIT/GNU Scheme implementation of the SRFI 27 API, a &ldquo;random
source&rdquo; happens to be the same as a random state, but users should
not rely on this.
</p>
<dl>
<dt id="index-make_002drandom_002dsource">procedure: <strong>make-random-source</strong></dt>
<dd><p>[SRFI 27]
Returns a random source.
Every random source created by <code>make-random-source</code> returns the
same sequence of outputs unless modified by
<code>random-source-state-set!</code>, <code>random-source-randomize!</code>, or
<code>random-source-pseudo-randomize!</code>.
</p></dd></dl>

<dl>
<dt id="index-random_002dsource_003f">procedure: <strong>random-source?</strong> <em>object</em></dt>
<dd><p>[SRFI 27]
Returns <code>#t</code> if <var>object</var> is a random source, otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-default_002drandom_002dsource">constant: <strong>default-random-source</strong></dt>
<dd><p>[SRFI 27]
The default random source, used by the various random procedures if no
explicit state is specified and <code>*random-state*</code> is false.
</p></dd></dl>

<dl>
<dt id="index-random_002dsource_002dstate_002dref">procedure: <strong>random-source-state-ref</strong> <em>source</em></dt>
<dt id="index-random_002dsource_002dstate_002dset_0021">procedure: <strong>random-source-state-set!</strong> <em>source exported-state</em></dt>
<dd><p>[SRFI 27]
<code>Random-source-state-ref</code> returns an external representation of a
random source &mdash; an object that can be safely read and written with
<code>read</code> and <code>write</code>, consisting only of nested lists,
vectors, symbols, and small exact integers.
<code>Random-source-state-set!</code> replaces the innards of <var>source</var>
by the source represented by <var>exported-state</var> from
<code>random-source-state-ref</code>.
</p></dd></dl>

<dl>
<dt id="index-random_002dsource_002drandomize_0021">procedure: <strong>random-source-randomize!</strong> <em>source</em></dt>
<dd><p>[SRFI 27]
Loads entropy from the environment into <var>source</var> so that its
subsequent outputs are nondeterministic.
</p>
<p><strong>Warning:</strong> Most implementations of SRFI 27 <em>do not</em> make
subsequent outputs unpredictable with cryptography, so don&rsquo;t rely on
this.
</p></dd></dl>

<dl>
<dt id="index-random_002dsource_002dpseudo_002drandomize_0021">procedure: <strong>random-source-pseudo-randomize!</strong> <em>source i j</em></dt>
<dd><p>[SRFI 27]
The arguments <var>i</var> and <var>j</var> must be exact nonnegative integers
below <em>2^{128}</em>.
This procedure sets <var>source</var> to generate one of <em>2^{256}</em>
distinct possible streams of output, so that if <var>i</var> and <var>j</var>
are chosen uniformly at random, it is hard to distinguish the outputs
of the source from uniform random.
</p>
<p><strong>Warning:</strong> Most implementations of SRFI 27 <em>do not</em> make
it hard to distinguish the outputs of the source from uniform random
even if the indices <var>i</var> and <var>j</var> are uniform random, so don&rsquo;t
rely on this.
</p></dd></dl>

<dl>
<dt id="index-random_002dinteger">procedure: <strong>random-integer</strong> <em>n</em></dt>
<dd><p>[SRFI 27]
Returns an exact nonnegative integer below <var>n</var> chosen uniformly at
random.
</p>
<p>Equivalent to:
</p>
<div class="example">
<pre class="example">((random-source-make-integers default-random-source) n)
</pre></div>
</dd></dl>

<dl>
<dt id="index-random_002dreal">procedure: <strong>random-real</strong></dt>
<dd><p>[SRFI 27]
Returns an inexact real in the open interval <em>(0, 1)</em> with
uniform distribution.
</p>
<p>Equivalent to:
</p>
<div class="example">
<pre class="example">((random-source-make-reals default-random-source))
</pre></div>
</dd></dl>

<dl>
<dt id="index-random_002dsource_002dmake_002dintegers">procedure: <strong>random-source-make-integers</strong> <em>source</em></dt>
<dd><p>[SRFI 27]
Returns a procedure of one argument, <var>n</var>, that deterministically
draws from <var>source</var> a exact nonnegative integer below <var>n</var> with
uniform distribution.
</p></dd></dl>

<dl>
<dt id="index-random_002dsource_002dmake_002dreals">procedure: <strong>random-source-make-reals</strong> <em>source [unit]</em></dt>
<dd><p>[SRFI 27]
Returns a procedure of zero arguments that deterministically draws
from <var>source</var> an inexact real in the interval <em>(0,1)</em> with
uniform distribution.
If <var>unit</var> is specified, the results are instead uniform random
integral multiples of <var>unit</var> in <em>(0,1)</em> and of the same
exactness as <var>unit</var>.
</p></dd></dl>
<hr>
<div class="header">
<p>
Previous: <a href="Fixnum-and-Flonum-Operations.html" accesskey="p" rel="prev">Fixnum and Flonum Operations</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
