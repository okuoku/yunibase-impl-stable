<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Error System (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Error System (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Error System (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Condition-Signalling.html" rel="next" title="Condition Signalling">
<link href="Miscellaneous-OS-Facilities.html" rel="prev" title="Miscellaneous OS Facilities">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Error-System"></span><div class="header">
<p>
Next: <a href="Graphics.html" accesskey="n" rel="next">Graphics</a>, Previous: <a href="Operating_002dSystem-Interface.html" accesskey="p" rel="prev">Operating-System Interface</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Error-System-1"></span><h2 class="chapter">16 Error System</h2>

<span id="index-error-1"></span>
<p>The MIT/GNU Scheme error system provides a uniform mechanism for the
signalling of errors and other exceptional conditions.  The simplest and
most generally useful procedures in the error system are:
</p>
<dl compact="compact">
<dt><code>error</code></dt>
<dd><p>is used to signal simple errors, specifying a message and some irritant
objects (see <a href="Condition-Signalling.html">Condition Signalling</a>).  Errors are usually handled by
stopping the computation and putting the user in an error <small>REPL</small>.
</p>
</dd>
<dt><code>warn</code></dt>
<dd><p>is used to signal warnings (see <a href="Condition-Signalling.html">Condition Signalling</a>).  Warnings are
usually handled by printing a message on the console and continuing the
computation normally.
</p>
</dd>
<dt><code>ignore-errors</code></dt>
<dd><p>is used to suppress the normal handling of errors within a given dynamic
extent (see <a href="Condition-Handling.html">Condition Handling</a>).  Any error that occurs within the
extent is trapped, returning immediately to the caller of
<code>ignore-errors</code>.
</p></dd>
</dl>

<p>More demanding applications require more powerful facilities.  To give a
concrete example, suppose you want floating-point division to return a very
large number whenever the denominator is zero.  This behavior can be
implemented using the error system.
</p>
<p>The Scheme arithmetic system can signal many different kinds of errors,
including floating-point divide by zero.  In our example, we would like to
handle this particular condition specially, allowing the system to handle
other arithmetic errors in its usual way.
</p>
<p>The error system supports this kind of application by providing
mechanisms for distinguishing different types of error conditions and
for specifying where control should be transferred should a given
condition arise.  In this example, there is a specific object that
represents the &ldquo;floating-point divide by zero&rdquo; condition type, and it
is possible to dynamically specify an arbitrary Scheme procedure to be
executed when a condition of that type is signalled.  This procedure
then finds the stack frame containing the call to the division operator,
and returns the appropriate value from that frame.
</p>
<p>Another useful kind of behavior is the ability to specify uniform
handling for related classes of conditions.  For example, it might be
desirable, when opening a file for input, to gracefully handle a variety of
different conditions associated with the file system.  One such condition
might be that the file does not exist, in which case the program will try
some other action, perhaps opening a different file instead.  Another
related condition is that the file exists, but is read protected, so it
cannot be opened for input.  If these or any other related conditions
occur, the program would like to skip this operation and move on to
something else.
</p>
<p>At the same time, errors unrelated to the file system should be treated in
their usual way.  For example, calling <code>car</code> on the argument <code>3</code>
should signal an error.  Or perhaps the name given for the file is
syntactically incorrect, a condition that probably wants to be handled
differently from the case of the file not existing.
</p>
<span id="index-taxonomical-link_002c-of-condition-type-_0028defn_0029"></span>
<span id="index-specialization_002c-of-condition-types-_0028defn_0029"></span>
<span id="index-generalization_002c-of-condition-types-_0028defn_0029"></span>
<p>To facilitate the handling of classes of conditions, the error system
taxonomically organizes all condition types.  The types are related to one
another by <em>taxonomical links</em>, which specify that one type is a &ldquo;kind
of&rdquo; another type.  If two types are linked this way, one is considered to
be a <em>specialization</em> of the other; or vice-versa, the second is a
<em>generalization</em> of the first.  In our example, all of the errors
associated with opening an input file would be specializations of the
condition type &ldquo;cannot open input file&rdquo;.
</p>
<span id="index-condition_002dtype_003asimple_002dcondition-1"></span>
<span id="index-condition_002dtype_003awarning-1"></span>
<span id="index-condition_002dtype_003abreakpoint-1"></span>
<span id="index-condition_002dtype_003aserious_002dcondition-1"></span>
<p>The taxonomy of condition types permits any condition type to have no
more than one immediate generalization.  Thus, the condition types form
a forest (set of trees).  While users can create new trees, the standard
taxonomy (see <a href="Taxonomy.html">Taxonomy</a>) is rooted at
<code>condition-type:serious-condition</code>, <code>condition-type:warning</code>,
<code>condition-type:simple-condition</code>, and
<code>condition-type:breakpoint</code>; users are encouraged to add new
subtypes to these condition types rather than create new trees in the
forest.
</p>
<p>To summarize, the error system provides facilities for the following tasks.
The sections that follow will describe these facilities in more
detail.
</p>
<dl compact="compact">
<dt>Signalling a condition</dt>
<dd><span id="index-signal_002dcondition"></span>
<p>A condition may be signalled in a number of different ways.  Simple
errors may be signalled, without explicitly defining a condition type,
using <code>error</code>.  The <code>signal-condition</code> procedure provides the
most general signalling mechanism.
</p>
</dd>
<dt>Handling a condition</dt>
<dd><span id="index-bind_002dcondition_002dhandler"></span>
<p>The programmer can dynamically specify handlers for particular condition
types or for classes of condition types, by means of the
<code>bind-condition-handler</code> procedure.  Individual handlers have
complete control over the handling of a condition, and additionally may
decide not to handle a particular condition, passing it on to previously
bound handlers.
</p>
</dd>
<dt>Restarting from a handler</dt>
<dd><span id="index-with_002drestart"></span>
<p>The <code>with-restart</code> procedure provides a means for
condition-signalling code to communicate to condition-handling code what
must be done to proceed past the condition.  Handlers can examine the
restarts in effect when a condition was signalled, allowing a structured
way to continue an interrupted computation.
</p>
</dd>
<dt>Packaging condition state</dt>
<dd><p>Each condition is represented by an explicit object.  Condition objects
contain information about the nature of the condition, information that
describes the state of the computation from which the condition arose,
and information about the ways the computation can be restarted.
</p>
</dd>
<dt>Classification of conditions</dt>
<dd><span id="index-condition-type"></span>
<span id="index-type_002c-condition"></span>
<span id="index-specialization_002c-of-condition-types"></span>
<span id="index-generalization_002c-of-condition-types"></span>
<p>Each condition has a type, represented by a condition type object.  Each
condition type may be a specialization of some other condition types.  A
group of types that share a common generalization can be handled
uniformly by specifying a handler for the generalization.
</p></dd>
</dl>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Condition-Signalling.html" accesskey="1">Condition Signalling</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Error-Messages.html" accesskey="2">Error Messages</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Condition-Handling.html" accesskey="3">Condition Handling</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Restarts.html" accesskey="4">Restarts</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Condition-Instances.html" accesskey="5">Condition Instances</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Condition-Types.html" accesskey="6">Condition Types</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Taxonomy.html" accesskey="7">Taxonomy</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Graphics.html" accesskey="n" rel="next">Graphics</a>, Previous: <a href="Operating_002dSystem-Interface.html" accesskey="p" rel="prev">Operating-System Interface</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
