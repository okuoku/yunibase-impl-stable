<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Condition Types (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Condition Types (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Condition Types (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-System.html" rel="up" title="Error System">
<link href="Taxonomy.html" rel="next" title="Taxonomy">
<link href="Simple-Condition-Instance-Operations.html" rel="prev" title="Simple Condition Instance Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Condition-Types"></span><div class="header">
<p>
Next: <a href="Taxonomy.html" accesskey="n" rel="next">Taxonomy</a>, Previous: <a href="Condition-Instances.html" accesskey="p" rel="prev">Condition Instances</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Condition-Types-1"></span><h3 class="section">16.6 Condition Types</h3>

<span id="index-condition-type-1"></span>
<span id="index-type_002c-of-condition"></span>
<p>Each condition has a <em>condition type</em> object associated with it.
These objects are used as a means of focusing on related classes of
conditions, first by concentrating all of the information about a
specific class of condition in a single place, and second by specifying
an inheritance relationship between types.  This inheritance
relationship forms the taxonomic structure of the condition hierarchy
(see <a href="Taxonomy.html">Taxonomy</a>).
</p>
<p>The following procedures consititute the abstraction for condition
types.
</p>
<dl>
<dt id="index-make_002dcondition_002dtype">procedure: <strong>make-condition-type</strong> <em>name generalization field-names reporter</em></dt>
<dd><span id="index-generalization_002c-of-condition-types-2"></span>
<p>Creates and returns a (new) condition type that is a specialization of
<var>generalization</var> (if it is a condition type) or is the root of a new
tree of condition types (if <var>generalization</var> is <code>#f</code>).  For
debugging purposes, the condition type has a <var>name</var>, and instances
of this type contain storage for the fields specified by
<var>field-names</var> (a list of symbols) in addition to the fields common
to all conditions (<var>type</var>, <var>continuation</var> and <var>restarts</var>).
</p>
<p><var>Reporter</var> is used to produce a description of a particular
condition of this type.  It may be a string describing the condition, a
procedure of arity two (the first argument will be a condition of this
type and the second a port) that will <code>write</code> the message to the
given port, or <code>#f</code> to specify that the reporter should be taken
from the condition type <var>generalization</var> (or produce an
&ldquo;undocumented condition of type &hellip;&rdquo; message if <var>generalization</var>
is <code>#f</code>).  The conventions used to form descriptions are spelled
out in <a href="Error-Messages.html">Error Messages</a>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_002ferror_003f">procedure: <strong>condition-type/error?</strong> <em>condition-type</em></dt>
<dd><span id="index-condition_002dtype_003aerror-3"></span>
<span id="index-specialization_002c-of-condition-types-5"></span>
<p>Returns <code>#t</code> if the <var>condition-type</var> is
<code>condition-type:error</code> or a specialization of it, <code>#f</code>
otherwise.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_002ffield_002dnames">procedure: <strong>condition-type/field-names</strong> <em>condition-type</em></dt>
<dd><span id="index-generalization_002c-of-condition-types-3"></span>
<p>Returns a list of all of the field names for a condition of type
<var>condition-type</var>.  This is the set union of the fields specified
when this <var>condition-type</var> was created with the
<code>condition-type/field-names</code> of the generalization of this
<var>condition-type</var>.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_002fgeneralizations">procedure: <strong>condition-type/generalizations</strong> <em>condition-type</em></dt>
<dd><span id="index-generalization_002c-of-condition-types-4"></span>
<p>Returns a list of all of the generalizations of <var>condition-type</var>.
Notice that every condition type is considered a generalization of
itself.
</p></dd></dl>

<dl>
<dt id="index-condition_002dtype_003f">procedure: <strong>condition-type?</strong> <em>object</em></dt>
<dd><p>Returns <code>#f</code> if and only if <var>object</var> is not a condition type.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Taxonomy.html" accesskey="n" rel="next">Taxonomy</a>, Previous: <a href="Condition-Instances.html" accesskey="p" rel="prev">Condition Instances</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
