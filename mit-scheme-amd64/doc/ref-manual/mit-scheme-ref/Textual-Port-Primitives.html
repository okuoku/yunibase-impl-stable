<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Textual Port Primitives (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Textual Port Primitives (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Textual Port Primitives (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Textual-Port-Types.html" rel="next" title="Textual Port Types">
<link href="Prompting.html" rel="prev" title="Prompting">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Textual-Port-Primitives"></span><div class="header">
<p>
Next: <a href="Parser-Buffers.html" accesskey="n" rel="next">Parser Buffers</a>, Previous: <a href="Prompting.html" accesskey="p" rel="prev">Prompting</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Textual-Port-Primitives-1"></span><h3 class="section">14.12 Textual Port Primitives</h3>
<span id="index-textual-port-primitives"></span>

<p>This section describes the low-level operations that can be used to
build and manipulate textual <acronym>I/O</acronym> ports.  The purpose of
these operations is to allow programmers to construct new kinds of
textual <acronym>I/O</acronym> ports.
</p>
<p>The mechanisms described in this section are exclusively for textual
ports; binary ports can&rsquo;t be customized.  In this section, any
reference to a &ldquo;port&rdquo; that isn&rsquo;t modified by &ldquo;textual&rdquo; or
&ldquo;binary&rdquo; is assumed to be a textual port.
</p>
<p>The abstract model of a textual <acronym>I/O</acronym> port, as implemented
here, is a combination of a set of named operations and a state.  The
state is an arbitrary object, the meaning of which is determined by
the operations.  The operations are defined by a mapping from names to
procedures.
</p>
<span id="index-textual-port-type"></span>
<p>The set of named operations is represented by an object called a
<em>textual port type</em>.  A port type is constructed from a set of named
operations, and is subsequently used to construct a port.  The port type
completely specifies the behavior of the port.  Port types also support
a simple form of inheritance, allowing you to create new ports that are
similar to existing ports.
</p>
<p>The port operations are divided into two classes:
</p>
<dl compact="compact">
<dt>Standard operations</dt>
<dd><p>There is a specific set of standard operations for input ports, and a
different set for output ports.  Applications can assume that the
standard input operations are implemented for all input ports, and
likewise the standard output operations are implemented for all output
ports.
<span id="index-standard-operations_002c-on-textual-port"></span>
</p>
</dd>
<dt>Custom operations</dt>
<dd><p>Some ports support additional operations.  For example, ports that
implement output to terminals (or windows) may define an operation named
<code>y-size</code> that returns the height of the terminal in characters.
Because only some ports will implement these operations, programs that
use custom operations must test each port for their existence, and be
prepared to deal with ports that do not implement them.
<span id="index-custom-operations_002c-on-textual-port"></span>
<span id="index-y_002dsize"></span>
</p></dd>
</dl>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Textual-Port-Types.html" accesskey="1">Textual Port Types</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Constructors-and-Accessors-for-Textual-Ports.html" accesskey="2">Constructors and Accessors for Textual Ports</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Textual-Input-Port-Operations.html" accesskey="3">Textual Input Port Operations</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Textual-Output-Port-Operations.html" accesskey="4">Textual Output Port Operations</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Parser-Buffers.html" accesskey="n" rel="next">Parser Buffers</a>, Previous: <a href="Prompting.html" accesskey="p" rel="prev">Prompting</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
