<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Searching Lists (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Searching Lists (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Searching Lists (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Lists.html" rel="up" title="Lists">
<link href="Mapping-of-Lists.html" rel="next" title="Mapping of Lists">
<link href="Filtering-Lists.html" rel="prev" title="Filtering Lists">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Searching-Lists"></span><div class="header">
<p>
Next: <a href="Mapping-of-Lists.html" accesskey="n" rel="next">Mapping of Lists</a>, Previous: <a href="Filtering-Lists.html" accesskey="p" rel="prev">Filtering Lists</a>, Up: <a href="Lists.html" accesskey="u" rel="up">Lists</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Searching-Lists-1"></span><h3 class="section">7.6 Searching Lists</h3>
<span id="index-searching_002c-of-list"></span>

<dl>
<dt id="index-find">SRFI 1 procedure: <strong>find</strong> <em>predicate list</em></dt>
<dd><p>Returns the first element in <var>list</var> for which <var>predicate</var> is
true; returns <code>#f</code> if it doesn&rsquo;t find such an element.
<var>Predicate</var> must be a procedure of one argument.
</p>
<div class="example">
<pre class="example">(find even? '(3 1 4 1 5 9)) =&gt; 4
</pre></div>

<p>Note that <code>find</code> has an ambiguity in its lookup semantics&mdash;if
<code>find</code> returns <code>#f</code>, you cannot tell (in general) if it
found a <code>#f</code> element that satisfied <var>predicate</var>, or if it did
not find any element at all.  In many situations, this ambiguity
cannot arise&mdash;either the list being searched is known not to contain
any <code>#f</code> elements, or the list is guaranteed to have an element
satisfying <var>predicate</var>.  However, in cases where this ambiguity
can arise, you should use <code>find-tail</code> instead of
<code>find</code>&mdash;<code>find-tail</code> has no such ambiguity:
</p>
<div class="example">
<pre class="example">(cond ((find-tail pred lis)
        =&gt; (lambda (pair) &hellip;)) ; Handle (CAR PAIR)
      (else &hellip;)) ; Search failed.
</pre></div>
</dd></dl>


<dl>
<dt id="index-find_002dtail">SRFI 1 procedure: <strong>find-tail</strong> <em>predicate list</em></dt>
<dd><p>Returns the first pair of <var>list</var> whose car satisfies
<var>predicate</var>; returns <code>#f</code> if there&rsquo;s no such pair.
<code>find-tail</code> can be viewed as a general-predicate variant of
<var>memv</var>.
</p></dd></dl>


<dl>
<dt id="index-memq">standard procedure: <strong>memq</strong> <em>object list</em></dt>
<dt id="index-memv">standard procedure: <strong>memv</strong> <em>object list</em></dt>
<dt id="index-member">standard procedure: <strong>member</strong> <em>object list [compare]</em></dt>
<dd><p>These procedures return the first pair of <var>list</var> whose car is
<var>object</var>; the returned pair is always one from which <var>list</var> is
composed.  If <var>object</var> does not occur in <var>list</var>, <code>#f</code>
(n.b.: not the empty list) is returned.  <code>memq</code> uses <code>eq?</code>
to compare <var>object</var> with the elements of <var>list</var>, while
<code>memv</code> uses <code>eqv?</code> and <code>member</code> uses <var>compare</var>, or
<code>equal?</code> if <var>compare</var> is not supplied.<a id="DOCF7" href="#FOOT7"><sup>7</sup></a>
</p>
<div class="example">
<pre class="example">(memq 'a '(a b c))                      &rArr; (a b c)
(memq 'b '(a b c))                      &rArr; (b c)
(memq 'a '(b c d))                      &rArr; #f
(memq (list 'a) '(b (a) c))             &rArr; #f
(member (list 'a) '(b (a) c))           &rArr; ((a) c)
(memq 101 '(100 101 102))               &rArr; <span class="roman">unspecified</span>
(memv 101 '(100 101 102))               &rArr; (101 102)
</pre></div>
</dd></dl>


<dl>
<dt id="index-member_002dprocedure">procedure: <strong>member-procedure</strong> <em>predicate</em></dt>
<dd><p>Returns a procedure similar to <code>memq</code>, except that <var>predicate</var>,
which must be an equivalence predicate, is used instead of <code>eq?</code>.
This could be used to define <code>memv</code> as follows:
</p>
<div class="example">
<pre class="example">(define memv (member-procedure eqv?))
</pre></div>
</dd></dl>


<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT7" href="#DOCF7">(7)</a></h3>
<p>Although they
are often used as predicates, <code>memq</code>, <code>memv</code>, and
<code>member</code> do not have question marks in their names because they
return useful values rather than just <code>#t</code> or <code>#f</code>.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Mapping-of-Lists.html" accesskey="n" rel="next">Mapping of Lists</a>, Previous: <a href="Filtering-Lists.html" accesskey="p" rel="prev">Filtering Lists</a>, Up: <a href="Lists.html" accesskey="u" rel="up">Lists</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
