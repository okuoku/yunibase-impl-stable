<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Fixnum Operations (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Fixnum Operations (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Fixnum Operations (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Fixnum-and-Flonum-Operations.html" rel="up" title="Fixnum and Flonum Operations">
<link href="Flonum-Operations.html" rel="next" title="Flonum Operations">
<link href="Fixnum-and-Flonum-Operations.html" rel="prev" title="Fixnum and Flonum Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Fixnum-Operations"></span><div class="header">
<p>
Next: <a href="Flonum-Operations.html" accesskey="n" rel="next">Flonum Operations</a>, Previous: <a href="Fixnum-and-Flonum-Operations.html" accesskey="p" rel="prev">Fixnum and Flonum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Fixnum-Operations-1"></span><h4 class="subsection">4.7.1 Fixnum Operations</h4>

<span id="index-fixnum-_0028defn_0029"></span>
<p>A <em>fixnum</em> is an exact integer that is small enough to fit in a
machine word.  In MIT/GNU Scheme, fixnums are typically 24 or 26 bits,
depending on the machine; it is reasonable to assume that fixnums are at
least 24 bits.  Fixnums are signed; they are encoded using 2&rsquo;s
complement.
</p>
<p>All exact integers that are small enough to be encoded as fixnums are
always encoded as fixnums &mdash; in other words, any exact integer that is
not a fixnum is too big to be encoded as such.  For this reason, small
constants such as <code>0</code> or <code>1</code> are guaranteed to be fixnums.
</p>
<dl>
<dt id="index-fix_003afixnum_003f">procedure: <strong>fix:fixnum?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-fixnum"></span>
<p>Returns <code>#t</code> if <var>object</var> is a fixnum; otherwise returns
<code>#f</code>.
</p></dd></dl>

<p>Here is an expression that determines the largest fixnum:
</p>
<div class="example">
<pre class="example">(let loop ((n 1))
  (if (fix:fixnum? n)
      (loop (* n 2))
      (- n 1)))
</pre></div>

<p>A similar expression determines the smallest fixnum.
</p>
<dl>
<dt id="index-fix_003a_003d">procedure: <strong>fix:=</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_003c">procedure: <strong>fix:&lt;</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_003e">procedure: <strong>fix:&gt;</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_003c_003d">procedure: <strong>fix:&lt;=</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_003e_003d">procedure: <strong>fix:&gt;=</strong> <em>fixnum fixnum</em></dt>
<dd><span id="index-equivalence-predicate_002c-for-fixnums"></span>
<p>These are the standard order and equality predicates on fixnums.  When
compiled, they do not check the types of their arguments.
</p></dd></dl>

<dl>
<dt id="index-fix_003azero_003f">procedure: <strong>fix:zero?</strong> <em>fixnum</em></dt>
<dt id="index-fix_003apositive_003f">procedure: <strong>fix:positive?</strong> <em>fixnum</em></dt>
<dt id="index-fix_003anegative_003f">procedure: <strong>fix:negative?</strong> <em>fixnum</em></dt>
<dd><p>These procedures compare their argument to zero.  When compiled, they do
not check the type of their argument.  The code produced by the
following expressions is identical:
</p>
<div class="example">
<pre class="example">(fix:zero? <var>fixnum</var>)
(fix:= <var>fixnum</var> 0)
</pre></div>

<p>Similarly, <code>fix:positive?</code> and <code>fix:negative?</code> produce code
identical to equivalent expressions using <code>fix:&gt;</code> and <code>fix:&lt;</code>.
</p></dd></dl>

<dl>
<dt id="index-fix_003a_002b">procedure: <strong>fix:+</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_002d">procedure: <strong>fix:-</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a_002a">procedure: <strong>fix:*</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003aquotient">procedure: <strong>fix:quotient</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003aremainder">procedure: <strong>fix:remainder</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003agcd">procedure: <strong>fix:gcd</strong> <em>fixnum fixnum</em></dt>
<dt id="index-fix_003a1_002b">procedure: <strong>fix:1+</strong> <em>fixnum</em></dt>
<dt id="index-fix_003a_002d1_002b">procedure: <strong>fix:-1+</strong> <em>fixnum</em></dt>
<dd><p>These procedures are the standard arithmetic operations on fixnums.
When compiled, they do not check the types of their arguments.
Furthermore, they do not check to see if the result can be encoded as a
fixnum.  If the result is too large to be encoded as a fixnum, a
malformed object is returned, with potentially disastrous effect on the
garbage collector.
</p></dd></dl>

<dl>
<dt id="index-fix_003adivide">procedure: <strong>fix:divide</strong> <em>fixnum fixnum</em></dt>
<dd><span id="index-integer_002ddivide-1"></span>
<span id="index-integer_002ddivide_002dquotient-1"></span>
<span id="index-integer_002ddivide_002dremainder-1"></span>
<p>This procedure is like <code>integer-divide</code>, except that its arguments
and its results must be fixnums.  It should be used in conjunction with
<code>integer-divide-quotient</code> and <code>integer-divide-remainder</code>.
</p></dd></dl>

<span id="index-logical-operations_002c-on-fixnums"></span>
<span id="index-bitwise_002dlogical-operations_002c-on-fixnums"></span>
<p>The following are <em>bitwise-logical</em> operations on fixnums.
</p>
<dl>
<dt id="index-fix_003anot">procedure: <strong>fix:not</strong> <em>fixnum</em></dt>
<dd><p>This returns the bitwise-logical inverse of its argument.  When
compiled, it does not check the type of its argument.
</p>
<div class="example">
<pre class="example">(fix:not 0)                             &rArr;  -1
(fix:not -1)                            &rArr;  0
(fix:not 1)                             &rArr;  -2
(fix:not -34)                           &rArr;  33
</pre></div>
</dd></dl>

<dl>
<dt id="index-fix_003aand">procedure: <strong>fix:and</strong> <em>fixnum fixnum</em></dt>
<dd><p>This returns the bitwise-logical &ldquo;and&rdquo; of its arguments.  When
compiled, it does not check the types of its arguments.
</p>
<div class="example">
<pre class="example">(fix:and #x43 #x0f)                     &rArr;  3
(fix:and #x43 #xf0)                     &rArr;  #x40
</pre></div>
</dd></dl>

<dl>
<dt id="index-fix_003aandc">procedure: <strong>fix:andc</strong> <em>fixnum fixnum</em></dt>
<dd><p>Returns the bitwise-logical &ldquo;and&rdquo; of the first argument with the
bitwise-logical inverse of the second argument.  When compiled, it does
not check the types of its arguments.
</p>
<div class="example">
<pre class="example">(fix:andc #x43 #x0f)                    &rArr;  #x40
(fix:andc #x43 #xf0)                    &rArr;  3
</pre></div>
</dd></dl>

<dl>
<dt id="index-fix_003aor">procedure: <strong>fix:or</strong> <em>fixnum fixnum</em></dt>
<dd><p>This returns the bitwise-logical &ldquo;inclusive or&rdquo; of its arguments.
When compiled, it does not check the types of its arguments.
</p>
<div class="example">
<pre class="example">(fix:or #x40 3)                         &rArr; #x43
(fix:or #x41 3)                         &rArr; #x43
</pre></div>
</dd></dl>

<dl>
<dt id="index-fix_003axor">procedure: <strong>fix:xor</strong> <em>fixnum fixnum</em></dt>
<dd><p>This returns the bitwise-logical &ldquo;exclusive or&rdquo; of its arguments.
When compiled, it does not check the types of its arguments.
</p>
<div class="example">
<pre class="example">(fix:xor #x40 3)                        &rArr; #x43
(fix:xor #x41 3)                        &rArr; #x42
</pre></div>
</dd></dl>

<dl>
<dt id="index-fix_003alsh">procedure: <strong>fix:lsh</strong> <em>fixnum1 fixnum2</em></dt>
<dd><p>This procedure returns the result of logically shifting <var>fixnum1</var> by
<var>fixnum2</var> bits.  If <var>fixnum2</var> is positive, <var>fixnum1</var> is
shifted left; if negative, it is shifted right.  When compiled, it does
not check the types of its arguments, nor the validity of its result.
</p>
<div class="example">
<pre class="example">(fix:lsh 1 10)                          &rArr;  #x400
(fix:lsh #x432 -10)                     &rArr;  1
(fix:lsh -1 3)                          &rArr;  -8
(fix:lsh -128 -4)                       &rArr;  #x3FFFF8
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Flonum-Operations.html" accesskey="n" rel="next">Flonum Operations</a>, Previous: <a href="Fixnum-and-Flonum-Operations.html" accesskey="p" rel="prev">Fixnum and Flonum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
