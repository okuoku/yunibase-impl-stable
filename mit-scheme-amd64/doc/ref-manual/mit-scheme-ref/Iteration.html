<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Iteration (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Iteration (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Iteration (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Structure-Definitions.html" rel="next" title="Structure Definitions">
<link href="Sequencing.html" rel="prev" title="Sequencing">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Iteration"></span><div class="header">
<p>
Next: <a href="Structure-Definitions.html" accesskey="n" rel="next">Structure Definitions</a>, Previous: <a href="Sequencing.html" accesskey="p" rel="prev">Sequencing</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Iteration-1"></span><h3 class="section">2.9 Iteration</h3>

<span id="index-expression_002c-iteration-_0028defn_0029"></span>
<span id="index-iteration-expression-_0028defn_0029"></span>
<span id="index-looping-_0028see-iteration-expressions_0029"></span>
<span id="index-tail-recursion_002c-vs_002e-iteration-expression"></span>
<p>The <em>iteration expressions</em> are: &ldquo;named <code>let</code>&rdquo; and <code>do</code>.
They are also binding expressions, but are more commonly referred to as
iteration expressions.  Because Scheme is properly tail-recursive, you
don&rsquo;t need to use these special forms to express iteration; you can
simply use appropriately written &ldquo;recursive&rdquo; procedure calls.
</p>
<dl>
<dt id="index-let-6">extended standard special form: <strong>let</strong> <em>name ((<var>variable</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><span id="index-named-let-_0028defn_0029"></span>
<p>MIT/GNU Scheme permits a variant on the syntax of <code>let</code> called
&ldquo;named <code>let</code>&rdquo; which provides a more general looping construct
than <code>do</code>, and may also be used to express recursions.
</p>
<p>Named <code>let</code> has the same syntax and semantics as ordinary
<code>let</code> except that <var>name</var> is bound within the <var>expr</var>s
to a procedure whose formal arguments are the <var>variable</var>s and whose
body is the <var>expr</var>s.  Thus the execution of the
<var>expr</var>s may be repeated by invoking the procedure named by
<var>name</var>.
</p>
<span id="index-unassigned-variable_002c-and-named-let"></span>
<p>MIT/GNU Scheme allows any of the <var>init</var>s to be omitted, in which
case the corresponding <var>variable</var>s are unassigned.
</p>
<p>Note: the following expressions are equivalent:
</p>
<div class="example">
<pre class="example">(let <var>name</var> ((<var>variable</var> <var>init</var>) &hellip;)
  <var>expr</var>
  <var>expr</var> &hellip;)

((letrec ((<var>name</var>
           (named-lambda (<var>name</var> <var>variable</var> &hellip;)
             <var>expr</var>
             <var>expr</var> &hellip;)))
   <var>name</var>)
 <var>init</var> &hellip;)
</pre></div>

<p>Here is an example:
</p>
<div class="example">
<pre class="example">(let loop
     ((numbers '(3 -2 1 6 -5))
      (nonneg '())
      (neg '()))
  (cond ((null? numbers)
         (list nonneg neg))
        ((&gt;= (car numbers) 0)
         (loop (cdr numbers)
               (cons (car numbers) nonneg)
               neg))
        (else
         (loop (cdr numbers)
               nonneg
               (cons (car numbers) neg)))))

     &rArr;  ((6 1 3) (-5 -2))
</pre></div>
</dd></dl>

<dl>
<dt id="index-do-2">extended standard special form: <strong>do</strong> <em>((<var>variable</var> <var>init</var> <var>step</var>) &hellip;) (<var>test</var> <var>expression</var> &hellip;) command &hellip;</em></dt>
<dd><p><code>do</code> is an iteration construct.  It specifies a set of variables to
be bound, how they are to be initialized at the start, and how they are
to be updated on each iteration.  When a termination condition is met,
the loop exits with a specified result value.
</p>
<p><code>do</code> expressions are evaluated as follows: The <var>init</var>
expressions are evaluated (in some unspecified order), the
<var>variable</var>s are bound to fresh locations, the results of the
<var>init</var> expressions are stored in the bindings of the
<var>variable</var>s, and then the iteration phase begins.
</p>
<p>Each iteration begins by evaluating <var>test</var>; if the result is false,
then the <var>command</var> expressions are evaluated in order for effect,
the <var>step</var> expressions are evaluated in some unspecified order, the
<var>variable</var>s are bound to fresh locations, the results of the
<var>step</var>s are stored in the bindings of the <var>variable</var>s, and the
next iteration begins.
</p>
<p>If <var>test</var> evaluates to a true value, then the <var>expression</var>s are
evaluated from left to right and the value of the last <var>expression</var>
is returned as the value of the <code>do</code> expression.  If no
<var>expression</var>s are present, then the value of the <code>do</code>
expression is unspecified in standard Scheme; in MIT/GNU Scheme, the
value of <var>test</var> is returned.
</p>
<span id="index-region-of-variable-binding_002c-do"></span>
<span id="index-variable-binding_002c-do"></span>
<p>The region of the binding of a <var>variable</var> consists of the entire
<code>do</code> expression except for the <var>init</var>s.  It is an error for a
<var>variable</var> to appear more than once in the list of <code>do</code>
variables.
</p>
<p>A <var>step</var> may be omitted, in which case the effect is the same as if
<code>(<var>variable</var> <var>init</var> <var>variable</var>)</code> had been written
instead of <code>(<var>variable</var> <var>init</var>)</code>.
</p>
<div class="example">
<pre class="example">(do ((vec (make-vector 5))
      (i 0 (+ i 1)))
    ((= i 5) vec)
   (vector-set! vec i i))               &rArr;  #(0 1 2 3 4)
</pre><pre class="example">

</pre><pre class="example">(let ((x '(1 3 5 7 9)))
   (do ((x x (cdr x))
        (sum 0 (+ sum (car x))))
       ((null? x) sum)))                &rArr;  25
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Structure-Definitions.html" accesskey="n" rel="next">Structure Definitions</a>, Previous: <a href="Sequencing.html" accesskey="p" rel="prev">Sequencing</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
