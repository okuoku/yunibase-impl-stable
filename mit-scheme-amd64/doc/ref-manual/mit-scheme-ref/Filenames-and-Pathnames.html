<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Filenames and Pathnames (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Filenames and Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Filenames and Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pathnames.html" rel="up" title="Pathnames">
<link href="Components-of-Pathnames.html" rel="next" title="Components of Pathnames">
<link href="Pathnames.html" rel="prev" title="Pathnames">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Filenames-and-Pathnames"></span><div class="header">
<p>
Next: <a href="Components-of-Pathnames.html" accesskey="n" rel="next">Components of Pathnames</a>, Previous: <a href="Pathnames.html" accesskey="p" rel="prev">Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Filenames-and-Pathnames-1"></span><h4 class="subsection">15.1.1 Filenames and Pathnames</h4>

<p>Pathname objects are usually created by parsing filenames (character
strings) into component parts.  MIT/GNU Scheme provides operations that
convert filenames into pathnames and vice versa.
</p>
<dl>
<dt id="index-_002d_003epathname">procedure: <strong>-&gt;pathname</strong> <em>object</em></dt>
<dd><span id="index-construction_002c-of-pathname"></span>
<p>Returns a pathname that is the equivalent of <var>object</var>.  <var>Object</var>
must be a pathname or a string.  If <var>object</var> is a pathname, it is
returned.  If <var>object</var> is a string, this procedure returns the
pathname that corresponds to the string; in this case it is equivalent
to <code>(parse-namestring <var>object</var> #f #f)</code>.
</p>
<div class="example">
<pre class="example">(-&gt;pathname &quot;foo&quot;)          &rArr;  #[pathname 65 &quot;foo&quot;]
(-&gt;pathname &quot;/usr/morris&quot;)  &rArr;  #[pathname 66 &quot;/usr/morris&quot;]
</pre></div>
</dd></dl>


<dl>
<dt id="index-parse_002dnamestring">procedure: <strong>parse-namestring</strong> <em>thing [host [defaults]]</em></dt>
<dd><span id="index-construction_002c-of-pathname-1"></span>
<p>This turns <var>thing</var> into a pathname.
<var>Thing</var> must be a pathname or a string.
If <var>thing</var> is a pathname, it is returned.  If <var>thing</var> is a
string, this procedure returns the pathname that corresponds to the
string, parsed according to the syntax of the file system specified by
<var>host</var>.
</p>
<p>This procedure <em>does not</em> do defaulting of pathname components.
</p>
<p>The optional arguments are used to determine what syntax should be used
for parsing the string.  In general this is only really useful if your
implementation of MIT/GNU Scheme supports more than one file system,
otherwise you would use <code>-&gt;pathname</code>.  If given, <var>host</var> must be
a host object or <code>#f</code>, and <var>defaults</var> must be a pathname.
<var>Host</var> specifies the syntax used to parse the string.  If <var>host</var>
is not given or <code>#f</code>, the host component from <var>defaults</var> is
used instead; if <var>defaults</var> is not given, the host component from
<code>param:default-pathname-defaults</code> is used.
</p></dd></dl>

<dl>
<dt id="index-_002d_003enamestring">procedure: <strong>-&gt;namestring</strong> <em>pathname</em></dt>
<dd><span id="index-conversion_002c-pathname-to-string"></span>
<p><code>-&gt;namestring</code> returns a newly allocated string that is the
filename corresponding to <var>pathname</var>.
</p><div class="example">
<pre class="example">(-&gt;namestring (-&gt;pathname &quot;/usr/morris/minor.van&quot;))
     &rArr;  &quot;/usr/morris/minor.van&quot;
</pre></div>
</dd></dl>


<dl>
<dt id="index-pathname_002dsimplify">procedure: <strong>pathname-simplify</strong> <em>pathname</em></dt>
<dd><span id="index-simplification_002c-of-pathname"></span>
<p>Returns a pathname that locates the same file or directory as
<var>pathname</var>, but is in some sense simpler.  Note that
<code>pathname-simplify</code> might not always be able to simplify the
pathname, e.g. on unix with symbolic links the directory
<samp>/usr/morris/../</samp> need not be the same as <samp>/usr/</samp>.  In cases
of uncertainty the behavior is conservative, returning the original or a
partly simplified pathname.
</p>
<div class="example">
<pre class="example">(pathname-simplify &quot;/usr/morris/../morris/dance&quot;)
     &rArr;  #[pathname &quot;/usr/morris/dance&quot;]
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Components-of-Pathnames.html" accesskey="n" rel="next">Components of Pathnames</a>, Previous: <a href="Pathnames.html" accesskey="p" rel="prev">Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
