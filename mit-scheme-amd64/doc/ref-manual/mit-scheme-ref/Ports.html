<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ports (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Ports (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Ports (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="File-Ports.html" rel="next" title="File Ports">
<link href="Input_002fOutput.html" rel="prev" title="Input/Output">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Ports"></span><div class="header">
<p>
Next: <a href="File-Ports.html" accesskey="n" rel="next">File Ports</a>, Previous: <a href="Input_002fOutput.html" accesskey="p" rel="prev">Input/Output</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Ports-1"></span><h3 class="section">14.1 Ports</h3>

<span id="index-port-_0028defn_0029"></span>
<span id="index-input-port-_0028defn_0029"></span>
<span id="index-output-port-_0028defn_0029"></span>
<p>Ports represent input and output devices.  To Scheme, an <em>input
port</em> is a Scheme object that can deliver data upon command, while an
<em>output port</em> is a Scheme object that can accept data.  Whether
the input and output port types are disjoint is
implementation-dependent.  (In MIT/GNU Scheme, there are input ports,
output ports, and input/output ports.)
</p>
<p>Different port types operate on different data.  Scheme
implementations are required to support textual ports and binary
ports, but may also provide other port types.
</p>
<span id="index-textual-port-_0028defn_0029"></span>
<span id="index-read_002dchar"></span>
<span id="index-write_002dchar-1"></span>
<span id="index-read-6"></span>
<span id="index-write-3"></span>
<p>A <em>textual port</em> supports reading or writing of individual
characters from or to a backing store containing characters using
<code>read-char</code> and <code>write-char</code> below, and it supports
operations defined in terms of characters, such as <code>read</code> and
<code>write</code>.
</p>
<span id="index-binary-port-_0028defn_0029"></span>
<span id="index-read_002du8"></span>
<span id="index-write_002du8"></span>
<p>A <em>binary port</em> supports reading or writing of individual bytes
from or to a backing store containing bytes using <code>read-u8</code> and
<code>write-u8</code> below, as well as operations defined in terms of
bytes.  Whether the textual and binary port types are disjoint is
implementation-dependent.  (In MIT/GNU Scheme, textual ports and
binary ports are distinct.)
</p>
<p>Ports can be used to access files, devices, and similar things on the
host system on which the Scheme program is running.
</p>
<dl>
<dt id="index-call_002dwith_002dport">standard procedure: <strong>call-with-port</strong> <em>port procedure</em></dt>
<dd><p>It is an error if <var>procedure</var> does not accept one argument.
</p>
<p>The <code>call-with-port</code> procedure calls <var>procedure</var> with
<var>port</var> as an argument.  If <var>procedure</var> returns, then the port
is closed automatically and the values yielded by <var>procedure</var> are
returned.  If <var>procedure</var> does not return, then the port must not
be closed automatically unless it is possible to prove that the port
will never again be used for a read or write operation.
</p>
<p><em>Rationale</em>: Because Scheme&rsquo;s escape procedures have unlimited
extent, it is possible to escape from the current continuation but
later to resume it.  If implementations were permitted to close the
port on any escape from the current continuation, then it would be
impossible to write portable code using both
<code>call-with-current-continuation</code> and <code>call-with-port</code>.
</p></dd></dl>

<dl>
<dt id="index-call_002dwith_002dtruncated_002doutput_002dport">procedure: <strong>call-with-truncated-output-port</strong> <em>limit output-port procedure</em></dt>
<dd><p>The <var>limit</var> argument must be a nonnegative integer.  It is an
error if <var>procedure</var> does not accept one argument.
</p>
<p>This procedure uses a continuation to escape from <var>procedure</var> if
it tries to write more than <var>limit</var> characters.
</p>
<p>It calls <var>procedure</var> with a special output port as an argument.
Up to <var>limit</var> characters may be written to that output port, and
those characters are transparently written through to
<var>output-port</var>.
</p>
<p>If the number of characters written to that port exceeds <var>limit</var>,
then the escape continuation is invoked and <code>#t</code> is returned.
Otherwise, <var>procedure</var> returns normally and <code>#f</code> is returned.
</p>
<p>Note that if <var>procedure</var> writes exactly <var>limit</var> characters,
then the escape continuation is <em>not</em> invoked, and <code>#f</code> is
returned.
</p>
<p>In no case does <code>call-with-truncated-output-port</code> close
<var>output-port</var>.
</p></dd></dl>

<dl>
<dt id="index-input_002dport_003f">standard procedure: <strong>input-port?</strong> <em>object</em></dt>
<dt id="index-output_002dport_003f">standard procedure: <strong>output-port?</strong> <em>object</em></dt>
<dt id="index-i_002fo_002dport_003f">procedure: <strong>i/o-port?</strong> <em>object</em></dt>
<dt id="index-textual_002dport_003f">standard procedure: <strong>textual-port?</strong> <em>object</em></dt>
<dt id="index-binary_002dport_003f">standard procedure: <strong>binary-port?</strong> <em>object</em></dt>
<dt id="index-port_003f">standard procedure: <strong>port?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-port"></span>
<p>These procedures return <code>#t</code> if <var>object</var> is an input port,
output port, input/output port, textual port, binary port, or any kind
of port, respectively.  Otherwise they return <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-input_002dport_002dopen_003f">standard procedure: <strong>input-port-open?</strong> <em>port</em></dt>
<dt id="index-output_002dport_002dopen_003f">standard procedure: <strong>output-port-open?</strong> <em>port</em></dt>
<dd><p>Returns <code>#t</code> if <var>port</var> is still open and capable of
performing input or output, respectively, and <code>#f</code> otherwise.
</p></dd></dl>

<dl>
<dt id="index-current_002dinput_002dport">standard parameter: <strong>current-input-port</strong> <em>[input-port]</em></dt>
<dt id="index-current_002doutput_002dport">standard parameter: <strong>current-output-port</strong> <em>[output-port]</em></dt>
<dt id="index-current_002derror_002dport">standard parameter: <strong>current-error-port</strong> <em>[output-port]</em></dt>
<dd><span id="index-current-input-port-_0028defn_0029"></span>
<span id="index-input-port_002c-current-_0028defn_0029"></span>
<span id="index-current-output-port-_0028defn_0029"></span>
<span id="index-output-port_002c-current-_0028defn_0029"></span>
<span id="index-current-error-port-_0028defn_0029"></span>
<span id="index-error-port_002c-current-_0028defn_0029"></span>
<span id="index-port_002c-current"></span>
<p>Returns the current default input port, output port, or error port (an
output port), respectively.  These procedures are parameter objects,
which can be overridden with <code>parameterize</code>.  The initial
bindings for these are implementation-defined textual ports.
</p></dd></dl>

<dl>
<dt id="index-notification_002doutput_002dport">parameter: <strong>notification-output-port</strong> <em>[output-port]</em></dt>
<dd><span id="index-current-notification-port-_0028defn_0029"></span>
<span id="index-notification-port_002c-current-_0028defn_0029"></span>
<p>Returns an output port suitable for generating &ldquo;notifications&rdquo;, that
is, messages to the user that supply interesting information about the
execution of a program.  For example, the <code>load</code> procedure writes
messages to this port informing the user that a file is being loaded.
</p>
<p>This procedure is a parameter object, which can be overridden with
<code>parameterize</code>.
</p></dd></dl>

<dl>
<dt id="index-trace_002doutput_002dport">parameter: <strong>trace-output-port</strong> <em>[output-port]</em></dt>
<dd><span id="index-current-tracing-output-port-_0028defn_0029"></span>
<span id="index-tracing-output-port_002c-current-_0028defn_0029"></span>
<p>Returns an output port suitable for generating &ldquo;tracing&rdquo; information
about a program&rsquo;s execution.  The output generated by the <code>trace</code>
procedure is sent to this port.
</p>
<p>This procedure is a parameter object, which can be overridden with
<code>parameterize</code>.
</p></dd></dl>

<dl>
<dt id="index-interaction_002di_002fo_002dport">parameter: <strong>interaction-i/o-port</strong> <em>[i/o-port]</em></dt>
<dd><span id="index-current-interaction-port-_0028defn_0029"></span>
<span id="index-interaction-port_002c-current-_0028defn_0029"></span>
<p>Returns an <acronym>I/O</acronym> port suitable for querying or prompting the
user.  The standard prompting procedures use this port by default
(see <a href="Prompting.html">Prompting</a>).
</p>
<p>This procedure is a parameter object, which can be overridden with
<code>parameterize</code>.
</p></dd></dl>

<dl>
<dt id="index-close_002dport">standard procedure: <strong>close-port</strong> <em>port</em></dt>
<dt id="index-close_002dinput_002dport">standard procedure: <strong>close-input-port</strong> <em>port</em></dt>
<dt id="index-close_002doutput_002dport">standard procedure: <strong>close-output-port</strong> <em>port</em></dt>
<dd><span id="index-closing_002c-of-port"></span>
<p>Closes the resource associated with <var>port</var>, rendering the port
incapable of delivering or accepting data.  It is an error to apply
the last two procedures to a port which is not an input or output
port, respectively.  Scheme implementations may provide ports which
are simultaneously input and output ports, such as sockets; the
close-input-port and close-output-port procedures can then be used to
close the input and output sides of the port independently.
</p>
<p>These routines have no effect if the port has already been closed.
</p></dd></dl>

<dl>
<dt id="index-set_002dcurrent_002dinput_002dport_0021">obsolete procedure: <strong>set-current-input-port!</strong> <em>input-port</em></dt>
<dt id="index-set_002dcurrent_002doutput_002dport_0021">obsolete procedure: <strong>set-current-output-port!</strong> <em>output-port</em></dt>
<dt id="index-set_002dnotification_002doutput_002dport_0021">obsolete procedure: <strong>set-notification-output-port!</strong> <em>output-port</em></dt>
<dt id="index-set_002dtrace_002doutput_002dport_0021">obsolete procedure: <strong>set-trace-output-port!</strong> <em>output-port</em></dt>
<dt id="index-set_002dinteraction_002di_002fo_002dport_0021">obsolete procedure: <strong>set-interaction-i/o-port!</strong> <em>i/o-port</em></dt>
<dd><p>These procedures are <strong>deprecated</strong>; instead call the
corresponding parameters with an argument.
</p></dd></dl>

<dl>
<dt id="index-with_002dinput_002dfrom_002dport">obsolete procedure: <strong>with-input-from-port</strong> <em>input-port thunk</em></dt>
<dt id="index-with_002doutput_002dto_002dport">obsolete procedure: <strong>with-output-to-port</strong> <em>output-port thunk</em></dt>
<dt id="index-with_002dnotification_002doutput_002dport">obsolete procedure: <strong>with-notification-output-port</strong> <em>output-port thunk</em></dt>
<dt id="index-with_002dtrace_002doutput_002dport">obsolete procedure: <strong>with-trace-output-port</strong> <em>output-port thunk</em></dt>
<dt id="index-with_002dinteraction_002di_002fo_002dport">obsolete procedure: <strong>with-interaction-i/o-port</strong> <em>i/o-port thunk</em></dt>
<dd><span id="index-parameterize-4"></span>
<p>These procedures are <strong>deprecated</strong>; instead use
<code>parameterize</code> on the corresponding parameters.
</p></dd></dl>

<dl>
<dt id="index-console_002di_002fo_002dport">variable: <strong>console-i/o-port</strong></dt>
<dd><span id="index-port_002c-console"></span>
<span id="index-console_002c-port"></span>
<span id="index-input-port_002c-console"></span>
<span id="index-output-port_002c-console"></span>
<p><code>console-i/o-port</code> is an <acronym>I/O</acronym> port that communicates
with the &ldquo;console&rdquo;.  Under unix, the console is the controlling
terminal of the Scheme process.  Under Windows, the console is the
window that is created when Scheme starts up.
</p>
<p>This variable is rarely used; instead programs should use one of the
standard ports defined above.  This variable should not be modified.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="File-Ports.html" accesskey="n" rel="next">File Ports</a>, Previous: <a href="Input_002fOutput.html" accesskey="p" rel="prev">Input/Output</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
