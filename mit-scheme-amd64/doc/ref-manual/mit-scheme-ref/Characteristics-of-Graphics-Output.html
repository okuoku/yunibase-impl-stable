<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Characteristics of Graphics Output (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Characteristics of Graphics Output (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Characteristics of Graphics Output (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Graphics.html" rel="up" title="Graphics">
<link href="Buffering-of-Graphics-Output.html" rel="next" title="Buffering of Graphics Output">
<link href="Drawing-Graphics.html" rel="prev" title="Drawing Graphics">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Characteristics-of-Graphics-Output"></span><div class="header">
<p>
Next: <a href="Buffering-of-Graphics-Output.html" accesskey="n" rel="next">Buffering of Graphics Output</a>, Previous: <a href="Drawing-Graphics.html" accesskey="p" rel="prev">Drawing Graphics</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Characteristics-of-Graphics-Output-1"></span><h3 class="section">17.4 Characteristics of Graphics Output</h3>

<span id="index-graphics_002c-output-characteristics"></span>
<p>Two characteristics of graphics output are so useful that they are
supported uniformly by all graphics devices: <em>drawing mode</em> and
<em>line style</em>.  A third characteristic, <em>color</em>, is equally
useful (if not more so), but implementation restrictions prohibit a
uniform interface.
</p>
<span id="index-drawing-mode_002c-graphics-_0028defn_0029"></span>
<span id="index-graphics_002c-drawing-mode-_0028defn_0029"></span>
<p>The <em>drawing mode</em>, an exact integer in the range <code>0</code> to
<code>15</code> inclusive, determines how the figure being drawn is combined
with the background over which it is drawn to generate the final result.
Initially the drawing mode is set to &ldquo;source&rdquo;, so that the new output
overwrites whatever appears in that place.  Useful alternative drawing
modes can, for example, erase what was already there, or invert it.
</p>
<p>Altogether 16 boolean operations are available for combining the source
(what is being drawn) and the destination (what is being drawn over).
The source and destination are combined by the device on a
pixel-by-pixel basis as follows:
</p>
<div class="example">
<pre class="example">Mode    Meaning
----    -------
0       ZERO <span class="roman">[erase; use background color]</span>
1       source AND destination
2       source AND (NOT destination)
3       source
4       (NOT source) AND destination
5       destination
6       source XOR destination
7       source OR destination
8       NOT (source OR destination)
9       NOT (source XOR destination)
10      NOT destination
11      source OR (NOT destination)
12      NOT source
13      (NOT source) OR destination
14      (NOT source) OR (NOT destination)
15      ONE <span class="roman">[use foreground color]</span>
</pre></div>

<span id="index-line-style_002c-graphics-_0028defn_0029"></span>
<span id="index-graphics_002c-line-style-_0028defn_0029"></span>
<p>The <em>line style</em>, an exact integer in the range <code>0</code> to <code>7</code>
inclusive, determines which parts of a line are drawn in the foreground
color, and which in the background color.  The default line style,
&ldquo;solid&rdquo;, draws the entire line in the foreground color.
Alternatively, the &ldquo;dash&rdquo; style alternates between foreground and
background colors to generate a dashed line.  This capability is useful
for plotting several things on the same graph.
</p>
<p>Here is a table showing the name and approximate pattern of the
different styles.  A &lsquo;<samp>1</samp>&rsquo; in the pattern represents a foreground
pixel, while a &lsquo;<samp>-</samp>&rsquo; represents a background pixel.  Note that the
precise output for each style will vary from device to device.  The only
style that is guaranteed to be the same for every device is &ldquo;solid&rdquo;.
</p>
<div class="example">
<pre class="example">Style   Name                    Pattern
-----   -------                 -------
0       solid                   1111111111111111
1       dash                    11111111--------
2       dot                     1-1-1-1-1-1-1-1-
3       dash dot                1111111111111-1-
4       dash dot dot            11111111111-1-1-
5       long dash               11111111111-----
6       center dash             111111111111-11-
7       center dash dash        111111111-11-11-
</pre></div>

<dl>
<dt id="index-graphics_002dbind_002ddrawing_002dmode">procedure: <strong>graphics-bind-drawing-mode</strong> <em>graphics-device drawing-mode thunk</em></dt>
<dt id="index-graphics_002dbind_002dline_002dstyle">procedure: <strong>graphics-bind-line-style</strong> <em>graphics-device line-style thunk</em></dt>
<dd><p>These procedures bind the drawing mode or line style, respectively, of
<var>graphics-device</var>, invoke the procedure <var>thunk</var> with no
arguments, then undo the binding when <var>thunk</var> returns.  The value of
each procedure is the value returned by <var>thunk</var>.  Graphics
operations performed during <var>thunk</var>&rsquo;s dynamic extent will see the
newly bound mode or style as current.
</p></dd></dl>

<dl>
<dt id="index-graphics_002dset_002ddrawing_002dmode">procedure: <strong>graphics-set-drawing-mode</strong> <em>graphics-device drawing-mode</em></dt>
<dt id="index-graphics_002dset_002dline_002dstyle">procedure: <strong>graphics-set-line-style</strong> <em>graphics-device line-style</em></dt>
<dd><p>These procedures change the drawing mode or line style, respectively, of
<var>graphics-device</var>.  The mode or style will remain in effect until
subsequent changes or bindings.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Buffering-of-Graphics-Output.html" accesskey="n" rel="next">Buffering of Graphics Output</a>, Previous: <a href="Drawing-Graphics.html" accesskey="p" rel="prev">Drawing Graphics</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
