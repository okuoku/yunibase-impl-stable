<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Flonum Operations (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Flonum Operations (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Flonum Operations (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Fixnum-and-Flonum-Operations.html" rel="up" title="Fixnum and Flonum Operations">
<link href="Floating_002dPoint-Environment.html" rel="next" title="Floating-Point Environment">
<link href="Fixnum-Operations.html" rel="prev" title="Fixnum Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Flonum-Operations"></span><div class="header">
<p>
Next: <a href="Floating_002dPoint-Environment.html" accesskey="n" rel="next">Floating-Point Environment</a>, Previous: <a href="Fixnum-Operations.html" accesskey="p" rel="prev">Fixnum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Flonum-Operations-1"></span><h4 class="subsection">4.7.2 Flonum Operations</h4>

<span id="index-flonum-_0028defn_0029"></span>
<p>A <em>flonum</em> is an inexact real number that is implemented as a
floating-point number.  In MIT/GNU Scheme, all inexact real numbers are
flonums.  For this reason, constants such as <code>0.</code> and <code>2.3</code>
are guaranteed to be flonums.
</p>
<p>MIT/GNU Scheme follows the <acronym>IEEE 754-2008</acronym> floating-point
standard, using binary64 arithmetic for flonums.
All floating-point values are classified into:
</p>
<dl compact="compact">
<dt>normal</dt>
<dd><span id="index-floating_002dpoint-number_002c-normal"></span>
<span id="index-normal-floating_002dpoint-number"></span>
<p>Numbers of the form
</p>
<div class="example">
<pre class="example">r^e (1 + f/r^p)
</pre></div>

<p>where <em>r</em>, the radix, is a positive integer, here always <em>2</em>;
<em>p</em>, the precision, is a positive integer, here always <em>53</em>;
<em>e</em>, the exponent, is an integer within a limited range, here
always <em>-1022</em> to <em>1023</em> (inclusive); and <em>f</em>, the
fractional part of the significand, is a <em>(p-1)</em>-bit unsigned
integer,
</p>
</dd>
<dt>subnormal</dt>
<dd><span id="index-floating_002dpoint-number_002c-subnormal"></span>
<span id="index-subnormal-floating_002dpoint-number"></span>
<span id="index-denormal"></span>
<p>Fixed-point numbers near zero that allow for gradual underflow.
Every subnormal number is an integer multiple of the smallest
subnormal number.
Subnormals were also historically called &ldquo;denormal&rdquo;.
</p>
</dd>
<dt>zero</dt>
<dd><span id="index-floating_002dpoint-number_002c-zero"></span>
<span id="index-zero-1"></span>
<span id="index-signed-zero"></span>
<p>There are two distinguished zero values, one with &ldquo;negative&rdquo; sign
bit and one with &ldquo;positive&rdquo; sign bit.
</p>
<p>The two zero values are considered numerically equal, but serve to
distinguish paths converging to zero along different branch cuts and
so some operations yield different results for differently signed
zero values.
</p>
</dd>
<dt>infinity</dt>
<dd><span id="index-_002binf_002e0"></span>
<span id="index-_002dinf_002e0"></span>
<span id="index-positive-infinity-_0028_002binf_002e0_0029"></span>
<span id="index-negative-infinity-_0028_002dinf_002e0_0029"></span>
<span id="index-floating_002dpoint-number_002c-infinite"></span>
<span id="index-infinity-_0028_002binf_002e0_002c-_002dinf_002e0_0029"></span>
<span id="index-extended-real-line"></span>
<p>There are two distinguished infinity values, negative infinity or
<code>-inf.0</code> and positive infinity or <code>+inf.0</code>, representing
overflow on the real line.
</p>
</dd>
<dt>NaN</dt>
<dd><span id="index-NaN-_0028not-a-number_0029"></span>
<span id="index-_002bnan_002e0"></span>
<span id="index-_002dnan_002e0"></span>
<span id="index-_002bsnan_002e1"></span>
<span id="index-_002dsnan_002e1"></span>
<span id="index-floating_002dpoint-number_002c-not-a-number"></span>
<span id="index-not-a-number-_0028NaN_002c-_002bnan_002e0_0029"></span>
<span id="index-NaN"></span>
<p>There are <em>4 r^{p-2} - 2</em> distinguished not-a-number values,
representing invalid operations or uninitialized data, distinguished
by their negative/positive sign bit, a quiet/signalling bit, and a
<em>(p-2)</em>-digit unsigned integer payload which must not be zero for
signalling NaNs.
</p>
<span id="index-quiet-NaN"></span>
<span id="index-signalling-NaN"></span>
<span id="index-invalid_002doperation-exception"></span>
<p>Arithmetic on <strong>quiet</strong> NaNs propagates them without raising any
floating-point exceptions.
In contrast, arithmetic on <strong>signalling</strong> NaNs raises the
floating-point invalid-operation exception.
Quiet NaNs are written <code>+nan.123</code>, <code>-nan.0</code>, etc.
Signalling NaNs are written <code>+snan.123</code>, <code>-snan.1</code>, etc.
The notation <code>+snan.0</code> and <code>-snan.0</code> is not allowed: what
would be the encoding for them actually means <code>+inf.0</code> and
<code>-inf.0</code>.
</p>
</dd>
</dl>

<dl>
<dt id="index-flo_003aflonum_003f">procedure: <strong>flo:flonum?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-flonum"></span>
<p>Returns <code>#t</code> if <var>object</var> is a flonum; otherwise returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003a_003d">procedure: <strong>flo:=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_003c">procedure: <strong>flo:&lt;</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_003c_003d">procedure: <strong>flo:&lt;=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_003e">procedure: <strong>flo:&gt;</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_003e_003d">procedure: <strong>flo:&gt;=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_003c_003e">procedure: <strong>flo:&lt;&gt;</strong> <em>flonum1 flonum2</em></dt>
<dd><span id="index-equivalence-predicate_002c-for-flonums"></span>
<span id="index-ordered-comparison"></span>
<span id="index-floating_002dpoint-comparison_002c-ordered"></span>
<span id="index-trichotomy"></span>
<p>These procedures are the standard order and equality predicates on
flonums.  When compiled, they do not check the types of their arguments.
These predicates raise floating-point invalid-operation exceptions on
NaN arguments; in other words, they are &ldquo;ordered comparisons&rdquo;.
When floating-point exception traps are disabled, they return false
when any argument is NaN.
</p>
<p>Every pair of floating-point numbers &mdash; excluding NaN &mdash; exhibits
ordered trichotomy: they are related either by <code>flo:=</code>,
<code>flo:&lt;</code>, or <code>flo:&gt;</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003asafe_003d">procedure: <strong>flo:safe=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003asafe_003c">procedure: <strong>flo:safe&lt;</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003asafe_003c_003d">procedure: <strong>flo:safe&lt;=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003asafe_003e">procedure: <strong>flo:safe&gt;</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003asafe_003e_003d">procedure: <strong>flo:safe&gt;=</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003asafe_003c_003e">procedure: <strong>flo:safe&lt;&gt;</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003aunordered_003f">procedure: <strong>flo:unordered?</strong> <em>flonum1 flonum2</em></dt>
<dd><span id="index-equivalence-predicate_002c-for-flonums-1"></span>
<span id="index-unordered-comparison"></span>
<span id="index-floating_002dpoint-comparison_002c-unordered"></span>
<span id="index-tetrachotomy"></span>
<p>These procedures are the standard order and equality predicates on
flonums.  When compiled, they do not check the types of their arguments.
These predicates do not raise floating-point exceptions, and simply
return false on NaN arguments, except <code>flo:unordered?</code> which
returns true iff at least one argument is NaN; in other words, they
are &ldquo;unordered comparisons&rdquo;.
</p>
<p>Every pair of floating-point values &mdash; including NaN &mdash; exhibits
unordered tetrachotomy: they are related either by <code>flo:safe=</code>,
<code>flo:safe&lt;</code>, <code>flo:safe&gt;</code>, or <code>flo:unordered?</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003azero_003f">procedure: <strong>flo:zero?</strong> <em>flonum</em></dt>
<dt id="index-flo_003apositive_003f">procedure: <strong>flo:positive?</strong> <em>flonum</em></dt>
<dt id="index-flo_003anegative_003f">procedure: <strong>flo:negative?</strong> <em>flonum</em></dt>
<dd><p>Each of these procedures compares its argument to zero.  When compiled,
they do not check the type of their argument.
These predicates raise floating-point invalid-operation exceptions on
NaN arguments; in other words, they are &ldquo;ordered comparisons&rdquo;.
</p>
<div class="example">
<pre class="example">(flo:zero? -0.)                &rArr; #t
(flo:negative? -0.)            &rArr; #f
(flo:negative? -1.)            &rArr; #t

(flo:zero? 0.)                 &rArr; #t
(flo:positive? 0.)             &rArr; #f
(flo:positive? 1.)             &rArr; #f

(flo:zero? +nan.123)           &rArr; #f  <span class="roman">; (raises invalid-operation)</span>
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003anormal_003f">procedure: <strong>flo:normal?</strong> <em>flonum</em></dt>
<dt id="index-flo_003asubnormal_003f">procedure: <strong>flo:subnormal?</strong> <em>flonum</em></dt>
<dt id="index-flo_003asafe_002dzero_003f">procedure: <strong>flo:safe-zero?</strong> <em>flonum</em></dt>
<dt id="index-flo_003ainfinite_003f">procedure: <strong>flo:infinite?</strong> <em>flonum</em></dt>
<dt id="index-flo_003anan_003f">procedure: <strong>flo:nan?</strong> <em>flonum</em></dt>
<dd><p>Floating-point classification predicates.
For any flonum, exactly one of these predicates returns true.
These predicates never raise floating-point exceptions.
</p>
<div class="example">
<pre class="example">(flo:normal? 1.23)             &rArr; #t
(flo:subnormal? 4e-124)        &rArr; #t
(flo:safe-zero? -0.)           &rArr; #t
(flo:infinite? +inf.0)         &rArr; #t
(flo:nan? -nan.123)            &rArr; #t
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003afinite_003f">procedure: <strong>flo:finite?</strong> <em>flonum</em></dt>
<dd><p>Equivalent to:
</p>
<div class="example">
<pre class="example">(or (flo:safe-zero? <var>flonum</var>)
    (flo:subnormal? <var>flonum</var>)
    (flo:normal? <var>flonum</var>))
; or
(and (not (flo:infinite? <var>flonum</var>))
     (not (flo:nan? <var>flonum</var>)))
</pre></div>

<p>True for normal, subnormal, and zero floating-point values; false for
infinity and NaN.
</p></dd></dl>

<dl>
<dt id="index-flo_003aclassify">procedure: <strong>flo:classify</strong> <em>flonum</em></dt>
<dd><p>Returns a symbol representing the classification of the flonum, one
of <code>normal</code>, <code>subnormal</code>, <code>zero</code>, <code>infinity</code>, or
<code>nan</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003asign_002dnegative_003f">procedure: <strong>flo:sign-negative?</strong> <em>flonum</em></dt>
<dd><p>Returns true if the sign bit of <var>flonum</var> is negative, and false
otherwise.
Never raises a floating-point exception.
</p>
<div class="example">
<pre class="example">(flo:sign-negative? +0.)       &rArr; #f
(flo:sign-negative? -0.)       &rArr; #t
(flo:sign-negative? -1.)       &rArr; #t
(flo:sign-negative? +inf.0)    &rArr; #f
(flo:sign-negative? +nan.123)  &rArr; #f

(flo:negative? -0.)            &rArr; #f
(flo:negative? +nan.123)       &rArr; #f  <span class="roman">; (raises invalid-operation)</span>
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003a_002b">procedure: <strong>flo:+</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_002d">procedure: <strong>flo:-</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_002a">procedure: <strong>flo:*</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003a_002f">procedure: <strong>flo:/</strong> <em>flonum1 flonum2</em></dt>
<dd><p>These procedures are the standard arithmetic operations on flonums.
When compiled, they do not check the types of their arguments.
</p></dd></dl>

<dl>
<dt id="index-flo_003a_002a_002b">procedure: <strong>flo:*+</strong> <em>flonum1 flonum2 flonum3</em></dt>
<dt id="index-flo_003afma">procedure: <strong>flo:fma</strong> <em>flonum1 flonum2 flonum3</em></dt>
<dt id="index-flo_003afast_002dfma_003f">procedure: <strong>flo:fast-fma?</strong></dt>
<dd><p>Fused multiply-add:
<code>(flo:*+ <var>u</var> <var>v</var> <var>a</var>)</code> computes <em>uv+a</em> correctly
rounded, with no intermediate overflow or underflow arising from
<em>uv</em>.
In contrast, <code>(flo:+ (flo:* <var>u</var> <var>v</var>) <var>a</var>)</code> may have
two rounding errors, and can overflow or underflow if <em>uv</em> is too
large or too small even if <em>uv + a</em> is normal.
<code>Flo:fma</code> is an alias for <code>flo:*+</code> with the more familiar
name used in other languages like C.
</p>
<p><code>Flo:fast-fma?</code> returns true if the implementation of fused
multiply-add is supported by fast hardware, and false if it is
emulated using Dekker&rsquo;s double-precision algorithm in software.
</p>
<div class="example">
<pre class="example">(flo:+ (flo:* 1.2e100 2e208) -1.4e308)
                               &rArr; +inf.0  <span class="roman">; (raises overflow)</span>
(flo:*+ 1.2e100 2e208  -1.4e308)
                               &rArr; 1e308
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003anegate">procedure: <strong>flo:negate</strong> <em>flonum</em></dt>
<dd><p>This procedure returns the negation of its argument.  When compiled, it
does not check the type of its argument.
</p>
<p>This is <em>not</em> equivalent to <code>(flo:- 0. <var>flonum</var>)</code>:
</p>
<div class="example">
<pre class="example">(flo:negate 1.2)               &rArr; -1.2
(flo:negate -nan.123)          &rArr; +nan.123
(flo:negate +inf.0)            &rArr; -inf.0
(flo:negate 0.)                &rArr; -0.
(flo:negate -0.)               &rArr; 0.

(flo:- 0. 1.2)                 &rArr; -1.2
(flo:- 0. -nan.123)            &rArr; -nan.123
(flo:- 0. +inf.0)              &rArr; -inf.0
(flo:- 0. 0.)                  &rArr; 0.
(flo:- 0. -0.)                 &rArr; 0.
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003aabs">procedure: <strong>flo:abs</strong> <em>flonum</em></dt>
<dt id="index-flo_003aexp">procedure: <strong>flo:exp</strong> <em>flonum</em></dt>
<dt id="index-flo_003alog">procedure: <strong>flo:log</strong> <em>flonum</em></dt>
<dt id="index-flo_003asin">procedure: <strong>flo:sin</strong> <em>flonum</em></dt>
<dt id="index-flo_003acos">procedure: <strong>flo:cos</strong> <em>flonum</em></dt>
<dt id="index-flo_003atan">procedure: <strong>flo:tan</strong> <em>flonum</em></dt>
<dt id="index-flo_003aasin">procedure: <strong>flo:asin</strong> <em>flonum</em></dt>
<dt id="index-flo_003aacos">procedure: <strong>flo:acos</strong> <em>flonum</em></dt>
<dt id="index-flo_003aatan">procedure: <strong>flo:atan</strong> <em>flonum</em></dt>
<dt id="index-flo_003asinh">procedure: <strong>flo:sinh</strong> <em>flonum</em></dt>
<dt id="index-flo_003acosh">procedure: <strong>flo:cosh</strong> <em>flonum</em></dt>
<dt id="index-flo_003atanh">procedure: <strong>flo:tanh</strong> <em>flonum</em></dt>
<dt id="index-flo_003aasinh">procedure: <strong>flo:asinh</strong> <em>flonum</em></dt>
<dt id="index-flo_003aacosh">procedure: <strong>flo:acosh</strong> <em>flonum</em></dt>
<dt id="index-flo_003aatanh">procedure: <strong>flo:atanh</strong> <em>flonum</em></dt>
<dt id="index-flo_003asqrt">procedure: <strong>flo:sqrt</strong> <em>flonum</em></dt>
<dt id="index-flo_003acbrt">procedure: <strong>flo:cbrt</strong> <em>flonum</em></dt>
<dt id="index-flo_003aexpt">procedure: <strong>flo:expt</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003aerf">procedure: <strong>flo:erf</strong> <em>flonum</em></dt>
<dt id="index-flo_003aerfc">procedure: <strong>flo:erfc</strong> <em>flonum</em></dt>
<dt id="index-flo_003ahypot">procedure: <strong>flo:hypot</strong> <em>flonum1 flonum2</em></dt>
<dt id="index-flo_003aj0">procedure: <strong>flo:j0</strong> <em>flonum</em></dt>
<dt id="index-flo_003aj1">procedure: <strong>flo:j1</strong> <em>flonum</em></dt>
<dt id="index-flo_003ajn">procedure: <strong>flo:jn</strong> <em>flonum</em></dt>
<dt id="index-flo_003ay0">procedure: <strong>flo:y0</strong> <em>flonum</em></dt>
<dt id="index-flo_003ay1">procedure: <strong>flo:y1</strong> <em>flonum</em></dt>
<dt id="index-flo_003ayn">procedure: <strong>flo:yn</strong> <em>flonum</em></dt>
<dt id="index-flo_003agamma">procedure: <strong>flo:gamma</strong> <em>flonum</em></dt>
<dt id="index-flo_003algamma">procedure: <strong>flo:lgamma</strong> <em>flonum</em></dt>
<dt id="index-flo_003afloor">procedure: <strong>flo:floor</strong> <em>flonum</em></dt>
<dt id="index-flo_003aceiling">procedure: <strong>flo:ceiling</strong> <em>flonum</em></dt>
<dt id="index-flo_003atruncate">procedure: <strong>flo:truncate</strong> <em>flonum</em></dt>
<dt id="index-flo_003around">procedure: <strong>flo:round</strong> <em>flonum</em></dt>
<dt id="index-flo_003afloor_002d_003eexact">procedure: <strong>flo:floor-&gt;exact</strong> <em>flonum</em></dt>
<dt id="index-flo_003aceiling_002d_003eexact">procedure: <strong>flo:ceiling-&gt;exact</strong> <em>flonum</em></dt>
<dt id="index-flo_003atruncate_002d_003eexact">procedure: <strong>flo:truncate-&gt;exact</strong> <em>flonum</em></dt>
<dt id="index-flo_003around_002d_003eexact">procedure: <strong>flo:round-&gt;exact</strong> <em>flonum</em></dt>
<dd><p>These procedures are flonum versions of the corresponding procedures.
When compiled, they do not check the types of their arguments.
</p></dd></dl>

<dl>
<dt id="index-flo_003aexpm1">procedure: <strong>flo:expm1</strong> <em>flonum</em></dt>
<dt id="index-flo_003alog1p">procedure: <strong>flo:log1p</strong> <em>flonum</em></dt>
<dd><p>Flonum versions of <code>expm1</code> and <code>log1p</code> with restricted
domains: <code>flo:expm1</code> is defined only on inputs bounded below
log(2)
in magnitude, and <code>flo:log1p</code> is defined only on inputs bounded
below
<em>1 - sqrt(1/2)</em>
in magnitude.
Callers must use <code>(- (flo:exp x) 1)</code> or <code>(flo:log (+ 1 x))</code>
outside these ranges.
</p></dd></dl>

<dl>
<dt id="index-flo_003aatan2">procedure: <strong>flo:atan2</strong> <em>flonum1 flonum2</em></dt>
<dd><span id="index-atan-2"></span>
<p>This is the flonum version of <code>atan</code> with two arguments.  When
compiled, it does not check the types of its arguments.
</p></dd></dl>

<dl>
<dt id="index-flo_003asigned_002dlgamma">procedure: <strong>flo:signed-lgamma</strong> <em>x</em></dt>
<dd><p>Returns two values,
</p>
<div class="example">
<pre class="example">m = log(|Gamma(<var>x</var>)|)

and

s = sign(Gamma(<var>x</var>)),
</pre></div>

<p>respectively a flonum and an exact integer either <code>-1</code> or
<code>1</code>, so that
</p>
<div class="example">
<pre class="example">Gamma(x) = s * e^m.
</pre></div>

</dd></dl>

<dl>
<dt id="index-flo_003amin">procedure: <strong>flo:min</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003amax">procedure: <strong>flo:max</strong> <em>x1 x2</em></dt>
<dd><p>Returns the min or max of two floating-point numbers.
If either argument is NaN, raises the floating-point invalid-operation
exception and returns the other one if it is not NaN, or the first
argument if they are both NaN.
</p></dd></dl>

<dl>
<dt id="index-flo_003amin_002dmag">procedure: <strong>flo:min-mag</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003amax_002dmag">procedure: <strong>flo:max-mag</strong> <em>x1 x2</em></dt>
<dd><p>Returns the argument that has the smallest or largest magnitude, as in
minNumMag or maxNumMag of <acronym>IEEE 754-2008</acronym>.
If either argument is NaN, raises the floating-point invalid-operation
exception and returns the other one if it is not NaN, or the first
argument if they are both NaN.
</p></dd></dl>

<dl>
<dt id="index-flo_003aldexp">procedure: <strong>flo:ldexp</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003ascalbn">procedure: <strong>flo:scalbn</strong> <em>x1 x2</em></dt>
<dd><p><code>Flo:ldexp</code> scales by a power of two; <code>flo:scalbn</code> scales by
a power of the floating-point radix.
</p>
<div class="example">
<pre class="example">ldexp x e := x * 2^e,
scalbn x e := x * r^e.
</pre></div>

<p>In MIT/GNU Scheme, these procedures are the same; they are both
provided to make it clearer which operation is meant.
</p></dd></dl>

<dl>
<dt id="index-flo_003alogb">procedure: <strong>flo:logb</strong> <em>x</em></dt>
<dd><p>For nonzero finite <var>x</var>, returns
<em>floor(log(x)/log(r))</em>
as an exact integer, where <em>r</em> is the floating-point radix.
</p>
<p>For all other inputs, raises invalid-operation and returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003anextafter">procedure: <strong>flo:nextafter</strong> <em>x1 x2</em></dt>
<dd><p>Returns the next floating-point number after <var>x1</var> in the direction
of <var>x2</var>.
</p>
<div class="example">
<pre class="example">(flo:nextafter 0. -1.)         &rArr; -4.9406564584124654e-324
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003acopysign">procedure: <strong>flo:copysign</strong> <em>x1 x2</em></dt>
<dd><p>Returns a floating-point number with the magnitude of <var>x1</var> and the
sign of <var>x2</var>.
</p>
<div class="example">
<pre class="example">(flo:copysign 123. 456.)       &rArr; 123.
(flo:copysign +inf.0 -1)       &rArr; -inf.0
(flo:copysign 0. -1)           &rArr; -0.
(flo:copysign -0. 0.)          &rArr; 0.
(flo:copysign -nan.123 0.)     &rArr; +nan.123
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003aradix">constant: <strong>flo:radix</strong></dt>
<dt id="index-flo_003aradix_002e">constant: <strong>flo:radix.</strong></dt>
<dt id="index-flo_003aprecision">constant: <strong>flo:precision</strong></dt>
<dd><p>Floating-point system parameters.
<code>Flo:radix</code> is the floating-point radix as an integer, and
<code>flo:precision</code> is the floating-point precision as an integer;
<code>flo:radix.</code> is the flotaing-point radix as a flonum.
</p></dd></dl>

<dl>
<dt id="index-flo_003aerror_002dbound">constant: <strong>flo:error-bound</strong></dt>
<dt id="index-flo_003alog_002derror_002dbound">constant: <strong>flo:log-error-bound</strong></dt>
<dt id="index-flo_003aulp_002dof_002done">constant: <strong>flo:ulp-of-one</strong></dt>
<dt id="index-flo_003alog_002dulp_002dof_002done">constant: <strong>flo:log-ulp-of-one</strong></dt>
<dd><p><code>Flo:error-bound</code>, sometimes called the machine epsilon, is the
maximum relative error of rounding to nearest:
</p>
<div class="example">
<pre class="example">max |x - fl(x)|/|x| = 1/(2 r^(p-1)),
</pre></div>

<p>where <em>r</em> is the floating-point radix and <em>p</em> is the
floating-point precision.
</p>
<p><code>Flo:ulp-of-one</code> is the distance from <em>1</em> to the next larger
floating-point number, and is equal to <em>1/r^{p-1}</em>.
</p>
<p><code>Flo:error-bound</code> is half <code>flo:ulp-of-one</code>.
</p>
<p><code>Flo:log-error-bound</code> is the logarithm of <code>flo:error-bound</code>,
and <code>flo:log-ulp-of-one</code> is the logarithm of
<code>flo:log-ulp-of-one</code>.
</p></dd></dl>

<dl>
<dt id="index-flo_003aulp">procedure: <strong>flo:ulp</strong> <em>flonum</em></dt>
<dd><p>Returns the distance from <var>flonum</var> to the next floating-point
number larger in magnitude with the same sign.
For zero, this returns the smallest subnormal.
For infinities, this returns positive infinity.
For NaN, this returns the same NaN.
</p>
<div class="example">
<pre class="example">(flo:ulp 1.)                    &rArr; 2.220446049250313e-16
(= (flo:ulp 1.) flo:ulp-of-one) &rArr; #t
</pre></div>
</dd></dl>

<dl>
<dt id="index-flo_003anormal_002dexponent_002dmax">constant: <strong>flo:normal-exponent-max</strong></dt>
<dt id="index-flo_003anormal_002dexponent_002dmin">constant: <strong>flo:normal-exponent-min</strong></dt>
<dt id="index-flo_003asubnormal_002dexponent_002dmin">constant: <strong>flo:subnormal-exponent-min</strong></dt>
<dd><p>Largest and smallest positive integer exponents of the radix in normal
and subnormal floating-point numbers.
</p>
<ul>
<li> <code>Flo:normal-exponent-max</code> is the largest positive integer such
that <code>(expt flo:radix. flo:normal-exponent-max)</code> does not
overflow.

</li><li> <code>Flo:normal-exponent-min</code> is the smallest positive integer such
that <code>(expt flo:radix. flo:normal-exponent-min)</code> is a normal
floating-point number.

</li><li> <code>Flo:subnormal-exponent-min</code> is the smallest positive integer such
that <code>(expt flo:radix. flo:subnormal-exponent-min)</code> is nonzero;
this is also the smallest positive floating-point number.
</li></ul>
</dd></dl>

<dl>
<dt id="index-flo_003alargest_002dpositive_002dnormal">constant: <strong>flo:largest-positive-normal</strong></dt>
<dt id="index-flo_003asmallest_002dpositive_002dnormal">constant: <strong>flo:smallest-positive-normal</strong></dt>
<dt id="index-flo_003asmallest_002dpositive_002dsubnormal">constant: <strong>flo:smallest-positive-subnormal</strong></dt>
<dd><p>Smallest and largest normal and subnormal numbers in magnitude.
</p></dd></dl>

<dl>
<dt id="index-flo_003agreatest_002dnormal_002dexponent_002dbase_002de">constant: <strong>flo:greatest-normal-exponent-base-e</strong></dt>
<dt id="index-flo_003agreatest_002dnormal_002dexponent_002dbase_002d2">constant: <strong>flo:greatest-normal-exponent-base-2</strong></dt>
<dt id="index-flo_003agreatest_002dnormal_002dexponent_002dbase_002d10">constant: <strong>flo:greatest-normal-exponent-base-10</strong></dt>
<dt id="index-flo_003aleast_002dnormal_002dexponent_002dbase_002de">constant: <strong>flo:least-normal-exponent-base-e</strong></dt>
<dt id="index-flo_003aleast_002dnormal_002dexponent_002dbase_002d2">constant: <strong>flo:least-normal-exponent-base-2</strong></dt>
<dt id="index-flo_003aleast_002dnormal_002dexponent_002dbase_002d10">constant: <strong>flo:least-normal-exponent-base-10</strong></dt>
<dt id="index-flo_003aleast_002dsubnormal_002dexponent_002dbase_002de">constant: <strong>flo:least-subnormal-exponent-base-e</strong></dt>
<dt id="index-flo_003aleast_002dsubnormal_002dexponent_002dbase_002d2">constant: <strong>flo:least-subnormal-exponent-base-2</strong></dt>
<dt id="index-flo_003aleast_002dsubnormal_002dexponent_002dbase_002d10">constant: <strong>flo:least-subnormal-exponent-base-10</strong></dt>
<dd><p>Least and greatest exponents of normal and subnormal floating-point
numbers, as floating-point numbers.
For example, <code>flo:greatest-normal-exponent-base-2</code> is the
greatest floating-point number such that <code>(expt
2. flo:greatest-normal-exponent-base-2)</code> does not overflow and is a
normal floating-point number.
</p></dd></dl>

<dl>
<dt id="index-flo_003atotal_003c">procedure: <strong>flo:total&lt;</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003atotal_002dmag_003c">procedure: <strong>flo:total-mag&lt;</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003atotal_002dorder">procedure: <strong>flo:total-order</strong> <em>x1 x2</em></dt>
<dt id="index-flo_003atotal_002dorder_002dmag">procedure: <strong>flo:total-order-mag</strong> <em>x1 x2</em></dt>
<dd><p>These procedures implement the <acronym>IEEE 754-2008</acronym> total ordering
on floating-point values and their magnitudes.
Here the &ldquo;magnitude&rdquo; of a floating-point value is a floating-point
value with positive sign bit and everything else the same; e.g.,
<code>+nan.123</code> is the &ldquo;magnitude&rdquo; of <code>-nan.123</code> and <code>0.0</code>
is the &ldquo;magnitude&rdquo; of <code>-0.0</code>.
</p>
<p>The total ordering has little to no numerical meaning and should be
used only when an arbitrary choice of total ordering is required for
some non-numerical reason.
</p>
<ul>
<li> <code>Flo:total&lt;</code> returns true if <var>x1</var> precedes <var>x2</var>.

</li><li> <code>Flo:total-mag&lt;</code> returns true if the magnitude of <var>x1</var>
precedes the magnitude of <var>x2</var>.

</li><li> <code>Flo:total-order</code> returns <em>-1</em> if <var>x1</var> precedes
<var>x2</var>, <em>0</em> if they are the same floating-point value
(including sign of zero, or sign and payload of NaN), and <em>+1</em> if
<var>x1</var> follows <var>x2</var>.

</li><li> <code>Flo:total-order-mag</code> returns <em>-1</em> if the magnitude of
<var>x1</var> precedes the magnitude of <var>x2</var>, etc.
</li></ul>
</dd></dl>

<dl>
<dt id="index-flo_003amake_002dnan">procedure: <strong>flo:make-nan</strong> <em>negative? quiet? payload</em></dt>
<dt id="index-flo_003anan_002dquiet_003f">procedure: <strong>flo:nan-quiet?</strong> <em>nan</em></dt>
<dt id="index-flo_003anan_002dpayload">procedure: <strong>flo:nan-payload</strong> <em>nan</em></dt>
<dd><p><code>Flo:make-nan</code> creates a NaN given the sign bit, quiet bit, and
payload.
<var>Negative?</var> and <var>quiet?</var> must be booleans, and <var>payload</var>
must be an unsigned <em>(p-2)</em>-bit integer, where <em>p</em> is the
floating-point precision.
If <var>quiet?</var> is false, <var>payload</var> must be nonzero.
</p>
<div class="example">
<pre class="example">(flo:sign-negative? (flo:make-nan <var>negative?</var> <var>quiet?</var> <var>payload</var>))
                               &rArr; <var>negative?</var>
(flo:nan-quiet? (flo:make-nan <var>negative?</var> <var>quiet?</var> <var>payload</var>))
                               &rArr; <var>quiet?</var>
(flo:nan-payload (flo:make-nan <var>negative?</var> <var>quiet?</var> <var>payload</var>))
                               &rArr; <var>payload</var>

(flo:make-nan #t #f 42)        &rArr; -snan.42
(flo:sign-negative? +nan.123)  &rArr; #f
(flo:quiet? +nan.123)          &rArr; #t
(flo:payload +nan.123)         &rArr; 123
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Floating_002dPoint-Environment.html" accesskey="n" rel="next">Floating-Point Environment</a>, Previous: <a href="Fixnum-Operations.html" accesskey="p" rel="prev">Fixnum Operations</a>, Up: <a href="Fixnum-and-Flonum-Operations.html" accesskey="u" rel="up">Fixnum and Flonum Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
