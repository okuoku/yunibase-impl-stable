<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Invoking Standard Restart Code (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Invoking Standard Restart Code (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Invoking Standard Restart Code (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Restarts.html" rel="up" title="Restarts">
<link href="Finding-and-Invoking-General-Restart-Code.html" rel="next" title="Finding and Invoking General Restart Code">
<link href="Establishing-Restart-Code.html" rel="prev" title="Establishing Restart Code">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Invoking-Standard-Restart-Code"></span><div class="header">
<p>
Next: <a href="Finding-and-Invoking-General-Restart-Code.html" accesskey="n" rel="next">Finding and Invoking General Restart Code</a>, Previous: <a href="Establishing-Restart-Code.html" accesskey="p" rel="prev">Establishing Restart Code</a>, Up: <a href="Restarts.html" accesskey="u" rel="up">Restarts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Invoking-Standard-Restart-Code-1"></span><h4 class="subsection">16.4.2 Invoking Standard Restart Code</h4>

<p>Scheme supports six standard protocols for restarting from a condition,
each encapsulated using a named restart (for use by condition-signalling
code) and a simple procedure (for use by condition-handling code).
Unless otherwise specified, if one of these procedures is unable to find
its corresponding restart, it returns immediately with an unspecified
value.
</p>
<p>Each of these procedures accepts an optional argument <var>restarts</var>,
which is described above in <a href="Restarts.html">Restarts</a>.
</p>
<dl>
<dt id="index-abort-1">procedure: <strong>abort</strong> <em>[restarts]</em></dt>
<dd><span id="index-REP-loop-6"></span>
<p>Abort the computation, using the restart named <code>abort</code>.  The
corresponding effector takes no arguments and abandons the current line
of computation.  This is the restart provided by Scheme&rsquo;s <small>REPL</small>.
</p>
<span id="index-condition_002dtype_003ano_002dsuch_002drestart-1"></span>
<p>If there is no restart named <code>abort</code>, this procedure signals an
error of type <code>condition-type:no-such-restart</code>.
</p></dd></dl>

<dl>
<dt id="index-continue-1">procedure: <strong>continue</strong> <em>[restarts]</em></dt>
<dd><p>Continue the current computation, using the restart named
<code>continue</code>.  The corresponding effector takes no arguments and
continues the computation beyond the point at which the condition was
signalled.
</p></dd></dl>

<dl>
<dt id="index-muffle_002dwarning-3">procedure: <strong>muffle-warning</strong> <em>[restarts]</em></dt>
<dd><span id="index-warn-2"></span>
<p>Continue the current computation, using the restart named
<code>muffle-warning</code>.  The corresponding effector takes no arguments
and continues the computation beyond the point at which any warning
message resulting from the condition would be presented to the user.
The procedure <code>warn</code> establishes a <code>muffle-warning</code> restart
for this purpose.
</p>
<span id="index-condition_002dtype_003ano_002dsuch_002drestart-2"></span>
<p>If there is no restart named <code>muffle-warning</code>, this procedure
signals an error of type <code>condition-type:no-such-restart</code>.
</p></dd></dl>

<dl>
<dt id="index-retry-1">procedure: <strong>retry</strong> <em>[restarts]</em></dt>
<dd><p>Retry the current computation, using the restart named <code>retry</code>.
The corresponding effector takes no arguments and simply retries the
same computation that triggered the condition.  The condition may
reoccur, of course, if the root cause has not been eliminated.  The code
that signals a &ldquo;file does not exist&rdquo; error can be expected to supply a
<code>retry</code> restart.  The restart would be invoked after first creating
the missing file, since the computation is then likely to succeed if it
is simply retried.
</p></dd></dl>

<dl>
<dt id="index-store_002dvalue-1">procedure: <strong>store-value</strong> <em>new-value [restarts]</em></dt>
<dd><p>Retry the current computation, using the restart named
<code>store-value</code>, after first storing <var>new-value</var>.  The
corresponding effector takes one argument, <var>new-value</var>, and stores
it away in a restart-dependent location, then retries the same
computation that triggered the condition.  The condition may reoccur, of
course, if the root cause has not been eliminated.  The code that
signals an &ldquo;unassigned variable&rdquo; error can be expected to supply a
<code>store-value</code> restart; this would store the value in the variable
and continue the computation.
</p></dd></dl>

<dl>
<dt id="index-use_002dvalue-1">procedure: <strong>use-value</strong> <em>new-value [restarts]</em></dt>
<dd><span id="index-retry-2"></span>
<span id="index-store_002dvalue-2"></span>
<p>Retry the current computation, using the restart named <code>use-value</code>,
but substituting <var>new-value</var> for a value that previously caused a
failure.  The corresponding effector takes one argument,
<var>new-value</var>, and retries the same computation that triggered the
condition with the new value substituted for the failing value.  The
condition may reoccur, of course, if the new value also induces the
condition.
</p>
<p>The code that signals an &ldquo;unassigned variable&rdquo; error can be expected
to supply a <code>use-value</code> restart; this would simply continue the
computation with <var>new-value</var> instead of the value of the variable.
Contrast this with the <code>retry</code> and <code>store-value</code> restarts.  If
the <code>retry</code> restart is used it will fail because the variable still
has no value.  The <code>store-value</code> restart could be used, but it
would alter the value of the variable, so that future references to the
variable would not be detected.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Finding-and-Invoking-General-Restart-Code.html" accesskey="n" rel="next">Finding and Invoking General Restart Code</a>, Previous: <a href="Establishing-Restart-Code.html" accesskey="p" rel="prev">Establishing Restart Code</a>, Up: <a href="Restarts.html" accesskey="u" rel="up">Restarts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
