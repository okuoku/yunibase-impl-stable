<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Lambda Expressions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Lambda Expressions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Lambda Expressions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Lexical-Binding.html" rel="next" title="Lexical Binding">
<link href="Special-Forms.html" rel="prev" title="Special Forms">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Lambda-Expressions"></span><div class="header">
<p>
Next: <a href="Lexical-Binding.html" accesskey="n" rel="next">Lexical Binding</a>, Previous: <a href="Special-Forms.html" accesskey="p" rel="prev">Special Forms</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Lambda-Expressions-1"></span><h3 class="section">2.1 Lambda Expressions</h3>

<dl>
<dt id="index-lambda-4">extended standard special form: <strong>lambda</strong> <em>formals expr expr &hellip;</em></dt>
<dd><span id="index-lambda-expression-_0028defn_0029"></span>
<span id="index-procedure_002c-construction"></span>
<span id="index-procedure_002c-closing-environment-_0028defn_0029"></span>
<span id="index-procedure_002c-invocation-environment-_0028defn_0029"></span>
<span id="index-construction_002c-of-procedure"></span>
<span id="index-closing-environment_002c-of-procedure-_0028defn_0029"></span>
<span id="index-invocation-environment_002c-of-procedure-_0028defn_0029"></span>
<span id="index-environment_002c-of-procedure"></span>
<span id="index-environment_002c-procedure-closing-_0028defn_0029"></span>
<span id="index-environment_002c-procedure-invocation-_0028defn_0029"></span>
<p>A <code>lambda</code> expression evaluates to a procedure.  The environment in
effect when the <code>lambda</code> expression is evaluated is remembered as
part of the procedure; it is called the <em>closing environment</em>.  When
the procedure is later called with some arguments, the closing
environment is extended by binding the variables in the formal parameter
list to fresh locations, and the locations are filled with the arguments
according to rules about to be given.  The new environment created by
this process is referred to as the <em>invocation environment</em>.
</p>
<span id="index-region-of-variable-binding_002c-lambda"></span>
<span id="index-variable-binding_002c-lambda"></span>
<p>Once the invocation environment has been constructed, the <var>expr</var>s
in the body of the <code>lambda</code> expression are evaluated sequentially
in it.  This means that the region of the variables bound by the
<code>lambda</code> expression is all of the <var>expr</var>s in the body.  The
result of evaluating the last <var>expr</var> in the body is returned as
the result of the procedure call.
</p>
<span id="index-lambda-list-_0028defn_0029"></span>
<span id="index-parameter-list_002c-of-lambda-_0028defn_0029"></span>
<span id="index-formal-parameter-list_002c-of-lambda-_0028defn_0029"></span>
<p><var>Formals</var>, the formal parameter list, is often referred to as a
<em>lambda list</em>.
</p>
<p>The process of matching up formal parameters with arguments is somewhat
involved.  There are three types of parameters, and the matching treats
each in sequence:
</p>
<dl compact="compact">
<dt>Required</dt>
<dd><p>All of the <em>required</em> parameters are matched against the arguments
first.  If there are fewer arguments than required parameters, an error
of type <code>condition-type:wrong-number-of-arguments</code> is signalled;
this error is also signalled if there are more arguments than required
parameters and there are no further parameters.
<span id="index-required-parameter-_0028defn_0029"></span>
<span id="index-parameter_002c-required-_0028defn_0029"></span>
<span id="index-condition_002dtype_003awrong_002dnumber_002dof_002darguments"></span>
</p>
</dd>
<dt>Optional</dt>
<dd><p>Once the required parameters have all been matched, the <em>optional</em>
parameters are matched against the remaining arguments.  If there are
fewer arguments than optional parameters, the unmatched parameters are
bound to special objects called <em>default objects</em>.  If there are
more arguments than optional parameters, and there are no further
parameters, an error of type
<code>condition-type:wrong-number-of-arguments</code> is signalled.
<span id="index-optional-parameter-_0028defn_0029"></span>
<span id="index-parameter_002c-optional-_0028defn_0029"></span>
<span id="index-default-object-_0028defn_0029"></span>
<span id="index-condition_002dtype_003awrong_002dnumber_002dof_002darguments-1"></span>
</p>
<span id="index-default_002dobject_003f"></span>
<p>The predicate <code>default-object?</code>, which is true only of default
objects, can be used to determine which optional parameters were
supplied, and which were defaulted.
</p>
</dd>
<dt>Rest</dt>
<dd><p>Finally, if there is a <em>rest</em> parameter (there can only be one), any
remaining arguments are made into a list, and the list is bound to the
rest parameter.  (If there are no remaining arguments, the rest
parameter is bound to the empty list.)
<span id="index-rest-parameter-_0028defn_0029"></span>
<span id="index-parameter_002c-rest-_0028defn_0029"></span>
</p>
<p>In Scheme, unlike some other Lisp implementations, the list to which a
rest parameter is bound is always freshly allocated.  It has infinite
extent and may be modified without affecting the procedure&rsquo;s caller.
</p></dd>
</dl>

<span id="index-_0023_0021optional-1"></span>
<span id="index-_0023_0021rest-1"></span>
<p>Specially recognized keywords divide the <var>formals</var> parameters into
these three classes.  The keywords used here are &lsquo;<samp>#!optional</samp>&rsquo;,
&lsquo;<samp>.</samp>&rsquo;, and &lsquo;<samp>#!rest</samp>&rsquo;.  Note that only &lsquo;<samp>.</samp>&rsquo; is defined by
standard Scheme &mdash; the other keywords are MIT/GNU Scheme extensions.
&lsquo;<samp>#!rest</samp>&rsquo; has the same meaning as &lsquo;<samp>.</samp>&rsquo; in <var>formals</var>.
</p>
<p>The use of these keywords is best explained by means of examples.  The
following are typical lambda lists, followed by descriptions of which
parameters are required, optional, and rest.  We will use &lsquo;<samp>#!rest</samp>&rsquo;
in these examples, but anywhere it appears &lsquo;<samp>.</samp>&rsquo; could be used
instead.
</p>
<dl compact="compact">
<dt><code>(a b c)</code></dt>
<dd><p><code>a</code>, <code>b</code>, and <code>c</code> are all required.  The procedure must
be passed exactly three arguments.
</p>
</dd>
<dt><code>(a b #!optional c)</code></dt>
<dd><p><code>a</code> and <code>b</code> are required, <code>c</code> is optional.  The procedure
may be passed either two or three arguments.
</p>
</dd>
<dt><code>(#!optional a b c)</code></dt>
<dd><p><code>a</code>, <code>b</code>, and <code>c</code> are all optional.  The procedure may be
passed any number of arguments between zero and three, inclusive.
</p>
</dd>
<dt><code>a</code></dt>
<dt><code>(#!rest a)</code></dt>
<dd><p>These two examples are equivalent.  <code>a</code> is a rest parameter.  The
procedure may be passed any number of arguments.  Note: this is the only
case in which &lsquo;<samp>.</samp>&rsquo; cannot be used in place of &lsquo;<samp>#!rest</samp>&rsquo;.
</p>
</dd>
<dt><code>(a b #!optional c d #!rest e)</code></dt>
<dd><p><code>a</code> and <code>b</code> are required, <code>c</code> and <code>d</code> are optional,
and <code>e</code> is rest.  The procedure may be passed two or more
arguments.
</p></dd>
</dl>

<p>Some examples of <code>lambda</code> expressions:
</p>
<div class="example">
<pre class="example">(lambda (x) (+ x x))            &rArr;  #[compound-procedure 53]

((lambda (x) (+ x x)) 4)                &rArr;  8

(define reverse-subtract
  (lambda (x y)
    (- y x)))
(reverse-subtract 7 10)                 &rArr;  3

(define foo
  (let ((x 4))
    (lambda (y) (+ x y))))
(foo 6)                                 &rArr;  10
</pre></div>
</dd></dl>

<dl>
<dt id="index-named_002dlambda">special form: <strong>named-lambda</strong> <em>formals expression expression &hellip;</em></dt>
<dd><span id="index-named-lambda-_0028defn_0029"></span>
<p>The <code>named-lambda</code> special form is similar to <code>lambda</code>, except
that the first &ldquo;required parameter&rdquo; in <var>formals</var> is not a
parameter but the <em>name</em> of the resulting procedure; thus
<var>formals</var> must have at least one required parameter.  This name has
no semantic meaning, but is included in the external representation of
the procedure, making it useful for debugging.  In MIT/GNU Scheme,
<code>lambda</code> is implemented as <code>named-lambda</code>, with a special name
that means &ldquo;unnamed&rdquo;.
</p>
<div class="example">
<pre class="example">(named-lambda (f x) (+ x x))    &rArr;  #[compound-procedure 53 f]
((named-lambda (f x) (+ x x)) 4)        &rArr;  8
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Lexical-Binding.html" accesskey="n" rel="next">Lexical Binding</a>, Previous: <a href="Special-Forms.html" accesskey="p" rel="prev">Special Forms</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
