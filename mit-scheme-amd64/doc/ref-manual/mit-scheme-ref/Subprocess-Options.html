<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Subprocess Options (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Subprocess Options (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Subprocess Options (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Subprocesses.html" rel="up" title="Subprocesses">
<link href="TCP-Sockets.html" rel="next" title="TCP Sockets">
<link href="Subprocess-Conditions.html" rel="prev" title="Subprocess Conditions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Subprocess-Options"></span><div class="header">
<p>
Previous: <a href="Subprocess-Conditions.html" accesskey="p" rel="prev">Subprocess Conditions</a>, Up: <a href="Subprocesses.html" accesskey="u" rel="up">Subprocesses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Subprocess-Options-1"></span><h4 class="subsection">15.7.3 Subprocess Options</h4>

<p>The following subprocess options may be passed to
<code>run-shell-command</code> or <code>run-synchronous-subprocess</code>.  These
options are passed as alternating keyword/value pairs, for example:
</p>
<div class="example">
<pre class="example">(run-shell-command &quot;ls /&quot;
                   'output my-output-port
                   'output-buffer-size 8192)
</pre></div>

<p>The example shows a shell command being run with two options specified:
<code>output</code> and <code>output-buffer-size</code>.
</p>
<dl>
<dt id="index-input-1">subprocess option: <strong>input</strong> <em>port</em></dt>
<dd><p>Specifies the standard input of the subprocess.  <var>Port</var> may be an
input port, in which case characters are read from <var>port</var> and fed to
the subprocess until <var>port</var> reaches end-of-file.  Alternatively,
<var>port</var> may be <code>#f</code>, indicating that the subprocess has no
standard input.
</p>
<p>The default value of this option is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(call-with-input-file &quot;foo.in&quot;
  (lambda (port)
    (run-shell-command &quot;cat &gt; /dev/null&quot; 'input port)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-input_002dline_002dtranslation">subprocess option: <strong>input-line-translation</strong> <em>line-ending</em></dt>
<dd><p>Specifies how line-endings should be translated when writing characters
to the subprocess.  Ignored if the <code>input</code> option is <code>#f</code>.
<var>Line-ending</var> must be either a string specifying the line ending, or
the symbol <code>default</code>, meaning to use the operating system&rsquo;s
standard line ending.  In either case, newline characters to be written
to the <code>input</code> port are translated to the specified line ending
before being written.
</p>
<p>The default value of this option is <code>default</code>.
</p>
<div class="example">
<pre class="example">(call-with-input-file &quot;foo.in&quot;
  (lambda (port)
    (run-shell-command &quot;cat &gt; /dev/null&quot;
                       'input port
                       'input-line-translation &quot;\r\n&quot;)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-input_002dbuffer_002dsize">subprocess option: <strong>input-buffer-size</strong> <em>n</em></dt>
<dd><p>Specifies the size of the input buffer for the standard input of the
subprocess.  (This is the buffer on the Scheme side, and has nothing to
do with any buffering done on the subprocess side.)  Ignored if the
<code>input</code> option is <code>#f</code>.  <var>N</var> must be an exact positive
integer specifying the number of characters the buffer can hold.
</p>
<p>The default value of this option is <code>512</code>.
</p>
<div class="example">
<pre class="example">(call-with-input-file &quot;foo.in&quot;
  (lambda (port)
    (run-shell-command &quot;cat &gt; /dev/null&quot;
                       'input port
                       'input-buffer-size 4096)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-output-1">subprocess option: <strong>output</strong> <em>port</em></dt>
<dd><p>Specifies the standard output and standard error of the subprocess.
<var>Port</var> may be an output port, in which case characters are read from
the subprocess and fed to <var>port</var> until the subprocess finishes.
Alternatively, <var>port</var> may be <code>#f</code>, indicating that the
subprocess has no standard output or standard error.
</p>
<p>The default value of this option is the value of
<code>(current-output-port)</code>.
</p>
<div class="example">
<pre class="example">(call-with-output-file &quot;foo.out&quot;
  (lambda (port)
    (run-shell-command &quot;ls -la /etc&quot; 'output port)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-output_002dline_002dtranslation">subprocess option: <strong>output-line-translation</strong> <em>line-ending</em></dt>
<dd><p>Specifies how line-endings should be translated when reading characters
from the standard output of the subprocess.  Ignored if the
<code>output</code> option is <code>#f</code>.  <var>Line-ending</var> must be either a
string specifying the line ending, or the symbol <code>default</code>, meaning
to use the operating system&rsquo;s standard line ending.  In either case,
newline characters read from the subprocess port are translated to the
specified line ending.
</p>
<p>The default value of this option is <code>default</code>.
</p>
<div class="example">
<pre class="example">(call-with-output-file &quot;foo.out&quot;
  (lambda (port)
    (run-shell-command &quot;ls -la /etc&quot;
                       'output port
                       'output-line-translation &quot;\r\n&quot;)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-output_002dbuffer_002dsize">subprocess option: <strong>output-buffer-size</strong> <em>n</em></dt>
<dd><p>Specifies the size of the output buffer for the standard output of the
subprocess.  (This is the buffer on the Scheme side, and has nothing to
do with any buffering done on the subprocess side.)  Ignored if the
<code>output</code> option is <code>#f</code>.  <var>N</var> must be an exact positive
integer specifying the number of characters the buffer can hold.
</p>
<p>The default value of this option is <code>512</code>.
</p>
<div class="example">
<pre class="example">(call-with-output-file &quot;foo.out&quot;
  (lambda (port)
    (run-shell-command &quot;ls -la /etc&quot;
                       'output port
                       'output-buffer-size 4096)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-redisplay_002dhook">subprocess option: <strong>redisplay-hook</strong> <em>thunk</em></dt>
<dd><p>Specifies that <var>thunk</var> is to be run periodically when output from
the subprocess is available.  <var>Thunk</var> must be a procedure of no
arguments, or <code>#f</code> indicating that no hook is supplied.  This
option is mostly useful for interactive systems.  For example, the Edwin
text editor uses this to update output buffers when running some
subprocesses.
</p>
<p>The default value of this option is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(run-shell-command &quot;ls -la /etc&quot;
                   'redisplay-hook
                   (lambda ()
                     (update-buffer-contents buffer)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-environment">subprocess option: <strong>environment</strong> <em>environment</em></dt>
<dd><p>Specifies the environment variables that are to be used for the
subprocess.  <var>Environment</var> must be either a vector of strings or
<code>#f</code> indicating the default environment.  If it is a vector of
strings, each string must be a name/value pair where the name and value
are separated by an equal sign, for example, <code>&quot;foo=bar&quot;</code>.  To
define a variable with no value, just omit the value, as in <code>&quot;foo=&quot;</code>.
</p>
<span id="index-scheme_002dsubprocess_002denvironment"></span>
<p>Note that the variable <code>scheme-subprocess-environment</code> is bound to
the default subprocess environment.
</p>
<p>The default value of this option is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(run-shell-command &quot;ls -la /etc&quot;
                   'environment
                   (let* ((v scheme-subprocess-environment)
                          (n (vector-length v))
                          (v (vector-grow v (+ n 1))))
                     (vector-set! v n &quot;TERM=none&quot;)
                     v))
</pre></div>
</dd></dl>

<dl>
<dt id="index-working_002ddirectory">subprocess option: <strong>working-directory</strong> <em>pathname</em></dt>
<dd><p>Specifies the working directory in which the subprocess will run.
</p>
<p>The default value of this option is <code>(working-directory-pathname)</code>.
</p>
<div class="example">
<pre class="example">(run-shell-command &quot;ls -la&quot; 'working-directory &quot;/etc/&quot;)
</pre></div>
</dd></dl>

<dl>
<dt id="index-use_002dpty_003f">subprocess option: <strong>use-pty?</strong> <em>boolean</em></dt>
<dd><p>This option is meaningful only on unix systems; on other systems it is
ignored.  Specifies whether to communicate with the subprocess using
<small>PTY</small> devices; if true, <small>PTY</small>s will be used, otherwise pipes will
be used.
</p>
<p>The default value of this option is <code>#f</code>.
</p>
<div class="example">
<pre class="example">(run-shell-command &quot;ls -la /etc&quot; 'use-pty? #t)
</pre></div>
</dd></dl>

<dl>
<dt id="index-shell_002dfile_002dname">subprocess option: <strong>shell-file-name</strong> <em>pathname</em></dt>
<dd><p>Specifies the shell program to use for <code>run-shell-command</code>.
</p>
<p>The default value of this option is <code>(os/shell-file-name)</code>.  This
is the value of the environment variable <code>SHELL</code>, or if
<code>SHELL</code> is not set, the value is operating-system dependent as
follows:
</p>
<ul>
<li> On unix systems, <samp>/bin/sh</samp> is used.

</li><li> On Windows systems, the value of the environment variable <code>COMSPEC</code>
is used.  If that is not set, <samp>cmd.exe</samp> is used for Windows NT, or
<samp>command.com</samp> is used for Windows 9x; in each case the shell is
found by searching the path.
</li></ul>

<div class="example">
<pre class="example">(run-shell-command &quot;ls -la /etc&quot;
                   'shell-file-name &quot;/usr/local/bin/bash&quot;)
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Subprocess-Conditions.html" accesskey="p" rel="prev">Subprocess Conditions</a>, Up: <a href="Subprocesses.html" accesskey="u" rel="up">Subprocesses</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
