<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Implementation restrictions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Implementation restrictions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Implementation restrictions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Numbers.html" rel="up" title="Numbers">
<link href="Syntax-of-numerical-constants.html" rel="next" title="Syntax of numerical constants">
<link href="Exactness.html" rel="prev" title="Exactness">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Implementation-restrictions"></span><div class="header">
<p>
Next: <a href="Syntax-of-numerical-constants.html" accesskey="n" rel="next">Syntax of numerical constants</a>, Previous: <a href="Exactness.html" accesskey="p" rel="prev">Exactness</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Implementation-restrictions-1"></span><h3 class="section">4.3 Implementation restrictions</h3>
<span id="index-implementation-restriction"></span>

<p>Implementations of Scheme are not required to implement the whole tower
of subtypes (see <a href="Numerical-types.html">Numerical types</a>), but they must implement a
coherent subset consistent with both the purposes of the implementation
and the spirit of the Scheme language.  For example, an implementation
in which all numbers are real may still be quite useful.<a id="DOCF1" href="#FOOT1"><sup>1</sup></a>
</p>
<p>Implementations may also support only a limited range of numbers of any
type, subject to the requirements of this section.  The supported range
for exact numbers of any type may be different from the supported range
for inexact numbers of that type.  For example, an implementation that
uses flonums to represent all its inexact real numbers may support a
practically unbounded range of exact integers and rationals while
limiting the range of inexact reals (and therefore the range of inexact
integers and rationals) to the dynamic range of the flonum format.
Furthermore the gaps between the representable inexact integers and
rationals are likely to be very large in such an implementation as the
limits of this range are approached.
</p>
<span id="index-length"></span>
<span id="index-vector_002dlength"></span>
<span id="index-string_002dlength"></span>
<p>An implementation of Scheme must support exact integers throughout the
range of numbers that may be used for indexes of lists, vectors, and
strings or that may result from computing the length of a list, vector,
or string.  The <code>length</code>, <code>vector-length</code>, and
<code>string-length</code> procedures must return an exact integer, and it is
an error to use anything but an exact integer as an index.  Furthermore
any integer constant within the index range, if expressed by an exact
integer syntax, will indeed be read as an exact integer, regardless of
any implementation restrictions that may apply outside this range.
Finally, the procedures listed below will always return an exact integer
result provided all their arguments are exact integers and the
mathematically expected result is representable as an exact integer
within the implementation:
</p>
<div class="example">
<pre class="example">*                gcd                modulo
+                imag-part          numerator
-                exact              quotient
abs              lcm                rationalize
angle            magnitude          real-part
ceiling          make-polar         remainder
denominator      make-rectangular   round
expt             max                truncate
floor            min
</pre></div>

<span id="index-_002f"></span>
<p>Implementations are encouraged, but not required, to support exact
integers and exact rationals of practically unlimited size and
precision, and to implement the above procedures and the <code>/</code>
procedure in such a way that they always return exact results when given
exact arguments.  If one of these procedures is unable to deliver an
exact result when given exact arguments, then it may either report a
violation of an implementation restriction or it may silently coerce its
result to an inexact number.  Such a coercion may cause an error
later.
</p>
<p>An implementation may use floating point and other approximate
representation strategies for inexact numbers.  This report recommends,
but does not require, that the <small>IEEE</small> 32-bit and 64-bit floating
point standards be followed by implementations that use flonum
representations, and that implementations using other representations
should match or exceed the precision achievable using these floating
point standards.
</p>
<span id="index-sqrt"></span>
<p>In particular, implementations that use flonum representations must
follow these rules: A flonum result must be represented with at least as
much precision as is used to express any of the inexact arguments to
that operation.  It is desirable (but not required) for potentially
inexact operations such as <code>sqrt</code>, when applied to exact arguments,
to produce exact answers whenever possible (for example the square root
of an exact 4 ought to be an exact 2).  If, however, an exact number is
operated upon so as to produce an inexact result (as by <code>sqrt</code>),
and if the result is represented as a flonum, then the most precise
flonum format available must be used; but if the result is represented
in some other way then the representation must have at least as much
precision as the most precise flonum format available.
</p>
<p>Although Scheme allows a variety of written notations for numbers, any
particular implementation may support only some of them.<a id="DOCF2" href="#FOOT2"><sup>2</sup></a> For
example, an implementation in which all numbers are real need not
support the rectangular and polar notations for complex numbers.  If an
implementation encounters an exact numerical constant that it cannot
represent as an exact number, then it may either report a violation of
an implementation restriction or it may silently represent the constant
by an inexact number.
</p>
<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT1" href="#DOCF1">(1)</a></h3>
<p>MIT/GNU
Scheme implements the whole tower of numerical types.  It has
unlimited-precision exact integers and exact rationals.  Flonums are
used to implement all inexact reals; on machines that support <small>IEEE</small>
floating-point arithmetic these are double-precision floating-point
numbers.</p>
<h5><a id="FOOT2" href="#DOCF2">(2)</a></h3>
<p>MIT/GNU
Scheme implements all of the written notations for numbers.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Syntax-of-numerical-constants.html" accesskey="n" rel="next">Syntax of numerical constants</a>, Previous: <a href="Exactness.html" accesskey="p" rel="prev">Exactness</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
