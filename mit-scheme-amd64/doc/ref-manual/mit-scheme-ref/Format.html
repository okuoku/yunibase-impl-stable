<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Format (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Format (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Format (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Custom-Output.html" rel="next" title="Custom Output">
<link href="Terminal-Mode.html" rel="prev" title="Terminal Mode">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Format"></span><div class="header">
<p>
Next: <a href="Custom-Output.html" accesskey="n" rel="next">Custom Output</a>, Previous: <a href="Terminal-Mode.html" accesskey="p" rel="prev">Terminal Mode</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Format-1"></span><h3 class="section">14.9 Format</h3>


<p>The procedure <code>format</code> is very useful for producing nicely
formatted text, producing good-looking messages, and so on.  MIT/GNU
Scheme&rsquo;s implementation of <code>format</code> is similar to that of Common
Lisp, except that Common Lisp defines many more
directives.<a id="DOCF14" href="#FOOT14"><sup>14</sup></a>
</p>
<span id="index-run_002dtime_002dloadable-option"></span>
<span id="index-option_002c-run_002dtime_002dloadable"></span>
<p><code>format</code> is a run-time-loadable option.  To use it, execute
</p>
<div class="example">
<pre class="example">(load-option 'format)
</pre></div>
<span id="index-load_002doption"></span>

<p>once before calling it.
</p>
<dl>
<dt id="index-format">procedure: <strong>format</strong> <em>destination control-string argument &hellip;</em></dt>
<dd><span id="index-write_002dstring-1"></span>
<span id="index-format-directive-_0028defn_0029"></span>
<span id="index-directive_002c-format-_0028defn_0029"></span>
<p>Writes the characters of <var>control-string</var> to <var>destination</var>,
except that a tilde (<code>~</code>) introduces a <em>format directive</em>.  The
character after the tilde, possibly preceded by prefix parameters and
modifiers, specifies what kind of formatting is desired.  Most
directives use one or more <var>argument</var>s to create their output; the
typical directive puts the next <var>argument</var> into the output,
formatted in some special way.  It is an error if no argument remains
for a directive requiring an argument, but it is not an error if one or
more arguments remain unprocessed by a directive.
</p>
<p>The output is sent to <var>destination</var>.  If <var>destination</var> is
<code>#f</code>, a string is created that contains the output; this string is
returned as the value of the call to <code>format</code>.  In all other cases
<code>format</code> returns an unspecified value.  If <var>destination</var> is
<code>#t</code>, the output is sent to the current output port.  Otherwise,
<var>destination</var> must be an output port, and the output is sent there.
</p>
<p>This procedure performs discretionary output flushing (see <a href="Output-Procedures.html">Output Procedures</a>).
</p>
<p>A <code>format</code> directive consists of a tilde (<code>~</code>), optional
prefix parameters separated by commas, optional colon (<code>:</code>) and
at-sign (<code>@</code>) modifiers, and a single character indicating what
kind of directive this is.  The alphabetic case of the directive
character is ignored.  The prefix parameters are generally integers,
notated as optionally signed decimal numbers.  If both the colon and
at-sign modifiers are given, they may appear in either order.
</p>
<span id="index-V-as-format-parameter"></span>
<span id="index-_0023-as-format-parameter"></span>
<p>In place of a prefix parameter to a directive, you can put the letter
&lsquo;<samp>V</samp>&rsquo; (or &lsquo;<samp>v</samp>&rsquo;), which takes an <var>argument</var> for use as a
parameter to the directive.  Normally this should be an exact integer.
This feature allows variable-width fields and the like.  You can also
use the character &lsquo;<samp>#</samp>&rsquo; in place of a parameter; it represents the
number of arguments remaining to be processed.
</p>
<p>It is an error to give a format directive more parameters than it is
described here as accepting.  It is also an error to give colon or
at-sign modifiers to a directive in a combination not specifically
described here as being meaningful.
</p>
<dl compact="compact">
<dt><code>~A</code></dt>
<dd><p>The next <var>argument</var>, which may be any object, is printed as if by
<code>display</code>.  <code>~<var>mincol</var>A</code> inserts spaces on the right, if
necessary, to make the width at least <var>mincol</var> columns.  The
<code>@</code> modifier causes the spaces to be inserted on the left rather
than the right.
</p>
</dd>
<dt><code>~S</code></dt>
<dd><p>The next <var>argument</var>, which may be any object, is printed as if by
<code>write</code>.  <code>~<var>mincol</var>S</code> inserts spaces on the right, if
necessary, to make the width at least <var>mincol</var> columns.  The
<code>@</code> modifier causes the spaces to be inserted on the left rather
than the right.
</p>
</dd>
<dt><code>~%</code></dt>
<dd><p>This outputs a <code>#\newline</code> character.  <code>~<var>n</var>%</code> outputs
<var>n</var> newlines.  No <var>argument</var> is used.  Simply putting a newline
in <var>control-string</var> would work, but <code>~%</code> is often used because
it makes the control string look nicer in the middle of a program.
</p>
</dd>
<dt><code>~~</code></dt>
<dd><p>This outputs a tilde.  <code>~<var>n</var>~</code> outputs <var>n</var> tildes.
</p>
</dd>
<dt><code>~<var>newline</var></code></dt>
<dd><p>Tilde immediately followed by a newline ignores the newline and any
following non-newline whitespace characters.  With an <code>@</code>, the
newline is left in place, but any following whitespace is ignored.  This
directive is typically used when <var>control-string</var> is too long to fit
nicely into one line of the program:
</p>
<div class="example">
<pre class="example">(define (type-clash-error procedure arg spec actual)
  (format
   #t
   &quot;~%Procedure ~S~%requires its %A argument ~
    to be of type ~S,~%but it was called with ~
    an argument of type ~S.~%&quot;
   procedure arg spec actual))
</pre></div>

<div class="example">
<pre class="example">(type-clash-error 'vector-ref
                  &quot;first&quot;
                  'integer
                  'vector)

<span class="roman">prints</span>

Procedure vector-ref
requires its first argument to be of type integer,
but it was called with an argument of type vector.
</pre></div>

<p>Note that in this example newlines appear in the output only as
specified by the <code>~%</code> directives; the actual newline characters in
the control string are suppressed because each is preceded by a tilde.
</p></dd>
</dl>
</dd></dl>


<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT14" href="#DOCF14">(14)</a></h3>
<p>This description of <code>format</code> is adapted from
<cite>Common Lisp, The Language</cite>, second edition, section 22.3.3.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Custom-Output.html" accesskey="n" rel="next">Custom Output</a>, Previous: <a href="Terminal-Mode.html" accesskey="p" rel="prev">Terminal Mode</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
