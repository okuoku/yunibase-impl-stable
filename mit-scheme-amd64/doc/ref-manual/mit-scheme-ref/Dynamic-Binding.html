<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Dynamic Binding (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Dynamic Binding (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Dynamic Binding (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Definitions.html" rel="next" title="Definitions">
<link href="Lexical-Binding.html" rel="prev" title="Lexical Binding">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Dynamic-Binding"></span><div class="header">
<p>
Next: <a href="Definitions.html" accesskey="n" rel="next">Definitions</a>, Previous: <a href="Lexical-Binding.html" accesskey="p" rel="prev">Lexical Binding</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Dynamic-Binding-1"></span><h3 class="section">2.3 Dynamic Binding</h3>

<span id="parameterize"></span><dl>
<dt id="index-parameterize">standard special form: <strong>parameterize</strong> <em>((<var>parameter</var> <var>value</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><p>Note that both <var>parameter</var> and <var>value</var> are expressions.
It is an error if the value of any <var>parameter</var> expression is not a
parameter object.
</p>
<p>A <code>parameterize</code> expression is used to change the values of
specified parameter objects during the evaluation of the body
<var>expression</var>s.
</p>
<p>The <var>parameter</var> and <var>value</var> expressions are evaluated in an
unspecified order. The body is evaluated in a dynamic
environment in which each <var>parameter</var> is bound to the converted
<var>value</var>&mdash;the result of passing <var>value</var> to the conversion
procedure specified when the <var>parameter</var> was created.
Then the previous value of <var>parameter</var> is restored
without passing it to the conversion procedure.
The value of the parameterize expression is the value of the last
body <var>expr</var>.
</p>
<p>The <code>parameterize</code> special form is standardized by <a href="https://srfi.schemers.org/srfi-39/srfi-39.html"><acronym>SRFI</acronym> 39</a> and
by <a href="https://small.r7rs.org/attachment/r7rs.pdf"><acronym>R7RS</acronym></a>.
</p></dd></dl>

<p>Parameter objects can be used to specify configurable settings for a
computation without the need to pass the value to every procedure in
the call chain explicitly.
</p>
<div class="example">
<pre class="example">(define radix
  (make-parameter
   10
   (lambda (x)
     (if (and (exact-integer?  x) (&lt;= 2 x 16))
         x
         (error &quot;invalid radix&quot;)))))
</pre><pre class="example">

(define (f n) (number-&gt;string n (radix)))

</pre><pre class="example">(f 12)                                  &rArr; &quot;12&quot;
(parameterize ((radix 2))
  (f 12))                               &rArr; &quot;1100&quot;
(f 12)                                  &rArr; &quot;12&quot;
(radix 16)                              error&rarr; Wrong number of arguments
(parameterize ((radix 0))
  (f 12))                               error&rarr; invalid radix
</pre></div>

<span id="index-binding-expression_002c-dynamic"></span>
<span id="index-dynamic-binding"></span>
<span id="index-dynamic-environment"></span>
<span id="index-dynamic-extent"></span>
<p>A <em>dynamic binding</em> changes the value of a parameter
(see <a href="Parameters.html">Parameters</a>) object temporarily, for a <em>dynamic extent</em>.
The set of all dynamic bindings at a given time is called the
<em>dynamic environment</em>.  The new values are only accessible to the
thread that constructed the dynamic environment, and any threads
created within that environment.
</p>
<span id="index-extent_002c-of-dynamic-binding-_0028defn_0029"></span>
<p>The <em>extent</em> of a dynamic binding is defined to be the time period
during which calling the parameter returns the new value.  Normally
this time period begins when the body is entered and ends when it is
exited, a contiguous time period.  However Scheme has first-class
continuations by which it is possible to leave the body and reenter it
many times.  In this situation, the extent is non-contiguous.
</p>
<span id="index-dynamic-binding_002c-and-continuations"></span>
<span id="index-continuation_002c-and-dynamic-binding"></span>
<p>When the body is exited by invoking a continuation, the current
dynamic environment is unwound until it can be re-wound to the
environment captured by the continuation.  When the continuation
returns, the process is reversed, restoring the original dynamic
environment.
</p>
<p>The following example shows the interaction between dynamic binding
and continuations.  Side effects to the binding that occur both inside
and outside of the body are preserved, even if continuations are used
to jump in and out of the body repeatedly.
</p>
<div class="example">
<pre class="example">(define (complicated-dynamic-parameter)
  (let ((variable (make-settable-parameter 1))
        (inside-continuation))
    (write-line (variable))
    (call-with-current-continuation
     (lambda (outside-continuation)
       (parameterize ((variable 2))
         (write-line (variable))
         (variable 3)
         (call-with-current-continuation
          (lambda (k)
            (set! inside-continuation k)
            (outside-continuation #t)))
         (write-line (variable))
         (set! inside-continuation #f))))
    (write-line (variable))
    (if inside-continuation
        (begin
          (variable 4)
          (inside-continuation #f)))))
</pre></div>

<p>Evaluating &lsquo;<samp>(complicated-dynamic-binding)</samp>&rsquo; writes the following on
the console:
</p>
<div class="example">
<pre class="example">1
2
1
3
4
</pre></div>

<p>Commentary: the first two values written are the initial binding of
<code>variable</code> and its new binding inside <code>parameterize</code>&rsquo;s body.
Immediately after they are written, the binding visible in the body
is set to &lsquo;<samp>3</samp>&rsquo;, and <code>outside-continuation</code> is invoked,
exiting the body.  At this point, &lsquo;<samp>1</samp>&rsquo; is written, demonstrating
that the original binding of <code>variable</code> is still visible outside
the body.  Then we set <code>variable</code> to &lsquo;<samp>4</samp>&rsquo; and reenter the
body by invoking <code>inside-continuation</code>.  At this point, &lsquo;<samp>3</samp>&rsquo;
is written, indicating that the binding modified in the body is still
the binding visible in the body.  Finally, we exit the body
normally, and write &lsquo;<samp>4</samp>&rsquo;, demonstrating that the binding modified
outside of the body was also preserved.
</p>
<span id="Fluid_002dLet"></span><h4 class="subsection">2.3.1 Fluid-Let</h4>

<p>The <code>fluid-let</code> special form can change the value of <em>any</em>
variable for a dynamic extent, but it is difficult to implement in a
multi-processing (SMP) world.  It and the cell object type
(see <a href="Parameters.html#Cells">Cells</a>) are now <strong>deprecated</strong>.  They are still available
and functional in a uni-processing (non-SMP) world, but will signal an
error when used in an SMP world.  The <code>parameterize</code> special form
(see <a href="#parameterize">parameterize</a>) should be used instead.
</p>
<dl>
<dt id="index-fluid_002dlet">special form: <strong>fluid-let</strong> <em>((<var>variable</var> <var>init</var>) &hellip;) expression expression &hellip;</em></dt>
<dd><span id="index-variable-binding_002c-fluid_002dlet"></span>
<p>The <var>init</var>s are evaluated in the current environment (in some
unspecified order), the current values of the <var>variable</var>s are saved,
the results are assigned to the <var>variable</var>s, the <var>expression</var>s
are evaluated sequentially in the current environment, the
<var>variable</var>s are restored to their original values, and the value of
the last <var>expression</var> is returned.
</p>
<span id="index-let-2"></span>
<p>The syntax of this special form is similar to that of <code>let</code>, but
<code>fluid-let</code> temporarily rebinds existing variables.  Unlike
<code>let</code>, <code>fluid-let</code> creates no new bindings; instead it
<em>assigns</em> the value of each <var>init</var> to the binding (determined
by the rules of lexical scoping) of its corresponding <var>variable</var>.
</p>
<span id="index-unassigned-variable_002c-and-dynamic-bindings"></span>
<p>MIT/GNU Scheme allows any of the <var>init</var>s to be omitted, in which
case the corresponding <var>variable</var>s are temporarily unassigned.
</p>
<p>An error of type <code>condition-type:unbound-variable</code> is signalled if
any of the <var>variable</var>s are unbound.  However, because
<code>fluid-let</code> operates by means of side effects, it is valid for any
<var>variable</var> to be unassigned when the form is entered.
<span id="index-condition_002dtype_003aunbound_002dvariable-1"></span>
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Definitions.html" accesskey="n" rel="next">Definitions</a>, Previous: <a href="Lexical-Binding.html" accesskey="p" rel="prev">Lexical Binding</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
