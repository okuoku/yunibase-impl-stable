<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Lexical Binding (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Lexical Binding (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Lexical Binding (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Dynamic-Binding.html" rel="next" title="Dynamic Binding">
<link href="Lambda-Expressions.html" rel="prev" title="Lambda Expressions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Lexical-Binding"></span><div class="header">
<p>
Next: <a href="Dynamic-Binding.html" accesskey="n" rel="next">Dynamic Binding</a>, Previous: <a href="Lambda-Expressions.html" accesskey="p" rel="prev">Lambda Expressions</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Lexical-Binding-1"></span><h3 class="section">2.2 Lexical Binding</h3>

<span id="index-lexical-binding-expression"></span>
<span id="index-binding-expression_002c-lexical"></span>
<span id="index-block-structure"></span>
<p>The binding constructs <code>let</code>, <code>let*</code>, <code>letrec</code>,
<code>letrec*</code>, <code>let-values</code>, and <code>let*-values</code> give Scheme
block structure, like Algol 60.  The syntax of the first four
constructs is identical, but they differ in the regions they establish
for their variable bindings.  In a <code>let</code> expression, the initial
values are computed before any of the variables become bound; in a
<code>let*</code> expression, the bindings and evaluations are performed
sequentially; while in <code>letrec</code> and <code>letrec*</code> expressions,
all the bindings are in effect while their initial values are being
computed, thus allowing mutually recursive definitions.  The
<code>let-values</code> and <code>let*-values</code> constructs are analogous to
<code>let</code> and <code>let*</code> respectively, but are designed to handle
multiple-valued expressions, binding different identifiers to the
returned values.
</p>
<dl>
<dt id="index-let-1">extended standard special form: <strong>let</strong> <em>((<var>variable</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><span id="index-region-of-variable-binding_002c-let"></span>
<span id="index-variable-binding_002c-let"></span>
<p>The <var>init</var>s are evaluated in the current environment (in some
unspecified order), the <var>variable</var>s are bound to fresh locations
holding the results, the <var>expr</var>s are evaluated sequentially in
the extended environment, and the value of the last <var>expr</var> is
returned.  Each binding of a <var>variable</var> has the <var>expr</var>s as
its region.
</p>
<p>MIT/GNU Scheme allows any of the <var>init</var>s to be omitted, in which
case the corresponding <var>variable</var>s are unassigned.
</p>
<span id="index-lambda_002c-implicit-in-let"></span>
<p>Note that the following are equivalent:
</p>
<div class="example">
<pre class="example">(let ((<var>variable</var> <var>init</var>) &hellip;) <var>expr</var> <var>expr</var> &hellip;)
((lambda (<var>variable</var> &hellip;) <var>expr</var> <var>expr</var> &hellip;) <var>init</var> &hellip;)
</pre></div>

<p>Some examples:
</p>
<div class="example">
<pre class="example">(let ((x 2) (y 3))
  (* x y))                              &rArr;  6
</pre><pre class="example">

</pre><pre class="example">(let ((x 2) (y 3))
  (let ((foo (lambda (z) (+ x y z)))
        (x 7))
    (foo 4)))                           &rArr;  9
</pre></div>

<p>See <a href="Iteration.html">Iteration</a>, for information on &ldquo;named <code>let</code>&rdquo;.
</p></dd></dl>

<dl>
<dt id="index-let_002a-1">extended standard special form: <strong>let*</strong> <em>((<var>variable</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><span id="index-region-of-variable-binding_002c-let_002a"></span>
<span id="index-variable-binding_002c-let_002a"></span>
<p><code>let*</code> is similar to <code>let</code>, but the bindings are performed
sequentially from left to right, and the region of a binding is that
part of the <code>let*</code> expression to the right of the binding.  Thus
the second binding is done in an environment in which the first binding
is visible, and so on.
</p>
<p>Note that the following are equivalent:
</p>
<div class="example">
<pre class="example">(let* ((<var>variable1</var> <var>init1</var>)
       (<var>variable2</var> <var>init2</var>)
       &hellip;
       (<var>variableN</var> <var>initN</var>))
   <var>expr</var>
   <var>expr</var> &hellip;)
</pre><pre class="example">

</pre><pre class="example">(let ((<var>variable1</var> <var>init1</var>))
  (let ((<var>variable2</var> <var>init2</var>))
    &hellip;
      (let ((<var>variableN</var> <var>initN</var>))
        <var>expr</var>
        <var>expr</var> &hellip;)
    &hellip;))
</pre></div>

<p>An example:
</p>
<div class="example">
<pre class="example">(let ((x 2) (y 3))
  (let* ((x 7)
         (z (+ x y)))
    (* z x)))                           &rArr;  70
</pre></div>
</dd></dl>

<dl>
<dt id="index-letrec-1">extended standard special form: <strong>letrec</strong> <em>((<var>variable</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><span id="index-region-of-variable-binding_002c-letrec"></span>
<span id="index-variable-binding_002c-letrec"></span>
<p>The <var>variable</var>s are bound to fresh locations holding unassigned
values, the <var>init</var>s are evaluated in the extended environment (in
some unspecified order), each <var>variable</var> is assigned to the result
of the corresponding <var>init</var>, the <var>expr</var>s are evaluated
sequentially in the extended environment, and the value of the last
<var>expr</var> is returned.  Each binding of a <var>variable</var> has the
entire <code>letrec</code> expression as its region, making it possible to
define mutually recursive procedures.
</p>
<p>MIT/GNU Scheme allows any of the <var>init</var>s to be omitted, in which
case the corresponding <var>variable</var>s are unassigned.
</p>
<div class="example">
<pre class="example">(letrec ((even?
          (lambda (n)
            (if (zero? n)
                #t
                (odd? (- n 1)))))
         (odd?
          (lambda (n)
            (if (zero? n)
                #f
                (even? (- n 1))))))
  (even? 88))                           &rArr;  #t
</pre></div>

<span id="index-lambda-5"></span>
<span id="index-delay"></span>
<p>One restriction on <code>letrec</code> is very important: it shall be possible
to evaluated each <var>init</var> without assigning or referring to the value
of any <var>variable</var>.  If this restriction is violated, then it is an
error.  The restriction is necessary because Scheme passes arguments by
value rather than by name.  In the most common uses of <code>letrec</code>,
all the <var>init</var>s are <code>lambda</code> or <code>delay</code> expressions and
the restriction is satisfied automatically.
</p></dd></dl>

<dl>
<dt id="index-letrec_002a">extended standard special form: <strong>letrec*</strong> <em>((<var>variable</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><p>The <var>variable</var>s are bound to fresh locations, each <var>variable</var>
is assigned in left-to-right order to the result of evaluating the
corresponding <var>init</var> (interleaving evaluations and assignments),
the <var>expr</var>s are evaluated in the resulting environment, and
the values of the last <var>expr</var> are returned.  Despite the
left-to-right evaluation and assignment order, each binding of a
<var>variable</var> has the entire <code>letrec*</code> expression as its region,
making it possible to define mutually recursive procedures.
</p>
<p>If it is not possible to evaluate each <var>init</var> without assigning or
referring to the value of the corresponding <var>variable</var> or the
<var>variable</var> of any of the bindings that follow it in
<var>bindings</var>, it is an error.  Another restriction is that it is an
error to invoke the continuation of an <var>init</var> more than once.
</p>
<div class="example">
<pre class="example">;; Returns the arithmetic, geometric, and
;; harmonic means of a nested list of numbers
(define (means ton)
  (letrec*
     ((mean
        (lambda (f g)
          (f (/ (sum g ton) n))))
      (sum
        (lambda (g ton)
          (if (null? ton)
            (+)
            (if (number? ton)
                (g ton)
                (+ (sum g (car ton))
                   (sum g (cdr ton)))))))
      (n (sum (lambda (x) 1) ton)))
    (values (mean values values)
            (mean exp log)
            (mean / /))))
</pre></div>
<p>Evaluating <code>(means '(3 (1 4)))</code> returns three values: <code>8/3</code>,
<code>2.28942848510666</code> (approximately), and <code>36/19</code>.
</p></dd></dl>

<dl>
<dt id="index-let_002dvalues">standard special form: <strong>let-values</strong> <em>((<var>formals</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><p>The <var>init</var>s are evaluated in the current environment (in some
unspecified order) as if by invoking <code>call-with-values</code>, and the
variables occurring in the <var>formals</var> are bound to fresh locations
holding the values returned by the <var>init</var>s, where the
<var>formals</var> are matched to the return values in the same way that
the <var>formals</var> in a <code>lambda</code> expression are matched to the
arguments in a procedure call.  Then, the <var>expr</var>s are
evaluated in the extended environment, and the values of the last
<var>expr</var> are returned.  Each binding of a <var>variable</var> has
the <var>expr</var>s as its region.
</p>
<p>It is an error if the <var>formals</var> do not match the number of values
returned by the corresponding <var>init</var>.
</p>
<div class="example">
<pre class="example">(let-values (((root rem) (exact-integer-sqrt 32)))
  (* root rem))         &rArr;  35
</pre></div>
</dd></dl>

<dl>
<dt id="index-let_002a_002dvalues">standard special form: <strong>let*-values</strong> <em>((<var>formals</var> <var>init</var>) &hellip;) expr expr &hellip;</em></dt>
<dd><p>The <code>let*-values</code> construct is similar to <code>let-values</code>, but the
<var>init</var>s are evaluated and bindings created sequentially from
left to right, with the region of the bindings of each <var>formals</var>
including the <var>init</var>s to its right as well as <var>body</var>.  Thus the
second <var>init</var> is evaluated in an environment in which the first
set of bindings is visible and initialized, and so on.
</p>
<div class="example">
<pre class="example">(let ((a 'a) (b 'b) (x 'x) (y 'y))
  (let*-values (((a b) (values x y))
                ((x y) (values a b)))
    (list a b x y)))    &rArr;  (x y x y)
</pre></div>
</dd></dl>


<hr>
<div class="header">
<p>
Next: <a href="Dynamic-Binding.html" accesskey="n" rel="next">Dynamic Binding</a>, Previous: <a href="Lambda-Expressions.html" accesskey="p" rel="prev">Lambda Expressions</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
