<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Macros (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Macros (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Macros (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Syntactic-Binding-Constructs.html" rel="next" title="Syntactic Binding Constructs">
<link href="Structure-Definitions.html" rel="prev" title="Structure Definitions">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Macros"></span><div class="header">
<p>
Next: <a href="SRFI-syntax.html" accesskey="n" rel="next">SRFI syntax</a>, Previous: <a href="Structure-Definitions.html" accesskey="p" rel="prev">Structure Definitions</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Macros-1"></span><h3 class="section">2.11 Macros</h3>

<p>(This section is largely taken from the <cite>Revised^4 Report on the
Algorithmic Language Scheme</cite>.  The section on Syntactic Closures is
derived from a document written by Chris Hanson.  The section on
Explicit Renaming is derived from a document written by William
Clinger.)
</p>
<span id="index-macro"></span>
<p>Scheme programs can define and use new derived expression types, called
<em>macros</em>.  Program-defined expression types have the syntax
</p>
<div class="example">
<pre class="example">(<var>keyword</var> <var>datum</var> &hellip;)
</pre></div>

<p><span id="index-syntactic-keyword-1"></span>
<span id="index-keyword"></span>
<span id="index-macro-keyword"></span>
where <var>keyword</var> is an identifier that uniquely determines the
expression type.  This identifier is called the <em>syntactic keyword</em>,
or simply <em>keyword</em>, of the macro.  The number of the <var>datum</var>s,
and their syntax, depends on the expression type.
</p>
<span id="index-macro-use"></span>
<span id="index-macro-transformer"></span>
<p>Each instance of a macro is called a <em>use</em> of the macro.  The set of
rules that specifies how a use of a macro is transcribed into a more
primitive expression is called the <em>transformer</em> of the macro.
</p>
<span id="index-anonymous-syntactic-keyword"></span>
<p>MIT/GNU Scheme also supports <em>anonymous syntactic keywords</em>.  This means
that it&rsquo;s not necessary to bind a macro transformer to a syntactic
keyword before it is used.  Instead, any macro-transformer expression
can appear as the first element of a form, and the form will be expanded
by the transformer.
</p>
<p>The macro definition facility consists of these parts:
</p>
<ul>
<li> A set of expressions used to establish that certain identifiers are
macro keywords, associate them with macro transformers, and control the
scope within which a macro is defined.

</li><li> A standard high-level pattern language for specifying macro
transformers, introduced by the <code>syntax-rules</code> special form.

</li><li> Two non-standard low-level languages for specifying macro transformers,
<em>syntactic closures</em> and <em>explicit renaming</em>.
</li></ul>

<span id="index-hygienic"></span>
<span id="index-referentially-transparent"></span>
<p>The syntactic keyword of a macro may shadow variable bindings, and local
variable bindings may shadow keyword bindings.  All macros defined using
the pattern language are &ldquo;hygienic&rdquo; and &ldquo;referentially transparent&rdquo;
and thus preserve Scheme&rsquo;s lexical scoping:
</p>
<ul>
<li> If a macro transformer inserts a binding for an identifier (variable or
keyword), the identifier will in effect be renamed throughout its scope
to avoid conflicts with other identifiers.

</li><li> If a macro transformer inserts a free reference to an identifier, the
reference refers to the binding that was visible where the transformer
was specified, regardless of any local bindings that may surround the
use of the macro.
</li></ul>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Syntactic-Binding-Constructs.html" accesskey="1">Syntactic Binding Constructs</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Pattern-Language.html" accesskey="2">Pattern Language</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Syntactic-Closures.html" accesskey="3">Syntactic Closures</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Explicit-Renaming.html" accesskey="4">Explicit Renaming</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="SRFI-syntax.html" accesskey="n" rel="next">SRFI syntax</a>, Previous: <a href="Structure-Definitions.html" accesskey="p" rel="prev">Structure Definitions</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
