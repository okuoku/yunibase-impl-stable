<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Character implementation (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Character implementation (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Character implementation (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Characters.html" rel="up" title="Characters">
<link href="Unicode.html" rel="next" title="Unicode">
<link href="Characters.html" rel="prev" title="Characters">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Character-implementation"></span><div class="header">
<p>
Next: <a href="Unicode.html" accesskey="n" rel="next">Unicode</a>, Previous: <a href="Characters.html" accesskey="p" rel="prev">Characters</a>, Up: <a href="Characters.html" accesskey="u" rel="up">Characters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Character-implementation-1"></span><h3 class="section">5.1 Character implementation</h3>
<span id="index-internal-representation_002c-for-character"></span>

<span id="index-character-code-_0028defn_0029"></span>
<span id="index-character-bits-_0028defn_0029"></span>
<span id="index-code_002c-of-character-_0028defn_0029"></span>
<span id="index-bucky-bit_002c-of-character-_0028defn_0029"></span>
<span id="index-ASCII-character"></span>
<p>An MIT/GNU Scheme character consists of a <em>code</em> part and a
<em>bucky bits</em> part.  The code part is a Unicode code point, while
the bucky bits are an additional set of bits representing shift keys
available on some keyboards.
</p>
<p>There are 4 bucky bits, named <em>control</em>, <em>meta</em>, <em>super</em>,
and <em>hyper</em>.  On GNU/Linux systems running a graphical desktop,
the control bit corresponds to the <tt class="key">CTRL</tt> key; the meta bit
corresponds to the <tt class="key">ALT</tt> key; and the super bit corresponds to the
&ldquo;windows&rdquo; key.  On macOS, these are the <tt class="key">CONTROL</tt>, <tt class="key">OPTION</tt>,
and <tt class="key">COMMAND</tt> keys respectively.
</p>
<p>Characters with bucky bits are not used much outside of graphical user
interfaces (e.g. Edwin).  They cannot be stored in strings or
character sets, and aren&rsquo;t read or written by textual I/O ports.
</p>
<dl>
<dt id="index-make_002dchar">procedure: <strong>make-char</strong> <em>code bucky-bits</em></dt>
<dd><span id="index-construction_002c-of-character"></span>
<p>Builds a character from <var>code</var> and <var>bucky-bits</var>.  The value of
<var>code</var> must be a Unicode code point; the value of <var>bucky-bits</var>
must be an exact non-negative integer strictly less than <code>16</code>.
If <code>0</code> is specified for <var>bucky-bits</var>, <code>make-char</code>
produces an ordinary character; otherwise, the appropriate bits are
set as follows:
</p><div class="example">
<pre class="example">1               meta
2               control
4               super
8               hyper
</pre></div>

<p>For example,
</p><div class="example">
<pre class="example">(make-char 97 0)                        &rArr;  #\a
(make-char 97 1)                        &rArr;  #\M-a
(make-char 97 2)                        &rArr;  #\C-a
(make-char 97 3)                        &rArr;  #\C-M-a
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_002dcode">procedure: <strong>char-code</strong> <em>char</em></dt>
<dd><p>Returns the Unicode code point of <var>char</var>.  Note that if <var>char</var>
has no bucky bits set, then this is the same value returned by
<code>char-&gt;integer</code>.
</p>
<p>For example,
</p><div class="example">
<pre class="example">(char-code #\a)                         &rArr;  97
(char-code #\c-a)                       &rArr;  97
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_002dbits">procedure: <strong>char-bits</strong> <em>char</em></dt>
<dd><span id="index-selection_002c-of-character-component"></span>
<span id="index-component-selection_002c-of-character"></span>
<p>Returns the exact integer representation of <var>char</var>&rsquo;s bucky bits.
For example,
</p>
<div class="example">
<pre class="example">(char-bits #\a)                         &rArr;  0
(char-bits #\m-a)                       &rArr;  1
(char-bits #\c-a)                       &rArr;  2
(char-bits #\c-m-a)                     &rArr;  3
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_002dcode_002dlimit">constant: <strong>char-code-limit</strong></dt>
<dd><p>This constant is the strict upper limit on a character&rsquo;s <var>code</var>
value. It is <code>#x110000</code> unless some future version of Unicode
increases the range of code points.
</p></dd></dl>

<dl>
<dt id="index-char_002dbits_002dlimit">constant: <strong>char-bits-limit</strong></dt>
<dd><p>This constant is the strict upper limit on a character&rsquo;s
<var>bucky-bits</var> value.  It is currently <code>#x10</code> and unlikely to
change in the future.
</p></dd></dl>

<dl>
<dt id="index-bitless_002dchar_003f">procedure: <strong>bitless-char?</strong> <em>object</em></dt>
<dd><span id="index-bitless-character"></span>
<span id="index-character_002c-bitless"></span>
<p>Returns <code>#t</code> if <var>object</var> is a character with no bucky bits
set, otherwise it returns <code>#f</code> .
</p></dd></dl>

<dl>
<dt id="index-char_002d_003ebitless_002dchar">procedure: <strong>char-&gt;bitless-char</strong> <em>char</em></dt>
<dd><p>Returns <var>char</var> with any bucky bits removed.  The result is
guaranteed to satisfy <code>bitless-char?</code>.
</p></dd></dl>

<dl>
<dt id="index-char_002dpredicate">procedure: <strong>char-predicate</strong> <em>char</em></dt>
<dd><p>Returns a procedure of one argument that returns <code>#t</code> if its
argument is a character <code>char=?</code> to <var>char</var>, otherwise it
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-char_002dci_002dpredicate">procedure: <strong>char-ci-predicate</strong> <em>char</em></dt>
<dd><p>Returns a procedure of one argument that returns <code>#t</code> if its
argument is a character <code>char-ci=?</code> to <var>char</var>, otherwise it
returns <code>#f</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Unicode.html" accesskey="n" rel="next">Unicode</a>, Previous: <a href="Characters.html" accesskey="p" rel="prev">Characters</a>, Up: <a href="Characters.html" accesskey="u" rel="up">Characters</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
