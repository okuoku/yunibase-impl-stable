<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Construction of Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Construction of Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Construction of Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Weight_002dBalanced-Trees.html" rel="up" title="Weight-Balanced Trees">
<link href="Basic-Operations-on-Weight_002dBalanced-Trees.html" rel="next" title="Basic Operations on Weight-Balanced Trees">
<link href="Weight_002dBalanced-Trees.html" rel="prev" title="Weight-Balanced Trees">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Construction-of-Weight_002dBalanced-Trees"></span><div class="header">
<p>
Next: <a href="Basic-Operations-on-Weight_002dBalanced-Trees.html" accesskey="n" rel="next">Basic Operations on Weight-Balanced Trees</a>, Previous: <a href="Weight_002dBalanced-Trees.html" accesskey="p" rel="prev">Weight-Balanced Trees</a>, Up: <a href="Weight_002dBalanced-Trees.html" accesskey="u" rel="up">Weight-Balanced Trees</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Construction-of-Weight_002dBalanced-Trees-1"></span><h4 class="subsection">11.7.1 Construction of Weight-Balanced Trees</h4>

<p>Binary trees require there to be a total order on the keys used to
arrange the elements in the tree.  Weight-balanced trees are organized
by <em>types</em>, where the type is an object encapsulating the ordering
relation.  Creating a tree is a two-stage process.  First a tree type
must be created from the predicate that gives the ordering.  The tree type
is then used for making trees, either empty or singleton trees or trees
from other aggregate structures like association lists.  Once created, a
tree &lsquo;knows&rsquo; its type and the type is used to test compatibility between
trees in operations taking two trees.  Usually a small number of tree
types are created at the beginning of a program and used many times
throughout the program&rsquo;s execution.
</p>
<dl>
<dt id="index-make_002dwt_002dtree_002dtype">procedure: <strong>make-wt-tree-type</strong> <em>key&lt;?</em></dt>
<dd><p>This procedure creates and returns a new tree type based on the ordering
predicate <var>key&lt;?</var>.
<var>Key&lt;?</var> must be a total ordering, having the property that for all
key values <code>a</code>, <code>b</code> and <code>c</code>:
</p>
<div class="example">
<pre class="example">(key&lt;? a a)                         &rArr; #f
(and (key&lt;? a b) (key&lt;? b a))       &rArr; #f
(if (and (key&lt;? a b) (key&lt;? b c))
    (key&lt;? a c)
    #t)                             &rArr; #t
</pre></div>

<p>Two key values are assumed to be equal if neither is less than the other
by <var>key&lt;?</var>.
</p>
<p>Each call to <code>make-wt-tree-type</code> returns a distinct value, and
trees are only compatible if their tree types are <code>eq?</code>.  A
consequence is that trees that are intended to be used in binary-tree
operations must all be created with a tree type originating from the
same call to <code>make-wt-tree-type</code>.
</p></dd></dl>

<dl>
<dt id="index-number_002dwt_002dtype">variable: <strong>number-wt-type</strong></dt>
<dd><p>A standard tree type for trees with numeric keys.  <code>Number-wt-type</code>
could have been defined by
</p>
<div class="example">
<pre class="example">(define number-wt-type (make-wt-tree-type  &lt;))
</pre></div>
</dd></dl>

<dl>
<dt id="index-string_002dwt_002dtype">variable: <strong>string-wt-type</strong></dt>
<dd><p>A standard tree type for trees with string keys.  <code>String-wt-type</code>
could have been defined by
</p>
<div class="example">
<pre class="example">(define string-wt-type (make-wt-tree-type  string&lt;?))
</pre></div>
</dd></dl>

<dl>
<dt id="index-make_002dwt_002dtree">procedure: <strong>make-wt-tree</strong> <em>wt-tree-type</em></dt>
<dd><p>This procedure creates and returns a newly allocated weight-balanced
tree.  The tree is empty, i.e. it contains no associations.
<var>Wt-tree-type</var> is a weight-balanced tree type obtained by calling
<code>make-wt-tree-type</code>; the returned tree has this type.
</p></dd></dl>

<dl>
<dt id="index-singleton_002dwt_002dtree">procedure: <strong>singleton-wt-tree</strong> <em>wt-tree-type key datum</em></dt>
<dd><p>This procedure creates and returns a newly allocated weight-balanced
tree.  The tree contains a single association, that of <var>datum</var> with
<var>key</var>.  <var>Wt-tree-type</var> is a weight-balanced tree type obtained
by calling <code>make-wt-tree-type</code>; the returned tree has this type.
</p></dd></dl>

<dl>
<dt id="index-alist_002d_003ewt_002dtree">procedure: <strong>alist-&gt;wt-tree</strong> <em>tree-type alist</em></dt>
<dd><p>Returns a newly allocated weight-balanced tree that contains the same
associations as <var>alist</var>.  This procedure is equivalent to:
</p>
<div class="example">
<pre class="example">(lambda (type alist)
  (let ((tree (make-wt-tree type)))
    (for-each (lambda (association)
                (wt-tree/add! tree
                              (car association)
                              (cdr association)))
              alist)
    tree))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Basic-Operations-on-Weight_002dBalanced-Trees.html" accesskey="n" rel="next">Basic Operations on Weight-Balanced Trees</a>, Previous: <a href="Weight_002dBalanced-Trees.html" accesskey="p" rel="prev">Weight-Balanced Trees</a>, Up: <a href="Weight_002dBalanced-Trees.html" accesskey="u" rel="up">Weight-Balanced Trees</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
