<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Finding and Invoking General Restart Code (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Finding and Invoking General Restart Code (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Finding and Invoking General Restart Code (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Restarts.html" rel="up" title="Restarts">
<link href="The-Named-Restart-Abstraction.html" rel="next" title="The Named Restart Abstraction">
<link href="Invoking-Standard-Restart-Code.html" rel="prev" title="Invoking Standard Restart Code">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Finding-and-Invoking-General-Restart-Code"></span><div class="header">
<p>
Next: <a href="The-Named-Restart-Abstraction.html" accesskey="n" rel="next">The Named Restart Abstraction</a>, Previous: <a href="Invoking-Standard-Restart-Code.html" accesskey="p" rel="prev">Invoking Standard Restart Code</a>, Up: <a href="Restarts.html" accesskey="u" rel="up">Restarts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Finding-and-Invoking-General-Restart-Code-1"></span><h4 class="subsection">16.4.3 Finding and Invoking General Restart Code</h4>

<span id="index-with_002drestart-3"></span>
<span id="index-with_002dsimple_002drestart-3"></span>
<span id="index-bound_002drestart"></span>
<span id="index-find_002drestart-1"></span>
<span id="index-invoke_002drestart-2"></span>
<span id="index-invoke_002drestart_002dinteractively-1"></span>
<p>Restarts are a general mechanism for establishing a protocol between
condition-signalling and condition-handling code.  The Scheme error
system provides &ldquo;packaging&rdquo; for a number of common protocols.  It also
provides lower-level hooks that are intended for implementing customized
protocols.  The mechanism used by signalling code (<code>with-restart</code>
and <code>with-simple-restart</code>) is used for both purposes.
</p>
<p>Four additional operations are provided for the use of
condition-handling code.  Two operations (<code>bound-restarts</code> and
<code>find-restart</code>) allow condition-handling code to locate active
restarts.  The other two operations (<code>invoke-restart</code> and
<code>invoke-restart-interactively</code>) allow restart effectors to be
invoked once the restart object has been located.
</p>
<p>In addition, there is a data abstraction that provides access to the
information encapsulated in restart objects.
</p>
<dl>
<dt id="index-bound_002drestarts-2">procedure: <strong>bound-restarts</strong></dt>
<dd><p>Returns a list of all currently active restart objects, most recently
installed first.  <code>bound-restarts</code> should be used with caution by
condition-handling code, since it reveals all restarts that are active
at the time it is called, rather than at the time the condition was
signalled.  It is useful, however, for collecting the list of restarts
for inclusion in newly generated condition objects or for inspecting the
current state of the system.
</p></dd></dl>

<dl>
<dt id="index-find_002drestart-2">procedure: <strong>find-restart</strong> <em>name [restarts]</em></dt>
<dd><p>Returns the first restart object named <var>name</var> in the list of
<var>restarts</var> (permissible values for <var>restarts</var> are described
above in <a href="Restarts.html">Restarts</a>).  When used in a condition handler,
<code>find-restart</code> is usually passed the name of a particular restart
<em>and</em> the condition object that has been signalled.  In this way
the handler finds only restarts that were available when the condition
was created (usually the same as when it was signalled).  If
<var>restarts</var> is omitted, the currently active restarts would be used,
and these often include restarts added after the condition ocurred.
</p></dd></dl>

<dl>
<dt id="index-invoke_002drestart-3">procedure: <strong>invoke-restart</strong> <em>restart argument &hellip;</em></dt>
<dd><span id="index-invoke_002drestart_002dinteractively-2"></span>
<p>Calls the restart effector encapsulated in <var>restart</var>, passing the
specified <var>argument</var>s to it.  <code>invoke-restart</code> is intended for
use by condition-handling code that understands the protocol implemented
by <var>restart</var>, and can therefore calculate and pass an appropriate
set of arguments.
</p>
<p>If a condition handler needs to interact with a user to gather the
arguments for an effector (e.g. if it does not understand the protocol
implemented by <var>restart</var>) <code>invoke-restart-interactively</code> should
be used instead of <code>invoke-restart</code>.
</p></dd></dl>

<dl>
<dt id="index-invoke_002drestart_002dinteractively-3">procedure: <strong>invoke-restart-interactively</strong> <em>restart</em></dt>
<dd><p>First calls the interactor encapsulated in <var>restart</var> to
interactively gather the arguments needed for <var>restart</var>&rsquo;s effector.
It then calls the effector, passing these arguments to it.
</p>
<span id="index-restart_002finteractor"></span>
<p><code>invoke-restart-interactively</code> is intended for calling interactive
restarts (those for which <code>restart/interactor</code> is not <code>#f</code>).
For convenience, <code>invoke-restart-interactively</code> will call the
restart&rsquo;s effector with no arguments if the restart has no interactor;
this behavior may change in the future.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="The-Named-Restart-Abstraction.html" accesskey="n" rel="next">The Named Restart Abstraction</a>, Previous: <a href="Invoking-Standard-Restart-Code.html" accesskey="p" rel="prev">Invoking Standard Restart Code</a>, Up: <a href="Restarts.html" accesskey="u" rel="up">Restarts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
