<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Coordinates for Graphics (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Coordinates for Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Coordinates for Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Graphics.html" rel="up" title="Graphics">
<link href="Drawing-Graphics.html" rel="next" title="Drawing Graphics">
<link href="Opening-and-Closing-of-Graphics-Devices.html" rel="prev" title="Opening and Closing of Graphics Devices">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Coordinates-for-Graphics"></span><div class="header">
<p>
Next: <a href="Drawing-Graphics.html" accesskey="n" rel="next">Drawing Graphics</a>, Previous: <a href="Opening-and-Closing-of-Graphics-Devices.html" accesskey="p" rel="prev">Opening and Closing of Graphics Devices</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Coordinates-for-Graphics-1"></span><h3 class="section">17.2 Coordinates for Graphics</h3>
<span id="index-graphics_002c-coordinate-systems"></span>

<span id="index-coordinates_002c-graphics"></span>
<span id="index-device-coordinates_002c-graphics-_0028defn_0029"></span>
<span id="index-graphics_002c-device-coordinates-_0028defn_0029"></span>
<span id="index-virtual-coordinates_002c-graphics-_0028defn_0029"></span>
<span id="index-graphics_002c-virtual-coordinates-_0028defn_0029"></span>
<p>Each graphics device has two different coordinate systems associated
with it: <em>device coordinates</em> and <em>virtual coordinates</em>.  Device
coordinates are generally defined by low-level characteristics of the
device itself, and often cannot be changed.  Most device coordinate
systems are defined in terms of pixels, and usually the upper-left-hand
corner is the origin of the coordinate system, with <var>x</var> coordinates
increasing to the right and <var>y</var> coordinates increasing downwards.
</p>
<p>In contrast, virtual coordinates are more flexible in the units
employed, the position of the origin, and even the direction in which
the coordinates increase.  A virtual coordinate system is defined by
assigning coordinates to the edges of a device.  Because these edge
coordinates are arbitrary real numbers, any Cartesian coordinate system
can be defined.
</p>
<p>All graphics procedures that use coordinates are defined on virtual
coordinates.  For example, to draw a line at a particular place on a
device, the virtual coordinates for the endpoints of that line are
given.
</p>
<p>When a graphics device is initialized, its virtual coordinate system is
reset so that the left edge corresponds to an x-coordinate of <code>-1</code>,
the right edge to x-coordinate <code>1</code>, the bottom edge to y-coordinate
<code>-1</code>, and the top edge to y-coordinate <code>1</code>.
</p>
<dl>
<dt id="index-graphics_002ddevice_002dcoordinate_002dlimits">procedure: <strong>graphics-device-coordinate-limits</strong> <em>graphics-device</em></dt>
<dd><p>Returns (as multiple values) the device coordinate limits for
<var>graphics-device</var>.  The values, which are exact non-negative
integers, are: <var>x-left</var>, <var>y-bottom</var>, <var>x-right</var>, and
<var>y-top</var>.
</p></dd></dl>

<dl>
<dt id="index-graphics_002dcoordinate_002dlimits">procedure: <strong>graphics-coordinate-limits</strong> <em>graphics-device</em></dt>
<dd><p>Returns (as multiple values) the virtual coordinate limits for
<var>graphics-device</var>.  The values, which are real numbers, are:
<var>x-left</var>, <var>y-bottom</var>, <var>x-right</var>, and <var>y-top</var>.
</p></dd></dl>

<dl>
<dt id="index-graphics_002dset_002dcoordinate_002dlimits">procedure: <strong>graphics-set-coordinate-limits</strong> <em>graphics-device x-left y-bottom x-right y-top</em></dt>
<dd><p>Changes the virtual coordinate limits of <var>graphics-device</var> to the
given arguments.  <var>X-left</var>, <var>y-bottom</var>, <var>x-right</var>, and
<var>y-top</var> must be real numbers.  Subsequent calls to
<code>graphics-coordinate-limits</code> will return the new limits.  This
operation has no effect on the device&rsquo;s displayed contents.
</p>
<p>Note: This operation usually resets the clip rectangle, although it is
not guaranteed to do so.  If a clip rectangle is in effect when this
procedure is called, it is necessary to redefine the clip rectangle
afterwards.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Drawing-Graphics.html" accesskey="n" rel="next">Drawing Graphics</a>, Previous: <a href="Opening-and-Closing-of-Graphics-Devices.html" accesskey="p" rel="prev">Opening and Closing of Graphics Devices</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
