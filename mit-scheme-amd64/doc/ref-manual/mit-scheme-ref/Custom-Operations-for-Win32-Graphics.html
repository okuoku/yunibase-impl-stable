<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Custom Operations for Win32 Graphics (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Custom Operations for Win32 Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Custom Operations for Win32 Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Win32-Graphics.html" rel="up" title="Win32 Graphics">
<link href="Win32-Package-Reference.html" rel="next" title="Win32 Package Reference">
<link href="Win32-Graphics-Type.html" rel="prev" title="Win32 Graphics Type">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Custom-Operations-for-Win32-Graphics"></span><div class="header">
<p>
Previous: <a href="Win32-Graphics-Type.html" accesskey="p" rel="prev">Win32 Graphics Type</a>, Up: <a href="Win32-Graphics.html" accesskey="u" rel="up">Win32 Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Custom-Operations-for-Win32-Graphics-1"></span><h4 class="subsection">17.10.2 Custom Operations for Win32 Graphics</h4>

<p>Custom operations are invoked using the procedure
<code>graphics-operation</code>.  For example,
</p>
<div class="example">
<pre class="example">(graphics-operation device 'set-foreground-color &quot;blue&quot;)
</pre></div>

<dl>
<dt id="index-set_002dbackground_002dcolor-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>set-background-color</strong> <em>color-name</em></dt>
<dt id="index-set_002dforeground_002dcolor-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>set-foreground-color</strong> <em>color-name</em></dt>
<dd><span id="index-set_002dbackground_002dcolor"></span>
<span id="index-set_002dforeground_002dcolor"></span>
<span id="index-color"></span>
<p>These operations change the colors associated with a window.
<var>Color-name</var> must be of one of the valid color specification forms
listed below.  <code>set-background-color</code> and
<code>set-foreground-color</code> change the colors to be used when drawing,
but have no effect on anything drawn prior to their invocation.  Because
changing the background color affects the entire window, we recommend
calling <code>graphics-clear</code> on the window&rsquo;s device afterwards.
</p>
<p>The foreground color affects the drawing of text, points, lines,
ellipses and filled polygons.
</p>
<p>Colors are specified in one of three ways:
</p>
<dl compact="compact">
<dt>An integer</dt>
<dd><p>This is the Win32 internal RGB value.
</p>
</dd>
<dt>By name</dt>
<dd><p>A limited number of names are understood by the system.
Names are strings, e.g. <code>&quot;red&quot;</code>, <code>&quot;blue&quot;</code>, <code>&quot;black&quot;</code>.
More names can be registered with the <code>define-color</code> operation.
</p>
</dd>
<dt>RGB (Red-Green-Blue) triples</dt>
<dd><p>A triple is either a vector or list of three integers in the range
0&ndash;255 inclusive which specify the intensity of the red, green and blue
components of the color.  Thus <code>#(0 0 0)</code> is black, <code>(0 0
128)</code> is dark blue and <code>#(255 255 255)</code> is white.
</p></dd>
</dl>

<p>If the color is not available in the graphics device then the nearest
available color is used instead.
</p></dd></dl>


<dl>
<dt id="index-define_002dcolor-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>define-color</strong> <em>name spec</em></dt>
<dd><p>Define the string <var>name</var> to be the color specified by <var>spec</var>.
<var>Spec</var> may be any acceptable color specification.  Note that the
color names defined this way are available to any Win32 graphics device,
and the names do <em>not</em> have to be defined for each device.
</p>

<p>Color names defined by this interface may also be used when setting the
colors of the Scheme console window, or the colors of Edwin editor
windows.
</p></dd></dl>

<dl>
<dt id="index-find_002dcolor-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>find-color</strong> <em>name</em></dt>
<dd><p>Looks up a color previously defined by <code>define-color</code>.  This returns
the color in its most efficient form for operations
<code>set-foreground-color</code> or <code>set-background-color</code>.
</p></dd></dl>


<dl>
<dt id="index-draw_002dellipse-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>draw-ellipse</strong> <em>left top right bottom</em></dt>
<dd><span id="index-ellipse_002c-graphics"></span>
<span id="index-circle_002c-graphics"></span>
<span id="index-graphics_002c-ellipse"></span>
<span id="index-graphics_002c-circle"></span>
<p>Draw an ellipse.  <var>Left</var>, <var>top</var>, <var>right</var> and <var>bottom</var>
indicate the coordinates of the bounding rectangle of the ellipse.
Circles are merely ellipses with equal width and height.  Note that the
bounding rectangle has horizontal and vertical sides.  Ellipses with
rotated axes cannot be drawn.  The rectangle applies to the center of the
line used to draw the ellipse; if the line width has been set to greater
than 1 then the ellipse will spill outside the bounding rectange by half
of the line width.
</p></dd></dl>


<dl>
<dt id="index-fill_002dpolygon-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>fill-polygon</strong> <em>points</em></dt>
<dd><span id="index-fill_002dpolygon"></span>
<p>Draws a filled polygon using the current foreground color.
<var>Points</var> is a vector of real numbers.
The numbers are in the order x1 y1 x2 y2 &hellip; xn yn.
For example,
</p>
<div class="example">
<pre class="example">(graphics-operation device 'fill-polygon #(0 0 0 1 1 0))
</pre></div>

<p>draws a solid triangular region between the points (0, 0), (0, 1) and
(1, 0).
</p></dd></dl>


<dl>
<dt id="index-load_002dbitmap-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>load-bitmap</strong> <em>pathname</em></dt>
<dd><span id="index-bitmaps"></span>
<p>The graphics device contents and size are initialized from the windows
bitmap file specified by <var>pathname</var>.  If no file type is supplied
then a <code>&quot;.BMP&quot;</code> extension is added.  If a clip rectangle is in
effect when this procedure is called, it is necessary to redefine the
clip rectangle afterwards.
</p></dd></dl>

<dl>
<dt id="index-save_002dbitmap-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>save-bitmap</strong> <em>pathname</em></dt>
<dd><span id="index-printing-graphics-output"></span>
<p>The graphics device contents are saved as a bitmap to the file specified
by <var>pathname</var>.  If no file type is supplied then a <code>&quot;.BMP&quot;</code>
extension is added.  The saved bitmap may be incorporated into documents
or printed.
</p></dd></dl>

<dl>
<dt id="index-move_002dwindow-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>move-window</strong> <em>x y</em></dt>
<dd><p>The graphics device window is moved to the screen position specified by
<var>x</var> and <var>y</var>.
</p></dd></dl>

<dl>
<dt id="index-resize_002dwindow-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>resize-window</strong> <em>width height</em></dt>
<dd><p>The graphics device window is resized to the specified <var>width</var> and
<var>height</var> in device coordinates (pixels).  If a clip rectangle is in effect
when this procedure is called, it is necessary to redefine the clip
rectangle afterwards.
</p></dd></dl>

<dl>
<dt id="index-set_002dline_002dwidth-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>set-line-width</strong> <em>width</em></dt>
<dd><p>This operation sets the line width for future drawing of lines, points
and ellipses.  It does not affect existing lines and has no effect on
filled polygons.  The line width is specified in device units.  The
default and initial value of this parameter is 1 pixel.
</p></dd></dl>

<dl>
<dt id="index-set_002dwindow_002dname-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>set-window-name</strong> <em>name</em></dt>
<dd><p>This sets the window title to the string <var>name</var>.  The window is
given the name <code>&quot;Scheme Graphics&quot;</code> at creation.
</p></dd></dl>

<dl>
<dt id="index-set_002dfont-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>set-font</strong> <em>handle</em></dt>
<dd><p>Sets the font for drawing text.  Currently not well supported.  If you
can get a Win32 font handle it can be used here.
</p></dd></dl>

<dl>
<dt id="index-copy_002darea-on-win32_002dgraphics_002ddevice">operation on win32-graphics-device: <strong>copy-area</strong> <em>source-x-left source-y-top width height destination-x-left destination-y-top</em></dt>
<dd><p>This operation copies the contents of the rectangle specified by
<var>source-x-left</var>, <var>source-y-top</var>, <var>width</var>, and <var>height</var>
to the rectangle of the same dimensions at <var>destination-x-left</var> and
<var>destination-y-top</var>.
</p></dd></dl>
<hr>
<div class="header">
<p>
Previous: <a href="Win32-Graphics-Type.html" accesskey="p" rel="prev">Win32 Graphics Type</a>, Up: <a href="Win32-Graphics.html" accesskey="u" rel="up">Win32 Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
