<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Weak References (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Weak References (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Weak References (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Weak-Pairs.html" rel="next" title="Weak Pairs">
<link href="Streams.html" rel="prev" title="Streams">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Weak-References"></span><div class="header">
<p>
Previous: <a href="Streams.html" accesskey="p" rel="prev">Streams</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Weak-References-1"></span><h3 class="section">10.7 Weak References</h3>

<p>Weak references are a mechanism for building data structures that
point at objects without protecting them from garbage collection.  An
example of such a data structure might be an entry in a lookup table
that should be removed if the rest of the program does not reference
its key.  Such an entry must still point at its key to carry out
comparisons, but should not in itself prevent its key from being
garbage collected.
</p>
<span id="index-weak-reference-_0028defn_0029"></span>
<span id="index-strong-reference-_0028defn_0029"></span>
<span id="index-reference_002c-weak-_0028defn_0029"></span>
<span id="index-reference_002c-strong-_0028defn_0029"></span>
<p>A <em>weak reference</em> is a reference that points at an object without
preventing it from being garbage collected.  The term <em>strong
reference</em> is used to distinguish normal references from weak ones.
If there is no path of strong references to some object, the garbage
collector will reclaim that object and mark any weak references to it
to indicate that it has been reclaimed.
</p>
<p>If there is a path of strong references from an object <var>A</var> to an
object <var>B</var>, <var>A</var> is said to hold <var>B</var> <em>strongly</em>.  If
there is a path of references from an object <var>A</var> to an object
<var>B</var>, but every such path traverses at least one weak reference,
<var>A</var> is said to hold <var>B</var> <em>weakly</em>.
</p>
<p>MIT Scheme provides two mechanisms for using weak references.
<em>Weak pairs</em> are like normal pairs, except that their car slot is
a weak reference (but the cdr is still strong).  The heavier-weight
<em>ephemerons</em> additionally arrange that the ephemeron does not
count as holding the object in its key field strongly even if the
object in its datum field does.
</p>
<p><strong>Warning</strong>: Working with weak references is subtle and requires
careful analysis; most programs should avoid working with them
directly.  The most common use cases for weak references ought to be
served by hash tables (see <a href="Hash-Tables.html">Hash Tables</a>), which can employ various
flavors of weak entry types, 1d tables (see <a href="1D-Tables.html">1D Tables</a>), which hold
their keys weakly, and the association table (see <a href="The-Association-Table.html">The Association Table</a>), which also holds its keys weakly.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Weak-Pairs.html" accesskey="1">Weak Pairs</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Ephemerons.html" accesskey="2">Ephemerons</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Reference-barriers.html" accesskey="3">Reference barriers</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Previous: <a href="Streams.html" accesskey="p" rel="prev">Streams</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
