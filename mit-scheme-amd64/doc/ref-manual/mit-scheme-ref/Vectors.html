<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Vectors (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Vectors (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Vectors (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Construction-of-Vectors.html" rel="next" title="Construction of Vectors">
<link href="Miscellaneous-List-Operations.html" rel="prev" title="Miscellaneous List Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Vectors"></span><div class="header">
<p>
Next: <a href="Bit-Strings.html" accesskey="n" rel="next">Bit Strings</a>, Previous: <a href="Lists.html" accesskey="p" rel="prev">Lists</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Vectors-1"></span><h2 class="chapter">8 Vectors</h2>

<span id="index-vector-_0028defn_0029"></span>
<p><em>Vectors</em> are heterogenous structures whose elements are indexed by
exact non-negative integers.  A vector typically occupies less space
than a list of the same length, and the average time required to access
a randomly chosen element is typically less for the vector than for the
list.
</p>
<span id="index-length_002c-of-vector-_0028defn_0029"></span>
<span id="index-index_002c-of-vector-_0028defn_0029"></span>
<span id="index-valid-index_002c-of-vector-_0028defn_0029"></span>
<span id="index-vector-length-_0028defn_0029"></span>
<span id="index-vector-index-_0028defn_0029"></span>
<p>The <em>length</em> of a vector is the number of elements that it contains.
This number is an exact non-negative integer that is fixed when the
vector is created.  The <em>valid indexes</em> of a vector are the exact
non-negative integers less than the length of the vector.  The first
element in a vector is indexed by zero, and the last element is indexed
by one less than the length of the vector.
</p>
<span id="index-external-representation_002c-for-vector"></span>
<span id="index-_0023_0028-as-external-representation"></span>
<span id="index-parenthesis_002c-as-external-representation-1"></span>
<span id="index-_0023_0028"></span>
<p>Vectors are written using the notation <code>#(<var>object</var> &hellip;)</code>.
For example, a vector of length 3 containing the number zero in element
0, the list <code>(2 2 2 2)</code> in element 1, and the string <code>&quot;Anna&quot;</code>
in element 2 can be written as
</p>
<div class="example">
<pre class="example">#(0 (2 2 2 2) &quot;Anna&quot;)
</pre></div>

<p>Note that this is the external representation of a vector, not an
expression evaluating to a vector.  Like list constants, vector
constants must be quoted:
</p>
<div class="example">
<pre class="example">'#(0 (2 2 2 2) &quot;Anna&quot;)          &rArr;  #(0 (2 2 2 2) &quot;Anna&quot;)
</pre></div>

<span id="index-subvector-_0028defn_0029"></span>
<span id="index-start_002c-of-subvector-_0028defn_0029"></span>
<span id="index-end_002c-of-subvector-_0028defn_0029"></span>
<span id="index-index_002c-of-subvector-_0028defn_0029"></span>
<span id="index-valid-index_002c-of-subvector-_0028defn_0029"></span>
<p>A number of the vector procedures operate on subvectors.  A
<em>subvector</em> is a segment of a vector that is specified by two exact
non-negative integers, <var>start</var> and <var>end</var>.  <var>Start</var> is the
index of the first element that is included in the subvector, and
<var>end</var> is one greater than the index of the last element that is
included in the subvector.  Thus if <var>start</var> and <var>end</var> are the
same, they refer to a null subvector, and if <var>start</var> is zero and
<var>end</var> is the length of the vector, they refer to the entire vector.
The <em>valid indexes</em> of a subvector are the exact integers between
<var>start</var> inclusive and <var>end</var> exclusive.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Construction-of-Vectors.html" accesskey="1">Construction of Vectors</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Selecting-Vector-Components.html" accesskey="2">Selecting Vector Components</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Cutting-Vectors.html" accesskey="3">Cutting Vectors</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Modifying-Vectors.html" accesskey="4">Modifying Vectors</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Bit-Strings.html" accesskey="n" rel="next">Bit Strings</a>, Previous: <a href="Lists.html" accesskey="p" rel="prev">Lists</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
