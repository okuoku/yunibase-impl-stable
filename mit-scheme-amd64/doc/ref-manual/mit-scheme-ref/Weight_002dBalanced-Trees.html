<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="Construction-of-Weight_002dBalanced-Trees.html" rel="next" title="Construction of Weight-Balanced Trees">
<link href="Red_002dBlack-Trees.html" rel="prev" title="Red-Black Trees">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Weight_002dBalanced-Trees"></span><div class="header">
<p>
Next: <a href="Associative-Maps.html" accesskey="n" rel="next">Associative Maps</a>, Previous: <a href="Red_002dBlack-Trees.html" accesskey="p" rel="prev">Red-Black Trees</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Weight_002dBalanced-Trees-1"></span><h3 class="section">11.7 Weight-Balanced Trees</h3>

<span id="index-trees_002c-balanced-binary-1"></span>
<span id="index-balanced-binary-trees-1"></span>
<span id="index-binary-trees-1"></span>
<span id="index-weight_002dbalanced-binary-trees"></span>
<p>Balanced binary trees are a useful data structure for maintaining large
sets of ordered objects or sets of associations whose keys are ordered.
MIT/GNU Scheme has a comprehensive implementation of weight-balanced binary
trees which has several advantages over the other data structures for
large aggregates:
</p>
<ul>
<li> In addition to the usual element-level operations like insertion,
deletion and lookup, there is a full complement of collection-level
operations, like set intersection, set union and subset test, all of
which are implemented with good orders of growth in time and space.
This makes weight-balanced trees ideal for rapid prototyping of
functionally derived specifications.

</li><li> An element in a tree may be indexed by its position under the ordering
of the keys, and the ordinal position of an element may be determined,
both with reasonable efficiency.

</li><li> Operations to find and remove minimum element make weight-balanced trees
simple to use for priority queues.

</li><li> The implementation is <em>functional</em> rather than <em>imperative</em>.
This means that operations like &lsquo;inserting&rsquo; an association in a tree do
not destroy the old tree, in much the same way that <code>(+ 1 x)</code>
modifies neither the constant 1 nor the value bound to <code>x</code>.  The
trees are referentially transparent thus the programmer need not worry
about copying the trees.  Referential transparency allows space
efficiency to be achieved by sharing subtrees.
</li></ul>

<p>These features make weight-balanced trees suitable for a wide range of
applications, especially those that require large numbers of sets or
discrete maps.  Applications that have a few global databases and/or
concentrate on element-level operations like insertion and lookup are
probably better off using hash tables or red-black trees.
</p>
<p>The <em>size</em> of a tree is the number of associations that it
contains.  Weight-balanced binary trees are balanced to keep the sizes
of the subtrees of each node within a constant factor of each other.
This ensures logarithmic times for single-path operations (like lookup
and insertion).  A weight-balanced tree takes space that is proportional
to the number of associations in the tree.  For the current
implementation, the constant of proportionality is six words per
association.
</p>
<span id="index-binary-trees_002c-as-sets"></span>
<span id="index-binary-trees_002c-as-discrete-maps"></span>
<span id="index-sets_002c-using-binary-trees"></span>
<span id="index-discrete-maps_002c-using-binary-trees"></span>
<p>Weight-balanced trees can be used as an implementation for either
discrete sets or discrete maps (associations).  Sets are implemented by
ignoring the datum that is associated with the key.  Under this scheme
if an association exists in the tree this indicates that the key of the
association is a member of the set.  Typically a value such as
<code>()</code>, <code>#t</code> or <code>#f</code> is associated with the key.
</p>
<p>Many operations can be viewed as computing a result that, depending on
whether the tree arguments are thought of as sets or maps, is known by
two different names.  An example is <code>wt-tree/member?</code>, which, when
regarding the tree argument as a set, computes the set membership
operation, but, when regarding the tree as a discrete map,
<code>wt-tree/member?</code> is the predicate testing if the map is defined at
an element in its domain.  Most names in this package have been chosen
based on interpreting the trees as sets, hence the name
<code>wt-tree/member?</code> rather than <code>wt-tree/defined-at?</code>.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Construction-of-Weight_002dBalanced-Trees.html" accesskey="1">Construction of Weight-Balanced Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Basic-Operations-on-Weight_002dBalanced-Trees.html" accesskey="2">Basic Operations on Weight-Balanced Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Advanced-Operations-on-Weight_002dBalanced-Trees.html" accesskey="3">Advanced Operations on Weight-Balanced Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Indexing-Operations-on-Weight_002dBalanced-Trees.html" accesskey="4">Indexing Operations on Weight-Balanced Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Associative-Maps.html" accesskey="n" rel="next">Associative Maps</a>, Previous: <a href="Red_002dBlack-Trees.html" accesskey="p" rel="prev">Red-Black Trees</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
