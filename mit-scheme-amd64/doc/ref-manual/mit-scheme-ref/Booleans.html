<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Booleans (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Booleans (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Booleans (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Symbols.html" rel="next" title="Symbols">
<link href="Miscellaneous-Datatypes.html" rel="prev" title="Miscellaneous Datatypes">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Booleans"></span><div class="header">
<p>
Next: <a href="Symbols.html" accesskey="n" rel="next">Symbols</a>, Previous: <a href="Miscellaneous-Datatypes.html" accesskey="p" rel="prev">Miscellaneous Datatypes</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Booleans-1"></span><h3 class="section">10.1 Booleans</h3>

<span id="index-_0023t-2"></span>
<span id="index-_0023f-2"></span>
<span id="index-_0023t-as-external-representation"></span>
<span id="index-_0023f-as-external-representation"></span>
<span id="index-boolean-object-_0028defn_0029"></span>
<span id="index-true_002c-boolean-object-_0028defn_0029"></span>
<span id="index-false_002c-boolean-object-_0028defn_0029"></span>
<p>The <em>boolean objects</em> are <em>true</em> and <em>false</em>.  The boolean
constant true is written as &lsquo;<samp>#t</samp>&rsquo;, and the boolean constant false is
written as &lsquo;<samp>#f</samp>&rsquo;.
</p>
<span id="index-if-1"></span>
<span id="index-cond-3"></span>
<span id="index-and-1"></span>
<span id="index-or-1"></span>
<p>The primary use for boolean objects is in the conditional expressions
<code>if</code>, <code>cond</code>, <code>and</code>, and <code>or</code>; the behavior of these
expressions is determined by whether objects are true or false.  These
expressions count only <code>#f</code> as false.  They count everything else,
including <code>#t</code>, pairs, symbols, numbers, strings, vectors, and
procedures as true (but see <a href="True-and-False.html">True and False</a>).
</p>
<span id="index-t"></span>
<span id="index-nil"></span>
<p>Programmers accustomed to other dialects of Lisp should note that Scheme
distinguishes <code>#f</code> and the empty list from the symbol <code>nil</code>.
Similarly, <code>#t</code> is distinguished from the symbol <code>t</code>.  In
fact, the boolean objects (and the empty list) are not symbols at all.
</p>
<p>Boolean constants evaluate to themselves, so you don&rsquo;t need to quote
them.
</p>
<div class="example">
<pre class="example">#t                                      &rArr;  #t
#f                                      &rArr;  #f
'#f                                     &rArr;  #f
t                                       error&rarr; Unbound variable
</pre></div>

<dl>
<dt id="index-false">variable: <strong>false</strong></dt>
<dt id="index-true">variable: <strong>true</strong></dt>
<dd><p>These variables are bound to the objects <code>#f</code> and <code>#t</code>
respectively.  The compiler, given the <code>usual-integrations</code>
declaration, replaces references to these variables with their
respective values.
</p>
<p>Note that the symbol <code>true</code> is not equivalent to <code>#t</code>, and the
symbol <code>false</code> is not equivalent to <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-boolean_003f">standard procedure: <strong>boolean?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-boolean"></span>
<p>Returns <code>#t</code> if <var>object</var> is either <code>#t</code> or <code>#f</code>;
otherwise returns <code>#f</code>.
</p>
<div class="example">
<pre class="example">(boolean? #f)                           &rArr;  #t
(boolean? 0)                            &rArr;  #f
</pre></div>
</dd></dl>

<dl>
<dt id="index-not">standard procedure: <strong>not</strong> <em>object</em></dt>
<dt id="index-false_003f">procedure: <strong>false?</strong> <em>object</em></dt>
<dd><span id="index-false_002c-predicate-for"></span>
<span id="index-inverse_002c-of-boolean-object"></span>
<p>These procedures return <code>#t</code> if <var>object</var> is false; otherwise
they return <code>#f</code>.  In other words they <em>invert</em> boolean
values.  These two procedures have identical semantics; their names are
different to give different connotations to the test.
</p>
<div class="example">
<pre class="example">(not #t)                                &rArr;  #f
(not 3)                                 &rArr;  #f
(not (list 3))                          &rArr;  #f
(not #f)                                &rArr;  #t
</pre></div>
</dd></dl>

<dl>
<dt id="index-procedure-1">extended standard procedure: <strong>procedure</strong> <em>boolean=? boolean1 boolean2 boolean3 &hellip;</em></dt>
<dd><span id="index-boolean-object_002c-equivalence-predicate"></span>
<span id="index-equivalence-predicate_002c-for-boolean-objects"></span>
<span id="index-comparison_002c-of-boolean-objects"></span>
<p>This predicate is true iff the <var>boolean</var> args are either all true or all false.
</p>
<p>Implementation note: The standard requires this procedure&rsquo;s arguments
to satisfy <code>boolean?</code>, but MIT/GNU Scheme allows any object to be
an argument.
</p></dd></dl>

<dl>
<dt id="index-boolean_002fand">procedure: <strong>boolean/and</strong> <em>object &hellip;</em></dt>
<dd><p>This procedure returns <code>#t</code> if none of its arguments are <code>#f</code>.
Otherwise it returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-boolean_002for">procedure: <strong>boolean/or</strong> <em>object &hellip;</em></dt>
<dd><p>This procedure returns <code>#f</code> if all of its arguments are <code>#f</code>.
Otherwise it returns <code>#t</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Symbols.html" accesskey="n" rel="next">Symbols</a>, Previous: <a href="Miscellaneous-Datatypes.html" accesskey="p" rel="prev">Miscellaneous Datatypes</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
