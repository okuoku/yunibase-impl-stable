<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Pathnames (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Operating_002dSystem-Interface.html" rel="up" title="Operating-System Interface">
<link href="Filenames-and-Pathnames.html" rel="next" title="Filenames and Pathnames">
<link href="Operating_002dSystem-Interface.html" rel="prev" title="Operating-System Interface">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Pathnames"></span><div class="header">
<p>
Next: <a href="Working-Directory.html" accesskey="n" rel="next">Working Directory</a>, Previous: <a href="Operating_002dSystem-Interface.html" accesskey="p" rel="prev">Operating-System Interface</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Pathnames-1"></span><h3 class="section">15.1 Pathnames</h3>

<span id="index-file-name"></span>
<p>MIT/GNU Scheme programs need to use names to designate files.  The main
difficulty in dealing with names of files is that different file systems
have different naming formats for files.  For example, here is a table
of several file systems (actually, operating systems that provide file
systems) and what equivalent file names might look like for each one:
</p>
<div class="example">
<pre class="example">System          File Name
------          ---------
TOPS-20         &lt;LISPIO&gt;FORMAT.FASL.13
TOPS-10         FORMAT.FAS[1,4]
ITS             LISPIO;FORMAT FASL
MULTICS         &gt;udd&gt;LispIO&gt;format.fasl
TENEX           &lt;LISPIO&gt;FORMAT.FASL;13
VAX/VMS         [LISPIO]FORMAT.FAS;13
UNIX            /usr/lispio/format.fasl
DOS             C:\USR\LISPIO\FORMAT.FAS
</pre></div>

<span id="index-filename-_0028defn_0029"></span>
<span id="index-pathname-_0028defn_0029"></span>
<p>It would be impossible for each program that deals with file names to
know about each different file name format that exists; a new operating
system to which Scheme was ported might use a format different from any
of its predecessors.  Therefore, MIT/GNU Scheme provides <em>two</em> ways to
represent file names: <em>filenames</em> (also called <em>namestrings</em>),
which are strings in the implementation-dependent form customary for the
file system, and <em>pathnames</em>, which are special abstract data
objects that represent file names in an implementation-independent way.
Procedures are provided to convert between these two representations,
and all manipulations of files can be expressed in machine-independent
terms by using pathnames.
</p>
<span id="index-host_002c-in-filename"></span>
<p>In order to allow MIT/GNU Scheme programs to operate in a network
environment that may have more than one kind of file system, the
pathname facility allows a file name to specify which file system is to
be used.  In this context, each file system is called a <em>host</em>, in
keeping with the usual networking terminology.<a id="DOCF15" href="#FOOT15"><sup>15</sup></a>
</p>
<p>Note that the examples given in this section are specific to unix
pathnames.  Pathnames for other operating systems have different
external representations.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Filenames-and-Pathnames.html" accesskey="1">Filenames and Pathnames</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Components-of-Pathnames.html" accesskey="2">Components of Pathnames</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Operations-on-Pathnames.html" accesskey="3">Operations on Pathnames</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Miscellaneous-Pathnames.html" accesskey="4">Miscellaneous Pathnames</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT15" href="#DOCF15">(15)</a></h3>
<p>This
introduction is adapted from <cite>Common Lisp, The Language</cite>, second
edition, section 23.1.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Working-Directory.html" accesskey="n" rel="next">Working Directory</a>, Previous: <a href="Operating_002dSystem-Interface.html" accesskey="p" rel="prev">Operating-System Interface</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
