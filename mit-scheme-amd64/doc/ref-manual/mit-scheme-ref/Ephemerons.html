<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ephemerons (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Ephemerons (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Ephemerons (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Weak-References.html" rel="up" title="Weak References">
<link href="Reference-barriers.html" rel="next" title="Reference barriers">
<link href="Weak-Pairs.html" rel="prev" title="Weak Pairs">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Ephemerons"></span><div class="header">
<p>
Next: <a href="Reference-barriers.html" accesskey="n" rel="next">Reference barriers</a>, Previous: <a href="Weak-Pairs.html" accesskey="p" rel="prev">Weak Pairs</a>, Up: <a href="Weak-References.html" accesskey="u" rel="up">Weak References</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Ephemerons-1"></span><h4 class="subsection">10.7.2 Ephemerons</h4>

<span id="index-ephemeron-_0028defn_0029"></span>
<span id="index-ephemeron_002c-broken"></span>
<span id="index-broken-ephemeron"></span>
<p>An
<em>ephemeron</em> is an object with two weakly referenced components called
its <em>key</em> and <em>datum</em>.  The garbage collector drops an
ephemeron&rsquo;s references to both key and datum, rendering the ephemeron
<em>broken</em>, if and only if the garbage collector can prove that
there are no strong references to the key.  In other words, an
ephemeron is broken when nobody else cares about its key.  In
particular, the datum holding a reference to the key will not in
itself prevent the ephemeron from becoming broken; in contrast,
See <a href="Weak-Pairs.html">Weak Pairs</a>.  Once broken, ephemerons never cease to be broken;
setting the key or datum of a broken ephemeron with
<code>set-ephemeron-key!</code> or <code>set-ephemeron-datum!</code> has no
effect.  Note that an ephemeron&rsquo;s reference to its datum may be
dropped even if the datum is still reachable; all that matters is
whether the key is reachable.
</p>
<p>Ephemerons are considerably heavier-weight than weak pairs, because
garbage-collecting ephemerons is more complicated than
garbage-collecting weak pairs.  Each ephemeron needs five words of
storage, rather than the two words needed by a weak pair.  However,
while the garbage collector spends more time on ephemerons than on
other objects, the amount of time it spends on ephemerons scales
linearly with the number of live ephemerons, which is how its running
time scales with the total number of live objects anyway.
</p>
<dl>
<dt id="index-ephemeron_003f">procedure: <strong>ephemeron?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-ephemeron"></span>
<p>Returns <code>#t</code> if <var>object</var> is a ephemeron; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-make_002dephemeron">procedure: <strong>make-ephemeron</strong> <em>key datum</em></dt>
<dd><span id="index-construction_002c-of-ephemeron"></span>
<p>Allocates and returns a new ephemeron, with components <var>key</var> and
<var>datum</var>.
</p></dd></dl>

<dl>
<dt id="index-ephemeron_002dbroken_003f">procedure: <strong>ephemeron-broken?</strong> <em>ephemeron</em></dt>
<dd><p>Returns <code>#t</code> if the garbage collector has dropped
<var>ephemeron</var>&rsquo;s references to its key and datum; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-ephemeron_002dkey">procedure: <strong>ephemeron-key</strong> <em>ephemeron</em></dt>
<dt id="index-ephemeron_002ddatum">procedure: <strong>ephemeron-datum</strong> <em>ephemeron</em></dt>
<dd><span id="index-selection_002c-of-ephemeron-component"></span>
<span id="index-component-selection_002c-of-ephemeron"></span>
<p>These return the key or datum component, respectively, of
<var>ephemeron</var>.  If <var>ephemeron</var> has been broken, these operations
return <code>#f</code>, but they can also return <code>#f</code> if that is the
value that was stored in the key or value component.
</p></dd></dl>

<dl>
<dt id="index-set_002dephemeron_002dkey_0021">procedure: <strong>set-ephemeron-key!</strong> <em>ephemeron object</em></dt>
<dt id="index-set_002dephemeron_002ddatum_0021">procedure: <strong>set-ephemeron-datum!</strong> <em>ephemeron object</em></dt>
<dd><p>These set the key or datum component, respectively, of <var>ephemeron</var>
to <var>object</var> and return an unspecified result.  If <var>ephemeron</var>
is broken, neither of these operations has any effect.
</p></dd></dl>

<p>Like <code>weak-pair/car?</code>, <code>ephemeron-broken?</code> must be used with
care.  If <code>(ephemeron-broken? <var>ephemeron</var>)</code> yields false, it
guarantees only that prior evaluations of <code>(ephemeron-key
<var>ephemeron</var>)</code> or <code>(ephemeron-datum <var>ephemeron</var>)</code> yielded the key
or datum that was stored in the ephemeron, but it makes no guarantees
about subsequent calls to <code>ephemeron-key</code> or
<code>ephemeron-datum</code>: the garbage collector may run and break the
ephemeron immediately after <code>ephemeron-broken?</code> returns.  Thus,
the correct idiom to fetch an ephemeron&rsquo;s key and datum and use them
if the ephemeron is not broken is
</p>
<div class="example">
<pre class="example">(let ((key (ephemeron-key ephemeron))
      (datum (ephemeron-datum ephemeron)))
  (if (ephemeron-broken? ephemeron)
      &hellip; <span class="roman">broken case</span> &hellip;
      &hellip; <span class="roman">code using <var>key</var> and <var>datum</var></span> &hellip;))
</pre></div>

<hr>
<div class="header">
<p>
Next: <a href="Reference-barriers.html" accesskey="n" rel="next">Reference barriers</a>, Previous: <a href="Weak-Pairs.html" accesskey="p" rel="prev">Weak Pairs</a>, Up: <a href="Weak-References.html" accesskey="u" rel="up">Weak References</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
