<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Resizing of Hash Tables (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Resizing of Hash Tables (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Resizing of Hash Tables (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Hash-Tables.html" rel="up" title="Hash Tables">
<link href="Address-Hashing.html" rel="next" title="Address Hashing">
<link href="Basic-Hash-Table-Operations.html" rel="prev" title="Basic Hash Table Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Resizing-of-Hash-Tables"></span><div class="header">
<p>
Next: <a href="Address-Hashing.html" accesskey="n" rel="next">Address Hashing</a>, Previous: <a href="Basic-Hash-Table-Operations.html" accesskey="p" rel="prev">Basic Hash Table Operations</a>, Up: <a href="Hash-Tables.html" accesskey="u" rel="up">Hash Tables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Resizing-of-Hash-Tables-1"></span><h4 class="subsection">11.4.3 Resizing of Hash Tables</h4>

<span id="index-resizing_002c-of-hash-table"></span>
<span id="index-size_002c-of-hash-table-_0028defn_0029"></span>
<p>Normally, hash tables automatically resize themselves according to need.
Because of this, the programmer need not be concerned with management of
the table&rsquo;s size.  However, some limited control over the table&rsquo;s size
is provided, which will be discussed below.  This discussion involves
two concepts, <em>usable size</em> and <em>physical size</em>, which we will
now define.
</p>
<span id="index-usable-size_002c-of-hash-table-_0028defn_0029"></span>
<p>The <em>usable size</em> of a hash table is the number of associations that
the table can hold at a given time.  If the number of associations in
the table exceeds the usable size, the table will automatically grow,
increasing the usable size to a new value that is sufficient to hold the
associations.
</p>
<span id="index-physical-size_002c-of-hash-table-_0028defn_0029"></span>
<p>The <em>physical size</em> is an abstract measure of a hash table that
specifies how much space is allocated to hold the associations of the
table.  The physical size is always greater than or equal to the usable
size.  The physical size is not interesting in itself; it is interesting
only for its effect on the performance of the hash table.  While the
average performance of a hash-table lookup is bounded by a constant, the
worst-case performance is not.  For a table containing a given number of
associations, increasing the physical size of the table decreases the
probability that worse-than-average performance will occur.
</p>
<p>The physical size of a hash table is statistically related to the number
of associations.  However, it is possible to place bounds on the
physical size, and from this to estimate the amount of space used by the
table:
</p>
<div class="example">
<pre class="example">(define (hash-table-space-bounds count rehash-size rehash-threshold)
  (let ((tf (/ 1 rehash-threshold)))
    (values (if (exact-integer? rehash-size)
                (- (* count (+ 4 tf))
                   (* tf (+ rehash-size rehash-size)))
                (* count (+ 4 (/ tf (* rehash-size rehash-size)))))
            (* count (+ 4 tf)))))
</pre></div>

<p>What this formula shows is that, for a &ldquo;normal&rdquo; rehash size (that is,
not an exact integer), the amount of space used by the hash table is
proportional to the number of associations in the table.  The constant
of proportionality varies statistically, with the low bound being
</p>
<div class="example">
<pre class="example">(+ 4 (/ (/ 1 rehash-threshold) (* rehash-size rehash-size)))
</pre></div>

<p>and the high bound being
</p>
<div class="example">
<pre class="example">(+ 4 (/ 1 rehash-threshold))
</pre></div>

<p>which, for the default values of these parameters, are <code>4.25</code> and
<code>5</code>, respectively.  Reducing the rehash size will tighten these
bounds, but increases the amount of time spent resizing, so you can see
that the rehash size gives some control over the time-space tradeoff of
the table.
</p>
<p>The programmer can control the size of a hash table by means of three
parameters:
</p>
<ul>
<li> Each table&rsquo;s <var>initial-size</var> may be specified when the table is
created.

</li><li> Each table has a <em>rehash size</em> that specifies how the size of the
table is changed when it is necessary to grow or shrink the table.

</li><li> Each table has a <em>rehash threshold</em> that specifies the relationship
of the table&rsquo;s physical size to its usable size.
</li></ul>

<span id="index-initial-size_002c-of-hash-table"></span>
<p>If the programmer knows that the table will initially contain a specific
number of items, <var>initial-size</var> can be given when the table is
created.  If <var>initial-size</var> is an exact non-negative integer, it
specifies the initial usable size of the hash table; the table will not
change size until the number of items in the table exceeds
<var>initial-size</var>, after which automatic resizing is enabled and
<var>initial-size</var> no longer has any effect.  Otherwise, if
<var>initial-size</var> is not given or is <code>#f</code>, the table is
initialized to an unspecified size and automatic resizing is immediately
enabled.
</p>
<span id="index-rehash-size_002c-of-hash-table-_0028defn_0029"></span>
<p>The <em>rehash size</em> specifies how much to increase the usable size of
the hash table when it becomes full.  It is either an exact positive
integer, or a real number greater than one.  If it is an integer, the
new size is the sum of the old size and the rehash size.  Otherwise, it
is a real number, and the new size is the product of the old size and
the rehash size.  Increasing the rehash size decreases the average cost
of an insertion, but increases the average amount of space used by the
table.  The rehash size of a table may be altered dynamically by the
application in order to optimize the resizing of the table; for example,
if the table will grow quickly for a known period and afterwards will
not change size, performance might be improved by using a large rehash
size during the growth phase and a small one during the static phase.
The default rehash size of a newly constructed hash table is <code>2.0</code>.
</p>
<p><strong>Warning</strong>: The use of an exact positive integer for a rehash
size is almost always undesirable; this option is provided solely for
compatibility with the Common Lisp hash-table mechanism.  The reason for
this has to do with the time penalty for resizing the hash table.  The
time needed to resize a hash table is proportional to the
number of associations in the table.  This resizing cost is
<em>amortized</em> across the insertions required to fill the table to the
point where it needs to grow again.  If the table grows by an amount
proportional to the number of associations, then the cost of
resizing and the increase in size are both proportional to the
number of associations, so the <em>amortized cost</em> of an insertion
operation is still bounded by a constant.  However, if the table grows
by a constant amount, this is not true: the amortized cost of an
insertion is not bounded by a constant.  Thus, using a constant rehash
size means that the average cost of an insertion increases
proportionally to the number of associations in the hash table.
</p>
<span id="index-rehash-threshold_002c-of-hash-table-_0028defn_0029"></span>
<p>The <em>rehash threshold</em> is a real number, between zero exclusive and
one inclusive, that specifies the ratio between a hash table&rsquo;s usable
size and its physical size.  Decreasing the rehash threshold decreases
the probability of worse-than-average insertion, deletion, and lookup
times, but increases the physical size of the table for a given usable
size.  The default rehash threshold of a newly constructed hash table is
<code>1</code>.
</p>
<dl>
<dt id="index-hash_002dtable_002dgrow_002dsize">procedure: <strong>hash-table-grow-size</strong> <em>hash-table</em></dt>
<dt id="index-hash_002dtable_002fsize">obsolete procedure: <strong>hash-table/size</strong> <em>hash-table</em></dt>
<dd><p>Returns the usable size of <var>hash-table</var> as an exact positive
integer.  This is the maximum number of associations that
<var>hash-table</var> can hold before it will grow.
</p></dd></dl>

<dl>
<dt id="index-hash_002dtable_002dshrink_002dsize">procedure: <strong>hash-table-shrink-size</strong> <em>hash-table</em></dt>
<dd><p>Returns the minimum number of associations that <var>hash-table</var> can
hold before it will shrink.
</p></dd></dl>

<dl>
<dt id="index-hash_002dtable_002drehash_002dsize">procedure: <strong>hash-table-rehash-size</strong> <em>hash-table</em></dt>
<dt id="index-hash_002dtable_002frehash_002dsize">obsolete procedure: <strong>hash-table/rehash-size</strong> <em>hash-table</em></dt>
<dd><p>Returns the rehash size of <var>hash-table</var>.
</p></dd></dl>

<dl>
<dt id="index-set_002dhash_002dtable_002drehash_002dsize_0021">procedure: <strong>set-hash-table-rehash-size!</strong> <em>hash-table x</em></dt>
<dt id="index-set_002dhash_002dtable_002frehash_002dsize_0021">obsolete procedure: <strong>set-hash-table/rehash-size!</strong> <em>hash-table x</em></dt>
<dd><p><var>X</var> must be either an exact positive integer, or a real number that
is greater than one.  Sets the rehash size of <var>hash-table</var> to
<var>x</var> and returns an unspecified result.  This operation adjusts the
&ldquo;shrink threshold&rdquo; of the table; the table might shrink if the number
of associations is less than the new threshold.
</p></dd></dl>

<dl>
<dt id="index-hash_002dtable_002drehash_002dthreshold">procedure: <strong>hash-table-rehash-threshold</strong> <em>hash-table</em></dt>
<dt id="index-hash_002dtable_002frehash_002dthreshold">obsolete procedure: <strong>hash-table/rehash-threshold</strong> <em>hash-table</em></dt>
<dd><p>Returns the rehash threshold of <var>hash-table</var>.
</p></dd></dl>

<dl>
<dt id="index-set_002dhash_002dtable_002drehash_002dthreshold_0021">procedure: <strong>set-hash-table-rehash-threshold!</strong> <em>hash-table x</em></dt>
<dt id="index-set_002dhash_002dtable_002frehash_002dthreshold_0021">obsolete procedure: <strong>set-hash-table/rehash-threshold!</strong> <em>hash-table x</em></dt>
<dd><p><var>X</var> must be a real number between zero exclusive and one inclusive.
Sets the rehash threshold of <var>hash-table</var> to <var>x</var> and returns an
unspecified result.  This operation does not change the usable size of
the table, but it usually changes the physical size of the table, which
causes the table to be rehashed.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Address-Hashing.html" accesskey="n" rel="next">Address Hashing</a>, Previous: <a href="Basic-Hash-Table-Operations.html" accesskey="p" rel="prev">Basic Hash Table Operations</a>, Up: <a href="Hash-Tables.html" accesskey="u" rel="up">Hash Tables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
