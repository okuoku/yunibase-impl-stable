<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>External Representation of Time (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="External Representation of Time (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="External Representation of Time (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Date-and-Time.html" rel="up" title="Date and Time">
<link href="Machine-Time.html" rel="next" title="Machine Time">
<link href="Time_002dFormat-Conversion.html" rel="prev" title="Time-Format Conversion">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="External-Representation-of-Time"></span><div class="header">
<p>
Previous: <a href="Time_002dFormat-Conversion.html" accesskey="p" rel="prev">Time-Format Conversion</a>, Up: <a href="Date-and-Time.html" accesskey="u" rel="up">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="External-Representation-of-Time-1"></span><h4 class="subsection">15.5.5 External Representation of Time</h4>

<p>The normal external representation for time is the time string, as
described above.  The procedures in this section generate alternate
external representations of time which are more verbose and may be more
suitable for presentation to human readers.
</p>
<dl>
<dt id="index-decoded_002dtime_002fdate_002dstring">procedure: <strong>decoded-time/date-string</strong> <em>decoded-time</em></dt>
<dt id="index-decoded_002dtime_002ftime_002dstring">procedure: <strong>decoded-time/time-string</strong> <em>decoded-time</em></dt>
<dd><p>These procedures return strings containing external representations of
the date and time, respectively, represented by <var>decoded-time</var>.  The
results are implicitly in local time.
</p>
<div class="example">
<pre class="example">(decoded-time/date-string (local-decoded-time))
    &rArr; &quot;Tuesday March 30, 1999&quot;
(decoded-time/time-string (local-decoded-time))
    &rArr; &quot;11:22:38 AM&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-day_002dof_002dweek_002flong_002dstring">procedure: <strong>day-of-week/long-string</strong> <em>day-of-week</em></dt>
<dt id="index-day_002dof_002dweek_002fshort_002dstring">procedure: <strong>day-of-week/short-string</strong> <em>day-of-week</em></dt>
<dd><p>Returns a string representing the given <var>day-of-week</var>.  The argument
must be an exact non-negative integer between <code>0</code> and <code>6</code>
inclusive.  <code>day-of-week/long-string</code> returns a long string that
fully spells out the name of the day.  <code>day-of-week/short-string</code>
returns a shortened string that abbreviates the day to three letters.
</p>
<div class="example">
<pre class="example">(day-of-week/long-string 0)  &rArr; &quot;Monday&quot;
(day-of-week/short-string 0) &rArr; &quot;Mon&quot;
(day-of-week/short-string 3) &rArr; &quot;Thu&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-month_002flong_002dstring">procedure: <strong>month/long-string</strong> <em>month</em></dt>
<dt id="index-month_002fshort_002dstring">procedure: <strong>month/short-string</strong> <em>month</em></dt>
<dd><p>Returns a string representing the given <var>month</var>.  The argument must
be an exact non-negative integer between <code>1</code> and <code>12</code>
inclusive.  <code>month/long-string</code> returns a long string that fully
spells out the name of the month.  <code>month/short-string</code> returns a
shortened string that abbreviates the month to three letters.
</p>
<div class="example">
<pre class="example">(month/long-string 1)   &rArr; &quot;January&quot;
(month/short-string 1)  &rArr; &quot;Jan&quot;
(month/short-string 10) &rArr; &quot;Oct&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-time_002dzone_002d_003estring">procedure: <strong>time-zone-&gt;string</strong></dt>
<dd><p>Returns a string corresponding to the given time zone.  This string is
the same string that is used to generate RFC-822 time strings.
</p>
<div class="example">
<pre class="example">(time-zone-&gt;string 5)    &rArr; &quot;-0500&quot;
(time-zone-&gt;string -4)   &rArr; &quot;+0400&quot;
(time-zone-&gt;string 11/2) &rArr; &quot;-0530&quot;
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Time_002dFormat-Conversion.html" accesskey="p" rel="prev">Time-Format Conversion</a>, Up: <a href="Date-and-Time.html" accesskey="u" rel="up">Date and Time</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
