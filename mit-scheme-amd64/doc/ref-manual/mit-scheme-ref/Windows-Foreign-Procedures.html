<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Windows Foreign Procedures (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Windows Foreign Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Windows Foreign Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Foreign-function-interface.html" rel="up" title="Foreign function interface">
<link href="Win32-API-names-and-procedures.html" rel="next" title="Win32 API names and procedures">
<link href="Windows-Types.html" rel="prev" title="Windows Types">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Windows-Foreign-Procedures"></span><div class="header">
<p>
Next: <a href="Win32-API-names-and-procedures.html" accesskey="n" rel="next">Win32 API names and procedures</a>, Previous: <a href="Windows-Types.html" accesskey="p" rel="prev">Windows Types</a>, Up: <a href="Foreign-function-interface.html" accesskey="u" rel="up">Foreign function interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Windows-Foreign-Procedures-1"></span><h4 class="subsection">18.2.2 Windows Foreign Procedures</h4>

<p>Foreign procedures are declared as callable entry-points in a module,
usually a dynamically linked library (DLL).
</p>

<dl>
<dt id="index-find_002dmodule">procedure: <strong>find-module</strong> <em>name</em></dt>
<dd><span id="index-loading-DLLs"></span>
<span id="index-DLL_002c-loading"></span>
<p>Returns a module suitable for use in creating procedures with
<code>windows-procedure</code>.  <var>Name</var> is a string which is the name of a
DLL file.  Internally, <code>find-module</code> uses the <code>LoadLibrary</code>
Win32 API, so <var>name</var> should conform to the specifications for this
call.  <var>Name</var> should be either a full path name of a DLL, or the
name of a DLL that resides in the same directory as the Scheme binary
<samp>SCHEME.EXE</samp> or in the system directory.
</p>
<p>The module returned is a description for the DLL, and the DLL need not
necessarily be linked at or immediately after this call.  DLL modules
are linked on need and unlinked before Scheme exits and when there
are no remaining references to entry points after a garbage-collection.
This behavior ensures that the Scheme system can run when a DLL is
absent, provided the DLL is not actually used (i.e. no attempt is made
to call a procedure in the DLL).
</p></dd></dl>


<dl>
<dt id="index-gdi32_002edll">variable: <strong>gdi32.dll</strong></dt>
<dd><span id="index-DLL_002c-GDI32_002eDLL"></span>
<p>This variable is bound to the module describing the <samp>GDI32.DLL</samp>
library, which contains the Win32 API graphics calls, e.g.
<code>LineTo</code>.
</p></dd></dl>

<dl>
<dt id="index-kernel32_002edll">variable: <strong>kernel32.dll</strong></dt>
<dd><span id="index-DLL_002c-KERNEL32_002eDLL"></span>
<p>This variable is bound to the module describing the <samp>KERNEL32.DLL</samp>
library.
</p></dd></dl>

<dl>
<dt id="index-user32_002edll">variable: <strong>user32.dll</strong></dt>
<dd><span id="index-DLL_002c-USER32_002eDLL"></span>
<p>This variable is bound to the module describing the <samp>USER32.DLL</samp>
library.  This module contains many useful Win32 API procedures, like
<code>MessageBox</code> and <code>SetWindowText</code>.
</p></dd></dl>


<dl>
<dt id="index-windows_002dprocedure">special form: <strong>windows-procedure</strong> <em>(name (parameter type) &hellip;) return-type module entry-name [options]</em></dt>
<dd><span id="index-defining-foreign-procedures"></span>
<p>This form creates a procedure, and could be thought of as
&ldquo;foreign-named-lambda&rdquo;.  The form creates a Scheme procedure that
calls the C procedure identified by the exported entry point
<var>entry-name</var> in the module identified by the value of <var>module</var>.
Both <var>entry-name</var> and <var>module</var> are evaluated at procedure
creation time, so either may be expression.  <var>Entry-name</var> must
evaluate to a string and <var>module</var> must evaluate to a module as
returned by <code>find-module</code>.
These are the only parts of the form that are evaluated at procedure
creation time.
</p>
<p><var>Name</var> is the name of the procedure and is for documentation
purposes only.  This form <em>does not</em> define a procedure called
<var>name</var>.  It is more like <code>lambda</code>.  The name might be used for
debugging and pretty-printing.
</p>
<p>A windows procedure has a fixed number of parameters (i.e. no &lsquo;rest&rsquo;
parameters or &lsquo;varargs&rsquo;), each of which is named and associated with a
windows type <var>type</var>.  Both the name <var>parameter</var> and the windows
type <var>type</var> must be symbols and are not evaluated.  The procedure
returns a value of the windows type <var>return-type</var>.
</p>
<p>The following example creates a procedure that takes a window handle
(<code>hwnd</code>) and a string and returns a boolean (<code>bool</code>) result.
The procedure does this by calling the <code>SetWindowText</code> entry in the
module that is the value of the variable <code>user32.dll</code>.  The
variable <code>set-window-title</code> is defined to have this procedure as
it&rsquo;s value.
</p>
<div class="example">
<pre class="example">(define set-window-title
  (windows-procedure
   (set-window-text (window hwnd) (text string))
   bool user32.dll &quot;SetWindowText&quot;))

(set-window-title my-win &quot;Hi&quot;)
                         &rArr;  #t
                         <span class="roman">;; Changes window&rsquo;s title/text</span>

set-window-title         &rArr;  #[compiled-procedure  &hellip;]
set-window-text          error&rarr;  Unbound variable
</pre></div>


<p>When there are no <var>options</var> the created procedure will (a) check its
arguments against the types, (b) convert the arguments, (c) call the C
procedure and (d) convert the returned value.  No reversion is
performed, even if one of the <var>types</var> has a reversion defined.
(Reverted types are rare [I have never used one], so paying a cost for
this unless it is used seems silly).
</p>
<p>The following options are allowed:
</p>
<dl compact="compact">
<dt><code>with-reversions</code></dt>
<dd><p>The reversions are included in the type conversions.
</p>
</dd>
<dt><code>expand</code></dt>
<dd><p>A synonym for <code>with-reversions</code>.
</p>
</dd>
<dt><var>Scheme code</var></dt>
<dd><p>The <var>Scheme code</var> is placed between steps (a) and (b) in the default
process.  The Scheme code can enforce constraints on the arguments,
including constraints between arguments such as checking that an index
refers to a valid position in a string.
</p></dd>
</dl>

<p>If both options (i.e. <code>with-reversions</code> and Scheme code) are used,
<code>with-reversions</code> must appear first.  There can be arbitrarily many
Scheme expression.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Win32-API-names-and-procedures.html" accesskey="n" rel="next">Win32 API names and procedures</a>, Previous: <a href="Windows-Types.html" accesskey="p" rel="prev">Windows Types</a>, Up: <a href="Foreign-function-interface.html" accesskey="u" rel="up">Foreign function interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
