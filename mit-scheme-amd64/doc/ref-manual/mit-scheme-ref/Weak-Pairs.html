<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Weak Pairs (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Weak Pairs (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Weak Pairs (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Weak-References.html" rel="up" title="Weak References">
<link href="Ephemerons.html" rel="next" title="Ephemerons">
<link href="Weak-References.html" rel="prev" title="Weak References">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Weak-Pairs"></span><div class="header">
<p>
Next: <a href="Ephemerons.html" accesskey="n" rel="next">Ephemerons</a>, Previous: <a href="Weak-References.html" accesskey="p" rel="prev">Weak References</a>, Up: <a href="Weak-References.html" accesskey="u" rel="up">Weak References</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Weak-Pairs-1"></span><h4 class="subsection">10.7.1 Weak Pairs</h4>

<span id="index-weak-pair-_0028defn_0029"></span>
<span id="index-pair_002c-weak-_0028defn_0029"></span>
<p>The car of a <em>weak pair</em> holds its pointer weakly, while the cdr
holds its pointer strongly.  If the object in the car of a weak pair
is not held strongly by any other data structure, it will be
garbage-collected, and the original value replaced with a unique
<em>reclaimed object</em>.
</p>
<p>Note: weak pairs can be defeated by cross references among their
slots.  Consider a weak pair <var>P</var> holding an object <var>A</var> in its
car and an object <var>D</var> in its cdr.  <var>P</var> points to <var>A</var>
weakly and to <var>D</var> strongly.  If <var>D</var> holds <var>A</var> strongly,
however, then <var>P</var> ends up holding <var>A</var> strongly after all.  If
avoiding this is worth a heavier-weight structure, See <a href="Ephemerons.html">Ephemerons</a>.
</p>
<span id="index-pair_003f-2"></span>
<p>Note: weak pairs are <em>not</em> pairs; that is, they do not satisfy the
predicate <code>pair?</code>.
</p>
<dl>
<dt id="index-weak_002dpair_003f">procedure: <strong>weak-pair?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-weak-pair"></span>
<p>Returns <code>#t</code> if <var>object</var> is a weak pair; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-weak_002dcons">procedure: <strong>weak-cons</strong> <em>car cdr</em></dt>
<dd><span id="index-construction_002c-of-weak-pair"></span>
<p>Allocates and returns a new weak pair, with components <var>car</var> and
<var>cdr</var>.  The <var>car</var> component is held weakly.
</p></dd></dl>

<dl>
<dt id="index-gc_002dreclaimed_002dobject_003f">procedure: <strong>gc-reclaimed-object?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is the reclaimed object, and
<code>#f</code> otherwise.
</p></dd></dl>

<dl>
<dt id="index-gc_002dreclaimed_002dobject">procedure: <strong>gc-reclaimed-object</strong></dt>
<dd><p>Returns the reclaimed object.
</p></dd></dl>

<dl>
<dt id="index-weak_002dpair_002fcar_003f">obsolete procedure: <strong>weak-pair/car?</strong> <em>weak-pair</em></dt>
<dd><p>This predicate returns <code>#f</code> if the car of <var>weak-pair</var> has been
garbage-collected; otherwise returns <code>#t</code>.  In other words, it is
true if <var>weak-pair</var> has a valid car component.
</p>
<p>This is equivalent to
</p><div class="example">
<pre class="example">(not (gc-reclaimed-object? (weak-car <var>weak-pair</var>)))
</pre></div>

<p>This predicate has been deprecated; instead use
<code>gc-reclaimed-object?</code>.  Please note that the previously
recommended way to use <code>weak-pair/car?</code> will no longer work, so
any code using it should be rewritten.
</p></dd></dl>

<dl>
<dt id="index-weak_002dcar">procedure: <strong>weak-car</strong> <em>weak-pair</em></dt>
<dd><span id="index-selection_002c-of-weak-pair-component"></span>
<span id="index-component-selection_002c-of-weak-pair"></span>
<p>Returns the car component of <var>weak-pair</var>.  If the car component has
been garbage-collected, this operation returns the reclaimed object.
</p></dd></dl>

<dl>
<dt id="index-weak_002dset_002dcar_0021">procedure: <strong>weak-set-car!</strong> <em>weak-pair object</em></dt>
<dd><p>Sets the car component of <var>weak-pair</var> to <var>object</var> and returns an
unspecified result.
</p></dd></dl>

<dl>
<dt id="index-weak_002dcdr">procedure: <strong>weak-cdr</strong> <em>weak-pair</em></dt>
<dd><p>Returns the cdr component of <var>weak-pair</var>.
</p></dd></dl>

<dl>
<dt id="index-weak_002dset_002dcdr_0021">procedure: <strong>weak-set-cdr!</strong> <em>weak-pair object</em></dt>
<dd><p>Sets the cdr component of <var>weak-pair</var> to <var>object</var> and returns an
unspecified result.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Ephemerons.html" accesskey="n" rel="next">Ephemerons</a>, Previous: <a href="Weak-References.html" accesskey="p" rel="prev">Weak References</a>, Up: <a href="Weak-References.html" accesskey="u" rel="up">Weak References</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
