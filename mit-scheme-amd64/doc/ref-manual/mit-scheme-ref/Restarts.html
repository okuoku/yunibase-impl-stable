<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Restarts (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Restarts (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Restarts (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-System.html" rel="up" title="Error System">
<link href="Establishing-Restart-Code.html" rel="next" title="Establishing Restart Code">
<link href="Condition-Handling.html" rel="prev" title="Condition Handling">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Restarts"></span><div class="header">
<p>
Next: <a href="Condition-Instances.html" accesskey="n" rel="next">Condition Instances</a>, Previous: <a href="Condition-Handling.html" accesskey="p" rel="prev">Condition Handling</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Restarts-1"></span><h3 class="section">16.4 Restarts</h3>

<span id="index-restart-effector-_0028defn_0029"></span>
<span id="index-effector_002c-restart-_0028defn_0029"></span>
<span id="index-restart-_0028defn_0029"></span>
<span id="index-with_002drestart-1"></span>
<span id="index-with_002dsimple_002drestart"></span>
<p>The Scheme error system provides a mechanism, known as <em>restarts</em>,
that helps coordinate condition-signalling code with condition-handling
code.  A module of code that detects and signals conditions can provide
procedures (using <code>with-simple-restart</code> or <code>with-restart</code>) to
be invoked by handlers that wish to continue, abort, or restart the
computation.  These procedures, called <em>restart effectors</em>, are
encapsulated in restart objects.
</p>
<span id="index-find_002drestart"></span>
<span id="index-invoke_002drestart"></span>
<span id="index-invoke_002drestart_002dinteractively"></span>
<p>When a condition object is created, it contains a set of restart
objects, each of which contains a restart effector.  Condition handlers
can inspect the condition they are handling (using <code>find-restart</code>
to find restarts by name, or <code>condition/restarts</code> to see the entire
set), and they can invoke the associated effectors (using
<code>invoke-restart</code> or <code>invoke-restart-interactively</code>).
Effectors can take arguments, and these may be computed directly by the
condition-handling code or by gathering them interactively from the
user.
</p>
<span id="index-abort"></span>
<span id="index-continue"></span>
<span id="index-muffle_002dwarning-2"></span>
<span id="index-retry"></span>
<span id="index-store_002dvalue"></span>
<span id="index-use_002dvalue"></span>
<span id="index-protocol_002c-restart-_0028defn_0029"></span>
<span id="index-restart-protocol"></span>
<p>The names of restarts can be chosen arbitrarily, but the choice of name
is significant.  These names are used to coordinate between the
signalling code (which supplies names for restarts) and the handling
code (which typically chooses a restart effector by the name of its
restart).  Thus, the names specify the <em>restart protocol</em>
implemented by the signalling code and invoked by the handling code.
The protocol indicates the number of arguments required by the effector
code as well as the semantics of the arguments.
</p>
<p>Scheme provides a conventional set of names (hence, protocols) for
common use.  By choosing the names of restarts from this set, signalling
code can indicate that it is able to perform a small set of fairly
common actions (<code>abort</code>, <code>continue</code>, <code>muffle-warning</code>,
<code>retry</code>, <code>store-value</code>, <code>use-value</code>).  In turn, simple
condition-handling code can look for the kind of action it wishes to
perform and simply invoke it by name.  All of Scheme&rsquo;s conventional
names are symbols, although in general restart names are not restricted
to any particular data type.  In addition, the object <code>#f</code> is
reserved to indicate the &ldquo;not for automated use&rdquo; protocol: these
restarts should be activated only under human control.
</p>
<span id="index-with_002dsimple_002drestart-1"></span>
<p>Restarts themselves are first-class objects.  They encapsulate their
name, a procedure (known as the <var>effector</var>) to be executed if they
are invoked, and a thunk (known as the <var>reporter</var>) that can be
invoked to display a description of the restart (used, for example, by
the interactive debugger).  Invoking a restart is an indication that a
handler has chosen to accept control for a condition; as a consequence,
the <var>effector</var> of the restart should not return, since this would
indicate that the handler declined to handle the condition.  Thus, the
<var>effector</var> should call a continuation captured before the
condition-signalling process began.  The most common pattern of usage by
signalling code is encapsulated in <code>with-simple-restart</code>.
</p>
<p>Within this chapter, a parameter named <var>restarts</var> will accept any of
the following values:
</p>
<ul>
<li> A list of restart objects.

</li><li> A condition.  The procedure <code>condition/restarts</code> is called on the
condition, and the resulting list of restarts is used in place of the
condition.

</li><li> The symbol <code>bound-restarts</code>.  The procedure <code>bound-restarts</code>
is called (with no arguments), and the resulting list of restarts is
used in place of the symbol.

</li><li> If the <var>restarts</var> parameter is optional and is not supplied, it is
equivalent to having specified the symbol <code>bound-restarts</code>.
</li></ul>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Establishing-Restart-Code.html" accesskey="1">Establishing Restart Code</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Invoking-Standard-Restart-Code.html" accesskey="2">Invoking Standard Restart Code</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Finding-and-Invoking-General-Restart-Code.html" accesskey="3">Finding and Invoking General Restart Code</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="The-Named-Restart-Abstraction.html" accesskey="4">The Named Restart Abstraction</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Condition-Instances.html" accesskey="n" rel="next">Condition Instances</a>, Previous: <a href="Condition-Handling.html" accesskey="p" rel="prev">Condition Handling</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
