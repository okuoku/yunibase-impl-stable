<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Parser Language (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Parser Language (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Parser Language (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="_002aMatcher.html" rel="next" title="*Matcher">
<link href="Parser-Buffers.html" rel="prev" title="Parser Buffers">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Parser-Language"></span><div class="header">
<p>
Next: <a href="XML-Support.html" accesskey="n" rel="next">XML Support</a>, Previous: <a href="Parser-Buffers.html" accesskey="p" rel="prev">Parser Buffers</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Parser-Language-1"></span><h3 class="section">14.14 Parser Language</h3>

<span id="index-Parser-language"></span>
<p>Although it is possible to write parsers using the parser-buffer
abstraction (see <a href="Parser-Buffers.html">Parser Buffers</a>), it is tedious.  The problem is
that the abstraction isn&rsquo;t closely matched to the way that people
think about syntactic structures.  In this section, we introduce a
higher-level mechanism that greatly simplifies the implementation of a
parser.
</p>
<p>The <em>parser language</em> described here allows the programmer to
write <acronym>BNF</acronym>-like specifications that are translated into
efficient Scheme code at compile time.  The language is declarative,
but it can be freely mixed with Scheme code; this allows the parsing
of grammars that aren&rsquo;t conveniently described in the language.
</p>
<span id="index-Backtracking_002c-in-parser-language"></span>
<p>The language also provides backtracking.  For example, this expression
matches any sequence of alphanumeric characters followed by a single
alphabetic character:
</p>
<div class="example">
<pre class="example">(*matcher
 (seq (* (char-set char-set:alphanumeric))
      (char-set char-set:alphabetic)))
</pre></div>

<p>The way that this works is that the matcher matches alphanumeric
characters in the input stream until it finds a non-alphanumeric
character.  It then tries to match an alphabetic character, which of
course fails.  At this point, if it matched at least one alphanumeric
character, it <em>backtracks</em>: the last matched alphanumeric is
&ldquo;unmatched&rdquo;, and it again attempts to match an alphabetic
character.  The backtracking can be arbitrarily deep; the matcher will
continue to back up until it finds a way to match the remainder of the
expression.
</p>
<p>So far, this sounds a lot like regular-expression matching
(see <a href="Regular-Expressions.html">Regular Expressions</a>).  However, there are some important
differences.
</p>
<ul>
<li> The parser language uses a Scheme-like syntax that is easier to read
and write than regular-expression notation.

</li><li> The language provides macros so that common syntactic constructs can
be abstracted.

</li><li> The language mixes easily with Scheme code, allowing the full power of
Scheme to be applied to program around limitations in the parser
language.

</li><li> The language provides expressive facilities for converting syntax into
parsed structure.  It also makes it easy to convert parsed strings
into meaningful objects (e.g. numbers).

</li><li> The language is compiled into machine language; regular expressions
are usually interpreted.
</li></ul>

<p>Here is an example that shows off several of the features of the
parser language.  The example is a parser for <acronym>XML</acronym> start
tags:
</p>
<span id="with_002dpointer-example"></span><div class="example">
<pre class="example">(*parser
 (with-pointer p
   (seq &quot;&lt;&quot;
        parse-name
        parse-attribute-list
        (alt (match &quot;&gt;&quot;)
             (match &quot;/&gt;&quot;)
             (sexp
              (lambda (b)
                (error
                 (string-append
                  &quot;Unterminated start tag at &quot;
                  (parser-buffer-position-string p)))))))))
</pre></div>

<p>This shows that the basic description of a start tag is very similar
to its <acronym>BNF</acronym>.  Non-terminal symbols <code>parse-name</code> and
<code>parse-attribute-list</code> do most of the work, and the noise strings
<code>&quot;&lt;&quot;</code> and <code>&quot;&gt;&quot;</code> are the syntactic markers delimiting the
form.  There are two alternate endings for start tags, and if the
parser doesn&rsquo;t find either of the endings, the Scheme code (wrapped in
<code>sexp</code>) is run to signal an error.  The error procedure
<code>perror</code> takes a pointer <code>p</code>, which it uses to indicate the
position in the input stream at which the error occurred.  In this
case, that is the beginning of the start tag, i.e. the position of
the leading <code>&quot;&lt;&quot;</code> marker.
</p>
<p>This example still looks pretty complicated, mostly due to the
error-signalling code.  In practice, this is abstracted into a macro,
after which the expression is quite succinct:
</p>
<div class="example">
<pre class="example">(*parser
 (bracket &quot;start tag&quot;
     (seq (noise (string &quot;&lt;&quot;)) parse-name)
     (match (alt (string &quot;&gt;&quot;) (string &quot;/&gt;&quot;)))
   parse-attribute-list))
</pre></div>

<p>The <code>bracket</code> macro captures the pattern of a bracketed item, and
hides much of the detail.
</p>
<p>The parser language actually consists of two languages: one for
defining matchers, and one for defining parsers.  The languages are
intentionally very similar, and are meant to be used together.  Each
sub-language is described below in its own section.
</p>
<span id="index-run_002dtime_002dloadable-option-1"></span>
<span id="index-option_002c-run_002dtime_002dloadable-1"></span>
<p>The parser language is a run-time-loadable option; to use it, execute
</p>
<div class="example">
<pre class="example">(load-option '*parser)
</pre></div>
<span id="index-load_002doption-1"></span>

<p>once before compiling any code that uses the language.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="_002aMatcher.html" accesskey="1">*Matcher</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="_002aParser.html" accesskey="2">*Parser</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Parser_002dlanguage-Macros.html" accesskey="3">Parser-language Macros</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="XML-Support.html" accesskey="n" rel="next">XML Support</a>, Previous: <a href="Parser-Buffers.html" accesskey="p" rel="prev">Parser Buffers</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
