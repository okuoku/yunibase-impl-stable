<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Blocking Mode (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Blocking Mode (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Blocking Mode (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Terminal-Mode.html" rel="next" title="Terminal Mode">
<link href="Output-Procedures.html" rel="prev" title="Output Procedures">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Blocking-Mode"></span><div class="header">
<p>
Next: <a href="Terminal-Mode.html" accesskey="n" rel="next">Terminal Mode</a>, Previous: <a href="Output-Procedures.html" accesskey="p" rel="prev">Output Procedures</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Blocking-Mode-1"></span><h3 class="section">14.7 Blocking Mode</h3>

<span id="index-blocking-mode_002c-of-port"></span>
<p>An interactive port is always in one of two modes: <em>blocking</em> or
<em>non-blocking</em>.  This mode is independent of the terminal mode:
each can be changed independently of the other.  Furthermore, if it is
an interactive <acronym>I/O</acronym> port, there are separate blocking modes
for input and for output.
</p>
<p>If an input port is in blocking mode, attempting to read from it when no
input is available will cause Scheme to &ldquo;block&rdquo;, i.e. suspend
itself, until input is available.  If an input port is in non-blocking
mode, attempting to read from it when no input is available will cause
the reading procedure to return immediately, indicating the lack of
input in some way (exactly how this situation is indicated is separately
specified for each procedure or operation).
</p>
<p>An output port in blocking mode will block if the output device is not
ready to accept output.  In non-blocking mode it will return immediately
after performing as much output as the device will allow (again, each
procedure or operation reports this situation in its own way).
</p>
<p>Interactive ports are initially in blocking mode; this can be changed at
any time with the procedures defined in this section.
</p>
<p>These procedures represent blocking mode by the symbol <code>blocking</code>,
and non-blocking mode by the symbol <code>nonblocking</code>.  An argument
called <var>mode</var> must be one of these symbols.  A <var>port</var> argument
to any of these procedures may be any port, even if that port does not
support blocking mode; in that case, the port is not modified in any
way.
</p>
<dl>
<dt id="index-input_002dport_002dblocking_002dmode">procedure: <strong>input-port-blocking-mode</strong> <em>input-port</em></dt>
<dt id="index-output_002dport_002dblocking_002dmode">procedure: <strong>output-port-blocking-mode</strong> <em>output-port</em></dt>
<dd><p>Returns the blocking mode of <var>input-port</var> or <var>output-port</var>.
Returns <code>#f</code> if the given port doesn&rsquo;t support blocking mode.
</p></dd></dl>

<dl>
<dt id="index-set_002dinput_002dport_002dblocking_002dmode_0021">procedure: <strong>set-input-port-blocking-mode!</strong> <em>input-port mode</em></dt>
<dt id="index-set_002doutput_002dport_002dblocking_002dmode">procedure: <strong>set-output-port-blocking-mode</strong> <em>output-port mode</em></dt>
<dd><p>Changes the blocking mode of <var>input-port</var> or <var>output-port</var> to
be <var>mode</var> and returns an unspecified value.
</p></dd></dl>

<dl>
<dt id="index-with_002dinput_002dport_002dblocking_002dmode">procedure: <strong>with-input-port-blocking-mode</strong> <em>input-port mode thunk</em></dt>
<dt id="index-with_002doutput_002dport_002dblocking_002dmode">procedure: <strong>with-output-port-blocking-mode</strong> <em>output-port mode thunk</em></dt>
<dd><p><var>Thunk</var> must be a procedure of no arguments.
</p>
<p>Binds the blocking mode of <var>input-port</var> or <var>output-port</var> to be
<var>mode</var>, and calls <var>thunk</var>.  When <var>thunk</var> returns, the
original blocking mode is restored and the values yielded by
<var>thunk</var> are returned.
</p></dd></dl>

<dl>
<dt id="index-port_002finput_002dblocking_002dmode">obsolete procedure: <strong>port/input-blocking-mode</strong> <em>input-port</em></dt>
<dt id="index-port_002fset_002dinput_002dblocking_002dmode">obsolete procedure: <strong>port/set-input-blocking-mode</strong> <em>input-port mode</em></dt>
<dt id="index-port_002fwith_002dinput_002dblocking_002dmode">obsolete procedure: <strong>port/with-input-blocking-mode</strong> <em>input-port mode thunk</em></dt>
<dt id="index-port_002foutput_002dblocking_002dmode">obsolete procedure: <strong>port/output-blocking-mode</strong> <em>output-port</em></dt>
<dt id="index-port_002fset_002doutput_002dblocking_002dmode">obsolete procedure: <strong>port/set-output-blocking-mode</strong> <em>output-port mode</em></dt>
<dt id="index-port_002fwith_002doutput_002dblocking_002dmode">obsolete procedure: <strong>port/with-output-blocking-mode</strong> <em>output-port mode thunk</em></dt>
<dd><p>These procedures are <strong>deprecated</strong>; instead use the
corresponding procedures above.
</p></dd></dl>
  
<hr>
<div class="header">
<p>
Next: <a href="Terminal-Mode.html" accesskey="n" rel="next">Terminal Mode</a>, Previous: <a href="Output-Procedures.html" accesskey="p" rel="prev">Output Procedures</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
