<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Streams (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Streams (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Streams (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Weak-References.html" rel="next" title="Weak References">
<link href="Promises.html" rel="prev" title="Promises">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Streams"></span><div class="header">
<p>
Next: <a href="Weak-References.html" accesskey="n" rel="next">Weak References</a>, Previous: <a href="Promises.html" accesskey="p" rel="prev">Promises</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Streams-1"></span><h3 class="section">10.6 Streams</h3>

<span id="index-stream-_0028defn_0029"></span>
<p>In addition to promises, MIT/GNU Scheme supports a higher-level abstraction
called <em>streams</em>.  Streams are similar to lists, except that the
tail of a stream is not computed until it is referred to.
This allows streams to be used to represent infinitely long lists.
</p>
<dl>
<dt id="index-stream">procedure: <strong>stream</strong> <em>object &hellip;</em></dt>
<dd><span id="index-construction_002c-of-stream"></span>
<p>Returns a newly allocated stream whose elements are the arguments.  Note
that the expression <code>(stream)</code> returns the empty stream, or
end-of-stream marker.
</p></dd></dl>

<dl>
<dt id="index-list_002d_003estream">procedure: <strong>list-&gt;stream</strong> <em>list</em></dt>
<dd><span id="index-list_002c-converting-to-stream"></span>
<p>Returns a newly allocated stream whose elements are the elements of
<var>list</var>.  Equivalent to <code>(apply stream <var>list</var>)</code>.
</p></dd></dl>

<dl>
<dt id="index-stream_002d_003elist">procedure: <strong>stream-&gt;list</strong> <em>stream</em></dt>
<dd><span id="index-stream_002c-converting-to-list"></span>
<p>Returns a newly allocated list whose elements are the elements of
<var>stream</var>.  If <var>stream</var> has infinite length this procedure will
not terminate.  This could have been defined by
</p>
<div class="example">
<pre class="example">(define (stream-&gt;list stream)
  (if (stream-null? stream)
      '()
      (cons (stream-car stream)
            (stream-&gt;list (stream-cdr stream)))))
</pre></div>
</dd></dl>

<dl>
<dt id="index-cons_002dstream">special form: <strong>cons-stream</strong> <em>object expression</em></dt>
<dd><p>Returns a newly allocated stream pair.  Equivalent to <code>(cons
<var>object</var> (delay <var>expression</var>))</code>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dpair_003f">procedure: <strong>stream-pair?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-stream-pair"></span>
<p>Returns <code>#t</code> if <var>object</var> is a pair whose cdr contains a
promise.  Otherwise returns <code>#f</code>.  This could have been defined by
</p>
<div class="example">
<pre class="example">(define (stream-pair? object)
  (and (pair? object)
       (promise? (cdr object))))
</pre></div>
</dd></dl>

<dl>
<dt id="index-stream_002dcar">procedure: <strong>stream-car</strong> <em>stream</em></dt>
<dt id="index-stream_002dfirst">procedure: <strong>stream-first</strong> <em>stream</em></dt>
<dd><span id="index-car-2"></span>
<p>Returns the first element in <var>stream</var>.  <code>stream-car</code> is
equivalent to <code>car</code>.  <code>stream-first</code> is a synonym for
<code>stream-car</code>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dcdr">procedure: <strong>stream-cdr</strong> <em>stream</em></dt>
<dt id="index-stream_002drest">procedure: <strong>stream-rest</strong> <em>stream</em></dt>
<dd><span id="index-force-1"></span>
<span id="index-cdr-2"></span>
<p>Returns the first tail of <var>stream</var>.  Equivalent to <code>(force (cdr
<var>stream</var>))</code>.  <code>stream-rest</code> is a synonym for <code>stream-cdr</code>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dnull_003f">procedure: <strong>stream-null?</strong> <em>stream</em></dt>
<dd><span id="index-empty-stream_002c-predicate-for"></span>
<span id="index-null_003f-2"></span>
<p>Returns <code>#t</code> if <var>stream</var> is the end-of-stream marker; otherwise
returns <code>#f</code>.  This is equivalent to <code>null?</code>, but should be
used whenever testing for the end of a stream.
</p></dd></dl>

<dl>
<dt id="index-stream_002dlength">procedure: <strong>stream-length</strong> <em>stream</em></dt>
<dd><span id="index-length_002c-of-stream"></span>
<p>Returns the number of elements in <var>stream</var>.  If <var>stream</var> has an
infinite number of elements this procedure will not terminate.  Note
that this procedure forces all of the promises that comprise
<var>stream</var>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dref">procedure: <strong>stream-ref</strong> <em>stream k</em></dt>
<dd><span id="index-selecting_002c-of-stream-component"></span>
<span id="index-component-selection_002c-of-stream"></span>
<p>Returns the element of <var>stream</var> that is indexed by <var>k</var>; that is,
the <var>k</var>th element.  <var>K</var> must be an exact non-negative integer
strictly less than the length of <var>stream</var>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dhead">procedure: <strong>stream-head</strong> <em>stream k</em></dt>
<dd><p>Returns the first <var>k</var> elements of <var>stream</var> as a list.  <var>K</var>
must be an exact non-negative integer strictly less than the length of
<var>stream</var>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dtail">procedure: <strong>stream-tail</strong> <em>stream k</em></dt>
<dd><p>Returns the tail of <var>stream</var> that is indexed by <var>k</var>; that is,
the <var>k</var>th tail.  This is equivalent to performing <code>stream-cdr</code>
<var>k</var> times.  <var>K</var> must be an exact non-negative integer strictly
less than the length of <var>stream</var>.
</p></dd></dl>

<dl>
<dt id="index-stream_002dmap">procedure: <strong>stream-map</strong> <em>procedure stream stream &hellip;</em></dt>
<dd><span id="index-mapping_002c-of-stream"></span>
<p>Returns a newly allocated stream, each element being the result of
invoking <var>procedure</var> with the corresponding elements of the
<var>stream</var>s as its arguments.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Weak-References.html" accesskey="n" rel="next">Weak References</a>, Previous: <a href="Promises.html" accesskey="p" rel="prev">Promises</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
