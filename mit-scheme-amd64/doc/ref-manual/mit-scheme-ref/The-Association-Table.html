<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>The Association Table (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="The Association Table (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="The Association Table (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="Hash-Tables.html" rel="next" title="Hash Tables">
<link href="1D-Tables.html" rel="prev" title="1D Tables">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="The-Association-Table"></span><div class="header">
<p>
Next: <a href="Hash-Tables.html" accesskey="n" rel="next">Hash Tables</a>, Previous: <a href="1D-Tables.html" accesskey="p" rel="prev">1D Tables</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="The-Association-Table-1"></span><h3 class="section">11.3 The Association Table</h3>

<span id="index-association-table-_0028defn_0029"></span>
<span id="index-table_002c-association-_0028defn_0029"></span>
<span id="index-property-list-2"></span>
<span id="index-eq_003f-7"></span>
<p>MIT/GNU Scheme provides a generalization of the property-list mechanism
found in most other implementations of Lisp: a global two-dimensional
<em>association table</em>.  This table is indexed by two keys, called
<var>x-key</var> and <var>y-key</var> in the following procedure descriptions.
These keys and the datum associated with them can be arbitrary objects.
<code>eq?</code> is used to discriminate keys.
</p>
<p>Think of the association table as a matrix: a single datum can be
accessed using both keys, a column using <var>x-key</var> only, and a row
using <var>y-key</var> only.
</p>
<dl>
<dt id="index-2d_002dput_0021">procedure: <strong>2d-put!</strong> <em>x-key y-key datum</em></dt>
<dd><p>Makes an entry in the association table that associates <var>datum</var> with
<var>x-key</var> and <var>y-key</var>.  Returns an unspecified result.
</p></dd></dl>

<dl>
<dt id="index-2d_002dremove_0021">procedure: <strong>2d-remove!</strong> <em>x-key y-key</em></dt>
<dd><p>If the association table has an entry for <var>x-key</var> and <var>y-key</var>,
it is removed.  Returns an unspecified result.
</p></dd></dl>

<dl>
<dt id="index-2d_002dget">procedure: <strong>2d-get</strong> <em>x-key y-key</em></dt>
<dd><p>Returns the <var>datum</var> associated with <var>x-key</var> and <var>y-key</var>.
Returns <code>#f</code> if no such association exists.
</p></dd></dl>

<dl>
<dt id="index-2d_002dget_002dalist_002dx">procedure: <strong>2d-get-alist-x</strong> <em>x-key</em></dt>
<dd><p>Returns an association list of all entries in the association table that
are associated with <var>x-key</var>.  The result is a list of
<code>(<var>y-key</var> . <var>datum</var>)</code> pairs.  Returns the empty list if no
entries for <var>x-key</var> exist.
</p>
<div class="example">
<pre class="example">(2d-put! 'foo 'bar 5)
(2d-put! 'foo 'baz 6)
(2d-get-alist-x 'foo)           &rArr;  ((baz . 6) (bar . 5))
</pre></div>
</dd></dl>

<dl>
<dt id="index-2d_002dget_002dalist_002dy">procedure: <strong>2d-get-alist-y</strong> <em>y-key</em></dt>
<dd><p>Returns an association list of all entries in the association table that
are associated with <var>y-key</var>.  The result is a list of
<code>(<var>x-key</var> . <var>datum</var>)</code> pairs.  Returns the empty list if no
entries for <var>y-key</var> exist.
</p>
<div class="example">
<pre class="example">(2d-put! 'bar 'foo 5)
(2d-put! 'baz 'foo 6)
(2d-get-alist-y 'foo)           &rArr;  ((baz . 6) (bar . 5))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Hash-Tables.html" accesskey="n" rel="next">Hash Tables</a>, Previous: <a href="1D-Tables.html" accesskey="p" rel="prev">1D Tables</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
