<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Characters (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Characters (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Characters (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Character-implementation.html" rel="next" title="Character implementation">
<link href="Random-Number-Generation.html" rel="prev" title="Random Number Generation">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Characters"></span><div class="header">
<p>
Next: <a href="Strings.html" accesskey="n" rel="next">Strings</a>, Previous: <a href="Numbers.html" accesskey="p" rel="prev">Numbers</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Characters-1"></span><h2 class="chapter">5 Characters</h2>

<span id="index-character-_0028defn_0029"></span>
<p>Characters are objects that represent printed characters such as
letters and digits.  MIT/GNU Scheme supports the full Unicode
character repertoire.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Character-implementation.html" accesskey="1">Character implementation</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Unicode.html" accesskey="2">Unicode</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Character-Sets.html" accesskey="3">Character Sets</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<span id="index-_0023_005c-as-external-representation"></span>
<span id="index-_0023_005c"></span>
<p>Characters are written using the notation <code>#\<var>character</var></code> or
<code>#\<var>character-name</var></code> or <code>#\x<var>hex-scalar-value</var></code>.
</p>
<p>The following standard character names are supported:
</p>
<div class="example">
<pre class="example">#\alarm                 <span class="roman">; U+0007</span>
#\backspace             <span class="roman">; U+0008</span>
#\delete                <span class="roman">; U+007F</span>
#\escape                <span class="roman">; U+001B</span>
#\newline               <span class="roman">; the linefeed character, U+000A</span>
#\null                  <span class="roman">; the null character, U+0000</span>
#\return                <span class="roman">; the return character, U+000D</span>
#\space                 <span class="roman">; the preferred way to write a space, U+0020</span>
#\tab                   <span class="roman">; the tab character, U+0009</span>
</pre></div>
<span id="index-_0023_005calarm"></span>
<span id="index-_0023_005cbackspace"></span>
<span id="index-_0023_005cdelete"></span>
<span id="index-_0023_005cescape"></span>
<span id="index-_0023_005cnewline"></span>
<span id="index-_0023_005cnull"></span>
<span id="index-_0023_005creturn"></span>
<span id="index-_0023_005cspace"></span>
<span id="index-_0023_005ctab"></span>

<p>Here are some additional examples:
</p>
<div class="example">
<pre class="example">#\a                     <span class="roman">; lowercase letter</span>
#\A                     <span class="roman">; uppercase letter</span>
#\(                     <span class="roman">; left parenthesis</span>
#\                      <span class="roman">; the space character</span>
</pre></div>

<p>Case is significant in <code>#\<var>character</var></code>, and in
<code>#\<var>character-name</var></code>, but not in
<code>#\x<var>hex-scalar-value</var></code>.  If <var>character</var> in
<code>#\<var>character</var></code> is alphabetic, then any character immediately
following <var>character</var> cannot be one that can appear in an
identifier.  This rule resolves the ambiguous case where, for example,
the sequence of characters &lsquo;<samp>#\space</samp>&rsquo; could be taken to be either
a representation of the space character or a representation of the
character &lsquo;<samp>#\s</samp>&rsquo; followed by a representation of the symbol
&lsquo;<samp>pace</samp>&rsquo;.
</p>
<p>Characters written in the <code>#\</code> notation are self-evaluating.
That is, they do not have to be quoted in programs.
</p>
<p>Some of the procedures that operate on characters ignore the
difference between upper case and lower case.  The procedures that
ignore case have &lsquo;<samp>-ci</samp>&rsquo; (for &ldquo;case insensitive&rdquo;) embedded in
their names.
</p>
<span id="index-bucky-bit_002c-prefix-_0028defn_0029"></span>
<span id="index-control_002c-bucky-bit-prefix-_0028defn_0029"></span>
<span id="index-meta_002c-bucky-bit-prefix-_0028defn_0029"></span>
<span id="index-super_002c-bucky-bit-prefix-_0028defn_0029"></span>
<span id="index-hyper_002c-bucky-bit-prefix-_0028defn_0029"></span>
<p>MIT/GNU Scheme allows a character name to include one or more
<em>bucky bit</em> prefixes to indicate that the character includes one
or more of the keyboard shift keys Control, Meta, Super, or Hyper
(note that the Control bucky bit prefix is not the same as the
<acronym>ASCII</acronym> control key).  The bucky bit prefixes and their
meanings are as follows (case is not significant):
</p>
<div class="example">
<pre class="example">Key             Bucky bit prefix        Bucky bit
---             ----------------        ---------

Meta            M- or Meta-                 1
Control         C- or Control-              2
Super           S- or Super-                4
Hyper           H- or Hyper-                8
</pre></div>

<p>For example,
</p>
<div class="example">
<pre class="example">#\c-a                   <span class="roman">; Control-a</span>
#\meta-b                <span class="roman">; Meta-b</span>
#\c-s-m-h-A             <span class="roman">; Control-Meta-Super-Hyper-A</span>
</pre></div>

<dl>
<dt id="index-char_002d_003ename">procedure: <strong>char-&gt;name</strong> <em>char</em></dt>
<dd><p>Returns a string corresponding to the printed representation of
<var>char</var>.  This is the <var>character</var>, <var>character-name</var>, or
<code>x<var>hex-scalar-value</var></code> component of the external
representation, combined with the appropriate bucky bit prefixes.
</p>
<div class="example">
<pre class="example">(char-&gt;name #\a)                        &rArr;  &quot;a&quot;
(char-&gt;name #\space)                    &rArr;  &quot;space&quot;
(char-&gt;name #\c-a)                      &rArr;  &quot;C-a&quot;
(char-&gt;name #\control-a)                &rArr;  &quot;C-a&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-name_002d_003echar">procedure: <strong>name-&gt;char</strong> <em>string</em></dt>
<dd><p>Converts a string that names a character into the character specified.
If <var>string</var> does not name any character, <code>name-&gt;char</code> signals
an error.
</p>
<div class="example">
<pre class="example">(name-&gt;char &quot;a&quot;)                        &rArr;  #\a
(name-&gt;char &quot;space&quot;)                    &rArr;  #\space
(name-&gt;char &quot;SPACE&quot;)                    &rArr;  #\space
(name-&gt;char &quot;c-a&quot;)                      &rArr;  #\C-a
(name-&gt;char &quot;control-a&quot;)                &rArr;  #\C-a
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_003f">standard procedure: <strong>char?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-character"></span>
<p>Returns <code>#t</code> if <var>object</var> is a character, otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-char_003d_003f-2">standard procedure: <strong>char=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_003c_003f">standard procedure: <strong>char&lt;?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_003e_003f">standard procedure: <strong>char&gt;?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_003c_003d_003f">standard procedure: <strong>char&lt;=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_003e_003d_003f">standard procedure: <strong>char&gt;=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dd><span id="index-equivalence-predicate_002c-for-characters"></span>
<span id="index-ordering_002c-of-characters"></span>
<span id="index-comparison_002c-of-characters"></span>
<p>These procedures return <code>#t</code> if the results of passing their
arguments to <code>char-&gt;integer</code> are respectively equal,
monotonically increasing, monotonically decreasing, monotonically
non-decreasing, or monotonically non-increasing.
</p>
<p>These predicates are transitive.
</p></dd></dl>

<dl>
<dt id="index-char_002dci_003d_003f">char library procedure: <strong>char-ci=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_002dci_003c_003f">char library procedure: <strong>char-ci&lt;?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_002dci_003e_003f">char library procedure: <strong>char-ci&gt;?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_002dci_003c_003d_003f">char library procedure: <strong>char-ci&lt;=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dt id="index-char_002dci_003e_003d_003f">char library procedure: <strong>char-ci&gt;=?</strong> <em>char1 char2 char3 &hellip;</em></dt>
<dd><p>These procedures are similar to <code>char=?</code> et cetera, but they
treat upper case and lower case letters as the same.  For example,
<code>(char-ci=? #\A #\a)</code> returns <code>#t</code>.
</p>
<p>Specifically, these procedures behave as if <code>char-foldcase</code> were
applied to their arguments before they were compared.
</p></dd></dl>

<dl>
<dt id="index-char_002dalphabetic_003f">char library procedure: <strong>char-alphabetic?</strong> <em>char</em></dt>
<dt id="index-char_002dnumeric_003f">char library procedure: <strong>char-numeric?</strong> <em>char</em></dt>
<dt id="index-char_002dwhitespace_003f">char library procedure: <strong>char-whitespace?</strong> <em>char</em></dt>
<dt id="index-char_002dupper_002dcase_003f">char library procedure: <strong>char-upper-case?</strong> <em>char</em></dt>
<dt id="index-char_002dlower_002dcase_003f">char library procedure: <strong>char-lower-case?</strong> <em>char</em></dt>
<dd><p>These procedures return <code>#t</code> if their arguments are alphabetic,
numeric, whitespace, upper case, or lower case characters
respectively, otherwise they return <code>#f</code>.
</p>
<p>Specifically, they return <code>#t</code> when applied to characters with
the Unicode properties Alphabetic, Numeric_Decimal, White_Space,
Uppercase, or Lowercase respectively, and <code>#f</code> when applied to
any other Unicode characters.  Note that many Unicode characters are
alphabetic but neither upper nor lower case.
</p></dd></dl>

<dl>
<dt id="index-char_002dalphanumeric_003f">procedure: <strong>char-alphanumeric?</strong> <em>char</em></dt>
<dd><p>Returns <code>#t</code> if <var>char</var> is either alphabetic or numeric,
otherwise it returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-digit_002dvalue">char library procedure: <strong>digit-value</strong> <em>char</em></dt>
<dd><p>This procedure returns the numeric value (0 to 9) of its argument
if it is a numeric digit (that is, if <code>char-numeric?</code> returns <code>#t</code>),
or <code>#f</code> on any other character.
</p>
<div class="example">
<pre class="example">(digit-value #\3) &rArr; 3
(digit-value #\x0664) &rArr; 4
(digit-value #\x0AE6) &rArr; 0
(digit-value #\x0EA6) &rArr; #f
</pre></div>
</dd></dl>

<dl>
<dt id="index-char_002d_003einteger">standard procedure: <strong>char-&gt;integer</strong> <em>char</em></dt>
<dt id="index-integer_002d_003echar">standard procedure: <strong>integer-&gt;char</strong> <em>n</em></dt>
<dd><p>Given a Unicode character, <code>char-&gt;integer</code> returns an exact
integer between <code>0</code> and <code>#xD7FF</code> or between <code>#xE000</code>
and <code>#x10FFFF</code> which is equal to the Unicode scalar value of that
character.  Given a non-Unicode character, it returns an exact integer
greater than <code>#x10FFFF</code>.
</p>
<p>Given an exact integer that is the value returned by a character when
<code>char-&gt;integer</code> is applied to it, <code>integer-&gt;char</code> returns
that character.
</p>
<p>Implementation note: MIT/GNU Scheme allows any Unicode code point, not
just scalar values.
</p>
<p>Implementation note: If the argument to <code>char-&gt;integer</code> or
<code>integer-&gt;char</code> is a constant, the MIT/GNU Scheme compiler will
constant-fold the call, replacing it with the corresponding result.
This is a very useful way to denote unusual character constants or
<acronym>ASCII</acronym> codes.
</p></dd></dl>

<dl>
<dt id="index-char_002dupcase">char library procedure: <strong>char-upcase</strong> <em>char</em></dt>
<dt id="index-char_002ddowncase">char library procedure: <strong>char-downcase</strong> <em>char</em></dt>
<dt id="index-char_002dfoldcase">char library procedure: <strong>char-foldcase</strong> <em>char</em></dt>
<dd><span id="index-uppercase_002c-character-conversion"></span>
<span id="index-lowercase_002c-character-conversion"></span>
<span id="index-case-conversion_002c-of-character"></span>
<span id="index-case-folding_002c-of-character"></span>
<p>The <code>char-upcase</code> procedure, given an argument that is the
lowercase part of a Unicode casing pair, returns the uppercase member
of the pair.  Note that language-sensitive casing pairs are not used.
If the argument is not the lowercase member of such a pair, it is
returned.
</p>
<p>The <code>char-downcase</code> procedure, given an argument that is the
uppercase part of a Unicode casing pair, returns the lowercase member
of the pair.  Note that language-sensitive casing pairs are not used.
If the argument is not the uppercase member of such a pair, it is
returned.
</p>
<p>The <code>char-foldcase</code> procedure applies the Unicode simple
case-folding algorithm to its argument and returns the result.  Note
that language-sensitive folding is not used.  See
<a href="http://www.unicode.org/reports/tr44/">UAX #44</a> (part of the
Unicode Standard) for details.
</p>
<p>Note that many Unicode lowercase characters do not have uppercase
equivalents.
</p></dd></dl>

<dl>
<dt id="index-char_002d_003edigit">procedure: <strong>char-&gt;digit</strong> <em>char [radix]</em></dt>
<dd><p>If <var>char</var> is a character representing a digit in the given
<var>radix</var>, returns the corresponding integer value.  If <var>radix</var>
is specified (which must be an exact integer between 2 and 36
inclusive), the conversion is done in that base, otherwise it is done
in base 10.  If <var>char</var> doesn&rsquo;t represent a digit in base
<var>radix</var>, <code>char-&gt;digit</code> returns <code>#f</code>.
</p>
<p>Note that this procedure is insensitive to the alphabetic case of
<var>char</var>.
</p>
<div class="example">
<pre class="example">(char-&gt;digit #\8)                       &rArr;  8
(char-&gt;digit #\e 16)                    &rArr;  14
(char-&gt;digit #\e)                       &rArr;  #f
</pre></div>
</dd></dl>

<dl>
<dt id="index-digit_002d_003echar">procedure: <strong>digit-&gt;char</strong> <em>digit [radix]</em></dt>
<dd><p>Returns a character that represents <var>digit</var> in the radix given by
<var>radix</var>.  The <var>radix</var> argument, if given, must be an exact
integer between 2 and 36 (inclusive); it defaults to 10.  The
<var>digit</var> argument must be an exact non-negative integer strictly
less than <var>radix</var>.
</p>
<div class="example">
<pre class="example">(digit-&gt;char 8)                         &rArr;  #\8
(digit-&gt;char 14 16)                     &rArr;  #\E
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Strings.html" accesskey="n" rel="next">Strings</a>, Previous: <a href="Numbers.html" accesskey="p" rel="prev">Numbers</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
