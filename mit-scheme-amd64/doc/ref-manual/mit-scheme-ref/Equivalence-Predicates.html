<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Equivalence Predicates (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Equivalence Predicates (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Equivalence Predicates (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Numbers.html" rel="next" title="Numbers">
<link href="define_002drecord_002dtype-_0028SRFI-9_0029.html" rel="prev" title="define-record-type (SRFI 9)">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Equivalence-Predicates"></span><div class="header">
<p>
Next: <a href="Numbers.html" accesskey="n" rel="next">Numbers</a>, Previous: <a href="Special-Forms.html" accesskey="p" rel="prev">Special Forms</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Equivalence-Predicates-1"></span><h2 class="chapter">3 Equivalence Predicates</h2>

<span id="index-predicate-_0028defn_0029-1"></span>
<span id="index-predicate_002c-equivalence-_0028defn_0029"></span>
<span id="index-equivalence-predicate-_0028defn_0029"></span>
<span id="index-comparison_002c-for-equivalence"></span>
<span id="index-eq_003f"></span>
<span id="index-eqv_003f-2"></span>
<span id="index-equal_003f-1"></span>
<p>A <em>predicate</em> is a procedure that always returns a boolean value
(<code>#t</code> or <code>#f</code>).  An <em>equivalence predicate</em> is the
computational analogue of a mathematical equivalence relation (it is
symmetric, reflexive, and transitive).  Of the equivalence predicates
described in this section, <code>eq?</code> is the finest or most
discriminating, and <code>equal?</code> is the coarsest.  <code>eqv?</code> is
slightly less discriminating than <code>eq?</code>.
</p>
<dl>
<dt id="index-eqv_003f-3">procedure: <strong>eqv?</strong> <em>obj1 obj2</em></dt>
<dd><p>The <code>eqv?</code> procedure defines a useful equivalence relation on
objects.  Briefly, it returns <code>#t</code> if <var>obj1</var> and <var>obj2</var>
should normally be regarded as the same object.
</p>
<p>The <code>eqv?</code> procedure returns <code>#t</code> if:
</p>
<ul>
<li> <var>obj1</var> and <var>obj2</var> are both <code>#t</code> or both <code>#f</code>.

</li><li> <var>obj1</var> and <var>obj2</var> are both interned symbols and

<div class="example">
<pre class="example">(string=? (symbol-&gt;string <var>obj1</var>)
          (symbol-&gt;string <var>obj2</var>))
     &rArr; #t
</pre></div>
<span id="index-string_003d_003f"></span>
<span id="index-symbol_002d_003estring-1"></span>

</li><li> <var>obj1</var> and <var>obj2</var> are both numbers, are numerically equal
according to the <code>=</code> procedure, and are either both exact or both
inexact (see <a href="Numbers.html">Numbers</a>).
<span id="index-_003d"></span>

</li><li> <var>obj1</var> and <var>obj2</var> are both characters and are the same character
according to the <code>char=?</code> procedure (see <a href="Characters.html">Characters</a>).
<span id="index-char_003d_003f"></span>

</li><li> both <var>obj1</var> and <var>obj2</var> are the empty list.

</li><li> <var>obj1</var> and <var>obj2</var> are procedures whose location tags are equal.

</li><li> <var>obj1</var> and <var>obj2</var> are pairs, vectors, strings, bit strings,
records, cells, or weak pairs that denote the same locations in the
store.
</li></ul>

<p>The <code>eqv?</code> procedure returns <code>#f</code> if:
</p>
<ul>
<li> <var>obj1</var> and <var>obj2</var> are of different types.

</li><li> one of <var>obj1</var> and <var>obj2</var> is <code>#t</code> but the other is
<code>#f</code>.

</li><li> <var>obj1</var> and <var>obj2</var> are symbols but

<div class="example">
<pre class="example">(string=? (symbol-&gt;string <var>obj1</var>)
          (symbol-&gt;string <var>obj2</var>))
     &rArr; #f
</pre></div>
<span id="index-string_003d_003f-1"></span>
<span id="index-symbol_002d_003estring-2"></span>

</li><li> one of <var>obj1</var> and <var>obj2</var> is an exact number but the other is an
inexact number.

</li><li> <var>obj1</var> and <var>obj2</var> are numbers for which the <code>=</code> procedure
returns <code>#f</code>.
<span id="index-_003d-1"></span>

</li><li> <var>obj1</var> and <var>obj2</var> are characters for which the <code>char=?</code>
procedure returns <code>#f</code>.
<span id="index-char_003d_003f-1"></span>

</li><li> one of <var>obj1</var> and <var>obj2</var> is the empty list but the other is not.

</li><li> <var>obj1</var> and <var>obj2</var> are procedures that would behave differently
(return a different value or have different side effects) for some
arguments.

</li><li> <var>obj1</var> and <var>obj2</var> are pairs, vectors, strings, bit strings,
records, cells, or weak pairs that denote distinct locations.
</li></ul>

<p>Some examples:
</p>
<div class="example">
<pre class="example">(eqv? 'a 'a)                    &rArr;  #t
(eqv? 'a 'b)                    &rArr;  #f
(eqv? 2 2)                      &rArr;  #t
(eqv? '() '())                  &rArr;  #t
(eqv? 100000000 100000000)      &rArr;  #t
(eqv? (cons 1 2) (cons 1 2))    &rArr;  #f
(eqv? (lambda () 1)
      (lambda () 2))            &rArr;  #f
(eqv? #f 'nil)                  &rArr;  #f
(let ((p (lambda (x) x)))
  (eqv? p p))                   &rArr;  #t
</pre></div>

<p>The following examples illustrate cases in which the above rules do not
fully specify the behavior of <code>eqv?</code>.  All that can be said about
such cases is that the value returned by <code>eqv?</code> must be a boolean.
</p>
<div class="example">
<pre class="example">(eqv? &quot;&quot; &quot;&quot;)                    &rArr;  <span class="roman">unspecified</span>
(eqv? '#() '#())                &rArr;  <span class="roman">unspecified</span>
(eqv? (lambda (x) x)
      (lambda (x) x))           &rArr;  <span class="roman">unspecified</span>
(eqv? (lambda (x) x)
      (lambda (y) y))           &rArr;  <span class="roman">unspecified</span>
</pre></div>

<p>The next set of examples shows the use of <code>eqv?</code> with procedures
that have local state.  <code>gen-counter</code> must return a distinct
procedure every time, since each procedure has its own internal counter.
<code>gen-loser</code>, however, returns equivalent procedures each time,
since the local state does not affect the value or side effects of the
procedures.
</p>
<div class="example">
<pre class="example">(define gen-counter
  (lambda ()
    (let ((n 0))
      (lambda () (set! n (+ n 1)) n))))
(let ((g (gen-counter)))
  (eqv? g g))                   &rArr;  #t
(eqv? (gen-counter) (gen-counter))
                                &rArr;  #f
</pre><pre class="example">

</pre><pre class="example">(define gen-loser
  (lambda ()
    (let ((n 0))
      (lambda () (set! n (+ n 1)) 27))))
(let ((g (gen-loser)))
  (eqv? g g))                   &rArr;  #t
(eqv? (gen-loser) (gen-loser))
                                &rArr;  <span class="roman">unspecified</span>
</pre><pre class="example">

</pre><pre class="example">(letrec ((f (lambda () (if (eqv? f g) 'both 'f)))
         (g (lambda () (if (eqv? f g) 'both 'g)))
  (eqv? f g))
                                &rArr;  <span class="roman">unspecified</span>

(letrec ((f (lambda () (if (eqv? f g) 'f 'both)))
         (g (lambda () (if (eqv? f g) 'g 'both)))
  (eqv? f g))
                                &rArr;  #f
</pre></div>

<p>Objects of distinct types must never be regarded as the same object.
</p>
<p>Since it is an error to modify constant objects (those returned by
literal expressions), the implementation may share structure between
constants where appropriate.  Thus the value of <code>eqv?</code> on constants
is sometimes unspecified.
</p>
<div class="example">
<pre class="example">(let ((x '(a)))
  (eqv? x x))                    &rArr;  #t
(eqv? '(a) '(a))                 &rArr;  <span class="roman">unspecified</span>
(eqv? &quot;a&quot; &quot;a&quot;)                   &rArr;  <span class="roman">unspecified</span>
(eqv? '(b) (cdr '(a b)))         &rArr;  <span class="roman">unspecified</span>
</pre></div>

<p>Rationale: The above definition of <code>eqv?</code> allows implementations
latitude in their treatment of procedures and literals: implementations
are free either to detect or to fail to detect that two procedures or
two literals are equivalent to each other, and can decide whether or not
to merge representations of equivalent objects by using the same pointer
or bit pattern to represent both.
</p></dd></dl>

<dl>
<dt id="index-eq_003f-1">procedure: <strong>eq?</strong> <em>obj1 obj2</em></dt>
<dd><p><code>eq?</code> is similar to <code>eqv?</code> except that in some cases it is
capable of discerning distinctions finer than those detectable by
<code>eqv?</code>.
</p>
<p><code>eq?</code> and <code>eqv?</code> are guaranteed to have the same behavior on
symbols, booleans, the empty list, pairs, records, and non-empty strings
and vectors.  <code>eq?</code>&rsquo;s behavior on numbers and characters is
implementation-dependent, but it will always return either true or
false, and will return true only when <code>eqv?</code> would also return
true.  <code>eq?</code> may also behave differently from <code>eqv?</code> on empty
vectors and empty strings.
</p>
<div class="example">
<pre class="example">(eq? 'a 'a)                     &rArr;  #t
(eq? '(a) '(a))                 &rArr;  <span class="roman">unspecified</span>
(eq? (list 'a) (list 'a))       &rArr;  #f
(eq? &quot;a&quot; &quot;a&quot;)                   &rArr;  <span class="roman">unspecified</span>
(eq? &quot;&quot; &quot;&quot;)                     &rArr;  <span class="roman">unspecified</span>
(eq? '() '())                   &rArr;  #t
(eq? 2 2)                       &rArr;  <span class="roman">unspecified</span>
(eq? #\A #\A)                   &rArr;  <span class="roman">unspecified</span>
(eq? car car)                   &rArr;  #t
(let ((n (+ 2 3)))
  (eq? n n))                    &rArr;  <span class="roman">unspecified</span>
(let ((x '(a)))
  (eq? x x))                    &rArr;  #t
(let ((x '#()))
  (eq? x x))                    &rArr;  #t
(let ((p (lambda (x) x)))
  (eq? p p))                    &rArr;  #t
</pre></div>

<p>Rationale: It will usually be possible to implement <code>eq?</code> much more
efficiently than <code>eqv?</code>, for example, as a simple pointer
comparison instead of as some more complicated operation.  One reason is
that it may not be possible to compute <code>eqv?</code> of two numbers in
constant time, whereas <code>eq?</code> implemented as pointer comparison will
always finish in constant time.  <code>eq?</code> may be used like <code>eqv?</code>
in applications using procedures to implement objects with state since
it obeys the same constraints as <code>eqv?</code>.
</p></dd></dl>

<dl>
<dt id="index-equal_003f-2">procedure: <strong>equal?</strong> <em>obj1 obj2</em></dt>
<dd><span id="index-circular-structure"></span>
<p><code>equal?</code> recursively compares the contents of pairs, vectors, and
strings, applying <code>eqv?</code> on other objects such as numbers, symbols,
and records.  A rule of thumb is that objects are generally
<code>equal?</code> if they print the same.  <code>equal?</code> may fail to
terminate if its arguments are circular data structures.
</p>
<div class="example">
<pre class="example">(equal? 'a 'a)                  &rArr;  #t
(equal? '(a) '(a))              &rArr;  #t
(equal? '(a (b) c)
        '(a (b) c))             &rArr;  #t
(equal? &quot;abc&quot; &quot;abc&quot;)            &rArr;  #t
(equal? 2 2)                    &rArr;  #t
(equal? (make-vector 5 'a)
        (make-vector 5 'a))     &rArr;  #t
(equal? (lambda (x) x)
        (lambda (y) y))         &rArr;  <span class="roman">unspecified</span>
</pre></div>
</dd></dl>
<hr>
<div class="header">
<p>
Next: <a href="Numbers.html" accesskey="n" rel="next">Numbers</a>, Previous: <a href="Special-Forms.html" accesskey="p" rel="prev">Special Forms</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
