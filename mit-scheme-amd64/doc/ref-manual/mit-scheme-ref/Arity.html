<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Arity (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Arity (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Arity (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Procedures.html" rel="up" title="Procedures">
<link href="Primitive-Procedures.html" rel="next" title="Primitive Procedures">
<link href="Procedure-Operations.html" rel="prev" title="Procedure Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Arity"></span><div class="header">
<p>
Next: <a href="Primitive-Procedures.html" accesskey="n" rel="next">Primitive Procedures</a>, Previous: <a href="Procedure-Operations.html" accesskey="p" rel="prev">Procedure Operations</a>, Up: <a href="Procedures.html" accesskey="u" rel="up">Procedures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Arity-1"></span><h3 class="section">12.2 Arity</h3>

<span id="index-arity"></span>
<p>Each procedure has an <em>arity</em>, which is the minimum and
(optionally) maximum number of arguments that it will accept.  MIT/GNU
Scheme provides an abstraction that represents arity, and tests for
the apparent arity of a procedure.
</p>
<p>Arity objects come in two forms: the simple form, an exact
non-negative integer, represents a fixed number of arguments.  The
general form is a pair whose <code>car</code> represents the minimum number
of arguments and whose <code>cdr</code> is the maximum number of arguments.
</p>
<dl>
<dt id="index-make_002dprocedure_002darity">procedure: <strong>make-procedure-arity</strong> <em>min [max [simple-ok?]]</em></dt>
<dd><p>Returns an arity object made from <var>min</var> and <var>max</var>.  <var>Min</var>
must be an exact non-negative integer.  <var>Max</var> must be an exact
non-negative integer at least as large as <var>min</var>.  Alternatively,
<var>max</var> may be omitted or given as &lsquo;<samp>#f</samp>&rsquo;, which represents an
arity with no upper bound.
</p>
<p>If <var>simple-ok?</var> is true, the returned arity is in the simple form
(an exact non-negative integer) when possible, and otherwise is always
in the general form.  <var>Simple-ok?</var> defaults to &lsquo;<samp>#f</samp>&rsquo;.
</p></dd></dl>

<dl>
<dt id="index-procedure_002darity_003f">procedure: <strong>procedure-arity?</strong> <em>object</em></dt>
<dd><p>Returns &lsquo;<samp>#t</samp>&rsquo; if <var>object</var> is an arity object, and &lsquo;<samp>#f</samp>&rsquo;
otherwise.
</p></dd></dl>

<dl>
<dt id="index-procedure_002darity_002dmin">procedure: <strong>procedure-arity-min</strong> <em>arity</em></dt>
<dt id="index-procedure_002darity_002dmax">procedure: <strong>procedure-arity-max</strong> <em>arity</em></dt>
<dd><p>Return the lower and upper bounds of <var>arity</var>, respectively.
</p></dd></dl>

<span id="index-condition_002dtype_003awrong_002dnumber_002dof_002darguments-2"></span>
<p>The following procedures test for the apparent arity of a procedure.
The results of the test may be less restrictive than the effect of
calling the procedure.  In other words, these procedures may indicate
that the procedure will accept a given number of arguments, but if you
call the procedure it may signal a
<code>condition-type:wrong-number-of-arguments</code> error.  For example,
here is a procedure that appears to accept any number of arguments,
but when called will signal an error if the number of arguments is not
one:
</p>
<div class="example">
<pre class="example">(lambda arguments (apply car arguments))
</pre></div>

<dl>
<dt id="index-procedure_002darity">procedure: <strong>procedure-arity</strong> <em>procedure</em></dt>
<dd><p>Returns the arity that <var>procedure</var> accepts.  The result may be in
either simple or general form.
</p>
<div class="example">
<pre class="example">(procedure-arity (lambda () 3))         &rArr;  (0 . 0)
(procedure-arity (lambda (x) x))        &rArr;  (1 . 1)
(procedure-arity car)                   &rArr;  (1 . 1)
(procedure-arity (lambda x x))          &rArr;  (0 . #f)
(procedure-arity (lambda (x . y) x))    &rArr;  (1 . #f)
(procedure-arity (lambda (x #!optional y) x))
                                        &rArr;  (1 . 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-procedure_002darity_002dvalid_003f">procedure: <strong>procedure-arity-valid?</strong> <em>procedure arity</em></dt>
<dd><p>Returns &lsquo;<samp>#t</samp>&rsquo; if <var>procedure</var> accepts <var>arity</var>, and
&lsquo;<samp>#f</samp>&rsquo; otherwise.
</p></dd></dl>

<dl>
<dt id="index-procedure_002dof_002darity_003f">procedure: <strong>procedure-of-arity?</strong> <em>object arity</em></dt>
<dd><p>Returns &lsquo;<samp>#t</samp>&rsquo; if <var>object</var> is a procedure that accepts
<var>arity</var>, and &lsquo;<samp>#f</samp>&rsquo; otherwise.  Equivalent to:
</p>
<div class="example">
<pre class="example">(and (procedure? <var>object</var>)
     (procedure-arity-valid? <var>object</var> <var>arity</var>))
</pre></div>
</dd></dl>

<dl>
<dt id="index-guarantee_002dprocedure_002dof_002darity">procedure: <strong>guarantee-procedure-of-arity</strong> <em>object arity caller</em></dt>
<dd><p>Signals an error if <var>object</var> is not a procedure accepting
<var>arity</var>.  <var>Caller</var> is a symbol that is printed as part of the
error message and is intended to be the name of the procedure where
the error occurs.
</p></dd></dl>

<dl>
<dt id="index-thunk_003f">procedure: <strong>thunk?</strong> <em>object</em></dt>
<dd><p>Returns &lsquo;<samp>#t</samp>&rsquo; if <var>object</var> is a procedure that accepts
zero arguments, and &lsquo;<samp>#f</samp>&rsquo; otherwise.  Equivalent to:
</p>
<div class="example">
<pre class="example">(procedure-of-arity? <var>object</var> 0)
</pre></div>
</dd></dl>


<hr>
<div class="header">
<p>
Next: <a href="Primitive-Procedures.html" accesskey="n" rel="next">Primitive Procedures</a>, Previous: <a href="Procedure-Operations.html" accesskey="p" rel="prev">Procedure Operations</a>, Up: <a href="Procedures.html" accesskey="u" rel="up">Procedures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
