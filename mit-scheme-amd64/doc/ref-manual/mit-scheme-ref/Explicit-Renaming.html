<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Explicit Renaming (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Explicit Renaming (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Explicit Renaming (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Macros.html" rel="up" title="Macros">
<link href="SRFI-syntax.html" rel="next" title="SRFI syntax">
<link href="SC-Identifiers.html" rel="prev" title="SC Identifiers">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Explicit-Renaming"></span><div class="header">
<p>
Previous: <a href="Syntactic-Closures.html" accesskey="p" rel="prev">Syntactic Closures</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Explicit-Renaming-1"></span><h4 class="subsection">2.11.4 Explicit Renaming</h4>

<span id="index-explicit-renaming"></span>
<p><em>Explicit renaming</em> is an alternative facility for defining macro
transformers.  In the MIT/GNU Scheme implementation, explicit-renaming
transformers are implemented as an abstraction layer on top of syntactic
closures.  An explicit-renaming macro transformer is defined by an
instance of the <code>er-macro-transformer</code> keyword:
</p>
<dl>
<dt id="index-er_002dmacro_002dtransformer">special form: <strong>er-macro-transformer</strong> <em>expression</em></dt>
<dd><p>The <var>expression</var> is expanded in the syntactic environment of the
<code>er-macro-transformer</code> expression, and the expanded expression is
evaluated in the transformer environment to yield a macro transformer as
described below.  This macro transformer is bound to a macro keyword by
the special form in which the <code>transformer</code> expression appears (for
example, <code>let-syntax</code>).
</p>
<span id="index-macro-transformer-2"></span>
<span id="index-input-form_002c-to-macro"></span>
<p>In the explicit-renaming facility, a <em>macro transformer</em> is a
procedure that takes three arguments, a form, a renaming procedure, and
a comparison predicate, and returns a new form.  The first argument, the
<em>input form</em>, is the form in which the macro keyword occurred.
</p>
<span id="index-renaming-procedure"></span>
<p>The second argument to a transformation procedure is a <em>renaming
procedure</em> that takes the representation of an identifier as its
argument and returns the representation of a fresh identifier that
occurs nowhere else in the program.  For example, the transformation
procedure for a simplified version of the <code>let</code> macro might be
written as
</p>
<div class="example">
<pre class="example">(lambda (exp rename compare)
  (let ((vars (map car (cadr exp)))
        (inits (map cadr (cadr exp)))
        (body (cddr exp)))
    `((lambda ,vars ,@body)
      ,@inits)))
</pre></div>

<p>This would not be hygienic, however.  A hygienic <code>let</code> macro must
rename the identifier <code>lambda</code> to protect it from being captured by
a local binding.  The renaming effectively creates an fresh alias for
<code>lambda</code>, one that cannot be captured by any subsequent binding:
</p>
<div class="example">
<pre class="example">(lambda (exp rename compare)
  (let ((vars (map car (cadr exp)))
        (inits (map cadr (cadr exp)))
        (body (cddr exp)))
    `((,(rename 'lambda) ,vars ,@body)
      ,@inits)))
</pre></div>

<p>The expression returned by the transformation procedure will be expanded
in the syntactic environment obtained from the syntactic environment of
the macro application by binding any fresh identifiers generated by the
renaming procedure to the denotations of the original identifiers in the
syntactic environment in which the macro was defined.  This means that a
renamed identifier will denote the same thing as the original identifier
unless the transformation procedure that renamed the identifier placed
an occurrence of it in a binding position.
</p>
<p>The renaming procedure acts as a mathematical function in the sense that
the identifiers obtained from any two calls with the same argument will
be the same in the sense of <code>eqv?</code>.  It is an error if the renaming
procedure is called after the transformation procedure has returned.
</p>
<span id="index-comparison-predicate"></span>
<p>The third argument to a transformation procedure is a <em>comparison
predicate</em> that takes the representations of two identifiers as its
arguments and returns true if and only if they denote the same thing in
the syntactic environment that will be used to expand the transformed
macro application.  For example, the transformation procedure for a
simplified version of the <code>cond</code> macro can be written as
</p>
<div class="example">
<pre class="example">(lambda (exp rename compare)
  (let ((clauses (cdr exp)))
    (if (null? clauses)
        `(,(rename 'quote) unspecified)
        (let* ((first (car clauses))
               (rest (cdr clauses))
               (test (car first)))
          (cond ((and (identifier? test)
                      (compare test (rename 'else)))
                 `(,(rename 'begin) ,@(cdr first)))
                (else `(,(rename 'if)
                        ,test
                         (,(rename 'begin) ,@(cdr first))
                         (cond ,@rest))))))))))
</pre></div>

<p>In this example the identifier <code>else</code> is renamed before being passed
to the comparison predicate, so the comparison will be true if and
only if the test expression is an identifier that denotes the same
thing in the syntactic environment of the expression being transformed
as <code>else</code> denotes in the syntactic environment in which the <code>cond</code>
macro was defined.  If <code>else</code> were not renamed before being passed to
the comparison predicate, then it would match a local variable that
happened to be named <code>else</code>, and the macro would not be hygienic.
</p>
<p>Some macros are non-hygienic by design.  For example, the following
defines a <code>loop</code> macro that implicitly binds <code>exit</code> to an
escape procedure.  The binding of <code>exit</code> is intended to capture
free references to <code>exit</code> in the body of the loop, so <code>exit</code>
is not renamed.
</p>
<div class="example">
<pre class="example">(define-syntax loop
  (er-macro-transformer
   (lambda (x r c)
     (let ((body (cdr x)))
       `(,(r 'call-with-current-continuation)
         (,(r 'lambda) (exit)
          (,(r 'let) ,(r 'f) () ,@body (,(r 'f)))))))))
</pre></div>

<p>Suppose a <code>while</code> macro is implemented using <code>loop</code>, with the
intent that <code>exit</code> may be used to escape from the <code>while</code>
loop.  The <code>while</code> macro cannot be written as
</p>
<div class="example">
<pre class="example">(define-syntax while
  (syntax-rules ()
    ((while test body ...)
     (loop (if (not test) (exit #f))
           body ...))))
</pre></div>

<p>because the reference to <code>exit</code> that is inserted by the
<code>while</code> macro is intended to be captured by the binding of
<code>exit</code> that will be inserted by the <code>loop</code> macro.  In other
words, this <code>while</code> macro is not hygienic.  Like <code>loop</code>, it
must be written using the <code>er-macro-transformer</code> syntax:
</p>
<div class="example">
<pre class="example">(define-syntax while
  (er-macro-transformer
   (lambda (x r c)
     (let ((test (cadr x))
           (body (cddr x)))
       `(,(r 'loop)
         (,(r 'if) (,(r 'not) ,test) (exit #f))
         ,@body)))))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Syntactic-Closures.html" accesskey="p" rel="prev">Syntactic Closures</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
