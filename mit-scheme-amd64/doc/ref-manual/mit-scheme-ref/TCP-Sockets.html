<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>TCP Sockets (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="TCP Sockets (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="TCP Sockets (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Operating_002dSystem-Interface.html" rel="up" title="Operating-System Interface">
<link href="Miscellaneous-OS-Facilities.html" rel="next" title="Miscellaneous OS Facilities">
<link href="Subprocess-Options.html" rel="prev" title="Subprocess Options">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="TCP-Sockets"></span><div class="header">
<p>
Next: <a href="Miscellaneous-OS-Facilities.html" accesskey="n" rel="next">Miscellaneous OS Facilities</a>, Previous: <a href="Subprocesses.html" accesskey="p" rel="prev">Subprocesses</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="TCP-Sockets-1"></span><h3 class="section">15.8 TCP Sockets</h3>

<span id="index-socket"></span>
<p>MIT/GNU Scheme provides access to <em>sockets</em>, which are a mechanism for
inter-process communication.  <small>TCP</small> stream sockets are supported,
which communicate between computers over a <small>TCP/IP</small> network.
<small>TCP</small> sockets are supported on all operating systems.
</p>
<span id="index-client-socket"></span>
<span id="index-server-socket"></span>
<p><small>TCP</small> sockets have two distinct interfaces: one interface to
implement a <em>client</em> and another to implement a <em>server</em>.  The
basic protocol is that servers set up a listening port and wait for
connections from clients.  Implementation of clients is simpler and will
be treated first.
</p>
<span id="index-hostname_002c-TCP"></span>
<p>The socket procedures accept two special arguments, called
<var>host-name</var> and <var>service</var>.  <var>Host-name</var> is a string which
must be the name of an internet host.  It is looked up using the
ordinary lookup rules for your computer.  For example, if your host is
<code>foo.mit.edu</code> and <var>host-name</var> is <code>&quot;bar&quot;</code>, then it
specifies <code>bar.mit.edu</code>.
</p>
<span id="index-service_002c-TCP"></span>
<span id="index-port-number_002c-TCP"></span>
<p><var>Service</var> specifies the service to which you will connect.  A
networked computer normally provides several different services, such as
telnet or <acronym>FTP</acronym>.  Each service is associated with a unique
<em>port number</em>; for example, the <code>&quot;www&quot;</code> service is associated
with port <code>80</code>.  The <var>service</var> argument specifies the port
number, either as a string, or directly as an exact non-negative
integer.  Port strings are decoded by the operating system using a
table; for example, on unix the table is in <samp>/etc/services</samp>.
Usually you will use a port string rather than a number.
</p>
<dl>
<dt id="index-open_002dtcp_002dstream_002dsocket">procedure: <strong>open-tcp-stream-socket</strong> <em>host-name service</em></dt>
<dd><p><code>open-tcp-stream-socket</code> opens a connection to the host specified
by <var>host-name</var>.  <var>Host-name</var> is looked up using the ordinary
lookup rules for your computer.  The connection is established to the
service specified by <var>service</var>.  The returned value is an
<acronym>I/O</acronym> port, to which you can read and write characters using
ordinary Scheme <acronym>I/O</acronym> procedures such as <code>read-char</code> and
<code>write-char</code>.
</p>
<p>When you wish to close the connection, just use <code>close-port</code>.
</p>
<p>As an example, here is how you can open a connection to a web server:
</p>
<div class="example">
<pre class="example">(open-tcp-stream-socket &quot;web.mit.edu&quot; &quot;www&quot;)
</pre></div>
</dd></dl>

<span id="index-server-socket-1"></span>
<p>Next we will treat setting up a <small>TCP</small> server, which is slightly more
complicated.  Creating a server is a two-part process.  First, you must
open a <em>server socket</em>, which causes the operating system to listen
to the network on a port that you specify.  Once the server socket is
opened, the operating system will allow clients to connect to your
computer on that port.
</p>
<p>In the second step of the process, you <em>accept</em> the connection,
which completes the connection initiated by the client, and allows you
to communicate with the client.  Accepting a connection does not affect
the server socket; it continues to listen for additional client
connections.  You can have multiple client connections to the same
server socket open simultaneously.
</p>
<dl>
<dt id="index-open_002dtcp_002dserver_002dsocket">procedure: <strong>open-tcp-server-socket</strong> <em>service [address]</em></dt>
<dd><p>This procedure opens a server socket that listens for connections to
<var>service</var>; the socket will continue to listen until you close it.
The returned value is a server socket object.
</p>
<p>An error is signalled if another process is already listening on the
service.  Additionally, ports whose number is less than <code>1024</code> are
privileged on many operating systems, and cannot be used by
non-privileged processes; if <var>service</var> specifies such a port and you
do not have administrative privileges, an error may be signalled.
</p>
<p>The optional argument <var>address</var> specifies the <acronym>IP</acronym> address
on which the socket will listen.  If this argument is not supplied or is
given as <code>#f</code>, then the socket listens on all <acronym>IP</acronym>
addresses for this machine.  (This is equivalent to passing the result
of calling <code>host-address-any</code>.)
</p></dd></dl>

<dl>
<dt id="index-tcp_002dserver_002dconnection_002daccept">procedure: <strong>tcp-server-connection-accept</strong> <em>server-socket block? peer-address [line-translation]</em></dt>
<dd><p>Checks to see if a client has connected to <var>server-socket</var>.  If
so, an <acronym>I/O</acronym> port is returned.  The returned port can be read
and written using ordinary Scheme <acronym>I/O</acronym> procedures such as
<code>read-char</code> and <code>write-char</code>.
</p>
<p>The argument <var>block?</var> says what to do if no client has connected at
the time of the call.  If <code>#f</code>, it says to return immediately with
two values of <code>#f</code>.  Otherwise, the call waits until a client
connects.
</p>
<p>The argument <var>peer-address</var> is either <code>#f</code> or an <acronym>IP</acronym>
address as allocated by <code>allocate-host-address</code>.  If it is an
<acronym>IP</acronym> address, the address is modified to be the address of the
client making the connection.
</p>
<p>The optional argument <var>line-translation</var> specifies how end-of-line
characters will be translated when reading or writing to the returned
socket.  If this is unspecified or <code>#f</code>, then lines will be
terminated by <small>CR-LF</small>, which is the standard for most internet
protocols.  Otherwise, it must be a string, which specifies the
line-ending character sequence to use.
</p>
<p>Note that closing the port returned by this procedure does not affect
<var>server-socket</var>; it just closes the particular client connection
that was opened by the call.  To close <var>server-socket</var>, use
<code>close-tcp-server-socket</code>.
</p></dd></dl>

<dl>
<dt id="index-close_002dtcp_002dserver_002dsocket">procedure: <strong>close-tcp-server-socket</strong> <em>server-socket</em></dt>
<dd><p>Closes the server socket <var>server-socket</var>.  The operating system will
cease listening for network connections to that service.  Client
connections to <var>server-socket</var> that have already been accepted will
not be affected.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Miscellaneous-OS-Facilities.html" accesskey="n" rel="next">Miscellaneous OS Facilities</a>, Previous: <a href="Subprocesses.html" accesskey="p" rel="prev">Subprocesses</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
