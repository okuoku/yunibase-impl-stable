<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Condition Signalling (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Condition Signalling (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Condition Signalling (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Error-System.html" rel="up" title="Error System">
<link href="Error-Messages.html" rel="next" title="Error Messages">
<link href="Error-System.html" rel="prev" title="Error System">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Condition-Signalling"></span><div class="header">
<p>
Next: <a href="Error-Messages.html" accesskey="n" rel="next">Error Messages</a>, Previous: <a href="Error-System.html" accesskey="p" rel="prev">Error System</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Condition-Signalling-1"></span><h3 class="section">16.1 Condition Signalling</h3>

<span id="index-condition-signalling-_0028defn_0029"></span>
<span id="index-signalling_002c-of-condition-_0028defn_0029"></span>
<span id="index-make_002dcondition"></span>
<p>Once a condition instance has been created using <code>make-condition</code>
(or any condition constructor), it can be <em>signalled</em>.  The act of
signalling a condition is separated from the act of creating the
condition to allow more flexibility in how conditions are handled.  For
example, a condition instance could be returned as the value of a
procedure, indicating that something unusual has happened, to allow the
caller to clean up some state.  The caller could then signal the
condition once it is ready.
</p>
<p>A more important reason for having a separate condition-signalling
mechanism is that it allows <em>resignalling</em>.  When a signalled
condition has been caught by a particular handler, and the handler decides
that it doesn&rsquo;t want to process that particular condition, it can signal
the condition again.  This is one way to allow other handlers to get a
chance to see the condition.
</p>
<dl>
<dt id="index-error-2">procedure: <strong>error</strong> <em>reason argument &hellip;</em></dt>
<dd><span id="index-REP-loop"></span>
<span id="index-signal_002dcondition-1"></span>
<span id="index-warn"></span>
<p>This is the simplest and most common way to signal a condition that
requires intervention before a computation can proceed (when
intervention is not required, <code>warn</code> is more appropriate).
<code>error</code> signals a condition (using <code>signal-condition</code>), and if
no handler for that condition alters the flow of control (by invoking a
restart, for example) it calls the procedure
<code>standard-error-handler</code>, which normally prints an error message
and stops the computation, entering an error <small>REPL</small>.  Under normal
circumstances <code>error</code> will not return a value (although an
interactive debugger can be used to force this to occur).
</p>
<span id="index-make_002dcondition-1"></span>
<span id="index-condition_002dtype_003asimple_002derror-1"></span>
<p>Precisely what condition is signalled depends on the first argument to
<code>error</code>.  If <var>reason</var> is a condition, then that condition is
signalled and the <var>argument</var>s are ignored.  If <var>reason</var> is a
condition type, then a new instance of this type is generated and
signalled; the <var>argument</var>s are used to generate the values of the
fields for this condition type (they are passed as the <var>field-plist</var>
argument to <code>make-condition</code>).  In the most common case, however,
<var>reason</var> is neither a condition nor a condition type, but rather a
string or symbol.  In this case a condition of type
<code>condition-type:simple-error</code> is created with the <var>message</var>
field containing the <var>reason</var> and the <var>irritants</var> field
containing the <var>argument</var>s.
</p></dd></dl>

<dl>
<dt id="index-warn-1">procedure: <strong>warn</strong> <em>reason argument &hellip;</em></dt>
<dd><span id="index-error-3"></span>
<span id="index-signal_002dcondition-2"></span>
<span id="index-condition_002dtype_003asimple_002dwarning-1"></span>
<p>When a condition is not severe enough to warrant intervention, it is
appropriate to signal the condition with <code>warn</code> rather than
<code>error</code>.  As with <code>error</code>, <code>warn</code> first calls
<code>signal-condition</code>; the condition that is signalled is chosen
exactly as in <code>error</code> except that a condition of type
<code>condition-type:simple-warning</code> is signalled if <var>reason</var> is
neither a condition nor a condition type.  If the condition is not
handled, <code>warn</code> calls the procedure
<code>standard-warning-handler</code>, which normally prints a warning message
and continues the computation by returning from <code>warn</code>.
</p>
<span id="index-muffle_002dwarning"></span>
<p><code>warn</code> establishes a restart named <code>muffle-warning</code> before
calling <code>signal-condition</code>.  This allows a signal handler to
prevent the generation of the warning message by calling
<code>muffle-warning</code>.  The value of a call to <code>warn</code> is
unspecified.
</p></dd></dl>

<dl>
<dt id="index-signal_002dcondition-3">procedure: <strong>signal-condition</strong> <em>condition</em></dt>
<dd><span id="index-generalization_002c-of-condition-types-1"></span>
<span id="index-specialization_002c-of-condition-types-1"></span>
<span id="index-break_002don_002dsignals"></span>
<span id="index-bind_002ddefault_002dcondition_002dhandler"></span>
<span id="index-bind_002dcondition_002dhandler-1"></span>
<p>This is the fundamental operation for signalling a condition.  The
precise operation of <code>signal-condition</code> depends on the condition
type of which <var>condition</var> is an instance, the condition types set by
<code>break-on-signals</code>, and the handlers established by
<code>bind-condition-handler</code> and <code>bind-default-condition-handler</code>.
</p>
<span id="index-REP-loop-1"></span>
<p>If the <var>condition</var> is an instance of a type that is a specialization
of any of the types specified by <code>break-on-signals</code>, then a
breakpoint <small>REPL</small> is initiated.  Otherwise (or when that <small>REPL</small>
returns), the handlers established by <code>bind-condition-handler</code> are
checked, most recent first.  Each applicable handler is invoked, and the
search for a handler continues if the handler returns normally.  If all
applicable handlers return, then the applicable handlers established by
<code>bind-default-condition-handler</code> are checked, again most recent
first.  Finally, if no handlers apply (or all return in a normal
manner), <code>signal-condition</code> returns an unspecified value.
</p>
<p><em>Note:</em> unlike many other systems, the MIT/GNU Scheme runtime library
does <em>not</em> establish handlers of any kind.  (However, the Edwin
text editor uses condition handlers extensively.)  Thus, calls to
<code>signal-condition</code> will return to the caller unless there are user
supplied condition handlers, as the following example shows:
</p>
<div class="example">
<pre class="example">(signal-condition
 (make-condition
  condition-type:error
  (call-with-current-continuation (lambda (x) x))
  '()    <span class="roman">; no restarts</span>
  '()))  <span class="roman">; no fields</span>
&rArr;  <span class="roman">unspecified</span>
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Error-Messages.html" accesskey="n" rel="next">Error Messages</a>, Previous: <a href="Error-System.html" accesskey="p" rel="prev">Error System</a>, Up: <a href="Error-System.html" accesskey="u" rel="up">Error System</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
