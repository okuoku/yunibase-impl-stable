<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Delimiters (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Delimiters (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Delimiters (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Lexical-Conventions.html" rel="up" title="Lexical Conventions">
<link href="Identifiers.html" rel="next" title="Identifiers">
<link href="Whitespace.html" rel="prev" title="Whitespace">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Delimiters"></span><div class="header">
<p>
Next: <a href="Identifiers.html" accesskey="n" rel="next">Identifiers</a>, Previous: <a href="Whitespace.html" accesskey="p" rel="prev">Whitespace</a>, Up: <a href="Lexical-Conventions.html" accesskey="u" rel="up">Lexical Conventions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Delimiters-1"></span><h4 class="subsection">1.3.2 Delimiters</h4>

<span id="index-delimiter_002c-in-programs-_0028defn_0029"></span>
<p>All whitespace characters are <em>delimiters</em>.  In addition, the
following characters act as delimiters:
</p>
<div class="example">
<pre class="example">(  )  ;  &quot;  '  `  |
</pre></div>

<p>Finally, these next characters act as delimiters, despite the fact that
Scheme does not define any special meaning for them:
</p>
<div class="example">
<pre class="example">[  ]  {  }
</pre></div>

<p>For example, if the value of the variable <code>name</code> is
<code>&quot;max&quot;</code>:
</p>
<div class="example">
<pre class="example">(list&quot;Hi&quot;name(+ 1 2))                   &rArr;  (&quot;Hi&quot; &quot;max&quot; 3)
</pre></div>




</body>
</html>
