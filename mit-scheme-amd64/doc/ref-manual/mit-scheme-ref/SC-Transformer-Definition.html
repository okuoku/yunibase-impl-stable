<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>SC Transformer Definition (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="SC Transformer Definition (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="SC Transformer Definition (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Syntactic-Closures.html" rel="up" title="Syntactic Closures">
<link href="SC-Identifiers.html" rel="next" title="SC Identifiers">
<link href="Syntax-Terminology.html" rel="prev" title="Syntax Terminology">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="SC-Transformer-Definition"></span><div class="header">
<p>
Next: <a href="SC-Identifiers.html" accesskey="n" rel="next">SC Identifiers</a>, Previous: <a href="Syntax-Terminology.html" accesskey="p" rel="prev">Syntax Terminology</a>, Up: <a href="Syntactic-Closures.html" accesskey="u" rel="up">Syntactic Closures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Transformer-Definition"></span><h4 class="subsubsection">2.11.3.2 Transformer Definition</h4>

<p>This section describes the special forms for defining syntactic-closures
macro transformers, and the associated procedures for manipulating
syntactic closures and syntactic environments.
</p>
<dl>
<dt id="index-sc_002dmacro_002dtransformer">special form: <strong>sc-macro-transformer</strong> <em>expression</em></dt>
<dd><p>The <var>expression</var> is expanded in the syntactic environment of the
<code>sc-macro-transformer</code> expression, and the expanded expression is
evaluated in the transformer environment to yield a macro transformer as
described below.  This macro transformer is bound to a macro keyword by
the special form in which the <code>transformer</code> expression appears (for
example, <code>let-syntax</code>).
</p>
<span id="index-macro-transformer-1"></span>
<span id="index-input-form"></span>
<span id="index-usage-environment"></span>
<span id="index-output-form"></span>
<span id="index-transformer-environment"></span>
<p>In the syntactic closures facility, a <em>macro transformer</em> is a
procedure that takes two arguments, a form and a syntactic environment,
and returns a new form.  The first argument, the <em>input form</em>, is
the form in which the macro keyword occurred.  The second argument, the
<em>usage environment</em>, is the syntactic environment in which the input
form occurred.  The result of the transformer, the <em>output form</em>, is
automatically closed in the <em>transformer environment</em>, which is the
syntactic environment in which the <code>transformer</code> expression
occurred.
</p>
<p>For example, here is a definition of a <code>push</code> macro using
<code>syntax-rules</code>:
</p>
<div class="example">
<pre class="example">(define-syntax push
  (syntax-rules ()
    ((push item list)
     (set! list (cons item list)))))
</pre></div>

<p>Here is an equivalent definition using <code>sc-macro-transformer</code>:
</p>
<div class="example">
<pre class="example">(define-syntax push
  (sc-macro-transformer
   (lambda (exp env)
     (let ((item (make-syntactic-closure env '() (cadr exp)))
           (list (make-syntactic-closure env '() (caddr exp))))
       `(set! ,list (cons ,item ,list))))))
</pre></div>

<p>In this example, the identifiers <code>set!</code> and <code>cons</code> are closed
in the transformer environment, and thus will not be affected by the
meanings of those identifiers in the usage environment <code>env</code>.
</p>
<p>Some macros may be non-hygienic by design.  For example, the following
defines a <code>loop</code> macro that implicitly binds <code>exit</code> to an
escape procedure.  The binding of <code>exit</code> is intended to capture
free references to <code>exit</code> in the body of the loop, so <code>exit</code>
must be left free when the body is closed:
</p>
<div class="example">
<pre class="example">(define-syntax loop
  (sc-macro-transformer
   (lambda (exp env)
     (let ((body (cdr exp)))
       `(call-with-current-continuation
         (lambda (exit)
           (let f ()
             ,@(map (lambda (exp)
                      (make-syntactic-closure env '(exit)
                        exp))
                    body)
             (f))))))))
</pre></div>
</dd></dl>

<dl>
<dt id="index-rsc_002dmacro_002dtransformer">special form: <strong>rsc-macro-transformer</strong> <em>expression</em></dt>
<dd><p>This form is an alternative way to define a syntactic-closures macro
transformer.  Its syntax and usage are identical to
<code>sc-macro-transformer</code>, except that the roles of the usage
environment and transformer environment are reversed.  (Hence
<acronym>RSC</acronym> stands for <em>Reversed Syntactic Closures</em>.)  In other
words, the procedure specified by <var>expression</var> still accepts two
arguments, but its second argument will be the transformer environment
rather than the usage environment, and the returned expression is closed
in the usage environment rather than the transformer environment.
</p>
<p>The advantage of this arrangement is that it allows a simpler definition
style in some situations.  For example, here is the <code>push</code> macro
from above, rewritten in this style:
</p>
<div class="example">
<pre class="example">(define-syntax push
  (rsc-macro-transformer
   (lambda (exp env)
     `(,(make-syntactic-closure env '() 'SET!)
       ,(caddr exp)
       (,(make-syntactic-closure env '() 'CONS)
        ,(cadr exp)
        ,(caddr exp))))))
</pre></div>

<p>In this style only the introduced keywords are closed, while everything
else remains open.
</p>
<p>Note that <code>rsc-macro-transformer</code> and <code>sc-macro-transformer</code>
are easily interchangeable.  Here is how to emulate
<code>rsc-macro-transformer</code> using <code>sc-macro-transformer</code>.  (This
technique can be used to effect the opposite emulation as well.)
</p>
<div class="example">
<pre class="example">(define-syntax push
  (sc-macro-transformer
   (lambda (exp usage-env)
     (capture-syntactic-environment
      (lambda (env)
        (make-syntactic-closure usage-env '()
          `(,(make-syntactic-closure env '() 'SET!)
            ,(caddr exp)
            (,(make-syntactic-closure env '() 'CONS)
             ,(cadr exp)
             ,(caddr exp)))))))))
</pre></div>
</dd></dl>

<p>To assign meanings to the identifiers in a form, use
<code>make-syntactic-closure</code> to close the form in a syntactic
environment.
</p>
<dl>
<dt id="index-make_002dsyntactic_002dclosure">procedure: <strong>make-syntactic-closure</strong> <em>environment free-names form</em></dt>
<dd><p><var>Environment</var> must be a syntactic environment, <var>free-names</var>
must be a list of identifiers, and <var>form</var> must be a form.
<code>make-syntactic-closure</code> constructs and returns a syntactic
closure of <var>form</var> in <var>environment</var>, which can be used anywhere
that <var>form</var> could have been used.  All the identifiers used in
<var>form</var>, except those explicitly excepted by <var>free-names</var>,
obtain their meanings from <var>environment</var>.
</p>
<p>Here is an example where <var>free-names</var> is something other than the
empty list.  It is instructive to compare the use of <var>free-names</var>
in this example with its use in the <code>loop</code> example above: the
examples are similar except for the source of the identifier being left
free.
</p>
<div class="example">
<pre class="example">(define-syntax let1
  (sc-macro-transformer
   (lambda (exp env)
     (let ((id (cadr exp))
           (init (caddr exp))
           (exp (cadddr exp)))
       `((lambda (,id)
           ,(make-syntactic-closure env (list id) exp))
         ,(make-syntactic-closure env '() init))))))
</pre></div>

<p><code>let1</code> is a simplified version of <code>let</code> that only binds a
single identifier, and whose body consists of a single expression.
When the body expression is syntactically closed in its original
syntactic environment, the identifier that is to be bound by
<code>let1</code> must be left free, so that it can be properly captured by
the <code>lambda</code> in the output form.
</p></dd></dl>

<p>In most situations, the <var>free-names</var> argument to
<code>make-syntactic-closure</code> is the empty list.  In those cases, the
more succinct <code>close-syntax</code> can be used:
</p>
<dl>
<dt id="index-close_002dsyntax">procedure: <strong>close-syntax</strong> <em>form environment</em></dt>
<dd><p><var>Environment</var> must be a syntactic environment and <var>form</var> must be
a form.  Returns a new syntactic closure of <var>form</var> in
<var>environment</var>, with no free names.  Entirely equivalent to
</p>
<div class="example">
<pre class="example">(make-syntactic-closure <var>environment</var> '() <var>form</var>)
</pre></div>
</dd></dl>

<p>To obtain a syntactic environment other than the usage environment,
use <code>capture-syntactic-environment</code>.
</p>
<dl>
<dt id="index-capture_002dsyntactic_002denvironment">procedure: <strong>capture-syntactic-environment</strong> <em>procedure</em></dt>
<dd><p><code>capture-syntactic-environment</code> returns a form that will, when
transformed, call <var>procedure</var> on the current syntactic environment.
<var>Procedure</var> should compute and return a new form to be transformed,
in that same syntactic environment, in place of the form.
</p>
<p>An example will make this clear.  Suppose we wanted to define a simple
<code>loop-until</code> keyword equivalent to
</p>
<div class="example">
<pre class="example">(define-syntax loop-until
  (syntax-rules ()
    ((loop-until id init test return step)
     (letrec ((loop
               (lambda (id)
                 (if test return (loop step)))))
       (loop init)))))
</pre></div>

<p>The following attempt at defining <code>loop-until</code> has a subtle
bug:
</p>
<div class="example">
<pre class="example">(define-syntax loop-until
  (sc-macro-transformer
   (lambda (exp env)
     (let ((id (cadr exp))
           (init (caddr exp))
           (test (cadddr exp))
           (return (cadddr (cdr exp)))
           (step (cadddr (cddr exp)))
           (close
            (lambda (exp free)
              (make-syntactic-closure env free exp))))
       `(letrec ((loop
                  (lambda (,id)
                    (if ,(close test (list id))
                        ,(close return (list id))
                        (loop ,(close step (list id)))))))
          (loop ,(close init '())))))))
</pre></div>

<p>This definition appears to take all of the proper precautions to
prevent unintended captures.  It carefully closes the subexpressions in
their original syntactic environment and it leaves the <code>id</code>
identifier free in the <code>test</code>, <code>return</code>, and <code>step</code>
expressions, so that it will be captured by the binding introduced by
the <code>lambda</code> expression.  Unfortunately it uses the identifiers
<code>if</code> and <code>loop</code> <em>within</em> that <code>lambda</code> expression,
so if the user of <code>loop-until</code> just happens to use, say, <code>if</code>
for the identifier, it will be inadvertently captured.
</p>
<p>The syntactic environment that <code>if</code> and <code>loop</code> want to be
exposed to is the one just outside the <code>lambda</code> expression: before
the user&rsquo;s identifier is added to the syntactic environment, but after
the identifier <code>loop</code> has been added.
<code>capture-syntactic-environment</code> captures exactly that environment
as follows:
</p>
<div class="example">
<pre class="example">(define-syntax loop-until
  (sc-macro-transformer
   (lambda (exp env)
     (let ((id (cadr exp))
           (init (caddr exp))
           (test (cadddr exp))
           (return (cadddr (cdr exp)))
           (step (cadddr (cddr exp)))
           (close
            (lambda (exp free)
              (make-syntactic-closure env free exp))))
       `(letrec ((loop
                  ,(capture-syntactic-environment
                    (lambda (env)
                      `(lambda (,id)
                         (,(make-syntactic-closure env '() `if)
                          ,(close test (list id))
                          ,(close return (list id))
                          (,(make-syntactic-closure env '() `loop)
                           ,(close step (list id)))))))))
          (loop ,(close init '())))))))
</pre></div>

<p>In this case, having captured the desired syntactic environment, it is
convenient to construct syntactic closures of the identifiers <code>if</code>
and the <code>loop</code> and use them in the body of the
<code>lambda</code>.
</p>
<p>A common use of <code>capture-syntactic-environment</code> is to get the
transformer environment of a macro transformer:
</p>
<div class="example">
<pre class="example">(sc-macro-transformer
 (lambda (exp env)
   (capture-syntactic-environment
    (lambda (transformer-env)
      &hellip;))))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="SC-Identifiers.html" accesskey="n" rel="next">SC Identifiers</a>, Previous: <a href="Syntax-Terminology.html" accesskey="p" rel="prev">Syntax Terminology</a>, Up: <a href="Syntactic-Closures.html" accesskey="u" rel="up">Syntactic Closures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
