<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Syntactic Binding Constructs (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Syntactic Binding Constructs (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Syntactic Binding Constructs (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Macros.html" rel="up" title="Macros">
<link href="Pattern-Language.html" rel="next" title="Pattern Language">
<link href="Macros.html" rel="prev" title="Macros">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Syntactic-Binding-Constructs"></span><div class="header">
<p>
Next: <a href="Pattern-Language.html" accesskey="n" rel="next">Pattern Language</a>, Previous: <a href="Macros.html" accesskey="p" rel="prev">Macros</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Binding-Constructs-for-Syntactic-Keywords"></span><h4 class="subsection">2.11.1 Binding Constructs for Syntactic Keywords</h4>

<p><code>let-syntax</code>, <code>letrec-syntax</code>, <code>let*-syntax</code> and
<code>define-syntax</code> are analogous to <code>let</code>, <code>letrec</code>,
<code>let*</code> and <code>define</code>, but they bind syntactic keywords to macro
transformers instead of binding variables to locations that contain
values.
</p>
<p>Any argument named <var>transformer-spec</var> must be a macro-transformer
expression, which is one of the following:
</p>
<ul>
<li> A macro transformer defined by the pattern language and denoted by the
syntactic keyword <code>syntax-rules</code>.

</li><li> A macro transformer defined by one of the low-level mechanisms and
denoted by one of the syntactic keywords <code>sc-macro-transformer</code>,
<code>rsc-macro-transformer</code>, or <code>er-macro-transformer</code>.

</li><li> A syntactic keyword bound in the enclosing environment.  This is used
to bind another name to an existing macro transformer.
</li></ul>

<dl>
<dt id="index-let_002dsyntax">standard special form: <strong>let-syntax</strong> <em>bindings expression expression &hellip;</em></dt>
<dd><p><var>Bindings</var> should have the form
</p>
<div class="example">
<pre class="example">((<var>keyword</var> <var>transformer-spec</var>) &hellip;)
</pre></div>

<p>Each <var>keyword</var> is an identifier, each <var>transformer-spec</var> is a
a macro-transformer expression, and the body is a sequence of
one or more expressions.  It is an error for a <var>keyword</var> to appear
more than once in the list of keywords being bound.
</p>
<p>The <var>expression</var>s are expanded in the syntactic environment obtained
by extending the syntactic environment of the <code>let-syntax</code>
expression with macros whose keywords are the <var>keyword</var>s, bound to
the specified transformers.  Each binding of a <var>keyword</var> has the
<var>expression</var>s as its region.
</p>
<div class="example">
<pre class="example">(let-syntax ((when (syntax-rules ()
                     ((when test stmt1 stmt2 ...)
                      (if test
                          (begin stmt1
                                 stmt2 ...))))))
  (let ((if #t))
    (when if (set! if 'now))
    if))                           &rArr;  now

(let ((x 'outer))
  (let-syntax ((m (syntax-rules () ((m) x))))
    (let ((x 'inner))
      (m))))                       &rArr;  outer
</pre></div>
</dd></dl>

<dl>
<dt id="index-letrec_002dsyntax">standard special form: <strong>letrec-syntax</strong> <em>bindings expression expression &hellip;</em></dt>
<dd><p>The syntax of <code>letrec-syntax</code> is the same as for <code>let-syntax</code>.
</p>
<p>The <var>expression</var>s are expanded in the syntactic environment obtained
by extending the syntactic environment of the <code>letrec-syntax</code>
expression with macros whose keywords are the <var>keyword</var>s, bound to
the specified transformers.  Each binding of a <var>keyword</var> has the
<var>bindings</var> as well as the <var>expression</var>s within its region, so
the transformers can transcribe expressions into uses of the macros
introduced by the <code>letrec-syntax</code> expression.
</p>
<div class="example">
<pre class="example">(letrec-syntax
  ((my-or (syntax-rules ()
            ((my-or) #f)
            ((my-or e) e)
            ((my-or e1 e2 ...)
             (let ((temp e1))
               (if temp
                   temp
                   (my-or e2 ...)))))))
  (let ((x #f)
        (y 7)
        (temp 8)
        (let odd?)
        (if even?))
    (my-or x
           (let temp)
           (if y)
           y)))        &rArr;  7
</pre></div>
</dd></dl>

<dl>
<dt id="index-let_002a_002dsyntax">standard special form: <strong>let*-syntax</strong> <em>bindings expression expression &hellip;</em></dt>
<dd><p>The syntax of <code>let*-syntax</code> is the same as for <code>let-syntax</code>.
</p>
<p>The <var>expression</var>s are expanded in the syntactic environment obtained
by extending the syntactic environment of the <code>letrec-syntax</code>
expression with macros whose keywords are the <var>keyword</var>s, bound to
the specified transformers.  Each binding of a <var>keyword</var> has the
subsequent <var>bindings</var> as well as the <var>expression</var>s within its
region.  Thus
</p>
<div class="example">
<pre class="example">(let*-syntax
   ((a (syntax-rules &hellip;))
    (b (syntax-rules &hellip;)))
  &hellip;)
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example">(let-syntax ((a (syntax-rules &hellip;)))
  (let-syntax ((b (syntax-rules &hellip;)))
    &hellip;))
</pre></div>
</dd></dl>

<dl>
<dt id="index-define_002dsyntax">standard special form: <strong>define-syntax</strong> <em>keyword transformer-spec</em></dt>
<dd><p><var>Keyword</var> is an identifier, and <var>transformer-spec</var> is a macro
transformer expression.  The syntactic environment is extended by
binding the <var>keyword</var> to the specified transformer.
</p>
<p>The region of the binding introduced by <code>define-syntax</code> is the
entire block in which it appears.  However, the <var>keyword</var> may only
be used after it has been defined.
</p>
<p>MIT/GNU Scheme permits <code>define-syntax</code> to appear both at top level and
within <code>lambda</code> bodies.  The Revised^4 Report permits only
top-level uses of <code>define-syntax</code>.
</p>
<p>When compiling a program, a top-level instance of <code>define-syntax</code>
both defines the syntactic keyword and generates code that will redefine
the keyword when the program is loaded.  This means that the same syntax
can be used for defining macros that will be used during compilation
and for defining macros to be used at run time.
</p>
<p>Although macros may expand into definitions and syntax definitions in
any context that permits them, it is an error for a definition or syntax
definition to shadow a syntactic keyword whose meaning is needed to
determine whether some form in the group of forms that contains the
shadowing definition is in fact a definition, or, for internal definitions,
is needed to determine the boundary between the group and the expressions
that follow the group.  For example, the following are errors:
</p>
<div class="example">
<pre class="example">(define define 3)

(begin (define begin list))

(let-syntax
  ((foo (syntax-rules ()
          ((foo (proc args ...) body ...)
           (define proc
             (lambda (args ...)
               body ...))))))
  (let ((x 3))
    (foo (plus x y) (+ x y))
    (define foo x)
    (plus foo x)))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Pattern-Language.html" accesskey="n" rel="next">Pattern Language</a>, Previous: <a href="Macros.html" accesskey="p" rel="prev">Macros</a>, Up: <a href="Macros.html" accesskey="u" rel="up">Macros</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
