<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Environment Operations (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Environment Operations (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Environment Operations (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Environments.html" rel="up" title="Environments">
<link href="Environment-Variables.html" rel="next" title="Environment Variables">
<link href="Environments.html" rel="prev" title="Environments">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Environment-Operations"></span><div class="header">
<p>
Next: <a href="Environment-Variables.html" accesskey="n" rel="next">Environment Variables</a>, Previous: <a href="Environments.html" accesskey="p" rel="prev">Environments</a>, Up: <a href="Environments.html" accesskey="u" rel="up">Environments</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Environment-Operations-1"></span><h3 class="section">13.1 Environment Operations</h3>

<p>Environments are first-class objects in MIT/GNU Scheme.  An environment
consists of some bindings and possibly a parent environment, from which
other bindings are inherited.  The operations in this section reveal the
frame-like structure of environments by permitting you to examine the
bindings of a particular environment separately from those of its
parent.
</p>
<span id="index-variable-binding-1"></span>
<span id="index-binding_002c-variable"></span>
<span id="index-unassigned-binding"></span>
<span id="index-binding_002c-unassigned"></span>
<span id="index-condition_002dtype_003aunassigned_002dvariable-1"></span>
<p>There are several types of bindings that can occur in an environment.
The most common is the simple variable binding, which associates a value
(any Scheme object) with an identifier (a symbol).  A variable binding
can also be <em>unassigned</em>, which means that it has no value.  An
unassigned variable is bound, in that is will shadow other bindings of
the same name in ancestor environments, but a reference to that variable
will signal an error of type <code>condition-type:unassigned-variable</code>.
An unassigned variable can be <em>assigned</em> (using <code>set!</code> or
<code>environment-assign!</code>) to give it a value.
</p>
<span id="index-keyword-binding"></span>
<span id="index-syntactic-keyword-binding"></span>
<span id="index-binding_002c-syntactic-keyword"></span>
<span id="index-condition_002dtype_003amacro_002dbinding"></span>
<p>In addition to variable bindings, an environment can also have
<em>keyword bindings</em>.  A keyword binding associates a syntactic
keyword (usually a macro transformer) with an identifier.  Keyword
bindings are special in that they are considered &ldquo;bound&rdquo;, but ordinary
variable references don&rsquo;t work on them.  So an attempt to reference or
assign a keyword binding results in an error of type
<code>condition-type:macro-binding</code>.  However, keyword bindings can be
redefined using <code>define</code> or <code>environment-define</code>.
</p>
<dl>
<dt id="index-environment_003f">procedure: <strong>environment?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-environment"></span>
<p>Returns <code>#t</code> if <var>object</var> is an environment; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-environment_002dhas_002dparent_003f">procedure: <strong>environment-has-parent?</strong> <em>environment</em></dt>
<dd><p>Returns <code>#t</code> if <var>environment</var> has a parent environment;
otherwise returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-environment_002dparent">procedure: <strong>environment-parent</strong> <em>environment</em></dt>
<dd><p>Returns the parent environment of <var>environment</var>.  It is an error if
<var>environment</var> has no parent.
</p></dd></dl>

<dl>
<dt id="index-environment_002dbound_002dnames">procedure: <strong>environment-bound-names</strong> <em>environment</em></dt>
<dd><p>Returns a newly allocated list of the names (symbols) that are bound by
<var>environment</var>.  This does not include the names that are bound by
the parent environment of <var>environment</var>.  It does include names that
are unassigned or keywords in <var>environment</var>.
</p></dd></dl>

<dl>
<dt id="index-environment_002dmacro_002dnames">procedure: <strong>environment-macro-names</strong> <em>environment</em></dt>
<dd><p>Returns a newly allocated list of the names (symbols) that are bound to
syntactic keywords in <var>environment</var>.
</p></dd></dl>

<dl>
<dt id="index-environment_002dbindings">procedure: <strong>environment-bindings</strong> <em>environment</em></dt>
<dd><p>Returns a newly allocated list of the bindings of <var>environment</var>;
does not include the bindings of the parent environment.  Each element
of this list takes one of two forms: <code>(<var>symbol</var>)</code> indicates
that <var>symbol</var> is bound but unassigned, while <code>(<var>symbol</var>
<var>object</var>)</code> indicates that <var>symbol</var> is bound, and its value is
<var>object</var>.
</p></dd></dl>

<dl>
<dt id="index-environment_002dreference_002dtype">procedure: <strong>environment-reference-type</strong> <em>environment symbol</em></dt>
<dd><p>Returns a symbol describing the <em>reference type</em> of <var>symbol</var> in
<var>environment</var> or one of its ancestor environments.  The result is
one of the following:
</p>
<dl compact="compact">
<dt><code>normal</code></dt>
<dd><p>means <var>symbol</var> is a variable binding with a normal value.
</p>
</dd>
<dt><code>unassigned</code></dt>
<dd><p>means <var>symbol</var> is a variable binding with no value.
</p>
</dd>
<dt><code>macro</code></dt>
<dd><p>means <var>symbol</var> is a keyword binding.
</p>
</dd>
<dt><code>unbound</code></dt>
<dd><p>means <var>symbol</var> has no associated binding.
</p></dd>
</dl>
</dd></dl>

<dl>
<dt id="index-environment_002dbound_003f">procedure: <strong>environment-bound?</strong> <em>environment symbol</em></dt>
<dd><p>Returns <code>#t</code> if <var>symbol</var> is bound in <var>environment</var> or one
of its ancestor environments; otherwise returns <code>#f</code>.  This is
equivalent to
</p>
<div class="example">
<pre class="example">(not (eq? 'unbound
          (environment-reference-type <var>environment</var> <var>symbol</var>)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-environment_002dassigned_003f">procedure: <strong>environment-assigned?</strong> <em>environment symbol</em></dt>
<dd><p>Returns <code>#t</code> if <var>symbol</var> is bound in <var>environment</var> or one
of its ancestor environments, and has a normal value.  Returns <code>#f</code>
if it is bound but unassigned.  Signals an error if it is unbound or is
bound to a keyword.
</p></dd></dl>

<dl>
<dt id="index-environment_002dlookup">procedure: <strong>environment-lookup</strong> <em>environment symbol</em></dt>
<dd><p><var>Symbol</var> must be bound to a normal value in <var>environment</var> or one
of its ancestor environments.  Returns the value to which it is bound.
Signals an error if unbound, unassigned, or a keyword.
</p></dd></dl>

<dl>
<dt id="index-environment_002dlookup_002dmacro">procedure: <strong>environment-lookup-macro</strong> <em>environment symbol</em></dt>
<dd><p>If <var>symbol</var> is a keyword binding in <var>environment</var> or one of its
ancestor environments, returns the value of the binding.  Otherwise,
returns <code>#f</code>.  Does not signal any errors other than argument-type
errors.
</p></dd></dl>

<dl>
<dt id="index-environment_002dassignable_003f">procedure: <strong>environment-assignable?</strong> <em>environment symbol</em></dt>
<dd><p><var>Symbol</var> must be bound in <var>environment</var> or one of its ancestor
environments.  Returns <code>#t</code> if the binding may be modified by side
effect.
</p></dd></dl>

<dl>
<dt id="index-environment_002dassign_0021">procedure: <strong>environment-assign!</strong> <em>environment symbol object</em></dt>
<dd><p><var>Symbol</var> must be bound in <var>environment</var> or one of its ancestor
environments, and must be assignable.  Modifies the binding to have
<var>object</var> as its value, and returns an unspecified result.
</p></dd></dl>

<dl>
<dt id="index-environment_002ddefinable_003f">procedure: <strong>environment-definable?</strong> <em>environment symbol</em></dt>
<dd><p>Returns <code>#t</code> if <var>symbol</var> is definable in <var>environment</var>, and
<code>#f</code> otherwise.  At present, this is false for environments
generated by application of compiled procedures, and true for all other
environments.
</p></dd></dl>

<dl>
<dt id="index-environment_002ddefine">procedure: <strong>environment-define</strong> <em>environment symbol object</em></dt>
<dd><p>Defines <var>symbol</var> to be bound to <var>object</var> in <var>environment</var>,
and returns an unspecified value.  Signals an error if <var>symbol</var>
isn&rsquo;t definable in <var>environment</var>.
</p></dd></dl>

<dl>
<dt id="index-environment_002ddefine_002dmacro">procedure: <strong>environment-define-macro</strong> <em>environment symbol transformer</em></dt>
<dd><p>Defines <var>symbol</var> to be a keyword bound to <var>transformer</var> in
<var>environment</var>, and returns an unspecified value.  Signals an error
if <var>symbol</var> isn&rsquo;t definable in <var>environment</var>.  The type of
<var>transformer</var> is defined by the syntax engine and is not checked by
this procedure.  If the type is incorrect this will subsequently signal
an error during syntax expansion.
</p></dd></dl>

<dl>
<dt id="index-eval">procedure: <strong>eval</strong> <em>expression environment</em></dt>
<dd><span id="index-s_002dexpression"></span>
<span id="index-evaluation_002c-of-s_002dexpression"></span>
<p>Evaluates <var>expression</var>, a list-structure representation (sometimes
called s-expression representation) of a Scheme expression, in
<var>environment</var>.  You rarely need <code>eval</code> in ordinary programs; it
is useful mostly for evaluating expressions that have been created &ldquo;on
the fly&rdquo; by a program.  <code>eval</code> is relatively expensive because it
must convert <var>expression</var> to an internal form before it is executed.
</p>
<div class="example">
<pre class="example">(define foo (list '+ 1 2))
(eval foo (the-environment))            &rArr;  3
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Environment-Variables.html" accesskey="n" rel="next">Environment Variables</a>, Previous: <a href="Environments.html" accesskey="p" rel="prev">Environments</a>, Up: <a href="Environments.html" accesskey="u" rel="up">Environments</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
