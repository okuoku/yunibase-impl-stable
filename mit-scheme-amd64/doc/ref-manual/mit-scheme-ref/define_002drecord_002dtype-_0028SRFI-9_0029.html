<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>define-record-type (SRFI 9) (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="define-record-type (SRFI 9) (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="define-record-type (SRFI 9) (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="SRFI-syntax.html" rel="up" title="SRFI syntax">
<link href="Equivalence-Predicates.html" rel="next" title="Equivalence Predicates">
<link href="and_002dlet_002a-_0028SRFI-2_0029.html" rel="prev" title="and-let* (SRFI 2)">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="define_002drecord_002dtype-_0028SRFI-9_0029"></span><div class="header">
<p>
Previous: <a href="and_002dlet_002a-_0028SRFI-2_0029.html" accesskey="p" rel="prev">and-let* (SRFI 2)</a>, Up: <a href="SRFI-syntax.html" accesskey="u" rel="up">SRFI syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="define_002drecord_002dtype-_0028SRFI-9_0029-1"></span><h4 class="subsection">2.12.4 define-record-type (SRFI 9)</h4>

<span id="index-SRFI-9"></span>
<p>The &lsquo;<samp>define-record-type</samp>&rsquo; syntax described in <a href="https://srfi.schemers.org/srfi-9/srfi-9.html"><acronym>SRFI</acronym> 9</a> is a slight
simplification of one written for Scheme 48 by Jonathan Rees.  Unlike
many record-defining special forms, it does not create any new
identifiers.  Instead, the names of the record type, predicate,
constructor, and so on are all listed explicitly in the source.  This
has the following advantages:
</p>
<ul>
<li> It can be defined using a simple macro in Scheme implementations that
provide a procedural interface for creating record types.

</li><li> It does not restrict users to a particular naming convention.

</li><li> Tools like <code>grep</code> and the GNU Emacs tag facility will see the
defining occurrence of each identifier.
</li></ul>

<dl>
<dt id="index-define_002drecord_002dtype">extended standard special form: <strong>define-record-type</strong> <em>type-name (constructor-name field-tag &hellip;) predicate-name field-spec &hellip;</em></dt>
<dd><p><var>Type-name</var>, <var>contructor-name</var>, <var>field-tag</var>, and
<var>predicate-name</var> are identifiers.  <var>Field-spec</var> has one of these
two forms:
</p>
<div class="example">
<pre class="example">(<var>field-tag</var> <var>accessor-name</var>)
(<var>field-tag</var> <var>accessor-name</var> <var>modifier-name</var>)
</pre></div>

<p>where <var>field-tag</var>, <var>accessor-name</var>, and <var>modifier-name</var> are
each identifiers.
</p>
<p><code>define-record-type</code> is generative: each use creates a new record
type that is distinct from all existing types, including other record
types and Scheme&rsquo;s predefined types.  Record-type definitions may only
occur at top-level (there are two possible semantics for &ldquo;internal&rdquo;
record-type definitions, generative and nongenerative, and no consensus
as to which is better).
</p>
<p>An instance of <code>define-record-type</code> is equivalent to the following
definitions:
</p>
<ul>
<li> <var>Type-name</var> is bound to a representation of the record type itself.
Operations on record types, such as defining print methods, reflection,
etc. are left to other SRFIs.

</li><li> <var>constructor-name</var> is bound to a procedure that takes as many
arguments as there are <var>field-tag</var>s in the (<var>constructor-name</var>
&hellip;) subform and returns a new <var>type-name</var> record.  Fields whose
tags are listed with <var>constructor-name</var> have the corresponding
argument as their initial value.  The initial values of all other fields
are unspecified.

</li><li> <var>predicate-name</var> is a predicate that returns <code>#t</code> when given a
value returned by <var>constructor-name</var> and <code>#f</code> for everything
else.

</li><li> Each <var>accessor-name</var> is a procedure that takes a record of type
<var>type-name</var> and returns the current value of the corresponding
field.  It is an error to pass an accessor a value which is not a record
of the appropriate type.

</li><li> Each <var>modifier-name</var> is a procedure that takes a record of type
<var>type-name</var> and a value which becomes the new value of the
corresponding field; an unspecified value is returned.  It is an error
to pass a modifier a first argument which is not a record of the
appropriate type.
</li></ul>

<p>Assigning the value of any of these identifiers has no effect on the
behavior of any of their original values.
</p></dd></dl>

<p>The following
</p>
<div class="example">
<pre class="example">(define-record-type :pare
  (kons x y)
  pare?
  (x kar set-kar!)
  (y kdr))
</pre></div>

<p>defines &lsquo;<samp>kons</samp>&rsquo; to be a constructor, &lsquo;<samp>kar</samp>&rsquo; and &lsquo;<samp>kdr</samp>&rsquo; to be
accessors, &lsquo;<samp>set-kar!</samp>&rsquo; to be a modifier, and &lsquo;<samp>pare?</samp>&rsquo; to be a
predicate for objects of type &lsquo;<samp>:pare</samp>&rsquo;.
</p>
<div class="example">
<pre class="example">(pare? (kons 1 2))        &rArr; #t
(pare? (cons 1 2))        &rArr; #f
(kar (kons 1 2))          &rArr; 1
(kdr (kons 1 2))          &rArr; 2
(let ((k (kons 1 2)))
  (set-kar! k 3)
  (kar k))                &rArr; 3
</pre></div>
<hr>
<div class="header">
<p>
Previous: <a href="and_002dlet_002a-_0028SRFI-2_0029.html" accesskey="p" rel="prev">and-let* (SRFI 2)</a>, Up: <a href="SRFI-syntax.html" accesskey="u" rel="up">SRFI syntax</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
