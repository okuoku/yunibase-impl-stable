<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Parser Buffers (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Parser Buffers (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Parser Buffers (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Parser-Language.html" rel="next" title="Parser Language">
<link href="Textual-Output-Port-Operations.html" rel="prev" title="Textual Output Port Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Parser-Buffers"></span><div class="header">
<p>
Next: <a href="Parser-Language.html" accesskey="n" rel="next">Parser Language</a>, Previous: <a href="Textual-Port-Primitives.html" accesskey="p" rel="prev">Textual Port Primitives</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Parser-Buffers-1"></span><h3 class="section">14.13 Parser Buffers</h3>

<span id="index-Parser-buffer"></span>
<p>The <em>parser buffer</em> mechanism facilitates construction of parsers
for complex grammars.  It does this by providing an input stream with
unbounded buffering and backtracking.  The amount of buffering is
under program control.  The stream can backtrack to any position in
the buffer.
</p>
<span id="index-Parser_002dbuffer-pointer"></span>
<p>The mechanism defines two data types: the <em>parser buffer</em> and the
<em>parser-buffer pointer</em>.  A parser buffer is like an input port
with buffering and backtracking.  A parser-buffer pointer is a pointer
into the stream of characters provided by a parser buffer.
</p>
<p>Note that all of the procedures defined here consider a parser buffer
to contain a stream of Unicode characters.
</p>
<p>There are several constructors for parser buffers:
</p>
<dl>
<dt id="index-textual_002dinput_002dport_002d_003eparser_002dbuffer">procedure: <strong>textual-input-port-&gt;parser-buffer</strong> <em>textual-input-port</em></dt>
<dt id="index-input_002dport_002d_003eparser_002dbuffer">obsolete procedure: <strong>input-port-&gt;parser-buffer</strong> <em>textual-input-port</em></dt>
<dd><p>Returns a parser buffer that buffers characters read from
<var>textual-input-port</var>.
</p></dd></dl>

<dl>
<dt id="index-substring_002d_003eparser_002dbuffer">procedure: <strong>substring-&gt;parser-buffer</strong> <em>string start end</em></dt>
<dd><p>Returns a parser buffer that buffers the characters in the argument
substring.  This is equivalent to creating a string input port and
calling <code>textual-input-port-&gt;parser-buffer</code>, but it runs faster
and uses less memory.
</p></dd></dl>

<dl>
<dt id="index-string_002d_003eparser_002dbuffer">procedure: <strong>string-&gt;parser-buffer</strong> <em>string</em></dt>
<dd><p>Like <code>substring-&gt;parser-buffer</code> but buffers the entire string.
</p></dd></dl>

<dl>
<dt id="index-source_002d_003eparser_002dbuffer">procedure: <strong>source-&gt;parser-buffer</strong> <em>source</em></dt>
<dd><p>Returns a parser buffer that buffers the characters returned by
calling <var>source</var>.  <var>Source</var> is a procedure of three arguments:
a string, a start index, and an end index (in other words, a substring
specifier).  Each time <var>source</var> is called, it writes some
characters in the substring, and returns the number of characters
written.  When there are no more characters available, it returns
zero.  It must not return zero in any other circumstance.
</p></dd></dl>

<p>Parser buffers and parser-buffer pointers may be distinguished from
other objects:
</p>
<dl>
<dt id="index-parser_002dbuffer_003f">procedure: <strong>parser-buffer?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is a parser buffer, otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-parser_002dbuffer_002dpointer_003f">procedure: <strong>parser-buffer-pointer?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is a parser-buffer pointer,
otherwise returns <code>#f</code>.
</p></dd></dl>

<p>Characters can be read from a parser buffer much as they can be read
from an input port.  The parser buffer maintains an internal pointer
indicating its current position in the input stream.  Additionally,
the buffer remembers all characters that were previously read, and can
look at characters arbitrarily far ahead in the stream.  It is this
buffering capability that facilitates complex matching and
backtracking.
</p>
<dl>
<dt id="index-read_002dparser_002dbuffer_002dchar">procedure: <strong>read-parser-buffer-char</strong> <em>buffer</em></dt>
<dd><p>Returns the next character in <var>buffer</var>, advancing the internal
pointer past that character.  If there are no more characters
available, returns <code>#f</code> and leaves the internal pointer
unchanged.
</p></dd></dl>

<dl>
<dt id="index-peek_002dparser_002dbuffer_002dchar">procedure: <strong>peek-parser-buffer-char</strong> <em>buffer</em></dt>
<dd><p>Returns the next character in <var>buffer</var>, or <code>#f</code> if no
characters are available.  Leaves the internal pointer unchanged.
</p></dd></dl>

<dl>
<dt id="index-parser_002dbuffer_002dref">procedure: <strong>parser-buffer-ref</strong> <em>buffer index</em></dt>
<dd><p>Returns a character in <var>buffer</var>.  <var>Index</var> is a non-negative
integer specifying the character to be returned.  If <var>index</var> is
zero, returns the next available character; if it is one, returns the
character after that, and so on.  If <var>index</var> specifies a position
after the last character in <var>buffer</var>, returns <code>#f</code>.  Leaves
the internal pointer unchanged.
</p></dd></dl>

<p>The internal pointer of a parser buffer can be read or written:
</p>
<dl>
<dt id="index-get_002dparser_002dbuffer_002dpointer">procedure: <strong>get-parser-buffer-pointer</strong> <em>buffer</em></dt>
<dd><p>Returns a parser-buffer pointer object corresponding to the internal
pointer of <var>buffer</var>.
</p></dd></dl>

<dl>
<dt id="index-set_002dparser_002dbuffer_002dpointer_0021">procedure: <strong>set-parser-buffer-pointer!</strong> <em>buffer pointer</em></dt>
<dd><p>Sets the internal pointer of <var>buffer</var> to the position specified by
<var>pointer</var>.  <var>Pointer</var> must have been returned from a previous
call of <code>get-parser-buffer-pointer</code> on <var>buffer</var>.
Additionally, if some of <var>buffer</var>&rsquo;s characters have been discarded
by <code>discard-parser-buffer-head!</code>, <var>pointer</var> must be outside
the range that was discarded.
</p></dd></dl>

<dl>
<dt id="index-get_002dparser_002dbuffer_002dtail">procedure: <strong>get-parser-buffer-tail</strong> <em>buffer pointer</em></dt>
<dd><p>Returns a newly-allocated string consisting of all of the characters
in <var>buffer</var> that fall between <var>pointer</var> and <var>buffer</var>&rsquo;s
internal pointer.  <var>Pointer</var> must have been returned from a
previous call of <code>get-parser-buffer-pointer</code> on <var>buffer</var>.
Additionally, if some of <var>buffer</var>&rsquo;s characters have been discarded
by <code>discard-parser-buffer-head!</code>, <var>pointer</var> must be outside
the range that was discarded.
</p></dd></dl>

<dl>
<dt id="index-discard_002dparser_002dbuffer_002dhead_0021">procedure: <strong>discard-parser-buffer-head!</strong> <em>buffer</em></dt>
<dd><p>Discards all characters in <var>buffer</var> that have already been read;
in other words, all characters prior to the internal pointer.  After
this operation has completed, it is no longer possible to move the
internal pointer backwards past the current position by calling
<code>set-parser-buffer-pointer!</code>.
</p></dd></dl>

<p>The next rather large set of procedures does conditional matching
against the contents of a parser buffer.  All matching is performed
relative to the buffer&rsquo;s internal pointer, so the first character to
be matched against is the next character that would be returned by
<code>peek-parser-buffer-char</code>.  The returned value is always
<code>#t</code> for a successful match, and <code>#f</code> otherwise.  For
procedures whose names do not end in &lsquo;<samp>-no-advance</samp>&rsquo;, a successful
match also moves the internal pointer of the buffer forward to the end
of the matched text; otherwise the internal pointer is unchanged.
</p>
<dl>
<dt id="index-match_002dparser_002dbuffer_002dchar">procedure: <strong>match-parser-buffer-char</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dchar_002dci">procedure: <strong>match-parser-buffer-char-ci</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dnot_002dchar">procedure: <strong>match-parser-buffer-not-char</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dnot_002dchar_002dci">procedure: <strong>match-parser-buffer-not-char-ci</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dchar_002dno_002dadvance">procedure: <strong>match-parser-buffer-char-no-advance</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dchar_002dci_002dno_002dadvance">procedure: <strong>match-parser-buffer-char-ci-no-advance</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dnot_002dchar_002dno_002dadvance">procedure: <strong>match-parser-buffer-not-char-no-advance</strong> <em>buffer char</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dnot_002dchar_002dci_002dno_002dadvance">procedure: <strong>match-parser-buffer-not-char-ci-no-advance</strong> <em>buffer char</em></dt>
<dd><p>Each of these procedures compares a single character in <var>buffer</var>
to <var>char</var>.  The basic comparison <code>match-parser-buffer-char</code>
compares the character to <var>char</var> using <code>char=?</code>.  The
procedures whose names contain the &lsquo;<samp>-ci</samp>&rsquo; modifier do
case-insensitive comparison (i.e. they use <code>char-ci=?</code>).  The
procedures whose names contain the &lsquo;<samp>not-</samp>&rsquo; modifier are successful
if the character <em>doesn&rsquo;t</em> match <var>char</var>.
</p></dd></dl>

<dl>
<dt id="index-match_002dparser_002dbuffer_002dchar_002din_002dset">procedure: <strong>match-parser-buffer-char-in-set</strong> <em>buffer char-set</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dchar_002din_002dset_002dno_002dadvance">procedure: <strong>match-parser-buffer-char-in-set-no-advance</strong> <em>buffer char-set</em></dt>
<dd><p>These procedures compare the next character in <var>buffer</var> against
<var>char-set</var> using <code>char-in-set?</code>.
</p></dd></dl>

<dl>
<dt id="index-match_002dparser_002dbuffer_002dstring">procedure: <strong>match-parser-buffer-string</strong> <em>buffer string</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dstring_002dci">procedure: <strong>match-parser-buffer-string-ci</strong> <em>buffer string</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dstring_002dno_002dadvance">procedure: <strong>match-parser-buffer-string-no-advance</strong> <em>buffer string</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dstring_002dci_002dno_002dadvance">procedure: <strong>match-parser-buffer-string-ci-no-advance</strong> <em>buffer string</em></dt>
<dd><p>These procedures match <var>string</var> against <var>buffer</var>&rsquo;s contents.
The &lsquo;<samp>-ci</samp>&rsquo; procedures do case-insensitive matching.
</p></dd></dl>

<dl>
<dt id="index-match_002dparser_002dbuffer_002dsubstring">procedure: <strong>match-parser-buffer-substring</strong> <em>buffer string start end</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dsubstring_002dci">procedure: <strong>match-parser-buffer-substring-ci</strong> <em>buffer string start end</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dsubstring_002dno_002dadvance">procedure: <strong>match-parser-buffer-substring-no-advance</strong> <em>buffer string   start end</em></dt>
<dt id="index-match_002dparser_002dbuffer_002dsubstring_002dci_002dno_002dadvance">procedure: <strong>match-parser-buffer-substring-ci-no-advance</strong> <em>buffer string   start end</em></dt>
<dd><p>These procedures match the specified substring against <var>buffer</var>&rsquo;s
contents.  The &lsquo;<samp>-ci</samp>&rsquo; procedures do case-insensitive matching.
</p></dd></dl>

<p>The remaining procedures provide information that can be used to
identify locations in a parser buffer&rsquo;s stream.
</p>
<dl>
<dt id="index-parser_002dbuffer_002dposition_002dstring">procedure: <strong>parser-buffer-position-string</strong> <em>pointer</em></dt>
<dd><p>Returns a string describing the location of <var>pointer</var> in terms of
its character and line indexes.  This resulting string is meant to be
presented to an end user in order to direct their attention to a
feature in the input stream.  In this string, the indexes are
presented as one-based numbers.
</p>
<p><var>Pointer</var> may alternatively be a parser buffer, in which case it
is equivalent to having specified the buffer&rsquo;s internal pointer.
</p></dd></dl>

<dl>
<dt id="index-parser_002dbuffer_002dpointer_002dindex">procedure: <strong>parser-buffer-pointer-index</strong> <em>pointer</em></dt>
<dt id="index-parser_002dbuffer_002dpointer_002dline">procedure: <strong>parser-buffer-pointer-line</strong> <em>pointer</em></dt>
<dd><p>Returns the character or line index, respectively, of <var>pointer</var>.
Both indexes are zero-based.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Parser-Language.html" accesskey="n" rel="next">Parser Language</a>, Previous: <a href="Textual-Port-Primitives.html" accesskey="p" rel="prev">Textual Port Primitives</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
