<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>SC Identifiers (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="SC Identifiers (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="SC Identifiers (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Syntactic-Closures.html" rel="up" title="Syntactic Closures">
<link href="Explicit-Renaming.html" rel="next" title="Explicit Renaming">
<link href="SC-Transformer-Definition.html" rel="prev" title="SC Transformer Definition">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="SC-Identifiers"></span><div class="header">
<p>
Previous: <a href="SC-Transformer-Definition.html" accesskey="p" rel="prev">SC Transformer Definition</a>, Up: <a href="Syntactic-Closures.html" accesskey="u" rel="up">Syntactic Closures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Identifiers-2"></span><h4 class="subsubsection">2.11.3.3 Identifiers</h4>

<p>This section describes the procedures that create and manipulate
identifiers.  The identifier data type extends the syntactic closures
facility to be compatible with the high-level <code>syntax-rules</code>
facility.
</p>
<span id="index-alias-1"></span>
<p>As discussed earlier, an identifier is either a symbol or an
<em>alias</em>.  An alias is implemented as a syntactic closure whose
<var>form</var> is an identifier:
</p>
<div class="example">
<pre class="example">(make-syntactic-closure env '() 'a) &rArr; <span class="roman">an alias</span>
</pre></div>

<p>Aliases are implemented as syntactic closures because they behave just
like syntactic closures most of the time.  The difference is that an
alias may be bound to a new value (for example by <code>lambda</code> or
<code>let-syntax</code>); other syntactic closures may not be used this way.
If an alias is bound, then within the scope of that binding it is looked
up in the syntactic environment just like any other identifier.
</p>
<p>Aliases are used in the implementation of the high-level facility
<code>syntax-rules</code>.  A macro transformer created by <code>syntax-rules</code>
uses a template to generate its output form, substituting subforms of
the input form into the template.  In a syntactic closures
implementation, all of the symbols in the template are replaced by
aliases closed in the transformer environment, while the output form
itself is closed in the usage environment.  This guarantees that the
macro transformation is hygienic, without requiring the transformer to
know the syntactic roles of the substituted input subforms.
</p>
<dl>
<dt id="index-identifier_003f">procedure: <strong>identifier?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is an identifier, otherwise returns
<code>#f</code>.  Examples:
</p>
<div class="example">
<pre class="example">(identifier? 'a)        &rArr; #t
(identifier? (make-syntactic-closure env '() 'a))
                        &rArr; #t

(identifier? &quot;a&quot;)       &rArr; #f
(identifier? #\a)       &rArr; #f
(identifier? 97)        &rArr; #f
(identifier? #f)        &rArr; #f
(identifier? '(a))      &rArr; #f
(identifier? '#(a))     &rArr; #f
</pre></div>
</dd></dl>

<p>The predicate <code>eq?</code> is used to determine if two identifers are
&ldquo;the same&rdquo;.  Thus <code>eq?</code> can be used to compare identifiers
exactly as it would be used to compare symbols.  Often, though, it is
useful to know whether two identifiers &ldquo;mean the same thing&rdquo;.  For
example, the <code>cond</code> macro uses the symbol <code>else</code> to identify
the final clause in the conditional.  A macro transformer for
<code>cond</code> cannot just look for the symbol <code>else</code>, because the
<code>cond</code> form might be the output of another macro transformer that
replaced the symbol <code>else</code> with an alias.  Instead the transformer
must look for an identifier that &ldquo;means the same thing&rdquo; in the usage
environment as the symbol <code>else</code> means in the transformer
environment.
</p>
<dl>
<dt id="index-identifier_003d_003f">procedure: <strong>identifier=?</strong> <em>environment1 identifier1 environment2 identifier2</em></dt>
<dd><p><var>Environment1</var> and <var>environment2</var> must be syntactic
environments, and <var>identifier1</var> and <var>identifier2</var> must be
identifiers.  <code>identifier=?</code> returns <code>#t</code> if the meaning of
<var>identifier1</var> in <var>environment1</var> is the same as that of
<var>identifier2</var> in <var>environment2</var>, otherwise it returns <code>#f</code>.
Examples:
</p>
<div class="example">
<pre class="example">(let-syntax
    ((foo
      (sc-macro-transformer
       (lambda (form env)
         (capture-syntactic-environment
          (lambda (transformer-env)
            (identifier=? transformer-env 'x env 'x)))))))
  (list (foo)
        (let ((x 3))
          (foo))))
                        &rArr; (#t #f)
</pre><pre class="example">

</pre><pre class="example">(let-syntax ((bar foo))
  (let-syntax
      ((foo
        (sc-macro-transformer
         (lambda (form env)
           (capture-syntactic-environment
            (lambda (transformer-env)
              (identifier=? transformer-env 'foo
                            env (cadr form))))))))
    (list (foo foo)
          (foo bar))))
                        &rArr; (#f #t)
</pre></div>
</dd></dl>

<p>Sometimes it is useful to be able to introduce a new identifier that is
guaranteed to be different from any existing identifier, similarly to
the way that <code>generate-uninterned-symbol</code> is used. 
</p>
<dl>
<dt id="index-make_002dsynthetic_002didentifier">procedure: <strong>make-synthetic-identifier</strong> <em>identifier</em></dt>
<dd><p>Creates and returns and new synthetic identifier (alias) that is
guaranteed to be different from all existing identifiers.
<var>Identifier</var> is any existing identifier, which is used in deriving
the name of the new identifier.
</p></dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="SC-Transformer-Definition.html" accesskey="p" rel="prev">SC Transformer Definition</a>, Up: <a href="Syntactic-Closures.html" accesskey="u" rel="up">Syntactic Closures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
