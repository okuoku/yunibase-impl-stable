<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Windows Types (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Windows Types (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Windows Types (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Foreign-function-interface.html" rel="up" title="Foreign function interface">
<link href="Windows-Foreign-Procedures.html" rel="next" title="Windows Foreign Procedures">
<link href="Foreign-function-interface.html" rel="prev" title="Foreign function interface">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Windows-Types"></span><div class="header">
<p>
Next: <a href="Windows-Foreign-Procedures.html" accesskey="n" rel="next">Windows Foreign Procedures</a>, Previous: <a href="Foreign-function-interface.html" accesskey="p" rel="prev">Foreign function interface</a>, Up: <a href="Foreign-function-interface.html" accesskey="u" rel="up">Foreign function interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Windows-Types-1"></span><h4 class="subsection">18.2.1 Windows Types</h4>

<span id="index-Windows-types"></span>
<span id="index-foreign-type-declarations"></span>
<span id="index-types_002c-Windows"></span>
<span id="index-defining-foreign-types"></span>
<p>Foreign types are designed to represent a correspondence between a
Scheme data type that is used to represent an object within the Scheme
world and a C data type that represents the data object in the C world.
Thus we cannot manipulate true C objects in Scheme, nor can we
manipulate Scheme objects in C.
</p>
<p>Each foreign type has four aspects that together ensure that the
correspondence between the Scheme and C objects is maintained.  These
aspects are all encoded as procedures that either check for validity or
convert between representations.  Thus a foreign type is not a
declarative type so much as a procedural description of how to pass the
type.  The underlying foreign procedure call mechanism can pass integers
and vector-like Scheme objects, and returns integer values.  All other
objects must be translated into integers or some other basic type, and
must be recovered from integers.
</p>
<p>The aspects are:
</p>
<dl compact="compact">
<dt><var>check</var></dt>
<dd><p>A predicate that returns <code>#t</code> if the argument is of an acceptable
Scheme type, otherwise returns <code>#f</code>.
The <var>check</var> procedure is used for type-checking.
</p>
</dd>
<dt><var>convert</var></dt>
<dd><p>A procedure of one argument which returns a Scheme object of one of the
basic types.
It is used to convert an object into a &lsquo;simpler&rsquo; object that will
eventually be converted into a C object.
The legal simpler objects are integers and strings.
</p>
</dd>
<dt><var>return-convert</var></dt>
<dd><p>A procedure of one argument that, given an integer, returns a Scheme
object of a type satisfying <var>check</var>.
Its purpose is to convert the result returned by the foreign procedure
into a Scheme value.
</p>
</dd>
<dt><var>revert</var></dt>
<dd><p>Some C procedures modify one or more of their arguments.  These
arguments are passed by reference, i.e. as a pointer to their address.
Since a Scheme object might have a different memory layout and storage
conventions, it must be passed by copy-in and copy-out rather than by
reference.
<var>Revert</var> is a procedure of two parameters, the original object
passed and the result of <var>convert</var> on that object.
<var>Revert</var> may then inspect the converted object and copy back the
changes to the original.
</p>
</dd>
</dl>

<dl>
<dt id="index-define_002dwindows_002dtype">special form: <strong>define-windows-type</strong> <em>name check convert return revert</em></dt>
<dt id="index-define_002dsimilar_002dwindows_002dtype">special form: <strong>define-similar-windows-type</strong> <em>name model [check [convert [return [revert]]]]</em></dt>
<dd><span id="index-defining-foreign-types-1"></span>
<p>Both forms define a windows type.
The first form defines a type in terms of its aspects as described
above.
The second defines the type as being like another type, except for
certain aspects, which are redefined.
<var>Name</var> is the name of the type.
<var>Model</var> is the name of a type.
<var>Check</var>, <var>convert</var>, <var>return</var> and <var>revert</var> are
procedures or the value <code>#f</code>.
A <code>#f</code> means use the default value, which in the second form means
use the definition provided for <var>model</var>.
The defaults are
</p>
<dl compact="compact">
<dt><var>check</var></dt>
<dd><p><code>(lambda (x) #t)</code>, i.e. unchecked.
</p></dd>
<dt><var>convert</var></dt>
<dd><p><code>(lambda (x) x)</code>, i.e. no translation performed.
</p></dd>
<dt><var>return</var></dt>
<dd><p><code>(lambda (x) x)</code>, i.e. no translation performed.
</p></dd>
<dt><var>revert</var></dt>
<dd><p><code>(lambda (x y) unspecific)</code>, i.e. no update performed
</p></dd>
</dl>

<p>The <code>unchecked</code> windows type (see below) is defined as:
</p>
<div class="example">
<pre class="example">(define-windows-type unchecked #f #f #f #f)
</pre></div>

<p>Windows types are <em>not</em> first class values, so they cannot be
stored in variables or defined using <code>define</code>:
</p>
<div class="example">
<pre class="example">(define my-type unchecked)            error&rarr;  Unbound variable
(define-similar-windows-type my-type unchecked)
                                      <span class="roman">;; the correct way</span>
</pre></div>

<p>Scheme characters must be converted to integers.  This is accomplished
as follows:
</p>
<div class="example">
<pre class="example">(define-windows-type char
   char?          <span class="roman">; check</span>
   char-&gt;integer  <span class="roman">; convert</span>
   integer-&gt;char  <span class="roman">; convert return value</span>
   #f             <span class="roman">; cannot be passed by reference</span>
)
</pre></div>
</dd></dl>

<dl>
<dt id="index-unchecked">windows type: <strong>unchecked</strong></dt>
<dd><p>The type which is not checked and undergoes only the basic conversion
from a Scheme integer to a C integer or from a Scheme string to a C
pointer to the first byte of the string.
Returned <code>unchecked</code> values are returned as integers.
</p></dd></dl>

<dl>
<dt id="index-bool">windows type: <strong>bool</strong></dt>
<dd><p>Scheme booleans are analogous to C integers <code>0</code> and <code>1</code>.
Windows type <code>bool</code> have been defined as:
</p>
<div class="example">
<pre class="example">(define-windows-type bool
   boolean?
   (lambda (x) (if x 1 0))
   (lambda (x) (if (eq? x 0) #f #t))
   #f)
</pre></div>
</dd></dl>

<dl>
<dt id="index-char-1">windows type: <strong>char</strong></dt>
<dd><p>Scheme characters are converted into C objects of type <code>char</code>,
which are indistinguishable from small integers.
</p></dd></dl>

<dl>
<dt id="index-int">windows type: <strong>int</strong></dt>
<dt id="index-uint">windows type: <strong>uint</strong></dt>
<dt id="index-long">windows type: <strong>long</strong></dt>
<dt id="index-ulong">windows type: <strong>ulong</strong></dt>
<dt id="index-short">windows type: <strong>short</strong></dt>
<dt id="index-ushort">windows type: <strong>ushort</strong></dt>
<dt id="index-word">windows type: <strong>word</strong></dt>
<dt id="index-byte">windows type: <strong>byte</strong></dt>
<dd><p>Various integer types that are passed without conversion.
</p></dd></dl>

<dl>
<dt id="index-string-2">windows type: <strong>string</strong></dt>
<dd><p>A string that is passed as a C pointer of type <code>char*</code> to the first
character in the string.
</p></dd></dl>

<dl>
<dt id="index-char_002a">windows type: <strong>char*</strong></dt>
<dd><p>A string or <code>#f</code>.  The string is passed as a pointer to characters.
The string is correctly null-terminated.  <code>#f</code> is passed as the null
pointer.  This is an example where there is a more complex mapping
between C objects and Scheme objects.  C&rsquo;s <code>char*</code> type is
represented as one of two Scheme types depending on its value.  This
allows us us to distinguish between the C string (pointer) that points
to the empty sequence of characters and the null pointer (which doesnt
point anywhere).
</p></dd></dl>

<dl>
<dt id="index-handle">windows type: <strong>handle</strong></dt>
<dt id="index-hbitmap">windows type: <strong>hbitmap</strong></dt>
<dt id="index-hbrush">windows type: <strong>hbrush</strong></dt>
<dt id="index-hcursor">windows type: <strong>hcursor</strong></dt>
<dt id="index-hdc">windows type: <strong>hdc</strong></dt>
<dt id="index-hicon">windows type: <strong>hicon</strong></dt>
<dt id="index-hinstance">windows type: <strong>hinstance</strong></dt>
<dt id="index-hmenu">windows type: <strong>hmenu</strong></dt>
<dt id="index-hpalette">windows type: <strong>hpalette</strong></dt>
<dt id="index-hpen">windows type: <strong>hpen</strong></dt>
<dt id="index-hrgn">windows type: <strong>hrgn</strong></dt>
<dt id="index-hwnd">windows type: <strong>hwnd</strong></dt>
<dd><p>Various kinds of Win32 handle.  These names correspond to the same, but
all uppercase, names in the Windows C language header files.  Win32 API
calls are the source of values of this type and the values are
meaningless except as arguments to other Win32 API calls.  Currently
these values are represented as integers but we expect that Win32
handles will in future be represented by allocated Scheme objects
(e.g. records) that will allow predicates (e.g. <code>hmenu?</code>) and
sensible interlocking with the garbage collector to free the programmer
of the current tedious allocation and deallocation of handles.
</p></dd></dl>

<dl>
<dt id="index-resource_002did">windows type: <strong>resource-id</strong></dt>
<dd><p>A Windows resource identifier is either a small integer or a string.
In C, this distinction is possible because pointers look like
larger integers, so a machine word representing a small integer can be
distinguished from a machine word that is a pointer to the text of the
name of the resource.
</p></dd></dl>


<hr>
<div class="header">
<p>
Next: <a href="Windows-Foreign-Procedures.html" accesskey="n" rel="next">Windows Foreign Procedures</a>, Previous: <a href="Foreign-function-interface.html" accesskey="p" rel="prev">Foreign function interface</a>, Up: <a href="Foreign-function-interface.html" accesskey="u" rel="up">Foreign function interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
