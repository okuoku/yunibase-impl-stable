<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Conditionals (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Conditionals (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Conditionals (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Sequencing.html" rel="next" title="Sequencing">
<link href="Quoting.html" rel="prev" title="Quoting">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Conditionals"></span><div class="header">
<p>
Next: <a href="Sequencing.html" accesskey="n" rel="next">Sequencing</a>, Previous: <a href="Quoting.html" accesskey="p" rel="prev">Quoting</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Conditionals-1"></span><h3 class="section">2.7 Conditionals</h3>

<span id="index-expression_002c-conditional-_0028defn_0029"></span>
<span id="index-conditional-expression-_0028defn_0029"></span>
<span id="index-true_002c-in-conditional-expression-_0028defn_0029"></span>
<span id="index-false_002c-in-conditional-expression-_0028defn_0029"></span>
<span id="index-_0023f-1"></span>
<span id="index-_0023t-1"></span>
<p>The behavior of the <em>conditional expressions</em> is determined by
whether objects are true or false.  The conditional expressions count
only <code>#f</code> as false.  They count everything else, including
<code>#t</code>, pairs, symbols, numbers, strings, vectors, and procedures as
true (but see <a href="True-and-False.html">True and False</a>).
</p>
<p>In the descriptions that follow, we say that an object has &ldquo;a true
value&rdquo; or &ldquo;is true&rdquo; when the conditional expressions treat it as
true, and we say that an object has &ldquo;a false value&rdquo; or &ldquo;is false&rdquo;
when the conditional expressions treat it as false.
</p>
<dl>
<dt id="index-if">standard special form: <strong>if</strong> <em>predicate consequent [alternative]</em></dt>
<dd><p><var>Predicate</var>, <var>consequent</var>, and <var>alternative</var> are
expressions.  An <code>if</code> expression is evaluated as follows: first,
<var>predicate</var> is evaluated.  If it yields a true value, then
<var>consequent</var> is evaluated and its value is returned.  Otherwise
<var>alternative</var> is evaluated and its value is returned.  If
<var>predicate</var> yields a false value and no <var>alternative</var> is
specified, then the result of the expression is unspecified.
</p>
<p>An <code>if</code> expression evaluates either <var>consequent</var> or
<var>alternative</var>, never both.  Programs should not depend on the value
of an <code>if</code> expression that has no <var>alternative</var>.
</p>
<div class="example">
<pre class="example">(if (&gt; 3 2) 'yes 'no)                   &rArr;  yes
(if (&gt; 2 3) 'yes 'no)                   &rArr;  no
(if (&gt; 3 2)
    (- 3 2)
    (+ 3 2))                            &rArr;  1
</pre></div>
</dd></dl>

<dl>
<dt id="index-cond-1">standard special form: <strong>cond</strong> <em>clause clause &hellip;</em></dt>
<dd><span id="index-cond-clause"></span>
<span id="index-clause_002c-of-cond-expression"></span>
<p>Each <var>clause</var> has this form:
</p>
<div class="example">
<pre class="example">(<var>predicate</var> <var>expression</var> &hellip;)
</pre></div>

<p><span id="index-else-clause_002c-of-cond-expression-_0028defn_0029"></span>
<span id="index-else-1"></span>
where <var>predicate</var> is any expression.  The last <var>clause</var> may be
an <em><code>else</code> clause</em>, which has the form:
</p>
<div class="example">
<pre class="example">(else <var>expression</var> <var>expression</var> &hellip;)
</pre></div>

<p>A <code>cond</code> expression does the following:
</p>
<ol>
<li> Evaluates the <var>predicate</var> expressions of successive <var>clause</var>s in
order, until one of the <var>predicate</var>s evaluates to a true
value.

</li><li> When a <var>predicate</var> evaluates to a true value, <code>cond</code> evaluates
the <var>expression</var>s in the associated <var>clause</var> in left to right
order, and returns the result of evaluating the last <var>expression</var> in
the <var>clause</var> as the result of the entire <code>cond</code> expression.

<p>If the selected <var>clause</var> contains only the <var>predicate</var> and no
<var>expression</var>s, <code>cond</code> returns the value of the <var>predicate</var>
as the result.
</p>
</li><li> If all <var>predicate</var>s evaluate to false values, and there is no
<code>else</code> clause, the result of the conditional expression is
unspecified; if there is an <code>else</code> clause, <code>cond</code> evaluates
its <var>expression</var>s (left to right) and returns the value of the last
one.
</li></ol>

<div class="example">
<pre class="example">(cond ((&gt; 3 2) 'greater)
      ((&lt; 3 2) 'less))                  &rArr;  greater

(cond ((&gt; 3 3) 'greater)
      ((&lt; 3 3) 'less)
      (else 'equal))                    &rArr;  equal
</pre></div>

<p>Normally, programs should not depend on the value of a <code>cond</code>
expression that has no <code>else</code> clause.  However, some Scheme
programmers prefer to write <code>cond</code> expressions in which at least
one of the <var>predicate</var>s is always true.  In this style, the final
<var>clause</var> is equivalent to an <code>else</code> clause.
</p>
<span id="index-_003d_003e-in-cond-clause"></span>
<span id="index-_003d_003e"></span>
<p>Scheme supports an alternative <var>clause</var> syntax:
</p>
<div class="example">
<pre class="example">(<var>predicate</var> =&gt; <var>recipient</var>)
</pre></div>

<p>where <var>recipient</var> is an expression.  If <var>predicate</var> evaluates to
a true value, then <var>recipient</var> is evaluated.  Its value must be a
procedure of one argument; this procedure is then invoked on the value
of the <var>predicate</var>.
</p>
<div class="example">
<pre class="example">(cond ((assv 'b '((a 1) (b 2))) =&gt; cadr)
      (else #f))                        &rArr;  2
</pre></div>
</dd></dl>

<dl>
<dt id="index-case">standard special form: <strong>case</strong> <em>key clause clause &hellip;</em></dt>
<dd><span id="index-case-clause"></span>
<span id="index-clause_002c-of-case-expression"></span>
<p><var>Key</var> may be any expression.  Each <var>clause</var> has this
form:
</p>
<div class="example">
<pre class="example">((<var>object</var> &hellip;) <var>expression</var> <var>expression</var> &hellip;)
</pre></div>

<span id="index-else-clause_002c-of-case-expression-_0028defn_0029"></span>
<span id="index-else-2"></span>
<p>No <var>object</var> is evaluated, and all the <var>object</var>s must be
distinct.  The last <var>clause</var> may be an <em><code>else</code> clause</em>,
which has the form:
</p>
<div class="example">
<pre class="example">(else <var>expression</var> <var>expression</var> &hellip;)
</pre></div>

<p>A <code>case</code> expression does the following:
</p>
<ol>
<li> Evaluates <var>key</var> and compares the result with each
<var>object</var>.

</li><li> If the result of evaluating <var>key</var> is equivalent (in the sense of
<code>eqv?</code>; see <a href="Equivalence-Predicates.html">Equivalence Predicates</a>) to an <var>object</var>,
<code>case</code> evaluates the <var>expression</var>s in the corresponding
<var>clause</var> from left to right and returns the result of evaluating the
last <var>expression</var> in the <var>clause</var> as the result of the
<code>case</code> expression.
<span id="index-eqv_003f-1"></span>

</li><li> If the result of evaluating <var>key</var> is different from every
<var>object</var>, and if there&rsquo;s an <code>else</code> clause, <code>case</code>
evaluates its <var>expression</var>s and returns the result of the last one
as the result of the <code>case</code> expression.  If there&rsquo;s no <code>else</code>
clause, <code>case</code> returns an unspecified result.  Programs should not
depend on the value of a <code>case</code> expression that has no <code>else</code>
clause.
</li></ol>

<p>For example,
</p>
<div class="example">
<pre class="example">(case (* 2 3)
   ((2 3 5 7) 'prime)
   ((1 4 6 8 9) 'composite))            &rArr;  composite

(case (car '(c d))
   ((a) 'a)
   ((b) 'b))                            &rArr;  <span class="roman">unspecified</span>

(case (car '(c d))
   ((a e i o u) 'vowel)
   ((w y) 'semivowel)
   (else 'consonant))                   &rArr;  consonant
</pre></div>
</dd></dl>

<dl>
<dt id="index-and">standard special form: <strong>and</strong> <em>expression &hellip;</em></dt>
<dd><p>The <var>expression</var>s are evaluated from left to right, and the value of
the first <var>expression</var> that evaluates to a false value is returned.
Any remaining <var>expression</var>s are not evaluated.  If all the
<var>expression</var>s evaluate to true values, the value of the last
<var>expression</var> is returned.  If there are no <var>expression</var>s then
<code>#t</code> is returned.
</p>
<div class="example">
<pre class="example">(and (= 2 2) (&gt; 2 1))                   &rArr;  #t
(and (= 2 2) (&lt; 2 1))                   &rArr;  #f
(and 1 2 'c '(f g))                     &rArr;  (f g)
(and)                                   &rArr;  #t
</pre></div>
</dd></dl>

<dl>
<dt id="index-or">standard special form: <strong>or</strong> <em>expression &hellip;</em></dt>
<dd><p>The <var>expression</var>s are evaluated from left to right, and the value of
the first <var>expression</var> that evaluates to a true value is returned.
Any remaining <var>expression</var>s are not evaluated.  If all
<var>expression</var>s evaluate to false values, the value of the last
<var>expression</var> is returned.  If there are no <var>expression</var>s then
<code>#f</code> is returned.
</p>
<div class="example">
<pre class="example">(or (= 2 2) (&gt; 2 1))                    &rArr;  #t
(or (= 2 2) (&lt; 2 1))                    &rArr;  #t
(or #f #f #f)                           &rArr;  #f
(or (memq 'b '(a b c)) (/ 3 0))         &rArr;  (b c)
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Sequencing.html" accesskey="n" rel="next">Sequencing</a>, Previous: <a href="Quoting.html" accesskey="p" rel="prev">Quoting</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
