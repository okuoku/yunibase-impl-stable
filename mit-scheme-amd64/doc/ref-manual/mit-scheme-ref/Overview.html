<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Overview (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Overview (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Overview (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Notational-Conventions.html" rel="next" title="Notational Conventions">
<link href="Acknowledgements.html" rel="prev" title="Acknowledgements">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Overview"></span><div class="header">
<p>
Next: <a href="Special-Forms.html" accesskey="n" rel="next">Special Forms</a>, Previous: <a href="Acknowledgements.html" accesskey="p" rel="prev">Acknowledgements</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Overview-1"></span><h2 class="chapter">1 Overview</h2>

<span id="index-runtime-system"></span>
<p>This manual is a detailed description of the MIT/GNU Scheme runtime system.
It is intended to be a reference document for programmers.  It does not
describe how to run Scheme or how to interact with it &mdash; that is the
subject of the <cite>MIT/GNU Scheme User&rsquo;s Manual</cite>.
</p>
<p>This chapter summarizes the semantics of Scheme, briefly describes the
MIT/GNU Scheme programming environment, and explains the syntactic and
lexical conventions of the language.  Subsequent chapters describe
special forms, numerous data abstractions, and facilities for input and
output.
</p>
<span id="index-standard-Scheme-_0028defn_0029"></span>
<span id="index-Scheme-standard"></span>
<span id="index-R4RS"></span>
<p>Throughout this manual, we will make frequent references to
<em>standard Scheme</em>, which is the language defined by the document
<cite>Revised^4 Report on the Algorithmic Language Scheme</cite>, by William
Clinger, Jonathan Rees, et al., or by <small>IEEE</small> Std. 1178-1990,
<cite>IEEE Standard for the Scheme Programming Language</cite> (in fact,
several parts of this document are copied from the <cite>Revised
Report</cite>).  MIT/GNU Scheme is an extension of standard Scheme.
</p>
<p>These are the significant semantic characteristics of the Scheme
language:
</p>
<dl compact="compact">
<dt>Variables are statically scoped</dt>
<dd><span id="index-static-scoping-_0028defn_0029"></span>
<span id="index-scope-_0028see-region_0029"></span>
<p>Scheme is a <em>statically scoped</em> programming language, which means that
each use of a variable is associated with a lexically apparent binding
of that variable.  Algol is another statically scoped language.
</p>
</dd>
<dt>Types are latent</dt>
<dd><span id="index-latent-types-_0028defn_0029"></span>
<span id="index-manifest-types-_0028defn_0029"></span>
<span id="index-weak-types-_0028defn_0029"></span>
<span id="index-strong-types-_0028defn_0029"></span>
<span id="index-dynamic-types-_0028defn_0029"></span>
<span id="index-static-types-_0028defn_0029"></span>
<span id="index-types_002c-latent-_0028defn_0029"></span>
<span id="index-types_002c-manifest-_0028defn_0029"></span>
<p>Scheme has <em>latent</em> types as opposed to <em>manifest</em> types, which
means that Scheme associates types with values (or objects) rather than
with variables.  Other languages with latent types (also referred to as
<em>weakly</em> typed or <em>dynamically</em> typed languages) include APL,
Snobol, and other dialects of Lisp.  Languages with manifest types
(sometimes referred to as <em>strongly</em> typed or <em>statically</em> typed
languages) include Algol 60, Pascal, and C.
</p>
</dd>
<dt>Objects have unlimited extent</dt>
<dd><span id="index-extent_002c-of-objects"></span>
<p>All objects created during a Scheme computation, including procedures
and continuations, have unlimited extent; no Scheme object is ever
destroyed.  The system doesn&rsquo;t run out of memory because the garbage
collector reclaims the storage occupied by an object when the object
cannot possibly be needed by a future computation.  Other languages in
which most objects have unlimited extent include APL and other Lisp
dialects.
</p>
</dd>
<dt>Proper tail recursion</dt>
<dd><span id="index-proper-tail-recursion-_0028defn_0029"></span>
<span id="index-tail-recursion-_0028defn_0029"></span>
<span id="index-recursion-_0028see-tail-recursion_0029"></span>
<p>Scheme is <em>properly tail-recursive</em>, which means that iterative
computation can occur in constant space, even if the iterative
computation is described by a syntactically recursive procedure.  With a
tail-recursive implementation, you can express iteration using the
ordinary procedure-call mechanics; special iteration expressions are
provided only for syntactic convenience.
</p>
</dd>
<dt>Procedures are objects</dt>
<dd><p>Scheme procedures are objects, which means that you can create them
dynamically, store them in data structures, return them as the results
of other procedures, and so on.  Other languages with such procedure
objects include Common Lisp and ML.
</p>
</dd>
<dt>Continuations are explicit</dt>
<dd><p>In most other languages, continuations operate behind the scenes.  In
Scheme, continuations are objects; you can use continuations for
implementing a variety of advanced control constructs, including
non-local exits, backtracking, and coroutines.
</p>
</dd>
<dt>Arguments are passed by value</dt>
<dd><p>Arguments to Scheme procedures are passed by value, which means that
Scheme evaluates the argument expressions before the procedure gains
control, whether or not the procedure needs the result of the
evaluations.  ML, C, and APL are three other languages that pass
arguments by value.  In languages such as SASL and Algol 60, argument
expressions are not evaluated unless the values are needed by the
procedure.
</p></dd>
</dl>

<span id="index-read"></span>
<p>Scheme uses a parenthesized-list Polish notation to describe programs
and (other) data.  The syntax of Scheme, like that of most Lisp
dialects, provides for great expressive power, largely due to its
simplicity.  An important consequence of this simplicity is the
susceptibility of Scheme programs and data to uniform treatment by other
Scheme programs.  As with other Lisp dialects, the <code>read</code> primitive
parses its input; that is, it performs syntactic as well as lexical
decomposition of what it reads.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Notational-Conventions.html" accesskey="1">Notational Conventions</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Scheme-Concepts.html" accesskey="2">Scheme Concepts</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Lexical-Conventions.html" accesskey="3">Lexical Conventions</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Expressions.html" accesskey="4">Expressions</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Special-Forms.html" accesskey="n" rel="next">Special Forms</a>, Previous: <a href="Acknowledgements.html" accesskey="p" rel="prev">Acknowledgements</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
