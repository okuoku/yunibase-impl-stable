<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Records (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Records (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Records (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Promises.html" rel="next" title="Promises">
<link href="Parameters.html" rel="prev" title="Parameters">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Records"></span><div class="header">
<p>
Next: <a href="Promises.html" accesskey="n" rel="next">Promises</a>, Previous: <a href="Parameters.html" accesskey="p" rel="prev">Parameters</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Records-1"></span><h3 class="section">10.4 Records</h3>

<p>MIT/GNU Scheme provides a <em>record</em> abstraction, which is a simple and
flexible mechanism for building structures with named components.
Records can be defined and accessed using the procedures defined in this
section.  A less flexible but more concise way to manipulate records is
to use the <code>define-structure</code> special form (see <a href="Structure-Definitions.html">Structure Definitions</a>).
<span id="index-define_002dstructure-1"></span>
</p>
<dl>
<dt id="index-make_002drecord_002dtype">procedure: <strong>make-record-type</strong> <em>type-name field-names</em></dt>
<dd><span id="index-record_002dtype-descriptor-_0028defn_0029"></span>
<p>Returns a <em>record-type descriptor</em>, a value representing a new data
type, disjoint from all others.  The <var>type-name</var> argument must be a
string, but is only used for debugging purposes (such as the printed
representation of a record of the new type).  The <var>field-names</var>
argument is a list of symbols naming the <em>fields</em> of a record of the
new type.  It is an error if the list contains any duplicates.  It is
unspecified how record-type descriptors are represented.
</p></dd></dl>

<dl>
<dt id="index-record_002dconstructor">procedure: <strong>record-constructor</strong> <em>record-type [field-names]</em></dt>
<dd><p>Returns a procedure for constructing new members of the type represented
by <var>record-type</var>.  The returned procedure accepts exactly as many
arguments as there are symbols in the given list, <var>field-names</var>;
these are used, in order, as the initial values of those fields in a new
record, which is returned by the constructor procedure.  The values of
any fields not named in the list of <var>field-names</var> are unspecified.
The <var>field-names</var> argument defaults to the list of field-names in
the call to <code>make-record-type</code> that created the type represented by
<var>record-type</var>; if the <var>field-names</var> argument is provided, it is
an error if it contains any duplicates or any symbols not in the default
list.
</p></dd></dl>

<dl>
<dt id="index-record_002dkeyword_002dconstructor">procedure: <strong>record-keyword-constructor</strong> <em>record-type</em></dt>
<dd><p>Returns a procedure for constructing new members of the type represented
by <var>record-type</var>.  The returned procedure accepts arguments in a
<em>keyword list</em>, which is an alternating sequence of names and
values.  In other words, the number of arguments must be a multiple of
two, and every other argument, starting with the first argument, must be
a symbol that is one of the field names for <var>record-type</var>.
</p>
<p>The returned procedure may be called with a keyword list that contains
multiple instances of the same keyword.  In this case, the leftmost
instance is used and the other instances are ignored.  This allows
keyword lists to be accumulated using <code>cons</code> or <code>cons*</code>, and
new bindings added to the front of the list override old bindings at the
end.
</p></dd></dl>

<dl>
<dt id="index-record_002dpredicate">procedure: <strong>record-predicate</strong> <em>record-type</em></dt>
<dd><p>Returns a procedure for testing membership in the type represented by
<var>record-type</var>.  The returned procedure accepts exactly one argument
and returns <code>#t</code> if the argument is a member of the indicated
record type; it returns <code>#f</code> otherwise.
</p></dd></dl>

<dl>
<dt id="index-record_002daccessor">procedure: <strong>record-accessor</strong> <em>record-type field-name</em></dt>
<dd><p>Returns a procedure for reading the value of a particular field of a
member of the type represented by <var>record-type</var>.  The returned
procedure accepts exactly one argument which must be a record of the
appropriate type; it returns the current value of the field named by the
symbol <var>field-name</var> in that record.  The symbol <var>field-name</var>
must be a member of the list of field names in the call to
<code>make-record-type</code> that created the type represented by
<var>record-type</var>.
</p></dd></dl>

<dl>
<dt id="index-record_002dmodifier">procedure: <strong>record-modifier</strong> <em>record-type field-name</em></dt>
<dd><p>Returns a procedure for writing the value of a particular field of a
member of the type represented by <var>record-type</var>.  The returned
procedure accepts exactly two arguments: first, a record of the
appropriate type, and second, an arbitrary Scheme value; it modifies the
field named by the symbol <var>field-name</var> in that record to contain the
given value.  The returned value of the modifier procedure is
unspecified.  The symbol <var>field-name</var> must be a member of the list
of field names in the call to <code>make-record-type</code> that created the
type represented by <var>record-type</var>.
</p></dd></dl>

<dl>
<dt id="index-record_003f">procedure: <strong>record?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-record"></span>
<p>Returns <code>#t</code> if <var>object</var> is a record of any type and <code>#f</code>
otherwise.  Note that <code>record?</code> may be true of any Scheme value; of
course, if it returns <code>#t</code> for some particular value, then
<code>record-type-descriptor</code> is applicable to that value and returns an
appropriate descriptor.
</p></dd></dl>

<dl>
<dt id="index-record_002dtype_002ddescriptor">procedure: <strong>record-type-descriptor</strong> <em>record</em></dt>
<dd><p>Returns the record-type descriptor representing the type of
<var>record</var>.  That is, for example, if the returned descriptor were
passed to <code>record-predicate</code>, the resulting predicate would return
<code>#t</code> when passed <var>record</var>.  Note that it is not necessarily the
case that the returned descriptor is the one that was passed to
<code>record-constructor</code> in the call that created the constructor
procedure that created <var>record</var>.
</p></dd></dl>

<dl>
<dt id="index-record_002dtype_003f">procedure: <strong>record-type?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-record-type"></span>
<p>Returns <code>#t</code> if <var>object</var> is a record-type descriptor; otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-record_002dtype_002dname">procedure: <strong>record-type-name</strong> <em>record-type</em></dt>
<dd><p>Returns the type name associated with the type represented by
<var>record-type</var>.  The returned value is <code>eqv?</code> to the
<var>type-name</var> argument given in the call to <code>make-record-type</code>
that created the type represented by <var>record-type</var>.
</p></dd></dl>

<dl>
<dt id="index-record_002dtype_002dfield_002dnames">procedure: <strong>record-type-field-names</strong> <em>record-type</em></dt>
<dd><p>Returns a list of the symbols naming the fields in members of the type
represented by <var>record-type</var>.  The returned value is <code>equal?</code>
to the <var>field-names</var> argument given in the call to
<code>make-record-type</code> that created the type represented by
<var>record-type</var>.<a id="DOCF10" href="#FOOT10"><sup>10</sup></a>
</p></dd></dl>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT10" href="#DOCF10">(10)</a></h3>
<p>In MIT/GNU Scheme, the returned list is always
newly allocated.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Promises.html" accesskey="n" rel="next">Promises</a>, Previous: <a href="Parameters.html" accesskey="p" rel="prev">Parameters</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
