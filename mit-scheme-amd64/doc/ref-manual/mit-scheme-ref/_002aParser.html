<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>*Parser (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="*Parser (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="*Parser (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parser-Language.html" rel="up" title="Parser Language">
<link href="Parser_002dlanguage-Macros.html" rel="next" title="Parser-language Macros">
<link href="_002aMatcher.html" rel="prev" title="*Matcher">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="g_t_002aParser"></span><div class="header">
<p>
Next: <a href="Parser_002dlanguage-Macros.html" accesskey="n" rel="next">Parser-language Macros</a>, Previous: <a href="_002aMatcher.html" accesskey="p" rel="prev">*Matcher</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="g_t_002aParser-1"></span><h4 class="subsection">14.14.2 *Parser</h4>

<span id="index-Parser-language-1"></span>
<span id="index-Parser-procedure"></span>
<p>The <em>parser language</em> is a declarative language for specifying a
<em>parser procedure</em>.  A parser procedure is a procedure that
accepts a single parser-buffer argument and parses some of the input
from the buffer.  If the parse is successful, the procedure returns a
vector of objects that are the result of the parse, and the internal
pointer of the parser buffer is advanced past the input that was
parsed.  If the parse fails, the procedure returns <code>#f</code> and the
internal pointer is unchanged.  This interface is much like that of a
matcher procedure, except that on success the parser procedure returns
a vector of values rather than <code>#t</code>.
</p>
<p>The <code>*parser</code> special form is the interface between the parser
language and Scheme.
</p>
<dl>
<dt id="index-_002aparser">special form: <strong>*parser</strong> <em>pexp</em></dt>
<dd><p>The operand <var>pexp</var> is an expression in the parser language.  The
<code>*parser</code> expression expands into Scheme code that implements a
parser procedure.
</p></dd></dl>

<p>There are several primitive expressions in the parser language.  The
first two provide a bridge to the matcher language (see <a href="_002aMatcher.html">*Matcher</a>):
</p>
<dl>
<dt id="index-match">parser expression: <strong>match</strong> <em>mexp</em></dt>
<dd><p>The <code>match</code> expression performs a match on the parser buffer.
The match to be performed is specified by <var>mexp</var>, which is an
expression in the matcher language.  If the match is successful, the
result of the <code>match</code> expression is a vector of one element: a
string containing that text.
</p></dd></dl>

<dl>
<dt id="index-noise">parser expression: <strong>noise</strong> <em>mexp</em></dt>
<dd><p>The <code>noise</code> expression performs a match on the parser buffer.
The match to be performed is specified by <var>mexp</var>, which is an
expression in the matcher language.  If the match is successful, the
result of the <code>noise</code> expression is a vector of zero elements.
(In other words, the text is matched and then thrown away.)
</p>
<p>The <var>mexp</var> operand is often a known character or string, so in the
case that <var>mexp</var> is a character or string literal, the
<code>noise</code> expression can be abbreviated as the literal.  In other
words, &lsquo;<samp>(noise &quot;foo&quot;)</samp>&rsquo; can be abbreviated just &lsquo;<samp>&quot;foo&quot;</samp>&rsquo;.
</p></dd></dl>

<dl>
<dt id="index-values-1">parser expression: <strong>values</strong> <em>expression &hellip;</em></dt>
<dd><p>Sometimes it is useful to be able to insert arbitrary values into the
parser result.  The <code>values</code> expression supports this.  The
<var>expression</var> arguments are arbitrary Scheme expressions that are
evaluated at run time and returned in a vector.  The <code>values</code>
expression always succeeds and never modifies the internal pointer of
the parser buffer.
</p></dd></dl>

<dl>
<dt id="index-discard_002dmatched-1">parser expression: <strong>discard-matched</strong></dt>
<dd><p>The <code>discard-matched</code> expression always succeeds, returning a
vector of zero elements.  In all other respects it is identical to the
<code>discard-matched</code> expression in the matcher language.
</p></dd></dl>

<p>Next there are several combinator expressions.  Parameters named
<var>pexp</var> are arbitrary expressions in the parser language.  The
first few combinators are direct equivalents of those in the matcher
language.
</p>
<dl>
<dt id="index-seq-2">parser expression: <strong>seq</strong> <em>pexp &hellip;</em></dt>
<dd><p>The <code>seq</code> expression parses each of the <var>pexp</var> operands in
order.  If all of the <var>pexp</var> operands successfully match, the
result is the concatenation of their values (by <code>vector-append</code>).
</p></dd></dl>

<dl>
<dt id="index-alt-2">parser expression: <strong>alt</strong> <em>pexp &hellip;</em></dt>
<dd><p>The <code>alt</code> expression attempts to parse each <var>pexp</var> operand in
order from left to right.  The first one that successfully parses
produces the result for the entire <code>alt</code> expression.
</p>
<p>Like the <code>alt</code> expression in the matcher language, this
expression participates in backtracking.
</p></dd></dl>

<dl>
<dt id="index-_002a-4">parser expression: <strong>*</strong> <em>pexp</em></dt>
<dd><p>The <code>*</code> expression parses zero or more occurrences of <var>pexp</var>.
The results of the parsed occurrences are concatenated together (by
<code>vector-append</code>) to produce the expression&rsquo;s result.
</p>
<p>Like the <code>*</code> expression in the matcher language, this expression
participates in backtracking.
</p></dd></dl>

<dl>
<dt id="index-_002b-5">parser expression: <strong>+</strong> <em>pexp</em></dt>
<dd><p>The <code>*</code> expression parses one or more occurrences of <var>pexp</var>.
It is equivalent to
</p>
<div class="example">
<pre class="example">(seq <var>pexp</var> (* <var>pexp</var>))
</pre></div>
</dd></dl>

<dl>
<dt id="index-_003f-2">parser expression: <strong>?</strong> <em>pexp</em></dt>
<dd><p>The <code>*</code> expression parses zero or one occurrences of <var>pexp</var>.
It is equivalent to
</p>
<div class="example">
<pre class="example">(alt <var>pexp</var> (seq))
</pre></div>
</dd></dl>

<p>The next three expressions do not have equivalents in the matcher
language.  Each accepts a single <var>pexp</var> argument, which is parsed
in the usual way.  These expressions perform transformations on the
returned values of a successful match.
</p>
<dl>
<dt id="index-transform">parser expression: <strong>transform</strong> <em>expression pexp</em></dt>
<dd><p>The <code>transform</code> expression performs an arbitrary transformation
of the values returned by parsing <var>pexp</var>.  <var>Expression</var> is a
Scheme expression that must evaluate to a procedure at run time.  If
<var>pexp</var> is successfully parsed, the procedure is called with the
vector of values as its argument, and must return a vector or
<code>#f</code>.  If it returns a vector, the parse is successful, and those
are the resulting values.  If it returns <code>#f</code>, the parse fails
and the internal pointer of the parser buffer is returned to what it
was before <var>pexp</var> was parsed.
</p>
<p>For example:
</p>
<div class="example">
<pre class="example">(transform (lambda (v) (if (= 0 (vector-length v)) #f v)) &hellip;)
</pre></div>
</dd></dl>

<dl>
<dt id="index-encapsulate">parser expression: <strong>encapsulate</strong> <em>expression pexp</em></dt>
<dd><p>The <code>encapsulate</code> expression transforms the values returned by
parsing <var>pexp</var> into a single value.  <var>Expression</var> is a Scheme
expression that must evaluate to a procedure at run time.  If
<var>pexp</var> is successfully parsed, the procedure is called with the
vector of values as its argument, and may return any Scheme object.
The result of the <code>encapsulate</code> expression is a vector of length
one containing that object.  (And consequently <code>encapsulate</code>
doesn&rsquo;t change the success or failure of <var>pexp</var>, only its value.)
</p>
<p>For example:
</p>
<div class="example">
<pre class="example">(encapsulate vector-&gt;list &hellip;)
</pre></div>
</dd></dl>

<dl>
<dt id="index-map-1">parser expression: <strong>map</strong> <em>expression pexp</em></dt>
<dd><p>The <code>map</code> expression performs a per-element transform on the
values returned by parsing <var>pexp</var>.  <var>Expression</var> is a Scheme
expression that must evaluate to a procedure at run time.  If
<var>pexp</var> is successfully parsed, the procedure is mapped (by
<code>vector-map</code>) over the values returned from the parse.  The
mapped values are returned as the result of the <code>map</code> expression.
(And consequently <code>map</code> doesn&rsquo;t change the success or failure of
<var>pexp</var>, nor the number of values returned.)
</p>
<p>For example:
</p>
<div class="example">
<pre class="example">(map string-&gt;symbol &hellip;)
</pre></div>
</dd></dl>

<p>Finally, as in the matcher language, we have <code>sexp</code> and
<code>with-pointer</code> to support embedding Scheme code in the parser.
</p>
<dl>
<dt id="index-sexp-1">parser expression: <strong>sexp</strong> <em>expression</em></dt>
<dd><p>The <code>sexp</code> expression allows arbitrary Scheme code to be embedded
inside a parser.  The <var>expression</var> operand must evaluate to a
parser procedure at run time; the procedure is called to parse the
parser buffer.  This is the parser-language equivalent of the
<code>sexp</code> expression in the matcher language.
</p>
<p>The case in which <var>expression</var> is a symbol is so common that it
has an abbreviation: &lsquo;<samp>(sexp <var>symbol</var>)</samp>&rsquo; may be abbreviated as
just <var>symbol</var>.
</p></dd></dl>

<dl>
<dt id="index-with_002dpointer-1">parser expression: <strong>with-pointer</strong> <em>identifier pexp</em></dt>
<dd><p>The <code>with-pointer</code> expression fetches the parser buffer&rsquo;s
internal pointer (using <code>get-parser-buffer-pointer</code>), binds it to
<var>identifier</var>, and then parses the pattern specified by <var>pexp</var>.
<var>Identifier</var> must be a symbol.  This is the parser-language
equivalent of the <code>with-pointer</code> expression in the matcher
language.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Parser_002dlanguage-Macros.html" accesskey="n" rel="next">Parser-language Macros</a>, Previous: <a href="_002aMatcher.html" accesskey="p" rel="prev">*Matcher</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
