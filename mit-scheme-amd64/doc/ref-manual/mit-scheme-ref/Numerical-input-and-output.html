<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Numerical input and output (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Numerical input and output (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Numerical input and output (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Numbers.html" rel="up" title="Numbers">
<link href="Fixnum-and-Flonum-Operations.html" rel="next" title="Fixnum and Flonum Operations">
<link href="Numerical-operations.html" rel="prev" title="Numerical operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Numerical-input-and-output"></span><div class="header">
<p>
Next: <a href="Fixnum-and-Flonum-Operations.html" accesskey="n" rel="next">Fixnum and Flonum Operations</a>, Previous: <a href="Numerical-operations.html" accesskey="p" rel="prev">Numerical operations</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Numerical-input-and-output-1"></span><h3 class="section">4.6 Numerical input and output</h3>
<span id="index-numerical-input-and-output"></span>

<dl>
<dt id="index-number_002d_003estring">procedure: <strong>number-&gt;string</strong> <em>number [radix]</em></dt>
<dd><p><var>Radix</var> must be an exact integer, either 2, 8, 10, or 16.  If
omitted, <var>radix</var> defaults to 10.  The procedure
<code>number-&gt;string</code> takes a number and a radix and returns as a string
an external representation of the given number in the given radix such
that
</p>
<div class="example">
<pre class="example">(let ((number <var>number</var>)
      (radix <var>radix</var>))
  (eqv? number
        (string-&gt;number (number-&gt;string number radix)
                        radix)))
</pre></div>

<p>is true.  It is an error if no possible result makes this expression
true.
</p>
<p>If <var>number</var> is inexact, the radix is 10, and the above expression
can be satisfied by a result that contains a decimal point, then the
result contains a decimal point and is expressed using the minimum
number of digits (exclusive of exponent and trailing zeroes) needed to
make the above expression true; otherwise the format of the result is
unspecified.
</p>
<p>The result returned by <code>number-&gt;string</code> never contains an explicit
radix prefix.
</p>
<p>Note: The error case can occur only when <var>number</var> is not a complex
number or is a complex number with an non-rational real or imaginary
part.
</p>
<p>Rationale: If <var>number</var> is an inexact number represented using
flonums, and the radix is 10, then the above expression is normally
satisfied by a result containing a decimal point.  The unspecified case
allows for infinities, NaNs, and non-flonum representations.
</p></dd></dl>

<dl>
<dt id="index-flonum_002dparser_002dfast_003f">variable: <strong>flonum-parser-fast?</strong></dt>
<dd><p>This variable controls the behavior of <code>string-&gt;number</code> when
parsing inexact numbers.  Specifically, it allows the user to trade off
accuracy against speed.
</p>
<p>When set to its default value, <code>#f</code>, the parser provides maximal
accuracy, as required by the Scheme standard.  If set to <code>#t</code>, the
parser uses faster algorithms that will sometimes introduce small errors
in the result.  The errors affect a few of the least-significant bits of
the result, and consequently can be tolerated by many applications.
</p></dd></dl>

<dl>
<dt id="index-flonum_002dunparser_002dcutoff">variable: <strong>flonum-unparser-cutoff</strong></dt>
<dd><p>This variable is <strong>deprecated</strong>; use
<code>param:flonum-printer-cutoff</code> instead.
</p></dd></dl>

<dl>
<dt id="index-param_003aflonum_002dprinter_002dcutoff">parameter: <strong>param:flonum-printer-cutoff</strong></dt>
<dd><p>This parameter controls the action of <code>number-&gt;string</code> when
<var>number</var> is a flonum (and consequently controls all printing of
flonums).  This parameter may be called with an argument to set its
value.
</p>
<p>The value of this parameter is normally a list of three items:
</p>
<dl compact="compact">
<dt><var>rounding-type</var></dt>
<dd><p>One of the following symbols: <code>normal</code>, <code>relative</code>, or
<code>absolute</code>.  The symbol <code>normal</code> means that the number should
be printed with full precision.  The symbol <code>relative</code> means that
the number should be rounded to a specific number of digits.  The symbol
<code>absolute</code> means that the number should be rounded so that there
are a specific number of digits to the right of the decimal point.
</p>
</dd>
<dt><var>precision</var></dt>
<dd><p>An exact integer.  If <var>rounding-type</var> is <code>normal</code>,
<var>precision</var> is ignored.  If <var>rounding-type</var> is <code>relative</code>,
<var>precision</var> must be positive, and it specifies the number of digits
to which the printed representation will be rounded.  If
<var>rounding-type</var> is <code>absolute</code>, the printed representation will
be rounded <var>precision</var> digits to the right of the decimal point; if
<var>precision</var> is negative, the representation is rounded <code>(-
<var>precision</var>)</code> digits to the left of the decimal point.
</p>
</dd>
<dt><var>format-type</var></dt>
<dd><p>One of the symbols: <code>normal</code>, <code>scientific</code>, or
<code>engineering</code>.  This specifies the format in which the number will
be printed.<br>  <code>scientific</code> specifies that the number will be printed
using scientific notation: <code><var>x</var>.<var>xxx</var>e<var>yyy</var></code>.  In other
words, the number is printed as a significand between zero inclusive and
ten exclusive, and an exponent.  <code>engineering</code> is like
<code>scientific</code>, except that the exponent is always a power of three,
and the significand is constrained to be between zero inclusive and 1000
exclusive.  If <code>normal</code> is specified, the number will be printed in
positional notation if it is &ldquo;small enough&rdquo;, otherwise it is printed
in scientific notation.  A number is &ldquo;small enough&rdquo; when the number of
digits that would be printed using positional notation does not exceed
the number of digits of precision in the underlying floating-point
number representation; <acronym>IEEE 754-2008</acronym> binary64 floating-point
numbers have 17 digits of precision.
</p></dd>
</dl>

<p>This three-element list may be abbreviated in two ways.  First, the
symbol <code>normal</code> may be used, which is equivalent to the list
<code>(normal 0 normal)</code>.  Second, the third element of the list,
<var>format-type</var>, may be omitted, in which case it defaults to
<code>normal</code>.
</p>
<p>The default value for <code>param:flonum-printer-cutoff</code> is <code>normal</code>.
If it is bound to a value different from those described here,
<code>number-&gt;string</code> issues a warning and acts as though the value had
been <code>normal</code>.
</p></dd></dl>

<p>Some examples of <code>param:flonum-printer-cutoff</code>:
</p>
<div class="example">
<pre class="example">(number-&gt;string (* 4 (atan 1 1)))
                                    &rArr; &quot;3.141592653589793&quot;
(parameterize ((param:flonum-printer-cutoff '(relative 5)))
  (number-&gt;string (* 4 (atan 1 1))))
                                    &rArr; &quot;3.1416&quot;
(parameterize ((param:flonum-printer-cutoff '(relative 5)))
  (number-&gt;string (* 4000 (atan 1 1))))
                                    &rArr; &quot;3141.6&quot;
(parameterize ((param:flonum-printer-cutoff '(relative 5 scientific)))
  (number-&gt;string (* 4000 (atan 1 1))))
                                    &rArr; &quot;3.1416e3&quot;
(parameterize ((param:flonum-printer-cutoff '(relative 5 scientific)))
  (number-&gt;string (* 40000 (atan 1 1))))
                                    &rArr; &quot;3.1416e4&quot;
(parameterize ((param:flonum-printer-cutoff '(relative 5 engineering)))
  (number-&gt;string (* 40000 (atan 1 1))))
                                    &rArr; &quot;31.416e3&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute 5)))
  (number-&gt;string (* 4 (atan 1 1))))
                                    &rArr; &quot;3.14159&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute 5)))
  (number-&gt;string (* 4000 (atan 1 1))))
                                    &rArr; &quot;3141.59265&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute -4)))
  (number-&gt;string (* 4e10 (atan 1 1))))
                                    &rArr; &quot;31415930000.&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute -4 scientific)))
  (number-&gt;string (* 4e10 (atan 1 1))))
                                    &rArr; &quot;3.141593e10&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute -4 engineering)))
  (number-&gt;string (* 4e10 (atan 1 1))))
                                    &rArr; &quot;31.41593e9&quot;
(parameterize ((param:flonum-printer-cutoff '(absolute -5)))
  (number-&gt;string (* 4e10 (atan 1 1))))
                                    &rArr; &quot;31415900000.&quot;
</pre></div>

<dl>
<dt id="index-string_002d_003enumber">procedure: <strong>string-&gt;number</strong> <em>string [radix]</em></dt>
<dd><p>Returns a number of the maximally precise representation expressed by
the given <var>string</var>.  <var>Radix</var> must be an exact integer, either 2,
8, 10, or 16.  If supplied, <var>radix</var> is a default radix that may be
overridden by an explicit radix prefix in <var>string</var> (e.g.
<code>&quot;#o177&quot;</code>).  If <var>radix</var> is not supplied, then the default radix
is 10.  If <var>string</var> is not a syntactically valid notation for a
number, then <code>string-&gt;number</code> returns <code>#f</code>.
</p>
<div class="example">
<pre class="example">(string-&gt;number &quot;100&quot;)        &rArr;  100
(string-&gt;number &quot;100&quot; 16)     &rArr;  256
(string-&gt;number &quot;1e2&quot;)        &rArr;  100.0
(string-&gt;number &quot;15##&quot;)       &rArr;  1500.0
</pre></div>

<p>Note that a numeric representation using a decimal point or an exponent
marker is not recognized unless <var>radix</var> is <code>10</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Fixnum-and-Flonum-Operations.html" accesskey="n" rel="next">Fixnum and Flonum Operations</a>, Previous: <a href="Numerical-operations.html" accesskey="p" rel="prev">Numerical operations</a>, Up: <a href="Numbers.html" accesskey="u" rel="up">Numbers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
