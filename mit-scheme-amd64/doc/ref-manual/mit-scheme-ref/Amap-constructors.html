<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Amap constructors (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Amap constructors (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Amap constructors (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associative-Maps.html" rel="up" title="Associative Maps">
<link href="Amap-predicates.html" rel="next" title="Amap predicates">
<link href="Associative-Maps.html" rel="prev" title="Associative Maps">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Amap-constructors"></span><div class="header">
<p>
Next: <a href="Amap-predicates.html" accesskey="n" rel="next">Amap predicates</a>, Previous: <a href="Associative-Maps.html" accesskey="p" rel="prev">Associative Maps</a>, Up: <a href="Associative-Maps.html" accesskey="u" rel="up">Associative Maps</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Amap-constructors-1"></span><h4 class="subsection">11.8.1 Amap constructors</h4>

<p>All associative-map constructors take a comparator argument followed
by a list of additional specification arguments.  In all cases, the
comparator is used to compare keys in the map, while the additional
arguments specify the implementation and/or features of the map being
created.
</p>
<dl>
<dt id="index-make_002damap">procedure: <strong>make-amap</strong> <em>comparator arg &hellip;</em></dt>
<dd><p>Creates a new mutable associative map as specified by <var>comparator</var>
and <var>arg</var>s.
</p></dd></dl>

<dl>
<dt id="index-alist_002d_003eamap">procedure: <strong>alist-&gt;amap</strong> <em>alist comparator arg &hellip;</em></dt>
<dd><p>Creates a new associative map as specified by <var>comparator</var> and
<var>arg</var>s, and which contains the associations in <var>alist</var>.
</p></dd></dl>

<dl>
<dt id="index-amap_002dunfold">procedure: <strong>amap-unfold</strong> <em>stop? mapper successor seed comparator arg &hellip;</em></dt>
<dd><p>Creates a new associative map as specified by <var>comparator</var> and
<var>arg</var>s.  The associations in the resulting map are generated using
the additional arguments.
</p>
<p>The <var>stop?</var> argument is a unary predicate that takes a state value
and returns <code>#t</code> if generation is complete, otherwise <code>#f</code>.
The <var>mapper</var> argument is a unary procedure that takes a state value
and returns two values: a key and a value.  The <var>successor</var>
argument is a unary procedure that takes a state value and returns a
new state value.  And <var>seed</var> is the initial state value.
</p>
<p>The process by which the generator adds associations to the map is
this:
</p>
<div class="example">
<pre class="example">(let loop ((state <var>seed</var>))
  (if (<var>stop?</var> state)
      result
      (let-values (((key value) (<var>mapper</var> state)))
        (amap-set! result key value)
        (loop (<var>successor</var> state)))))
</pre></div>
</dd></dl>

<p>The arguments passed to a constructor can be divided into categories:
</p>
<ul>
<li> Each backing implementation has an associated name, which can be
passed as one of the arguments.  At present this includes
<code>alist</code>, <code>hash-table</code>, <code>red/black-tree</code>, etc.

<p>There must be at most one such name; it is an error to pass two or
more implementation names.  If an implementation name is given, then
the remaining arguments must be supported by that implementation.
</p>
</li><li> The arguments <code>weak-keys</code>, <code>weak-values</code>,
<code>ephemeral-keys</code>, and <code>ephemeral-values</code> specify how
associations interact with garbage collection.  Not all
implementations support these arguments, and those that do may support
only some of them.  Some combinations are not allowed: for example,
<code>weak-keys</code> and <code>ephemeral-keys</code> are mutually exclusive.

</li><li> Additional arguments are used to guide the choice of implementation
when it is not explicitly specified.  The desired time complexity can
be specified: <code>linear-time</code>, <code>log-time</code>,
<code>amortized-constant-time</code>, and <code>sublinear-time</code>.  The
argument <code>ordered-by-key</code> specifies that the map keeps its
associations ordered.

</li><li> The argument <code>thread-safe</code> specifies that the implementation is
safe to use in a multi-threaded environment.  At present none of the
implementations are thread-safe.

</li><li> Finally, an exact non-negative integer argument specifies the initial
size of the map.
</li></ul>

<p>This is a complex set of possible arguments.  In order to help explore
what arguments can be used, and in what combinations, we provide some
utility procedures:
</p>
<dl>
<dt id="index-amap_002dimplementation_002dnames">procedure: <strong>amap-implementation-names</strong></dt>
<dd><p>Returns a list of the supported implementation names.
</p></dd></dl>

<dl>
<dt id="index-amap_002dimplementation_002dsupported_002dargs">procedure: <strong>amap-implementation-supported-args</strong> <em>name</em></dt>
<dd><p>Returns a list of the arguments supported by the implementation
specified by <var>name</var>.  This list can include the procedure
<code>exact-nonnegative-integer?</code> if the implementation supports an
initial size.
</p></dd></dl>

<dl>
<dt id="index-amap_002dimplementation_002dsupports_002dargs_003f">procedure: <strong>amap-implementation-supports-args?</strong> <em>name args</em></dt>
<dd><p>Returns <code>#t</code> if the implementation specified by <var>name</var>
supports <var>args</var>, otherwise returns <code>#f</code>.
</p></dd></dl>

<p>An implementation may support a limited set of comparators.  For
example, a hash table requires a comparator that satisfies
<code>comparator-hashable?</code>, while a binary tree requires one
satisfying <code>comparator-ordered?</code>.
</p>
<dl>
<dt id="index-amap_002dimplementation_002dsupports_002dcomparator_003f">procedure: <strong>amap-implementation-supports-comparator?</strong> <em>name comparator</em></dt>
<dd><p>Returns <code>#t</code> if the implementation specified by <var>name</var>
supports <var>comparator</var>, otherwise returns <code>#f</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Amap-predicates.html" accesskey="n" rel="next">Amap predicates</a>, Previous: <a href="Associative-Maps.html" accesskey="p" rel="prev">Associative Maps</a>, Up: <a href="Associative-Maps.html" accesskey="u" rel="up">Associative Maps</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
