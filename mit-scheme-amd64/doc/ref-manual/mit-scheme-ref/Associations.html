<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Associations (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Associations (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Associations (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Association-Lists.html" rel="next" title="Association Lists">
<link href="Reference-barriers.html" rel="prev" title="Reference barriers">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Associations"></span><div class="header">
<p>
Next: <a href="Procedures.html" accesskey="n" rel="next">Procedures</a>, Previous: <a href="Miscellaneous-Datatypes.html" accesskey="p" rel="prev">Miscellaneous Datatypes</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Associations-1"></span><h2 class="chapter">11 Associations</h2>

<p>MIT/GNU Scheme provides several mechanisms for associating objects with
one another.  Each of these mechanisms creates a link between one or
more objects, called <em>keys</em>, and some other object, called a
<em>datum</em>.  Beyond this common idea, however, each of the mechanisms
has various different properties that make it appropriate in different
situations:
</p>
<ul>
<li> <em>Association lists</em> are one of Lisp&rsquo;s oldest association mechanisms.
Because they are made from ordinary pairs, they are easy to build and
manipulate, and very flexible in use.  However, the average lookup time
for an association list is linear in the number of associations.

</li><li> <em>1D tables</em> have a very simple interface, making them easy to use,
and offer the feature that they do not prevent their keys from being
reclaimed by the garbage collector.  Like association lists, their
average lookup time is linear in the number of associations; but 1D
tables aren&rsquo;t as flexible.

</li><li> <span id="index-property-list"></span>
<em>The association table</em> is MIT/GNU Scheme&rsquo;s equivalent to the
<em>property lists</em> of Lisp.  It has the advantages that the keys may
be any type of object and that it does not prevent the keys from being
reclaimed by the garbage collector.  However, two linear-time lookups
must be performed, one for each key, whereas for traditional property
lists only one lookup is required for both keys.

</li><li> <em>Hash tables</em> are a powerful mechanism with constant-time access to
large amounts of data.  Hash tables are not as flexible as association
lists, but because their access times are independent of the number of
associations in the table, for most applications they are the mechanism
of choice.

</li><li> <em>Balanced binary trees</em> are another association mechanism that is
useful for applications in which the keys are ordered.  Binary trees
have access times that are proportional to the logarithm of the number
of associations in the tree.  While they aren&rsquo;t as fast as hash tables,
they offer the advantage that the contents of the tree can be converted
to a sorted alist in linear time.  Additionally, two trees can be
compared for equality in worst-case linear time.

</li><li> <em>Red-Black trees</em> are a kind of balanced binary tree.  The
implementation supports destructive insertion and deletion operations
with a good constant factor.

</li><li> <em>Weight-Balanced trees</em> are a kind of balanced binary tree.  The
implementation provides non-destructive operations.  There is a
comprehensive set of operations, including: a constant-time size
operation; many high-level operations such as the set operations union,
intersection and difference; and indexing of elements by position.

</li></ul>

<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Association-Lists.html" accesskey="1">Association Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="1D-Tables.html" accesskey="2">1D Tables</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="The-Association-Table.html" accesskey="3">The Association Table</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Hash-Tables.html" accesskey="4">Hash Tables</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Object-Hashing.html" accesskey="5">Object Hashing</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Red_002dBlack-Trees.html" accesskey="6">Red-Black Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Weight_002dBalanced-Trees.html" accesskey="7">Weight-Balanced Trees</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Associative-Maps.html" accesskey="8">Associative Maps</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<hr>
<div class="header">
<p>
Next: <a href="Procedures.html" accesskey="n" rel="next">Procedures</a>, Previous: <a href="Miscellaneous-Datatypes.html" accesskey="p" rel="prev">Miscellaneous Datatypes</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
