<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Input Procedures (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Input Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Input Procedures (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Output-Procedures.html" rel="next" title="Output Procedures">
<link href="Bytevector-Ports.html" rel="prev" title="Bytevector Ports">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Input-Procedures"></span><div class="header">
<p>
Next: <a href="Output-Procedures.html" accesskey="n" rel="next">Output Procedures</a>, Previous: <a href="Bytevector-Ports.html" accesskey="p" rel="prev">Bytevector Ports</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Input-Procedures-1"></span><h3 class="section">14.5 Input Procedures</h3>
<span id="index-input-operations"></span>

<p>This section describes the procedures that read input.  Input procedures
can read either from the current input port or from a given port.
Remember that to read from a file, you must first open a port to the
file.
</p>
<span id="index-interactive-input-ports-_0028defn_0029"></span>
<p>Input ports can be divided into two types, called <em>interactive</em> and
<em>non-interactive</em>.  Interactive input ports are ports that read
input from a source that is time-dependent; for example, a port that
reads input from a terminal or from another program.  Non-interactive
input ports read input from a time-independent source, such as an
ordinary file or a character string.
</p>
<p>In this section, all optional arguments called <var>port</var> default to
the current input port.
</p>
<dl>
<dt id="index-read-7">standard procedure: <strong>read</strong> <em>[port]</em></dt>
<dd><span id="index-expression_002c-input-from-port"></span>
<span id="index-external-representation_002c-parsing"></span>
<span id="index-parsing_002c-of-external-representation"></span>
<p>The <code>read</code> procedure converts external representations of Scheme
objects into the objects themselves.  It returns the next object
parsable from the given textual input <var>port</var>, updating <var>port</var>
to point to the first character past the end of the external
representation of the object.
</p>
<p>Implementations may support extended syntax to represent record types
or other types that do not have datum representations.
</p>
<p>If an end of file is encountered in the input before any characters
are found that can begin an object, then an end-of-file object is
returned.  The port remains open, and further attempts to read will
also return an end-of-file object.  If an end of file is encountered
after the beginning of an object&rsquo;s external representation, but the
external representation is incomplete and therefore not parsable, an
error that satisfies <code>read-error?</code> is signaled.
</p>
<p>The <var>port</var> remains open, and further attempts to read will also
return an end-of-file object.  If an end of file is encountered after
the beginning of an object&rsquo;s written representation, but the written
representation is incomplete and therefore not parsable, an error is
signalled.
</p></dd></dl>

<dl>
<dt id="index-read_002dchar-1">standard procedure: <strong>read-char</strong> <em>[port]</em></dt>
<dd><span id="index-character_002c-input-from-port"></span>
<p>Returns the next character available from the textual input
<var>port</var>, updating <var>port</var> to point to the following character.
If no more characters are available, an end-of-file object is
returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port and no
characters are immediately available, <code>read-char</code> will hang
waiting for input, even if the port is in non-blocking mode.
</p></dd></dl>

<dl>
<dt id="index-read_002dchar_002dno_002dhang">procedure: <strong>read-char-no-hang</strong> <em>[port]</em></dt>
<dd><p>This procedure behaves exactly like <code>read-char</code> except when
<var>port</var> is an interactive port in non-blocking mode, and there are
no characters immediately available.  In that case this procedure
returns <code>#f</code> without blocking.
</p></dd></dl>

<dl>
<dt id="index-unread_002dchar">procedure: <strong>unread-char</strong> <em>char [port]</em></dt>
<dd><p>The given <var>char</var> must be the most-recently read character from the
textual input <var>port</var>.  This procedure &ldquo;unreads&rdquo; the character,
updating <var>port</var> as if the character had never been read.
</p>
<p>Note that this only works with characters returned by <code>read-char</code>
or <code>read-char-no-hang</code>.
</p></dd></dl>

<dl>
<dt id="index-peek_002dchar">standard procedure: <strong>peek-char</strong> <em>[port]</em></dt>
<dd><p>Returns the next character available from the textual input
<var>port</var>, <em>without</em> updating <var>port</var> to point to the
following character.  If no more characters are available, an
end-of-file object is returned.
</p>
<p><em>Note</em>: The value returned by a call to <code>peek-char</code> is the
same as the value that would have been returned by a call to
<code>read-char</code> on the same port.  The only difference is that the
very next call to <code>read-char</code> or <code>peek-char</code> on that
<var>port</var> will return the value returned by the preceding call to
<code>peek-char</code>.  In particular, a call to <code>peek-char</code> on an
interactive port will hang waiting for input whenever a call to
<code>read-char</code> would have hung.
</p></dd></dl>

<dl>
<dt id="index-read_002dline">standard procedure: <strong>read-line</strong> <em>[port]</em></dt>
<dd><p>Returns the next line of text available from the textual input
<var>port</var>, updating the <var>port</var> to point to the following
character.  If an end of line is read, a string containing all of the
text up to (but not including) the end of line is returned, and the
port is updated to point just past the end of line.  If an end of file
is encountered before any end of line is read, but some characters
have been read, a string containing those characters is returned.  If
an end of file is encountered before any characters are read, an
end-of-file object is returned.  For the purpose of this procedure, an
end of line consists of either a linefeed character, a carriage return
character, or a sequence of a carriage return character followed by
a linefeed character.  Implementations may also recognize other end of
line characters or sequences.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port and no
characters are immediately available, <code>read-line</code> will hang
waiting for input, even if the port is in non-blocking mode.
</p></dd></dl>

<dl>
<dt id="index-eof_002dobject_003f">standard procedure: <strong>eof-object?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-EOF-object"></span>
<span id="index-EOF-object_002c-predicate-for"></span>
<span id="index-end-of-file-object-_0028see-EOF-object_0029"></span>
<span id="index-file_002c-end_002dof_002dfile-marker-_0028see-EOF-object_0029"></span>
<p>Returns <code>#t</code> if <var>object</var> is an end-of-file object, otherwise
returns <code>#f</code>.  The precise set of end-of-file objects will vary
among implementations, but in any case no end-of-file object will ever
be an object that can be read in using <code>read</code>.
</p></dd></dl>

<dl>
<dt id="index-eof_002dobject">standard procedure: <strong>eof-object</strong></dt>
<dd><span id="index-EOF-object_002c-construction"></span>
<span id="index-construction_002c-of-EOF-object"></span>
<p>Returns an end-of-file object, not necessarily unique.
</p></dd></dl>

<dl>
<dt id="index-char_002dready_003f">standard procedure: <strong>char-ready?</strong> <em>[port]</em></dt>
<dd><span id="index-read_002dchar-2"></span>
<p>Returns <code>#t</code> if a character is ready on the textual input
<var>port</var> and returns <code>#f</code> otherwise.  If <code>char-ready?</code>
returns <code>#t</code> then the next <code>read-char</code> operation on the
given <var>port</var> is guaranteed not to hang.  If the <var>port</var> is at
end of file then <code>char-ready?</code> returns <code>#t</code>.
</p>
<p>Rationale: The <code>char-ready?</code> procedure exists to make it possible
for a program to accept characters from interactive ports without
getting stuck waiting for input.  Any input editors associated with
such ports must ensure that characters whose existence has been
asserted by <code>char-ready?</code> cannot be removed from the input.  If
<code>char-ready?</code> were to return <code>#f</code> at end of file, a port at
end of file would be indistinguishable from an interactive port that
has no ready characters.
</p></dd></dl>

<dl>
<dt id="index-read_002dstring">standard procedure: <strong>read-string</strong> <em>k [port]</em></dt>
<dd><p>Reads the next <var>k</var> characters, or as many as are available before
the end of file, from the textual input <var>port</var> into a newly
allocated string in left-to-right order and returns the string.  If no
characters are available before the end of file, an end-of-file object
is returned.
</p>
<p><em>Note</em>: MIT/GNU Scheme previously defined this procedure
differently, and this alternate usage is <strong>deprecated</strong>; please
use <code>read-delimited-string</code> instead.  For now, <code>read-string</code>
will redirect to <code>read-delimited-string</code> as needed, but this
redirection will be eliminated in a future release.
</p></dd></dl>

<dl>
<dt id="index-read_002dstring_0021">procedure: <strong>read-string!</strong> <em>string [port [start [end]]]</em></dt>
<dd><p>Reads the next end-start characters, or as many as are available
before the end of file, from the textual input <var>port</var> into
<var>string</var> in left-to-right order beginning at the <var>start</var>
position.  If <var>end</var> is not supplied, reads until the end of
<var>string</var> has been reached.  If <var>start</var> is not supplied, reads
beginning at position <code>0</code>.  Returns the number of characters
read.  If no characters are available, an end-of-file object is
returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive port in
non-blocking mode and no characters are immediately available,
<code>#f</code> is returned without any modification of <var>string</var>.
</p>
<p>However, if one or more characters are immediately available, the
region is filled using the available characters.  The procedure then
returns the number of characters filled in, without waiting for
further characters, even if the number of filled characters is less
than the size of the region.
</p></dd></dl>

<dl>
<dt id="index-read_002dsubstring_0021">obsolete procedure: <strong>read-substring!</strong> <em>string start end [port]</em></dt>
<dd><p>This procedure is <strong>deprecated</strong>; use <code>read-string!</code> instead.
</p></dd></dl>

<dl>
<dt id="index-read_002du8-1">standard procedure: <strong>read-u8</strong> <em>[port]</em></dt>
<dd><p>Returns the next byte available from the binary input <var>port</var>,
updating the <var>port</var> to point to the following byte.  If no more
bytes are available, an end-of-file object is returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port in
non-blocking mode and no characters are immediately available,
<code>read-u8</code> will return <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-peek_002du8">standard procedure: <strong>peek-u8</strong> <em>[port]</em></dt>
<dd><p>Returns the next byte available from the binary input <var>port</var>, but
<em>without</em> updating the <var>port</var> to point to the following byte.
If no more bytes are available, an end-of-file object is returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port in
non-blocking mode and no characters are immediately available,
<code>peek-u8</code> will return <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-u8_002dready_003f">standard procedure: <strong>u8-ready?</strong> <em>[port]</em></dt>
<dd><p>Returns <code>#t</code> if a byte is ready on the binary input <code>port</code>
and returns <code>#f</code> otherwise.  If <code>u8-ready?</code> returns
<code>#t</code> then the next <code>read-u8</code> operation on the given
<var>port</var> is guaranteed not to hang.  If the <var>port</var> is at end of
file then <code>u8-ready?</code> returns <code>#t</code>.
</p></dd></dl>

<dl>
<dt id="index-read_002dbytevector">standard procedure: <strong>read-bytevector</strong> <em>k [port]</em></dt>
<dd><p>Reads the next <var>k</var> bytes, or as many as are available before the
end of file, from the binary input <var>port</var> into a newly allocated
bytevector in left-to-right order and returns the bytevector.  If no
bytes are available before the end of file, an end-of-file object is
returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port in
non-blocking mode and no characters are immediately available,
<code>read-bytevector</code> will return <code>#f</code>.
</p>
<p>However, if one or more bytes are immediately available, they are read
and returned as a bytevector, without waiting for further bytes, even
if the number of bytes is less than <var>k</var>.
</p></dd></dl>

<dl>
<dt id="index-read_002dbytevector_0021">standard procedure: <strong>read-bytevector!</strong> <em>bytevector [port [start [end]]]</em></dt>
<dd><p>Reads the next end-start bytes, or as many as are available before the
end of file, from the binary input <var>port</var> into <var>bytevector</var> in
left-to-right order beginning at the <var>start</var> position.  If
<var>end</var> is not supplied, reads until the end of <var>bytevector</var> has
been reached.  If <var>start</var> is not supplied, reads beginning at
position <code>0</code>.  Returns the number of bytes read.  If no bytes are
available, an end-of-file object is returned.
</p>
<p>In MIT/GNU Scheme, if <var>port</var> is an interactive input port in
non-blocking mode and no characters are immediately available,
<code>read-bytevector!</code> will return <code>#f</code>.
</p>
<p>However, if one or more bytes are immediately available, the region is
filled using the available bytes.  The procedure then returns the
number of bytes filled in, without waiting for further bytes, even if
the number of filled bytes is less than the size of the region.
</p></dd></dl>

<dl>
<dt id="index-read_002ddelimited_002dstring">procedure: <strong>read-delimited-string</strong> <em>char-set [port]</em></dt>
<dd><span id="index-string_002c-input-from-port"></span>
<p>Reads characters from <var>port</var> until it finds a terminating
character that is a member of <var>char-set</var> (see <a href="Character-Sets.html">Character Sets</a>)
or encounters end of file.  The port is updated to point to the
terminating character, or to end of file if no terminating character
was found.  <code>read-delimited-string</code> returns the characters, up to
but excluding the terminating character, as a newly allocated string.
</p>
<p>This procedure ignores the blocking mode of the port, blocking
unconditionally until it sees either a delimiter or end of file.  If end
of file is encountered before any characters are read, an end-of-file
object is returned.
</p>
<span id="index-read_002dchar-3"></span>
<p>On many input ports, this operation is significantly faster than the
following equivalent code using <code>peek-char</code> and <code>read-char</code>:
</p>
<div class="example">
<pre class="example">(define (read-delimited-string char-set port)
  (let ((char (peek-char port)))
    (if (eof-object? char)
        char
        (list-&gt;string
         (let loop ((char char))
           (if (or (eof-object? char)
                   (char-in-set? char char-set))
               '()
               (begin
                 (read-char port)
                 (cons char
                       (loop (peek-char port))))))))))
</pre></div>
</dd></dl>

<span id="reader_002dcontrols"></span><span id="Reader-Controls"></span><h4 class="subsection">14.5.1 Reader Controls</h4>

<p>The following parameters control the behavior of the <code>read</code>
procedure.
</p>
<dl>
<dt id="index-param_003areader_002dradix">parameter: <strong>param:reader-radix</strong></dt>
<dd><p>This parameter defines the radix used by the reader when it parses
numbers.  This is similar to passing a radix argument to
<code>string-&gt;number</code>.  The value of the parameter must be one of
<code>2</code>, <code>8</code>, <code>10</code>, or <code>16</code>; an error is signaled if
the parameter is bound to any other value.
</p>
<p>Note that much of the number syntax is invalid for radixes other than
<code>10</code>.  The reader detects cases where such invalid syntax is used
and signals an error.  However, problems can still occur when
<code>param:reader-radix</code> is bound to <code>16</code>, because syntax that
normally denotes symbols can now denote numbers (e.g. <code>abc</code>).
Because of this, it is usually undesirable to bind this parameter to
anything other than the default.
</p>
<p>The default value of this parameter is <code>10</code>.
</p></dd></dl>

<dl>
<dt id="index-param_003areader_002dfold_002dcase_003f">parameter: <strong>param:reader-fold-case?</strong></dt>
<dd><p>This parameter controls whether the parser folds the case of symbols,
character names, and certain other syntax.  If it is bound to its
default value of <code>#t</code>, symbols read by the parser are case-folded
prior to being interned.  Otherwise, symbols are interned without
folding.
</p>
<p>At present, it is a bad idea to use this feature, as it doesn&rsquo;t really
make Scheme case-sensitive, and therefore can break features of the
Scheme runtime that depend on case-folded symbols.  Instead, use the
<code>#!fold-case</code> or <code>#!no-fold-case</code> markers in your code.
</p></dd></dl>

<dl>
<dt id="index-_002aparser_002dradix_002a">obsolete variable: <strong>*parser-radix*</strong></dt>
<dt id="index-_002aparser_002dcanonicalize_002dsymbols_003f_002a">obsolete variable: <strong>*parser-canonicalize-symbols?*</strong></dt>
<dd><p>These variables are <strong>deprecated</strong>; instead use the corresponding
parameter objects.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Output-Procedures.html" accesskey="n" rel="next">Output Procedures</a>, Previous: <a href="Bytevector-Ports.html" accesskey="p" rel="prev">Bytevector Ports</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
