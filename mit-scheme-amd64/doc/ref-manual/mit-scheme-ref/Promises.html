<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Promises (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Promises (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Promises (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Streams.html" rel="next" title="Streams">
<link href="Records.html" rel="prev" title="Records">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Promises"></span><div class="header">
<p>
Next: <a href="Streams.html" accesskey="n" rel="next">Streams</a>, Previous: <a href="Records.html" accesskey="p" rel="prev">Records</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Promises-1"></span><h3 class="section">10.5 Promises</h3>

<dl>
<dt id="index-delay-1">special form: <strong>delay</strong> <em>expression</em></dt>
<dd><span id="index-promise-_0028defn_0029"></span>
<span id="index-promise_002c-construction"></span>
<span id="index-construction_002c-of-promise"></span>
<span id="index-lazy-evaluation-_0028defn_0029"></span>
<span id="index-call-by-need-evaluation-_0028defn_0029"></span>
<span id="index-evaluation_002c-lazy-_0028defn_0029"></span>
<span id="index-evaluation_002c-call-by-need-_0028defn_0029"></span>
<p>The <code>delay</code> construct is used together with the procedure
<code>force</code> to implement <em>lazy evaluation</em> or <em>call by need</em>.
<code>(delay <var>expression</var>)</code> returns an object called a <em>promise</em>
which at some point in the future may be asked (by the <code>force</code>
procedure) to evaluate <var>expression</var> and deliver the resulting value.
</p></dd></dl>

<dl>
<dt id="index-force">procedure: <strong>force</strong> <em>promise</em></dt>
<dd><span id="index-promise_002c-forcing"></span>
<span id="index-forcing_002c-of-promise"></span>
<span id="index-memoization_002c-of-promise"></span>
<p>Forces the value of <em>promise</em>.  If no value has been computed for
the promise, then a value is computed and returned.  The value of the
promise is cached (or &ldquo;memoized&rdquo;) so that if it is forced a second
time, the previously computed value is returned without any
recomputation.
</p>
<div class="example">
<pre class="example">(force (delay (+ 1 2)))                 &rArr;  3

(let ((p (delay (+ 1 2))))
  (list (force p) (force p)))           &rArr;  (3 3)
</pre><pre class="example">

</pre><pre class="example">(define head car)

(define tail
  (lambda (stream)
    (force (cdr stream))))
</pre><pre class="example">

</pre><pre class="example">(define a-stream
  (letrec ((next
            (lambda (n)
              (cons n (delay (next (+ n 1)))))))
    (next 0)))

(head (tail (tail a-stream)))           &rArr;  2
</pre></div>
</dd></dl>

<dl>
<dt id="index-promise_003f">procedure: <strong>promise?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-promise"></span>
<p>Returns <code>#t</code> if <var>object</var> is a promise; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-promise_002dforced_003f">procedure: <strong>promise-forced?</strong> <em>promise</em></dt>
<dd><p>Returns <code>#t</code> if <var>promise</var> has been forced and its value cached;
otherwise returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-promise_002dvalue">procedure: <strong>promise-value</strong> <em>promise</em></dt>
<dd><p>If <var>promise</var> has been forced and its value cached, this procedure
returns the cached value.  Otherwise, an error is signalled.
</p></dd></dl>

<p><code>force</code> and <code>delay</code> are mainly intended for programs written
in functional style.  The following examples should not be considered to
illustrate good programming style, but they illustrate the property that
the value of a promise is computed at most once.
</p>
<div class="example">
<pre class="example">(define count 0)

(define p
  (delay
   (begin
     (set! count (+ count 1))
     (* x 3))))

(define x 5)
</pre><pre class="example">

</pre><pre class="example">count                                   &rArr;  0
p                                       &rArr;  #[promise 54]
(force p)                               &rArr;  15
p                                       &rArr;  #[promise 54]
count                                   &rArr;  1
(force p)                               &rArr;  15
count                                   &rArr;  1
</pre></div>

<p>Here is a possible implementation of <code>delay</code> and <code>force</code>.  We
define the expression
</p>
<div class="example">
<pre class="example">(delay <var>expression</var>)
</pre></div>

<p>to have the same meaning as the procedure call
</p>
<div class="example">
<pre class="example">(make-promise (lambda () <var>expression</var>))
</pre></div>

<p>where <code>make-promise</code> is defined as follows:
</p>
<div class="example">
<pre class="example">(define make-promise
  (lambda (proc)
    (let ((already-run? #f)
          (result #f))
      (lambda ()
        (cond ((not already-run?)
               (set! result (proc))
               (set! already-run? #t)))
        result))))
</pre></div>

<p>Promises are implemented here as procedures of no arguments, and
<code>force</code> simply calls its argument.
</p>
<div class="example">
<pre class="example">(define force
  (lambda (promise)
    (promise)))
</pre></div>

<p>Various extensions to this semantics of <code>delay</code> and <code>force</code>
are supported in some implementations (none of these are currently
supported in MIT/GNU Scheme):
</p>
<ul>
<li> Calling <code>force</code> on an object that is not a promise may simply
return the object.

</li><li> It may be the case that there is no means by which a promise can be
operationally distinguished from its forced value.  That is, expressions
like the following may evaluate to either <code>#t</code> or <code>#f</code>,
depending on the implementation:

<div class="example">
<pre class="example">(eqv? (delay 1) 1)              &rArr;  <span class="roman">unspecified</span>
(pair? (delay (cons 1 2)))      &rArr;  <span class="roman">unspecified</span>
</pre></div>

</li><li> Some implementations will implement &ldquo;implicit forcing&rdquo;, where the
value of a promise is forced by primitive procedures like <code>car</code> and
<code>+</code>:

<div class="example">
<pre class="example">(+ (delay (* 3 7)) 13)          &rArr;  34
</pre></div>
</li></ul>

<hr>
<div class="header">
<p>
Next: <a href="Streams.html" accesskey="n" rel="next">Streams</a>, Previous: <a href="Records.html" accesskey="p" rel="prev">Records</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
