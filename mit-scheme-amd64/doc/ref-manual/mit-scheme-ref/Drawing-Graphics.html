<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Drawing Graphics (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Drawing Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Drawing Graphics (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Graphics.html" rel="up" title="Graphics">
<link href="Characteristics-of-Graphics-Output.html" rel="next" title="Characteristics of Graphics Output">
<link href="Coordinates-for-Graphics.html" rel="prev" title="Coordinates for Graphics">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Drawing-Graphics"></span><div class="header">
<p>
Next: <a href="Characteristics-of-Graphics-Output.html" accesskey="n" rel="next">Characteristics of Graphics Output</a>, Previous: <a href="Coordinates-for-Graphics.html" accesskey="p" rel="prev">Coordinates for Graphics</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Drawing-Graphics-1"></span><h3 class="section">17.3 Drawing Graphics</h3>
<span id="index-graphics_002c-drawing"></span>

<p>The procedures in this section provide the basic drawing capabilities of
Scheme&rsquo;s graphics system.
</p>
<dl>
<dt id="index-graphics_002dclear">procedure: <strong>graphics-clear</strong> <em>graphics-device</em></dt>
<dd><p>Clears the display of <var>graphics-device</var>.  Unaffected by the current
drawing mode.
</p></dd></dl>

<dl>
<dt id="index-graphics_002ddraw_002dpoint">procedure: <strong>graphics-draw-point</strong> <em>graphics-device x y</em></dt>
<dd><p>Draws a single point on <var>graphics-device</var> at the virtual coordinates
given by <var>x</var> and <var>y</var>, using the current drawing mode.
</p></dd></dl>

<dl>
<dt id="index-graphics_002derase_002dpoint">procedure: <strong>graphics-erase-point</strong> <em>graphics-device x y</em></dt>
<dd><p>Erases a single point on <var>graphics-device</var> at the virtual
coordinates given by <var>x</var> and <var>y</var>.  This procedure is unaffected
by the current drawing mode.
</p></dd></dl>

<p>This is equivalent to
</p>
<div class="example">
<pre class="example">(lambda (device x y)
  (graphics-bind-drawing-mode device 0
    (lambda ()
      (graphics-draw-point device x y))))
</pre></div>

<dl>
<dt id="index-graphics_002ddraw_002dline">procedure: <strong>graphics-draw-line</strong> <em>graphics-device x-start y-start x-end y-end</em></dt>
<dd><p><var>X-start</var>, <var>y-start</var>, <var>x-end</var>, and <var>y-end</var> must be real
numbers.  Draws a line on <var>graphics-device</var> that connects the points
(<var>x-start</var>, <var>y-start</var>) and (<var>x-end</var>, <var>y-end</var>).  The line
is drawn using the current drawing mode and line style.
</p></dd></dl>

<dl>
<dt id="index-graphics_002ddraw_002dtext">procedure: <strong>graphics-draw-text</strong> <em>graphics-device x y string</em></dt>
<dd><p>Draws the characters of <var>string</var> at the point (<var>x</var>, <var>y</var>) on
<var>graphics-device</var>, using the current drawing mode.  The
characteristics of the characters drawn are device-dependent, but all
devices are initialized so that the characters are drawn upright, from
left to right, with the leftmost edge of the leftmost character at
<var>x</var>, and the baseline of the characters at <var>y</var>.
</p></dd></dl>

<span id="index-graphics_002c-cursor-_0028defn_0029"></span>
<span id="index-cursor_002c-graphics-_0028defn_0029"></span>
<p>The following two procedures provide an alternate mechanism for drawing
lines, which is more akin to using a plotter.  They maintain a
<em>cursor</em>, which can be positioned to a particular point and then
dragged to another point, producing a line.  Sequences of connected line
segments can be drawn by dragging the cursor from point to point.
</p>
<p>Many graphics operations have an unspecified effect on the cursor.  The
following exceptions are guaranteed to leave the cursor unaffected:
</p>
<div class="example">
<pre class="example">graphics-device-coordinate-limits
graphics-coordinate-limits
graphics-enable-buffering
graphics-disable-buffering
graphics-flush
graphics-bind-drawing-mode
graphics-set-drawing-mode
graphics-bind-line-style
graphics-set-line-style
</pre></div>

<p>The initial state of the cursor is unspecified.
</p>
<dl>
<dt id="index-graphics_002dmove_002dcursor">procedure: <strong>graphics-move-cursor</strong> <em>graphics-device x y</em></dt>
<dd><p>Moves the cursor for <var>graphics-device</var> to the point (<var>x</var>,
<var>y</var>).  The contents of the device&rsquo;s display are unchanged.
</p></dd></dl>

<dl>
<dt id="index-graphics_002ddrag_002dcursor">procedure: <strong>graphics-drag-cursor</strong> <em>graphics-device x y</em></dt>
<dd><p>Draws a line from <var>graphics-device</var>&rsquo;s cursor to the point (<var>x</var>,
<var>y</var>), simultaneously moving the cursor to that point.  The line is
drawn using the current drawing mode and line style.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Characteristics-of-Graphics-Output.html" accesskey="n" rel="next">Characteristics of Graphics Output</a>, Previous: <a href="Coordinates-for-Graphics.html" accesskey="p" rel="prev">Coordinates for Graphics</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
