<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>*Matcher (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="*Matcher (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="*Matcher (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Parser-Language.html" rel="up" title="Parser Language">
<link href="_002aParser.html" rel="next" title="*Parser">
<link href="Parser-Language.html" rel="prev" title="Parser Language">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="g_t_002aMatcher"></span><div class="header">
<p>
Next: <a href="_002aParser.html" accesskey="n" rel="next">*Parser</a>, Previous: <a href="Parser-Language.html" accesskey="p" rel="prev">Parser Language</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="g_t_002aMatcher-1"></span><h4 class="subsection">14.14.1 *Matcher</h4>

<span id="index-Matcher-language"></span>
<span id="index-Matcher-procedure"></span>
<p>The <em>matcher language</em> is a declarative language for specifying a
<em>matcher procedure</em>.  A matcher procedure is a procedure that
accepts a single parser-buffer argument and returns a boolean value
indicating whether the match it performs was successful.  If the match
succeeds, the internal pointer of the parser buffer is moved forward
over the matched text.  If the match fails, the internal pointer is
unchanged.
</p>
<p>For example, here is a matcher procedure that matches the character
&lsquo;<samp>a</samp>&rsquo;:
</p>
<div class="example">
<pre class="example">(lambda (b) (match-parser-buffer-char b #\a))
</pre></div>

<p>Here is another example that matches two given characters, <var>c1</var>
and <var>c2</var>, in sequence:
</p>
<div class="example">
<pre class="example">(lambda (b)
  (let ((p (get-parser-buffer-pointer b)))
    (if (match-parser-buffer-char b <var>c1</var>)
        (if (match-parser-buffer-char b <var>c2</var>)
            #t
            (begin
              (set-parser-buffer-pointer! b p)
              #f))
        #f)))
</pre></div>

<p>This is code is clear, but has lots of details that get in the way of
understanding what it is doing.  Here is the same example in the
matcher language:
</p>
<div class="example">
<pre class="example">(*matcher (seq (char <var>c1</var>) (char <var>c2</var>)))
</pre></div>

<p>This is much simpler and more intuitive.  And it generates virtually
the same code:
</p>
<div class="example">
<pre class="example">(pp (*matcher (seq (char c1) (char c2))))
-| (lambda (#[b1])
-|   (let ((#[p1] (get-parser-buffer-pointer #[b1])))
-|     (and (match-parser-buffer-char #[b1] c1)
-|          (if (match-parser-buffer-char #[b1] c2)
-|              #t
-|              (begin
-|                (set-parser-buffer-pointer! #[b1] #[p1])
-|                #f)))))
</pre></div>

<p>Now that we have seen an example of the language, it&rsquo;s time to look at
the detail.  The <code>*matcher</code> special form is the interface between
the matcher language and Scheme.
</p>
<dl>
<dt id="index-_002amatcher">special form: <strong>*matcher</strong> <em>mexp</em></dt>
<dd><p>The operand <var>mexp</var> is an expression in the matcher language.  The
<code>*matcher</code> expression expands into Scheme code that implements a
matcher procedure.
</p></dd></dl>

<p>Here are the predefined matcher expressions.  New matcher expressions
can be defined using the macro facility (see <a href="Parser_002dlanguage-Macros.html">Parser-language Macros</a>).  We will start with the primitive expressions.
</p>
<dl>
<dt id="index-char">matcher expression: <strong>char</strong> <em>expression</em></dt>
<dt id="index-char_002dci-1">matcher expression: <strong>char-ci</strong> <em>expression</em></dt>
<dt id="index-not_002dchar">matcher expression: <strong>not-char</strong> <em>expression</em></dt>
<dt id="index-not_002dchar_002dci">matcher expression: <strong>not-char-ci</strong> <em>expression</em></dt>
<dd><p>These expressions match a given character.  In each case, the
<var>expression</var> operand is a Scheme expression that must evaluate to
a character at run time.  The &lsquo;<samp>-ci</samp>&rsquo; expressions do
case-insensitive matching.  The &lsquo;<samp>not-</samp>&rsquo; expressions match any
character other than the given one.
</p></dd></dl>

<dl>
<dt id="index-string-1">matcher expression: <strong>string</strong> <em>expression</em></dt>
<dt id="index-string_002dci-1">matcher expression: <strong>string-ci</strong> <em>expression</em></dt>
<dd><p>These expressions match a given string.  The <var>expression</var> operand
is a Scheme expression that must evaluate to a string at run time.
The <code>string-ci</code> expression does case-insensitive matching.
</p></dd></dl>

<dl>
<dt id="index-char_002dset-1">matcher expression: <strong>char-set</strong> <em>expression</em></dt>
<dd><p>These expressions match a single character that is a member of a given
character set.  The <var>expression</var> operand is a Scheme expression
that must evaluate to a character set at run time.
</p></dd></dl>

<dl>
<dt id="index-end_002dof_002dinput">matcher expression: <strong>end-of-input</strong></dt>
<dd><p>The <code>end-of-input</code> expression is successful only when there are
no more characters available to be matched.
</p></dd></dl>

<dl>
<dt id="index-discard_002dmatched">matcher expression: <strong>discard-matched</strong></dt>
<dd><p>The <code>discard-matched</code> expression always successfully matches the
null string.  However, it isn&rsquo;t meant to be used as a matching
expression; it is used for its effect.  <code>discard-matched</code> causes
all of the buffered text prior to this point to be discarded (i.e.
it calls <code>discard-parser-buffer-head!</code> on the parser buffer).
</p>
<p>Note that <code>discard-matched</code> may not be used in certain places in
a matcher expression.  The reason for this is that it deliberately
discards information needed for backtracking, so it may not be used in
a place where subsequent backtracking will need to back over it.  As a
rule of thumb, use <code>discard-matched</code> only in the last operand of
a <code>seq</code> or <code>alt</code> expression (including any <code>seq</code> or
<code>alt</code> expressions in which it is indirectly contained).
</p></dd></dl>

<p>In addition to the above primitive expressions, there are two
convenient abbreviations.  A character literal (e.g. &lsquo;<samp>#\A</samp>&rsquo;) is
a legal primitive expression, and is equivalent to a <code>char</code>
expression with that literal as its operand (e.g. &lsquo;<samp>(char
#\A)</samp>&rsquo;).  Likewise, a string literal is equivalent to a <code>string</code>
expression (e.g. &lsquo;<samp>(string &quot;abc&quot;)</samp>&rsquo;).
</p>
<p>Next there are several combinator expressions.  These closely
correspond to similar combinators in regular expressions.  Parameters
named <var>mexp</var> are arbitrary expressions in the matcher language.
</p>
<dl>
<dt id="index-seq-1">matcher expression: <strong>seq</strong> <em>mexp &hellip;</em></dt>
<dd><p>This matches each <var>mexp</var> operand in sequence.  For example,
</p>
<div class="example">
<pre class="example">(seq (char-set char-set:alphabetic)
     (char-set char-set:numeric))
</pre></div>

<p>matches an alphabetic character followed by a numeric character, such
as &lsquo;<samp>H4</samp>&rsquo;.
</p>
<p>Note that if there are no <var>mexp</var> operands, the <code>seq</code>
expression successfully matches the null string.
</p></dd></dl>

<dl>
<dt id="index-alt-1">matcher expression: <strong>alt</strong> <em>mexp &hellip;</em></dt>
<dd><p>This attempts to match each <var>mexp</var> operand in order from left to
right.  The first one that successfully matches becomes the match for
the entire <code>alt</code> expression.
</p>
<p>The <code>alt</code> expression participates in backtracking.  If one of the
<var>mexp</var> operands matches, but the overall match in which this
expression is embedded fails, the backtracking mechanism will cause
the <code>alt</code> expression to try the remaining <var>mexp</var> operands.
For example, if the expression
</p>
<div class="example">
<pre class="example">(seq (alt &quot;ab&quot; &quot;a&quot;) &quot;b&quot;)
</pre></div>

<p>is matched against the text &lsquo;<samp>abc</samp>&rsquo;, the <code>alt</code> expression will
initially match its first operand.  But it will then fail to match the
second operand of the <code>seq</code> expression.  This will cause the
<code>alt</code> to be restarted, at which time it will match &lsquo;<samp>a</samp>&rsquo;, and
the overall match will succeed.
</p>
<p>Note that if there are no <var>mexp</var> operands, the <code>alt</code> match
will always fail.
</p></dd></dl>

<dl>
<dt id="index-_002a-3">matcher expression: <strong>*</strong> <em>mexp</em></dt>
<dd><p>This matches zero or more occurrences of the <var>mexp</var> operand.
(Consequently this match always succeeds.)
</p>
<p>The <code>*</code> expression participates in backtracking; if it matches
<var>N</var> occurrences of <var>mexp</var>, but the overall match fails, it
will backtrack to <var>N-1</var> occurrences and continue.  If the overall
match continues to fail, the <code>*</code> expression will continue to
backtrack until there are no occurrences left.
</p></dd></dl>

<dl>
<dt id="index-_002b-4">matcher expression: <strong>+</strong> <em>mexp</em></dt>
<dd><p>This matches one or more occurrences of the <var>mexp</var> operand.  It is
equivalent to
</p>
<div class="example">
<pre class="example">(seq <var>mexp</var> (* <var>mexp</var>))
</pre></div>
</dd></dl>

<dl>
<dt id="index-_003f-1">matcher expression: <strong>?</strong> <em>mexp</em></dt>
<dd><p>This matches zero or one occurrences of the <var>mexp</var> operand.  It is
equivalent to
</p>
<div class="example">
<pre class="example">(alt <var>mexp</var> (seq))
</pre></div>
</dd></dl>

<dl>
<dt id="index-sexp">matcher expression: <strong>sexp</strong> <em>expression</em></dt>
<dd><p>The <code>sexp</code> expression allows arbitrary Scheme code to be embedded
inside a matcher.  The <var>expression</var> operand must evaluate to a
matcher procedure at run time; the procedure is called to match the
parser buffer.  For example,
</p>
<div class="example">
<pre class="example">(*matcher
 (seq &quot;a&quot;
      (sexp parse-foo)
      &quot;b&quot;))
</pre></div>

<p>expands to
</p>
<div class="example">
<pre class="example">(lambda (#[b1])
  (let ((#[p1] (get-parser-buffer-pointer #[b1])))
    (and (match-parser-buffer-char #[b1] #\a)
         (if (parse-foo #[b1])
             (if (match-parser-buffer-char #[b1] #\b)
                 #t
                 (begin
                   (set-parser-buffer-pointer! #[b1] #[p1])
                   #f))
             (begin
               (set-parser-buffer-pointer! #[b1] #[p1])
               #f)))))
</pre></div>

<p>The case in which <var>expression</var> is a symbol is so common that it
has an abbreviation: &lsquo;<samp>(sexp <var>symbol</var>)</samp>&rsquo; may be abbreviated as
just <var>symbol</var>.
</p></dd></dl>

<dl>
<dt id="index-with_002dpointer">matcher expression: <strong>with-pointer</strong> <em>identifier mexp</em></dt>
<dd><p>The <code>with-pointer</code> expression fetches the parser buffer&rsquo;s
internal pointer (using <code>get-parser-buffer-pointer</code>), binds it to
<var>identifier</var>, and then matches the pattern specified by
<var>mexp</var>.  <var>Identifier</var> must be a symbol.
</p>
<p>This is meant to be used on conjunction with <code>sexp</code>, as a way to
capture a pointer to a part of the input stream that is outside the
<code>sexp</code> expression.  An example of the use of <code>with-pointer</code>
appears above (see <a href="Parser-Language.html#with_002dpointer-example">with-pointer example</a>).
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="_002aParser.html" accesskey="n" rel="next">*Parser</a>, Previous: <a href="Parser-Language.html" accesskey="p" rel="prev">Parser Language</a>, Up: <a href="Parser-Language.html" accesskey="u" rel="up">Parser Language</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
