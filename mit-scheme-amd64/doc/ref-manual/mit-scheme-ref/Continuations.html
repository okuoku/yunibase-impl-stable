<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Continuations (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Continuations (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Continuations (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Procedures.html" rel="up" title="Procedures">
<link href="Application-Hooks.html" rel="next" title="Application Hooks">
<link href="Primitive-Procedures.html" rel="prev" title="Primitive Procedures">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Continuations"></span><div class="header">
<p>
Next: <a href="Application-Hooks.html" accesskey="n" rel="next">Application Hooks</a>, Previous: <a href="Primitive-Procedures.html" accesskey="p" rel="prev">Primitive Procedures</a>, Up: <a href="Procedures.html" accesskey="u" rel="up">Procedures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Continuations-1"></span><h3 class="section">12.4 Continuations</h3>

<dl>
<dt id="index-call_002dwith_002dcurrent_002dcontinuation">procedure: <strong>call-with-current-continuation</strong> <em>procedure</em></dt>
<dd><span id="index-continuation"></span>
<span id="index-construction_002c-of-continuation"></span>
<span id="index-procedure_002c-escape-_0028defn_0029"></span>
<span id="index-escape-procedure-_0028defn_0029"></span>
<p><var>Procedure</var> must be a procedure of one argument.  Packages up the
current continuation (see below) as an <em>escape procedure</em> and passes
it as an argument to <var>procedure</var>.  The escape procedure is a Scheme
procedure of one argument that, if it is later passed a value, will
ignore whatever continuation is in effect at that later time and will
give the value instead to the continuation that was in effect when the
escape procedure was created.  The escape procedure created by
<code>call-with-current-continuation</code> has unlimited extent just like any
other procedure in Scheme.  It may be stored in variables or data
structures and may be called as many times as desired.
</p>
<p>The following examples show only the most common uses of this procedure.
If all real programs were as simple as these examples, there would be no
need for a procedure with the power of
<code>call-with-current-continuation</code>.
</p>
<div class="example">
<pre class="example">(call-with-current-continuation
  (lambda (exit)
    (for-each (lambda (x)
                (if (negative? x)
                    (exit x)))
              '(54 0 37 -3 245 19))
    #t))                                &rArr;  -3
</pre><pre class="example">

</pre><pre class="example">(define list-length
  (lambda (obj)
    (call-with-current-continuation
      (lambda (return)
        (letrec ((r
                  (lambda (obj)
                    (cond ((null? obj) 0)
                          ((pair? obj) (+ (r (cdr obj)) 1))
                          (else (return #f))))))
          (r obj))))))
(list-length '(1 2 3 4))                &rArr;  4
(list-length '(a b . c))                &rArr;  #f
</pre></div>

<span id="index-non_002dlocal-exit"></span>
<span id="index-exit_002c-non_002dlocal"></span>
<p>A common use of <code>call-with-current-continuation</code> is for structured,
non-local exits from loops or procedure bodies, but in fact
<code>call-with-current-continuation</code> is quite useful for implementing a
wide variety of advanced control structures.
</p>
<p>Whenever a Scheme expression is evaluated a continuation exists that
wants the result of the expression.  The continuation represents an
entire (default) future for the computation.  If the expression is
evaluated at top level, for example, the continuation will take the
result, print it on the screen, prompt for the next input, evaluate it,
and so on forever.  Most of the time the continuation includes actions
specified by user code, as in a continuation that will take the result,
multiply it by the value stored in a local variable, add seven, and give
the answer to the top-level continuation to be printed.  Normally these
ubiquitous continuations are hidden behind the scenes and programmers
don&rsquo;t think much about them.  On the rare occasions that you may need to
deal explicitly with continuations,
<code>call-with-current-continuation</code> lets you do so by creating a
procedure that acts just like the current continuation.
</p></dd></dl>

<dl>
<dt id="index-continuation_003f">procedure: <strong>continuation?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-continuation"></span>
<p>Returns <code>#t</code> if <var>object</var> is a continuation; otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-within_002dcontinuation">procedure: <strong>within-continuation</strong> <em>continuation thunk</em></dt>
<dd><span id="index-continuation_002c-alternate-invocation"></span>
<span id="index-escape-procedure_002c-alternate-invocation"></span>
<p><var>Thunk</var> must be a procedure of no arguments.  Conceptually,<br>
<code>within-continuation</code> invokes <var>continuation</var> on the result of
invoking <var>thunk</var>, but <var>thunk</var> is executed in the dynamic state
of <var>continuation</var>.  In other words, the &ldquo;current&rdquo; continuation is
abandoned before <var>thunk</var> is invoked.
</p></dd></dl>

<dl>
<dt id="index-dynamic_002dwind">procedure: <strong>dynamic-wind</strong> <em>before thunk after</em></dt>
<dd><p>Calls <var>thunk</var> without arguments, returning the result(s) of this
call.  <var>Before</var> and <var>after</var> are called, also without arguments,
as required by the following rules.  Note that in the absence of calls to
continuations captured using <code>call-with-current-continuation</code> the
three arguments are called once each, in order.  <var>Before</var> is called
whenever execution enters the dynamic extent of the call to <var>thunk</var>
and <var>after</var> is called whenever it exits that dynamic extent.  The
dynamic extent of a procedure call is the period between when the call
is initiated and when it returns.  In Scheme, because of
<code>call-with-current-continuation</code>, the dynamic extent of a call may
not be a single, connected time period.  It is defined as follows:
</p>
<ul>
<li> The dynamic extent is entered when execution of the body of the called
procedure begins.

</li><li> The dynamic extent is also entered when execution is not within the
dynamic extent and a continuation is invoked that was captured (using
<code>call-with-current-continuation</code>) during the dynamic extent.

</li><li> It is exited when the called procedure returns.

</li><li> It is also exited when execution is within the dynamic extent and a
continuation is invoked that was captured while not within the dynamic
extent.
</li></ul>

<p>If a second call to <code>dynamic-wind</code> occurs within the dynamic extent
of the call to <var>thunk</var> and then a continuation is invoked in such a
way that the <var>after</var>s from these two invocations of
<code>dynamic-wind</code> are both to be called, then the <var>after</var>
associated with the second (inner) call to <code>dynamic-wind</code> is called
first.
</p>
<p>If a second call to <code>dynamic-wind</code> occurs within the dynamic extent
of the call to <var>thunk</var> and then a continuation is invoked in such a
way that the <var>before</var>s from these two invocations of
<code>dynamic-wind</code> are both to be called, then the <var>before</var>
associated with the first (outer) call to <code>dynamic-wind</code> is called
first.
</p>
<p>If invoking a continuation requires calling the <var>before</var> from one
call to <code>dynamic-wind</code> and the <var>after</var> from another, then the
<var>after</var> is called first.
</p>
<p>The effect of using a captured continuation to enter or exit the dynamic
extent of a call to <var>before</var> or <var>after</var> is undefined.
</p>
<div class="example">
<pre class="example">(let ((path '())
      (c #f))
  (let ((add (lambda (s)
               (set! path (cons s path)))))
    (dynamic-wind
      (lambda () (add 'connect))
      (lambda ()
        (add (call-with-current-continuation
               (lambda (c0)
                 (set! c c0)
                 'talk1))))
      (lambda () (add 'disconnect)))
    (if (&lt; (length path) 4)
        (c 'talk2)
        (reverse path))))

&rArr; (connect talk1 disconnect connect talk2 disconnect)
</pre></div>
</dd></dl>

<p>The following two procedures support multiple values.
</p>
<dl>
<dt id="index-call_002dwith_002dvalues">procedure: <strong>call-with-values</strong> <em>thunk procedure</em></dt>
<dd><span id="index-multiple-values_002c-from-procedure"></span>
<span id="index-values_002c-multiple"></span>
<p><var>Thunk</var> must be a procedure of no arguments, and <var>procedure</var>
must be a procedure.  <var>Thunk</var> is invoked with a continuation that
expects to receive multiple values; specifically, the continuation
expects to receive the same number of values that <var>procedure</var>
accepts as arguments.  <var>Thunk</var> must return multiple values using the
<code>values</code> procedure.  Then <var>procedure</var> is called with the
multiple values as its arguments.  The result yielded by <var>procedure</var>
is returned as the result of <code>call-with-values</code>.
</p></dd></dl>

<dl>
<dt id="index-values">procedure: <strong>values</strong> <em>object &hellip;</em></dt>
<dd><p>Returns multiple values.  The continuation in effect when this procedure
is called must be a multiple-value continuation that was created by
<code>call-with-values</code>.  Furthermore it must accept as many values as
there are <var>object</var>s.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Application-Hooks.html" accesskey="n" rel="next">Application Hooks</a>, Previous: <a href="Primitive-Procedures.html" accesskey="p" rel="prev">Primitive Procedures</a>, Up: <a href="Procedures.html" accesskey="u" rel="up">Procedures</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
