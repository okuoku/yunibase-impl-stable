<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>String Ports (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="String Ports (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="String Ports (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Input_002fOutput.html" rel="up" title="Input/Output">
<link href="Bytevector-Ports.html" rel="next" title="Bytevector Ports">
<link href="File-Ports.html" rel="prev" title="File Ports">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="String-Ports"></span><div class="header">
<p>
Next: <a href="Bytevector-Ports.html" accesskey="n" rel="next">Bytevector Ports</a>, Previous: <a href="File-Ports.html" accesskey="p" rel="prev">File Ports</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="String-Ports-1"></span><h3 class="section">14.3 String Ports</h3>

<span id="index-string_002c-input-and-output-ports"></span>
<span id="index-port_002c-string"></span>
<span id="index-input-port_002c-string"></span>
<span id="index-output-port_002c-string"></span>
<span id="index-I_002fO_002c-to-strings"></span>
<p>This section describes textual input ports that read their input from
given strings, and textual output ports that accumulate their output
and return it as a string.
</p>
<dl>
<dt id="index-open_002dinput_002dstring">standard procedure: <strong>open-input-string</strong> <em>string [start [end]]</em></dt>
<dd><p>Takes a string and returns a textual input port that delivers
characters from the string.  If the string is modified, the effect is
unspecified.
</p>
<p>The optional arguments <var>start</var> and <var>end</var> may be used to specify
that the string port delivers characters from a substring of
<var>string</var>; if not given, <var>start</var> defaults to <code>0</code> and
<var>end</var> defaults to <code>(string-length <var>string</var>)</code>.
</p></dd></dl>

<dl>
<dt id="index-open_002doutput_002dstring">standard procedure: <strong>open-output-string</strong></dt>
<dd><p>Returns a textual output port that will accumulate characters for
retrieval by <code>get-output-string</code>.
</p></dd></dl>

<dl>
<dt id="index-get_002doutput_002dstring">standard procedure: <strong>get-output-string</strong> <em>port</em></dt>
<dd><p>It is an error if <var>port</var> was not created with <var>open-output-string</var>.
</p>
<p>Returns a string consisting of the characters that have been output to
the port so far in the order they were output.  If the result string
is modified, the effect is unspecified.
</p>
<div class="example">
<pre class="example">(parameterize ((current-output-port (open-output-string)))
  (display &quot;piece&quot;)
  (display &quot; by piece &quot;)
  (display &quot;by piece.&quot;)
  (newline)
  (get-output-string (current-output-port)))

    &rArr; &quot;piece by piece by piece.\n&quot;
</pre></div>
</dd></dl>

<dl>
<dt id="index-call_002dwith_002doutput_002dstring">procedure: <strong>call-with-output-string</strong> <em>procedure</em></dt>
<dd><p>The <var>procedure</var> is called with one argument, a textual output
port.  The values yielded by <var>procedure</var> are ignored.  When
<var>procedure</var> returns, <code>call-with-output-string</code> returns the
port&rsquo;s accumulated output as a string.  If the result string is
modified, the effect is unspecified.
</p>
<p>This procedure could have been defined as follows:
<span id="index-open_002doutput_002dstring-1"></span>
<span id="index-get_002doutput_002dstring-1"></span>
</p><div class="example">
<pre class="example">(define (call-with-output-string procedure)
  (let ((port (open-output-string)))
    (procedure port)
    (get-output-string port)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-call_002dwith_002dtruncated_002doutput_002dstring">procedure: <strong>call-with-truncated-output-string</strong> <em>limit procedure</em></dt>
<dd><p>Similar to <code>call-with-output-string</code>, except that the output is
limited to at most <var>limit</var> characters.  The returned value is a
pair; the car of the pair is <code>#t</code> if <var>procedure</var> attempted to
write more than <var>limit</var> characters, and <code>#f</code> otherwise.  The
cdr of the pair is a newly allocated string containing the accumulated
output.
</p>
<p>This procedure could have been defined as follows:
<span id="index-open_002doutput_002dstring-2"></span>
<span id="index-call_002dwith_002dtruncated_002doutput_002dport-1"></span>
</p><div class="example">
<pre class="example">(define (call-with-truncated-output-string limit procedure)
  (let ((port (open-output-string)))
    (let ((truncated?
           (call-with-truncated-output-port limit port
                                            procedure)))
      (cons truncated? (get-output-string port)))))
</pre></div>

<p>This procedure is helpful for displaying circular lists, as shown in this
example:
<span id="index-list-5"></span>
<span id="index-call_002dwith_002dtruncated_002doutput_002dstring-1"></span>
<span id="index-write-4"></span>
<span id="index-set_002dcdr_0021-2"></span>
</p><div class="example">
<pre class="example">(define inf (list 'inf))
(call-with-truncated-output-string 40
  (lambda (port)
    (write inf port)))                  &rArr;  (#f . &quot;(inf)&quot;)
(set-cdr! inf inf)
(call-with-truncated-output-string 40
  (lambda (port)
    (write inf port)))
        &rArr;  (#t . &quot;(inf inf inf inf inf inf inf inf inf inf&quot;)
</pre></div>
</dd></dl>

<dl>
<dt id="index-write_002dto_002dstring">procedure: <strong>write-to-string</strong> <em>object [limit]</em></dt>
<dd><p>Writes <var>object</var> to a string output port, and returns the resulting
string.
</p>
<p>If <var>limit</var> is supplied and not <code>#f</code>, then this procedure is
equivalent to the following and returns a pair instead of just a string:
<span id="index-call_002dwith_002dtruncated_002doutput_002dstring-2"></span>
<span id="index-write-5"></span>
</p><div class="example">
<pre class="example">(call-with-truncated-output-string limit
  (lambda (port)
    (write object port)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-with_002dinput_002dfrom_002dstring">obsolete procedure: <strong>with-input-from-string</strong> <em>string thunk</em></dt>
<dt id="index-with_002doutput_002dto_002dstring">obsolete procedure: <strong>with-output-to-string</strong> <em>thunk</em></dt>
<dt id="index-with_002doutput_002dto_002dtruncated_002dstring">obsolete procedure: <strong>with-output-to-truncated-string</strong> <em>limit thunk</em></dt>
<dd><p>These procedures are <strong>deprecated</strong>; instead use
<code>open-input-string</code>, <code>call-with-output-string</code>, or
<code>call-with-truncated-output-string</code> along with
<code>parameterize</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Bytevector-Ports.html" accesskey="n" rel="next">Bytevector Ports</a>, Previous: <a href="File-Ports.html" accesskey="p" rel="prev">File Ports</a>, Up: <a href="Input_002fOutput.html" accesskey="u" rel="up">Input/Output</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
