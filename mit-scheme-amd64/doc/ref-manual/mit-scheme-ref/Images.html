<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Images (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Images (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Images (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Graphics.html" rel="up" title="Graphics">
<link href="X-Graphics.html" rel="next" title="X Graphics">
<link href="Custom-Graphics-Operations.html" rel="prev" title="Custom Graphics Operations">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Images"></span><div class="header">
<p>
Next: <a href="X-Graphics.html" accesskey="n" rel="next">X Graphics</a>, Previous: <a href="Custom-Graphics-Operations.html" accesskey="p" rel="prev">Custom Graphics Operations</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Images-1"></span><h3 class="section">17.8 Images</h3>
<span id="index-graphics_002c-images"></span>
<span id="index-images_002c-graphics"></span>
<span id="index-graphics_002c-bitmaps"></span>
<span id="index-bitmaps_002c-graphics"></span>

<p>Some graphics device types support images, which are rectangular pieces
of picture that may be drawn into a graphics device.  Images are often
called something else in the host graphics system, such as bitmaps or
pixmaps.  The operations supported vary between devices, so look under
the different device types to see what operations are available.  All
devices that support images support the following operations.
</p>
<dl>
<dt id="index-create_002dimage-on-graphics_002ddevice">operation on graphics-device: <strong>create-image</strong> <em>width height</em></dt>
<dd><p>Images are created using the <code>create-image</code> graphics operation,
specifying the <var>width</var> and <var>height</var> of the image in device
coordinates (pixels).
</p>
<div class="example">
<pre class="example">(graphics-operation device 'create-image 200 100)
</pre></div>

<p>The initial contents of an image are unspecified.
</p>
<p><code>create-image</code> is a graphics operation rather than a procedure
because the kind of image returned depends on the kind of graphics
device used and the options specified in its creation.  The image may be
used freely with other graphics devices created with the same
attributes, but the effects of using an image with a graphics device
with different attributes (for example, different colors) is undefined.
Under X, the image is display dependent.
</p></dd></dl>

<dl>
<dt id="index-draw_002dimage-on-graphics_002ddevice">operation on graphics-device: <strong>draw-image</strong> <em>x y image</em></dt>
<dd><p>The image is copied into the graphics device at the specified position.
</p></dd></dl>

<dl>
<dt id="index-draw_002dsubimage-on-graphics_002ddevice">operation on graphics-device: <strong>draw-subimage</strong> <em>x y image im-x im-y w h</em></dt>
<dd><p>Part of the image is copied into the graphics device at the specified
(<var>x</var>, <var>y</var>) position.  The part of the image that is copied is the
rectangular region at <var>im-x</var> and <var>im-y</var> and of width <var>w</var> and
height <var>h</var>.  These four numbers are given in device coordinates
(pixels).
</p></dd></dl>

<dl>
<dt id="index-image_003f">procedure: <strong>image?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is an image, otherwise returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-image_002fdestroy">procedure: <strong>image/destroy</strong> <em>image</em></dt>
<dd><p>This procedure destroys <var>image</var>, returning storage to the system.
Programs should destroy images after they have been used because even
modest images may use large amounts of memory.  Images are reclaimed by
the garbage collector, but they may be implemented using memory outside
of Scheme&rsquo;s heap.  If an image is reclaimed before being destroyed, the
implementation might not deallocate that non-heap memory, which can
cause a subsequent call to <code>create-image</code> to fail because it is
unable to allocate enough memory.
</p></dd></dl>


<dl>
<dt id="index-image_002fheight">procedure: <strong>image/height</strong> <em>image</em></dt>
<dd><p>Returns the height of the image in device coordinates.
</p></dd></dl>

<dl>
<dt id="index-image_002fwidth">procedure: <strong>image/width</strong> <em>image</em></dt>
<dd><p>Returns the width of the image in device coordinates.
</p></dd></dl>

<dl>
<dt id="index-image_002ffill_002dfrom_002dbyte_002dvector">procedure: <strong>image/fill-from-byte-vector</strong> <em>image bytes</em></dt>
<dd><p>The contents of <var>image</var> are set in a device-dependent way, using one
byte per pixel from <var>bytes</var> (a string).  Pixels are filled row by
row from the top of the image to the bottom, with each row being filled
from left to right.  There must be at least <code>(* (image/height
<var>image</var>) (image/width <var>image</var>))</code> bytes in <var>bytes</var>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="X-Graphics.html" accesskey="n" rel="next">X Graphics</a>, Previous: <a href="Custom-Graphics-Operations.html" accesskey="p" rel="prev">Custom Graphics Operations</a>, Up: <a href="Graphics.html" accesskey="u" rel="up">Graphics</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
