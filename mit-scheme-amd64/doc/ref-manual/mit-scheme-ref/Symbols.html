<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Symbols (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Symbols (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Symbols (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Miscellaneous-Datatypes.html" rel="up" title="Miscellaneous Datatypes">
<link href="Parameters.html" rel="next" title="Parameters">
<link href="Booleans.html" rel="prev" title="Booleans">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Symbols"></span><div class="header">
<p>
Next: <a href="Parameters.html" accesskey="n" rel="next">Parameters</a>, Previous: <a href="Booleans.html" accesskey="p" rel="prev">Booleans</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Symbols-1"></span><h3 class="section">10.2 Symbols</h3>

<span id="index-symbol-_0028defn_0029"></span>
<span id="index-interned-symbol-_0028defn_0029"></span>
<span id="index-uninterned-symbol-_0028defn_0029"></span>
<span id="index-property-list_002c-of-symbol"></span>
<span id="index-disembodied-property-list"></span>
<span id="index-read-3"></span>
<p>MIT/GNU Scheme provides two types of symbols: <em>interned</em> and
<em>uninterned</em>.  Interned symbols are far more common than uninterned
symbols, and there are more ways to create them.  Interned symbols have
an external representation that is recognized by the procedure
<code>read</code>; uninterned symbols do not.<a id="DOCF8" href="#FOOT8"><sup>8</sup></a>
</p>
<span id="index-string_003d_003f-4"></span>
<span id="index-eq_003f-2"></span>
<p>Interned symbols have an extremely useful property: any two interned
symbols whose names are the same, in the sense of <code>string=?</code>, are
the same object (i.e. they are <code>eq?</code> to one another).  The term
<em>interned</em> refers to the process of <em>interning</em> by which this is
accomplished.  Uninterned symbols do not share this property.
</p>
<span id="index-case_002c-of-interned-symbol"></span>
<span id="index-alphabetic-case_002c-of-interned-symbol"></span>
<span id="index-write-1"></span>
<p>The names of interned symbols are not distinguished by their alphabetic
case.  Because of this, MIT/GNU Scheme converts all alphabetic
characters in the name of an interned symbol to a specific case (lower
case) when the symbol is created.  When the name of an interned symbol
is referenced (using <code>symbol-&gt;string</code>) or written (using
<code>write</code>) it appears in this case.  It is a bad idea to depend on
the name being lower case.  In fact, it is preferable to take this one
step further: don&rsquo;t depend on the name of a symbol being in a uniform
case.
</p>
<span id="index-external-representation_002c-for-symbol"></span>
<span id="index-read-4"></span>
<span id="index-write-2"></span>
<p>The rules for writing an interned symbol are the same as the rules for
writing an identifier (see <a href="Identifiers.html">Identifiers</a>).  Any interned symbol that
has been returned as part of a literal expression, or read using the
<code>read</code> procedure and subsequently written out using the
<code>write</code> procedure, will read back in as the identical symbol (in
the sense of <code>eq?</code>).
</p>
<p>Usually it is also true that reading in an interned symbol that was
previously written out produces the same symbol.  An exception are
symbols created by the procedures <code>string-&gt;symbol</code> and
<code>intern</code>; they can create symbols for which this write/read
invariance may not hold because the symbols&rsquo; names contain special
characters or letters in the non-standard case.<a id="DOCF9" href="#FOOT9"><sup>9</sup></a>
</p>
<span id="index-read-5"></span>
<p>The external representation for uninterned symbols is special, to
distinguish them from interned symbols and prevent them from being
recognized by the <code>read</code> procedure:
</p>
<div class="example">
<pre class="example">(string-&gt;uninterned-symbol &quot;foo&quot;)
     &rArr;  #[uninterned-symbol 30 foo]
</pre></div>

<p>In this section, the procedures that return symbols as values will
either always return interned symbols, or always return uninterned
symbols.  The procedures that accept symbols as arguments will always
accept either interned or uninterned symbols, and do not distinguish the
two.
</p>
<dl>
<dt id="index-symbol_003f">procedure: <strong>symbol?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-symbol"></span>
<p>Returns <code>#t</code> if <var>object</var> is a symbol, otherwise returns
<code>#f</code>.
</p>
<div class="example">
<pre class="example">(symbol? 'foo)                                  &rArr;  #t
(symbol? (car '(a b)))                          &rArr;  #t
(symbol? &quot;bar&quot;)                                 &rArr;  #f
</pre></div>
</dd></dl>

<dl>
<dt id="index-symbol_002d_003estring-3">procedure: <strong>symbol-&gt;string</strong> <em>symbol</em></dt>
<dd><span id="index-name_002c-of-symbol"></span>
<span id="index-print-name_002c-of-symbol"></span>
<span id="index-string_003d_003f-5"></span>
<span id="index-string_002dset_0021-2"></span>
<p>Returns the name of <var>symbol</var> as a string.  If <var>symbol</var> was
returned by <code>string-&gt;symbol</code>, the value of this procedure will be
identical (in the sense of <code>string=?</code>) to the string that was
passed to <code>string-&gt;symbol</code>.  It is an error to apply mutation
procedures such as <code>string-set!</code> to strings returned by this
procedure.
</p>
<div class="example">
<pre class="example">(symbol-&gt;string 'flying-fish)           &rArr;  &quot;flying-fish&quot;
(symbol-&gt;string 'Martin)                &rArr;  &quot;martin&quot;
(symbol-&gt;string (string-&gt;symbol &quot;Malvina&quot;))
                                        &rArr;  &quot;Malvina&quot;
</pre></div>

<p>Note that two distinct uninterned symbols can have the same name.
</p></dd></dl>

<dl>
<dt id="index-intern">procedure: <strong>intern</strong> <em>string</em></dt>
<dd><span id="index-interning_002c-of-symbols"></span>
<span id="index-construction_002c-of-symbols"></span>
<p>Returns the interned symbol whose name is <var>string</var>.  Converts
<var>string</var> to the standard alphabetic case before generating the
symbol.  This is the preferred way to create interned symbols, as it
guarantees the following independent of which case the implementation
uses for symbols&rsquo; names:
</p>
<div class="example">
<pre class="example">(eq? 'bitBlt (intern &quot;bitBlt&quot;)) &rArr;     #t
</pre></div>

<p>The user should take care that <var>string</var> obeys the rules for
identifiers (see <a href="Identifiers.html">Identifiers</a>), otherwise the resulting symbol cannot
be read as itself.
</p></dd></dl>

<dl>
<dt id="index-intern_002dsoft">procedure: <strong>intern-soft</strong> <em>string</em></dt>
<dd><p>Returns the interned symbol whose name is <var>string</var>.  Converts
<var>string</var> to the standard alphabetic case before generating the
symbol.  If no such interned symbol exists, returns <code>#f</code>.
</p>
<p>This is exactly like <code>intern</code>, except that it will not create an
interned symbol, but only returns symbols that already exist.
</p></dd></dl>

<dl>
<dt id="index-string_002d_003esymbol">procedure: <strong>string-&gt;symbol</strong> <em>string</em></dt>
<dd><span id="index-string_002c-interning-as-symbol"></span>
<p>Returns the interned symbol whose name is <var>string</var>.  Although you
can use this procedure to create symbols with names containing special
characters or lowercase letters, it&rsquo;s usually a bad idea to create such
symbols because they cannot be read as themselves.  See
<code>symbol-&gt;string</code>.
</p>
<div class="example">
<pre class="example">(eq? 'mISSISSIppi 'mississippi)         &rArr;  #t
(string-&gt;symbol &quot;mISSISSIppi&quot;)
     &rArr;  <span class="roman">the symbol with the name</span> &quot;mISSISSIppi&quot;
(eq? 'bitBlt (string-&gt;symbol &quot;bitBlt&quot;)) &rArr;  #f
(eq? 'JollyWog
      (string-&gt;symbol
        (symbol-&gt;string 'JollyWog)))    &rArr;  #t
(string=? &quot;K. Harper, M.D.&quot;
           (symbol-&gt;string
             (string-&gt;symbol
               &quot;K. Harper, M.D.&quot;)))     &rArr;  #t
</pre></div>
</dd></dl>

<dl>
<dt id="index-string_002d_003euninterned_002dsymbol">procedure: <strong>string-&gt;uninterned-symbol</strong> <em>string</em></dt>
<dd><p>Returns a newly allocated uninterned symbol whose name is <var>string</var>.
It is unimportant what case or characters are used in
<var>string</var>.
</p>
<p>Note: this is the fastest way to make a symbol.
</p></dd></dl>

<dl>
<dt id="index-generate_002duninterned_002dsymbol">procedure: <strong>generate-uninterned-symbol</strong> <em>[object]</em></dt>
<dd><span id="index-gensym-_0028see-uninterned-symbol_0029"></span>
<span id="index-eq_003f-3"></span>
<p>Returns a newly allocated uninterned symbol that is guaranteed to be
different from any other object.  The symbol&rsquo;s name consists of a prefix
string followed by the (exact non-negative integer) value of an internal
counter.  The counter is initially zero, and is incremented after each
call to this procedure.
</p>
<p>The optional argument <var>object</var> is used to control how the symbol is
generated.  It may take one of the following values:
</p>
<ul>
<li> If <var>object</var> is omitted or <code>#f</code>, the prefix is <code>&quot;G&quot;</code>.

</li><li> If <var>object</var> is an exact non-negative integer, the internal counter
is set to that integer prior to generating the result.

</li><li> If <var>object</var> is a string, it is used as the prefix.

</li><li> If <var>object</var> is a symbol, its name is used as the prefix.
</li></ul>

<div class="example">
<pre class="example">(generate-uninterned-symbol)
     &rArr;  #[uninterned-symbol 31 G0]
(generate-uninterned-symbol)
     &rArr;  #[uninterned-symbol 32 G1]
(generate-uninterned-symbol 'this)
     &rArr;  #[uninterned-symbol 33 this2]
(generate-uninterned-symbol)
     &rArr;  #[uninterned-symbol 34 G3]
(generate-uninterned-symbol 100)
     &rArr;  #[uninterned-symbol 35 G100]
(generate-uninterned-symbol)
     &rArr;  #[uninterned-symbol 36 G101]
</pre></div>
</dd></dl>

<dl>
<dt id="index-symbol_002dappend">procedure: <strong>symbol-append</strong> <em>symbol &hellip;</em></dt>
<dd><span id="index-appending_002c-of-symbols"></span>
<span id="index-pasting_002c-of-symbols"></span>
<p>Returns the interned symbol whose name is formed by concatenating the
names of the given symbols.  This procedure preserves the case of the
names of its arguments, so if one or more of the arguments&rsquo; names has
non-standard case, the result will also have non-standard case.
</p>
<div class="example">
<pre class="example">(symbol-append 'foo- 'bar)              &rArr;  foo-bar
<span class="roman">;; the arguments may be uninterned:</span>
(symbol-append 'foo- (string-&gt;uninterned-symbol &quot;baz&quot;))
                                        &rArr;  foo-baz
<span class="roman">;; the result has the same case as the arguments:</span>
(symbol-append 'foo- (string-&gt;symbol &quot;BAZ&quot;))    &rArr;  foo-BAZ
</pre></div>
</dd></dl>

<dl>
<dt id="index-symbol_002dhash">procedure: <strong>symbol-hash</strong> <em>symbol</em></dt>
<dd><span id="index-hashing_002c-of-symbol"></span>
<span id="index-string_002dhash-1"></span>
<p>Returns a hash number for <var>symbol</var>, which is computed by calling
<code>string-hash</code> on <var>symbol</var>&rsquo;s name.  The hash number is an exact
non-negative integer.
</p></dd></dl>

<dl>
<dt id="index-symbol_002dhash_002dmod">procedure: <strong>symbol-hash-mod</strong> <em>symbol modulus</em></dt>
<dd><p><var>Modulus</var> must be an exact positive integer.  Equivalent to
</p>
<div class="example">
<pre class="example">(modulo (symbol-hash <var>symbol</var>) <var>modulus</var>)
</pre></div>

<p>This procedure is provided for convenience in constructing hash tables.
However, it is normally preferable to use
<code>make-strong-eq-hash-table</code> to build hash tables keyed by symbols,
because <code>eq?</code> hash tables are much faster.
</p></dd></dl>

<dl>
<dt id="index-symbol_003c_003f">procedure: <strong>symbol&lt;?</strong> <em>symbol1 symbol2</em></dt>
<dd><p>This procedure computes a total order on symbols.  It is equivalent to
</p>
<div class="example">
<pre class="example">(string&lt;? (symbol-&gt;string <var>symbol1</var>)
          (symbol-&gt;string <var>symbol2</var>))
</pre></div>
</dd></dl>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT8" href="#DOCF8">(8)</a></h3>
<p>In older dialects of
Lisp, uninterned symbols were fairly important.  This was true because
symbols were complicated data structures: in addition to having value
cells (and sometimes, function cells), these structures contained
<em>property lists</em>.  Because of this, uninterned symbols were often
used merely for their property lists &mdash; sometimes an uninterned symbol
used this way was referred to as a <em>disembodied property list</em>.  In
MIT/GNU Scheme, symbols do not have property lists, or any other components
besides their names.  There is a different data structure similar to
disembodied property lists: one-dimensional tables (see <a href="1D-Tables.html">1D Tables</a>).
For these reasons, uninterned symbols are not very useful in MIT/GNU Scheme.
In fact, their primary purpose is to simplify the generation of unique
variable names in programs that generate Scheme code.</p>
<h5><a id="FOOT9" href="#DOCF9">(9)</a></h3>
<p>MIT/GNU Scheme
reserves a specific set of interned symbols for its own use.  If you use
these reserved symbols it is possible that you could break specific
pieces of software that depend on them.  The reserved symbols all have
names beginning with the characters &lsquo;<samp>#[</samp>&rsquo; and ending with the
character &lsquo;<samp>]</samp>&rsquo;; thus none of these symbols can be read by the
procedure <code>read</code> and hence are not likely to be used by accident.
For example, <code>(intern &quot;#[unnamed-procedure]&quot;)</code> produces a reserved
symbol.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Parameters.html" accesskey="n" rel="next">Parameters</a>, Previous: <a href="Booleans.html" accesskey="p" rel="prev">Booleans</a>, Up: <a href="Miscellaneous-Datatypes.html" accesskey="u" rel="up">Miscellaneous Datatypes</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
