<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Red-Black Trees (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Red-Black Trees (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Red-Black Trees (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="Weight_002dBalanced-Trees.html" rel="next" title="Weight-Balanced Trees">
<link href="Object-Hashing.html" rel="prev" title="Object Hashing">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Red_002dBlack-Trees"></span><div class="header">
<p>
Next: <a href="Weight_002dBalanced-Trees.html" accesskey="n" rel="next">Weight-Balanced Trees</a>, Previous: <a href="Object-Hashing.html" accesskey="p" rel="prev">Object Hashing</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Red_002dBlack-Trees-1"></span><h3 class="section">11.6 Red-Black Trees</h3>

<span id="index-trees_002c-balanced-binary"></span>
<span id="index-balanced-binary-trees"></span>
<span id="index-binary-trees"></span>
<span id="index-red_002dblack-binary-trees"></span>
<p>Balanced binary trees are a useful data structure for maintaining large
sets of associations whose keys are ordered.  While most applications
involving large association sets should use hash tables, some
applications can benefit from the use of binary trees.  Binary trees
have two advantages over hash tables:
</p>
<ul>
<li> The contents of a binary tree can be converted to an alist, sorted by
key, in time proportional to the number of associations in the
tree.  A hash table can be converted into an unsorted alist in linear
time; sorting it requires additional time.

</li><li> Two binary trees can be compared for equality in linear time.  Hash
tables, on the other hand, cannot be compared at all; they must be
converted to alists before comparison can be done, and alist comparison
is quadratic unless the alists are sorted.
</li></ul>

<p>MIT/GNU Scheme provides an implementation of <em>red-black</em> trees.  The
red-black tree-balancing algorithm provides generally good performance
because it doesn&rsquo;t try to keep the tree very closely balanced.  At any
given node in the tree, one side of the node can be twice as high as the
other in the worst case.  With typical data the tree will remain fairly
well balanced anyway.
</p>
<p>A red-black tree takes space that is proportional to the number of
associations in the tree.  For the current implementation, the constant
of proportionality is eight words per association.
</p>
<p>Red-black trees hold their keys <em>strongly</em>.  In other words, if a
red-black tree contains an association for a given key, that key cannot
be reclaimed by the garbage collector.
</p>
<dl>
<dt id="index-make_002drb_002dtree">procedure: <strong>make-rb-tree</strong> <em>key=? key&lt;?</em></dt>
<dd><p>This procedure creates and returns a newly allocated red-black tree.
The tree contains no associations.  <var>Key=?</var> and <var>key&lt;?</var> are
predicates that compare two keys and determine whether they are equal to
or less than one another, respectively.  For any two keys, at most one
of these predicates is true.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_003f">procedure: <strong>rb-tree?</strong> <em>object</em></dt>
<dd><p>Returns <code>#t</code> if <var>object</var> is a red-black tree, otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002finsert_0021">procedure: <strong>rb-tree/insert!</strong> <em>rb-tree key datum</em></dt>
<dd><p>Associates <var>datum</var> with <var>key</var> in <var>rb-tree</var> and returns an
unspecified value.  If <var>rb-tree</var> already has an association for
<var>key</var>, that association is replaced.  The average and worst-case
times required by this operation are proportional to the logarithm of
the number of assocations in <var>rb-tree</var>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002flookup">procedure: <strong>rb-tree/lookup</strong> <em>rb-tree key default</em></dt>
<dd><p>Returns the datum associated with <var>key</var> in <var>rb-tree</var>.  If
<var>rb-tree</var> doesn&rsquo;t contain an association for <var>key</var>,
<var>default</var> is returned.  The average and worst-case times required by
this operation are proportional to the logarithm of the number of
assocations in <var>rb-tree</var>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fdelete_0021">procedure: <strong>rb-tree/delete!</strong> <em>rb-tree key</em></dt>
<dd><p>If <var>rb-tree</var> contains an association for <var>key</var>, removes it.
Returns an unspecified value.  The average and worst-case times required
by this operation are proportional to the logarithm of the number of
assocations in <var>rb-tree</var>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002d_003ealist">procedure: <strong>rb-tree-&gt;alist</strong> <em>rb-tree</em></dt>
<dd><p>Returns the contents of <var>rb-tree</var> as a newly allocated alist.  Each
element of the alist is a pair <code>(<var>key</var> . <var>datum</var>)</code> where
<var>key</var> is one of the keys of <var>rb-tree</var>, and <var>datum</var> is its
associated datum.  The alist is sorted by key according to the
<var>key&lt;?</var> argument used to construct <var>rb-tree</var>.  The
time required by this operation is proportional to the
number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fkey_002dlist">procedure: <strong>rb-tree/key-list</strong> <em>rb-tree</em></dt>
<dd><p>Returns a newly allocated list of the keys in <var>rb-tree</var>.  The list
is sorted by key according to the <var>key&lt;?</var> argument used to construct
<var>rb-tree</var>.  The time required by this
operation is proportional to the number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fdatum_002dlist">procedure: <strong>rb-tree/datum-list</strong> <em>rb-tree</em></dt>
<dd><p>Returns a newly allocated list of the datums in <var>rb-tree</var>.  Each
element of the list corresponds to one of the associations in
<var>rb-tree</var>, so if the tree contains multiple associations with the
same datum, so will this list.  The list is sorted by the keys of the
associations, even though they do not appear in the result.  The time required by this operation is proportional to the
number of associations in the tree.
</p>
<p>This procedure is equivalent to:
</p>
<div class="example">
<pre class="example">(lambda (rb-tree) (map cdr (rb-tree-&gt;alist rb-tree)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-rb_002dtree_002fequal_003f">procedure: <strong>rb-tree/equal?</strong> <em>rb-tree-1 rb-tree-2 datum=?</em></dt>
<dd><p>Compares <var>rb-tree-1</var> and <var>rb-tree-2</var> for equality, returning
<code>#t</code> iff they are equal and <code>#f</code> otherwise.  The trees must
have been constructed with the same equality and order predicates (same
in the sense of <code>eq?</code>).  The keys of the trees are compared using
the <var>key=?</var> predicate used to build the trees, while the datums of
the trees are compared using the equivalence predicate <var>datum=?</var>.
The worst-case time required by this operation is proportional to the
number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fempty_003f">procedure: <strong>rb-tree/empty?</strong> <em>rb-tree</em></dt>
<dd><p>Returns <code>#t</code> iff <var>rb-tree</var> contains no associations.  Otherwise
returns <code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fsize">procedure: <strong>rb-tree/size</strong> <em>rb-tree</em></dt>
<dd><p>Returns the number of associations in <var>rb-tree</var>, an exact
non-negative integer.  The average and worst-case times required by this
operation are proportional to the number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fheight">procedure: <strong>rb-tree/height</strong> <em>rb-tree</em></dt>
<dd><p>Returns the height of <var>rb-tree</var>, an exact non-negative integer.
This is the length of the longest path from a leaf of the tree to the
root.  The average and worst-case times required by this operation are
proportional to the number of associations in the tree.
</p>
<p>The returned value satisfies the following:
</p>
<div class="example">
<pre class="example">(lambda (rb-tree)
  (let ((size (rb-tree/size rb-tree))
        (lg (lambda (x) (/ (log x) (log 2)))))
    (&lt;= (lg size)
        (rb-tree/height rb-tree)
        (* 2 (lg (+ size 1))))))
</pre></div>
</dd></dl>

<dl>
<dt id="index-rb_002dtree_002fcopy">procedure: <strong>rb-tree/copy</strong> <em>rb-tree</em></dt>
<dd><p>Returns a newly allocated copy of <var>rb-tree</var>.  The copy is identical
to <var>rb-tree</var> in all respects, except that changes to <var>rb-tree</var>
do not affect the copy, and vice versa.  The time required by this
operation is proportional to the number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-alist_002d_003erb_002dtree">procedure: <strong>alist-&gt;rb-tree</strong> <em>alist key=? key&lt;?</em></dt>
<dd><p>Returns a newly allocated red-black tree that contains the same
associations as <var>alist</var>.  This procedure is equivalent to:
</p>
<div class="example">
<pre class="example">(lambda (alist key=? key&lt;?)
  (let ((tree (make-rb-tree key=? key&lt;?)))
    (for-each (lambda (association)
                (rb-tree/insert! tree
                                 (car association)
                                 (cdr association)))
              alist)
    tree))
</pre></div>
</dd></dl>

<p>The following operations provide access to the smallest and largest
members in a red/black tree.  They are useful for implementing priority
queues.
</p>
<dl>
<dt id="index-rb_002dtree_002fmin">procedure: <strong>rb-tree/min</strong> <em>rb-tree default</em></dt>
<dd><p>Returns the smallest key in <var>rb-tree</var>, or <var>default</var> if the tree
is empty.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fmin_002ddatum">procedure: <strong>rb-tree/min-datum</strong> <em>rb-tree default</em></dt>
<dd><p>Returns the datum associated with the smallest key in <var>rb-tree</var>, or
<var>default</var> if the tree is empty.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fmin_002dpair">procedure: <strong>rb-tree/min-pair</strong> <em>rb-tree</em></dt>
<dd><p>Finds the smallest key in <var>rb-tree</var> and returns a pair containing
that key and its associated datum.  If the tree is empty, returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fmax">procedure: <strong>rb-tree/max</strong> <em>rb-tree default</em></dt>
<dd><p>Returns the largest key in <var>rb-tree</var>, or <var>default</var> if the tree
is empty.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fmax_002ddatum">procedure: <strong>rb-tree/max-datum</strong> <em>rb-tree default</em></dt>
<dd><p>Returns the datum associated with the largest key in <var>rb-tree</var>, or
<var>default</var> if the tree is empty.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fmax_002dpair">procedure: <strong>rb-tree/max-pair</strong> <em>rb-tree</em></dt>
<dd><p>Finds the largest key in <var>rb-tree</var> and returns a pair containing
that key and its associated datum.  If the tree is empty, returns
<code>#f</code>.
</p></dd></dl>

<dl>
<dt id="index-rb_002dtree_002fdelete_002dmin_0021">procedure: <strong>rb-tree/delete-min!</strong> <em>rb-tree default</em></dt>
<dt id="index-rb_002dtree_002fdelete_002dmin_002ddatum_0021">procedure: <strong>rb-tree/delete-min-datum!</strong> <em>rb-tree default</em></dt>
<dt id="index-rb_002dtree_002fdelete_002dmin_002dpair_0021">procedure: <strong>rb-tree/delete-min-pair!</strong> <em>rb-tree</em></dt>
<dt id="index-rb_002dtree_002fdelete_002dmax_0021">procedure: <strong>rb-tree/delete-max!</strong> <em>rb-tree default</em></dt>
<dt id="index-rb_002dtree_002fdelete_002dmax_002ddatum_0021">procedure: <strong>rb-tree/delete-max-datum!</strong> <em>rb-tree default</em></dt>
<dt id="index-rb_002dtree_002fdelete_002dmax_002dpair_0021">procedure: <strong>rb-tree/delete-max-pair!</strong> <em>rb-tree</em></dt>
<dd><p>These operations are exactly like the accessors above, in that they
return information associated with the smallest or largest key, except
that they simultaneously delete that key.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Weight_002dBalanced-Trees.html" accesskey="n" rel="next">Weight-Balanced Trees</a>, Previous: <a href="Object-Hashing.html" accesskey="p" rel="prev">Object Hashing</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
