<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Machine Time (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Machine Time (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Machine Time (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Operating_002dSystem-Interface.html" rel="up" title="Operating-System Interface">
<link href="Subprocesses.html" rel="next" title="Subprocesses">
<link href="External-Representation-of-Time.html" rel="prev" title="External Representation of Time">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Machine-Time"></span><div class="header">
<p>
Next: <a href="Subprocesses.html" accesskey="n" rel="next">Subprocesses</a>, Previous: <a href="Date-and-Time.html" accesskey="p" rel="prev">Date and Time</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Machine-Time-1"></span><h3 class="section">15.6 Machine Time</h3>

<p>The previous section dealt with procedures that manipulate clock time.
This section describes procedures that deal with computer time: elapsed
CPU time, elapsed real time, and so forth.  These procedures are useful
for measuring the amount of time it takes to execute code.
</p>
<span id="index-tick"></span>
<p>Some of the procedures in this section manipulate a time representation
called <em>ticks</em>.  A tick is a unit of time that is unspecified here
but can be converted to and from seconds by supplied procedures.  A
count in ticks is represented as an exact integer.  At present each tick
is one millisecond, but this may change in the future.
</p>
<dl>
<dt id="index-process_002dtime_002dclock">procedure: <strong>process-time-clock</strong></dt>
<dd><p>Returns the amount of process time, in ticks, that has elapsed since
Scheme was started.  Process time is measured by the operating system
and is time during which the Scheme process is computing.  It does not
include time in system calls, but depending on the operating system it
may include time used by subprocesses.
</p>
<div class="example">
<pre class="example">(process-time-clock) &rArr; 21290
</pre></div>
</dd></dl>

<dl>
<dt id="index-real_002dtime_002dclock">procedure: <strong>real-time-clock</strong></dt>
<dd><p>Returns the amount of real time, in ticks, that has elapsed since Scheme
was started.  Real time is the time measured by an ordinary clock.
</p>
<div class="example">
<pre class="example">(real-time-clock) &rArr; 33474836
</pre></div>
</dd></dl>

<dl>
<dt id="index-internal_002dtime_002fticks_002d_003eseconds">procedure: <strong>internal-time/ticks-&gt;seconds</strong> <em>ticks</em></dt>
<dd><p>Returns the number of seconds corresponding to <var>ticks</var>.  The result
is always a real number.
</p>
<div class="example">
<pre class="example">(internal-time/ticks-&gt;seconds 21290) &rArr; 21.29
(internal-time/ticks-&gt;seconds 33474836) &rArr; 33474.836
</pre></div>
</dd></dl>

<dl>
<dt id="index-internal_002dtime_002fseconds_002d_003eticks">procedure: <strong>internal-time/seconds-&gt;ticks</strong> <em>seconds</em></dt>
<dd><p>Returns the number of ticks corresponding to <var>seconds</var>.
<var>Seconds</var> must be a real number.
</p>
<div class="example">
<pre class="example">(internal-time/seconds-&gt;ticks 20.88) &rArr; 20880
(internal-time/seconds-&gt;ticks 20.83) &rArr; 20830
</pre></div>
</dd></dl>

<dl>
<dt id="index-system_002dclock">procedure: <strong>system-clock</strong></dt>
<dd><p>Returns the amount of process time, in seconds, that has elapsed since
Scheme was started.  Roughly equivalent to:
</p>
<div class="example">
<pre class="example">(internal-time/ticks-&gt;seconds (process-time-clock))
</pre></div>

<p>Example:
</p>
<div class="example">
<pre class="example">(system-clock) &rArr; 20.88
</pre></div>
</dd></dl>

<dl>
<dt id="index-runtime">procedure: <strong>runtime</strong></dt>
<dd><p>Returns the amount of process time, in seconds, that has elapsed since
Scheme was started.  However, it does not include time spent in garbage
collection.
</p>
<div class="example">
<pre class="example">(runtime) &rArr; 20.83
</pre></div>
</dd></dl>

<dl>
<dt id="index-with_002dtimings">procedure: <strong>with-timings</strong> <em>thunk receiver</em></dt>
<dd><p>Calls <var>thunk</var> with no arguments.  After <var>thunk</var> returns,
<var>receiver</var> is called with three arguments describing the time spent
while computing <var>thunk</var>: the elapsed run time, the amount of time
spent in the garbage collector, and the elapsed real time.  All three
times are in ticks.
</p>
<p>This procedure is most useful for doing performance measurements, and is
designed to have relatively low overhead.
</p>
<div class="example">
<pre class="example">(with-timings
 (lambda () <span class="roman">&hellip; hairy computation &hellip;</span>)
 (lambda (run-time gc-time real-time)
   (write (internal-time/ticks-&gt;seconds run-time))
   (write-char #\space)
   (write (internal-time/ticks-&gt;seconds gc-time))
   (write-char #\space)
   (write (internal-time/ticks-&gt;seconds real-time))
   (newline)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-measure_002dinterval">procedure: <strong>measure-interval</strong> <em>runtime? procedure</em></dt>
<dd><p>Calls <var>procedure</var>, passing it the current process time, in seconds,
as an argument.  The result of this call must be another procedure.
When <var>procedure</var> returns, the resulting procedure is
tail-recursively called with the ending time, in seconds, as an
argument.
</p>
<p>If <var>runtime?</var> is <code>#f</code>, the elapsed time is deducted from the
elapsed system time returned by <code>runtime</code>.
</p>
<p>While this procedure can be used for time measurement, its interface is
somewhat clumsy for that purpose.  We recommend that you use
<code>with-timings</code> instead, because it is more convenient and has lower
overhead.
</p>
<div class="example">
<pre class="example">(measure-interval #t
                  (lambda (start-time)
                    (let ((v <span class="roman">&hellip; hairy computation &hellip;</span>))
                      (lambda (end-time)
                        (write (- end-time start-time))
                        (newline)
                        v))))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Subprocesses.html" accesskey="n" rel="next">Subprocesses</a>, Previous: <a href="Date-and-Time.html" accesskey="p" rel="prev">Date and Time</a>, Up: <a href="Operating_002dSystem-Interface.html" accesskey="u" rel="up">Operating-System Interface</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
