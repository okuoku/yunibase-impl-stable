<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Lists (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Lists (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Lists (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Pairs.html" rel="next" title="Pairs">
<link href="Regsexp-Procedures.html" rel="prev" title="Regsexp Procedures">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Lists"></span><div class="header">
<p>
Next: <a href="Vectors.html" accesskey="n" rel="next">Vectors</a>, Previous: <a href="Strings.html" accesskey="p" rel="prev">Strings</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Lists-1"></span><h2 class="chapter">7 Lists</h2>

<span id="index-pair-_0028defn_0029"></span>
<span id="index-dotted-pair-_0028see-pair_0029"></span>
<span id="index-car-field_002c-of-pair-_0028defn_0029"></span>
<span id="index-cdr-field_002c-of-pair-_0028defn_0029"></span>
<p>A <em>pair</em> (sometimes called a <em>dotted pair</em>) is a data structure
with two fields called the <em>car</em> and <em>cdr</em> fields (for
historical reasons).  Pairs are created by the procedure <code>cons</code>.
The car and cdr fields are accessed by the procedures <code>car</code> and
<code>cdr</code>.  The car and cdr fields are assigned by the procedures
<code>set-car!</code> and <code>set-cdr!</code>.
</p>
<span id="index-list-_0028defn_0029"></span>
<p>Pairs are used primarily to represent <em>lists</em>.  A list can be
defined recursively as either the empty list or a pair whose cdr is
a list.  More precisely, the set of lists is defined as the smallest set
<var>X</var> such that
</p>
<ul>
<li> The empty list is in <var>X</var>.

</li><li> If <var>list</var> is in <var>X</var>, then any pair whose cdr field contains
<var>list</var> is also in <var>X</var>.
</li></ul>

<span id="index-element_002c-of-list-_0028defn_0029"></span>
<span id="index-length_002c-of-list-_0028defn_0029"></span>
<span id="index-empty-list-_0028defn_0029"></span>
<p>The objects in the car fields of successive pairs of a list are the
<em>elements</em> of the list.  For example, a two-element list is a pair
whose car is the first element and whose cdr is a pair whose car is the
second element and whose cdr is the empty list.  The <em>length</em> of a
list is the number of elements, which is the same as the number of
pairs.  The <em>empty list</em> is a special object of its own type (it is
not a pair); it has no elements and its length is zero.<a id="DOCF5" href="#FOOT5"><sup>5</sup></a>
</p>
<span id="index-dotted-notation_002c-for-pair-_0028defn_0029"></span>
<span id="index-notation_002c-dotted-_0028defn_0029"></span>
<span id="index-external-representation_002c-for-pair"></span>
<span id="index-pair_002c-external-representation"></span>
<span id="index-_0028-as-external-representation"></span>
<span id="index-_0029-as-external-representation"></span>
<span id="index-_002e-as-external-representation"></span>
<span id="index-parenthesis_002c-as-external-representation"></span>
<span id="index-dot_002c-as-external-representation"></span>
<span id="index-period_002c-as-external-representation"></span>
<span id="index-_0028"></span>
<span id="index-_0029"></span>
<span id="index-_002e"></span>
<p>The most general notation (external representation) for Scheme pairs is
the &ldquo;dotted&rdquo; notation <code>(<var>c1</var> . <var>c2</var>)</code> where <var>c1</var> is
the value of the car field and <var>c2</var> is the value of the cdr field.
For example, <code>(4 . 5)</code> is a pair whose car is <code>4</code> and whose
cdr is <code>5</code>.  Note that <code>(4 . 5)</code> is the external
representation of a pair, not an expression that evaluates to a pair.
</p>
<span id="index-external-representation_002c-for-list"></span>
<span id="index-list_002c-external-representation"></span>
<span id="index-external-representation_002c-for-empty-list"></span>
<span id="index-empty-list_002c-external-representation"></span>
<span id="index-_0028_0029"></span>
<p>A more streamlined notation can be used for lists: the elements of the
list are simply enclosed in parentheses and separated by spaces.  The
empty list is written <code>()</code>.  For example, the following are
equivalent notations for a list of symbols:
</p>
<div class="example">
<pre class="example">(a b c d e)
(a . (b . (c . (d . (e . ())))))
</pre></div>

<span id="index-set_002dcdr_0021"></span>
<p>Whether a given pair is a list depends upon what is stored in the cdr
field.  When the <code>set-cdr!</code> procedure is used, an object can be a
list one moment and not the next:
</p>
<div class="example">
<pre class="example">(define x (list 'a 'b 'c))
(define y x)
y                                       &rArr; (a b c)
(list? y)                               &rArr; #t
(set-cdr! x 4)                          &rArr; <span class="roman">unspecified</span>
x                                       &rArr; (a . 4)
(eqv? x y)                              &rArr; #t
y                                       &rArr; (a . 4)
(list? y)                               &rArr; #f
(set-cdr! x x)                          &rArr; <span class="roman">unspecified</span>
(list? y)                               &rArr; #f
</pre></div>

<span id="index-improper-list-_0028defn_0029"></span>
<span id="index-list_002c-improper-_0028defn_0029"></span>
<p>A chain of pairs that doesn&rsquo;t end in the empty list is called an
<em>improper list</em>.  Note that an improper list is not a list.  The
list and dotted notations can be combined to represent improper lists,
as the following equivalent notations show:
</p>
<div class="example">
<pre class="example">(a b c . d)
(a . (b . (c . d)))
</pre></div>

<span id="index-quote-1"></span>
<span id="index-quasiquote-1"></span>
<span id="index-unquote-1"></span>
<span id="index-unquote_002dsplicing-1"></span>
<span id="index-_0027-1"></span>
<span id="index-_0060-1"></span>
<span id="index-_002c-1"></span>
<span id="index-_002c_0040-1"></span>
<span id="index-read-2"></span>
<p>Within literal expressions and representations of objects read by the
<code>read</code> procedure, the forms <code>'<var>datum</var></code>,
<code>`<var>datum</var></code>, <code>,<var>datum</var></code>, and <code>,@<var>datum</var></code>
denote two-element lists whose first elements are the symbols
<code>quote</code>, <code>quasiquote</code>, <code>unquote</code>, and
<code>unquote-splicing</code>, respectively.  The second element in each case
is <var>datum</var>.  This convention is supported so that arbitrary Scheme
programs may be represented as lists.  Among other things, this permits
the use of the <code>read</code> procedure to parse Scheme programs.
</p>
<table class="menu" border="0" cellspacing="0">
<tr><td align="left" valign="top">&bull; <a href="Pairs.html" accesskey="1">Pairs</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Construction-of-Lists.html" accesskey="2">Construction of Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Selecting-List-Components.html" accesskey="3">Selecting List Components</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Cutting-and-Pasting-Lists.html" accesskey="4">Cutting and Pasting Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Filtering-Lists.html" accesskey="5">Filtering Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Searching-Lists.html" accesskey="6">Searching Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Mapping-of-Lists.html" accesskey="7">Mapping of Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Folding-of-Lists.html" accesskey="8">Folding of Lists</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
<tr><td align="left" valign="top">&bull; <a href="Miscellaneous-List-Operations.html" accesskey="9">Miscellaneous List Operations</a></td><td>&nbsp;&nbsp;</td><td align="left" valign="top">
</td></tr>
</table>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT5" href="#DOCF5">(5)</a></h3>
<p>The
above definitions imply that all lists have finite length and are
terminated by the empty list.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Vectors.html" accesskey="n" rel="next">Vectors</a>, Previous: <a href="Strings.html" accesskey="p" rel="prev">Strings</a>, Up: <a href="index.html" accesskey="u" rel="up">Top</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
