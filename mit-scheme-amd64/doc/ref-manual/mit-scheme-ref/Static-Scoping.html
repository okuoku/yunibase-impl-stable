<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Static Scoping (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Static Scoping (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Static Scoping (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Scheme-Concepts.html" rel="up" title="Scheme Concepts">
<link href="True-and-False.html" rel="next" title="True and False">
<link href="Initial-and-Current-Environments.html" rel="prev" title="Initial and Current Environments">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Static-Scoping"></span><div class="header">
<p>
Next: <a href="True-and-False.html" accesskey="n" rel="next">True and False</a>, Previous: <a href="Initial-and-Current-Environments.html" accesskey="p" rel="prev">Initial and Current Environments</a>, Up: <a href="Scheme-Concepts.html" accesskey="u" rel="up">Scheme Concepts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Static-Scoping-1"></span><h4 class="subsection">1.2.4 Static Scoping</h4>
<span id="index-scoping_002c-static"></span>
<span id="index-static-scoping"></span>

<span id="index-dynamic-binding_002c-versus-static-scoping"></span>
<p>Scheme is a statically scoped language with block structure.  In this
respect, it is like Algol and Pascal, and unlike most other dialects of
Lisp except for Common Lisp.
</p>
<span id="index-binding-expression-_0028defn_0029"></span>
<span id="index-expression_002c-binding-_0028defn_0029"></span>
<p>The fact that Scheme is statically scoped (rather than
dynamically bound) means that the environment that is extended (and
becomes current) when a procedure is called is the environment in which
the procedure was created (i.e. in which the procedure&rsquo;s defining
lambda expression was evaluated), not the environment in which the
procedure is called.  Because all the other Scheme <em>binding
expressions</em> can be expressed in terms of procedures, this determines
how all bindings behave.
</p>
<p>Consider the following definitions, made at the top-level <acronym>REP</acronym>
loop (in the initial environment):
</p>
<div class="example">
<pre class="example">(define x 1)
(define (f x) (g 2))
(define (g y) (+ x y))
(f 5)                                       &rArr;  3 <span class="roman">; not</span> 7
</pre></div>

<p>Here <code>f</code> and <code>g</code> are bound to procedures created in the
initial environment.  Because Scheme is statically scoped, the call to
<code>g</code> from <code>f</code> extends the initial environment (the one in which
<code>g</code> was created) with a binding of <code>y</code> to <code>2</code>.  In this
extended environment, <code>y</code> is <code>2</code> and <code>x</code> is <code>1</code>.
(In a dynamically bound Lisp, the call to <code>g</code> would extend the
environment in effect during the call to <code>f</code>, in which <code>x</code> is
bound to <code>5</code> by the call to <code>f</code>, and the answer would be
<code>7</code>.)
</p>
<span id="index-lexical-scoping-_0028defn_0029"></span>
<span id="index-scoping_002c-lexical-_0028defn_0029"></span>
<span id="index-region_002c-of-variable-binding-_0028defn_0029"></span>
<span id="index-variable_002c-binding-region-_0028defn_0029"></span>
<span id="index-lambda-1"></span>
<p>Note that with static scoping, you can tell what binding a variable
reference refers to just from looking at the text of the program; the
referenced binding cannot depend on how the program is used.  That is,
the nesting of environments (their parent-child relationship)
corresponds to the nesting of binding expressions in program text.
(Because of this connection to the text of the program, static scoping
is also called <em>lexical</em> scoping.)  For each place where a variable
is bound in a program there is a corresponding <em>region</em> of the
program text within which the binding is effective.  For example, the
region of a binding established by a <code>lambda</code> expression is the
entire body of the <code>lambda</code> expression.  The documentation of each
binding expression explains what the region of the bindings it makes is.
A use of a variable (that is, a reference to or assignment of a
variable) refers to the innermost binding of that variable whose region
contains the variable use.  If there is no such region, the use refers
to the binding of the variable in the global environment (which is an
ancestor of all other environments, and can be thought of as a region in
which all your programs are contained).
</p>
<hr>
<div class="header">
<p>
Next: <a href="True-and-False.html" accesskey="n" rel="next">True and False</a>, Previous: <a href="Initial-and-Current-Environments.html" accesskey="p" rel="prev">Initial and Current Environments</a>, Up: <a href="Scheme-Concepts.html" accesskey="u" rel="up">Scheme Concepts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
