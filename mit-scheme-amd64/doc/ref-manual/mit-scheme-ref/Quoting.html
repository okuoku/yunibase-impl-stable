<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Quoting (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Quoting (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Quoting (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Conditionals.html" rel="next" title="Conditionals">
<link href="Assignments.html" rel="prev" title="Assignments">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Quoting"></span><div class="header">
<p>
Next: <a href="Conditionals.html" accesskey="n" rel="next">Conditionals</a>, Previous: <a href="Assignments.html" accesskey="p" rel="prev">Assignments</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Quoting-1"></span><h3 class="section">2.6 Quoting</h3>
<span id="index-quoting"></span>

<p>This section describes the expressions that are used to modify or
prevent the evaluation of objects.
</p>
<dl>
<dt id="index-quote">standard special form: <strong>quote</strong> <em>datum</em></dt>
<dd><span id="index-external-representation_002c-and-quote"></span>
<span id="index-literal_002c-and-quote"></span>
<span id="index-constant_002c-and-quote"></span>
<p><code>(quote <var>datum</var>)</code> evaluates to <var>datum</var>.  <var>Datum</var> may be
any external representation of a Scheme object
(see <a href="External-Representations.html">External Representations</a>).
Use <code>quote</code> to include literal constants in
Scheme code.
</p>
<div class="example">
<pre class="example">(quote a)                               &rArr;  a
(quote #(a b c))                        &rArr;  #(a b c)
(quote (+ 1 2))                         &rArr;  (+ 1 2)
</pre></div>

<span id="index-_0027-as-external-representation"></span>
<span id="index-apostrophe_002c-as-external-representation"></span>
<span id="index-quote_002c-as-external-representation"></span>
<span id="index-_0027"></span>
<p><code>(quote <var>datum</var>)</code> may be abbreviated as <code>'<var>datum</var></code>.
The two notations are equivalent in all respects.
</p>
<div class="example">
<pre class="example">'a                                      &rArr;  a
'#(a b c)                               &rArr;  #(a b c)
'(+ 1 2)                                &rArr;  (+ 1 2)
'(quote a)                              &rArr;  (quote a)
''a                                     &rArr;  (quote a)
</pre></div>

<p>Numeric constants, string constants, character constants, and boolean
constants evaluate to themselves, so they don&rsquo;t need to be quoted.
</p>
<div class="example">
<pre class="example">'&quot;abc&quot;                                  &rArr;  &quot;abc&quot;
&quot;abc&quot;                                   &rArr;  &quot;abc&quot;
'145932                                 &rArr;  145932
145932                                  &rArr;  145932
'#t                                     &rArr;  #t
#t                                      &rArr;  #t
'#\a                                    &rArr;  #\a
#\a                                     &rArr;  #\a
</pre></div>
</dd></dl>

<dl>
<dt id="index-quasiquote">standard special form: <strong>quasiquote</strong> <em>template</em></dt>
<dd><span id="index-external-representation_002c-and-quasiquote"></span>
<span id="index-literal_002c-and-quasiquote"></span>
<span id="index-constant_002c-and-quasiquote"></span>
<span id="index-equal_003f"></span>
<p>&ldquo;Backquote&rdquo; or &ldquo;quasiquote&rdquo; expressions are useful for constructing
a list or vector structure when most but not all of the desired
structure is known in advance.  If no commas appear within the
<var>template</var>, the result of evaluating <code>`<var>template</var></code> is
equivalent (in the sense of <code>equal?</code>) to the result of evaluating
<code>'<var>template</var></code>.  If a comma appears within the <var>template</var>,
however, the expression following the comma is evaluated (&ldquo;unquoted&rdquo;)
and its result is inserted into the structure instead of the comma and
the expression.  If a comma appears followed immediately by an at-sign
(@), then the following expression shall evaluate to a list; the
opening and closing parentheses of the list are then &ldquo;stripped away&rdquo;
and the elements of the list are inserted in place of the comma at-sign
expression sequence.
</p>
<div class="example">
<pre class="example">`(list ,(+ 1 2) 4)                       &rArr;  (list 3 4)

(let ((name 'a)) `(list ,name ',name))   &rArr;  (list a 'a)

`(a ,(+ 1 2) ,@(map abs '(4 -5 6)) b)    &rArr;  (a 3 4 5 6 b)

`((foo ,(- 10 3)) ,@(cdr '(c)) . ,(car '(cons)))
                                         &rArr;  ((foo 7) . cons)

`#(10 5 ,(sqrt 4) ,@(map sqrt '(16 9)) 8)
                                         &rArr;  #(10 5 2 4 3 8)

`,(+ 2 3)                                &rArr;  5
</pre></div>

<span id="index-nesting_002c-of-quasiquote-expressions"></span>
<p>Quasiquote forms may be nested.  Substitutions are made only for
unquoted components appearing at the same nesting level as the outermost
backquote.  The nesting level increases by one inside each successive
quasiquotation, and decreases by one inside each unquotation.
</p>
<div class="example">
<pre class="example">`(a `(b ,(+ 1 2) ,(foo ,(+ 1 3) d) e) f)
     &rArr;  (a `(b ,(+ 1 2) ,(foo 4 d) e) f)

(let ((name1 'x)
      (name2 'y))
   `(a `(b ,,name1 ,',name2 d) e))
     &rArr;  (a `(b ,x ,'y d) e)
</pre></div>

<span id="index-backquote_002c-as-external-representation"></span>
<span id="index-_0060-as-external-representation"></span>
<span id="index-comma_002c-as-external-representation"></span>
<span id="index-_002c-as-external-representation"></span>
<span id="index-_002c_0040-as-external-representation"></span>
<span id="index-unquote"></span>
<span id="index-unquote_002dsplicing"></span>
<span id="index-_0060"></span>
<span id="index-_002c"></span>
<span id="index-_002c_0040"></span>
<p>The notations <code>`<var>template</var></code> and (<code>quasiquote
<var>template</var></code>) are identical in all respects.
<code>,<var>expression</var></code> is identical to <code>(unquote
<var>expression</var>)</code> and <code>,@<var>expression</var></code> is identical to
<code>(unquote-splicing <var>expression</var>)</code>.
</p>
<div class="example">
<pre class="example">(quasiquote (list (unquote (+ 1 2)) 4))
     &rArr;  (list 3 4)

'(quasiquote (list (unquote (+ 1 2)) 4))
     &rArr;  `(list ,(+ 1 2) 4)
     <em>i.e.,</em> (quasiquote (list (unquote (+ 1 2)) 4))
</pre></div>

<p>Unpredictable behavior can result if any of the symbols
<code>quasiquote</code>, <code>unquote</code>, or <code>unquote-splicing</code> appear in
a <var>template</var> in ways otherwise than as described above.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Conditionals.html" accesskey="n" rel="next">Conditionals</a>, Previous: <a href="Assignments.html" accesskey="p" rel="prev">Assignments</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
