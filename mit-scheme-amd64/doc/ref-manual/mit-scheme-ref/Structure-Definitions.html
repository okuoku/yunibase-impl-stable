<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Structure Definitions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Structure Definitions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Structure Definitions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Special-Forms.html" rel="up" title="Special Forms">
<link href="Macros.html" rel="next" title="Macros">
<link href="Iteration.html" rel="prev" title="Iteration">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Structure-Definitions"></span><div class="header">
<p>
Next: <a href="Macros.html" accesskey="n" rel="next">Macros</a>, Previous: <a href="Iteration.html" accesskey="p" rel="prev">Iteration</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Structure-Definitions-1"></span><h3 class="section">2.10 Structure Definitions</h3>

<p>This section provides examples and describes the options and syntax of
<code>define-structure</code>, an MIT/GNU Scheme macro that is very similar to
<code>defstruct</code> in Common Lisp.  The differences between them are
summarized at the end of this section.  For more information, see
Steele&rsquo;s Common Lisp book.
</p>
<dl>
<dt id="index-define_002dstructure">special form: <strong>define-structure</strong> <em>(name structure-option &hellip;) slot-description &hellip;</em></dt>
<dd><p>Each <var>slot-description</var> takes one of the following forms:
</p>
<div class="example">
<pre class="example"><var>slot-name</var>
(<var>slot-name</var> <var>default-init</var> [<var>slot-option</var> <var>value</var>]*)
</pre></div>

<span id="index-keyword-constructor"></span>
<span id="index-BOA-constructor"></span>
<p>The fields <var>name</var> and <var>slot-name</var> must both be symbols.  The
field <var>default-init</var> is an expression for the initial value of the
slot.  It is evaluated each time a new instance is constructed.  If it
is not specified, the initial content of the slot is undefined.  Default
values are only useful with a <small>BOA</small> constructor with argument list or
a keyword constructor (see below).
</p>
<p>Evaluation of a <code>define-structure</code> expression defines a structure
descriptor and a set of procedures to manipulate instances of the
structure.  These instances are represented as records by default
(see <a href="Records.html">Records</a>) but may alternately be lists or vectors.  The
accessors and modifiers are marked with compiler declarations so that
calls to them are automatically transformed into appropriate references.
Often, no options are required, so a simple call to
<code>define-structure</code> looks like:
</p>
<div class="example">
<pre class="example">(define-structure foo a b c)
</pre></div>

<p>This defines a type descriptor <code>rtd:foo</code>, a constructor
<code>make-foo</code>, a predicate <code>foo?</code>, accessors <code>foo-a</code>,
<code>foo-b</code>, and <code>foo-c</code>, and modifiers <code>set-foo-a!</code>,
<code>set-foo-b!</code>, and <code>set-foo-c!</code>.
</p>
<p>In general, if no options are specified, <code>define-structure</code> defines
the following (using the simple call above as an example):
</p>
<dl compact="compact">
<dt>type descriptor</dt>
<dd><p>The name of the type descriptor is <code>&quot;rtd:&quot;</code> followed by the name of
the structure, e.g. &lsquo;<samp>rtd:foo</samp>&rsquo;.  The type descriptor satisfies the
predicate <code>record-type?</code>.
</p>
</dd>
<dt>constructor</dt>
<dd><p>The name of the constructor is <code>&quot;make-&quot;</code> followed by the name of
the structure, e.g. &lsquo;<samp>make-foo</samp>&rsquo;.  The number of arguments accepted
by the constructor is the same as the number of slots; the arguments are
the initial values for the slots, and the order of the arguments matches
the order of the slot definitions.
</p>
</dd>
<dt>predicate</dt>
<dd><p>The name of the predicate is the name of the structure followed by
<code>&quot;?&quot;</code>, e.g. &lsquo;<samp>foo?</samp>&rsquo;.  The predicate is a procedure of one
argument, which returns <code>#t</code> if its argument is a record of the
type defined by this structure definition, and <code>#f</code> otherwise.
</p>
</dd>
<dt>accessors</dt>
<dd><p>For each slot, an accessor is defined.  The name of the accessor is
formed by appending the name of the structure, a hyphen, and the name of
the slot, e.g. &lsquo;<samp>foo-a</samp>&rsquo;.  The accessor is a procedure of one
argument, which must be a record of the type defined by this structure
definition.  The accessor extracts the contents of the corresponding
slot in that record and returns it.
</p>
</dd>
<dt>modifiers</dt>
<dd><p>For each slot, a modifier is defined.  The name of the modifier is
formed by appending <code>&quot;set-&quot;</code>, the name of the accessor, and
<code>&quot;!&quot;</code>, e.g. &lsquo;<samp>set-foo-a!</samp>&rsquo;.  The modifier is a procedure of
two arguments, the first of which must be a record of the type defined
by this structure definition, and the second of which may be any object.
The modifier modifies the contents of the corresponding slot in that
record to be that object, and returns an unspecified value.
</p></dd>
</dl>

<p>When options are not supplied, <code>(<var>name</var>)</code> may be abbreviated to
<var>name</var>.  This convention holds equally for <var>structure-options</var>
and <var>slot-options</var>.  Hence, these are equivalent:
</p>
<div class="example">
<pre class="example">(define-structure foo a b c)
(define-structure (foo) (a) b (c))
</pre></div>

<p>as are
</p>
<div class="example">
<pre class="example">(define-structure (foo keyword-constructor) a b c)
(define-structure (foo (keyword-constructor)) a b c)
</pre></div>

<p>When specified as option values, <code>false</code> and <code>nil</code> are
equivalent to <code>#f</code>, and <code>true</code> and <code>t</code> are equivalent to
<code>#t</code>.
</p></dd></dl>

<p>Possible <var>slot-options</var> are:
</p>
<dl>
<dt id="index-read_002donly">slot option: <strong>read-only</strong> <em>value</em></dt>
<dd><p>When given a <var>value</var> other than <code>#f</code>, this specifies that no
modifier should be created for the slot.
</p></dd></dl>

<dl>
<dt id="index-type">slot option: <strong>type</strong> <em>type-descriptor</em></dt>
<dd><p>This is accepted but not presently used.
</p></dd></dl>

<p>Possible <var>structure-options</var> are:
</p>
<dl>
<dt id="index-predicate">structure option: <strong>predicate</strong> <em>[name]</em></dt>
<dd><p>This option controls the definition of a predicate procedure for the
structure.  If <var>name</var> is not given, the predicate is defined with
the default name (see above).  If <var>name</var> is <code>#f</code>, the predicate
is not defined at all.  Otherwise, <var>name</var> must be a symbol, and
the predicate is defined with that symbol as its name.
</p></dd></dl>

<dl>
<dt id="index-copier">structure option: <strong>copier</strong> <em>[name]</em></dt>
<dd><p>This option controls the definition of a procedure to copy instances of
the structure.  This is a procedure of one argument, a structure
instance, that makes a newly allocated copy of the structure and returns
it.  If <var>name</var> is not given, the copier is defined, and the name
of the copier is <code>&quot;copy-&quot;</code> followed by the structure name (e.g.
&lsquo;<samp>copy-foo</samp>&rsquo;).  If <var>name</var> is <code>#f</code>, the copier is not
defined.  Otherwise, <var>name</var> must be a symbol, and the copier is
defined with that symbol as its name.
</p></dd></dl>

<dl>
<dt id="index-print_002dprocedure">structure option: <strong>print-procedure</strong> <em>expression</em></dt>
<dd><p>Evaluating <var>expression</var> must yield a procedure of two arguments,
which is used to print instances of the structure.  The procedure is a
<em>print method</em> (see <a href="Custom-Output.html">Custom Output</a>).
</p></dd></dl>

<dl>
<dt id="index-constructor">structure option: <strong>constructor</strong> <em>[name [argument-list]]</em></dt>
<dd><span id="index-BOA-constructor-_0028defn_0029"></span>
<p>This option controls the definition of constructor procedures.  These
constructor procedures are called &ldquo;<small>BOA</small> constructors&rdquo;, for &ldquo;By
Order of Arguments&rdquo;, because the arguments to the constructor specify
the initial contents of the structure&rsquo;s slots by the order in which they
are given.  This is as opposed to &ldquo;keyword constructors&rdquo;, which
specify the initial contents using keywords, and in which the order of
arguments is irrelevant.
</p>
<p>If <var>name</var> is not given, a constructor is defined with the default
name and arguments (see above).  If <var>name</var> is <code>#f</code>, no
constructor is defined; <var>argument-list</var> may not be specified in this
case.  Otherwise, <var>name</var> must be a symbol, and a constructor is
defined with that symbol as its name.  If <var>name</var> is a symbol,
<var>argument-list</var> is optionally allowed; if it is omitted, the
constructor accepts one argument for each slot in the structure
definition, in the same order in which the slots appear in the
definition.  Otherwise, <var>argument-list</var> must be a lambda list
(see <a href="Lambda-Expressions.html">Lambda Expressions</a>), and each of the parameters of the lambda
list must be the name of a slot in the structure.  The arguments
accepted by the constructor are defined by this lambda list.  Any slot
that is not specified by the lambda list is initialized to the
<var>default-init</var> as specified above; likewise for any slot specified
as an optional parameter when the corresponding argument is not
supplied.
</p>
<p>If the <code>constructor</code> option is specified, the default constructor
is not defined.  Additionally, the <code>constructor</code> option may be
specified multiple times to define multiple constructors with
different names and argument lists.
</p>
<div class="example">
<pre class="example">(define-structure (foo
                   (constructor make-foo (#!optional a b)))
  (a 6 read-only #t)
  (b 9))
</pre></div>
</dd></dl>

<dl>
<dt id="index-keyword_002dconstructor">structure option: <strong>keyword-constructor</strong> <em>[name]</em></dt>
<dd><span id="index-keyword-constructor-_0028defn_0029"></span>
<p>This option controls the definition of keyword constructor procedures.
A <em>keyword constructor</em> is a procedure that accepts arguments that
are alternating slot names and values.  If <var>name</var> is omitted, a
keyword constructor is defined, and the name of the constructor is
<code>&quot;make-&quot;</code> followed by the name of the structure (e.g.
&lsquo;<samp>make-foo</samp>&rsquo;).  Otherwise, <var>name</var> must be a symbol, and a keyword
constructor is defined with this symbol as its name.
</p>
<p>If the <code>keyword-constructor</code> option is specified, the default
constructor is not defined.  Additionally, the
<code>keyword-constructor</code> option may be specified multiple times to
define multiple keyword constructors; this is usually not done since
such constructors would all be equivalent.
</p>
<div class="example">
<pre class="example">(define-structure (foo (keyword-constructor make-bar)) a b)
(foo-a (make-bar 'b 20 'a 19))         &rArr; 19
</pre></div>
</dd></dl>

<dl>
<dt id="index-type_002ddescriptor">structure option: <strong>type-descriptor</strong> <em>name</em></dt>
<dd><p>This option cannot be used with the <code>type</code> or <code>named</code> options.
</p>
<p>By default, structures are implemented as records.  The name of the
structure is defined to hold the type descriptor of the record defined
by the structure.  The <code>type-descriptor</code> option specifies a
different name to hold the type descriptor.
</p>
<div class="example">
<pre class="example">(define-structure foo a b)
foo             &rArr; #[record-type 18]

(define-structure (bar (type-descriptor &lt;bar&gt;)) a b)
bar             error&rarr; Unbound variable: bar
&lt;bar&gt;         &rArr; #[record-type 19]
</pre></div>
</dd></dl>

<dl>
<dt id="index-conc_002dname">structure option: <strong>conc-name</strong> <em>[name]</em></dt>
<dd><p>By default, the prefix for naming accessors and modifiers is the name of
the structure followed by a hyphen.  The <code>conc-name</code> option can be
used to specify an alternative.  If <var>name</var> is not given, the prefix
is the name of the structure followed by a hyphen (the default).  If
<var>name</var> is <code>#f</code>, the slot names are used directly, without
prefix.  Otherwise, <var>name</var> must a symbol, and that symbol is used as
the prefix.
</p>
<div class="example">
<pre class="example"><code>(define-structure (foo (conc-name moby/)) a b)</code>
</pre></div>

<p>defines accessors <code>moby/a</code> and <code>moby/b</code>, and modifiers
<code>set-moby/a!</code> and <code>set-moby/b!</code>.
</p>
<div class="example">
<pre class="example"><code>(define-structure (foo (conc-name #f)) a b)</code>
</pre></div>

<p>defines accessors <code>a</code> and <code>b</code>, and modifiers <code>set-a!</code> and
<code>set-b!</code>.
</p></dd></dl>

<dl>
<dt id="index-type-1">structure option: <strong>type</strong> <em>representation-type</em></dt>
<dd><p>This option cannot be used with the <code>type-descriptor</code> option.
</p>
<p>By default, structures are implemented as records.  The <code>type</code>
option overrides this default, allowing the programmer to specify that
the structure be implemented using another data type.  The option value
<var>representation-type</var> specifies the alternate data type; it is
allowed to be one of the symbols <code>vector</code> or <code>list</code>, and the
data type used is the one corresponding to the symbol.
</p>
<p>If this option is given, and the <code>named</code> option is not specified,
the representation will not be tagged, and neither a predicate nor a
type descriptor will be defined; also, the <code>print-procedure</code>
option may not be given.
</p>
<div class="example">
<pre class="example">(define-structure (foo (type list)) a b) 
(make-foo 1 2)                          &rArr; (1 2)
</pre></div>
</dd></dl>

<dl>
<dt id="index-named">structure option: <strong>named</strong> <em>[expression]</em></dt>
<dd><p>This is valid only in conjunction with the <code>type</code> option and
specifies that the structure instances be tagged to make them
identifiable as instances of this structure type.  This option cannot be
used with the <code>type-descriptor</code> option.
</p>
<p>In the usual case, where <var>expression</var> is not given, the <code>named</code>
option causes a type descriptor and predicate to be defined for the
structure (recall that the <code>type</code> option without <code>named</code>
suppresses their definition), and also defines a default print method
for the structure instances (which can be overridden by the
<code>print-procedure</code> option).  If the default print method is not
wanted then the <code>print-procedure</code> option should be specified as
<code>#f</code>.  This causes the structure to be printed in its native
representation, as a list or vector, which includes the type descriptor.
The type descriptor is a unique object, <em>not</em> a record type, that
describes the structure instances and is additionally stored in the
structure instances to identify them: if the representation type is
<code>vector</code>, the type descriptor is stored in the zero-th slot of the
vector, and if the representation type is <code>list</code>, it is stored as
the first element of the list.
</p>

<div class="example">
<pre class="example">(define-structure (foo (type vector) named) a b c)
(vector-ref (make-foo 1 2 3) 0) &rArr; #[structure-type 52]
</pre></div>

<p>If <var>expression</var> is specified, it is an expression that is evaluated
to yield a tag object.  The <var>expression</var> is evaluated once when the
structure definition is evaluated (to specify the print method), and
again whenever a predicate or constructor is called.  Because of this,
<var>expression</var> is normally a variable reference or a constant.  The
value yielded by <var>expression</var> may be any object at all.  That object
is stored in the structure instances in the same place that the type
descriptor is normally stored, as described above.  If <var>expression</var>
is specified, no type descriptor is defined, only a predicate.
</p>
<div class="example">
<pre class="example">(define-structure (foo (type vector) (named 'foo)) a b c)
(vector-ref (make-foo 1 2 3) 0) &rArr; foo
</pre></div>
</dd></dl>

<dl>
<dt id="index-safe_002daccessors">structure option: <strong>safe-accessors</strong> <em>[boolean]</em></dt>
<dd><p>This option allows the programmer to have some control over the safety
of the slot accessors (and modifiers) generated by
<code>define-structure</code>.  If <code>safe-accessors</code> is not specified, or
if <var>boolean</var> is <code>#f</code>, then the accessors are optimized for
speed at the expense of safety; when compiled, the accessors will turn
into very fast inline sequences, usually one to three machine
instructions in length.  However, if <code>safe-accessors</code> is specified
and <var>boolean</var> is either omitted or <code>#t</code>, then the accessors are
optimized for safety, will check the type and structure of their
argument, and will be close-coded.
</p>
<div class="example">
<pre class="example">(define-structure (foo safe-accessors) a b c)
</pre></div>
</dd></dl>

<dl>
<dt id="index-initial_002doffset">structure option: <strong>initial-offset</strong> <em>offset</em></dt>
<dd><p>This is valid only in conjunction with the <code>type</code> option.
<var>Offset</var> must be an exact non-negative integer and specifies the
number of slots to leave open at the beginning of the structure instance
before the specified slots are allocated.  Specifying an <var>offset</var> of
zero is equivalent to omitting the <code>initial-offset</code> option.
</p>
<p>If the <code>named</code> option is specified, the structure tag appears in
the first slot, followed by the &ldquo;offset&rdquo; slots, and then the regular
slots.  Otherwise, the &ldquo;offset&rdquo; slots come first, followed by the
regular slots.
</p>
<div class="example">
<pre class="example">(define-structure (foo (type vector) (initial-offset 3))
  a b c)
(make-foo 1 2 3)                &rArr; #(() () () 1 2 3)
</pre></div>
</dd></dl>

<p>The essential differences between MIT/GNU Scheme&rsquo;s <code>define-structure</code>
and Common Lisp&rsquo;s <code>defstruct</code> are:
</p>
<ul>
<li> The default constructor procedure takes positional arguments, in the
same order as specified in the definition of the structure.  A keyword
constructor may be specified by giving the option
<code>keyword-constructor</code>.

</li><li> <small>BOA</small> constructors are described using Scheme lambda lists.  Since there
is nothing corresponding to <code>&amp;aux</code> in Scheme lambda lists, this
functionality is not implemented.

</li><li> By default, no <code>copier</code> procedure is defined.

</li><li> The side-effect procedure corresponding to the accessor <code>foo</code> is
given the name <code>set-foo!</code>.

</li><li> Keywords are ordinary symbols &ndash; use <code>foo</code> instead of <code>:foo</code>.

</li><li> The option values <code>false</code>, <code>nil</code>, <code>true</code>, and <code>t</code>
are treated as if the appropriate boolean constant had been specified
instead.

</li><li> The <code>print-function</code> option is named <code>print-procedure</code>.  Its
argument is a procedure of two arguments (the structure instance and a
textual output port) rather than three as in Common Lisp.

</li><li> By default, named structures are tagged with a unique object of some
kind.  In Common Lisp, the structures are tagged with symbols.  This
depends on the Common Lisp package system to help generate unique tags;
MIT/GNU Scheme has no such way to generate unique symbols.

</li><li> The <code>named</code> option may optionally take an argument, which is
normally the name of a variable (any expression may be used, but it is
evaluated whenever the tag name is needed).  If used, structure
instances will be tagged with that variable&rsquo;s value.  The variable must
be defined when <code>define-structure</code> is evaluated.

</li><li> The <code>type</code> option is restricted to the values <code>vector</code> and
<code>list</code>.

</li><li> The <code>include</code> option is not implemented.
</li></ul>

<hr>
<div class="header">
<p>
Next: <a href="Macros.html" accesskey="n" rel="next">Macros</a>, Previous: <a href="Iteration.html" accesskey="p" rel="prev">Iteration</a>, Up: <a href="Special-Forms.html" accesskey="u" rel="up">Special Forms</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
