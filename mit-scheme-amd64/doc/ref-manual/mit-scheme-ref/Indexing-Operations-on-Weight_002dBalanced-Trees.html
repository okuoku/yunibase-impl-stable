<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Indexing Operations on Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Indexing Operations on Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Indexing Operations on Weight-Balanced Trees (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Weight_002dBalanced-Trees.html" rel="up" title="Weight-Balanced Trees">
<link href="Associative-Maps.html" rel="next" title="Associative Maps">
<link href="Advanced-Operations-on-Weight_002dBalanced-Trees.html" rel="prev" title="Advanced Operations on Weight-Balanced Trees">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Indexing-Operations-on-Weight_002dBalanced-Trees"></span><div class="header">
<p>
Previous: <a href="Advanced-Operations-on-Weight_002dBalanced-Trees.html" accesskey="p" rel="prev">Advanced Operations on Weight-Balanced Trees</a>, Up: <a href="Weight_002dBalanced-Trees.html" accesskey="u" rel="up">Weight-Balanced Trees</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Indexing-Operations-on-Weight_002dBalanced-Trees-1"></span><h4 class="subsection">11.7.4 Indexing Operations on Weight-Balanced Trees</h4>

<p>Weight-balanced trees support operations that view the tree as sorted
sequence of associations.  Elements of the sequence can be accessed by
position, and the position of an element in the sequence can be
determined, both in logarthmic time.
</p>
<dl>
<dt id="index-wt_002dtree_002findex">procedure: <strong>wt-tree/index</strong> <em>wt-tree index</em></dt>
<dt id="index-wt_002dtree_002findex_002ddatum">procedure: <strong>wt-tree/index-datum</strong> <em>wt-tree index</em></dt>
<dt id="index-wt_002dtree_002findex_002dpair">procedure: <strong>wt-tree/index-pair</strong> <em>wt-tree index</em></dt>
<dd><p>Returns the 0-based <var>index</var>th association of <var>wt-tree</var> in the
sorted sequence under the tree&rsquo;s ordering relation on the keys.
<code>wt-tree/index</code> returns the <var>index</var>th key,
<code>wt-tree/index-datum</code> returns the datum associated with the
<var>index</var>th key and <code>wt-tree/index-pair</code> returns a new pair
<code>(<var>key</var> . <var>datum</var>)</code> which is the <code>cons</code> of the
<var>index</var>th key and its datum.  The average and worst-case times
required by this operation are proportional to the logarithm of the
number of associations in the tree.
</p>
<p>These operations signal a condition of type
<code>condition-type:bad-range-argument</code> if <var>index</var><code>&lt;0</code> or if
<var>index</var> is greater than or equal to the number of associations in
the tree.  If the tree is empty, they signal an anonymous error.
</p>
<p>Indexing can be used to find the median and maximum keys in the tree as
follows:
</p>
<div class="example">
<pre class="example">median:   (wt-tree/index <var>wt-tree</var>
                         (quotient (wt-tree/size <var>wt-tree</var>)
                                   2))
maximum:  (wt-tree/index <var>wt-tree</var>
                         (- (wt-tree/size <var>wt-tree</var>)
                            1))
</pre></div>
</dd></dl>

<dl>
<dt id="index-wt_002dtree_002frank">procedure: <strong>wt-tree/rank</strong> <em>wt-tree key</em></dt>
<dd><p>Determines the 0-based position of <var>key</var> in the sorted sequence of
the keys under the tree&rsquo;s ordering relation, or <code>#f</code> if the tree
has no association with for <var>key</var>.  This procedure returns either an
exact non-negative integer or <code>#f</code>.  The average and worst-case
times required by this operation are proportional to the logarithm of
the number of associations in the tree.
</p></dd></dl>

<dl>
<dt id="index-wt_002dtree_002fmin">procedure: <strong>wt-tree/min</strong> <em>wt-tree</em></dt>
<dt id="index-wt_002dtree_002fmin_002ddatum">procedure: <strong>wt-tree/min-datum</strong> <em>wt-tree</em></dt>
<dt id="index-wt_002dtree_002fmin_002dpair">procedure: <strong>wt-tree/min-pair</strong> <em>wt-tree</em></dt>
<dd><p>Returns the association of <var>wt-tree</var> that has the least key under the tree&rsquo;s ordering relation.
<code>wt-tree/min</code> returns the least key,
<code>wt-tree/min-datum</code> returns the datum associated with the
least key and <code>wt-tree/min-pair</code> returns a new pair
<code>(key . datum)</code> which is the <code>cons</code> of the minimum key and its datum.
The average and worst-case times required by this operation are
proportional to the logarithm of the number of associations in the tree.
</p>
<p>These operations signal an error if the tree is empty.
They could have been written
</p>
<div class="example">
<pre class="example">(define (wt-tree/min tree)
  (wt-tree/index tree 0))
(define (wt-tree/min-datum tree)
  (wt-tree/index-datum tree 0))
(define (wt-tree/min-pair tree)
  (wt-tree/index-pair tree 0))
</pre></div>
</dd></dl>

<dl>
<dt id="index-wt_002dtree_002fdelete_002dmin">procedure: <strong>wt-tree/delete-min</strong> <em>wt-tree</em></dt>
<dd><p>Returns a new tree containing all of the associations in <var>wt-tree</var>
except the association with the least key under the <var>wt-tree</var>&rsquo;s
ordering relation.  An error is signalled if the tree is empty.  The
average and worst-case times required by this operation are proportional
to the logarithm of the number of associations in the tree.  This
operation is equivalent to
</p>
<div class="example">
<pre class="example">(wt-tree/delete <var>wt-tree</var> (wt-tree/min <var>wt-tree</var>))
</pre></div>
</dd></dl>

<dl>
<dt id="index-wt_002dtree_002fdelete_002dmin_0021">procedure: <strong>wt-tree/delete-min!</strong> <em>wt-tree</em></dt>
<dd><p>Removes the association with the least key under the <var>wt-tree</var>&rsquo;s
ordering relation.  An error is signalled if the tree is empty.  The
average and worst-case times required by this operation are proportional
to the logarithm of the number of associations in the tree.  This
operation is equivalent to
</p>
<div class="example">
<pre class="example">(wt-tree/delete! <var>wt-tree</var> (wt-tree/min <var>wt-tree</var>))
</pre></div>
</dd></dl>

<hr>
<div class="header">
<p>
Previous: <a href="Advanced-Operations-on-Weight_002dBalanced-Trees.html" accesskey="p" rel="prev">Advanced Operations on Weight-Balanced Trees</a>, Up: <a href="Weight_002dBalanced-Trees.html" accesskey="u" rel="up">Weight-Balanced Trees</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
