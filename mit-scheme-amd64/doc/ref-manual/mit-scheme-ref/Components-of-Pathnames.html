<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Components of Pathnames (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Components of Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Components of Pathnames (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Pathnames.html" rel="up" title="Pathnames">
<link href="Operations-on-Pathnames.html" rel="next" title="Operations on Pathnames">
<link href="Filenames-and-Pathnames.html" rel="prev" title="Filenames and Pathnames">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Components-of-Pathnames"></span><div class="header">
<p>
Next: <a href="Operations-on-Pathnames.html" accesskey="n" rel="next">Operations on Pathnames</a>, Previous: <a href="Filenames-and-Pathnames.html" accesskey="p" rel="prev">Filenames and Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Components-of-Pathnames-1"></span><h4 class="subsection">15.1.2 Components of Pathnames</h4>
<span id="index-components_002c-of-pathname"></span>
<span id="index-pathname-components"></span>

<p>A pathname object always has six components, described below.  These
components are the common interface that allows programs to work the
same way with different file systems; the mapping of the pathname
components into the concepts peculiar to each file system is taken care
of by the Scheme implementation.
</p>
<dl compact="compact">
<dt><var>host</var></dt>
<dd><p>The name of the file system on which the file resides.  In the current
implementation, this component is always a host object that is filled in
automatically by the runtime system.  When specifying the host
component, use either <code>#f</code> or the value of the variable
<code>local-host</code>.
<span id="index-host_002c-pathname-component"></span>
</p>
</dd>
<dt><var>device</var></dt>
<dd><p>Corresponds to the &ldquo;device&rdquo; or &ldquo;file structure&rdquo; concept in many host
file systems: the name of a (logical or physical) device containing
files.  This component is the drive letter for PC file systems, and is
unused for unix file systems.
<span id="index-device_002c-pathname-component"></span>
</p>
</dd>
<dt><var>directory</var></dt>
<dd><p>Corresponds to the &ldquo;directory&rdquo; concept in many host file systems: the
name of a group of related files (typically those belonging to a single
user or project).  This component is always used for all file systems.
<span id="index-directory_002c-pathname-component"></span>
</p>
</dd>
<dt><var>name</var></dt>
<dd><p>The name of a group of files that can be thought of as conceptually the
&ldquo;same&rdquo; file.  This component is always used for all file systems.
<span id="index-name_002c-pathname-component"></span>
</p>
</dd>
<dt><var>type</var></dt>
<dd><p>Corresponds to the &ldquo;filetype&rdquo; or &ldquo;extension&rdquo; concept in many host
file systems.  This says what kind of file this is.  Files with the same
name but different type are usually related in some specific way, such
as one being a source file, another the compiled form of that source,
and a third the listing of error messages from the compiler.  This
component is currently used for all file systems, and is formed by
taking the characters that follow the last dot in the namestring.
<span id="index-type_002c-pathname-component"></span>
</p>
</dd>
<dt><var>version</var></dt>
<dd><p>Corresponds to the &ldquo;version number&rdquo; concept in many host file systems.
Typically this is a number that is incremented every time the file is
modified.  This component is currently unused for all file systems.
<span id="index-version_002c-pathname-component"></span>
</p></dd>
</dl>

<p>Note that a pathname is not necessarily the name of a specific file.
Rather, it is a specification (possibly only a partial specification) of
how to access a file.  A pathname need not correspond to any file that
actually exists, and more than one pathname can refer to the same file.
For example, the pathname with a version of <code>newest</code> may refer to
the same file as a pathname with the same components except a certain
number as the version.  Indeed, a pathname with version <code>newest</code>
may refer to different files as time passes, because the meaning of such
a pathname depends on the state of the file system.  In file systems
with such facilities as &ldquo;links&rdquo;, multiple file names, logical devices,
and so on, two pathnames that look quite different may turn out to
address the same file.  To access a file given a pathname, one must do a
file-system operation such as <code>open-input-file</code>.
</p>
<p>Two important operations involving pathnames are <em>parsing</em> and
<em>merging</em>.  Parsing is the conversion of a filename (which might be
something supplied interactively  by the users when asked to supply the
name of a file) into a pathname object.  This operation is
implementation-dependent, because the format of filenames is
implementation-dependent.  Merging takes a pathname with missing
components and supplies values for those components from a source of
default values.
</p>
<p>Not all of the components of a pathname need to be specified.  If a
component of a pathname is missing, its value is <code>#f</code>.
Before the file system interface can do anything interesting with a
file, such as opening the file, all the missing components of a pathname
must be filled in.  Pathnames with missing components are used
internally for various purposes; in particular, parsing a namestring
that does not specify certain components will result in a pathname with
missing components.
</p>
<p>Any component of a pathname may be the symbol <code>unspecific</code>, meaning
that the component simply does not exist, for file systems in which such
a value makes no sense.  For example, unix and Windows file
systems usually do not support version numbers, so the version component
for such a host might be <code>unspecific</code>.<a id="DOCF16" href="#FOOT16"><sup>16</sup></a>
</p>
<p>In addition to <code>#f</code> and <code>unspecific</code>, the components of a
pathname may take on the following meaningful values:
</p>
<dl compact="compact">
<dt><var>host</var></dt>
<dd><p>An implementation-defined type which may be tested for using the
<code>host?</code> predicate.
</p>
</dd>
<dt><var>device</var></dt>
<dd><p>On systems that support this component (Windows), it may be specified
as a string containing a single alphabetic character, for which the
alphabetic case is ignored.
</p>
</dd>
<dt><var>directory</var></dt>
<dd><p>A non-empty list, which represents a <em>directory path</em>: a sequence of
directories, each of which has a name in the previous directory, the
last of which is the directory specified by the entire path.  Each
element in such a path specifies the name of the directory relative to
the directory specified by the elements to its left.  The first element
of the list is either the symbol <code>absolute</code> or the symbol
<code>relative</code>.  If the first element in the list is the symbol
<code>absolute</code>, then the directory component (and subsequently the
pathname) is <em>absolute</em>; the first component in the sequence is to
be found at the &ldquo;root&rdquo; of the file system.  If the directory is
<em>relative</em> then the first component is to be found in some as yet
unspecified directory; typically this is later specified to be the
<em>current working directory</em>.
<span id="index-root_002c-as-pathname-component"></span>
<span id="index-directory-path-_0028defn_0029"></span>
<span id="index-path_002c-directory-_0028defn_0029"></span>
</p>
<span id="index-up_002c-as-pathname-component"></span>
<span id="index-parent_002c-of-directory"></span>
<p>Aside from <code>absolute</code> and <code>relative</code>, which may only appear as
the first element of the list, each subsequent element in the list is
either: a string, which is a literal component; the symbol <code>wild</code>,
meaningful only when used in conjunction with the directory reader; or
the symbol <code>up</code>, meaning the next directory is the &ldquo;parent&rdquo; of
the previous one.  <code>up</code> corresponds to the file <samp>..</samp> in unix
and PC file systems.
</p>
<p>(The following note does not refer to any file system currently
supported by MIT/GNU Scheme, but is included for completeness.)  In file
systems that do not have &ldquo;hierarchical&rdquo; structure, a specified
directory component will always be a list whose first element is
<code>absolute</code>.  If the system does not support directories other than
a single global directory, the list will have no other elements.  If the
system supports &ldquo;flat&rdquo; directories, i.e. a global set of directories
with no subdirectories, then the list will contain a second element,
which is either a string or <code>wild</code>.  In other words, a
non-hierarchical file system is treated as if it were hierarchical, but
the hierarchical features are unused.  This representation is somewhat
inconvenient for such file systems, but it discourages programmers from
making code depend on the lack of a file hierarchy.
</p>
</dd>
<dt><var>name</var></dt>
<dd><p>A string, which is a literal component; or the symbol <code>wild</code>,
meaningful only when used in conjunction with the directory reader.
</p>
</dd>
<dt><var>type</var></dt>
<dd><p>A string, which is a literal component; or the symbol <code>wild</code>,
meaningful only when used in conjunction with the directory reader.
</p>
</dd>
<dt><var>version</var></dt>
<dd><p>An exact positive integer, which is a literal component; the symbol
<code>newest</code>, which means to choose the largest available version
number for that file; the symbol <code>oldest</code>, which means to choose
the smallest version number; or the symbol <code>wild</code>, meaningful only
when used in conjunction with the directory reader.  In the future some
other possible values may be added, e.g. <code>installed</code>.  Note that
currently no file systems support version numbers; thus this component
is not used and should be specified as <code>#f</code>.
<span id="index-newest_002c-as-pathname-component"></span>
<span id="index-oldest_002c-as-pathname-component"></span>
<span id="index-installed_002c-as-pathname-component"></span>
</p></dd>
</dl>

<dl>
<dt id="index-make_002dpathname">procedure: <strong>make-pathname</strong> <em>host device directory name type version</em></dt>
<dd><span id="index-construction_002c-of-pathname-2"></span>
<p>Returns a pathname object whose components are the respective arguments.
Each argument must satisfy the restrictions for the corresponding
component, which were outlined above.
</p>
<div class="example">
<pre class="example">(make-pathname #f
               #f
               '(absolute &quot;usr&quot; &quot;morris&quot;)
               &quot;foo&quot;
               &quot;scm&quot;
               #f)
     &rArr;  #[pathname 67 &quot;/usr/morris/foo.scm&quot;]
</pre></div>
</dd></dl>

<dl>
<dt id="index-pathname_002dhost">procedure: <strong>pathname-host</strong> <em>pathname</em></dt>
<dt id="index-pathname_002ddevice">procedure: <strong>pathname-device</strong> <em>pathname</em></dt>
<dt id="index-pathname_002ddirectory">procedure: <strong>pathname-directory</strong> <em>pathname</em></dt>
<dt id="index-pathname_002dname">procedure: <strong>pathname-name</strong> <em>pathname</em></dt>
<dt id="index-pathname_002dtype">procedure: <strong>pathname-type</strong> <em>pathname</em></dt>
<dt id="index-pathname_002dversion">procedure: <strong>pathname-version</strong> <em>pathname</em></dt>
<dd><p>Returns a particular component of <var>pathname</var>.
</p>
<div class="example">
<pre class="example">(define x (-&gt;pathname &quot;/usr/morris/foo.scm&quot;))
(pathname-host x)       &rArr;  #[host 1]
(pathname-device x)     &rArr;  unspecific
(pathname-directory x)  &rArr;  (absolute &quot;usr&quot; &quot;morris&quot;)
(pathname-name x)       &rArr;  &quot;foo&quot;
(pathname-type x)       &rArr;  &quot;scm&quot;
(pathname-version x)    &rArr;  unspecific
</pre></div>
</dd></dl>

<dl>
<dt id="index-pathname_002dnew_002ddevice">procedure: <strong>pathname-new-device</strong> <em>pathname device</em></dt>
<dt id="index-pathname_002dnew_002ddirectory">procedure: <strong>pathname-new-directory</strong> <em>pathname directory</em></dt>
<dt id="index-pathname_002dnew_002dname">procedure: <strong>pathname-new-name</strong> <em>pathname name</em></dt>
<dt id="index-pathname_002dnew_002dtype">procedure: <strong>pathname-new-type</strong> <em>pathname type</em></dt>
<dt id="index-pathname_002dnew_002dversion">procedure: <strong>pathname-new-version</strong> <em>pathname version</em></dt>
<dd><p>Returns a new copy of <var>pathname</var> with the respective component
replaced by the second argument.  <var>Pathname</var> is unchanged.
Portable programs should not explicitly replace a component with
<code>unspecific</code> because this might not be permitted in some
situations.
</p>
<div class="example">
<pre class="example">(define p (-&gt;pathname &quot;/usr/blisp/rel15&quot;))
p
     &rArr;  #[pathname 71 &quot;/usr/blisp/rel15&quot;]
(pathname-new-name p &quot;rel100&quot;)
     &rArr;  #[pathname 72 &quot;/usr/blisp/rel100&quot;]
(pathname-new-directory p '(relative &quot;test&quot; &quot;morris&quot;))
     &rArr;  #[pathname 73 &quot;test/morris/rel15&quot;]
p
     &rArr;  #[pathname 71 &quot;/usr/blisp/rel15&quot;]
</pre></div>
</dd></dl>

<dl>
<dt id="index-pathname_002ddefault_002ddevice">procedure: <strong>pathname-default-device</strong> <em>pathname device</em></dt>
<dt id="index-pathname_002ddefault_002ddirectory">procedure: <strong>pathname-default-directory</strong> <em>pathname directory</em></dt>
<dt id="index-pathname_002ddefault_002dname">procedure: <strong>pathname-default-name</strong> <em>pathname name</em></dt>
<dt id="index-pathname_002ddefault_002dtype">procedure: <strong>pathname-default-type</strong> <em>pathname type</em></dt>
<dt id="index-pathname_002ddefault_002dversion">procedure: <strong>pathname-default-version</strong> <em>pathname version</em></dt>
<dd><p>These operations are similar to the <code>pathname-new-<var>component</var></code>
operations, except that they only change the specified <var>component</var>
if it has the value <code>#f</code> in <var>pathname</var>.
</p></dd></dl>

<div class="footnote">
<hr>
<h4 class="footnotes-heading">Footnotes</h4>

<h5><a id="FOOT16" href="#DOCF16">(16)</a></h3>
<p>This description is
adapted from <cite>Common Lisp, The Language</cite>, second edition, section
23.1.1.</p>
</div>
<hr>
<div class="header">
<p>
Next: <a href="Operations-on-Pathnames.html" accesskey="n" rel="next">Operations on Pathnames</a>, Previous: <a href="Filenames-and-Pathnames.html" accesskey="p" rel="prev">Filenames and Pathnames</a>, Up: <a href="Pathnames.html" accesskey="u" rel="up">Pathnames</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
