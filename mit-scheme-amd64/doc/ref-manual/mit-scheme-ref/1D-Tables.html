<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>1D Tables (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="1D Tables (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="1D Tables (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Associations.html" rel="up" title="Associations">
<link href="The-Association-Table.html" rel="next" title="The Association Table">
<link href="Association-Lists.html" rel="prev" title="Association Lists">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="g_t1D-Tables"></span><div class="header">
<p>
Next: <a href="The-Association-Table.html" accesskey="n" rel="next">The Association Table</a>, Previous: <a href="Association-Lists.html" accesskey="p" rel="prev">Association Lists</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="g_t1D-Tables-1"></span><h3 class="section">11.2 1D Tables</h3>

<span id="index-1D-table-_0028defn_0029"></span>
<span id="index-one_002ddimensional-table-_0028defn_0029"></span>
<span id="index-table_002c-one_002ddimensional-_0028defn_0029"></span>
<span id="index-weak-pair_002c-and-1D-table"></span>
<p><em>1D tables</em> (&ldquo;one-dimensional&rdquo; tables) are similar to association
lists.  In a 1D table, unlike an association list, the keys of the table
are held <em>weakly</em>: if a key is garbage-collected, its associated
value in the table is removed.  1D tables compare their keys for
equality using <code>eq?</code>.
</p>
<span id="index-property-list-1"></span>
<p>1D tables can often be used as a higher-performance alternative to the
two-dimensional association table (see <a href="The-Association-Table.html">The Association Table</a>).  If
one of the keys being associated is a compound object such as a vector,
a 1D table can be stored in one of the vector&rsquo;s slots.  Under these
circumstances, accessing items in a 1D table will be comparable in
performance to using a property list in a conventional Lisp.
</p>
<dl>
<dt id="index-make_002d1d_002dtable">procedure: <strong>make-1d-table</strong></dt>
<dd><p>Returns a newly allocated empty 1D table.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_003f">procedure: <strong>1d-table?</strong> <em>object</em></dt>
<dd><span id="index-type-predicate_002c-for-1D-table"></span>
<span id="index-list_003f-2"></span>
<p>Returns <code>#t</code> if <var>object</var> is a 1D table, otherwise returns
<code>#f</code>.  Any object that satisfies this predicate also satisfies
<code>list?</code>.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_002fput_0021">procedure: <strong>1d-table/put!</strong> <em>1d-table key datum</em></dt>
<dd><p>Creates an association between <var>key</var> and <var>datum</var> in
<var>1d-table</var>.  Returns an unspecified value.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_002fremove_0021">procedure: <strong>1d-table/remove!</strong> <em>1d-table key</em></dt>
<dd><p>Removes any association for <var>key</var> in <var>1d-table</var> and returns an
unspecified value.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_002fget">procedure: <strong>1d-table/get</strong> <em>1d-table key default</em></dt>
<dd><p>Returns the <var>datum</var> associated with <var>key</var> in <var>1d-table</var>.  If
there is no association for <var>key</var>, <var>default</var> is returned.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_002flookup">procedure: <strong>1d-table/lookup</strong> <em>1d-table key if-found if-not-found</em></dt>
<dd><p><var>If-found</var> must be a procedure of one argument, and
<var>if-not-found</var> must be a procedure of no arguments.  If
<var>1d-table</var> contains an association for <var>key</var>, <var>if-found</var> is
invoked on the <var>datum</var> of the association.  Otherwise,
<var>if-not-found</var> is invoked with no arguments.  In either case, the
result of the invoked procedure is returned as the result of
<code>1d-table/lookup</code>.
</p></dd></dl>

<dl>
<dt id="index-1d_002dtable_002falist">procedure: <strong>1d-table/alist</strong> <em>1d-table</em></dt>
<dd><p>Returns a newly allocated association list that contains the same
information as <var>1d-table</var>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="The-Association-Table.html" accesskey="n" rel="next">The Association Table</a>, Previous: <a href="Association-Lists.html" accesskey="p" rel="prev">Association Lists</a>, Up: <a href="Associations.html" accesskey="u" rel="up">Associations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
