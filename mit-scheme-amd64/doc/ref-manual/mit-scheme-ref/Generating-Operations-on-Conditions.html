<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual documents MIT/GNU Scheme 11.0.90.

Copyright (C) 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993,
    1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004,
    2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015,
    2016, 2017, 2018, 2019, 2020 Massachusetts Institute of Technology

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU Free
Documentation License." -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Generating Operations on Conditions (MIT/GNU Scheme 11.0.90)</title>

<meta name="description" content="Generating Operations on Conditions (MIT/GNU Scheme 11.0.90)">
<meta name="keywords" content="Generating Operations on Conditions (MIT/GNU Scheme 11.0.90)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Binding-Index.html" rel="index" title="Binding Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Condition-Instances.html" rel="up" title="Condition Instances">
<link href="Condition-State.html" rel="next" title="Condition State">
<link href="Condition-Instances.html" rel="prev" title="Condition Instances">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<span id="Generating-Operations-on-Conditions"></span><div class="header">
<p>
Next: <a href="Condition-State.html" accesskey="n" rel="next">Condition State</a>, Previous: <a href="Condition-Instances.html" accesskey="p" rel="prev">Condition Instances</a>, Up: <a href="Condition-Instances.html" accesskey="u" rel="up">Condition Instances</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Generating-Operations-on-Conditions-1"></span><h4 class="subsection">16.5.1 Generating Operations on Conditions</h4>

<span id="index-condition_002dconstructor"></span>
<span id="index-condition_002daccessor"></span>
<span id="index-condition_002dsignaller"></span>
<span id="index-condition_002dpredicate"></span>
<p>Scheme provides four procedures that take a condition type as input and
produce operations on the corresponding condition object.  These are
reminiscent of the operations on record types that produce record
operators (see <a href="Records.html">Records</a>).  Given a condition type it is possible to
generate: a constructor for instances of the type (using
<code>condition-constructor</code>); an accessor to extract the contents of a
field in instances of the type (using <code>condition-accessor</code>); a
predicate to test for instances of the type (using
<code>condition-predicate</code>); and a procedure to create and signal an
instance of the type (using <code>condition-signaller</code>).
</p>
<p>Notice that the creation of a condition object is distinct from
signalling an occurrence of the condition.  Condition objects are
first-class; they may be created and never signalled, or they may be
signalled more than once.  Further notice that there are no procedures
for modifying conditions; once created, a condition cannot be altered.
</p>
<dl>
<dt id="index-condition_002dconstructor-1">procedure: <strong>condition-constructor</strong> <em>condition-type field-names</em></dt>
<dd><span id="index-condition_002frestarts"></span>
<span id="index-bound_002drestarts"></span>
<span id="index-restarts_002c-bound"></span>
<p>Returns a constructor procedure that takes as arguments values for the
fields specified in <var>field-names</var> and creates a condition of type
<var>condition-type</var>.  <var>Field-names</var> must be a list of symbols that
is a subset of the <var>field-names</var> in <var>condition-type</var>.  The
constructor procedure returned by <code>condition-constructor</code> has
signature
</p>
<div class="example">
<pre class="example">(lambda (<var>continuation</var> <var>restarts</var> . <var>field-values</var>) &hellip;)
</pre></div>

<p>where the <var>field-names</var> correspond to the <var>field-values</var>.  The
constructor argument <var>restarts</var> is described in <a href="Restarts.html">Restarts</a>.
Conditions created by the constructor procedure have <code>#f</code> for the
values of all fields other than those specified by <var>field-names</var>.
</p>
<p>For example, the following procedure <code>make-simple-warning</code>
constructs a condition of type <code>condition-type:simple-warning</code>
given a continuation (where the condition occurred), a description of
the restarts to be made available, a warning message, and a list of
irritants that caused the warning:
</p>
<div class="example">
<pre class="example">(define make-simple-warning
  (condition-constructor condition-type:simple-warning
                         '(message irritants)))
</pre></div>
</dd></dl>

<dl>
<dt id="index-condition_002daccessor-1">procedure: <strong>condition-accessor</strong> <em>condition-type field-name</em></dt>
<dd><span id="index-specialization_002c-of-condition-types-2"></span>
<p>Returns a procedure that takes as input a condition object of type
<var>condition-type</var> and extracts the contents of the specified
<var>field-name</var>.  <code>condition-accessor</code> signals
<code>error:bad-range-argument</code> if the <var>field-name</var> isn&rsquo;t one of the
named fields of <var>condition-type</var>; the returned procedure will signal
<code>error:wrong-type-argument</code> if passed an object other than a
condition of type <var>condition-type</var> or one of its specializations.
</p>
<span id="index-access_002dcondition"></span>
<p>If it is known in advance that a particular field of a condition will be
accessed repeatedly it is worth constructing an accessor for the field
using <code>condition-accessor</code> rather than using the (possibly more
convenient, but slower) <code>access-condition</code> procedure.
</p></dd></dl>

<dl>
<dt id="index-condition_002dpredicate-1">procedure: <strong>condition-predicate</strong> <em>condition-type</em></dt>
<dd><span id="index-specialization_002c-of-condition-types-3"></span>
<p>Returns a predicate procedure for testing whether an object is a
condition of type <var>condition-type</var> or one of its specializations
(there is no predefined way to test for a condition of a given type but
<em>not</em> a specialization of that type).
</p></dd></dl>

<dl>
<dt id="index-condition_002dsignaller-1">procedure: <strong>condition-signaller</strong> <em>condition-type field-names default-handler</em></dt>
<dd><p>Returns a signalling procedure with parameters <var>field-names</var>.  When
the signalling procedure is called it creates and signals a condition of
type <var>condition-type</var>.  If the condition isn&rsquo;t handled (i.e. if no
handler is invoked that causes an escape from the current continuation)
the signalling procedure reduces to a call to <var>default-handler</var> with
the condition as its argument.
</p>
<p>There are several standard procedures that are conventionally used for
<var>default-handler</var>.  If <var>condition-type</var> is a specialization of
<code>condition-type:error</code>, <var>default-handler</var> should be the
procedure<br> <code>standard-error-handler</code>.  If <var>condition-type</var> is a
specialization of <code>condition-type:warning</code>, <var>default-handler</var>
should be the procedure <code>standard-warning-handler</code>.  If
<var>condition-type</var> is a specialization of
<code>condition-type:breakpoint</code>, <var>default-handler</var> should be the
procedure <code>standard-breakpoint-handler</code>.
</p></dd></dl>

<hr>
<div class="header">
<p>
Next: <a href="Condition-State.html" accesskey="n" rel="next">Condition State</a>, Previous: <a href="Condition-Instances.html" accesskey="p" rel="prev">Condition Instances</a>, Up: <a href="Condition-Instances.html" accesskey="u" rel="up">Condition Instances</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Binding-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
