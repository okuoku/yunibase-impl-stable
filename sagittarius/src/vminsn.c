/* Generated automatically from boot/instructions.scm */
/* DO NOT EDIT */
#ifdef DEFINSN
DEFINSN(NOP, 0, 0, FALSE, FALSE)
DEFINSN(HALT, 0, 0, FALSE, FALSE)
DEFINSN(UNDEF, 0, 0, FALSE, FALSE)
DEFINSN(CONST, 0, 1, FALSE, FALSE)
DEFINSN(CONSTI, 1, 0, FALSE, FALSE)
DEFINSN(LREF, 1, 0, TRUE, FALSE)
DEFINSN(LSET, 1, 0, TRUE, FALSE)
DEFINSN(FREF, 1, 0, TRUE, FALSE)
DEFINSN(FSET, 1, 0, TRUE, FALSE)
DEFINSN(GREF, 0, 1, TRUE, FALSE)
DEFINSN(GSET, 0, 1, TRUE, FALSE)
DEFINSN(PUSH, 0, 0, FALSE, FALSE)
DEFINSN(BOX, 1, 0, FALSE, FALSE)
DEFINSN(UNBOX, 0, 0, FALSE, FALSE)
DEFINSN(ADD, 0, 0, TRUE, FALSE)
DEFINSN(ADDI, 1, 0, TRUE, FALSE)
DEFINSN(SUB, 0, 0, TRUE, FALSE)
DEFINSN(SUBI, 1, 0, TRUE, FALSE)
DEFINSN(MUL, 0, 0, TRUE, FALSE)
DEFINSN(MULI, 1, 0, TRUE, FALSE)
DEFINSN(DIV, 0, 0, TRUE, FALSE)
DEFINSN(DIVI, 1, 0, TRUE, FALSE)
DEFINSN(NEG, 0, 0, TRUE, FALSE)
DEFINSN(TEST, 0, 1, TRUE, TRUE)
DEFINSN(JUMP, 0, 1, TRUE, TRUE)
DEFINSN(SHIFTJ, 2, 0, FALSE, FALSE)
DEFINSN(BNNUME, 0, 1, TRUE, TRUE)
DEFINSN(BNLT, 0, 1, TRUE, TRUE)
DEFINSN(BNLE, 0, 1, TRUE, TRUE)
DEFINSN(BNGT, 0, 1, TRUE, TRUE)
DEFINSN(BNGE, 0, 1, TRUE, TRUE)
DEFINSN(BNEQ, 0, 1, TRUE, TRUE)
DEFINSN(BNEQV, 0, 1, TRUE, TRUE)
DEFINSN(BNNULL, 0, 1, TRUE, TRUE)
DEFINSN(NOT, 0, 0, FALSE, FALSE)
DEFINSN(NUM_EQ, 0, 0, TRUE, FALSE)
DEFINSN(NUM_LT, 0, 0, TRUE, FALSE)
DEFINSN(NUM_LE, 0, 0, TRUE, FALSE)
DEFINSN(NUM_GT, 0, 0, TRUE, FALSE)
DEFINSN(NUM_GE, 0, 0, TRUE, FALSE)
DEFINSN(RECEIVE, 2, 0, TRUE, FALSE)
DEFINSN(CLOSURE, 1, 1, FALSE, FALSE)
DEFINSN(APPLY, 2, 0, TRUE, FALSE)
DEFINSN(CALL, 1, 0, TRUE, FALSE)
DEFINSN(LOCAL_CALL, 1, 0, TRUE, FALSE)
DEFINSN(TAIL_CALL, 1, 0, TRUE, FALSE)
DEFINSN(LOCAL_TAIL_CALL, 1, 0, TRUE, FALSE)
DEFINSN(RET, 0, 0, FALSE, FALSE)
DEFINSN(FRAME, 0, 1, FALSE, TRUE)
DEFINSN(INST_STACK, 1, 0, FALSE, FALSE)
DEFINSN(LEAVE, 1, 0, FALSE, FALSE)
DEFINSN(DEFINE, 1, 1, TRUE, FALSE)
DEFINSN(LIBRARY, 0, 1, FALSE, FALSE)
DEFINSN(CAR, 0, 0, TRUE, FALSE)
DEFINSN(CDR, 0, 0, TRUE, FALSE)
DEFINSN(CONS, 0, 0, TRUE, FALSE)
DEFINSN(LIST, 1, 0, TRUE, FALSE)
DEFINSN(APPEND, 1, 0, TRUE, FALSE)
DEFINSN(VALUES, 1, 0, TRUE, FALSE)
DEFINSN(EQ, 0, 0, TRUE, FALSE)
DEFINSN(EQV, 0, 0, TRUE, FALSE)
DEFINSN(NULLP, 0, 0, TRUE, FALSE)
DEFINSN(PAIRP, 0, 0, TRUE, FALSE)
DEFINSN(SYMBOLP, 0, 0, TRUE, FALSE)
DEFINSN(VECTOR, 1, 0, TRUE, FALSE)
DEFINSN(VECTORP, 0, 0, TRUE, FALSE)
DEFINSN(VEC_LEN, 0, 0, TRUE, FALSE)
DEFINSN(VEC_REF, 0, 0, TRUE, FALSE)
DEFINSN(VEC_SET, 0, 0, TRUE, FALSE)
DEFINSN(LREF_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(FREF_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(GREF_PUSH, 0, 1, TRUE, FALSE)
DEFINSN(CONST_PUSH, 0, 1, FALSE, FALSE)
DEFINSN(CONSTI_PUSH, 1, 0, FALSE, FALSE)
DEFINSN(GREF_CALL, 1, 1, TRUE, FALSE)
DEFINSN(GREF_TAIL_CALL, 1, 1, TRUE, FALSE)
DEFINSN(SET_CAR, 0, 0, TRUE, FALSE)
DEFINSN(SET_CDR, 0, 0, TRUE, FALSE)
DEFINSN(CAAR, 0, 0, TRUE, FALSE)
DEFINSN(CADR, 0, 0, TRUE, FALSE)
DEFINSN(CDAR, 0, 0, TRUE, FALSE)
DEFINSN(CDDR, 0, 0, TRUE, FALSE)
DEFINSN(CAR_PUSH, 0, 0, TRUE, FALSE)
DEFINSN(CDR_PUSH, 0, 0, TRUE, FALSE)
DEFINSN(CONS_PUSH, 0, 0, TRUE, FALSE)
DEFINSN(LREF_CAR, 1, 0, TRUE, FALSE)
DEFINSN(LREF_CDR, 1, 0, TRUE, FALSE)
DEFINSN(FREF_CAR, 1, 0, TRUE, FALSE)
DEFINSN(FREF_CDR, 1, 0, TRUE, FALSE)
DEFINSN(GREF_CAR, 0, 1, TRUE, FALSE)
DEFINSN(GREF_CDR, 0, 1, TRUE, FALSE)
DEFINSN(LREF_CAR_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(LREF_CDR_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(FREF_CAR_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(FREF_CDR_PUSH, 1, 0, TRUE, FALSE)
DEFINSN(GREF_CAR_PUSH, 0, 1, TRUE, FALSE)
DEFINSN(GREF_CDR_PUSH, 0, 1, TRUE, FALSE)
DEFINSN(CONST_RET, 0, 1, FALSE, FALSE)
DEFINSN(APPLY_VALUES, 1, 1, FALSE, FALSE)
DEFINSN(RESV_STACK, 1, 0, FALSE, FALSE)
#endif /* DEFINSN */
#ifdef STATE_TABLE
{PUSH, LREF, ARGUMENT0, LREF_PUSH},
{PUSH, FREF, ARGUMENT0, FREF_PUSH},
{PUSH, GREF, ARGUMENT1, GREF_PUSH},
{PUSH, CONST, ARGUMENT1, CONST_PUSH},
{PUSH, CONSTI, ARGUMENT0, CONSTI_PUSH},
{CALL, GREF, ARGUMENT1, GREF_CALL},
{TAIL_CALL, GREF, ARGUMENT1, GREF_TAIL_CALL},
{PUSH, CAR, ARGUMENT0, CAR_PUSH},
{PUSH, CDR, ARGUMENT0, CDR_PUSH},
{PUSH, CONS, ARGUMENT0, CONS_PUSH},
{CAR, LREF, ARGUMENT0, LREF_CAR},
{CDR, LREF, ARGUMENT0, LREF_CDR},
{CAR, FREF, ARGUMENT0, FREF_CAR},
{CDR, FREF, ARGUMENT0, FREF_CDR},
{CAR, GREF, ARGUMENT1, GREF_CAR},
{CDR, GREF, ARGUMENT1, GREF_CDR},
{PUSH, LREF_CAR, ARGUMENT0, LREF_CAR_PUSH},
{PUSH, LREF_CDR, ARGUMENT0, LREF_CDR_PUSH},
{PUSH, FREF_CAR, ARGUMENT0, FREF_CAR_PUSH},
{PUSH, FREF_CDR, ARGUMENT0, FREF_CDR_PUSH},
{PUSH, GREF_CAR, ARGUMENT1, GREF_CAR_PUSH},
{PUSH, GREF_CDR, ARGUMENT1, GREF_CDR_PUSH},
{RET, CONST, ARGUMENT1, CONST_RET},
#endif /* STATE_TABLE */
#ifdef VM_LOOP

label_NOP:
CASE(NOP) 
{
{NEXT;}
}

label_HALT:
CASE(HALT) 
{
{
#line 74 "instructions.scm"
return (AC(vm));}
}

label_UNDEF:
CASE(UNDEF) 
{
{
#line 76 "instructions.scm"
{AC(vm)=(SG_UNDEF),(vm)->valuesCount=(1);NEXT;}}
}

label_CONST:
CASE(CONST) 
{
{
#line 78 "instructions.scm"
{SgObject val=FETCH_OPERAND(PC(vm));
{AC(vm)=(val),(vm)->valuesCount=(1);NEXT;}}}
}

label_CONSTI:
CASE(CONSTI) 
{
{
#line 83 "instructions.scm"
{long cise__1163=INSN_VALUE1(c);{AC(vm)=(SG_MAKE_INT(cise__1163)),(vm)->valuesCount=(1);NEXT;}}}
}

label_LREF:
CASE(LREF) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{AC(vm)=((*((FP(vm))+(val1)))),(vm)->valuesCount=(1);NEXT;}}
}

label_LSET:
CASE(LSET) 
{
int val1;
{
#line 94 "instructions.scm"
INSN_VAL1(val1,c);
#line 95 "instructions.scm"
(SG_BOX((*((FP(vm))+(val1)))))->value=(AC(vm)),
AC(vm)=(SG_UNDEF),
(vm)->valuesCount=(1);NEXT;}
}

label_FREF:
CASE(FREF) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{AC(vm)=(((SG_CLOSURE(CL(vm)))->frees)[val1]),(vm)->valuesCount=(1);NEXT;}}
}

label_FSET:
CASE(FSET) 
{
int val1;
{
#line 109 "instructions.scm"
INSN_VAL1(val1,c);
#line 110 "instructions.scm"
(SG_BOX(((SG_CLOSURE(CL(vm)))->frees)[val1]))->value=(AC(vm)),
AC(vm)=(SG_UNDEF),
(vm)->valuesCount=(1);NEXT;}
}

label_GREF:
CASE(GREF) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1164=FETCH_OPERAND(PC(vm));SgObject s1165=(vm)->sandbox;if (SG_GLOCP(id1164)){if ((!(SG_FALSEP(s1165)))){{{SgObject id1166=Sg_MakeGlobalIdentifier((SG_GLOC(id1164))->name,(SG_GLOC(id1164))->library);{id1164=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1166),SG_IDENTIFIER_NAME(id1166),SG_UNBOUND));if (SG_UNBOUNDP(id1164)){{id1164=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1166),SG_IDENTIFIER_LIBRARY(id1166),id1166));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1164)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1164),SG_IDENTIFIER_NAME(id1164),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1164),SG_IDENTIFIER_LIBRARY(id1164),id1164));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1165)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v),(vm)->valuesCount=(1);NEXT;}}}
}

label_GSET:
CASE(GSET) 
{
{
#line 153 "instructions.scm"
{SgObject var=FETCH_OPERAND(PC(vm));
if (SG_GLOCP(var)){
SG_GLOC_SET(SG_GLOC(var),AC(vm));} else {
{SgObject oldval;
{oldval=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(var),SG_IDENTIFIER_NAME(var),SG_UNBOUND));if (SG_UNBOUNDP(oldval)){{oldval=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(var),SG_IDENTIFIER_LIBRARY(var),var));}}}
{SgObject g=Sg_MakeBinding(SG_IDENTIFIER_LIBRARY(var),
SG_IDENTIFIER_NAME(var),
AC(vm),0);
#line 162 "instructions.scm"
(*((PC(vm))-(1)))=(SG_WORD(g));}}}}
#line 163 "instructions.scm"
AC(vm)=(SG_UNDEF),
(vm)->valuesCount=(1);NEXT;}
}

label_PUSH:
CASE(PUSH) 
{
{
#line 168 "instructions.scm"
PUSH(SP(vm),AC(vm));NEXT;}
}

label_BOX:
CASE(BOX) 
{
int val1;
{
#line 172 "instructions.scm"
INSN_VAL1(val1,c);
#line 173 "instructions.scm"
INDEX_SET(SP(vm),val1,make_box(INDEX(SP(vm),val1)));CHECK_ATTENTION;NEXT;}
}

label_UNBOX:
CASE(UNBOX) 
{
{
#line 179 "instructions.scm"
AC(vm)=((SG_BOX(AC(vm)))->value);NEXT;}
}

label_ADD:
CASE(ADD) 
{
{
#line 188 "instructions.scm"
{SgObject obj=POP(SP(vm));
if ((SG_INTP(AC(vm)))&&(SG_INTP(obj))){
{long cise__1168=(SG_INT_VALUE(obj))+(SG_INT_VALUE(AC(vm)));if (((SG_INT_MIN)<=(cise__1168))&&((SG_INT_MAX)>=(cise__1168))){{AC(vm)=(SG_MAKE_INT(cise__1168)),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(Sg_MakeBignumFromSI(cise__1168)),(vm)->valuesCount=(1);NEXT;}}}}else if(
((SG_FLONUMP(AC(vm)))&&(SG_REALP(obj)))||(
(SG_FLONUMP(obj))&&(SG_REALP(AC(vm))))){
{double cise__1167=(Sg_GetDouble(obj))+(Sg_GetDouble(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1167)),(vm)->valuesCount=(1);NEXT;}}} else {
#line 195 "instructions.scm"
{SgObject v=obj;{AC(vm)=(Sg_Add(v,AC(vm))),(vm)->valuesCount=(1);NEXT;}}}}}
}

label_ADDI:
CASE(ADDI) 
{
int val1;
{
#line 202 "instructions.scm"
INSN_VAL1(val1,c);
#line 203 "instructions.scm"
if (SG_INTP(AC(vm))){
{long cise__1170=(val1)+(SG_INT_VALUE(AC(vm)));if (((SG_INT_MIN)<=(cise__1170))&&((SG_INT_MAX)>=(cise__1170))){{AC(vm)=(SG_MAKE_INT(cise__1170)),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(Sg_MakeBignumFromSI(cise__1170)),(vm)->valuesCount=(1);NEXT;}}}}else if(
SG_FLONUMP(AC(vm))){
{double cise__1169=(((double )(val1)))+(SG_FLONUM_VALUE(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1169)),(vm)->valuesCount=(1);NEXT;}}} else {
#line 208 "instructions.scm"
{AC(vm)=(Sg_Add(SG_MAKE_INT(val1),AC(vm))),(vm)->valuesCount=(1);NEXT;}}}
}

label_SUB:
CASE(SUB) 
{
{
#line 211 "instructions.scm"
{SgObject obj=POP(SP(vm));
if ((SG_INTP(AC(vm)))&&(SG_INTP(obj))){
{long cise__1172=(SG_INT_VALUE(obj))-(SG_INT_VALUE(AC(vm)));if (((SG_INT_MIN)<=(cise__1172))&&((SG_INT_MAX)>=(cise__1172))){{AC(vm)=(SG_MAKE_INT(cise__1172)),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(Sg_MakeBignumFromSI(cise__1172)),(vm)->valuesCount=(1);NEXT;}}}}else if(
((SG_FLONUMP(AC(vm)))&&(SG_REALP(obj)))||(
(SG_FLONUMP(obj))&&(SG_REALP(AC(vm))))){
{double cise__1171=(Sg_GetDouble(obj))-(Sg_GetDouble(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1171)),(vm)->valuesCount=(1);NEXT;}}} else {
#line 218 "instructions.scm"
{SgObject v=obj;{AC(vm)=(Sg_Sub(v,AC(vm))),(vm)->valuesCount=(1);NEXT;}}}}}
}

label_SUBI:
CASE(SUBI) 
{
int val1;
{
#line 221 "instructions.scm"
INSN_VAL1(val1,c);
#line 222 "instructions.scm"
if (SG_INTP(AC(vm))){
{long cise__1174=(val1)-(SG_INT_VALUE(AC(vm)));if (((SG_INT_MIN)<=(cise__1174))&&((SG_INT_MAX)>=(cise__1174))){{AC(vm)=(SG_MAKE_INT(cise__1174)),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(Sg_MakeBignumFromSI(cise__1174)),(vm)->valuesCount=(1);NEXT;}}}}else if(
SG_FLONUMP(AC(vm))){
{double cise__1173=(((double )(val1)))-(SG_FLONUM_VALUE(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1173)),(vm)->valuesCount=(1);NEXT;}}} else {
#line 227 "instructions.scm"
{AC(vm)=(Sg_Sub(SG_MAKE_INT(val1),AC(vm))),(vm)->valuesCount=(1);NEXT;}}}
}

label_MUL:
CASE(MUL) 
{
{
#line 230 "instructions.scm"
{SgObject obj=POP(SP(vm));
if (((SG_FLONUMP(AC(vm)))&&(SG_REALP(obj)))||(
(SG_FLONUMP(obj))&&(SG_REALP(AC(vm))))){
{double cise__1175=(Sg_GetDouble(obj))*(Sg_GetDouble(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1175)),(vm)->valuesCount=(1);NEXT;}}} else {
{SgObject v=obj;{AC(vm)=(Sg_Mul(v,AC(vm))),(vm)->valuesCount=(1);NEXT;}}}}}
}

label_MULI:
CASE(MULI) 
{
int val1;
{
#line 237 "instructions.scm"
INSN_VAL1(val1,c);
#line 238 "instructions.scm"
if (SG_FLONUMP(AC(vm))){
{double cise__1176=(((double )(val1)))*(SG_FLONUM_VALUE(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1176)),(vm)->valuesCount=(1);NEXT;}}} else {
{AC(vm)=(Sg_Mul(SG_MAKE_INT(val1),AC(vm))),(vm)->valuesCount=(1);NEXT;}}}
}

label_DIV:
CASE(DIV) 
{
{
#line 249 "instructions.scm"
{SgObject obj=POP(SP(vm));int exact=
(Sg_ExactP(obj))&&(Sg_ExactP(AC(vm)));
if ((exact)&&(Sg_ZeroP(AC(vm)))){
{Sg_AssertionViolation(SG_INTERN("/"),SG_MAKE_STRING("undefined for 0"),SG_LIST2(obj,AC(vm)));return (SG_UNDEF);}}else if(
((SG_FLONUMP(AC(vm)))&&(SG_REALP(obj)))||(
(SG_FLONUMP(obj))&&(SG_REALP(AC(vm))))){
{double cise__1177=(Sg_GetDouble(obj))/(Sg_GetDouble(AC(vm)));{AC(vm)=(Sg_MakeFlonum(cise__1177)),(vm)->valuesCount=(1);NEXT;}}} else {
{SgObject v=obj;{AC(vm)=(Sg_Div(v,AC(vm))),(vm)->valuesCount=(1);NEXT;}}}}}
}

label_DIVI:
CASE(DIVI) 
{
int val1;
{
#line 259 "instructions.scm"
INSN_VAL1(val1,c);
#line 260 "instructions.scm"
{AC(vm)=(Sg_Div(SG_MAKE_INT(val1),AC(vm))),(vm)->valuesCount=(1);NEXT;}}
}

label_NEG:
CASE(NEG) 
{
{
#line 266 "instructions.scm"
{AC(vm)=(Sg_Negate(AC(vm))),(vm)->valuesCount=(1);NEXT;}}
}

label_TEST:
CASE(TEST) 
{
{
#line 269 "instructions.scm"
if (SG_FALSEP(AC(vm))){
(PC(vm))+=(PEEK_OPERAND(PC(vm)));} else {
#line 272 "instructions.scm"
(PC(vm))++;}CHECK_ATTENTION;NEXT;}
}

label_JUMP:
CASE(JUMP) 
{
{
#line 277 "instructions.scm"
(PC(vm))+=(PEEK_OPERAND(PC(vm)));CHECK_ATTENTION;NEXT;}
}

label_SHIFTJ:
CASE(SHIFTJ) 
{
int val1;
int val2;
{
#line 282 "instructions.scm"
INSN_VAL2(val1,val2,c);
#line 283 "instructions.scm"
SP(vm)=(shift_args((FP(vm))+(val2),val1,SP(vm)));NEXT;}
}

label_BNNUME:
CASE(BNNUME) 
{
{
#line 314 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){if ((((intptr_t )(s)))==(((intptr_t )(AC(vm))))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}else if((SG_FLONUMP(AC(vm)))&&(SG_FLONUMP(s))){if ((SG_FLONUM_VALUE(s))==(SG_FLONUM_VALUE(AC(vm)))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}} else {if (Sg_NumEq(s,AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}CHECK_ATTENTION;NEXT;}}
}

label_BNLT:
CASE(BNLT) 
{
{
#line 317 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){if ((((intptr_t )(s)))<(((intptr_t )(AC(vm))))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}else if((SG_FLONUMP(AC(vm)))&&(SG_FLONUMP(s))){if ((SG_FLONUM_VALUE(s))<(SG_FLONUM_VALUE(AC(vm)))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}} else {if (Sg_NumLt(s,AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}CHECK_ATTENTION;NEXT;}}
}

label_BNLE:
CASE(BNLE) 
{
{
#line 320 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){if ((((intptr_t )(s)))<=(((intptr_t )(AC(vm))))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}else if((SG_FLONUMP(AC(vm)))&&(SG_FLONUMP(s))){if ((SG_FLONUM_VALUE(s))<=(SG_FLONUM_VALUE(AC(vm)))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}} else {if (Sg_NumLe(s,AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}CHECK_ATTENTION;NEXT;}}
}

label_BNGT:
CASE(BNGT) 
{
{
#line 323 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){if ((((intptr_t )(s)))>(((intptr_t )(AC(vm))))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}else if((SG_FLONUMP(AC(vm)))&&(SG_FLONUMP(s))){if ((SG_FLONUM_VALUE(s))>(SG_FLONUM_VALUE(AC(vm)))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}} else {if (Sg_NumGt(s,AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}CHECK_ATTENTION;NEXT;}}
}

label_BNGE:
CASE(BNGE) 
{
{
#line 326 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){if ((((intptr_t )(s)))>=(((intptr_t )(AC(vm))))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}else if((SG_FLONUMP(AC(vm)))&&(SG_FLONUMP(s))){if ((SG_FLONUM_VALUE(s))>=(SG_FLONUM_VALUE(AC(vm)))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}} else {if (Sg_NumGe(s,AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}}CHECK_ATTENTION;NEXT;}}
}

label_BNEQ:
CASE(BNEQ) 
{
{
#line 342 "instructions.scm"
{if (SG_EQ(POP(SP(vm)),AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}CHECK_ATTENTION;NEXT;}}
}

label_BNEQV:
CASE(BNEQV) 
{
{
#line 345 "instructions.scm"
{if (Sg_EqvP(POP(SP(vm)),AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}CHECK_ATTENTION;NEXT;}}
}

label_BNNULL:
CASE(BNNULL) 
{
{
#line 361 "instructions.scm"
{if (SG_NULLP(AC(vm))){{AC(vm)=(SG_TRUE);(PC(vm))++;}} else {{AC(vm)=(SG_FALSE);(PC(vm))+=(PEEK_OPERAND(PC(vm)));}}CHECK_ATTENTION;NEXT;}}
}

label_NOT:
CASE(NOT) 
{
{
#line 364 "instructions.scm"
{AC(vm)=(SG_MAKE_BOOL(SG_FALSEP(AC(vm)))),(vm)->valuesCount=(1);NEXT;}}
}

label_NUM_EQ:
CASE(NUM_EQ) 
{
{
#line 374 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){{AC(vm)=(SG_MAKE_BOOL((((intptr_t )(s)))==(((intptr_t )(AC(vm)))))),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(SG_MAKE_BOOL(Sg_NumEq(s,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}}
}

label_NUM_LT:
CASE(NUM_LT) 
{
{
#line 377 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){{AC(vm)=(SG_MAKE_BOOL((((intptr_t )(s)))<(((intptr_t )(AC(vm)))))),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(SG_MAKE_BOOL(Sg_NumLt(s,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}}
}

label_NUM_LE:
CASE(NUM_LE) 
{
{
#line 380 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){{AC(vm)=(SG_MAKE_BOOL((((intptr_t )(s)))<=(((intptr_t )(AC(vm)))))),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(SG_MAKE_BOOL(Sg_NumLe(s,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}}
}

label_NUM_GT:
CASE(NUM_GT) 
{
{
#line 383 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){{AC(vm)=(SG_MAKE_BOOL((((intptr_t )(s)))>(((intptr_t )(AC(vm)))))),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(SG_MAKE_BOOL(Sg_NumGt(s,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}}
}

label_NUM_GE:
CASE(NUM_GE) 
{
{
#line 386 "instructions.scm"
{SgObject s=POP(SP(vm));if ((SG_INTP(AC(vm)))&&(SG_INTP(s))){{AC(vm)=(SG_MAKE_BOOL((((intptr_t )(s)))>=(((intptr_t )(AC(vm)))))),(vm)->valuesCount=(1);NEXT;}} else {{AC(vm)=(SG_MAKE_BOOL(Sg_NumGe(s,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}}
}

label_RECEIVE:
CASE(RECEIVE) 
{
int val1;
int val2;
{
#line 389 "instructions.scm"
INSN_VAL2(val1,val2,c);
#line 390 "instructions.scm"
{int numValues=(vm)->valuesCount;
if ((numValues)<(val1)){{
{Sg_AssertionViolation(SG_INTERN("receive"),SG_MAKE_STRING("recieved fewer values than expected"),
#line 394 "instructions.scm"
AC(vm));return (SG_UNDEF);}}}
if (((val2)==(0))&&((numValues)>(val1))){{
{Sg_AssertionViolation(SG_INTERN("receive"),SG_MAKE_STRING("recieved more values than expected"),
#line 398 "instructions.scm"
AC(vm));return (SG_UNDEF);}}}
if ((val2)==(0)){
#line 401 "instructions.scm"
if ((val1)>(0)){{PUSH(SP(vm),AC(vm));}}
{int i=0;int cise__1179=(val1)-(1);for (;(i)<(cise__1179);(i)++){
PUSH(SP(vm),SG_VALUES_REF(vm,i));}}}else if(
(val1)==(0)){
#line 406 "instructions.scm"
{SgObject h=SG_NIL;SgObject t=SG_NIL;
if ((numValues)>(0)){{SG_APPEND1(h,t,AC(vm));}}
if ((numValues)>(1)){{
{int i=0;int cise__1178=(numValues)-(1);for (;(i)<(cise__1178);(i)++){
SG_APPEND1(h,t,SG_VALUES_REF(vm,i));}}}}
PUSH(SP(vm),h);}} else {
#line 414 "instructions.scm"
{SgObject h=SG_NIL;SgObject t=SG_NIL;int i=0;
PUSH(SP(vm),AC(vm));
for (;(i)<((numValues)-(1));(i)++){
if ((i)<((val1)-(1))){
PUSH(SP(vm),SG_VALUES_REF(vm,i));} else {
SG_APPEND1(h,t,SG_VALUES_REF(vm,i));}}
PUSH(SP(vm),h);}}}
#line 421 "instructions.scm"
(vm)->valuesCount=(1);NEXT;}
}

label_CLOSURE:
CASE(CLOSURE) 
{
int val1;
{
#line 429 "instructions.scm"
INSN_VAL1(val1,c);
#line 430 "instructions.scm"
{SgObject cb=FETCH_OPERAND(PC(vm));
#line 434 "instructions.scm"
(SP(vm))-=(SG_CODE_BUILDER_FREEC(cb));
{AC(vm)=(Sg_VMMakeClosure(cb,val1,SP(vm))),(vm)->valuesCount=(1);NEXT;}}}
}

label_APPLY:
CASE(APPLY) 
{
int val1;
int val2;
{
#line 453 "instructions.scm"
INSN_VAL2(val1,val2,c);
#line 454 "instructions.scm"
{long rargc=Sg_Length(AC(vm));int nargc=
(val1)-(2);SgObject proc=
INDEX(SP(vm),nargc);SgObject* fp=
(SP(vm))-((val1)-(1));
if ((rargc)<(0)){{
{Sg_AssertionViolation(SG_INTERN("apply"),SG_MAKE_STRING("improper list not allowed"),AC(vm));return (SG_UNDEF);}}}
shift_args(fp,nargc,SP(vm));
if ((rargc)==(0)){
(SP(vm))--;
if (val2){{
SP(vm)=(shift_args(FP(vm),nargc,SP(vm)));}}
AC(vm)=(proc);
#line 468 "instructions.scm"
c=(MERGE_INSN_VALUE1(CALL,nargc));
goto label_CALL;} else {
#line 471 "instructions.scm"
INDEX_SET(SP(vm),0,AC(vm));
if (val2){{
SP(vm)=(shift_args(FP(vm),(nargc)+(1),SP(vm)));}}
c=(MERGE_INSN_VALUE1(CALL,(nargc)+(1)));
AC(vm)=(proc);
goto tail_apply_entry;}}}
}

label_CALL:
CASE(CALL) 
{
{
#line 479 "instructions.scm"

#undef APPLY_CALL

#line 480 "instructions.scm"

#include "vmcall.c"

#line 481 "instructions.scm"
tail_apply_entry :; 
#line 482 "instructions.scm"

#define APPLY_CALL

#line 483 "instructions.scm"

#include "vmcall.c"
}
}

label_LOCAL_CALL:
CASE(LOCAL_CALL) 
{
int val1;
{
#line 501 "instructions.scm"
CHECK_STACK(SG_CLOSURE_MAX_STACK(AC(vm)),vm);
#line 502 "instructions.scm"
{INSN_VAL1(val1,c);
#if defined(SHOW_CALL_TRACE)
if ((SG_VM_LOG_LEVEL(vm,SG_TRACE_LEVEL))&&(((vm)->state)==(RUNNING))){{Sg_Printf((vm)->logPort,UC(";; calling %S\n"),AC(vm));}}
#endif /* defined(SHOW_CALL_TRACE) */
SG_PROF_COUNT_CALL(vm,AC(vm));{SgCodeBuilder* cb=(SG_CLOSURE(AC(vm)))->code;CL(vm)=(AC(vm)),PC(vm)=((cb)->code),FP(vm)=((SP(vm))-(val1));}}CHECK_ATTENTION;NEXT;}
}

label_TAIL_CALL:
CASE(TAIL_CALL) 
{
int val1;
{
#line 513 "instructions.scm"
{INSN_VAL1(val1,c);SP(vm)=(shift_args(FP(vm),val1,SP(vm)));}
#line 514 "instructions.scm"
goto label_CALL;}
}

label_LOCAL_TAIL_CALL:
CASE(LOCAL_TAIL_CALL) 
{
int val1;
{
#line 517 "instructions.scm"
CHECK_STACK(SG_CLOSURE_MAX_STACK(AC(vm)),vm);
#line 518 "instructions.scm"
{INSN_VAL1(val1,c);SP(vm)=(shift_args(FP(vm),val1,SP(vm)));}
#line 519 "instructions.scm"
{INSN_VAL1(val1,c);
#if defined(SHOW_CALL_TRACE)
if ((SG_VM_LOG_LEVEL(vm,SG_TRACE_LEVEL))&&(((vm)->state)==(RUNNING))){{Sg_Printf((vm)->logPort,UC(";; calling %S\n"),AC(vm));}}
#endif /* defined(SHOW_CALL_TRACE) */
SG_PROF_COUNT_CALL(vm,AC(vm));{SgCodeBuilder* cb=(SG_CLOSURE(AC(vm)))->code;CL(vm)=(AC(vm)),PC(vm)=((cb)->code),FP(vm)=((SP(vm))-(val1));}}CHECK_ATTENTION;NEXT;}
}

label_RET:
CASE(RET) 
{
{
#line 524 "instructions.scm"
RET_INSN();CHECK_ATTENTION;NEXT;}
}

label_FRAME:
CASE(FRAME) 
{
{
#line 529 "instructions.scm"
{intptr_t n=((intptr_t )(FETCH_OPERAND(PC(vm))));
PUSH_CONT(vm,(PC(vm))+((n)-(1)));}CHECK_ATTENTION;NEXT;}
}

label_INST_STACK:
CASE(INST_STACK) 
{
int val1;
{
#line 537 "instructions.scm"
INSN_VAL1(val1,c);
#line 538 "instructions.scm"
(*((FP(vm))+(val1)))=(AC(vm));NEXT;}
}

label_LEAVE:
CASE(LEAVE) 
{
int val1;
{
#line 542 "instructions.scm"
INSN_VAL1(val1,c);
#line 543 "instructions.scm"
(SP(vm))-=(val1);NEXT;}
}

label_DEFINE:
CASE(DEFINE) 
{
int val1;
{
#line 547 "instructions.scm"
INSN_VAL1(val1,c);
#line 548 "instructions.scm"
{SgObject var=FETCH_OPERAND(PC(vm));
ASSERT(SG_IDENTIFIERP(var));
Sg_MakeBinding(SG_IDENTIFIER_LIBRARY(var),
SG_IDENTIFIER_NAME(var),
AC(vm),val1);
#line 554 "instructions.scm"
AC(vm)=(SG_UNDEF);}CHECK_ATTENTION;NEXT;}
}

label_LIBRARY:
CASE(LIBRARY) 
{
{
#line 562 "instructions.scm"
{SgObject lib=Sg_FindLibrary(FETCH_OPERAND(PC(vm)),FALSE);
(vm)->currentLibrary=(((SgLibrary* )(lib)));}CHECK_ATTENTION;NEXT;}
}

label_CAR:
CASE(CAR) 
{
{
#line 568 "instructions.scm"
if (SG_PAIRP(AC(vm))){
{AC(vm)=(SG_CAR(AC(vm))),(vm)->valuesCount=(1);NEXT;}} else {
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("car"),SG_MAKE_STRING("pair"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
}

label_CDR:
CASE(CDR) 
{
{
#line 573 "instructions.scm"
if (SG_PAIRP(AC(vm))){
{AC(vm)=(SG_CDR(AC(vm))),(vm)->valuesCount=(1);NEXT;}} else {
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cdr"),SG_MAKE_STRING("pair"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
}

label_CONS:
CASE(CONS) 
{
{
#line 578 "instructions.scm"
{SgObject v=POP(SP(vm));{AC(vm)=(Sg_Cons(v,AC(vm))),(vm)->valuesCount=(1);NEXT;}}}
}

label_LIST:
CASE(LIST) 
{
int val1;
{
#line 581 "instructions.scm"
INSN_VAL1(val1,c);
#line 582 "instructions.scm"
{int n=(val1)-(1);SgObject ret=SG_NIL;
#line 584 "instructions.scm"
if ((val1)>(0)){{
ret=(Sg_Cons(AC(vm),ret));
{int i=0;int cise__1180=n;for (;(i)<(cise__1180);(i)++){
ret=(Sg_Cons(INDEX(SP(vm),i),ret));}}
(SP(vm))-=(n);}}
{AC(vm)=(ret),(vm)->valuesCount=(1);NEXT;}}}
}

label_APPEND:
CASE(APPEND) 
{
int val1;
{
#line 592 "instructions.scm"
INSN_VAL1(val1,c);
#line 593 "instructions.scm"
{int nargs=(val1)-(1);SgObject ret=SG_NIL;
#line 595 "instructions.scm"
if ((val1)>(0)){{
ret=(AC(vm));
{int i=0;int cise__1181=nargs;for (;(i)<(cise__1181);(i)++){
{SgObject obj=INDEX(SP(vm),i);
if ((Sg_Length(obj))<(0)){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("append"),SG_MAKE_STRING("list"),obj,SG_NIL);return (SG_UNDEF);}}}
ret=(Sg_Append2(obj,ret));}}}
(SP(vm))-=(nargs);}}
{AC(vm)=(ret),(vm)->valuesCount=(1);NEXT;}}}
}

label_VALUES:
CASE(VALUES) 
{
int val1;
{
#line 606 "instructions.scm"
INSN_VAL1(val1,c);
#line 607 "instructions.scm"
{SgObject v=AC(vm);int n=(val1)-(1);
(vm)->valuesCount=(val1);
if ((n)>(DEFAULT_VALUES_SIZE)){{
SG_ALLOC_VALUES_BUFFER(vm,(n)-(DEFAULT_VALUES_SIZE));}}
for (;(n)>(0);(n)--){
SG_VALUES_SET(vm,(n)-(1),v);
v=(POP(SP(vm)));}
AC(vm)=(v);}NEXT;}
}

label_EQ:
CASE(EQ) 
{
{
#line 623 "instructions.scm"
{SgObject v=POP(SP(vm));{AC(vm)=(SG_MAKE_BOOL(SG_EQ(v,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}
}

label_EQV:
CASE(EQV) 
{
{
#line 626 "instructions.scm"
{SgObject v=POP(SP(vm));{AC(vm)=(SG_MAKE_BOOL(Sg_EqvP(v,AC(vm)))),(vm)->valuesCount=(1);NEXT;}}}
}

label_NULLP:
CASE(NULLP) 
{
{
#line 629 "instructions.scm"
{AC(vm)=(SG_MAKE_BOOL(SG_NULLP(AC(vm)))),(vm)->valuesCount=(1);NEXT;}}
}

label_PAIRP:
CASE(PAIRP) 
{
{
#line 632 "instructions.scm"
{AC(vm)=(SG_MAKE_BOOL(SG_PAIRP(AC(vm)))),(vm)->valuesCount=(1);NEXT;}}
}

label_SYMBOLP:
CASE(SYMBOLP) 
{
{
#line 635 "instructions.scm"
{AC(vm)=(SG_MAKE_BOOL(SG_SYMBOLP(AC(vm)))),(vm)->valuesCount=(1);NEXT;}}
}

label_VECTOR:
CASE(VECTOR) 
{
int val1;
{
#line 638 "instructions.scm"
{SgObject v=SG_UNDEF;
INSN_VAL1(val1,c);
v=(Sg_MakeVector(val1,SG_UNDEF));
if ((val1)>(0)){
{int i=0;int n=
(val1)-(1);
SG_VECTOR_ELEMENT(v,n)=(AC(vm));
for (i=(0);(i)<(n);(i)++){
SG_VECTOR_ELEMENT(v,((n)-(i))-(1))=(
INDEX(SP(vm),i));}
(SP(vm))-=(n);}}
{AC(vm)=(v),(vm)->valuesCount=(1);NEXT;}}}
}

label_VECTORP:
CASE(VECTORP) 
{
{
#line 652 "instructions.scm"
{AC(vm)=(SG_MAKE_BOOL(SG_VECTORP(AC(vm)))),(vm)->valuesCount=(1);NEXT;}}
}

label_VEC_LEN:
CASE(VEC_LEN) 
{
{
#line 655 "instructions.scm"
if (SG_VECTORP(AC(vm))){
{long cise__1182=SG_VECTOR_SIZE(AC(vm));{AC(vm)=(SG_MAKE_INT(cise__1182)),(vm)->valuesCount=(1);NEXT;}}} else {
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("vector-length"),SG_MAKE_STRING("vector"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
}

label_VEC_REF:
CASE(VEC_REF) 
{
{
#line 660 "instructions.scm"
{SgObject obj=POP(SP(vm));
if ((!(SG_VECTORP(obj)))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("vector-ref"),SG_MAKE_STRING("vector"),obj,SG_NIL);return (SG_UNDEF);}}}
if ((!(SG_INTP(AC(vm))))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("vector-ref"),SG_MAKE_STRING("fixnum"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
{long index=SG_INT_VALUE(AC(vm));
if (((index)>=(SG_VECTOR_SIZE(obj)))||((index)<(0))){{
{Sg_AssertionViolation(SG_INTERN("vector-ref"),SG_MAKE_STRING("index out of range"),
SG_LIST2(obj,AC(vm)));return (SG_UNDEF);}}}
{AC(vm)=(SG_VECTOR_ELEMENT(obj,index)),(vm)->valuesCount=(1);NEXT;}}}}
}

label_VEC_SET:
CASE(VEC_SET) 
{
{
#line 672 "instructions.scm"
{SgObject index=POP(SP(vm));SgObject obj=
POP(SP(vm));
if ((!(SG_VECTORP(obj)))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("vector-set!"),SG_MAKE_STRING("vector"),obj,SG_NIL);return (SG_UNDEF);}}}
if (SG_LITERAL_VECTORP(obj)){{
{Sg_AssertionViolation(SG_INTERN("vector-set!"),SG_MAKE_STRING("attempt to modify immutable vector"),
#line 679 "instructions.scm"
SG_LIST1(obj));return (SG_UNDEF);}}}
if ((!(SG_INTP(index)))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("vector-set!"),SG_MAKE_STRING("fixnum"),index,SG_NIL);return (SG_UNDEF);}}}
{long i=SG_INT_VALUE(index);
if (((i)>=(SG_VECTOR_SIZE(obj)))||((i)<(0))){{
{Sg_AssertionViolation(SG_INTERN("vector-set!"),SG_MAKE_STRING("index out of range"),
SG_LIST2(obj,index));return (SG_UNDEF);}}}
SG_VECTOR_ELEMENT(obj,i)=(AC(vm));
{AC(vm)=(SG_UNDEF),(vm)->valuesCount=(1);NEXT;}}}}
}

label_LREF_PUSH:
CASE(LREF_PUSH) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{PUSH(SP(vm),(*((FP(vm))+(val1))));(vm)->valuesCount=(1);NEXT;}}
}

label_FREF_PUSH:
CASE(FREF_PUSH) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{PUSH(SP(vm),((SG_CLOSURE(CL(vm)))->frees)[val1]);(vm)->valuesCount=(1);NEXT;}}
}

label_GREF_PUSH:
CASE(GREF_PUSH) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1183=FETCH_OPERAND(PC(vm));SgObject s1184=(vm)->sandbox;if (SG_GLOCP(id1183)){if ((!(SG_FALSEP(s1184)))){{{SgObject id1185=Sg_MakeGlobalIdentifier((SG_GLOC(id1183))->name,(SG_GLOC(id1183))->library);{id1183=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1185),SG_IDENTIFIER_NAME(id1185),SG_UNBOUND));if (SG_UNBOUNDP(id1183)){{id1183=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1185),SG_IDENTIFIER_LIBRARY(id1185),id1185));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1183)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1183),SG_IDENTIFIER_NAME(id1183),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1183),SG_IDENTIFIER_LIBRARY(id1183),id1183));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1184)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{PUSH(SP(vm),v);(vm)->valuesCount=(1);NEXT;}}}
}

label_CONST_PUSH:
CASE(CONST_PUSH) 
{
{
#line 78 "instructions.scm"
{SgObject val=FETCH_OPERAND(PC(vm));
{PUSH(SP(vm),val);(vm)->valuesCount=(1);NEXT;}}}
}

label_CONSTI_PUSH:
CASE(CONSTI_PUSH) 
{
{
#line 83 "instructions.scm"
{long cise__1186=INSN_VALUE1(c);{PUSH(SP(vm),SG_MAKE_INT(cise__1186));(vm)->valuesCount=(1);NEXT;}}}
}

label_GREF_CALL:
CASE(GREF_CALL) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1187=FETCH_OPERAND(PC(vm));SgObject s1188=(vm)->sandbox;if (SG_GLOCP(id1187)){if ((!(SG_FALSEP(s1188)))){{{SgObject id1189=Sg_MakeGlobalIdentifier((SG_GLOC(id1187))->name,(SG_GLOC(id1187))->library);{id1187=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1189),SG_IDENTIFIER_NAME(id1189),SG_UNBOUND));if (SG_UNBOUNDP(id1187)){{id1187=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1189),SG_IDENTIFIER_LIBRARY(id1189),id1189));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1187)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1187),SG_IDENTIFIER_NAME(id1187),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1187),SG_IDENTIFIER_LIBRARY(id1187),id1187));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1188)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_CALL;}
}

label_GREF_TAIL_CALL:
CASE(GREF_TAIL_CALL) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1190=FETCH_OPERAND(PC(vm));SgObject s1191=(vm)->sandbox;if (SG_GLOCP(id1190)){if ((!(SG_FALSEP(s1191)))){{{SgObject id1192=Sg_MakeGlobalIdentifier((SG_GLOC(id1190))->name,(SG_GLOC(id1190))->library);{id1190=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1192),SG_IDENTIFIER_NAME(id1192),SG_UNBOUND));if (SG_UNBOUNDP(id1190)){{id1190=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1192),SG_IDENTIFIER_LIBRARY(id1192),id1192));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1190)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1190),SG_IDENTIFIER_NAME(id1190),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1190),SG_IDENTIFIER_LIBRARY(id1190),id1190));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1191)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_TAIL_CALL;}
}

label_SET_CAR:
CASE(SET_CAR) 
{
{
#line 712 "instructions.scm"
{SgObject obj=POP(SP(vm));
if ((!(SG_PAIRP(obj)))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("set-car!"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}
if (Sg_ConstantLiteralP(obj)){{
{Sg_AssertionViolation(SG_INTERN("set-car!"),SG_MAKE_STRING("attempt to modify constant literal"),obj);return (SG_UNDEF);}}}
SG_SET_CAR(obj,AC(vm));
{AC(vm)=(SG_UNDEF),(vm)->valuesCount=(1);NEXT;}}}
}

label_SET_CDR:
CASE(SET_CDR) 
{
{
#line 722 "instructions.scm"
{SgObject obj=POP(SP(vm));
if ((!(SG_PAIRP(obj)))){{
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("set-cdr!"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}
if (Sg_ConstantLiteralP(obj)){{
{Sg_AssertionViolation(SG_INTERN("set-cdr!"),SG_MAKE_STRING("attempt to modify constant literal"),obj);return (SG_UNDEF);}}}
SG_SET_CDR(obj,AC(vm));
{AC(vm)=(SG_UNDEF),(vm)->valuesCount=(1);NEXT;}}}
}

label_CAAR:
CASE(CAAR) 
{
{
#line 740 "instructions.scm"
{SgObject obj=AC(vm);if (SG_PAIRP(obj)){{SgObject obj2=SG_CAR(obj);if (SG_PAIRP(obj2)){{AC(vm)=(SG_CAR(obj2)),(vm)->valuesCount=(1);NEXT;}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("caar"),SG_MAKE_STRING("pair"),obj2,obj);return (SG_UNDEF);}}}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("caar"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}}
}

label_CADR:
CASE(CADR) 
{
{
#line 741 "instructions.scm"
{SgObject obj=AC(vm);if (SG_PAIRP(obj)){{SgObject obj2=SG_CDR(obj);if (SG_PAIRP(obj2)){{AC(vm)=(SG_CAR(obj2)),(vm)->valuesCount=(1);NEXT;}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cadr"),SG_MAKE_STRING("pair"),obj2,obj);return (SG_UNDEF);}}}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cadr"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}}
}

label_CDAR:
CASE(CDAR) 
{
{
#line 742 "instructions.scm"
{SgObject obj=AC(vm);if (SG_PAIRP(obj)){{SgObject obj2=SG_CAR(obj);if (SG_PAIRP(obj2)){{AC(vm)=(SG_CDR(obj2)),(vm)->valuesCount=(1);NEXT;}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cdar"),SG_MAKE_STRING("pair"),obj2,obj);return (SG_UNDEF);}}}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cdar"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}}
}

label_CDDR:
CASE(CDDR) 
{
{
#line 743 "instructions.scm"
{SgObject obj=AC(vm);if (SG_PAIRP(obj)){{SgObject obj2=SG_CDR(obj);if (SG_PAIRP(obj2)){{AC(vm)=(SG_CDR(obj2)),(vm)->valuesCount=(1);NEXT;}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cddr"),SG_MAKE_STRING("pair"),obj2,obj);return (SG_UNDEF);}}}} else {{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cddr"),SG_MAKE_STRING("pair"),obj,SG_NIL);return (SG_UNDEF);}}}}
}

label_CAR_PUSH:
CASE(CAR_PUSH) 
{
{
#line 568 "instructions.scm"
if (SG_PAIRP(AC(vm))){
{PUSH(SP(vm),SG_CAR(AC(vm)));(vm)->valuesCount=(1);NEXT;}} else {
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("car"),SG_MAKE_STRING("pair"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
}

label_CDR_PUSH:
CASE(CDR_PUSH) 
{
{
#line 573 "instructions.scm"
if (SG_PAIRP(AC(vm))){
{PUSH(SP(vm),SG_CDR(AC(vm)));(vm)->valuesCount=(1);NEXT;}} else {
{Sg_WrongTypeOfArgumentViolation(SG_INTERN("cdr"),SG_MAKE_STRING("pair"),AC(vm),SG_NIL);return (SG_UNDEF);}}}
}

label_CONS_PUSH:
CASE(CONS_PUSH) 
{
{
#line 578 "instructions.scm"
{SgObject v=POP(SP(vm));{PUSH(SP(vm),Sg_Cons(v,AC(vm)));(vm)->valuesCount=(1);NEXT;}}}
}

label_LREF_CAR:
CASE(LREF_CAR) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{AC(vm)=((*((FP(vm))+(val1))));}}
{goto label_CAR;}
}

label_LREF_CDR:
CASE(LREF_CDR) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{AC(vm)=((*((FP(vm))+(val1))));}}
{goto label_CDR;}
}

label_FREF_CAR:
CASE(FREF_CAR) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{AC(vm)=(((SG_CLOSURE(CL(vm)))->frees)[val1]);}}
{goto label_CAR;}
}

label_FREF_CDR:
CASE(FREF_CDR) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{AC(vm)=(((SG_CLOSURE(CL(vm)))->frees)[val1]);}}
{goto label_CDR;}
}

label_GREF_CAR:
CASE(GREF_CAR) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1193=FETCH_OPERAND(PC(vm));SgObject s1194=(vm)->sandbox;if (SG_GLOCP(id1193)){if ((!(SG_FALSEP(s1194)))){{{SgObject id1195=Sg_MakeGlobalIdentifier((SG_GLOC(id1193))->name,(SG_GLOC(id1193))->library);{id1193=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1195),SG_IDENTIFIER_NAME(id1195),SG_UNBOUND));if (SG_UNBOUNDP(id1193)){{id1193=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1195),SG_IDENTIFIER_LIBRARY(id1195),id1195));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1193)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1193),SG_IDENTIFIER_NAME(id1193),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1193),SG_IDENTIFIER_LIBRARY(id1193),id1193));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1194)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_CAR;}
}

label_GREF_CDR:
CASE(GREF_CDR) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1196=FETCH_OPERAND(PC(vm));SgObject s1197=(vm)->sandbox;if (SG_GLOCP(id1196)){if ((!(SG_FALSEP(s1197)))){{{SgObject id1198=Sg_MakeGlobalIdentifier((SG_GLOC(id1196))->name,(SG_GLOC(id1196))->library);{id1196=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1198),SG_IDENTIFIER_NAME(id1198),SG_UNBOUND));if (SG_UNBOUNDP(id1196)){{id1196=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1198),SG_IDENTIFIER_LIBRARY(id1198),id1198));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1196)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1196),SG_IDENTIFIER_NAME(id1196),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1196),SG_IDENTIFIER_LIBRARY(id1196),id1196));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1197)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_CDR;}
}

label_LREF_CAR_PUSH:
CASE(LREF_CAR_PUSH) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{AC(vm)=((*((FP(vm))+(val1))));}}
{goto label_CAR_PUSH;}
}

label_LREF_CDR_PUSH:
CASE(LREF_CDR_PUSH) 
{
int val1;
{
#line 90 "instructions.scm"
INSN_VAL1(val1,c);
#line 91 "instructions.scm"
{AC(vm)=((*((FP(vm))+(val1))));}}
{goto label_CDR_PUSH;}
}

label_FREF_CAR_PUSH:
CASE(FREF_CAR_PUSH) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{AC(vm)=(((SG_CLOSURE(CL(vm)))->frees)[val1]);}}
{goto label_CAR_PUSH;}
}

label_FREF_CDR_PUSH:
CASE(FREF_CDR_PUSH) 
{
int val1;
{
#line 105 "instructions.scm"
INSN_VAL1(val1,c);
#line 106 "instructions.scm"
{AC(vm)=(((SG_CLOSURE(CL(vm)))->frees)[val1]);}}
{goto label_CDR_PUSH;}
}

label_GREF_CAR_PUSH:
CASE(GREF_CAR_PUSH) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1199=FETCH_OPERAND(PC(vm));SgObject s1200=(vm)->sandbox;if (SG_GLOCP(id1199)){if ((!(SG_FALSEP(s1200)))){{{SgObject id1201=Sg_MakeGlobalIdentifier((SG_GLOC(id1199))->name,(SG_GLOC(id1199))->library);{id1199=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1201),SG_IDENTIFIER_NAME(id1201),SG_UNBOUND));if (SG_UNBOUNDP(id1199)){{id1199=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1201),SG_IDENTIFIER_LIBRARY(id1201),id1201));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1199)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1199),SG_IDENTIFIER_NAME(id1199),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1199),SG_IDENTIFIER_LIBRARY(id1199),id1199));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1200)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_CAR_PUSH;}
}

label_GREF_CDR_PUSH:
CASE(GREF_CDR_PUSH) 
{
{
#line 148 "instructions.scm"
{SgObject v;
{SgObject id1202=FETCH_OPERAND(PC(vm));SgObject s1203=(vm)->sandbox;if (SG_GLOCP(id1202)){if ((!(SG_FALSEP(s1203)))){{{SgObject id1204=Sg_MakeGlobalIdentifier((SG_GLOC(id1202))->name,(SG_GLOC(id1202))->library);{id1202=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1204),SG_IDENTIFIER_NAME(id1204),SG_UNBOUND));if (SG_UNBOUNDP(id1202)){{id1202=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1204),SG_IDENTIFIER_LIBRARY(id1204),id1204));}}}}}}v=(SG_GLOC_GET(SG_GLOC(id1202)));} else {{v=(Sg_FindBinding(SG_IDENTIFIER_LIBRARY(id1202),SG_IDENTIFIER_NAME(id1202),SG_UNBOUND));if (SG_UNBOUNDP(v)){{v=(Sg_Apply3((&(Sg_GenericUnboundVariable)),SG_IDENTIFIER_NAME(id1202),SG_IDENTIFIER_LIBRARY(id1202),id1202));}}}if (SG_GLOCP(v)){{if (SG_FALSEP(s1203)){{(*((PC(vm))-(1)))=(SG_WORD(v));}}v=(SG_GLOC_GET(SG_GLOC(v)));}}}}
{AC(vm)=(v);}}}
{goto label_CDR_PUSH;}
}

label_CONST_RET:
CASE(CONST_RET) 
{
{
#line 78 "instructions.scm"
{SgObject val=FETCH_OPERAND(PC(vm));
{AC(vm)=(val);RET_INSN();CHECK_ATTENTION;NEXT;}}}
}

label_APPLY_VALUES:
CASE(APPLY_VALUES) 
{
int val1;
{SgObject cise__1205;
#line 797 "instructions.scm"
{SgObject rest=FETCH_OPERAND(PC(vm));int i;
#line 799 "instructions.scm"
INSN_VAL1(val1,c);
CHECK_STACK(val1,vm);
for (i=(0);(i)<(val1);(i)++){
if ((i)==(DEFAULT_VALUES_SIZE)){{break;}}
PUSH(SP(vm),((vm)->values)[i]);}
SG_FOR_EACH(cise__1205,rest) {{SgObject v=SG_CAR(cise__1205);
PUSH(SP(vm),v);}}
goto label_TAIL_CALL;}}
}

label_RESV_STACK:
CASE(RESV_STACK) 
{
int val1;
{
#line 812 "instructions.scm"
INSN_VAL1(val1,c);
#line 815 "instructions.scm"
(SP(vm))+=(val1);NEXT;}
}
#endif /* VM_LOOP */


