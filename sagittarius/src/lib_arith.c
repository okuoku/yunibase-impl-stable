/* Generated automatically from ../boot/lib/arith.scm. DO NOT EDIT! */
#define LIBSAGITTARIUS_BODY 
#include <sagittarius.h>
#include <sagittarius/private.h>
static struct sg__rc_cgen21587Rec {
  SgObject d21595[103];
  SgWord d21596[370];
  SgCodeBuilder d21597[9];
} sg__rc_cgen21587 = {
  {  /* SgObject d21595 */
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
  },
  {  /* SgWord d21596 */
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier integer?#core.arithmetic> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(4),
    0x00000002    /*   7 UNDEF */,
    0x00000018    /*   8 JUMP */,
    SG_WORD(16),
    0x00000030    /*  10 FRAME */,
    SG_WORD(14),
    0x00000048    /*  12 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* gcd */,
    0x00000030    /*  14 FRAME */,
    SG_WORD(6),
    0x00000048    /*  16 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* integer */,
    0x00000045    /*  18 LREF_PUSH */,
    0x0000024a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier wrong-type-argument-message#core.arithmetic> */,
    0x0000000b    /*  21 PUSH */,
    0x00000046    /*  22 FREF_PUSH */,
    0x0000034a    /*  23 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.arithmetic> */,
    0x00000045    /*  25 LREF_PUSH */,
    0x0000014b    /*  26 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier abs#core.arithmetic> */,
    0x0000002f    /*  28 RET */,
    /* gcd */0x00000030    /*   0 FRAME */,
    SG_WORD(8),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[0])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*   5 PUSH */,
    0x00000045    /*   6 LREF_PUSH */,
    0x0000024a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.arithmetic> */,
    0x0000000b    /*   9 PUSH */,
    0x00000105    /*  10 LREF */,
    0x00000021    /*  11 BNNULL */,
    SG_WORD(3),
    0x00000004    /*  13 CONSTI */,
    0x0000002f    /*  14 RET */,
    0x00000156    /*  15 LREF_CDR */,
    0x00000021    /*  16 BNNULL */,
    SG_WORD(3),
    0x00000155    /*  18 LREF_CAR */,
    0x0000002f    /*  19 RET */,
    0x0000015b    /*  20 LREF_CAR_PUSH */,
    0x0000015c    /*  21 LREF_CDR_PUSH */,
    0x00000305    /*  22 LREF */,
    0x00000021    /*  23 BNNULL */,
    SG_WORD(3),
    0x00000205    /*  25 LREF */,
    0x0000002f    /*  26 RET */,
    0x00000030    /*  27 FRAME */,
    SG_WORD(5),
    0x00000245    /*  29 LREF_PUSH */,
    0x0000035b    /*  30 LREF_CAR_PUSH */,
    0x0000024a    /*  31 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %gcd#core.arithmetic> */,
    0x0000000b    /*  33 PUSH */,
    0x0000035c    /*  34 LREF_CDR_PUSH */,
    0x00200219    /*  35 SHIFTJ */,
    0x00000018    /*  36 JUMP */,
    SG_WORD(-15),
    0x0000002f    /*  38 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier integer?#core.arithmetic> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(4),
    0x00000002    /*   7 UNDEF */,
    0x00000018    /*   8 JUMP */,
    SG_WORD(16),
    0x00000030    /*  10 FRAME */,
    SG_WORD(14),
    0x00000048    /*  12 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* lcm */,
    0x00000030    /*  14 FRAME */,
    SG_WORD(6),
    0x00000048    /*  16 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* integer */,
    0x00000045    /*  18 LREF_PUSH */,
    0x0000024a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier wrong-type-argument-message#core.arithmetic> */,
    0x0000000b    /*  21 PUSH */,
    0x00000046    /*  22 FREF_PUSH */,
    0x0000034a    /*  23 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.arithmetic> */,
    0x00000045    /*  25 LREF_PUSH */,
    0x0000014b    /*  26 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier abs#core.arithmetic> */,
    0x0000002f    /*  28 RET */,
    /* lcm */0x00000030    /*   0 FRAME */,
    SG_WORD(8),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[2])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*   5 PUSH */,
    0x00000045    /*   6 LREF_PUSH */,
    0x0000024a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.arithmetic> */,
    0x0000000b    /*   9 PUSH */,
    0x00000105    /*  10 LREF */,
    0x00000021    /*  11 BNNULL */,
    SG_WORD(3),
    0x00000104    /*  13 CONSTI */,
    0x0000002f    /*  14 RET */,
    0x00000156    /*  15 LREF_CDR */,
    0x00000021    /*  16 BNNULL */,
    SG_WORD(3),
    0x00000155    /*  18 LREF_CAR */,
    0x0000002f    /*  19 RET */,
    0x0000015b    /*  20 LREF_CAR_PUSH */,
    0x0000015c    /*  21 LREF_CDR_PUSH */,
    0x00000305    /*  22 LREF */,
    0x00000021    /*  23 BNNULL */,
    SG_WORD(3),
    0x00000205    /*  25 LREF */,
    0x0000002f    /*  26 RET */,
    0x0000035b    /*  27 LREF_CAR_PUSH */,
    0x00000030    /*  28 FRAME */,
    SG_WORD(5),
    0x00000245    /*  30 LREF_PUSH */,
    0x00000445    /*  31 LREF_PUSH */,
    0x0000024a    /*  32 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %gcd#core.arithmetic> */,
    0x0000000b    /*  34 PUSH */,
    0x00000245    /*  35 LREF_PUSH */,
    0x00000004    /*  36 CONSTI */,
    0x0000001a    /*  37 BNNUME */,
    SG_WORD(4),
    0x00000004    /*  39 CONSTI */,
    0x00000018    /*  40 JUMP */,
    SG_WORD(10),
    0x00000030    /*  42 FRAME */,
    SG_WORD(5),
    0x00000245    /*  44 LREF_PUSH */,
    0x00000545    /*  45 LREF_PUSH */,
    0x0000024a    /*  46 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier quotient#core.arithmetic> */,
    0x0000000b    /*  48 PUSH */,
    0x00000405    /*  49 LREF */,
    0x00000012    /*  50 MUL */,
    0x00000232    /*  51 LEAVE */,
    0x0000000b    /*  52 PUSH */,
    0x0000035c    /*  53 LREF_CDR_PUSH */,
    0x00200219    /*  54 SHIFTJ */,
    0x00000018    /*  55 JUMP */,
    SG_WORD(-34),
    0x0000002f    /*  57 RET */,
    /* div-and-mod */0x00000030    /*   0 FRAME */,
    SG_WORD(5),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x0000024a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier div#core.arithmetic> */,
    0x0000000b    /*   6 PUSH */,
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000145    /*  10 LREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier mod#core.arithmetic> */,
    0x0000000b    /*  13 PUSH */,
    0x00000245    /*  14 LREF_PUSH */,
    0x00000305    /*  15 LREF */,
    0x0000023a    /*  16 VALUES */,
    0x0000002f    /*  17 RET */,
    /* div0-and-mod0 */0x00000030    /*   0 FRAME */,
    SG_WORD(5),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x0000024a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier div0#core.arithmetic> */,
    0x0000000b    /*   6 PUSH */,
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000145    /*  10 LREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier mod0#core.arithmetic> */,
    0x0000000b    /*  13 PUSH */,
    0x00000245    /*  14 LREF_PUSH */,
    0x00000305    /*  15 LREF */,
    0x0000023a    /*  16 VALUES */,
    0x0000002f    /*  17 RET */,
    /* bitwise-rotate-bit-field */0x00000245    /*   0 LREF_PUSH */,
    0x00000105    /*   1 LREF */,
    0x00000010    /*   2 SUB */,
    0x0000000b    /*   3 PUSH */,
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x00000445    /*   6 LREF_PUSH */,
    0x0000014a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier positive?#core.arithmetic> */,
    0x00000017    /*   9 TEST */,
    SG_WORD(47),
    0x00000030    /*  11 FRAME */,
    SG_WORD(5),
    0x00000345    /*  13 LREF_PUSH */,
    0x00000445    /*  14 LREF_PUSH */,
    0x0000024a    /*  15 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier mod#core.arithmetic> */,
    0x0000000b    /*  17 PUSH */,
    0x00000030    /*  18 FRAME */,
    SG_WORD(6),
    0x00000045    /*  20 LREF_PUSH */,
    0x00000145    /*  21 LREF_PUSH */,
    0x00000245    /*  22 LREF_PUSH */,
    0x0000034a    /*  23 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-bit-field#core.arithmetic> */,
    0x0000000b    /*  25 PUSH */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(5),
    0x00000645    /*  28 LREF_PUSH */,
    0x00000545    /*  29 LREF_PUSH */,
    0x0000024a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift-left#core.arithmetic> */,
    0x0000000b    /*  32 PUSH */,
    0x00000030    /*  33 FRAME */,
    SG_WORD(8),
    0x00000645    /*  35 LREF_PUSH */,
    0x00000445    /*  36 LREF_PUSH */,
    0x00000505    /*  37 LREF */,
    0x00000010    /*  38 SUB */,
    0x0000000b    /*  39 PUSH */,
    0x0000024a    /*  40 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift-right#core.arithmetic> */,
    0x0000000b    /*  42 PUSH */,
    0x00000030    /*  43 FRAME */,
    SG_WORD(5),
    0x00000745    /*  45 LREF_PUSH */,
    0x00000845    /*  46 LREF_PUSH */,
    0x0000024a    /*  47 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-ior#core.arithmetic> */,
    0x0000000b    /*  49 PUSH */,
    0x00000045    /*  50 LREF_PUSH */,
    0x00000145    /*  51 LREF_PUSH */,
    0x00000245    /*  52 LREF_PUSH */,
    0x00000945    /*  53 LREF_PUSH */,
    0x0000044b    /*  54 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-copy-bit-field#core.arithmetic> */,
    0x0000002f    /*  56 RET */,
    0x00000005    /*  57 LREF */,
    0x0000002f    /*  58 RET */,
    /* bitwise-reverse-bit-field */0x00000245    /*   0 LREF_PUSH */,
    0x00000105    /*   1 LREF */,
    0x00000010    /*   2 SUB */,
    0x0000000b    /*   3 PUSH */,
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x00000345    /*   6 LREF_PUSH */,
    0x0000014a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier positive?#core.arithmetic> */,
    0x00000017    /*   9 TEST */,
    SG_WORD(80),
    0x00000049    /*  11 CONSTI_PUSH */,
    0x00000030    /*  12 FRAME */,
    SG_WORD(6),
    0x00000045    /*  14 LREF_PUSH */,
    0x00000145    /*  15 LREF_PUSH */,
    0x00000245    /*  16 LREF_PUSH */,
    0x0000034a    /*  17 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-bit-field#core.arithmetic> */,
    0x0000000b    /*  19 PUSH */,
    0x00000345    /*  20 LREF_PUSH */,
    0x00000645    /*  21 LREF_PUSH */,
    0x00000004    /*  22 CONSTI */,
    0x0000001a    /*  23 BNNUME */,
    SG_WORD(8),
    0x00000045    /*  25 LREF_PUSH */,
    0x00000145    /*  26 LREF_PUSH */,
    0x00000245    /*  27 LREF_PUSH */,
    0x00000445    /*  28 LREF_PUSH */,
    0x0000044b    /*  29 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-copy-bit-field#core.arithmetic> */,
    0x0000002f    /*  31 RET */,
    0x00000030    /*  32 FRAME */,
    SG_WORD(5),
    0x00000545    /*  34 LREF_PUSH */,
    0x00000149    /*  35 CONSTI_PUSH */,
    0x0000024a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-and#core.arithmetic> */,
    0x0000000b    /*  38 PUSH */,
    0x00000004    /*  39 CONSTI */,
    0x0000001a    /*  40 BNNUME */,
    SG_WORD(22),
    0x00000030    /*  42 FRAME */,
    SG_WORD(5),
    0x00000445    /*  44 LREF_PUSH */,
    0x00000149    /*  45 CONSTI_PUSH */,
    0x0000024a    /*  46 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift#core.arithmetic> */,
    0x0000000b    /*  48 PUSH */,
    0x00000030    /*  49 FRAME */,
    SG_WORD(5),
    0x00000545    /*  51 LREF_PUSH */,
    0x00000149    /*  52 CONSTI_PUSH */,
    0x0000024a    /*  53 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift-right#core.arithmetic> */,
    0x0000000b    /*  55 PUSH */,
    0x00000605    /*  56 LREF */,
    -0x000000f1   /*  57 ADDI */,
    0x0000000b    /*  58 PUSH */,
    0x00400319    /*  59 SHIFTJ */,
    0x00000018    /*  60 JUMP */,
    SG_WORD(-40),
    0x0000002f    /*  62 RET */,
    0x00000030    /*  63 FRAME */,
    SG_WORD(11),
    0x00000030    /*  65 FRAME */,
    SG_WORD(5),
    0x00000445    /*  67 LREF_PUSH */,
    0x00000149    /*  68 CONSTI_PUSH */,
    0x0000024a    /*  69 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift#core.arithmetic> */,
    0x0000000b    /*  71 PUSH */,
    0x00000149    /*  72 CONSTI_PUSH */,
    0x0000024a    /*  73 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-ior#core.arithmetic> */,
    0x0000000b    /*  75 PUSH */,
    0x00000030    /*  76 FRAME */,
    SG_WORD(5),
    0x00000545    /*  78 LREF_PUSH */,
    0x00000149    /*  79 CONSTI_PUSH */,
    0x0000024a    /*  80 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-arithmetic-shift-right#core.arithmetic> */,
    0x0000000b    /*  82 PUSH */,
    0x00000605    /*  83 LREF */,
    -0x000000f1   /*  84 ADDI */,
    0x0000000b    /*  85 PUSH */,
    0x00400319    /*  86 SHIFTJ */,
    0x00000018    /*  87 JUMP */,
    SG_WORD(-67),
    0x0000002f    /*  89 RET */,
    0x00000005    /*  90 LREF */,
    0x0000002f    /*  91 RET */,
    /* #f */0x00000034    /*   0 LIBRARY */,
    SG_WORD(SG_UNDEF) /* #<library core.arithmetic> */,
    0x00000029    /*   2 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[1])) /* #<code-builder gcd (0 1 0)> */,
    0x00000033    /*   4 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier gcd#core.arithmetic> */,
    0x00000029    /*   6 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[3])) /* #<code-builder lcm (0 1 0)> */,
    0x00000033    /*   8 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lcm#core.arithmetic> */,
    0x00000029    /*  10 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[4])) /* #<code-builder div-and-mod (2 0 0)> */,
    0x00000033    /*  12 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier div-and-mod#core.arithmetic> */,
    0x00000029    /*  14 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[5])) /* #<code-builder div0-and-mod0 (2 0 0)> */,
    0x00000033    /*  16 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier div0-and-mod0#core.arithmetic> */,
    0x00000029    /*  18 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[6])) /* #<code-builder bitwise-rotate-bit-field (4 0 0)> */,
    0x00000033    /*  20 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-rotate-bit-field#core.arithmetic> */,
    0x00000029    /*  22 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen21587.d21597[7])) /* #<code-builder bitwise-reverse-bit-field (3 0 0)> */,
    0x00000033    /*  24 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bitwise-reverse-bit-field#core.arithmetic> */,
    0x00000002    /*  26 UNDEF */,
    0x0000002f    /*  27 RET */,
  },
  {  /* SgCodeBuilder d21597 */
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[0]), SG_FALSE, 1, 0, 1, 12, 29),
    
    SG_STATIC_CODE_BUILDER( /* gcd */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[29]), SG_FALSE, 0, 1, 0, 19, 39),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[68]), SG_FALSE, 1, 0, 1, 12, 29),
    
    SG_STATIC_CODE_BUILDER( /* lcm */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[97]), SG_FALSE, 0, 1, 0, 26, 58),
    
    SG_STATIC_CODE_BUILDER( /* div-and-mod */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[155]), SG_FALSE, 2, 0, 0, 15, 18),
    
    SG_STATIC_CODE_BUILDER( /* div0-and-mod0 */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[173]), SG_FALSE, 2, 0, 0, 15, 18),
    
    SG_STATIC_CODE_BUILDER( /* bitwise-rotate-bit-field */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[191]), SG_FALSE, 4, 0, 0, 40, 59),
    
    SG_STATIC_CODE_BUILDER( /* bitwise-reverse-bit-field */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[250]), SG_FALSE, 3, 0, 0, 31, 92),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen21587.d21596[342]), SG_FALSE, 0, 0, 0, 0, 28),
  },
};
static SgCodeBuilder *G21588 = 
   SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[8]));
void Sg__Init_core_arithmetic() {
  SgObject save = Sg_VM()->currentLibrary;

  sg__rc_cgen21587.d21595[2] = SG_MAKE_STRING("(core arithmetic)");
  sg__rc_cgen21587.d21595[1] = Sg_Intern(sg__rc_cgen21587.d21595[2]); /* (core arithmetic) */
  sg__rc_cgen21587.d21595[0] = Sg_FindLibrary(SG_SYMBOL(sg__rc_cgen21587.d21595[1]), TRUE);
  sg__rc_cgen21587.d21595[3] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[5] = SG_MAKE_STRING("integer?");
  sg__rc_cgen21587.d21595[4] = Sg_Intern(sg__rc_cgen21587.d21595[5]); /* integer? */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[3],SG_SYMBOL(sg__rc_cgen21587.d21595[4]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* integer? */
  sg__rc_cgen21587.d21595[7] = SG_MAKE_STRING("gcd");
  sg__rc_cgen21587.d21595[6] = Sg_Intern(sg__rc_cgen21587.d21595[7]); /* gcd */
  sg__rc_cgen21587.d21595[8] = SG_MAKE_STRING("integer");
  sg__rc_cgen21587.d21595[9] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[11] = SG_MAKE_STRING("wrong-type-argument-message");
  sg__rc_cgen21587.d21595[10] = Sg_Intern(sg__rc_cgen21587.d21595[11]); /* wrong-type-argument-message */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[9],SG_SYMBOL(sg__rc_cgen21587.d21595[10]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* wrong-type-argument-message */
  sg__rc_cgen21587.d21595[12] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[14] = SG_MAKE_STRING("assertion-violation");
  sg__rc_cgen21587.d21595[13] = Sg_Intern(sg__rc_cgen21587.d21595[14]); /* assertion-violation */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[12],SG_SYMBOL(sg__rc_cgen21587.d21595[13]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* assertion-violation */
  sg__rc_cgen21587.d21595[15] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[17] = SG_MAKE_STRING("abs");
  sg__rc_cgen21587.d21595[16] = Sg_Intern(sg__rc_cgen21587.d21595[17]); /* abs */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[15],SG_SYMBOL(sg__rc_cgen21587.d21595[16]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* abs */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[4] = SG_WORD(sg__rc_cgen21587.d21595[3]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[13] = SG_WORD(sg__rc_cgen21587.d21595[6]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[17] = SG_WORD(sg__rc_cgen21587.d21595[8]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[20] = SG_WORD(sg__rc_cgen21587.d21595[9]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[24] = SG_WORD(sg__rc_cgen21587.d21595[12]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[0]))[27] = SG_WORD(sg__rc_cgen21587.d21595[15]);
  sg__rc_cgen21587.d21595[18] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[20] = SG_MAKE_STRING("map");
  sg__rc_cgen21587.d21595[19] = Sg_Intern(sg__rc_cgen21587.d21595[20]); /* map */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[18],SG_SYMBOL(sg__rc_cgen21587.d21595[19]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* map */
  sg__rc_cgen21587.d21595[21] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[23] = SG_MAKE_STRING("%gcd");
  sg__rc_cgen21587.d21595[22] = Sg_Intern(sg__rc_cgen21587.d21595[23]); /* %gcd */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[21],SG_SYMBOL(sg__rc_cgen21587.d21595[22]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* %gcd */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[1]))->name = sg__rc_cgen21587.d21595[6];/* gcd */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[29]))[8] = SG_WORD(sg__rc_cgen21587.d21595[18]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[29]))[32] = SG_WORD(sg__rc_cgen21587.d21595[21]);
  sg__rc_cgen21587.d21595[24] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[24],SG_SYMBOL(sg__rc_cgen21587.d21595[6]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* gcd */
  sg__rc_cgen21587.d21595[25] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[25],SG_SYMBOL(sg__rc_cgen21587.d21595[4]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* integer? */
  sg__rc_cgen21587.d21595[27] = SG_MAKE_STRING("lcm");
  sg__rc_cgen21587.d21595[26] = Sg_Intern(sg__rc_cgen21587.d21595[27]); /* lcm */
  sg__rc_cgen21587.d21595[28] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[28],SG_SYMBOL(sg__rc_cgen21587.d21595[10]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* wrong-type-argument-message */
  sg__rc_cgen21587.d21595[29] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[29],SG_SYMBOL(sg__rc_cgen21587.d21595[13]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* assertion-violation */
  sg__rc_cgen21587.d21595[30] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[30],SG_SYMBOL(sg__rc_cgen21587.d21595[16]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* abs */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[4] = SG_WORD(sg__rc_cgen21587.d21595[25]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[13] = SG_WORD(sg__rc_cgen21587.d21595[26]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[17] = SG_WORD(sg__rc_cgen21587.d21595[8]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[20] = SG_WORD(sg__rc_cgen21587.d21595[28]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[24] = SG_WORD(sg__rc_cgen21587.d21595[29]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[68]))[27] = SG_WORD(sg__rc_cgen21587.d21595[30]);
  sg__rc_cgen21587.d21595[31] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[31],SG_SYMBOL(sg__rc_cgen21587.d21595[19]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* map */
  sg__rc_cgen21587.d21595[32] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[32],SG_SYMBOL(sg__rc_cgen21587.d21595[22]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* %gcd */
  sg__rc_cgen21587.d21595[33] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[35] = SG_MAKE_STRING("quotient");
  sg__rc_cgen21587.d21595[34] = Sg_Intern(sg__rc_cgen21587.d21595[35]); /* quotient */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[33],SG_SYMBOL(sg__rc_cgen21587.d21595[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* quotient */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[3]))->name = sg__rc_cgen21587.d21595[26];/* lcm */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[97]))[8] = SG_WORD(sg__rc_cgen21587.d21595[31]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[97]))[33] = SG_WORD(sg__rc_cgen21587.d21595[32]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[97]))[47] = SG_WORD(sg__rc_cgen21587.d21595[33]);
  sg__rc_cgen21587.d21595[36] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[36],SG_SYMBOL(sg__rc_cgen21587.d21595[26]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* lcm */
  sg__rc_cgen21587.d21595[37] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[39] = SG_MAKE_STRING("div");
  sg__rc_cgen21587.d21595[38] = Sg_Intern(sg__rc_cgen21587.d21595[39]); /* div */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[37],SG_SYMBOL(sg__rc_cgen21587.d21595[38]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* div */
  sg__rc_cgen21587.d21595[40] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[42] = SG_MAKE_STRING("mod");
  sg__rc_cgen21587.d21595[41] = Sg_Intern(sg__rc_cgen21587.d21595[42]); /* mod */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[40],SG_SYMBOL(sg__rc_cgen21587.d21595[41]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* mod */
  sg__rc_cgen21587.d21595[44] = SG_MAKE_STRING("div-and-mod");
  sg__rc_cgen21587.d21595[43] = Sg_Intern(sg__rc_cgen21587.d21595[44]); /* div-and-mod */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[4]))->name = sg__rc_cgen21587.d21595[43];/* div-and-mod */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[155]))[5] = SG_WORD(sg__rc_cgen21587.d21595[37]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[155]))[12] = SG_WORD(sg__rc_cgen21587.d21595[40]);
  sg__rc_cgen21587.d21595[45] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[45],SG_SYMBOL(sg__rc_cgen21587.d21595[43]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* div-and-mod */
  sg__rc_cgen21587.d21595[46] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[48] = SG_MAKE_STRING("div0");
  sg__rc_cgen21587.d21595[47] = Sg_Intern(sg__rc_cgen21587.d21595[48]); /* div0 */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[46],SG_SYMBOL(sg__rc_cgen21587.d21595[47]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* div0 */
  sg__rc_cgen21587.d21595[49] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[51] = SG_MAKE_STRING("mod0");
  sg__rc_cgen21587.d21595[50] = Sg_Intern(sg__rc_cgen21587.d21595[51]); /* mod0 */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[49],SG_SYMBOL(sg__rc_cgen21587.d21595[50]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* mod0 */
  sg__rc_cgen21587.d21595[53] = SG_MAKE_STRING("div0-and-mod0");
  sg__rc_cgen21587.d21595[52] = Sg_Intern(sg__rc_cgen21587.d21595[53]); /* div0-and-mod0 */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[5]))->name = sg__rc_cgen21587.d21595[52];/* div0-and-mod0 */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[173]))[5] = SG_WORD(sg__rc_cgen21587.d21595[46]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[173]))[12] = SG_WORD(sg__rc_cgen21587.d21595[49]);
  sg__rc_cgen21587.d21595[54] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[54],SG_SYMBOL(sg__rc_cgen21587.d21595[52]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* div0-and-mod0 */
  sg__rc_cgen21587.d21595[55] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[57] = SG_MAKE_STRING("positive?");
  sg__rc_cgen21587.d21595[56] = Sg_Intern(sg__rc_cgen21587.d21595[57]); /* positive? */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[55],SG_SYMBOL(sg__rc_cgen21587.d21595[56]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* positive? */
  sg__rc_cgen21587.d21595[58] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[58],SG_SYMBOL(sg__rc_cgen21587.d21595[41]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* mod */
  sg__rc_cgen21587.d21595[59] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[61] = SG_MAKE_STRING("bitwise-bit-field");
  sg__rc_cgen21587.d21595[60] = Sg_Intern(sg__rc_cgen21587.d21595[61]); /* bitwise-bit-field */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[59],SG_SYMBOL(sg__rc_cgen21587.d21595[60]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-bit-field */
  sg__rc_cgen21587.d21595[62] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[64] = SG_MAKE_STRING("bitwise-arithmetic-shift-left");
  sg__rc_cgen21587.d21595[63] = Sg_Intern(sg__rc_cgen21587.d21595[64]); /* bitwise-arithmetic-shift-left */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[62],SG_SYMBOL(sg__rc_cgen21587.d21595[63]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift-left */
  sg__rc_cgen21587.d21595[65] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[67] = SG_MAKE_STRING("bitwise-arithmetic-shift-right");
  sg__rc_cgen21587.d21595[66] = Sg_Intern(sg__rc_cgen21587.d21595[67]); /* bitwise-arithmetic-shift-right */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[65],SG_SYMBOL(sg__rc_cgen21587.d21595[66]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift-right */
  sg__rc_cgen21587.d21595[68] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[70] = SG_MAKE_STRING("bitwise-ior");
  sg__rc_cgen21587.d21595[69] = Sg_Intern(sg__rc_cgen21587.d21595[70]); /* bitwise-ior */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[68],SG_SYMBOL(sg__rc_cgen21587.d21595[69]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-ior */
  sg__rc_cgen21587.d21595[71] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[73] = SG_MAKE_STRING("bitwise-copy-bit-field");
  sg__rc_cgen21587.d21595[72] = Sg_Intern(sg__rc_cgen21587.d21595[73]); /* bitwise-copy-bit-field */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[71],SG_SYMBOL(sg__rc_cgen21587.d21595[72]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-copy-bit-field */
  sg__rc_cgen21587.d21595[75] = SG_MAKE_STRING("bitwise-rotate-bit-field");
  sg__rc_cgen21587.d21595[74] = Sg_Intern(sg__rc_cgen21587.d21595[75]); /* bitwise-rotate-bit-field */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[6]))->name = sg__rc_cgen21587.d21595[74];/* bitwise-rotate-bit-field */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[8] = SG_WORD(sg__rc_cgen21587.d21595[55]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[16] = SG_WORD(sg__rc_cgen21587.d21595[58]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[24] = SG_WORD(sg__rc_cgen21587.d21595[59]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[31] = SG_WORD(sg__rc_cgen21587.d21595[62]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[41] = SG_WORD(sg__rc_cgen21587.d21595[65]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[48] = SG_WORD(sg__rc_cgen21587.d21595[68]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[191]))[55] = SG_WORD(sg__rc_cgen21587.d21595[71]);
  sg__rc_cgen21587.d21595[76] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[76],SG_SYMBOL(sg__rc_cgen21587.d21595[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-rotate-bit-field */
  sg__rc_cgen21587.d21595[77] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[77],SG_SYMBOL(sg__rc_cgen21587.d21595[56]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* positive? */
  sg__rc_cgen21587.d21595[78] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[78],SG_SYMBOL(sg__rc_cgen21587.d21595[60]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-bit-field */
  sg__rc_cgen21587.d21595[79] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[79],SG_SYMBOL(sg__rc_cgen21587.d21595[72]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-copy-bit-field */
  sg__rc_cgen21587.d21595[80] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[82] = SG_MAKE_STRING("bitwise-and");
  sg__rc_cgen21587.d21595[81] = Sg_Intern(sg__rc_cgen21587.d21595[82]); /* bitwise-and */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[80],SG_SYMBOL(sg__rc_cgen21587.d21595[81]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-and */
  sg__rc_cgen21587.d21595[83] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen21587.d21595[85] = SG_MAKE_STRING("bitwise-arithmetic-shift");
  sg__rc_cgen21587.d21595[84] = Sg_Intern(sg__rc_cgen21587.d21595[85]); /* bitwise-arithmetic-shift */
  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[83],SG_SYMBOL(sg__rc_cgen21587.d21595[84]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift */
  sg__rc_cgen21587.d21595[86] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[86],SG_SYMBOL(sg__rc_cgen21587.d21595[66]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift-right */
  sg__rc_cgen21587.d21595[87] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[87],SG_SYMBOL(sg__rc_cgen21587.d21595[84]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift */
  sg__rc_cgen21587.d21595[88] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[88],SG_SYMBOL(sg__rc_cgen21587.d21595[69]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-ior */
  sg__rc_cgen21587.d21595[89] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[89],SG_SYMBOL(sg__rc_cgen21587.d21595[66]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-arithmetic-shift-right */
  sg__rc_cgen21587.d21595[91] = SG_MAKE_STRING("bitwise-reverse-bit-field");
  sg__rc_cgen21587.d21595[90] = Sg_Intern(sg__rc_cgen21587.d21595[91]); /* bitwise-reverse-bit-field */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen21587.d21597[7]))->name = sg__rc_cgen21587.d21595[90];/* bitwise-reverse-bit-field */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[8] = SG_WORD(sg__rc_cgen21587.d21595[77]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[18] = SG_WORD(sg__rc_cgen21587.d21595[78]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[30] = SG_WORD(sg__rc_cgen21587.d21595[79]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[37] = SG_WORD(sg__rc_cgen21587.d21595[80]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[47] = SG_WORD(sg__rc_cgen21587.d21595[83]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[54] = SG_WORD(sg__rc_cgen21587.d21595[86]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[70] = SG_WORD(sg__rc_cgen21587.d21595[87]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[74] = SG_WORD(sg__rc_cgen21587.d21595[88]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[250]))[81] = SG_WORD(sg__rc_cgen21587.d21595[89]);
  sg__rc_cgen21587.d21595[92] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen21587.d21595[92],SG_SYMBOL(sg__rc_cgen21587.d21595[90]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen21587.d21595[0]),FALSE); /* bitwise-reverse-bit-field */
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[1] = SG_WORD(sg__rc_cgen21587.d21595[0]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[5] = SG_WORD(sg__rc_cgen21587.d21595[24]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[9] = SG_WORD(sg__rc_cgen21587.d21595[36]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[13] = SG_WORD(sg__rc_cgen21587.d21595[45]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[17] = SG_WORD(sg__rc_cgen21587.d21595[54]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[21] = SG_WORD(sg__rc_cgen21587.d21595[76]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen21587.d21596[342]))[25] = SG_WORD(sg__rc_cgen21587.d21595[92]);
  sg__rc_cgen21587.d21595[94] = SG_MAKE_STRING("(core)");
  sg__rc_cgen21587.d21595[93] = Sg_Intern(sg__rc_cgen21587.d21595[94]); /* (core) */
  Sg_ImportLibrary(sg__rc_cgen21587.d21595[0], sg__rc_cgen21587.d21595[93]);

  sg__rc_cgen21587.d21595[96] = SG_MAKE_STRING("(core base)");
  sg__rc_cgen21587.d21595[95] = Sg_Intern(sg__rc_cgen21587.d21595[96]); /* (core base) */
  Sg_ImportLibrary(sg__rc_cgen21587.d21595[0], sg__rc_cgen21587.d21595[95]);

  sg__rc_cgen21587.d21595[98] = SG_MAKE_STRING("(core errors)");
  sg__rc_cgen21587.d21595[97] = Sg_Intern(sg__rc_cgen21587.d21595[98]); /* (core errors) */
  Sg_ImportLibrary(sg__rc_cgen21587.d21595[0], sg__rc_cgen21587.d21595[97]);

  sg__rc_cgen21587.d21595[100] = SG_MAKE_STRING("(sagittarius)");
  sg__rc_cgen21587.d21595[99] = Sg_Intern(sg__rc_cgen21587.d21595[100]); /* (sagittarius) */
  Sg_ImportLibrary(sg__rc_cgen21587.d21595[0], sg__rc_cgen21587.d21595[99]);

  do {
    /* (bitwise-reverse-bit-field bitwise-rotate-bit-field div0-and-mod0 div-and-mod lcm gcd) */ 
    SgObject G21598 = SG_NIL, G21599 = SG_NIL;
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[90]); /* bitwise-reverse-bit-field */ 
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[74]); /* bitwise-rotate-bit-field */ 
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[52]); /* div0-and-mod0 */ 
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[43]); /* div-and-mod */ 
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[26]); /* lcm */ 
    SG_APPEND1(G21598, G21599, sg__rc_cgen21587.d21595[6]); /* gcd */ 
    sg__rc_cgen21587.d21595[102] = G21598;
  } while (0);
  do {
    /* ((bitwise-reverse-bit-field bitwise-rotate-bit-field div0-and-mod0 div-and-mod lcm gcd)) */ 
    SgObject G21600 = SG_NIL, G21601 = SG_NIL;
    SG_APPEND1(G21600, G21601, sg__rc_cgen21587.d21595[102]); /* (bitwise-reverse-bit-field bitwise-rotate-bit-field div0-and-mod0 div-and-mod lcm gcd) */ 
    sg__rc_cgen21587.d21595[101] = G21600;
  } while (0);
  Sg_LibraryExportedSet(sg__rc_cgen21587.d21595[0], sg__rc_cgen21587.d21595[101]);

  Sg_VM()->currentLibrary = sg__rc_cgen21587.d21595[0];
  Sg_VMExecute(SG_OBJ(G21588));
  Sg_VM()->currentLibrary = save;
}
