/* Generated automatically from ../boot/lib/macro.scm. DO NOT EDIT! */
#define LIBSAGITTARIUS_BODY 
#include <sagittarius.h>
#include <sagittarius/private.h>
static struct sg__rc_cgen15752Rec {
  SgWord d15866[6661];
  SgObject d15867[981];
  SgCodeBuilder d15868[112];
} sg__rc_cgen15752 = {
  {  /* SgWord d15866 */
    /* (literal-match? cmp) */0x00000245    /*   0 LREF_PUSH */,
    0x00000305    /*   1 LREF */,
    0x00000021    /*   2 BNNULL */,
    SG_WORD(3),
    0x00000061    /*   4 CONST_RET */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(5),
    0x00000145    /*   8 LREF_PUSH */,
    0x0000035b    /*   9 LREF_CAR_PUSH */,
    0x00000005    /*  10 LREF */,
    0x0000022b    /*  11 CALL */,
    0x00000017    /*  12 TEST */,
    SG_WORD(2),
    0x0000002f    /*  14 RET */,
    0x0000035c    /*  15 LREF_CDR_PUSH */,
    0x00300119    /*  16 SHIFTJ */,
    0x00000018    /*  17 JUMP */,
    SG_WORD(-17),
    0x0000002f    /*  19 RET */,
    /* (literal-match? lambda15864) */0x00000045    /*   0 LREF_PUSH */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(4),
    0x00000145    /*   3 LREF_PUSH */,
    0x0000014a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-name#core.macro> */,
    0x0000003b    /*   6 EQ */,
    0x0000002f    /*   7 RET */,
    /* (collect-unique-ids loop) */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(14),
    0x0000005c    /*   4 LREF_CDR_PUSH */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(6),
    0x0000005b    /*   7 LREF_CAR_PUSH */,
    0x00000145    /*   8 LREF_PUSH */,
    0x00000009    /*   9 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x0000022c    /*  11 LOCAL_CALL */,
    0x0000000b    /*  12 PUSH */,
    0x00000009    /*  13 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x0000022e    /*  15 LOCAL_TAIL_CALL */,
    0x0000002f    /*  16 RET */,
    0x00000030    /*  17 FRAME */,
    SG_WORD(4),
    0x00000045    /*  19 LREF_PUSH */,
    0x0000014a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  22 TEST */,
    SG_WORD(3),
    0x00000105    /*  24 LREF */,
    0x0000002f    /*  25 RET */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(4),
    0x00000045    /*  28 LREF_PUSH */,
    0x0000014a    /*  29 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  31 TEST */,
    SG_WORD(15),
    0x00000030    /*  33 FRAME */,
    SG_WORD(5),
    0x00000045    /*  35 LREF_PUSH */,
    0x00000145    /*  36 LREF_PUSH */,
    0x0000024a    /*  37 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier memq#core.macro> */,
    0x00000017    /*  39 TEST */,
    SG_WORD(3),
    0x00000105    /*  41 LREF */,
    0x0000002f    /*  42 RET */,
    0x00000045    /*  43 LREF_PUSH */,
    0x00000105    /*  44 LREF */,
    0x00000037    /*  45 CONS */,
    0x0000002f    /*  46 RET */,
    0x00000005    /*  47 LREF */,
    0x00000041    /*  48 VECTORP */,
    0x00000017    /*  49 TEST */,
    SG_WORD(12),
    0x00000030    /*  51 FRAME */,
    SG_WORD(4),
    0x00000045    /*  53 LREF_PUSH */,
    0x0000014a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  56 PUSH */,
    0x00000145    /*  57 LREF_PUSH */,
    0x00000009    /*  58 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x0000022e    /*  60 LOCAL_TAIL_CALL */,
    0x0000002f    /*  61 RET */,
    0x00000105    /*  62 LREF */,
    0x0000002f    /*  63 RET */,
    /* (compile-syntax-case lambda15862) */0x00000045    /*   0 LREF_PUSH */,
    0x00000005    /*   1 LREF */,
    0x0000003f    /*   2 SYMBOLP */,
    0x00000017    /*   3 TEST */,
    SG_WORD(5),
    0x00000003    /*   5 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*   7 JUMP */,
    SG_WORD(2),
    0x00000105    /*   9 LREF */,
    0x0000000b    /*  10 PUSH */,
    0x00000245    /*  11 LREF_PUSH */,
    0x0000034b    /*  12 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000002f    /*  14 RET */,
    /* (compile-syntax-case lambda15861) */0x00000061    /*   0 CONST_RET */,
    SG_WORD(SG_FALSE) /* #f */,
    /* (compile-syntax-case lambda15860) */0x00000045    /*   0 LREF_PUSH */,
    0x00000005    /*   1 LREF */,
    0x0000003f    /*   2 SYMBOLP */,
    0x00000017    /*   3 TEST */,
    SG_WORD(5),
    0x00000003    /*   5 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*   7 JUMP */,
    SG_WORD(2),
    0x00000105    /*   9 LREF */,
    0x0000000b    /*  10 PUSH */,
    0x00000245    /*  11 LREF_PUSH */,
    0x0000034b    /*  12 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000002f    /*  14 RET */,
    /* (match-pattern? ensure-id) */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000005    /*   7 LREF */,
    0x0000002f    /*   8 RET */,
    0x00000045    /*   9 LREF_PUSH */,
    0x00000145    /*  10 LREF_PUSH */,
    0x00000104    /*  11 CONSTI */,
    0x00000043    /*  12 VEC_REF */,
    0x0000000b    /*  13 PUSH */,
    0x00000145    /*  14 LREF_PUSH */,
    0x00000004    /*  15 CONSTI */,
    0x00000043    /*  16 VEC_REF */,
    0x0000000b    /*  17 PUSH */,
    0x0000034b    /*  18 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000002f    /*  20 RET */,
    /* (compile-syntax lambda15858) */0x00000045    /*   0 LREF_PUSH */,
    0x00000005    /*   1 LREF */,
    0x0000003f    /*   2 SYMBOLP */,
    0x00000017    /*   3 TEST */,
    SG_WORD(5),
    0x00000003    /*   5 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*   7 JUMP */,
    SG_WORD(2),
    0x00000105    /*   9 LREF */,
    0x0000000b    /*  10 PUSH */,
    0x00000245    /*  11 LREF_PUSH */,
    0x0000034b    /*  12 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000002f    /*  14 RET */,
    /* (expand-syntax loop) */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(19),
    0x00000055    /*   4 LREF_CAR */,
    0x00000021    /*   5 BNNULL */,
    SG_WORD(2),
    0x0000002f    /*   7 RET */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(5),
    0x0000005b    /*  10 LREF_CAR_PUSH */,
    0x00000009    /*  11 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x0000012c    /*  13 LOCAL_CALL */,
    0x00000017    /*  14 TEST */,
    SG_WORD(2),
    0x0000002f    /*  16 RET */,
    0x0000005c    /*  17 LREF_CDR_PUSH */,
    0x00000009    /*  18 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x0000012e    /*  20 LOCAL_TAIL_CALL */,
    0x0000002f    /*  21 RET */,
    0x00000005    /*  22 LREF */,
    0x00000041    /*  23 VECTORP */,
    0x00000017    /*  24 TEST */,
    SG_WORD(28),
    0x00000005    /*  26 LREF */,
    0x00000042    /*  27 VEC_LEN */,
    -0x000000f1   /*  28 ADDI */,
    0x0000000b    /*  29 PUSH */,
    0x00000145    /*  30 LREF_PUSH */,
    0x00000004    /*  31 CONSTI */,
    0x0000001e    /*  32 BNGE */,
    SG_WORD(19),
    0x00000030    /*  34 FRAME */,
    SG_WORD(8),
    0x00000045    /*  36 LREF_PUSH */,
    0x00000105    /*  37 LREF */,
    0x00000043    /*  38 VEC_REF */,
    0x0000000b    /*  39 PUSH */,
    0x00000009    /*  40 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x0000012c    /*  42 LOCAL_CALL */,
    0x00000017    /*  43 TEST */,
    SG_WORD(2),
    0x0000002f    /*  45 RET */,
    0x00000105    /*  46 LREF */,
    -0x000000f1   /*  47 ADDI */,
    0x0000000b    /*  48 PUSH */,
    0x00100119    /*  49 SHIFTJ */,
    0x00000018    /*  50 JUMP */,
    SG_WORD(-21),
    0x0000002f    /*  52 RET */,
    0x00000045    /*  53 LREF_PUSH */,
    0x0000014b    /*  54 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x0000002f    /*  56 RET */,
    /* (expand-syntax lambda15856) */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier lookup-transformer-env#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(2),
    0x0000002f    /*   7 RET */,
    0x00000105    /*   8 LREF */,
    0x00000021    /*   9 BNNULL */,
    SG_WORD(6),
    0x00000004    /*  11 CONSTI */,
    0x00000138    /*  12 LIST */,
    0x00000138    /*  13 LIST */,
    0x00000018    /*  14 JUMP */,
    SG_WORD(2),
    0x00000105    /*  16 LREF */,
    0x0000000b    /*  17 PUSH */,
    0x00000030    /*  18 FRAME */,
    SG_WORD(8),
    0x00000045    /*  20 LREF_PUSH */,
    0x00000345    /*  21 LREF_PUSH */,
    0x00000245    /*  22 LREF_PUSH */,
    0x00000048    /*  23 CONST_PUSH */,
    SG_WORD(SG_TRUE) /* #t */,
    0x0000044a    /*  25 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %make-identifier#core.macro> */,
    0x0000000b    /*  27 PUSH */,
    0x00000045    /*  28 LREF_PUSH */,
    0x00000445    /*  29 LREF_PUSH */,
    0x0000024b    /*  30 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier add-to-transformer-env!#core.macro> */,
    0x0000002f    /*  32 RET */,
    /* (transcribe-template lambda15855) */0x0000005c    /*   0 LREF_CDR_PUSH */,
    0x0000014b    /*   1 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000002f    /*   3 RET */,
    /* (datum->syntax lambda15854) */0x00000061    /*   0 CONST_RET */,
    SG_WORD(SG_FALSE) /* #f */,
    /* (make-variable-transformer lambda15853) */0x00000045    /*   0 LREF_PUSH */,
    0x00000005    /*   1 LREF */,
    0x0000003f    /*   2 SYMBOLP */,
    0x00000017    /*   3 TEST */,
    SG_WORD(5),
    0x00000003    /*   5 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*   7 JUMP */,
    SG_WORD(2),
    0x00000105    /*   9 LREF */,
    0x0000000b    /*  10 PUSH */,
    0x00000245    /*  11 LREF_PUSH */,
    0x0000034b    /*  12 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000002f    /*  14 RET */,
    /* (er-rename rename) */0x00000030    /*   0 FRAME */,
    SG_WORD(6),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000245    /*   3 LREF_PUSH */,
    0x00000345    /*   4 LREF_PUSH */,
    0x0000034a    /*   5 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000000b    /*   7 PUSH */,
    0x00000105    /*   8 LREF */,
    0x00000017    /*   9 TEST */,
    SG_WORD(10),
    0x00000030    /*  11 FRAME */,
    SG_WORD(6),
    0x00000145    /*  13 LREF_PUSH */,
    0x00000045    /*  14 LREF_PUSH */,
    0x00000445    /*  15 LREF_PUSH */,
    0x0000034a    /*  16 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x00000018    /*  18 JUMP */,
    SG_WORD(2),
    0x00000002    /*  20 UNDEF */,
    0x00000405    /*  21 LREF */,
    0x0000002f    /*  22 RET */,
    /* (er-macro-transformer compare) */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(143),
    0x00000105    /*   4 LREF */,
    0x0000003e    /*   5 PAIRP */,
    0x00000017    /*   6 TEST */,
    SG_WORD(16),
    0x00000030    /*   8 FRAME */,
    SG_WORD(6),
    0x0000005b    /*  10 LREF_CAR_PUSH */,
    0x0000015b    /*  11 LREF_CAR_PUSH */,
    0x00000009    /*  12 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x0000022c    /*  14 LOCAL_CALL */,
    0x00000017    /*  15 TEST */,
    SG_WORD(6),
    0x0000005c    /*  17 LREF_CDR_PUSH */,
    0x0000015c    /*  18 LREF_CDR_PUSH */,
    0x00000009    /*  19 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x0000022e    /*  21 LOCAL_TAIL_CALL */,
    0x0000002f    /*  22 RET */,
    0x00000030    /*  23 FRAME */,
    SG_WORD(4),
    0x00000045    /*  25 LREF_PUSH */,
    0x0000014a    /*  26 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  28 TEST */,
    SG_WORD(114),
    0x00000030    /*  30 FRAME */,
    SG_WORD(4),
    0x00000145    /*  32 LREF_PUSH */,
    0x0000014a    /*  33 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  35 TEST */,
    SG_WORD(53),
    0x00000030    /*  37 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  39 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x0000000b    /*  41 PUSH */,
    0x00000045    /*  42 LREF_PUSH */,
    0x00000245    /*  43 LREF_PUSH */,
    0x00000030    /*  44 FRAME */,
    SG_WORD(4),
    0x00000345    /*  46 LREF_PUSH */,
    0x0000014a    /*  47 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  49 TEST */,
    SG_WORD(4),
    0x00000305    /*  51 LREF */,
    0x00000018    /*  52 JUMP */,
    SG_WORD(9),
    0x00000030    /*  54 FRAME */,
    SG_WORD(7),
    0x00000345    /*  56 LREF_PUSH */,
    0x00000445    /*  57 LREF_PUSH */,
    0x00000048    /*  58 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*  60 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier er-rename#core.macro> */,
    0x00000232    /*  62 LEAVE */,
    0x0000000b    /*  63 PUSH */,
    0x00000145    /*  64 LREF_PUSH */,
    0x00000245    /*  65 LREF_PUSH */,
    0x00000030    /*  66 FRAME */,
    SG_WORD(4),
    0x00000445    /*  68 LREF_PUSH */,
    0x0000014a    /*  69 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  71 TEST */,
    SG_WORD(4),
    0x00000405    /*  73 LREF */,
    0x00000018    /*  74 JUMP */,
    SG_WORD(9),
    0x00000030    /*  76 FRAME */,
    SG_WORD(7),
    0x00000445    /*  78 LREF_PUSH */,
    0x00000545    /*  79 LREF_PUSH */,
    0x00000048    /*  80 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*  82 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier er-rename#core.macro> */,
    0x00000232    /*  84 LEAVE */,
    0x0000000b    /*  85 PUSH */,
    0x0000024b    /*  86 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier free-identifier=?#core.macro> */,
    0x0000002f    /*  88 RET */,
    0x00000005    /*  89 LREF */,
    0x00000041    /*  90 VECTORP */,
    0x00000017    /*  91 TEST */,
    SG_WORD(48),
    0x00000105    /*  93 LREF */,
    0x00000041    /*  94 VECTORP */,
    0x00000017    /*  95 TEST */,
    SG_WORD(39),
    0x00000005    /*  97 LREF */,
    0x00000042    /*  98 VEC_LEN */,
    0x0000000b    /*  99 PUSH */,
    0x00000105    /* 100 LREF */,
    0x00000042    /* 101 VEC_LEN */,
    0x0000000b    /* 102 PUSH */,
    0x00000245    /* 103 LREF_PUSH */,
    0x00000305    /* 104 LREF */,
    0x0000001a    /* 105 BNNUME */,
    SG_WORD(28),
    0x00000049    /* 107 CONSTI_PUSH */,
    0x00000445    /* 108 LREF_PUSH */,
    0x00000205    /* 109 LREF */,
    0x0000001a    /* 110 BNNUME */,
    SG_WORD(2),
    0x0000002f    /* 112 RET */,
    0x00000030    /* 113 FRAME */,
    SG_WORD(12),
    0x00000045    /* 115 LREF_PUSH */,
    0x00000405    /* 116 LREF */,
    0x00000043    /* 117 VEC_REF */,
    0x0000000b    /* 118 PUSH */,
    0x00000145    /* 119 LREF_PUSH */,
    0x00000405    /* 120 LREF */,
    0x00000043    /* 121 VEC_REF */,
    0x0000000b    /* 122 PUSH */,
    0x00000009    /* 123 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x0000022c    /* 125 LOCAL_CALL */,
    0x00000017    /* 126 TEST */,
    SG_WORD(7),
    0x00000405    /* 128 LREF */,
    0x0000010f    /* 129 ADDI */,
    0x0000000b    /* 130 PUSH */,
    0x00400119    /* 131 SHIFTJ */,
    0x00000018    /* 132 JUMP */,
    SG_WORD(-25),
    0x0000002f    /* 134 RET */,
    0x00000045    /* 135 LREF_PUSH */,
    0x00000145    /* 136 LREF_PUSH */,
    0x0000024b    /* 137 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier equal?#core.macro> */,
    0x0000002f    /* 139 RET */,
    0x00000018    /* 140 JUMP */,
    SG_WORD(-6),
    0x0000002f    /* 142 RET */,
    0x00000018    /* 143 JUMP */,
    SG_WORD(-55),
    0x0000002f    /* 145 RET */,
    0x00000018    /* 146 JUMP */,
    SG_WORD(-124),
    0x0000002f    /* 148 RET */,
    /* lookup-transformer-env */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(37),
    0x00000030    /*   7 FRAME */,
    SG_WORD(14),
    0x00000030    /*   9 FRAME */,
    SG_WORD(4),
    0x00000045    /*  11 LREF_PUSH */,
    0x0000014a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-name#core.macro> */,
    0x0000000b    /*  14 PUSH */,
    0x00000030    /*  15 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  17 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x0000000b    /*  19 PUSH */,
    0x0000024a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x0000000b    /*  22 PUSH */,
    0x00000105    /*  23 LREF */,
    0x00000017    /*  24 TEST */,
    SG_WORD(17),
    0x00000156    /*  26 LREF_CDR */,
    0x0000003e    /*  27 PAIRP */,
    0x00000017    /*  28 TEST */,
    SG_WORD(13),
    0x00000030    /*  30 FRAME */,
    SG_WORD(4),
    0x00000045    /*  32 LREF_PUSH */,
    0x0000014a    /*  33 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-library#core.macro> */,
    0x0000000b    /*  35 PUSH */,
    0x00000105    /*  36 LREF */,
    0x0000004f    /*  37 CADR */,
    0x0000001f    /*  38 BNEQ */,
    SG_WORD(3),
    0x00000105    /*  40 LREF */,
    0x00000051    /*  41 CDDR */,
    0x0000002f    /*  42 RET */,
    0x00000005    /*  43 LREF */,
    0x0000003f    /*  44 SYMBOLP */,
    0x00000017    /*  45 TEST */,
    SG_WORD(22),
    0x00000030    /*  47 FRAME */,
    SG_WORD(9),
    0x00000045    /*  49 LREF_PUSH */,
    0x00000030    /*  50 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  52 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x0000000b    /*  54 PUSH */,
    0x0000024a    /*  55 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x0000000b    /*  57 PUSH */,
    0x00000105    /*  58 LREF */,
    0x00000017    /*  59 TEST */,
    SG_WORD(7),
    0x00000156    /*  61 LREF_CDR */,
    0x0000003e    /*  62 PAIRP */,
    0x00000022    /*  63 NOT */,
    0x00000017    /*  64 TEST */,
    SG_WORD(2),
    0x00000156    /*  66 LREF_CDR */,
    0x0000002f    /*  67 RET */,
    0x00000002    /*  68 UNDEF */,
    0x0000002f    /*  69 RET */,
    /* add-to-transformer-env! */0x00000030    /*   0 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   2 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x0000000b    /*   4 PUSH */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(4),
    0x00000045    /*   7 LREF_PUSH */,
    0x0000014a    /*   8 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  10 TEST */,
    SG_WORD(24),
    0x00000030    /*  12 FRAME */,
    SG_WORD(20),
    0x00000030    /*  14 FRAME */,
    SG_WORD(4),
    0x00000045    /*  16 LREF_PUSH */,
    0x0000014a    /*  17 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-name#core.macro> */,
    0x0000000b    /*  19 PUSH */,
    0x00000030    /*  20 FRAME */,
    SG_WORD(4),
    0x00000045    /*  22 LREF_PUSH */,
    0x0000014a    /*  23 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-library#core.macro> */,
    0x0000000b    /*  25 PUSH */,
    0x00000105    /*  26 LREF */,
    0x00000037    /*  27 CONS */,
    0x00000054    /*  28 CONS_PUSH */,
    0x00000205    /*  29 LREF */,
    0x00000054    /*  30 CONS_PUSH */,
    0x0000014a    /*  31 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x00000018    /*  33 JUMP */,
    SG_WORD(10),
    0x00000030    /*  35 FRAME */,
    SG_WORD(8),
    0x00000045    /*  37 LREF_PUSH */,
    0x00000105    /*  38 LREF */,
    0x00000054    /*  39 CONS_PUSH */,
    0x00000205    /*  40 LREF */,
    0x00000054    /*  41 CONS_PUSH */,
    0x0000014a    /*  42 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x00000105    /*  44 LREF */,
    0x0000002f    /*  45 RET */,
    /* generate-identity */0x00000048    /*   0 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* id. */,
    0x0000014b    /*   2 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier gensym#core.macro> */,
    0x0000002f    /*   4 RET */,
    /* %make-identifier */0x00000005    /*   0 LREF */,
    0x0000003f    /*   1 SYMBOLP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(4),
    0x00000002    /*   4 UNDEF */,
    0x00000018    /*   5 JUMP */,
    SG_WORD(21),
    0x00000030    /*   7 FRAME */,
    SG_WORD(4),
    0x00000045    /*   9 LREF_PUSH */,
    0x0000014a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  12 TEST */,
    SG_WORD(5),
    0x00000018    /*  14 JUMP */,
    SG_WORD(-11),
    0x00000018    /*  16 JUMP */,
    SG_WORD(10),
    0x00000030    /*  18 FRAME */,
    SG_WORD(8),
    0x00000048    /*  20 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* make-identifier */,
    0x00000048    /*  22 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* symbol or identifier is required */,
    0x00000045    /*  24 LREF_PUSH */,
    0x0000034a    /*  25 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.macro> */,
    0x00000030    /*  27 FRAME */,
    SG_WORD(4),
    0x00000045    /*  29 LREF_PUSH */,
    0x0000014a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  32 TEST */,
    SG_WORD(8),
    0x00000030    /*  34 FRAME */,
    SG_WORD(4),
    0x00000045    /*  36 LREF_PUSH */,
    0x0000014a    /*  37 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-name#core.macro> */,
    0x00000018    /*  39 JUMP */,
    SG_WORD(2),
    0x00000005    /*  41 LREF */,
    0x0000000b    /*  42 PUSH */,
    0x00000030    /*  43 FRAME */,
    SG_WORD(4),
    0x00000245    /*  45 LREF_PUSH */,
    0x0000014a    /*  46 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier library?#core.macro> */,
    0x00000017    /*  48 TEST */,
    SG_WORD(4),
    0x00000205    /*  50 LREF */,
    0x00000018    /*  51 JUMP */,
    SG_WORD(8),
    0x00000030    /*  53 FRAME */,
    SG_WORD(6),
    0x00000245    /*  55 LREF_PUSH */,
    0x00000048    /*  56 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000024a    /*  58 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier find-library#core.macro> */,
    0x0000000b    /*  60 PUSH */,
    0x00000105    /*  61 LREF */,
    0x00000021    /*  62 BNNULL */,
    SG_WORD(5),
    0x00000003    /*  64 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*  66 JUMP */,
    SG_WORD(19),
    0x00000030    /*  68 FRAME */,
    SG_WORD(4),
    0x00000045    /*  70 LREF_PUSH */,
    0x0000014a    /*  71 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  73 TEST */,
    SG_WORD(10),
    0x00000145    /*  75 LREF_PUSH */,
    0x00000030    /*  76 FRAME */,
    SG_WORD(4),
    0x00000045    /*  78 LREF_PUSH */,
    0x0000014a    /*  79 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-envs#core.macro> */,
    0x00000037    /*  81 CONS */,
    0x00000018    /*  82 JUMP */,
    SG_WORD(3),
    0x00000105    /*  84 LREF */,
    0x00000138    /*  85 LIST */,
    0x0000000b    /*  86 PUSH */,
    0x00000105    /*  87 LREF */,
    0x00000021    /*  88 BNNULL */,
    SG_WORD(5),
    0x00000003    /*  90 CONST */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000018    /*  92 JUMP */,
    SG_WORD(29),
    0x00000030    /*  94 FRAME */,
    SG_WORD(4),
    0x00000045    /*  96 LREF_PUSH */,
    0x0000014a    /*  97 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  99 TEST */,
    SG_WORD(14),
    0x00000030    /* 101 FRAME */,
    SG_WORD(3),
    0x0000004a    /* 103 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x0000000b    /* 105 PUSH */,
    0x00000030    /* 106 FRAME */,
    SG_WORD(4),
    0x00000045    /* 108 LREF_PUSH */,
    0x0000014a    /* 109 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-identity#core.macro> */,
    0x00000037    /* 111 CONS */,
    0x00000018    /* 112 JUMP */,
    SG_WORD(9),
    0x00000030    /* 114 FRAME */,
    SG_WORD(3),
    0x0000004a    /* 116 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x0000000b    /* 118 PUSH */,
    0x00000003    /* 119 CONST */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000037    /* 121 CONS */,
    0x0000000b    /* 122 PUSH */,
    0x00000445    /* 123 LREF_PUSH */,
    0x00000645    /* 124 LREF_PUSH */,
    0x00000745    /* 125 LREF_PUSH */,
    0x00000545    /* 126 LREF_PUSH */,
    0x00000345    /* 127 LREF_PUSH */,
    0x0000054b    /* 128 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-raw-identifier#core.macro> */,
    0x0000002f    /* 130 RET */,
    /* make-identifier */0x00000045    /*   0 LREF_PUSH */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000245    /*   2 LREF_PUSH */,
    0x00000048    /*   3 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000044b    /*   5 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %make-identifier#core.macro> */,
    0x0000002f    /*   7 RET */,
    /* make-pending-identifier */0x00000045    /*   0 LREF_PUSH */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000245    /*   2 LREF_PUSH */,
    0x00000048    /*   3 CONST_PUSH */,
    SG_WORD(SG_TRUE) /* #t */,
    0x0000044b    /*   5 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %make-identifier#core.macro> */,
    0x0000002f    /*   7 RET */,
    /* p1env-library */0x00000045    /*   0 LREF_PUSH */,
    0x00000004    /*   1 CONSTI */,
    0x00000043    /*   2 VEC_REF */,
    0x0000002f    /*   3 RET */,
    /* p1env-frames */0x00000045    /*   0 LREF_PUSH */,
    0x00000104    /*   1 CONSTI */,
    0x00000043    /*   2 VEC_REF */,
    0x0000002f    /*   3 RET */,
    /* free-identifier=? */0x00000045    /*   0 LREF_PUSH */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000030    /*   2 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x0000000b    /*   6 PUSH */,
    0x0000034b    /*   7 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vm-free-identifier=?#core.macro> */,
    0x0000002f    /*   9 RET */,
    /* literal-match? */0x00000163    /*   0 RESV_STACK */,
    0x00000009    /*   1 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15865#core.macro> */,
    0x00000231    /*   3 INST_STACK */,
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x00000045    /*   6 LREF_PUSH */,
    0x0000014a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   9 TEST */,
    SG_WORD(9),
    0x00000047    /*  11 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x00000045    /*  13 LREF_PUSH */,
    0x00000145    /*  14 LREF_PUSH */,
    0x00000009    /*  15 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15865#core.macro> */,
    0x0000032e    /*  17 LOCAL_TAIL_CALL */,
    0x0000002f    /*  18 RET */,
    0x00000047    /*  19 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15864#core.macro> */,
    0x00000045    /*  21 LREF_PUSH */,
    0x00000145    /*  22 LREF_PUSH */,
    0x00000009    /*  23 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15865#core.macro> */,
    0x0000032e    /*  25 LOCAL_TAIL_CALL */,
    0x0000002f    /*  26 RET */,
    /* bar? */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(41),
    0x00000030    /*   7 FRAME */,
    SG_WORD(6),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000047    /*  10 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .bar#core.macro> */,
    0x0000024a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier free-identifier=?#core.macro> */,
    0x00000017    /*  14 TEST */,
    SG_WORD(2),
    0x0000002f    /*  16 RET */,
    0x00000030    /*  17 FRAME */,
    SG_WORD(4),
    0x00000045    /*  19 LREF_PUSH */,
    0x0000014a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  22 TEST */,
    SG_WORD(23),
    0x00000030    /*  24 FRAME */,
    SG_WORD(15),
    0x00000030    /*  26 FRAME */,
    SG_WORD(10),
    0x00000030    /*  28 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x0000000b    /*  32 PUSH */,
    0x00000045    /*  33 LREF_PUSH */,
    0x00000049    /*  34 CONSTI_PUSH */,
    0x0000034a    /*  35 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  37 PUSH */,
    0x0000014a    /*  38 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  40 TEST */,
    SG_WORD(5),
    0x00000045    /*  42 LREF_PUSH */,
    0x00000003    /*  43 CONST */,
    SG_WORD(SG_UNDEF) /* _ */,
    0x0000003b    /*  45 EQ */,
    0x0000002f    /*  46 RET */,
    0x00000018    /*  47 JUMP */,
    SG_WORD(-31),
    0x0000002f    /*  49 RET */,
    /* ellipsis? */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(41),
    0x00000030    /*   7 FRAME */,
    SG_WORD(6),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000047    /*  10 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .ellipsis#core.macro> */,
    0x0000024a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier free-identifier=?#core.macro> */,
    0x00000017    /*  14 TEST */,
    SG_WORD(2),
    0x0000002f    /*  16 RET */,
    0x00000030    /*  17 FRAME */,
    SG_WORD(4),
    0x00000045    /*  19 LREF_PUSH */,
    0x0000014a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  22 TEST */,
    SG_WORD(23),
    0x00000030    /*  24 FRAME */,
    SG_WORD(15),
    0x00000030    /*  26 FRAME */,
    SG_WORD(10),
    0x00000030    /*  28 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x0000000b    /*  32 PUSH */,
    0x00000045    /*  33 LREF_PUSH */,
    0x00000049    /*  34 CONSTI_PUSH */,
    0x0000034a    /*  35 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  37 PUSH */,
    0x0000014a    /*  38 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  40 TEST */,
    SG_WORD(5),
    0x00000045    /*  42 LREF_PUSH */,
    0x00000003    /*  43 CONST */,
    SG_WORD(SG_UNDEF) /* ... */,
    0x0000003b    /*  45 EQ */,
    0x0000002f    /*  46 RET */,
    0x00000018    /*  47 JUMP */,
    SG_WORD(-31),
    0x0000002f    /*  49 RET */,
    /* ellipsis-pair? */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(10),
    0x00000056    /*   4 LREF_CDR */,
    0x0000003e    /*   5 PAIRP */,
    0x00000017    /*   6 TEST */,
    SG_WORD(6),
    0x00000005    /*   8 LREF */,
    0x0000004f    /*   9 CADR */,
    0x0000000b    /*  10 PUSH */,
    0x0000014b    /*  11 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x0000002f    /*  13 RET */,
    /* ellipsis-splicing-pair? */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(27),
    0x00000056    /*   4 LREF_CDR */,
    0x0000003e    /*   5 PAIRP */,
    0x00000017    /*   6 TEST */,
    SG_WORD(23),
    0x00000030    /*   8 FRAME */,
    SG_WORD(6),
    0x00000005    /*  10 LREF */,
    0x0000004f    /*  11 CADR */,
    0x0000000b    /*  12 PUSH */,
    0x0000014a    /*  13 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  15 TEST */,
    SG_WORD(14),
    0x00000005    /*  17 LREF */,
    0x00000051    /*  18 CDDR */,
    0x0000003e    /*  19 PAIRP */,
    0x00000017    /*  20 TEST */,
    SG_WORD(9),
    0x00000030    /*  22 FRAME */,
    SG_WORD(4),
    0x00000045    /*  24 LREF_PUSH */,
    0x0000014a    /*  25 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  27 PUSH */,
    0x0000014b    /*  28 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x0000002f    /*  30 RET */,
    /* ellipsis-quote? */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(15),
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x0000005b    /*   6 LREF_CAR_PUSH */,
    0x0000014a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*   9 TEST */,
    SG_WORD(8),
    0x00000056    /*  11 LREF_CDR */,
    0x0000003e    /*  12 PAIRP */,
    0x00000017    /*  13 TEST */,
    SG_WORD(4),
    0x00000005    /*  15 LREF */,
    0x00000051    /*  16 CDDR */,
    0x0000003d    /*  17 NULLP */,
    0x0000002f    /*  18 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(14),
    0x00000048    /*   7 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax pattern */,
    0x00000048    /*   9 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* improper use of ellipsis */,
    0x00000030    /*  11 FRAME */,
    SG_WORD(4),
    0x00000346    /*  13 FREF_PUSH */,
    0x0000014a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  16 PUSH */,
    0x0000034b    /*  17 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  19 RET */,
    0x00000005    /*  20 LREF */,
    0x0000003e    /*  21 PAIRP */,
    0x00000017    /*  22 TEST */,
    SG_WORD(108),
    0x00000056    /*  24 LREF_CDR */,
    0x0000003e    /*  25 PAIRP */,
    0x00000017    /*  26 TEST */,
    SG_WORD(101),
    0x00000030    /*  28 FRAME */,
    SG_WORD(6),
    0x00000005    /*  30 LREF */,
    0x0000004f    /*  31 CADR */,
    0x0000000b    /*  32 PUSH */,
    0x0000014a    /*  33 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  35 TEST */,
    SG_WORD(63),
    0x00000030    /*  37 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  39 LREF_CAR_PUSH */,
    0x0000014a    /*  40 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  42 TEST */,
    SG_WORD(29),
    0x00000030    /*  44 FRAME */,
    SG_WORD(5),
    0x0000005b    /*  46 LREF_CAR_PUSH */,
    0x00000246    /*  47 FREF_PUSH */,
    0x0000024a    /*  48 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /*  50 TEST */,
    SG_WORD(21),
    0x00000030    /*  52 FRAME */,
    SG_WORD(19),
    0x00000048    /*  54 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax pattern */,
    0x00000048    /*  56 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ellipsis following literal */,
    0x00000030    /*  58 FRAME */,
    SG_WORD(4),
    0x00000346    /*  60 FREF_PUSH */,
    0x0000014a    /*  61 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  63 PUSH */,
    0x00000030    /*  64 FRAME */,
    SG_WORD(4),
    0x00000045    /*  66 LREF_PUSH */,
    0x0000014a    /*  67 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  69 PUSH */,
    0x0000044a    /*  70 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000005    /*  72 LREF */,
    0x00000051    /*  73 CDDR */,
    0x0000000b    /*  74 PUSH */,
    0x00000105    /*  75 LREF */,
    0x0000003e    /*  76 PAIRP */,
    0x00000017    /*  77 TEST */,
    SG_WORD(20),
    0x00000030    /*  79 FRAME */,
    SG_WORD(4),
    0x0000015b    /*  81 LREF_CAR_PUSH */,
    0x0000014a    /*  82 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  84 TEST */,
    SG_WORD(9),
    0x00000048    /*  86 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax pattern */,
    0x00000048    /*  88 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ambiguous use of ellipsis */,
    0x00000146    /*  90 FREF_PUSH */,
    0x0000034b    /*  91 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.macro> */,
    0x0000002f    /*  93 RET */,
    0x0000015c    /*  94 LREF_CDR_PUSH */,
    0x00100119    /*  95 SHIFTJ */,
    0x00000018    /*  96 JUMP */,
    SG_WORD(-22),
    0x0000002f    /*  98 RET */,
    0x00000005    /*  99 LREF */,
    0x0000003e    /* 100 PAIRP */,
    0x00000017    /* 101 TEST */,
    SG_WORD(13),
    0x00000030    /* 103 FRAME */,
    SG_WORD(4),
    0x0000005b    /* 105 LREF_CAR_PUSH */,
    0x00000007    /* 106 FREF */,
    0x0000012c    /* 107 LOCAL_CALL */,
    0x00000017    /* 108 TEST */,
    SG_WORD(2),
    0x0000002f    /* 110 RET */,
    0x0000005c    /* 111 LREF_CDR_PUSH */,
    0x00000007    /* 112 FREF */,
    0x0000012e    /* 113 LOCAL_TAIL_CALL */,
    0x0000002f    /* 114 RET */,
    0x00000005    /* 115 LREF */,
    0x00000041    /* 116 VECTORP */,
    0x00000017    /* 117 TEST */,
    SG_WORD(9),
    0x00000030    /* 119 FRAME */,
    SG_WORD(4),
    0x00000045    /* 121 LREF_PUSH */,
    0x0000014a    /* 122 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 124 PUSH */,
    0x00000007    /* 125 FREF */,
    0x0000012e    /* 126 LOCAL_TAIL_CALL */,
    0x0000002f    /* 127 RET */,
    0x00000018    /* 128 JUMP */,
    SG_WORD(-30),
    0x0000002f    /* 130 RET */,
    0x00000018    /* 131 JUMP */,
    SG_WORD(-33),
    0x0000002f    /* 133 RET */,
    /* loop */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(12),
    0x0000005c    /*   4 LREF_CDR_PUSH */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(5),
    0x0000005b    /*   7 LREF_CAR_PUSH */,
    0x00000145    /*   8 LREF_PUSH */,
    0x00000207    /*   9 FREF */,
    0x0000022c    /*  10 LOCAL_CALL */,
    0x0000000b    /*  11 PUSH */,
    0x00000207    /*  12 FREF */,
    0x0000022e    /*  13 LOCAL_TAIL_CALL */,
    0x0000002f    /*  14 RET */,
    0x00000030    /*  15 FRAME */,
    SG_WORD(4),
    0x00000045    /*  17 LREF_PUSH */,
    0x0000014a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  20 TEST */,
    SG_WORD(3),
    0x00000105    /*  22 LREF */,
    0x0000002f    /*  23 RET */,
    0x00000030    /*  24 FRAME */,
    SG_WORD(4),
    0x00000045    /*  26 LREF_PUSH */,
    0x0000014a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bar?#core.macro> */,
    0x00000017    /*  29 TEST */,
    SG_WORD(3),
    0x00000105    /*  31 LREF */,
    0x0000002f    /*  32 RET */,
    0x00000030    /*  33 FRAME */,
    SG_WORD(4),
    0x00000045    /*  35 LREF_PUSH */,
    0x0000014a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  38 TEST */,
    SG_WORD(42),
    0x00000030    /*  40 FRAME */,
    SG_WORD(5),
    0x00000045    /*  42 LREF_PUSH */,
    0x00000146    /*  43 FREF_PUSH */,
    0x0000024a    /*  44 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /*  46 TEST */,
    SG_WORD(3),
    0x00000105    /*  48 LREF */,
    0x0000002f    /*  49 RET */,
    0x00000030    /*  50 FRAME */,
    SG_WORD(5),
    0x00000045    /*  52 LREF_PUSH */,
    0x00000145    /*  53 LREF_PUSH */,
    0x0000024a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier memq#core.macro> */,
    0x00000017    /*  56 TEST */,
    SG_WORD(20),
    0x00000048    /*  58 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax pattern */,
    0x00000048    /*  60 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* duplicate pattern variables */,
    0x00000030    /*  62 FRAME */,
    SG_WORD(4),
    0x00000046    /*  64 FREF_PUSH */,
    0x0000014a    /*  65 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  67 PUSH */,
    0x00000030    /*  68 FRAME */,
    SG_WORD(4),
    0x00000045    /*  70 LREF_PUSH */,
    0x0000014a    /*  71 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  73 PUSH */,
    0x0000044b    /*  74 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  76 RET */,
    0x00000045    /*  77 LREF_PUSH */,
    0x00000105    /*  78 LREF */,
    0x00000037    /*  79 CONS */,
    0x0000002f    /*  80 RET */,
    0x00000005    /*  81 LREF */,
    0x00000041    /*  82 VECTORP */,
    0x00000017    /*  83 TEST */,
    SG_WORD(11),
    0x00000030    /*  85 FRAME */,
    SG_WORD(4),
    0x00000045    /*  87 LREF_PUSH */,
    0x0000014a    /*  88 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  90 PUSH */,
    0x00000145    /*  91 LREF_PUSH */,
    0x00000207    /*  92 FREF */,
    0x0000022e    /*  93 LOCAL_TAIL_CALL */,
    0x0000002f    /*  94 RET */,
    0x00000105    /*  95 LREF */,
    0x0000002f    /*  96 RET */,
    /* check-pattern */0x00000045    /*   0 LREF_PUSH */,
    0x00000163    /*   1 RESV_STACK */,
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x00000245    /*   4 LREF_PUSH */,
    0x00000345    /*   5 LREF_PUSH */,
    0x00000129    /*   6 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[30])) /* #<code-builder loop (1 0 4)> */,
    0x00000331    /*   8 INST_STACK */,
    0x00000030    /*   9 FRAME */,
    SG_WORD(4),
    0x00000245    /*  11 LREF_PUSH */,
    0x00000305    /*  12 LREF */,
    0x0000012c    /*  13 LOCAL_CALL */,
    0x00000232    /*  14 LEAVE */,
    0x00000045    /*  15 LREF_PUSH */,
    0x00000163    /*  16 RESV_STACK */,
    0x00000345    /*  17 LREF_PUSH */,
    0x00000145    /*  18 LREF_PUSH */,
    0x00000045    /*  19 LREF_PUSH */,
    0x00000329    /*  20 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[31])) /* #<code-builder loop (2 0 3)> */,
    0x00000331    /*  22 INST_STACK */,
    0x00000245    /*  23 LREF_PUSH */,
    0x00000048    /*  24 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000305    /*  26 LREF */,
    0x0000022e    /*  27 LOCAL_TAIL_CALL */,
    0x0000002f    /*  28 RET */,
    /* collect-unique-ids */0x00000163    /*   0 RESV_STACK */,
    0x00000009    /*   1 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x00000131    /*   3 INST_STACK */,
    0x00000045    /*   4 LREF_PUSH */,
    0x00000048    /*   5 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000009    /*   7 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x0000022e    /*   9 LOCAL_TAIL_CALL */,
    0x0000002f    /*  10 RET */,
    /* collect-vars-ranks */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bar?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000305    /*   7 LREF */,
    0x0000002f    /*   8 RET */,
    0x00000030    /*   9 FRAME */,
    SG_WORD(4),
    0x00000045    /*  11 LREF_PUSH */,
    0x0000014a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  14 TEST */,
    SG_WORD(17),
    0x00000030    /*  16 FRAME */,
    SG_WORD(5),
    0x00000045    /*  18 LREF_PUSH */,
    0x00000145    /*  19 LREF_PUSH */,
    0x0000024a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /*  22 TEST */,
    SG_WORD(3),
    0x00000305    /*  24 LREF */,
    0x0000002f    /*  25 RET */,
    0x00000045    /*  26 LREF_PUSH */,
    0x00000205    /*  27 LREF */,
    0x00000054    /*  28 CONS_PUSH */,
    0x00000305    /*  29 LREF */,
    0x00000037    /*  30 CONS */,
    0x0000002f    /*  31 RET */,
    0x00000005    /*  32 LREF */,
    0x0000003e    /*  33 PAIRP */,
    0x00000017    /*  34 TEST */,
    SG_WORD(88),
    0x00000056    /*  36 LREF_CDR */,
    0x0000003e    /*  37 PAIRP */,
    0x00000017    /*  38 TEST */,
    SG_WORD(81),
    0x00000030    /*  40 FRAME */,
    SG_WORD(6),
    0x00000005    /*  42 LREF */,
    0x0000004f    /*  43 CADR */,
    0x0000000b    /*  44 PUSH */,
    0x0000014a    /*  45 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  47 TEST */,
    SG_WORD(35),
    0x00000005    /*  49 LREF */,
    0x00000051    /*  50 CDDR */,
    0x0000000b    /*  51 PUSH */,
    0x00000145    /*  52 LREF_PUSH */,
    0x00000245    /*  53 LREF_PUSH */,
    0x00000030    /*  54 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  56 LREF_CAR_PUSH */,
    0x0000014a    /*  57 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  59 TEST */,
    SG_WORD(9),
    0x0000005b    /*  61 LREF_CAR_PUSH */,
    0x00000205    /*  62 LREF */,
    0x0000010f    /*  63 ADDI */,
    0x00000054    /*  64 CONS_PUSH */,
    0x00000305    /*  65 LREF */,
    0x00000037    /*  66 CONS */,
    0x00000018    /*  67 JUMP */,
    SG_WORD(11),
    0x00000030    /*  69 FRAME */,
    SG_WORD(9),
    0x0000005b    /*  71 LREF_CAR_PUSH */,
    0x00000145    /*  72 LREF_PUSH */,
    0x00000205    /*  73 LREF */,
    0x0000010f    /*  74 ADDI */,
    0x0000000b    /*  75 PUSH */,
    0x00000345    /*  76 LREF_PUSH */,
    0x0000044a    /*  77 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000000b    /*  79 PUSH */,
    0x0000044b    /*  80 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000002f    /*  82 RET */,
    0x00000005    /*  83 LREF */,
    0x0000003e    /*  84 PAIRP */,
    0x00000017    /*  85 TEST */,
    SG_WORD(16),
    0x0000005c    /*  87 LREF_CDR_PUSH */,
    0x00000145    /*  88 LREF_PUSH */,
    0x00000245    /*  89 LREF_PUSH */,
    0x00000030    /*  90 FRAME */,
    SG_WORD(7),
    0x0000005b    /*  92 LREF_CAR_PUSH */,
    0x00000145    /*  93 LREF_PUSH */,
    0x00000245    /*  94 LREF_PUSH */,
    0x00000345    /*  95 LREF_PUSH */,
    0x0000044a    /*  96 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000000b    /*  98 PUSH */,
    0x0000044b    /*  99 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000002f    /* 101 RET */,
    0x00000005    /* 102 LREF */,
    0x00000041    /* 103 VECTORP */,
    0x00000017    /* 104 TEST */,
    SG_WORD(13),
    0x00000030    /* 106 FRAME */,
    SG_WORD(4),
    0x00000045    /* 108 LREF_PUSH */,
    0x0000014a    /* 109 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 111 PUSH */,
    0x00000145    /* 112 LREF_PUSH */,
    0x00000245    /* 113 LREF_PUSH */,
    0x00000345    /* 114 LREF_PUSH */,
    0x0000044b    /* 115 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000002f    /* 117 RET */,
    0x00000305    /* 118 LREF */,
    0x0000002f    /* 119 RET */,
    0x00000018    /* 120 JUMP */,
    SG_WORD(-38),
    0x0000002f    /* 122 RET */,
    0x00000018    /* 123 JUMP */,
    SG_WORD(-41),
    0x0000002f    /* 125 RET */,
    /* seen-or-gen */0x00000030    /*   0 FRAME */,
    SG_WORD(7),
    0x00000146    /*   2 FREF_PUSH */,
    0x00000045    /*   3 LREF_PUSH */,
    0x00000048    /*   4 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*   6 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-ref#core.macro> */,
    0x00000017    /*   8 TEST */,
    SG_WORD(2),
    0x0000002f    /*  10 RET */,
    0x00000030    /*  11 FRAME */,
    SG_WORD(6),
    0x00000045    /*  13 LREF_PUSH */,
    0x00000145    /*  14 LREF_PUSH */,
    0x00000245    /*  15 LREF_PUSH */,
    0x00000007    /*  16 FREF */,
    0x0000032b    /*  17 CALL */,
    0x0000000b    /*  18 PUSH */,
    0x00000030    /*  19 FRAME */,
    SG_WORD(6),
    0x00000146    /*  21 FREF_PUSH */,
    0x00000045    /*  22 LREF_PUSH */,
    0x00000345    /*  23 LREF_PUSH */,
    0x0000034a    /*  24 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x00000305    /*  26 LREF */,
    0x0000002f    /*  27 RET */,
    /* loop */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(30),
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x0000005b    /*   6 LREF_CAR_PUSH */,
    0x00000407    /*   7 FREF */,
    0x0000012c    /*   8 LOCAL_CALL */,
    0x0000000b    /*   9 PUSH */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  12 LREF_CDR_PUSH */,
    0x00000407    /*  13 FREF */,
    0x0000012c    /*  14 LOCAL_CALL */,
    0x0000000b    /*  15 PUSH */,
    0x00000145    /*  16 LREF_PUSH */,
    0x00000055    /*  17 LREF_CAR */,
    0x0000001f    /*  18 BNEQ */,
    SG_WORD(11),
    0x00000245    /*  20 LREF_PUSH */,
    0x00000056    /*  21 LREF_CDR */,
    0x0000001f    /*  22 BNEQ */,
    SG_WORD(3),
    0x00000005    /*  24 LREF */,
    0x0000002f    /*  25 RET */,
    0x00000145    /*  26 LREF_PUSH */,
    0x00000205    /*  27 LREF */,
    0x00000037    /*  28 CONS */,
    0x0000002f    /*  29 RET */,
    0x00000018    /*  30 JUMP */,
    SG_WORD(-5),
    0x0000002f    /*  32 RET */,
    0x00000005    /*  33 LREF */,
    0x00000041    /*  34 VECTORP */,
    0x00000017    /*  35 TEST */,
    SG_WORD(63),
    0x00000005    /*  37 LREF */,
    0x00000042    /*  38 VEC_LEN */,
    0x0000000b    /*  39 PUSH */,
    0x00000049    /*  40 CONSTI_PUSH */,
    0x00000048    /*  41 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000245    /*  43 LREF_PUSH */,
    0x00000105    /*  44 LREF */,
    0x0000001a    /*  45 BNNUME */,
    SG_WORD(7),
    0x00000305    /*  47 LREF */,
    0x00000017    /*  48 TEST */,
    SG_WORD(2),
    0x0000002f    /*  50 RET */,
    0x00000005    /*  51 LREF */,
    0x0000002f    /*  52 RET */,
    0x00000030    /*  53 FRAME */,
    SG_WORD(7),
    0x00000045    /*  55 LREF_PUSH */,
    0x00000205    /*  56 LREF */,
    0x00000043    /*  57 VEC_REF */,
    0x0000000b    /*  58 PUSH */,
    0x00000407    /*  59 FREF */,
    0x0000012c    /*  60 LOCAL_CALL */,
    0x0000000b    /*  61 PUSH */,
    0x00000445    /*  62 LREF_PUSH */,
    0x00000045    /*  63 LREF_PUSH */,
    0x00000205    /*  64 LREF */,
    0x00000043    /*  65 VEC_REF */,
    0x0000001f    /*  66 BNEQ */,
    SG_WORD(9),
    0x00000205    /*  68 LREF */,
    0x0000010f    /*  69 ADDI */,
    0x0000000b    /*  70 PUSH */,
    0x00000345    /*  71 LREF_PUSH */,
    0x00200219    /*  72 SHIFTJ */,
    0x00000018    /*  73 JUMP */,
    SG_WORD(-31),
    0x0000002f    /*  75 RET */,
    0x00000305    /*  76 LREF */,
    0x00000017    /*  77 TEST */,
    SG_WORD(3),
    0x00000018    /*  79 JUMP */,
    SG_WORD(6),
    0x00000030    /*  81 FRAME */,
    SG_WORD(4),
    0x00000045    /*  83 LREF_PUSH */,
    0x0000014a    /*  84 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector-copy#core.macro> */,
    0x0000000b    /*  86 PUSH */,
    0x00000545    /*  87 LREF_PUSH */,
    0x00000245    /*  88 LREF_PUSH */,
    0x00000405    /*  89 LREF */,
    0x00000044    /*  90 VEC_SET */,
    0x00000205    /*  91 LREF */,
    0x0000010f    /*  92 ADDI */,
    0x0000000b    /*  93 PUSH */,
    0x00000545    /*  94 LREF_PUSH */,
    0x00200219    /*  95 SHIFTJ */,
    0x00000018    /*  96 JUMP */,
    SG_WORD(-54),
    0x0000002f    /*  98 RET */,
    0x00000030    /*  99 FRAME */,
    SG_WORD(4),
    0x00000045    /* 101 LREF_PUSH */,
    0x0000014a    /* 102 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /* 104 TEST */,
    SG_WORD(31),
    0x00000030    /* 106 FRAME */,
    SG_WORD(4),
    0x00000045    /* 108 LREF_PUSH */,
    0x00000307    /* 109 FREF */,
    0x0000012b    /* 110 CALL */,
    0x00000017    /* 111 TEST */,
    SG_WORD(12),
    0x00000045    /* 113 LREF_PUSH */,
    0x00000246    /* 114 FREF_PUSH */,
    0x00000030    /* 115 FRAME */,
    SG_WORD(4),
    0x00000045    /* 117 LREF_PUSH */,
    0x0000014a    /* 118 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-library#core.macro> */,
    0x0000000b    /* 120 PUSH */,
    0x00000107    /* 121 FREF */,
    0x0000032e    /* 122 LOCAL_TAIL_CALL */,
    0x0000002f    /* 123 RET */,
    0x00000005    /* 124 LREF */,
    0x0000003f    /* 125 SYMBOLP */,
    0x00000017    /* 126 TEST */,
    SG_WORD(7),
    0x00000045    /* 128 LREF_PUSH */,
    0x00000246    /* 129 FREF_PUSH */,
    0x00000046    /* 130 FREF_PUSH */,
    0x00000107    /* 131 FREF */,
    0x0000032e    /* 132 LOCAL_TAIL_CALL */,
    0x0000002f    /* 133 RET */,
    0x00000005    /* 134 LREF */,
    0x0000002f    /* 135 RET */,
    0x00000018    /* 136 JUMP */,
    SG_WORD(-13),
    0x0000002f    /* 138 RET */,
    /* rewrite-form */0x00000163    /*   0 RESV_STACK */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000445    /*   2 LREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[35])) /* #<code-builder seen-or-gen (3 0 2)> */,
    0x00000631    /*   5 INST_STACK */,
    0x00000163    /*   6 RESV_STACK */,
    0x00000745    /*   7 LREF_PUSH */,
    0x00000545    /*   8 LREF_PUSH */,
    0x00000245    /*   9 LREF_PUSH */,
    0x00000645    /*  10 LREF_PUSH */,
    0x00000345    /*  11 LREF_PUSH */,
    0x00000529    /*  12 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[36])) /* #<code-builder loop (1 0 5)> */,
    0x00000731    /*  14 INST_STACK */,
    0x00000045    /*  15 LREF_PUSH */,
    0x00000705    /*  16 LREF */,
    0x0000012e    /*  17 LOCAL_TAIL_CALL */,
    0x0000002f    /*  18 RET */,
    /* pattern-identifier? */0x00000030    /*   0 FRAME */,
    SG_WORD(6),
    0x00000145    /*   2 LREF_PUSH */,
    0x00000045    /*   3 LREF_PUSH */,
    0x00000249    /*   4 CONSTI_PUSH */,
    0x0000034a    /*   5 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*   7 PUSH */,
    0x0000014b    /*   8 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier number?#core.macro> */,
    0x0000002f    /*  10 RET */,
    /* #f */0x00000046    /*   0 FREF_PUSH */,
    0x00000045    /*   1 LREF_PUSH */,
    0x00000145    /*   2 LREF_PUSH */,
    0x0000034b    /*   3 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x0000002f    /*   5 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier pending-identifier?#core.macro> */,
    0x00000022    /*   5 NOT */,
    0x00000017    /*   6 TEST */,
    SG_WORD(14),
    0x00000030    /*   8 FRAME */,
    SG_WORD(11),
    0x00000030    /*  10 FRAME */,
    SG_WORD(6),
    0x00000046    /*  12 FREF_PUSH */,
    0x00000045    /*  13 LREF_PUSH */,
    0x00000249    /*  14 CONSTI_PUSH */,
    0x0000034a    /*  15 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  17 PUSH */,
    0x0000014a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier number?#core.macro> */,
    0x00000022    /*  20 NOT */,
    0x0000002f    /*  21 RET */,
    /* rewrite */0x00000163    /*   0 RESV_STACK */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x00000331    /*   5 INST_STACK */,
    0x00000145    /*   6 LREF_PUSH */,
    0x00000405    /*   7 LREF */,
    0x00000021    /*   8 BNNULL */,
    SG_WORD(4),
    0x00000002    /*  10 UNDEF */,
    0x00000018    /*  11 JUMP */,
    SG_WORD(14),
    0x0000045b    /*  13 LREF_CAR_PUSH */,
    0x00000030    /*  14 FRAME */,
    SG_WORD(6),
    0x00000345    /*  16 LREF_PUSH */,
    0x0000055b    /*  17 LREF_CAR_PUSH */,
    0x0000055c    /*  18 LREF_CDR_PUSH */,
    0x0000034a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x0000045c    /*  21 LREF_CDR_PUSH */,
    0x00400119    /*  22 SHIFTJ */,
    0x00000018    /*  23 JUMP */,
    SG_WORD(-17),
    0x00000132    /*  25 LEAVE */,
    0x00000132    /*  26 LEAVE */,
    0x00000030    /*  27 FRAME */,
    SG_WORD(9),
    0x00000345    /*  29 LREF_PUSH */,
    0x00000029    /*  30 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[39])) /* #<code-builder #f (2 0 1)> */,
    0x0000000b    /*  32 PUSH */,
    0x00000346    /*  33 FREF_PUSH */,
    0x00000246    /*  34 FREF_PUSH */,
    0x0000034a    /*  35 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier for-each#core.macro> */,
    0x00000045    /*  37 LREF_PUSH */,
    0x00000345    /*  38 LREF_PUSH */,
    0x00000146    /*  39 FREF_PUSH */,
    0x00000046    /*  40 FREF_PUSH */,
    0x00000047    /*  41 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15860#core.macro> */,
    0x00000245    /*  43 LREF_PUSH */,
    0x00000029    /*  44 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[40])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*  46 PUSH */,
    0x0000064b    /*  47 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x0000002f    /*  49 RET */,
    /* gen-patvar */0x0000005b    /*   0 LREF_CAR_PUSH */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000030    /*   2 FRAME */,
    SG_WORD(6),
    0x00000145    /*   4 LREF_PUSH */,
    0x00000146    /*   5 FREF_PUSH */,
    0x00000046    /*   6 FREF_PUSH */,
    0x0000034a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000037    /*   9 CONS */,
    0x0000002f    /*  10 RET */,
    /* parse-pattern */0x00000163    /*   0 RESV_STACK */,
    0x00000546    /*   1 FREF_PUSH */,
    0x00000446    /*   2 FREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[42])) /* #<code-builder gen-patvar (1 0 2)> */,
    0x00000131    /*   5 INST_STACK */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(5),
    0x00000045    /*   8 LREF_PUSH */,
    0x00000346    /*   9 FREF_PUSH */,
    0x0000024a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier check-pattern#core.macro> */,
    0x00000030    /*  12 FRAME */,
    SG_WORD(8),
    0x00000045    /*  14 LREF_PUSH */,
    0x00000346    /*  15 FREF_PUSH */,
    0x00000049    /*  16 CONSTI_PUSH */,
    0x00000048    /*  17 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000044a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x0000000b    /*  21 PUSH */,
    0x00000030    /*  22 FRAME */,
    SG_WORD(5),
    0x00000145    /*  24 LREF_PUSH */,
    0x00000245    /*  25 LREF_PUSH */,
    0x0000024a    /*  26 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.macro> */,
    0x0000000b    /*  28 PUSH */,
    0x00000030    /*  29 FRAME */,
    SG_WORD(6),
    0x00000045    /*  31 LREF_PUSH */,
    0x00000345    /*  32 LREF_PUSH */,
    0x00000246    /*  33 FREF_PUSH */,
    0x00000107    /*  34 FREF */,
    0x0000032c    /*  35 LOCAL_CALL */,
    0x0000000b    /*  36 PUSH */,
    0x00000030    /*  37 FRAME */,
    SG_WORD(19),
    0x00000246    /*  39 FREF_PUSH */,
    0x00000030    /*  40 FRAME */,
    SG_WORD(6),
    0x00000245    /*  42 LREF_PUSH */,
    0x00000345    /*  43 LREF_PUSH */,
    0x00000246    /*  44 FREF_PUSH */,
    0x00000107    /*  45 FREF */,
    0x0000032c    /*  46 LOCAL_CALL */,
    0x0000000b    /*  47 PUSH */,
    0x00000249    /*  48 CONSTI_PUSH */,
    0x00000c05    /*  49 LREF */,
    0x00000054    /*  50 CONS_PUSH */,
    0x00000507    /*  51 FREF */,
    0x00000037    /*  52 CONS */,
    0x00000132    /*  53 LEAVE */,
    0x0000000b    /*  54 PUSH */,
    0x00000007    /*  55 FREF */,
    0x0000022b    /*  56 CALL */,
    0x0000000b    /*  57 PUSH */,
    0x00000305    /*  58 LREF */,
    0x0000033a    /*  59 VALUES */,
    0x0000002f    /*  60 RET */,
    /* #f */0x00000207    /*   0 FREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(86),
    0x0000025d    /*   4 FREF_CAR_PUSH */,
    0x0000025e    /*   5 FREF_CDR_PUSH */,
    0x00000105    /*   6 LREF */,
    0x0000003e    /*   7 PAIRP */,
    0x00000017    /*   8 TEST */,
    SG_WORD(74),
    0x0000015b    /*  10 LREF_CAR_PUSH */,
    0x0000015c    /*  11 LREF_CDR_PUSH */,
    0x00000305    /*  12 LREF */,
    0x0000003e    /*  13 PAIRP */,
    0x00000017    /*  14 TEST */,
    SG_WORD(62),
    0x0000035b    /*  16 LREF_CAR_PUSH */,
    0x0000035c    /*  17 LREF_CDR_PUSH */,
    0x00000505    /*  18 LREF */,
    0x00000021    /*  19 BNNULL */,
    SG_WORD(51),
    0x00000045    /*  21 LREF_PUSH */,
    0x00000245    /*  22 LREF_PUSH */,
    0x00000445    /*  23 LREF_PUSH */,
    0x00000030    /*  24 FRAME */,
    SG_WORD(4),
    0x00000645    /*  26 LREF_PUSH */,
    0x00000107    /*  27 FREF */,
    0x0000012c    /*  28 LOCAL_CALL */,
    0x00000328    /*  29 RECEIVE */,
    0x00000047    /*  30 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .list#core.macro> */,
    0x00000047    /*  32 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000905    /*  34 LREF */,
    0x00000238    /*  35 LIST */,
    0x0000000b    /*  36 PUSH */,
    0x00000047    /*  37 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .lambda#core.macro> */,
    0x00000009    /*  39 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000138    /*  41 LIST */,
    0x0000000b    /*  42 PUSH */,
    0x00000030    /*  43 FRAME */,
    SG_WORD(6),
    0x00000745    /*  45 LREF_PUSH */,
    0x00000b45    /*  46 LREF_PUSH */,
    0x00000a45    /*  47 LREF_PUSH */,
    0x00000007    /*  48 FREF */,
    0x0000032c    /*  49 LOCAL_CALL */,
    0x00000338    /*  50 LIST */,
    0x0000000b    /*  51 PUSH */,
    0x00000047    /*  52 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .lambda#core.macro> */,
    0x00000009    /*  54 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000138    /*  56 LIST */,
    0x0000000b    /*  57 PUSH */,
    0x00000030    /*  58 FRAME */,
    SG_WORD(6),
    0x00000845    /*  60 LREF_PUSH */,
    0x00000b45    /*  61 LREF_PUSH */,
    0x00000a45    /*  62 LREF_PUSH */,
    0x00000007    /*  63 FREF */,
    0x0000032c    /*  64 LOCAL_CALL */,
    0x00000338    /*  65 LIST */,
    0x00000438    /*  66 LIST */,
    0x0000000b    /*  67 PUSH */,
    0x00000a05    /*  68 LREF */,
    0x00000037    /*  69 CONS */,
    0x0000002f    /*  70 RET */,
    0x00000048    /*  71 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid form */,
    0x00000246    /*  73 FREF_PUSH */,
    0x0000024b    /*  74 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-error#~precomp.smatch> */,
    0x0000002f    /*  76 RET */,
    0x00000048    /*  77 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid form */,
    0x00000246    /*  79 FREF_PUSH */,
    0x0000024b    /*  80 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-error#~precomp.smatch> */,
    0x0000002f    /*  82 RET */,
    0x00000048    /*  83 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid form */,
    0x00000246    /*  85 FREF_PUSH */,
    0x0000024b    /*  86 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-error#~precomp.smatch> */,
    0x0000002f    /*  88 RET */,
    0x00000048    /*  89 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid form */,
    0x00000246    /*  91 FREF_PUSH */,
    0x0000024b    /*  92 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-error#~precomp.smatch> */,
    0x0000002f    /*  94 RET */,
    /* #f */0x00000045    /*   0 LREF_PUSH */,
    0x00000146    /*   1 FREF_PUSH */,
    0x00000046    /*   2 FREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[44])) /* #<code-builder #f (0 0 3)> */,
    0x0000000b    /*   5 PUSH */,
    0x00000005    /*   6 LREF */,
    0x0000003e    /*   7 PAIRP */,
    0x00000017    /*   8 TEST */,
    SG_WORD(54),
    0x0000005b    /*  10 LREF_CAR_PUSH */,
    0x0000005c    /*  11 LREF_CDR_PUSH */,
    0x00000305    /*  12 LREF */,
    0x0000003e    /*  13 PAIRP */,
    0x00000017    /*  14 TEST */,
    SG_WORD(45),
    0x0000035b    /*  16 LREF_CAR_PUSH */,
    0x0000035c    /*  17 LREF_CDR_PUSH */,
    0x00000505    /*  18 LREF */,
    0x00000021    /*  19 BNNULL */,
    SG_WORD(37),
    0x00000245    /*  21 LREF_PUSH */,
    0x00000445    /*  22 LREF_PUSH */,
    0x00000030    /*  23 FRAME */,
    SG_WORD(4),
    0x00000645    /*  25 LREF_PUSH */,
    0x00000107    /*  26 FREF */,
    0x0000012c    /*  27 LOCAL_CALL */,
    0x00000328    /*  28 RECEIVE */,
    0x00000047    /*  29 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .list#core.macro> */,
    0x00000047    /*  31 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000805    /*  33 LREF */,
    0x00000238    /*  34 LIST */,
    0x0000000b    /*  35 PUSH */,
    0x00000048    /*  36 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000047    /*  38 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .lambda#core.macro> */,
    0x00000009    /*  40 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000138    /*  42 LIST */,
    0x0000000b    /*  43 PUSH */,
    0x00000030    /*  44 FRAME */,
    SG_WORD(6),
    0x00000745    /*  46 LREF_PUSH */,
    0x00000a45    /*  47 LREF_PUSH */,
    0x00000945    /*  48 LREF_PUSH */,
    0x00000007    /*  49 FREF */,
    0x0000032c    /*  50 LOCAL_CALL */,
    0x00000338    /*  51 LIST */,
    0x00000438    /*  52 LIST */,
    0x0000000b    /*  53 PUSH */,
    0x00000905    /*  54 LREF */,
    0x00000037    /*  55 CONS */,
    0x0000002f    /*  56 RET */,
    0x00000105    /*  57 LREF */,
    0x0000002e    /*  58 LOCAL_TAIL_CALL */,
    0x0000002f    /*  59 RET */,
    0x00000105    /*  60 LREF */,
    0x0000002e    /*  61 LOCAL_TAIL_CALL */,
    0x0000002f    /*  62 RET */,
    0x00000105    /*  63 LREF */,
    0x0000002e    /*  64 LOCAL_TAIL_CALL */,
    0x0000002f    /*  65 RET */,
    /* compile-syntax-case */0x00000263    /*   0 RESV_STACK */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000831    /*   5 INST_STACK */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   8 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000931    /*  10 INST_STACK */,
    0x00000030    /*  11 FRAME */,
    SG_WORD(4),
    0x00000645    /*  13 LREF_PUSH */,
    0x0000014a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000030    /*  16 FRAME */,
    SG_WORD(4),
    0x00000645    /*  18 LREF_PUSH */,
    0x0000014a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000163    /*  21 RESV_STACK */,
    0x00000030    /*  22 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  24 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x00000a31    /*  26 INST_STACK */,
    0x00000030    /*  27 FRAME */,
    SG_WORD(11),
    0x00000245    /*  29 LREF_PUSH */,
    0x00000a45    /*  30 LREF_PUSH */,
    0x00000545    /*  31 LREF_PUSH */,
    0x00000445    /*  32 LREF_PUSH */,
    0x00000047    /*  33 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15862#core.macro> */,
    0x00000047    /*  35 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15861#core.macro> */,
    0x0000064a    /*  37 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x00000132    /*  39 LEAVE */,
    0x0000000b    /*  40 PUSH */,
    0x00000263    /*  41 RESV_STACK */,
    0x00000245    /*  42 LREF_PUSH */,
    0x00000a45    /*  43 LREF_PUSH */,
    0x00000545    /*  44 LREF_PUSH */,
    0x00000445    /*  45 LREF_PUSH */,
    0x00000029    /*  46 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[41])) /* #<code-builder rewrite (3 0 4)> */,
    0x00000b31    /*  48 INST_STACK */,
    0x00000545    /*  49 LREF_PUSH */,
    0x00000445    /*  50 LREF_PUSH */,
    0x00000a45    /*  51 LREF_PUSH */,
    0x00000645    /*  52 LREF_PUSH */,
    0x00000b45    /*  53 LREF_PUSH */,
    0x00000745    /*  54 LREF_PUSH */,
    0x00000029    /*  55 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[43])) /* #<code-builder parse-pattern (1 0 6)> */,
    0x00000c31    /*  57 INST_STACK */,
    0x00000030    /*  58 FRAME */,
    SG_WORD(4),
    0x00000a45    /*  60 LREF_PUSH */,
    0x0000014a    /*  61 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list?#core.macro> */,
    0x00000017    /*  63 TEST */,
    SG_WORD(24),
    0x00000030    /*  65 FRAME */,
    SG_WORD(6),
    0x00000047    /*  67 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000a45    /*  69 LREF_PUSH */,
    0x0000024a    /*  70 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier for-all#core.macro> */,
    0x00000017    /*  72 TEST */,
    SG_WORD(3),
    0x00000018    /*  74 JUMP */,
    SG_WORD(11),
    0x00000030    /*  76 FRAME */,
    SG_WORD(9),
    0x00000048    /*  78 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax-case */,
    0x00000048    /*  80 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid literals */,
    0x00000145    /*  82 LREF_PUSH */,
    0x00000a45    /*  83 LREF_PUSH */,
    0x0000044a    /*  84 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000018    /*  86 JUMP */,
    SG_WORD(3),
    0x00000018    /*  88 JUMP */,
    SG_WORD(-13),
    0x00000030    /*  90 FRAME */,
    SG_WORD(4),
    0x00000a45    /*  92 LREF_PUSH */,
    0x0000014a    /*  93 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unique-id-list?#core.macro> */,
    0x00000017    /*  95 TEST */,
    SG_WORD(3),
    0x00000018    /*  97 JUMP */,
    SG_WORD(11),
    0x00000030    /*  99 FRAME */,
    SG_WORD(9),
    0x00000048    /* 101 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax-case */,
    0x00000048    /* 103 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* duplicate literals */,
    0x00000145    /* 105 LREF_PUSH */,
    0x00000a45    /* 106 LREF_PUSH */,
    0x0000044a    /* 107 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /* 109 FRAME */,
    SG_WORD(6),
    0x00000047    /* 111 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .bar#core.macro> */,
    0x00000a45    /* 113 LREF_PUSH */,
    0x0000024a    /* 114 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /* 116 TEST */,
    SG_WORD(11),
    0x00000030    /* 118 FRAME */,
    SG_WORD(9),
    0x00000048    /* 120 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax-case */,
    0x00000048    /* 122 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* _ in literals */,
    0x00000145    /* 124 LREF_PUSH */,
    0x00000a45    /* 125 LREF_PUSH */,
    0x0000044a    /* 126 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /* 128 FRAME */,
    SG_WORD(6),
    0x00000047    /* 130 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .ellipsis#core.macro> */,
    0x00000a45    /* 132 LREF_PUSH */,
    0x0000024a    /* 133 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /* 135 TEST */,
    SG_WORD(11),
    0x00000030    /* 137 FRAME */,
    SG_WORD(9),
    0x00000048    /* 139 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax-case */,
    0x00000048    /* 141 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ... in literals */,
    0x00000145    /* 143 LREF_PUSH */,
    0x00000a45    /* 144 LREF_PUSH */,
    0x0000044a    /* 145 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /* 147 FRAME */,
    SG_WORD(9),
    0x00000c45    /* 149 LREF_PUSH */,
    0x00000b45    /* 150 LREF_PUSH */,
    0x00000029    /* 151 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[45])) /* #<code-builder #f (1 0 2)> */,
    0x0000000b    /* 153 PUSH */,
    0x00000345    /* 154 LREF_PUSH */,
    0x0000024a    /* 155 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.macro> */,
    0x0000000b    /* 157 PUSH */,
    0x00000030    /* 158 FRAME */,
    SG_WORD(4),
    0x00000845    /* 160 LREF_PUSH */,
    0x0000014a    /* 161 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000030    /* 163 FRAME */,
    SG_WORD(4),
    0x00000945    /* 165 LREF_PUSH */,
    0x0000014a    /* 166 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000047    /* 168 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .match-syntax-case#core.macro> */,
    0x00000a45    /* 170 LREF_PUSH */,
    0x00000030    /* 171 FRAME */,
    SG_WORD(8),
    0x00000645    /* 173 LREF_PUSH */,
    0x00000047    /* 174 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000049    /* 176 CONSTI_PUSH */,
    0x00000349    /* 177 CONSTI_PUSH */,
    0x0000044a    /* 178 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /* 180 PUSH */,
    0x00000d05    /* 181 LREF */,
    0x0000043a    /* 182 VALUES */,
    0x0000002f    /* 183 RET */,
    /* count-pair */0x00000045    /*   0 LREF_PUSH */,
    0x00000049    /*   1 CONSTI_PUSH */,
    0x00000105    /*   2 LREF */,
    0x0000003e    /*   3 PAIRP */,
    0x00000017    /*   4 TEST */,
    SG_WORD(9),
    0x0000015c    /*   6 LREF_CDR_PUSH */,
    0x00000205    /*   7 LREF */,
    0x0000010f    /*   8 ADDI */,
    0x0000000b    /*   9 PUSH */,
    0x00100219    /*  10 SHIFTJ */,
    0x00000018    /*  11 JUMP */,
    SG_WORD(-10),
    0x0000002f    /*  13 RET */,
    0x00000205    /*  14 LREF */,
    0x0000002f    /*  15 RET */,
    /* match-ellipsis? */0x00000005    /*   0 LREF */,
    0x00000021    /*   1 BNNULL */,
    SG_WORD(2),
    0x0000002f    /*   3 RET */,
    0x00000005    /*   4 LREF */,
    0x0000003e    /*   5 PAIRP */,
    0x00000017    /*   6 TEST */,
    SG_WORD(15),
    0x00000030    /*   8 FRAME */,
    SG_WORD(6),
    0x0000005b    /*  10 LREF_CAR_PUSH */,
    0x0000015b    /*  11 LREF_CAR_PUSH */,
    0x00000245    /*  12 LREF_PUSH */,
    0x0000034a    /*  13 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x00000017    /*  15 TEST */,
    SG_WORD(6),
    0x0000005c    /*  17 LREF_CDR_PUSH */,
    0x00000145    /*  18 LREF_PUSH */,
    0x00000245    /*  19 LREF_PUSH */,
    0x0000034b    /*  20 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis?#core.macro> */,
    0x0000002f    /*  22 RET */,
    /* match-ellipsis-n? */0x00000245    /*   0 LREF_PUSH */,
    0x00000004    /*   1 CONSTI */,
    0x0000001a    /*   2 BNNUME */,
    SG_WORD(2),
    0x0000002f    /*   4 RET */,
    0x00000005    /*   5 LREF */,
    0x0000003e    /*   6 PAIRP */,
    0x00000017    /*   7 TEST */,
    SG_WORD(18),
    0x00000030    /*   9 FRAME */,
    SG_WORD(6),
    0x0000005b    /*  11 LREF_CAR_PUSH */,
    0x0000015b    /*  12 LREF_CAR_PUSH */,
    0x00000345    /*  13 LREF_PUSH */,
    0x0000034a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x00000017    /*  16 TEST */,
    SG_WORD(9),
    0x0000005c    /*  18 LREF_CDR_PUSH */,
    0x00000145    /*  19 LREF_PUSH */,
    0x00000205    /*  20 LREF */,
    -0x000000f1   /*  21 ADDI */,
    0x0000000b    /*  22 PUSH */,
    0x00000345    /*  23 LREF_PUSH */,
    0x0000044b    /*  24 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis-n?#core.macro> */,
    0x0000002f    /*  26 RET */,
    /* match-pattern? */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000145    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bar?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000061    /*   7 CONST_RET */,
    SG_WORD(SG_TRUE) /* #t */,
    0x00000030    /*   9 FRAME */,
    SG_WORD(4),
    0x00000145    /*  11 LREF_PUSH */,
    0x0000014a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  14 TEST */,
    SG_WORD(53),
    0x00000030    /*  16 FRAME */,
    SG_WORD(5),
    0x00000145    /*  18 LREF_PUSH */,
    0x00000245    /*  19 LREF_PUSH */,
    0x0000024a    /*  20 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /*  22 TEST */,
    SG_WORD(43),
    0x00000030    /*  24 FRAME */,
    SG_WORD(4),
    0x00000045    /*  26 LREF_PUSH */,
    0x0000014a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  29 TEST */,
    SG_WORD(35),
    0x00000145    /*  31 LREF_PUSH */,
    0x00000045    /*  32 LREF_PUSH */,
    0x00000163    /*  33 RESV_STACK */,
    0x00000009    /*  34 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15859#core.macro> */,
    0x00000531    /*  36 INST_STACK */,
    0x00000030    /*  37 FRAME */,
    SG_WORD(10),
    0x00000345    /*  39 LREF_PUSH */,
    0x00000030    /*  40 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  42 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x0000000b    /*  44 PUSH */,
    0x00000009    /*  45 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15859#core.macro> */,
    0x0000022c    /*  47 LOCAL_CALL */,
    0x0000000b    /*  48 PUSH */,
    0x00000030    /*  49 FRAME */,
    SG_WORD(10),
    0x00000445    /*  51 LREF_PUSH */,
    0x00000030    /*  52 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x0000000b    /*  56 PUSH */,
    0x00000009    /*  57 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15859#core.macro> */,
    0x0000022c    /*  59 LOCAL_CALL */,
    0x0000000b    /*  60 PUSH */,
    0x00000645    /*  61 LREF_PUSH */,
    0x00000745    /*  62 LREF_PUSH */,
    0x0000024b    /*  63 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier free-identifier=?#core.macro> */,
    0x0000002f    /*  65 RET */,
    0x00000061    /*  66 CONST_RET */,
    SG_WORD(SG_TRUE) /* #t */,
    0x00000105    /*  68 LREF */,
    0x0000003e    /*  69 PAIRP */,
    0x00000017    /*  70 TEST */,
    SG_WORD(152),
    0x00000156    /*  72 LREF_CDR */,
    0x0000003e    /*  73 PAIRP */,
    0x00000017    /*  74 TEST */,
    SG_WORD(145),
    0x00000030    /*  76 FRAME */,
    SG_WORD(6),
    0x00000105    /*  78 LREF */,
    0x0000004f    /*  79 CADR */,
    0x0000000b    /*  80 PUSH */,
    0x0000014a    /*  81 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  83 TEST */,
    SG_WORD(84),
    0x00000105    /*  85 LREF */,
    0x00000051    /*  86 CDDR */,
    0x00000021    /*  87 BNNULL */,
    SG_WORD(77),
    0x00000030    /*  89 FRAME */,
    SG_WORD(4),
    0x00000045    /*  91 LREF_PUSH */,
    0x0000014a    /*  92 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list?#core.macro> */,
    0x00000017    /*  94 TEST */,
    SG_WORD(15),
    0x00000030    /*  96 FRAME */,
    SG_WORD(4),
    0x0000015b    /*  98 LREF_CAR_PUSH */,
    0x0000014a    /*  99 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /* 101 TEST */,
    SG_WORD(2),
    0x0000002f    /* 103 RET */,
    0x00000045    /* 104 LREF_PUSH */,
    0x00000145    /* 105 LREF_PUSH */,
    0x00000245    /* 106 LREF_PUSH */,
    0x0000034b    /* 107 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis?#core.macro> */,
    0x0000002f    /* 109 RET */,
    0x00000030    /* 110 FRAME */,
    SG_WORD(4),
    0x00000045    /* 112 LREF_PUSH */,
    0x0000014a    /* 113 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier count-pair#core.macro> */,
    0x0000000b    /* 115 PUSH */,
    0x00000030    /* 116 FRAME */,
    SG_WORD(6),
    0x00000105    /* 118 LREF */,
    0x00000051    /* 119 CDDR */,
    0x0000000b    /* 120 PUSH */,
    0x0000014a    /* 121 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier count-pair#core.macro> */,
    0x00000010    /* 123 SUB */,
    0x0000000b    /* 124 PUSH */,
    0x00000345    /* 125 LREF_PUSH */,
    0x00000004    /* 126 CONSTI */,
    0x0000001a    /* 127 BNNUME */,
    SG_WORD(9),
    0x00000045    /* 129 LREF_PUSH */,
    0x00000105    /* 130 LREF */,
    0x00000051    /* 131 CDDR */,
    0x0000000b    /* 132 PUSH */,
    0x00000245    /* 133 LREF_PUSH */,
    0x0000034b    /* 134 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x0000002f    /* 136 RET */,
    0x00000345    /* 137 LREF_PUSH */,
    0x00000004    /* 138 CONSTI */,
    0x0000001d    /* 139 BNGT */,
    SG_WORD(24),
    0x00000030    /* 141 FRAME */,
    SG_WORD(7),
    0x00000045    /* 143 LREF_PUSH */,
    0x00000145    /* 144 LREF_PUSH */,
    0x00000345    /* 145 LREF_PUSH */,
    0x00000245    /* 146 LREF_PUSH */,
    0x0000044a    /* 147 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis-n?#core.macro> */,
    0x00000017    /* 149 TEST */,
    SG_WORD(14),
    0x00000030    /* 151 FRAME */,
    SG_WORD(5),
    0x00000045    /* 153 LREF_PUSH */,
    0x00000345    /* 154 LREF_PUSH */,
    0x0000024a    /* 155 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list-tail#core.macro> */,
    0x0000000b    /* 157 PUSH */,
    0x00000105    /* 158 LREF */,
    0x00000051    /* 159 CDDR */,
    0x0000000b    /* 160 PUSH */,
    0x00000245    /* 161 LREF_PUSH */,
    0x0000034b    /* 162 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x0000002f    /* 164 RET */,
    0x00000018    /* 165 JUMP */,
    SG_WORD(-56),
    0x0000002f    /* 167 RET */,
    0x00000105    /* 168 LREF */,
    0x0000003e    /* 169 PAIRP */,
    0x00000017    /* 170 TEST */,
    SG_WORD(20),
    0x00000005    /* 172 LREF */,
    0x0000003e    /* 173 PAIRP */,
    0x00000017    /* 174 TEST */,
    SG_WORD(15),
    0x00000030    /* 176 FRAME */,
    SG_WORD(6),
    0x0000005b    /* 178 LREF_CAR_PUSH */,
    0x0000015b    /* 179 LREF_CAR_PUSH */,
    0x00000245    /* 180 LREF_PUSH */,
    0x0000034a    /* 181 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x00000017    /* 183 TEST */,
    SG_WORD(6),
    0x0000005c    /* 185 LREF_CDR_PUSH */,
    0x0000015c    /* 186 LREF_CDR_PUSH */,
    0x00000245    /* 187 LREF_PUSH */,
    0x0000034b    /* 188 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x0000002f    /* 190 RET */,
    0x00000105    /* 191 LREF */,
    0x00000041    /* 192 VECTORP */,
    0x00000017    /* 193 TEST */,
    SG_WORD(21),
    0x00000005    /* 195 LREF */,
    0x00000041    /* 196 VECTORP */,
    0x00000017    /* 197 TEST */,
    SG_WORD(16),
    0x00000030    /* 199 FRAME */,
    SG_WORD(4),
    0x00000045    /* 201 LREF_PUSH */,
    0x0000014a    /* 202 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 204 PUSH */,
    0x00000030    /* 205 FRAME */,
    SG_WORD(4),
    0x00000145    /* 207 LREF_PUSH */,
    0x0000014a    /* 208 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 210 PUSH */,
    0x00000245    /* 211 LREF_PUSH */,
    0x0000034b    /* 212 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x0000002f    /* 214 RET */,
    0x00000145    /* 215 LREF_PUSH */,
    0x00000045    /* 216 LREF_PUSH */,
    0x0000024b    /* 217 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier equal?#core.macro> */,
    0x0000002f    /* 219 RET */,
    0x00000018    /* 220 JUMP */,
    SG_WORD(-53),
    0x0000002f    /* 222 RET */,
    0x00000018    /* 223 JUMP */,
    SG_WORD(-56),
    0x0000002f    /* 225 RET */,
    /* union-vars */0x00000105    /*   0 LREF */,
    0x00000021    /*   1 BNNULL */,
    SG_WORD(3),
    0x00000005    /*   3 LREF */,
    0x0000002f    /*   4 RET */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(15),
    0x00000105    /*   7 LREF */,
    0x0000004e    /*   8 CAAR */,
    0x0000000b    /*   9 PUSH */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(6),
    0x00000105    /*  12 LREF */,
    0x00000050    /*  13 CDAR */,
    0x0000000b    /*  14 PUSH */,
    0x0000014a    /*  15 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier reverse#core.macro> */,
    0x0000000b    /*  17 PUSH */,
    0x00000045    /*  18 LREF_PUSH */,
    0x0000034a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x0000000b    /*  21 PUSH */,
    0x0000015c    /*  22 LREF_CDR_PUSH */,
    0x0000024b    /*  23 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier union-vars#core.macro> */,
    0x0000002f    /*  25 RET */,
    /* bind-var! */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bar?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000205    /*   7 LREF */,
    0x0000002f    /*   8 RET */,
    0x00000030    /*   9 FRAME */,
    SG_WORD(5),
    0x00000045    /*  11 LREF_PUSH */,
    0x00000245    /*  12 LREF_PUSH */,
    0x0000024a    /*  13 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x0000000b    /*  15 PUSH */,
    0x00000305    /*  16 LREF */,
    0x00000017    /*  17 TEST */,
    SG_WORD(8),
    0x00000345    /*  19 LREF_PUSH */,
    0x00000145    /*  20 LREF_PUSH */,
    0x00000356    /*  21 LREF_CDR */,
    0x00000037    /*  22 CONS */,
    0x0000004d    /*  23 SET_CDR */,
    0x00000205    /*  24 LREF */,
    0x0000002f    /*  25 RET */,
    0x00000045    /*  26 LREF_PUSH */,
    0x00000105    /*  27 LREF */,
    0x00000138    /*  28 LIST */,
    0x00000054    /*  29 CONS_PUSH */,
    0x00000205    /*  30 LREF */,
    0x00000037    /*  31 CONS */,
    0x0000002f    /*  32 RET */,
    /* bind-null-ellipsis */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x0000005b    /*   2 LREF_CAR_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x0000000b    /*   5 PUSH */,
    0x00000245    /*   6 LREF_PUSH */,
    0x00000305    /*   7 LREF */,
    0x00000021    /*   8 BNNULL */,
    SG_WORD(3),
    0x00000405    /*  10 LREF */,
    0x0000002f    /*  11 RET */,
    0x0000035c    /*  12 LREF_CDR_PUSH */,
    0x00000030    /*  13 FRAME */,
    SG_WORD(5),
    0x0000035b    /*  15 LREF_CAR_PUSH */,
    0x00000145    /*  16 LREF_PUSH */,
    0x0000024a    /*  17 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier memq#core.macro> */,
    0x00000017    /*  19 TEST */,
    SG_WORD(4),
    0x00000405    /*  21 LREF */,
    0x00000018    /*  22 JUMP */,
    SG_WORD(9),
    0x00000030    /*  24 FRAME */,
    SG_WORD(7),
    0x0000035b    /*  26 LREF_CAR_PUSH */,
    0x00000048    /*  27 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000445    /*  29 LREF_PUSH */,
    0x0000034a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x0000000b    /*  32 PUSH */,
    0x00300219    /*  33 SHIFTJ */,
    0x00000018    /*  34 JUMP */,
    SG_WORD(-28),
    0x0000002f    /*  36 RET */,
    /* bind-ellipsis */0x00000005    /*   0 LREF */,
    0x00000021    /*   1 BNNULL */,
    SG_WORD(15),
    0x00000405    /*   3 LREF */,
    0x00000021    /*   4 BNNULL */,
    SG_WORD(7),
    0x00000145    /*   6 LREF_PUSH */,
    0x00000245    /*   7 LREF_PUSH */,
    0x00000345    /*   8 LREF_PUSH */,
    0x0000034b    /*   9 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-null-ellipsis#core.macro> */,
    0x0000002f    /*  11 RET */,
    0x00000345    /*  12 LREF_PUSH */,
    0x00000445    /*  13 LREF_PUSH */,
    0x0000024b    /*  14 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier union-vars#core.macro> */,
    0x0000002f    /*  16 RET */,
    0x0000005c    /*  17 LREF_CDR_PUSH */,
    0x00000145    /*  18 LREF_PUSH */,
    0x00000245    /*  19 LREF_PUSH */,
    0x00000345    /*  20 LREF_PUSH */,
    0x00000030    /*  21 FRAME */,
    SG_WORD(7),
    0x0000005b    /*  23 LREF_CAR_PUSH */,
    0x0000015b    /*  24 LREF_CAR_PUSH */,
    0x00000245    /*  25 LREF_PUSH */,
    0x00000445    /*  26 LREF_PUSH */,
    0x0000044a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000000b    /*  29 PUSH */,
    0x0000054b    /*  30 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis#core.macro> */,
    0x0000002f    /*  32 RET */,
    /* bind-ellipsis-n */0x00000345    /*   0 LREF_PUSH */,
    0x00000004    /*   1 CONSTI */,
    0x0000001a    /*   2 BNNUME */,
    SG_WORD(15),
    0x00000505    /*   4 LREF */,
    0x00000021    /*   5 BNNULL */,
    SG_WORD(7),
    0x00000145    /*   7 LREF_PUSH */,
    0x00000245    /*   8 LREF_PUSH */,
    0x00000445    /*   9 LREF_PUSH */,
    0x0000034b    /*  10 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-null-ellipsis#core.macro> */,
    0x0000002f    /*  12 RET */,
    0x00000445    /*  13 LREF_PUSH */,
    0x00000545    /*  14 LREF_PUSH */,
    0x0000024b    /*  15 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier union-vars#core.macro> */,
    0x0000002f    /*  17 RET */,
    0x0000005c    /*  18 LREF_CDR_PUSH */,
    0x00000145    /*  19 LREF_PUSH */,
    0x00000245    /*  20 LREF_PUSH */,
    0x00000305    /*  21 LREF */,
    -0x000000f1   /*  22 ADDI */,
    0x0000000b    /*  23 PUSH */,
    0x00000445    /*  24 LREF_PUSH */,
    0x00000030    /*  25 FRAME */,
    SG_WORD(7),
    0x0000005b    /*  27 LREF_CAR_PUSH */,
    0x0000015b    /*  28 LREF_CAR_PUSH */,
    0x00000245    /*  29 LREF_PUSH */,
    0x00000545    /*  30 LREF_PUSH */,
    0x0000044a    /*  31 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000000b    /*  33 PUSH */,
    0x0000064b    /*  34 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis-n#core.macro> */,
    0x0000002f    /*  36 RET */,
    /* bind-pattern */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000145    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(17),
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000145    /*   9 LREF_PUSH */,
    0x00000245    /*  10 LREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000017    /*  13 TEST */,
    SG_WORD(3),
    0x00000305    /*  15 LREF */,
    0x0000002f    /*  16 RET */,
    0x00000145    /*  17 LREF_PUSH */,
    0x00000045    /*  18 LREF_PUSH */,
    0x00000345    /*  19 LREF_PUSH */,
    0x0000034b    /*  20 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x0000002f    /*  22 RET */,
    0x00000105    /*  23 LREF */,
    0x0000003e    /*  24 PAIRP */,
    0x00000017    /*  25 TEST */,
    SG_WORD(161),
    0x00000156    /*  27 LREF_CDR */,
    0x0000003e    /*  28 PAIRP */,
    0x00000017    /*  29 TEST */,
    SG_WORD(154),
    0x00000030    /*  31 FRAME */,
    SG_WORD(6),
    0x00000105    /*  33 LREF */,
    0x0000004f    /*  34 CADR */,
    0x0000000b    /*  35 PUSH */,
    0x0000014a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  38 TEST */,
    SG_WORD(103),
    0x00000105    /*  40 LREF */,
    0x00000051    /*  41 CDDR */,
    0x00000021    /*  42 BNNULL */,
    SG_WORD(96),
    0x00000030    /*  44 FRAME */,
    SG_WORD(4),
    0x00000045    /*  46 LREF_PUSH */,
    0x0000014a    /*  47 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list?#core.macro> */,
    0x00000017    /*  49 TEST */,
    SG_WORD(23),
    0x00000030    /*  51 FRAME */,
    SG_WORD(4),
    0x0000015b    /*  53 LREF_CAR_PUSH */,
    0x0000014a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  56 TEST */,
    SG_WORD(7),
    0x0000015b    /*  58 LREF_CAR_PUSH */,
    0x00000045    /*  59 LREF_PUSH */,
    0x00000345    /*  60 LREF_PUSH */,
    0x0000034b    /*  61 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x0000002f    /*  63 RET */,
    0x00000045    /*  64 LREF_PUSH */,
    0x00000145    /*  65 LREF_PUSH */,
    0x00000245    /*  66 LREF_PUSH */,
    0x00000345    /*  67 LREF_PUSH */,
    0x00000048    /*  68 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000054b    /*  70 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis#core.macro> */,
    0x0000002f    /*  72 RET */,
    0x00000030    /*  73 FRAME */,
    SG_WORD(4),
    0x00000045    /*  75 LREF_PUSH */,
    0x0000014a    /*  76 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier count-pair#core.macro> */,
    0x0000000b    /*  78 PUSH */,
    0x00000030    /*  79 FRAME */,
    SG_WORD(6),
    0x00000105    /*  81 LREF */,
    0x00000051    /*  82 CDDR */,
    0x0000000b    /*  83 PUSH */,
    0x0000014a    /*  84 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier count-pair#core.macro> */,
    0x00000010    /*  86 SUB */,
    0x0000000b    /*  87 PUSH */,
    0x00000030    /*  88 FRAME */,
    SG_WORD(5),
    0x00000045    /*  90 LREF_PUSH */,
    0x00000445    /*  91 LREF_PUSH */,
    0x0000024a    /*  92 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list-tail#core.macro> */,
    0x0000000b    /*  94 PUSH */,
    0x00000105    /*  95 LREF */,
    0x00000051    /*  96 CDDR */,
    0x0000000b    /*  97 PUSH */,
    0x00000245    /*  98 LREF_PUSH */,
    0x00000445    /*  99 LREF_PUSH */,
    0x00000004    /* 100 CONSTI */,
    0x0000001a    /* 101 BNNUME */,
    SG_WORD(31),
    0x00000030    /* 103 FRAME */,
    SG_WORD(4),
    0x0000015b    /* 105 LREF_CAR_PUSH */,
    0x0000014a    /* 106 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /* 108 TEST */,
    SG_WORD(11),
    0x00000030    /* 110 FRAME */,
    SG_WORD(7),
    0x0000015b    /* 112 LREF_CAR_PUSH */,
    0x00000048    /* 113 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000345    /* 115 LREF_PUSH */,
    0x0000034a    /* 116 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x00000018    /* 118 JUMP */,
    SG_WORD(12),
    0x00000030    /* 120 FRAME */,
    SG_WORD(10),
    0x00000045    /* 122 LREF_PUSH */,
    0x00000145    /* 123 LREF_PUSH */,
    0x00000245    /* 124 LREF_PUSH */,
    0x00000445    /* 125 LREF_PUSH */,
    0x00000345    /* 126 LREF_PUSH */,
    0x00000048    /* 127 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000064a    /* 129 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis-n#core.macro> */,
    0x00000018    /* 131 JUMP */,
    SG_WORD(3),
    0x00000018    /* 133 JUMP */,
    SG_WORD(-14),
    0x0000000b    /* 135 PUSH */,
    0x0000044b    /* 136 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000002f    /* 138 RET */,
    0x00000018    /* 139 JUMP */,
    SG_WORD(-67),
    0x0000002f    /* 141 RET */,
    0x00000105    /* 142 LREF */,
    0x0000003e    /* 143 PAIRP */,
    0x00000017    /* 144 TEST */,
    SG_WORD(16),
    0x0000005c    /* 146 LREF_CDR_PUSH */,
    0x0000015c    /* 147 LREF_CDR_PUSH */,
    0x00000245    /* 148 LREF_PUSH */,
    0x00000030    /* 149 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 151 LREF_CAR_PUSH */,
    0x0000015b    /* 152 LREF_CAR_PUSH */,
    0x00000245    /* 153 LREF_PUSH */,
    0x00000345    /* 154 LREF_PUSH */,
    0x0000044a    /* 155 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000000b    /* 157 PUSH */,
    0x0000044b    /* 158 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000002f    /* 160 RET */,
    0x00000105    /* 161 LREF */,
    0x00000041    /* 162 VECTORP */,
    0x00000017    /* 163 TEST */,
    SG_WORD(18),
    0x00000030    /* 165 FRAME */,
    SG_WORD(4),
    0x00000045    /* 167 LREF_PUSH */,
    0x0000014a    /* 168 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 170 PUSH */,
    0x00000030    /* 171 FRAME */,
    SG_WORD(4),
    0x00000145    /* 173 LREF_PUSH */,
    0x0000014a    /* 174 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 176 PUSH */,
    0x00000245    /* 177 LREF_PUSH */,
    0x00000345    /* 178 LREF_PUSH */,
    0x0000044b    /* 179 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000002f    /* 181 RET */,
    0x00000305    /* 182 LREF */,
    0x0000002f    /* 183 RET */,
    0x00000018    /* 184 JUMP */,
    SG_WORD(-43),
    0x0000002f    /* 186 RET */,
    0x00000018    /* 187 JUMP */,
    SG_WORD(-46),
    0x0000002f    /* 189 RET */,
    /* match-syntax-case */0x00000345    /*   0 LREF_PUSH */,
    0x00000405    /*   1 LREF */,
    0x00000021    /*   2 BNNULL */,
    SG_WORD(18),
    0x00000205    /*   4 LREF */,
    0x0000003e    /*   5 PAIRP */,
    0x00000017    /*   6 TEST */,
    SG_WORD(2),
    0x00000255    /*   8 LREF_CAR */,
    0x0000000b    /*   9 PUSH */,
    0x00000048    /*  10 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid syntax */,
    0x00000030    /*  12 FRAME */,
    SG_WORD(4),
    0x00000245    /*  14 LREF_PUSH */,
    0x0000014a    /*  15 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  17 PUSH */,
    0x0000034b    /*  18 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  20 RET */,
    0x0000045b    /*  21 LREF_CAR_PUSH */,
    0x0000055b    /*  22 LREF_CAR_PUSH */,
    0x00000505    /*  23 LREF */,
    0x0000004f    /*  24 CADR */,
    0x0000000b    /*  25 PUSH */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(4),
    0x00000545    /*  28 LREF_PUSH */,
    0x0000014a    /*  29 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  31 PUSH */,
    0x00000030    /*  32 FRAME */,
    SG_WORD(6),
    0x00000245    /*  34 LREF_PUSH */,
    0x00000645    /*  35 LREF_PUSH */,
    0x00000145    /*  36 LREF_PUSH */,
    0x0000034a    /*  37 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x00000017    /*  39 TEST */,
    SG_WORD(10),
    0x00000030    /*  41 FRAME */,
    SG_WORD(8),
    0x00000245    /*  43 LREF_PUSH */,
    0x00000645    /*  44 LREF_PUSH */,
    0x00000145    /*  45 LREF_PUSH */,
    0x00000048    /*  46 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000044a    /*  48 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x0000000b    /*  50 PUSH */,
    0x00000905    /*  51 LREF */,
    0x00000017    /*  52 TEST */,
    SG_WORD(29),
    0x00000705    /*  54 LREF */,
    0x00000017    /*  55 TEST */,
    SG_WORD(23),
    0x00000030    /*  57 FRAME */,
    SG_WORD(7),
    0x00000745    /*  59 LREF_PUSH */,
    0x00000945    /*  60 LREF_PUSH */,
    0x00000005    /*  61 LREF */,
    0x00000239    /*  62 APPEND */,
    0x00000138    /*  63 LIST */,
    0x0000022a    /*  64 APPLY */,
    0x00000017    /*  65 TEST */,
    SG_WORD(8),
    0x00000845    /*  67 LREF_PUSH */,
    0x00000945    /*  68 LREF_PUSH */,
    0x00000005    /*  69 LREF */,
    0x00000239    /*  70 APPEND */,
    0x00000138    /*  71 LIST */,
    0x0010022a    /*  72 APPLY */,
    0x0000002f    /*  73 RET */,
    0x0000045c    /*  74 LREF_CDR_PUSH */,
    0x00400119    /*  75 SHIFTJ */,
    0x00000018    /*  76 JUMP */,
    SG_WORD(-76),
    0x0000002f    /*  78 RET */,
    0x00000018    /*  79 JUMP */,
    SG_WORD(-13),
    0x0000002f    /*  81 RET */,
    0x00000018    /*  82 JUMP */,
    SG_WORD(-9),
    0x0000002f    /*  84 RET */,
    /* loop */0x00000005    /*   0 LREF */,
    0x00000021    /*   1 BNNULL */,
    SG_WORD(3),
    0x00000005    /*   3 LREF */,
    0x0000002f    /*   4 RET */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(5),
    0x0000005b    /*   7 LREF_CAR_PUSH */,
    0x00000146    /*   8 FREF_PUSH */,
    0x0000024a    /*   9 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x00000017    /*  11 TEST */,
    SG_WORD(5),
    0x0000005c    /*  13 LREF_CDR_PUSH */,
    0x00000007    /*  14 FREF */,
    0x0000012e    /*  15 LOCAL_TAIL_CALL */,
    0x0000002f    /*  16 RET */,
    0x0000005b    /*  17 LREF_CAR_PUSH */,
    0x00000030    /*  18 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  20 LREF_CDR_PUSH */,
    0x00000007    /*  21 FREF */,
    0x0000012c    /*  22 LOCAL_CALL */,
    0x00000037    /*  23 CONS */,
    0x0000002f    /*  24 RET */,
    /* collect-rename-ids */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x0000000b    /*   5 PUSH */,
    0x00000163    /*   6 RESV_STACK */,
    0x00000145    /*   7 LREF_PUSH */,
    0x00000345    /*   8 LREF_PUSH */,
    0x00000129    /*   9 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[58])) /* #<code-builder loop (1 0 2)> */,
    0x00000331    /*  11 INST_STACK */,
    0x00000245    /*  12 LREF_PUSH */,
    0x00000305    /*  13 LREF */,
    0x0000012e    /*  14 LOCAL_TAIL_CALL */,
    0x0000002f    /*  15 RET */,
    /* parse-ellipsis-splicing */0x00000249    /*   0 CONSTI_PUSH */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(4),
    0x00000045    /*   3 LREF_PUSH */,
    0x0000014a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier cdddr#core.macro> */,
    0x0000000b    /*   6 PUSH */,
    0x00000205    /*   7 LREF */,
    0x0000003e    /*   8 PAIRP */,
    0x00000017    /*   9 TEST */,
    SG_WORD(27),
    0x00000030    /*  11 FRAME */,
    SG_WORD(4),
    0x0000025b    /*  13 LREF_CAR_PUSH */,
    0x0000014a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  16 TEST */,
    SG_WORD(9),
    0x00000105    /*  18 LREF */,
    0x0000010f    /*  19 ADDI */,
    0x0000000b    /*  20 PUSH */,
    0x0000025c    /*  21 LREF_CDR_PUSH */,
    0x00100219    /*  22 SHIFTJ */,
    0x00000018    /*  23 JUMP */,
    SG_WORD(-17),
    0x0000002f    /*  25 RET */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(5),
    0x00000045    /*  28 LREF_PUSH */,
    0x00000145    /*  29 LREF_PUSH */,
    0x0000024a    /*  30 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list-head#core.macro> */,
    0x0000000b    /*  32 PUSH */,
    0x00000245    /*  33 LREF_PUSH */,
    0x00000105    /*  34 LREF */,
    0x0000033a    /*  35 VALUES */,
    0x0000002f    /*  36 RET */,
    0x00000018    /*  37 JUMP */,
    SG_WORD(-12),
    0x0000002f    /*  39 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(5),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000146    /*   3 FREF_PUSH */,
    0x0000024a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*   6 PUSH */,
    0x00000007    /*   7 FREF */,
    0x00000027    /*   8 NUM_GE */,
    0x0000002f    /*   9 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(11),
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000146    /*  10 FREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  13 PUSH */,
    0x00000105    /*  14 LREF */,
    0x00000027    /*  15 NUM_GE */,
    0x0000002f    /*  16 RET */,
    0x00000005    /*  17 LREF */,
    0x0000003e    /*  18 PAIRP */,
    0x00000017    /*  19 TEST */,
    SG_WORD(193),
    0x00000030    /*  21 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  23 LREF_CAR_PUSH */,
    0x0000014a    /*  24 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  26 TEST */,
    SG_WORD(183),
    0x00000056    /*  28 LREF_CDR */,
    0x0000003e    /*  29 PAIRP */,
    0x00000017    /*  30 TEST */,
    SG_WORD(176),
    0x00000005    /*  32 LREF */,
    0x00000051    /*  33 CDDR */,
    0x00000021    /*  34 BNNULL */,
    SG_WORD(15),
    0x00000146    /*  36 FREF_PUSH */,
    0x00000145    /*  37 LREF_PUSH */,
    0x00000029    /*  38 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[61])) /* #<code-builder #f (1 0 2)> */,
    0x0000000b    /*  40 PUSH */,
    0x00000030    /*  41 FRAME */,
    SG_WORD(4),
    0x00000045    /*  43 LREF_PUSH */,
    0x0000014a    /*  44 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x0000000b    /*  46 PUSH */,
    0x0000024b    /*  47 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier exists#core.macro> */,
    0x0000002f    /*  49 RET */,
    0x00000005    /*  50 LREF */,
    0x0000003e    /*  51 PAIRP */,
    0x00000017    /*  52 TEST */,
    SG_WORD(151),
    0x00000056    /*  54 LREF_CDR */,
    0x0000003e    /*  55 PAIRP */,
    0x00000017    /*  56 TEST */,
    SG_WORD(144),
    0x00000030    /*  58 FRAME */,
    SG_WORD(6),
    0x00000005    /*  60 LREF */,
    0x0000004f    /*  61 CADR */,
    0x0000000b    /*  62 PUSH */,
    0x0000014a    /*  63 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  65 TEST */,
    SG_WORD(132),
    0x00000005    /*  67 LREF */,
    0x00000051    /*  68 CDDR */,
    0x0000003e    /*  69 PAIRP */,
    0x00000017    /*  70 TEST */,
    SG_WORD(124),
    0x00000030    /*  72 FRAME */,
    SG_WORD(9),
    0x00000030    /*  74 FRAME */,
    SG_WORD(4),
    0x00000045    /*  76 LREF_PUSH */,
    0x0000014a    /*  77 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  79 PUSH */,
    0x0000014a    /*  80 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  82 TEST */,
    SG_WORD(31),
    0x00000030    /*  84 FRAME */,
    SG_WORD(4),
    0x00000045    /*  86 LREF_PUSH */,
    0x0000014a    /*  87 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000328    /*  89 RECEIVE */,
    0x00000030    /*  90 FRAME */,
    SG_WORD(7),
    0x00000245    /*  92 LREF_PUSH */,
    0x00000105    /*  93 LREF */,
    0x0000010f    /*  94 ADDI */,
    0x0000000b    /*  95 PUSH */,
    0x00000007    /*  96 FREF */,
    0x0000022c    /*  97 LOCAL_CALL */,
    0x00000017    /*  98 TEST */,
    SG_WORD(2),
    0x0000002f    /* 100 RET */,
    0x00000030    /* 101 FRAME */,
    SG_WORD(5),
    0x00000245    /* 103 LREF_PUSH */,
    0x00000149    /* 104 CONSTI_PUSH */,
    0x00000007    /* 105 FREF */,
    0x0000022c    /* 106 LOCAL_CALL */,
    0x00000017    /* 107 TEST */,
    SG_WORD(5),
    0x00000345    /* 109 LREF_PUSH */,
    0x00000145    /* 110 LREF_PUSH */,
    0x00000007    /* 111 FREF */,
    0x0000022e    /* 112 LOCAL_TAIL_CALL */,
    0x0000002f    /* 113 RET */,
    0x00000005    /* 114 LREF */,
    0x0000003e    /* 115 PAIRP */,
    0x00000017    /* 116 TEST */,
    SG_WORD(75),
    0x00000056    /* 118 LREF_CDR */,
    0x0000003e    /* 119 PAIRP */,
    0x00000017    /* 120 TEST */,
    SG_WORD(68),
    0x00000030    /* 122 FRAME */,
    SG_WORD(6),
    0x00000005    /* 124 LREF */,
    0x0000004f    /* 125 CADR */,
    0x0000000b    /* 126 PUSH */,
    0x0000014a    /* 127 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 129 TEST */,
    SG_WORD(27),
    0x00000030    /* 131 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 133 LREF_CAR_PUSH */,
    0x00000105    /* 134 LREF */,
    0x0000010f    /* 135 ADDI */,
    0x0000000b    /* 136 PUSH */,
    0x00000007    /* 137 FREF */,
    0x0000022c    /* 138 LOCAL_CALL */,
    0x00000017    /* 139 TEST */,
    SG_WORD(2),
    0x0000002f    /* 141 RET */,
    0x00000030    /* 142 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 144 LREF_CAR_PUSH */,
    0x00000149    /* 145 CONSTI_PUSH */,
    0x00000007    /* 146 FREF */,
    0x0000022c    /* 147 LOCAL_CALL */,
    0x00000017    /* 148 TEST */,
    SG_WORD(7),
    0x00000005    /* 150 LREF */,
    0x00000051    /* 151 CDDR */,
    0x0000000b    /* 152 PUSH */,
    0x00000145    /* 153 LREF_PUSH */,
    0x00000007    /* 154 FREF */,
    0x0000022e    /* 155 LOCAL_TAIL_CALL */,
    0x0000002f    /* 156 RET */,
    0x00000005    /* 157 LREF */,
    0x0000003e    /* 158 PAIRP */,
    0x00000017    /* 159 TEST */,
    SG_WORD(15),
    0x00000030    /* 161 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 163 LREF_CAR_PUSH */,
    0x00000145    /* 164 LREF_PUSH */,
    0x00000007    /* 165 FREF */,
    0x0000022c    /* 166 LOCAL_CALL */,
    0x00000017    /* 167 TEST */,
    SG_WORD(2),
    0x0000002f    /* 169 RET */,
    0x0000005c    /* 170 LREF_CDR_PUSH */,
    0x00000145    /* 171 LREF_PUSH */,
    0x00000007    /* 172 FREF */,
    0x0000022e    /* 173 LOCAL_TAIL_CALL */,
    0x0000002f    /* 174 RET */,
    0x00000005    /* 175 LREF */,
    0x00000041    /* 176 VECTORP */,
    0x00000017    /* 177 TEST */,
    SG_WORD(10),
    0x00000030    /* 179 FRAME */,
    SG_WORD(4),
    0x00000045    /* 181 LREF_PUSH */,
    0x0000014a    /* 182 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 184 PUSH */,
    0x00000145    /* 185 LREF_PUSH */,
    0x00000007    /* 186 FREF */,
    0x0000022e    /* 187 LOCAL_TAIL_CALL */,
    0x0000002f    /* 188 RET */,
    0x00000018    /* 189 JUMP */,
    SG_WORD(-33),
    0x0000002f    /* 191 RET */,
    0x00000018    /* 192 JUMP */,
    SG_WORD(-36),
    0x0000002f    /* 194 RET */,
    0x00000018    /* 195 JUMP */,
    SG_WORD(-82),
    0x0000002f    /* 197 RET */,
    0x00000018    /* 198 JUMP */,
    SG_WORD(-85),
    0x0000002f    /* 200 RET */,
    0x00000018    /* 201 JUMP */,
    SG_WORD(-88),
    0x0000002f    /* 203 RET */,
    0x00000018    /* 204 JUMP */,
    SG_WORD(-91),
    0x0000002f    /* 206 RET */,
    0x00000018    /* 207 JUMP */,
    SG_WORD(-158),
    0x0000002f    /* 209 RET */,
    0x00000018    /* 210 JUMP */,
    SG_WORD(-161),
    0x0000002f    /* 212 RET */,
    0x00000018    /* 213 JUMP */,
    SG_WORD(-164),
    0x0000002f    /* 215 RET */,
    /* control-patvar-exists? */0x00000163    /*   0 RESV_STACK */,
    0x00000046    /*   1 FREF_PUSH */,
    0x00000245    /*   2 LREF_PUSH */,
    0x00000129    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[62])) /* #<code-builder loop (2 0 2)> */,
    0x00000231    /*   5 INST_STACK */,
    0x00000045    /*   6 LREF_PUSH */,
    0x00000145    /*   7 LREF_PUSH */,
    0x00000205    /*   8 LREF */,
    0x0000022e    /*   9 LOCAL_TAIL_CALL */,
    0x0000002f    /*  10 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(35),
    0x00000030    /*   7 FRAME */,
    SG_WORD(12),
    0x00000049    /*   9 CONSTI_PUSH */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(5),
    0x00000045    /*  12 LREF_PUSH */,
    0x00000346    /*  13 FREF_PUSH */,
    0x0000024a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  16 PUSH */,
    0x00000246    /*  17 FREF_PUSH */,
    0x0000034a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier <#core.macro> */,
    0x00000017    /*  20 TEST */,
    SG_WORD(19),
    0x00000048    /*  22 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /*  24 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* too few ellipsis following subtemplate */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(4),
    0x00000146    /*  28 FREF_PUSH */,
    0x0000014a    /*  29 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  31 PUSH */,
    0x00000030    /*  32 FRAME */,
    SG_WORD(4),
    0x00000045    /*  34 LREF_PUSH */,
    0x0000014a    /*  35 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  37 PUSH */,
    0x0000044b    /*  38 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  40 RET */,
    0x00000005    /*  41 LREF */,
    0x0000003e    /*  42 PAIRP */,
    0x00000017    /*  43 TEST */,
    SG_WORD(10),
    0x00000030    /*  45 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  47 LREF_CAR_PUSH */,
    0x00000007    /*  48 FREF */,
    0x0000012c    /*  49 LOCAL_CALL */,
    0x0000005c    /*  50 LREF_CDR_PUSH */,
    0x00000007    /*  51 FREF */,
    0x0000012e    /*  52 LOCAL_TAIL_CALL */,
    0x0000002f    /*  53 RET */,
    0x00000005    /*  54 LREF */,
    0x00000041    /*  55 VECTORP */,
    0x00000017    /*  56 TEST */,
    SG_WORD(10),
    0x00000030    /*  58 FRAME */,
    SG_WORD(4),
    0x00000045    /*  60 LREF_PUSH */,
    0x0000014a    /*  61 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  63 PUSH */,
    0x00000007    /*  64 FREF */,
    0x0000012e    /*  65 LOCAL_TAIL_CALL */,
    0x0000002f    /*  66 RET */,
    0x00000002    /*  67 UNDEF */,
    0x0000002f    /*  68 RET */,
    /* check-escaped */0x00000163    /*   0 RESV_STACK */,
    0x00000146    /*   1 FREF_PUSH */,
    0x00000145    /*   2 LREF_PUSH */,
    0x00000046    /*   3 FREF_PUSH */,
    0x00000245    /*   4 LREF_PUSH */,
    0x00000129    /*   5 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[64])) /* #<code-builder loop (1 0 4)> */,
    0x00000231    /*   7 INST_STACK */,
    0x00000045    /*   8 LREF_PUSH */,
    0x00000205    /*   9 LREF */,
    0x0000012e    /*  10 LOCAL_TAIL_CALL */,
    0x0000002f    /*  11 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(46),
    0x00000030    /*   7 FRAME */,
    SG_WORD(4),
    0x00000045    /*   9 LREF_PUSH */,
    0x0000014a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  12 TEST */,
    SG_WORD(10),
    0x00000030    /*  14 FRAME */,
    SG_WORD(8),
    0x00000048    /*  16 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /*  18 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* misplaced ellipsis */,
    0x00000446    /*  20 FREF_PUSH */,
    0x0000034a    /*  21 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /*  23 FRAME */,
    SG_WORD(5),
    0x00000045    /*  25 LREF_PUSH */,
    0x00000346    /*  26 FREF_PUSH */,
    0x0000024a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  29 PUSH */,
    0x00000105    /*  30 LREF */,
    0x0000001d    /*  31 BNGT */,
    SG_WORD(19),
    0x00000048    /*  33 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /*  35 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* too few ellipsis following subtemplate */,
    0x00000030    /*  37 FRAME */,
    SG_WORD(4),
    0x00000446    /*  39 FREF_PUSH */,
    0x0000014a    /*  40 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  42 PUSH */,
    0x00000030    /*  43 FRAME */,
    SG_WORD(4),
    0x00000045    /*  45 LREF_PUSH */,
    0x0000014a    /*  46 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  48 PUSH */,
    0x0000044b    /*  49 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  51 RET */,
    0x00000005    /*  52 LREF */,
    0x0000003e    /*  53 PAIRP */,
    0x00000017    /*  54 TEST */,
    SG_WORD(315),
    0x00000030    /*  56 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  58 LREF_CAR_PUSH */,
    0x0000014a    /*  59 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  61 TEST */,
    SG_WORD(305),
    0x00000056    /*  63 LREF_CDR */,
    0x0000003e    /*  64 PAIRP */,
    0x00000017    /*  65 TEST */,
    SG_WORD(298),
    0x00000005    /*  67 LREF */,
    0x00000051    /*  68 CDDR */,
    0x00000021    /*  69 BNNULL */,
    SG_WORD(8),
    0x00000005    /*  71 LREF */,
    0x0000004f    /*  72 CADR */,
    0x0000000b    /*  73 PUSH */,
    0x00000145    /*  74 LREF_PUSH */,
    0x00000207    /*  75 FREF */,
    0x0000022e    /*  76 LOCAL_TAIL_CALL */,
    0x0000002f    /*  77 RET */,
    0x00000005    /*  78 LREF */,
    0x0000003e    /*  79 PAIRP */,
    0x00000017    /*  80 TEST */,
    SG_WORD(280),
    0x00000056    /*  82 LREF_CDR */,
    0x0000003e    /*  83 PAIRP */,
    0x00000017    /*  84 TEST */,
    SG_WORD(273),
    0x00000030    /*  86 FRAME */,
    SG_WORD(6),
    0x00000005    /*  88 LREF */,
    0x0000004f    /*  89 CADR */,
    0x0000000b    /*  90 PUSH */,
    0x0000014a    /*  91 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  93 TEST */,
    SG_WORD(261),
    0x00000005    /*  95 LREF */,
    0x00000051    /*  96 CDDR */,
    0x0000003e    /*  97 PAIRP */,
    0x00000017    /*  98 TEST */,
    SG_WORD(253),
    0x00000030    /* 100 FRAME */,
    SG_WORD(9),
    0x00000030    /* 102 FRAME */,
    SG_WORD(4),
    0x00000045    /* 104 LREF_PUSH */,
    0x0000014a    /* 105 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /* 107 PUSH */,
    0x0000014a    /* 108 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 110 TEST */,
    SG_WORD(54),
    0x00000030    /* 112 FRAME */,
    SG_WORD(4),
    0x00000045    /* 114 LREF_PUSH */,
    0x0000014a    /* 115 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000328    /* 117 RECEIVE */,
    0x00000145    /* 118 LREF_PUSH */,
    0x00000004    /* 119 CONSTI */,
    0x0000001a    /* 120 BNNUME */,
    SG_WORD(31),
    0x00000030    /* 122 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 124 LREF_CAR_PUSH */,
    0x00000445    /* 125 LREF_PUSH */,
    0x00000107    /* 126 FREF */,
    0x0000022c    /* 127 LOCAL_CALL */,
    0x00000017    /* 128 TEST */,
    SG_WORD(3),
    0x00000018    /* 130 JUMP */,
    SG_WORD(21),
    0x00000030    /* 132 FRAME */,
    SG_WORD(19),
    0x00000048    /* 134 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 136 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* missing pattern variable that used in same level as in pattern */,
    0x00000030    /* 138 FRAME */,
    SG_WORD(4),
    0x00000446    /* 140 FREF_PUSH */,
    0x0000014a    /* 141 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 143 PUSH */,
    0x00000030    /* 144 FRAME */,
    SG_WORD(4),
    0x00000045    /* 146 LREF_PUSH */,
    0x0000014a    /* 147 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 149 PUSH */,
    0x0000044a    /* 150 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /* 152 FRAME */,
    SG_WORD(7),
    0x00000245    /* 154 LREF_PUSH */,
    0x00000105    /* 155 LREF */,
    0x0000010f    /* 156 ADDI */,
    0x0000000b    /* 157 PUSH */,
    0x00000007    /* 158 FREF */,
    0x0000022c    /* 159 LOCAL_CALL */,
    0x00000345    /* 160 LREF_PUSH */,
    0x00000145    /* 161 LREF_PUSH */,
    0x00000007    /* 162 FREF */,
    0x0000022e    /* 163 LOCAL_TAIL_CALL */,
    0x0000002f    /* 164 RET */,
    0x00000005    /* 165 LREF */,
    0x0000003e    /* 166 PAIRP */,
    0x00000017    /* 167 TEST */,
    SG_WORD(181),
    0x00000056    /* 169 LREF_CDR */,
    0x0000003e    /* 170 PAIRP */,
    0x00000017    /* 171 TEST */,
    SG_WORD(174),
    0x00000030    /* 173 FRAME */,
    SG_WORD(6),
    0x00000005    /* 175 LREF */,
    0x0000004f    /* 176 CADR */,
    0x0000000b    /* 177 PUSH */,
    0x0000014a    /* 178 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 180 TEST */,
    SG_WORD(134),
    0x00000030    /* 182 FRAME */,
    SG_WORD(4),
    0x0000005b    /* 184 LREF_CAR_PUSH */,
    0x0000014a    /* 185 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /* 187 TEST */,
    SG_WORD(62),
    0x00000030    /* 189 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 191 LREF_CAR_PUSH */,
    0x00000346    /* 192 FREF_PUSH */,
    0x0000024a    /* 193 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /* 195 PUSH */,
    0x00000245    /* 196 LREF_PUSH */,
    0x00000004    /* 197 CONSTI */,
    0x0000001b    /* 198 BNLT */,
    SG_WORD(20),
    0x00000048    /* 200 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 202 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* misplaced ellipsis following literal */,
    0x00000030    /* 204 FRAME */,
    SG_WORD(4),
    0x00000446    /* 206 FREF_PUSH */,
    0x0000014a    /* 207 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 209 PUSH */,
    0x00000030    /* 210 FRAME */,
    SG_WORD(4),
    0x0000005b    /* 212 LREF_CAR_PUSH */,
    0x0000014a    /* 213 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 215 PUSH */,
    0x0000044b    /* 216 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /* 218 RET */,
    0x00000245    /* 219 LREF_PUSH */,
    0x00000105    /* 220 LREF */,
    0x0000010f    /* 221 ADDI */,
    0x0000001d    /* 222 BNGT */,
    SG_WORD(20),
    0x00000048    /* 224 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 226 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* too few ellipsis following subtemplate */,
    0x00000030    /* 228 FRAME */,
    SG_WORD(4),
    0x00000446    /* 230 FREF_PUSH */,
    0x0000014a    /* 231 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 233 PUSH */,
    0x00000030    /* 234 FRAME */,
    SG_WORD(4),
    0x0000005b    /* 236 LREF_CAR_PUSH */,
    0x0000014a    /* 237 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /* 239 PUSH */,
    0x0000044b    /* 240 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /* 242 RET */,
    0x00000005    /* 243 LREF */,
    0x00000051    /* 244 CDDR */,
    0x0000000b    /* 245 PUSH */,
    0x00000145    /* 246 LREF_PUSH */,
    0x00000007    /* 247 FREF */,
    0x0000022e    /* 248 LOCAL_TAIL_CALL */,
    0x0000002f    /* 249 RET */,
    0x00000055    /* 250 LREF_CAR */,
    0x0000003e    /* 251 PAIRP */,
    0x00000017    /* 252 TEST */,
    SG_WORD(42),
    0x00000145    /* 254 LREF_PUSH */,
    0x00000004    /* 255 CONSTI */,
    0x0000001a    /* 256 BNNUME */,
    SG_WORD(23),
    0x00000030    /* 258 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 260 LREF_CAR_PUSH */,
    0x00000105    /* 261 LREF */,
    0x0000010f    /* 262 ADDI */,
    0x0000000b    /* 263 PUSH */,
    0x00000107    /* 264 FREF */,
    0x0000022c    /* 265 LOCAL_CALL */,
    0x00000017    /* 266 TEST */,
    SG_WORD(3),
    0x00000018    /* 268 JUMP */,
    SG_WORD(11),
    0x00000030    /* 270 FRAME */,
    SG_WORD(9),
    0x00000048    /* 272 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 274 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* missing pattern variable that used in same level as in pattern */,
    0x00000446    /* 276 FREF_PUSH */,
    0x0000005b    /* 277 LREF_CAR_PUSH */,
    0x0000044a    /* 278 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000030    /* 280 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 282 LREF_CAR_PUSH */,
    0x00000105    /* 283 LREF */,
    0x0000010f    /* 284 ADDI */,
    0x0000000b    /* 285 PUSH */,
    0x00000007    /* 286 FREF */,
    0x0000022c    /* 287 LOCAL_CALL */,
    0x00000005    /* 288 LREF */,
    0x00000051    /* 289 CDDR */,
    0x0000000b    /* 290 PUSH */,
    0x00000145    /* 291 LREF_PUSH */,
    0x00000007    /* 292 FREF */,
    0x0000022e    /* 293 LOCAL_TAIL_CALL */,
    0x0000002f    /* 294 RET */,
    0x00000055    /* 295 LREF_CAR */,
    0x00000021    /* 296 BNNULL */,
    SG_WORD(9),
    0x00000048    /* 298 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 300 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* misplaced ellipsis following empty list */,
    0x00000446    /* 302 FREF_PUSH */,
    0x0000034b    /* 303 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /* 305 RET */,
    0x00000048    /* 306 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 308 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* misplaced ellipsis following literal */,
    0x00000446    /* 310 FREF_PUSH */,
    0x0000005b    /* 311 LREF_CAR_PUSH */,
    0x0000044b    /* 312 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /* 314 RET */,
    0x00000005    /* 315 LREF */,
    0x0000003e    /* 316 PAIRP */,
    0x00000017    /* 317 TEST */,
    SG_WORD(12),
    0x00000030    /* 319 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 321 LREF_CAR_PUSH */,
    0x00000145    /* 322 LREF_PUSH */,
    0x00000007    /* 323 FREF */,
    0x0000022c    /* 324 LOCAL_CALL */,
    0x0000005c    /* 325 LREF_CDR_PUSH */,
    0x00000145    /* 326 LREF_PUSH */,
    0x00000007    /* 327 FREF */,
    0x0000022e    /* 328 LOCAL_TAIL_CALL */,
    0x0000002f    /* 329 RET */,
    0x00000005    /* 330 LREF */,
    0x00000041    /* 331 VECTORP */,
    0x00000017    /* 332 TEST */,
    SG_WORD(11),
    0x00000030    /* 334 FRAME */,
    SG_WORD(4),
    0x00000045    /* 336 LREF_PUSH */,
    0x0000014a    /* 337 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 339 PUSH */,
    0x00000145    /* 340 LREF_PUSH */,
    0x00000007    /* 341 FREF */,
    0x0000022e    /* 342 LOCAL_TAIL_CALL */,
    0x0000002f    /* 343 RET */,
    0x00000002    /* 344 UNDEF */,
    0x0000002f    /* 345 RET */,
    0x00000018    /* 346 JUMP */,
    SG_WORD(-32),
    0x0000002f    /* 348 RET */,
    0x00000018    /* 349 JUMP */,
    SG_WORD(-35),
    0x0000002f    /* 351 RET */,
    0x00000018    /* 352 JUMP */,
    SG_WORD(-188),
    0x0000002f    /* 354 RET */,
    0x00000018    /* 355 JUMP */,
    SG_WORD(-191),
    0x0000002f    /* 357 RET */,
    0x00000018    /* 358 JUMP */,
    SG_WORD(-194),
    0x0000002f    /* 360 RET */,
    0x00000018    /* 361 JUMP */,
    SG_WORD(-197),
    0x0000002f    /* 363 RET */,
    0x00000018    /* 364 JUMP */,
    SG_WORD(-287),
    0x0000002f    /* 366 RET */,
    0x00000018    /* 367 JUMP */,
    SG_WORD(-290),
    0x0000002f    /* 369 RET */,
    0x00000018    /* 370 JUMP */,
    SG_WORD(-293),
    0x0000002f    /* 372 RET */,
    /* check-template */0x00000263    /*   0 RESV_STACK */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000029    /*   2 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[63])) /* #<code-builder control-patvar-exists? (2 0 1)> */,
    0x00000231    /*   4 INST_STACK */,
    0x00000145    /*   5 LREF_PUSH */,
    0x00000045    /*   6 LREF_PUSH */,
    0x00000029    /*   7 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[65])) /* #<code-builder check-escaped (2 0 2)> */,
    0x00000331    /*   9 INST_STACK */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(4),
    0x00000045    /*  12 LREF_PUSH */,
    0x0000014a    /*  13 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier length#core.macro> */,
    0x0000000b    /*  15 PUSH */,
    0x00000204    /*  16 CONSTI */,
    0x0000001a    /*  17 BNNUME */,
    SG_WORD(29),
    0x00000030    /*  19 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  21 LREF_CAR_PUSH */,
    0x0000014a    /*  22 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  24 TEST */,
    SG_WORD(8),
    0x00000005    /*  26 LREF */,
    0x0000004f    /*  27 CADR */,
    0x0000000b    /*  28 PUSH */,
    0x00000049    /*  29 CONSTI_PUSH */,
    0x00000305    /*  30 LREF */,
    0x0000022e    /*  31 LOCAL_TAIL_CALL */,
    0x0000002f    /*  32 RET */,
    0x00000163    /*  33 RESV_STACK */,
    0x00000045    /*  34 LREF_PUSH */,
    0x00000145    /*  35 LREF_PUSH */,
    0x00000345    /*  36 LREF_PUSH */,
    0x00000245    /*  37 LREF_PUSH */,
    0x00000445    /*  38 LREF_PUSH */,
    0x00000129    /*  39 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[66])) /* #<code-builder loop (2 0 5)> */,
    0x00000431    /*  41 INST_STACK */,
    0x00000045    /*  42 LREF_PUSH */,
    0x00000049    /*  43 CONSTI_PUSH */,
    0x00000405    /*  44 LREF */,
    0x0000022e    /*  45 LOCAL_TAIL_CALL */,
    0x0000002f    /*  46 RET */,
    0x00000018    /*  47 JUMP */,
    SG_WORD(-15),
    0x0000002f    /*  49 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(6),
    0x00000046    /*   2 FREF_PUSH */,
    0x00000045    /*   3 LREF_PUSH */,
    0x00000249    /*   4 CONSTI_PUSH */,
    0x0000034a    /*   5 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*   7 PUSH */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(4),
    0x00000145    /*  10 LREF_PUSH */,
    0x0000014a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier number?#core.macro> */,
    0x00000017    /*  13 TEST */,
    SG_WORD(4),
    0x00000045    /*  15 LREF_PUSH */,
    0x00000105    /*  16 LREF */,
    0x00000037    /*  17 CONS */,
    0x0000002f    /*  18 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier pending-identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(26),
    0x00000030    /*   7 FRAME */,
    SG_WORD(11),
    0x00000030    /*   9 FRAME */,
    SG_WORD(6),
    0x00000046    /*  11 FREF_PUSH */,
    0x00000045    /*  12 LREF_PUSH */,
    0x00000049    /*  13 CONSTI_PUSH */,
    0x0000034a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  16 PUSH */,
    0x0000014a    /*  17 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000022    /*  19 NOT */,
    0x00000017    /*  20 TEST */,
    SG_WORD(10),
    0x00000030    /*  22 FRAME */,
    SG_WORD(7),
    0x00000045    /*  24 LREF_PUSH */,
    0x00000146    /*  25 FREF_PUSH */,
    0x00000047    /*  26 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*  28 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assoc#core.macro> */,
    0x00000022    /*  30 NOT */,
    0x0000002f    /*  31 RET */,
    0x00000018    /*  32 JUMP */,
    SG_WORD(-11),
    0x0000002f    /*  34 RET */,
    /* compile-syntax */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000145    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x0000000b    /*   5 PUSH */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(8),
    0x00000345    /*   8 LREF_PUSH */,
    0x00000029    /*   9 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[68])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*  11 PUSH */,
    0x00000445    /*  12 LREF_PUSH */,
    0x0000024a    /*  13 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier filter-map#core.macro> */,
    0x0000000b    /*  15 PUSH */,
    0x00000030    /*  16 FRAME */,
    SG_WORD(21),
    0x00000145    /*  18 LREF_PUSH */,
    0x00000030    /*  19 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  21 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x0000000b    /*  23 PUSH */,
    0x00000245    /*  24 LREF_PUSH */,
    0x00000345    /*  25 LREF_PUSH */,
    0x00000004    /*  26 CONSTI */,
    0x00000043    /*  27 VEC_REF */,
    0x0000000b    /*  28 PUSH */,
    0x00000047    /*  29 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15858#core.macro> */,
    0x00000545    /*  31 LREF_PUSH */,
    0x00000345    /*  32 LREF_PUSH */,
    0x00000029    /*  33 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[69])) /* #<code-builder #f (1 0 2)> */,
    0x0000000b    /*  35 PUSH */,
    0x0000064a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x0000000b    /*  38 PUSH */,
    0x00000030    /*  39 FRAME */,
    SG_WORD(5),
    0x00000645    /*  41 LREF_PUSH */,
    0x00000545    /*  42 LREF_PUSH */,
    0x0000024a    /*  43 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier check-template#core.macro> */,
    0x00000030    /*  45 FRAME */,
    SG_WORD(8),
    0x00000345    /*  47 LREF_PUSH */,
    0x00000047    /*  48 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000049    /*  50 CONSTI_PUSH */,
    0x00000349    /*  51 CONSTI_PUSH */,
    0x0000044a    /*  52 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  54 PUSH */,
    0x00000030    /*  55 FRAME */,
    SG_WORD(4),
    0x00000745    /*  57 LREF_PUSH */,
    0x0000014a    /*  58 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  60 TEST */,
    SG_WORD(5),
    0x00000003    /*  62 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /*  64 JUMP */,
    SG_WORD(3),
    0x00000009    /*  66 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000132    /*  68 LEAVE */,
    0x0000000b    /*  69 PUSH */,
    0x00000030    /*  70 FRAME */,
    SG_WORD(4),
    0x00000645    /*  72 LREF_PUSH */,
    0x0000014a    /*  73 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*  75 TEST */,
    SG_WORD(35),
    0x00000505    /*  77 LREF */,
    0x00000021    /*  78 BNNULL */,
    SG_WORD(15),
    0x00000047    /*  80 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .expand-syntax#core.macro> */,
    0x00000745    /*  82 LREF_PUSH */,
    0x00000047    /*  83 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000605    /*  85 LREF */,
    0x00000238    /*  86 LIST */,
    0x0000000b    /*  87 PUSH */,
    0x00000003    /*  88 CONST */,
    SG_WORD(SG_UNDEF) /* (()) */,
    0x00000237    /*  90 CONS */,
    0x00000237    /*  91 CONS */,
    0x00000237    /*  92 CONS */,
    0x0000002f    /*  93 RET */,
    0x00000047    /*  94 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .expand-syntax#core.macro> */,
    0x00000745    /*  96 LREF_PUSH */,
    0x00000047    /*  97 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000605    /*  99 LREF */,
    0x00000238    /* 100 LIST */,
    0x0000000b    /* 101 PUSH */,
    0x00000047    /* 102 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000645    /* 104 LREF_PUSH */,
    0x00000004    /* 105 CONSTI */,
    0x00000037    /* 106 CONS */,
    0x00000138    /* 107 LIST */,
    0x00000238    /* 108 LIST */,
    0x00000438    /* 109 LIST */,
    0x0000002f    /* 110 RET */,
    0x00000047    /* 111 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .expand-syntax#core.macro> */,
    0x00000745    /* 113 LREF_PUSH */,
    0x00000047    /* 114 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000605    /* 116 LREF */,
    0x00000238    /* 117 LIST */,
    0x0000000b    /* 118 PUSH */,
    0x00000047    /* 119 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000505    /* 121 LREF */,
    0x00000238    /* 122 LIST */,
    0x00000438    /* 123 LIST */,
    0x0000002f    /* 124 RET */,
    /* wrap-symbol */0x00000046    /*   0 FREF_PUSH */,
    0x00000004    /*   1 CONSTI */,
    0x00000043    /*   2 VEC_REF */,
    0x0000000b    /*   3 PUSH */,
    0x00000030    /*   4 FRAME */,
    SG_WORD(7),
    0x00000045    /*   6 LREF_PUSH */,
    0x00000048    /*   7 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000145    /*   9 LREF_PUSH */,
    0x0000034a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000000b    /*  12 PUSH */,
    0x00000045    /*  13 LREF_PUSH */,
    0x00000245    /*  14 LREF_PUSH */,
    0x0000024b    /*  15 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier add-to-transformer-env!#core.macro> */,
    0x0000002f    /*  17 RET */,
    /* rename-vector */0x00000163    /*   0 RESV_STACK */,
    0x00000005    /*   1 LREF */,
    0x00000042    /*   2 VEC_LEN */,
    0x00000131    /*   3 INST_STACK */,
    0x00000049    /*   4 CONSTI_PUSH */,
    0x00000048    /*   5 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000245    /*   7 LREF_PUSH */,
    0x00000105    /*   8 LREF */,
    0x0000001a    /*   9 BNNUME */,
    SG_WORD(7),
    0x00000305    /*  11 LREF */,
    0x00000017    /*  12 TEST */,
    SG_WORD(2),
    0x0000002f    /*  14 RET */,
    0x00000005    /*  15 LREF */,
    0x0000002f    /*  16 RET */,
    0x00000030    /*  17 FRAME */,
    SG_WORD(7),
    0x00000045    /*  19 LREF_PUSH */,
    0x00000205    /*  20 LREF */,
    0x00000043    /*  21 VEC_REF */,
    0x0000000b    /*  22 PUSH */,
    0x00000007    /*  23 FREF */,
    0x0000012c    /*  24 LOCAL_CALL */,
    0x0000000b    /*  25 PUSH */,
    0x00000445    /*  26 LREF_PUSH */,
    0x00000045    /*  27 LREF_PUSH */,
    0x00000205    /*  28 LREF */,
    0x00000043    /*  29 VEC_REF */,
    0x0000001f    /*  30 BNEQ */,
    SG_WORD(9),
    0x00000205    /*  32 LREF */,
    0x0000010f    /*  33 ADDI */,
    0x0000000b    /*  34 PUSH */,
    0x00000345    /*  35 LREF_PUSH */,
    0x00200219    /*  36 SHIFTJ */,
    0x00000018    /*  37 JUMP */,
    SG_WORD(-31),
    0x0000002f    /*  39 RET */,
    0x00000305    /*  40 LREF */,
    0x00000017    /*  41 TEST */,
    SG_WORD(3),
    0x00000018    /*  43 JUMP */,
    SG_WORD(6),
    0x00000030    /*  45 FRAME */,
    SG_WORD(4),
    0x00000045    /*  47 LREF_PUSH */,
    0x0000014a    /*  48 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector-copy#core.macro> */,
    0x0000000b    /*  50 PUSH */,
    0x00000545    /*  51 LREF_PUSH */,
    0x00000245    /*  52 LREF_PUSH */,
    0x00000405    /*  53 LREF */,
    0x00000044    /*  54 VEC_SET */,
    0x00000205    /*  55 LREF */,
    0x0000010f    /*  56 ADDI */,
    0x0000000b    /*  57 PUSH */,
    0x00000545    /*  58 LREF_PUSH */,
    0x00200219    /*  59 SHIFTJ */,
    0x00000018    /*  60 JUMP */,
    SG_WORD(-54),
    0x0000002f    /*  62 RET */,
    /* loop */0x00000045    /*   0 LREF_PUSH */,
    0x00000163    /*   1 RESV_STACK */,
    0x00000009    /*   2 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x00000231    /*   4 INST_STACK */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(5),
    0x00000145    /*   7 LREF_PUSH */,
    0x00000009    /*   8 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x0000012c    /*  10 LOCAL_CALL */,
    0x00000232    /*  11 LEAVE */,
    0x00000017    /*  12 TEST */,
    SG_WORD(44),
    0x00000005    /*  14 LREF */,
    0x0000003e    /*  15 PAIRP */,
    0x00000017    /*  16 TEST */,
    SG_WORD(30),
    0x00000030    /*  18 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  20 LREF_CAR_PUSH */,
    0x00000207    /*  21 FREF */,
    0x0000012c    /*  22 LOCAL_CALL */,
    0x0000000b    /*  23 PUSH */,
    0x00000030    /*  24 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  26 LREF_CDR_PUSH */,
    0x00000207    /*  27 FREF */,
    0x0000012c    /*  28 LOCAL_CALL */,
    0x0000000b    /*  29 PUSH */,
    0x0000005b    /*  30 LREF_CAR_PUSH */,
    0x00000105    /*  31 LREF */,
    0x0000001f    /*  32 BNEQ */,
    SG_WORD(11),
    0x0000005c    /*  34 LREF_CDR_PUSH */,
    0x00000205    /*  35 LREF */,
    0x0000001f    /*  36 BNEQ */,
    SG_WORD(3),
    0x00000005    /*  38 LREF */,
    0x0000002f    /*  39 RET */,
    0x00000145    /*  40 LREF_PUSH */,
    0x00000205    /*  41 LREF */,
    0x00000037    /*  42 CONS */,
    0x0000002f    /*  43 RET */,
    0x00000018    /*  44 JUMP */,
    SG_WORD(-5),
    0x0000002f    /*  46 RET */,
    0x00000005    /*  47 LREF */,
    0x00000041    /*  48 VECTORP */,
    0x00000017    /*  49 TEST */,
    SG_WORD(5),
    0x00000045    /*  51 LREF_PUSH */,
    0x00000107    /*  52 FREF */,
    0x0000012e    /*  53 LOCAL_TAIL_CALL */,
    0x0000002f    /*  54 RET */,
    0x00000005    /*  55 LREF */,
    0x0000002f    /*  56 RET */,
    0x00000005    /*  57 LREF */,
    0x00000021    /*  58 BNNULL */,
    SG_WORD(3),
    0x00000061    /*  60 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    0x00000005    /*  62 LREF */,
    0x0000003f    /*  63 SYMBOLP */,
    0x00000017    /*  64 TEST */,
    SG_WORD(13),
    0x00000030    /*  66 FRAME */,
    SG_WORD(4),
    0x00000045    /*  68 LREF_PUSH */,
    0x0000014a    /*  69 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier lookup-transformer-env#core.macro> */,
    0x00000017    /*  71 TEST */,
    SG_WORD(2),
    0x0000002f    /*  73 RET */,
    0x00000045    /*  74 LREF_PUSH */,
    0x00000007    /*  75 FREF */,
    0x0000012e    /*  76 LOCAL_TAIL_CALL */,
    0x0000002f    /*  77 RET */,
    0x00000005    /*  78 LREF */,
    0x00000041    /*  79 VECTORP */,
    0x00000017    /*  80 TEST */,
    SG_WORD(5),
    0x00000045    /*  82 LREF_PUSH */,
    0x00000107    /*  83 FREF */,
    0x0000012e    /*  84 LOCAL_TAIL_CALL */,
    0x0000002f    /*  85 RET */,
    0x00000005    /*  86 LREF */,
    0x0000003e    /*  87 PAIRP */,
    0x00000017    /*  88 TEST */,
    SG_WORD(30),
    0x00000030    /*  90 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  92 LREF_CAR_PUSH */,
    0x00000207    /*  93 FREF */,
    0x0000012c    /*  94 LOCAL_CALL */,
    0x0000000b    /*  95 PUSH */,
    0x00000030    /*  96 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  98 LREF_CDR_PUSH */,
    0x00000207    /*  99 FREF */,
    0x0000012c    /* 100 LOCAL_CALL */,
    0x0000000b    /* 101 PUSH */,
    0x00000145    /* 102 LREF_PUSH */,
    0x00000055    /* 103 LREF_CAR */,
    0x0000001f    /* 104 BNEQ */,
    SG_WORD(11),
    0x00000245    /* 106 LREF_PUSH */,
    0x00000056    /* 107 LREF_CDR */,
    0x0000001f    /* 108 BNEQ */,
    SG_WORD(3),
    0x00000005    /* 110 LREF */,
    0x0000002f    /* 111 RET */,
    0x00000145    /* 112 LREF_PUSH */,
    0x00000205    /* 113 LREF */,
    0x00000037    /* 114 CONS */,
    0x0000002f    /* 115 RET */,
    0x00000018    /* 116 JUMP */,
    SG_WORD(-5),
    0x0000002f    /* 118 RET */,
    0x00000005    /* 119 LREF */,
    0x0000002f    /* 120 RET */,
    /* partial-identifier */0x00000163    /*   0 RESV_STACK */,
    0x00000146    /*   1 FREF_PUSH */,
    0x00000029    /*   2 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[72])) /* #<code-builder rename-vector (1 0 1)> */,
    0x00000131    /*   4 INST_STACK */,
    0x00000163    /*   5 RESV_STACK */,
    0x00000245    /*   6 LREF_PUSH */,
    0x00000145    /*   7 LREF_PUSH */,
    0x00000046    /*   8 FREF_PUSH */,
    0x00000329    /*   9 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[73])) /* #<code-builder loop (1 0 3)> */,
    0x00000231    /*  11 INST_STACK */,
    0x00000045    /*  12 LREF_PUSH */,
    0x00000205    /*  13 LREF */,
    0x0000012e    /*  14 LOCAL_TAIL_CALL */,
    0x0000002f    /*  15 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(7),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000046    /*   3 FREF_PUSH */,
    0x00000047    /*   4 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*   6 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assoc#core.macro> */,
    0x00000022    /*   8 NOT */,
    0x0000002f    /*   9 RET */,
    /* expand-syntax */0x00000463    /*   0 RESV_STACK */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000331    /*   5 INST_STACK */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   8 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000431    /*  10 INST_STACK */,
    0x00000345    /*  11 LREF_PUSH */,
    0x00000029    /*  12 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[71])) /* #<code-builder wrap-symbol (1 0 1)> */,
    0x00000531    /*  14 INST_STACK */,
    0x00000645    /*  15 LREF_PUSH */,
    0x00000545    /*  16 LREF_PUSH */,
    0x00000229    /*  17 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[74])) /* #<code-builder partial-identifier (1 0 2)> */,
    0x00000631    /*  19 INST_STACK */,
    0x00000105    /*  20 LREF */,
    0x00000021    /*  21 BNNULL */,
    SG_WORD(3),
    0x00000061    /*  23 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    0x00000030    /*  25 FRAME */,
    SG_WORD(30),
    0x00000030    /*  27 FRAME */,
    SG_WORD(23),
    0x00000145    /*  29 LREF_PUSH */,
    0x00000030    /*  30 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  32 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x0000000b    /*  34 PUSH */,
    0x00000445    /*  35 LREF_PUSH */,
    0x00000104    /*  36 CONSTI */,
    0x00000043    /*  37 VEC_REF */,
    0x0000000b    /*  38 PUSH */,
    0x00000445    /*  39 LREF_PUSH */,
    0x00000004    /*  40 CONSTI */,
    0x00000043    /*  41 VEC_REF */,
    0x0000000b    /*  42 PUSH */,
    0x00000047    /*  43 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15856#core.macro> */,
    0x00000245    /*  45 LREF_PUSH */,
    0x00000029    /*  46 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[75])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*  48 PUSH */,
    0x0000064a    /*  49 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x0000000b    /*  51 PUSH */,
    0x00000245    /*  52 LREF_PUSH */,
    0x00000045    /*  53 LREF_PUSH */,
    0x0000034a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier transcribe-template#core.macro> */,
    0x0000000b    /*  56 PUSH */,
    0x00000705    /*  57 LREF */,
    0x00000021    /*  58 BNNULL */,
    SG_WORD(3),
    0x00000061    /*  60 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    0x00000345    /*  62 LREF_PUSH */,
    0x00000405    /*  63 LREF */,
    0x0000001f    /*  64 BNEQ */,
    SG_WORD(3),
    0x00000705    /*  66 LREF */,
    0x0000002f    /*  67 RET */,
    0x00000030    /*  68 FRAME */,
    SG_WORD(4),
    0x00000745    /*  70 LREF_PUSH */,
    0x0000014a    /*  71 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*  73 TEST */,
    SG_WORD(3),
    0x00000705    /*  75 LREF */,
    0x0000002f    /*  76 RET */,
    0x00000705    /*  77 LREF */,
    0x0000003f    /*  78 SYMBOLP */,
    0x00000017    /*  79 TEST */,
    SG_WORD(13),
    0x00000030    /*  81 FRAME */,
    SG_WORD(4),
    0x00000745    /*  83 LREF_PUSH */,
    0x0000014a    /*  84 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier lookup-transformer-env#core.macro> */,
    0x00000017    /*  86 TEST */,
    SG_WORD(2),
    0x0000002f    /*  88 RET */,
    0x00000745    /*  89 LREF_PUSH */,
    0x00000505    /*  90 LREF */,
    0x0000012e    /*  91 LOCAL_TAIL_CALL */,
    0x0000002f    /*  92 RET */,
    0x00000745    /*  93 LREF_PUSH */,
    0x00000605    /*  94 LREF */,
    0x0000012e    /*  95 LOCAL_TAIL_CALL */,
    0x0000002f    /*  96 RET */,
    /* rank-of */0x00000145    /*   0 LREF_PUSH */,
    0x00000245    /*   1 LREF_PUSH */,
    0x00000305    /*   2 LREF */,
    0x00000021    /*   3 BNNULL */,
    SG_WORD(3),
    -0x000000fc   /*   5 CONSTI */,
    0x0000002f    /*   6 RET */,
    0x0000035b    /*   7 LREF_CAR_PUSH */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(5),
    0x00000045    /*  10 LREF_PUSH */,
    0x0000045b    /*  11 LREF_CAR_PUSH */,
    0x0000024a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x00000017    /*  14 TEST */,
    SG_WORD(3),
    0x00000456    /*  16 LREF_CDR */,
    0x0000002f    /*  17 RET */,
    0x0000035c    /*  18 LREF_CDR_PUSH */,
    0x00300119    /*  19 SHIFTJ */,
    0x00000018    /*  20 JUMP */,
    SG_WORD(-19),
    0x0000002f    /*  22 RET */,
    /* subform-of */0x00000030    /*   0 FRAME */,
    SG_WORD(5),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x0000024a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x00000036    /*   6 CDR */,
    0x0000002f    /*   7 RET */,
    /* collect-ellipsis-vars */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x0000000b    /*   5 PUSH */,
    0x00000345    /*   6 LREF_PUSH */,
    0x00000048    /*   7 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /*   9 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000505    /*  11 LREF */,
    0x00000021    /*  12 BNNULL */,
    SG_WORD(5),
    0x00000745    /*  14 LREF_PUSH */,
    0x0000014b    /*  15 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier reverse!#core.macro> */,
    0x0000002f    /*  17 RET */,
    0x0000055b    /*  18 LREF_CAR_PUSH */,
    0x0000085b    /*  19 LREF_CAR_PUSH */,
    0x00000030    /*  20 FRAME */,
    SG_WORD(7),
    0x00000945    /*  22 LREF_PUSH */,
    0x00000445    /*  23 LREF_PUSH */,
    0x00000047    /*  24 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*  26 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier member#core.macro> */,
    0x00000017    /*  28 TEST */,
    SG_WORD(55),
    0x00000030    /*  30 FRAME */,
    SG_WORD(7),
    0x00000945    /*  32 LREF_PUSH */,
    0x00000645    /*  33 LREF_PUSH */,
    0x00000047    /*  34 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier member#core.macro> */,
    0x00000017    /*  38 TEST */,
    SG_WORD(8),
    0x0000055c    /*  40 LREF_CDR_PUSH */,
    0x00000645    /*  41 LREF_PUSH */,
    0x00000745    /*  42 LREF_PUSH */,
    0x00500319    /*  43 SHIFTJ */,
    0x00000018    /*  44 JUMP */,
    SG_WORD(-34),
    0x0000002f    /*  46 RET */,
    0x00000030    /*  47 FRAME */,
    SG_WORD(7),
    0x00000945    /*  49 LREF_PUSH */,
    0x00000145    /*  50 LREF_PUSH */,
    0x00000047    /*  51 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*  53 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assoc#core.macro> */,
    0x00000053    /*  55 CDR_PUSH */,
    0x0000055c    /*  56 LREF_CDR_PUSH */,
    0x00000945    /*  57 LREF_PUSH */,
    0x00000605    /*  58 LREF */,
    0x00000054    /*  59 CONS_PUSH */,
    0x00000a45    /*  60 LREF_PUSH */,
    0x00000205    /*  61 LREF */,
    0x0000001b    /*  62 BNLT */,
    SG_WORD(4),
    0x00000805    /*  64 LREF */,
    0x00000018    /*  65 JUMP */,
    SG_WORD(11),
    0x00000856    /*  67 LREF_CDR */,
    0x00000021    /*  68 BNNULL */,
    SG_WORD(4),
    0x00000805    /*  70 LREF */,
    0x00000018    /*  71 JUMP */,
    SG_WORD(5),
    0x0000085b    /*  73 LREF_CAR_PUSH */,
    0x00000805    /*  74 LREF */,
    0x0000004f    /*  75 CADR */,
    0x00000037    /*  76 CONS */,
    0x0000000b    /*  77 PUSH */,
    0x00000705    /*  78 LREF */,
    0x00000054    /*  79 CONS_PUSH */,
    0x00500319    /*  80 SHIFTJ */,
    0x00000018    /*  81 JUMP */,
    SG_WORD(-71),
    0x0000002f    /*  83 RET */,
    0x00000018    /*  84 JUMP */,
    SG_WORD(-45),
    0x0000002f    /*  86 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(13),
    0x00000049    /*   7 CONSTI_PUSH */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(5),
    0x00000045    /*  10 LREF_PUSH */,
    0x00000146    /*  11 FREF_PUSH */,
    0x0000024a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  14 PUSH */,
    0x00000145    /*  15 LREF_PUSH */,
    0x0000034b    /*  16 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier <#core.macro> */,
    0x0000002f    /*  18 RET */,
    0x00000005    /*  19 LREF */,
    0x0000003e    /*  20 PAIRP */,
    0x00000017    /*  21 TEST */,
    SG_WORD(15),
    0x00000030    /*  23 FRAME */,
    SG_WORD(5),
    0x0000005b    /*  25 LREF_CAR_PUSH */,
    0x00000145    /*  26 LREF_PUSH */,
    0x00000007    /*  27 FREF */,
    0x0000022c    /*  28 LOCAL_CALL */,
    0x00000017    /*  29 TEST */,
    SG_WORD(2),
    0x0000002f    /*  31 RET */,
    0x0000005c    /*  32 LREF_CDR_PUSH */,
    0x00000145    /*  33 LREF_PUSH */,
    0x00000007    /*  34 FREF */,
    0x0000022e    /*  35 LOCAL_TAIL_CALL */,
    0x0000002f    /*  36 RET */,
    0x00000005    /*  37 LREF */,
    0x00000041    /*  38 VECTORP */,
    0x00000017    /*  39 TEST */,
    SG_WORD(10),
    0x00000030    /*  41 FRAME */,
    SG_WORD(4),
    0x00000045    /*  43 LREF_PUSH */,
    0x0000014a    /*  44 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  46 PUSH */,
    0x00000145    /*  47 LREF_PUSH */,
    0x00000007    /*  48 FREF */,
    0x0000022e    /*  49 LOCAL_TAIL_CALL */,
    0x0000002f    /*  50 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(13),
    0x00000049    /*   7 CONSTI_PUSH */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(5),
    0x00000045    /*  10 LREF_PUSH */,
    0x00000146    /*  11 FREF_PUSH */,
    0x0000024a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  14 PUSH */,
    0x00000145    /*  15 LREF_PUSH */,
    0x0000034b    /*  16 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier <#core.macro> */,
    0x0000002f    /*  18 RET */,
    0x00000005    /*  19 LREF */,
    0x0000003e    /*  20 PAIRP */,
    0x00000017    /*  21 TEST */,
    SG_WORD(178),
    0x00000030    /*  23 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  25 LREF_CAR_PUSH */,
    0x0000014a    /*  26 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  28 TEST */,
    SG_WORD(168),
    0x00000056    /*  30 LREF_CDR */,
    0x0000003e    /*  31 PAIRP */,
    0x00000017    /*  32 TEST */,
    SG_WORD(161),
    0x00000005    /*  34 LREF */,
    0x00000051    /*  35 CDDR */,
    0x00000021    /*  36 BNNULL */,
    SG_WORD(16),
    0x00000005    /*  38 LREF */,
    0x0000004f    /*  39 CADR */,
    0x0000000b    /*  40 PUSH */,
    0x00000145    /*  41 LREF_PUSH */,
    0x00000163    /*  42 RESV_STACK */,
    0x00000146    /*  43 FREF_PUSH */,
    0x00000445    /*  44 LREF_PUSH */,
    0x00000129    /*  45 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[80])) /* #<code-builder loop (2 0 2)> */,
    0x00000431    /*  47 INST_STACK */,
    0x00000245    /*  48 LREF_PUSH */,
    0x00000345    /*  49 LREF_PUSH */,
    0x00000405    /*  50 LREF */,
    0x0000022e    /*  51 LOCAL_TAIL_CALL */,
    0x0000002f    /*  52 RET */,
    0x00000005    /*  53 LREF */,
    0x0000003e    /*  54 PAIRP */,
    0x00000017    /*  55 TEST */,
    SG_WORD(135),
    0x00000056    /*  57 LREF_CDR */,
    0x0000003e    /*  58 PAIRP */,
    0x00000017    /*  59 TEST */,
    SG_WORD(128),
    0x00000030    /*  61 FRAME */,
    SG_WORD(6),
    0x00000005    /*  63 LREF */,
    0x0000004f    /*  64 CADR */,
    0x0000000b    /*  65 PUSH */,
    0x0000014a    /*  66 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  68 TEST */,
    SG_WORD(116),
    0x00000005    /*  70 LREF */,
    0x00000051    /*  71 CDDR */,
    0x0000003e    /*  72 PAIRP */,
    0x00000017    /*  73 TEST */,
    SG_WORD(108),
    0x00000030    /*  75 FRAME */,
    SG_WORD(9),
    0x00000030    /*  77 FRAME */,
    SG_WORD(4),
    0x00000045    /*  79 LREF_PUSH */,
    0x0000014a    /*  80 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  82 PUSH */,
    0x0000014a    /*  83 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  85 TEST */,
    SG_WORD(23),
    0x00000030    /*  87 FRAME */,
    SG_WORD(4),
    0x00000045    /*  89 LREF_PUSH */,
    0x0000014a    /*  90 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000328    /*  92 RECEIVE */,
    0x00000030    /*  93 FRAME */,
    SG_WORD(7),
    0x00000245    /*  95 LREF_PUSH */,
    0x00000105    /*  96 LREF */,
    0x0000010f    /*  97 ADDI */,
    0x0000000b    /*  98 PUSH */,
    0x00000007    /*  99 FREF */,
    0x0000022c    /* 100 LOCAL_CALL */,
    0x00000017    /* 101 TEST */,
    SG_WORD(2),
    0x0000002f    /* 103 RET */,
    0x00000345    /* 104 LREF_PUSH */,
    0x00000145    /* 105 LREF_PUSH */,
    0x00000007    /* 106 FREF */,
    0x0000022e    /* 107 LOCAL_TAIL_CALL */,
    0x0000002f    /* 108 RET */,
    0x00000005    /* 109 LREF */,
    0x0000003e    /* 110 PAIRP */,
    0x00000017    /* 111 TEST */,
    SG_WORD(67),
    0x00000056    /* 113 LREF_CDR */,
    0x0000003e    /* 114 PAIRP */,
    0x00000017    /* 115 TEST */,
    SG_WORD(60),
    0x00000030    /* 117 FRAME */,
    SG_WORD(6),
    0x00000005    /* 119 LREF */,
    0x0000004f    /* 120 CADR */,
    0x0000000b    /* 121 PUSH */,
    0x0000014a    /* 122 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 124 TEST */,
    SG_WORD(19),
    0x00000030    /* 126 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 128 LREF_CAR_PUSH */,
    0x00000105    /* 129 LREF */,
    0x0000010f    /* 130 ADDI */,
    0x0000000b    /* 131 PUSH */,
    0x00000007    /* 132 FREF */,
    0x0000022c    /* 133 LOCAL_CALL */,
    0x00000017    /* 134 TEST */,
    SG_WORD(2),
    0x0000002f    /* 136 RET */,
    0x00000005    /* 137 LREF */,
    0x00000051    /* 138 CDDR */,
    0x0000000b    /* 139 PUSH */,
    0x00000145    /* 140 LREF_PUSH */,
    0x00000007    /* 141 FREF */,
    0x0000022e    /* 142 LOCAL_TAIL_CALL */,
    0x0000002f    /* 143 RET */,
    0x00000005    /* 144 LREF */,
    0x0000003e    /* 145 PAIRP */,
    0x00000017    /* 146 TEST */,
    SG_WORD(15),
    0x00000030    /* 148 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 150 LREF_CAR_PUSH */,
    0x00000145    /* 151 LREF_PUSH */,
    0x00000007    /* 152 FREF */,
    0x0000022c    /* 153 LOCAL_CALL */,
    0x00000017    /* 154 TEST */,
    SG_WORD(2),
    0x0000002f    /* 156 RET */,
    0x0000005c    /* 157 LREF_CDR_PUSH */,
    0x00000145    /* 158 LREF_PUSH */,
    0x00000007    /* 159 FREF */,
    0x0000022e    /* 160 LOCAL_TAIL_CALL */,
    0x0000002f    /* 161 RET */,
    0x00000005    /* 162 LREF */,
    0x00000041    /* 163 VECTORP */,
    0x00000017    /* 164 TEST */,
    SG_WORD(10),
    0x00000030    /* 166 FRAME */,
    SG_WORD(4),
    0x00000045    /* 168 LREF_PUSH */,
    0x0000014a    /* 169 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 171 PUSH */,
    0x00000145    /* 172 LREF_PUSH */,
    0x00000007    /* 173 FREF */,
    0x0000022e    /* 174 LOCAL_TAIL_CALL */,
    0x0000002f    /* 175 RET */,
    0x00000018    /* 176 JUMP */,
    SG_WORD(-33),
    0x0000002f    /* 178 RET */,
    0x00000018    /* 179 JUMP */,
    SG_WORD(-36),
    0x0000002f    /* 181 RET */,
    0x00000018    /* 182 JUMP */,
    SG_WORD(-74),
    0x0000002f    /* 184 RET */,
    0x00000018    /* 185 JUMP */,
    SG_WORD(-77),
    0x0000002f    /* 187 RET */,
    0x00000018    /* 188 JUMP */,
    SG_WORD(-80),
    0x0000002f    /* 190 RET */,
    0x00000018    /* 191 JUMP */,
    SG_WORD(-83),
    0x0000002f    /* 193 RET */,
    0x00000018    /* 194 JUMP */,
    SG_WORD(-142),
    0x0000002f    /* 196 RET */,
    0x00000018    /* 197 JUMP */,
    SG_WORD(-145),
    0x0000002f    /* 199 RET */,
    0x00000018    /* 200 JUMP */,
    SG_WORD(-148),
    0x0000002f    /* 202 RET */,
    /* contain-rank-moved-var? */0x00000163    /*   0 RESV_STACK */,
    0x00000145    /*   1 LREF_PUSH */,
    0x00000345    /*   2 LREF_PUSH */,
    0x00000129    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[81])) /* #<code-builder loop (2 0 2)> */,
    0x00000331    /*   5 INST_STACK */,
    0x00000045    /*   6 LREF_PUSH */,
    0x00000049    /*   7 CONSTI_PUSH */,
    0x00000305    /*   8 LREF */,
    0x0000022e    /*   9 LOCAL_TAIL_CALL */,
    0x0000002f    /*  10 RET */,
    /* revealed */0x00000030    /*   0 FRAME */,
    SG_WORD(12),
    0x00000049    /*   2 CONSTI_PUSH */,
    0x00000030    /*   3 FRAME */,
    SG_WORD(5),
    0x00000045    /*   5 LREF_PUSH */,
    0x00000546    /*   6 FREF_PUSH */,
    0x0000024a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*   9 PUSH */,
    0x00000145    /*  10 LREF_PUSH */,
    0x0000034a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier <#core.macro> */,
    0x00000017    /*  13 TEST */,
    SG_WORD(80),
    0x00000030    /*  15 FRAME */,
    SG_WORD(9),
    0x00000030    /*  17 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier gensym#core.macro> */,
    0x0000000b    /*  21 PUSH */,
    0x00000045    /*  22 LREF_PUSH */,
    0x0000024a    /*  23 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier copy-identifier#core.macro> */,
    0x0000000b    /*  25 PUSH */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(7),
    0x00000446    /*  28 FREF_PUSH */,
    0x00000245    /*  29 LREF_PUSH */,
    0x00000048    /*  30 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*  32 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-ref#core.macro> */,
    0x00000017    /*  34 TEST */,
    SG_WORD(3),
    0x00000018    /*  36 JUMP */,
    SG_WORD(55),
    0x00000145    /*  38 LREF_PUSH */,
    0x00000030    /*  39 FRAME */,
    SG_WORD(5),
    0x00000045    /*  41 LREF_PUSH */,
    0x00000346    /*  42 FREF_PUSH */,
    0x0000024a    /*  43 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x00000010    /*  45 SUB */,
    0x0000000b    /*  46 PUSH */,
    0x00000030    /*  47 FRAME */,
    SG_WORD(5),
    0x00000045    /*  49 LREF_PUSH */,
    0x00000246    /*  50 FREF_PUSH */,
    0x0000024a    /*  51 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x00000053    /*  53 CDR_PUSH */,
    0x00000345    /*  54 LREF_PUSH */,
    0x00000004    /*  55 CONSTI */,
    0x0000001d    /*  56 BNGT */,
    SG_WORD(20),
    0x00000305    /*  58 LREF */,
    -0x000000f1   /*  59 ADDI */,
    0x0000000b    /*  60 PUSH */,
    0x0000045b    /*  61 LREF_CAR_PUSH */,
    0x00000605    /*  62 LREF */,
    0x00000138    /*  63 LIST */,
    0x0000000b    /*  64 PUSH */,
    0x00000745    /*  65 LREF_PUSH */,
    0x00000705    /*  66 LREF */,
    0x0000004d    /*  67 SET_CDR */,
    0x00000705    /*  68 LREF */,
    0x00000232    /*  69 LEAVE */,
    0x00000138    /*  70 LIST */,
    0x0000000b    /*  71 PUSH */,
    0x00300219    /*  72 SHIFTJ */,
    0x00000018    /*  73 JUMP */,
    SG_WORD(-20),
    0x00000018    /*  75 JUMP */,
    SG_WORD(15),
    0x00000030    /*  77 FRAME */,
    SG_WORD(6),
    0x00000446    /*  79 FREF_PUSH */,
    0x00000245    /*  80 LREF_PUSH */,
    0x00000145    /*  81 LREF_PUSH */,
    0x0000034a    /*  82 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x00000030    /*  84 FRAME */,
    SG_WORD(6),
    0x00000146    /*  86 FREF_PUSH */,
    0x00000245    /*  87 LREF_PUSH */,
    0x00000445    /*  88 LREF_PUSH */,
    0x0000034a    /*  89 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-set!#core.macro> */,
    0x00000232    /*  91 LEAVE */,
    0x00000205    /*  92 LREF */,
    0x0000002f    /*  93 RET */,
    0x00000030    /*  94 FRAME */,
    SG_WORD(5),
    0x00000045    /*  96 LREF_PUSH */,
    0x00000046    /*  97 FREF_PUSH */,
    0x0000024a    /*  98 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assq#core.macro> */,
    0x00000017    /* 100 TEST */,
    SG_WORD(3),
    0x00000005    /* 102 LREF */,
    0x0000002f    /* 103 RET */,
    0x00000005    /* 104 LREF */,
    0x0000002f    /* 105 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(6),
    0x00000045    /*   7 LREF_PUSH */,
    0x00000145    /*   8 LREF_PUSH */,
    0x00000107    /*   9 FREF */,
    0x0000022e    /*  10 LOCAL_TAIL_CALL */,
    0x0000002f    /*  11 RET */,
    0x00000005    /*  12 LREF */,
    0x0000003e    /*  13 PAIRP */,
    0x00000017    /*  14 TEST */,
    SG_WORD(16),
    0x00000030    /*  16 FRAME */,
    SG_WORD(5),
    0x0000005b    /*  18 LREF_CAR_PUSH */,
    0x00000145    /*  19 LREF_PUSH */,
    0x00000007    /*  20 FREF */,
    0x0000022c    /*  21 LOCAL_CALL */,
    0x0000000b    /*  22 PUSH */,
    0x00000030    /*  23 FRAME */,
    SG_WORD(5),
    0x0000005c    /*  25 LREF_CDR_PUSH */,
    0x00000145    /*  26 LREF_PUSH */,
    0x00000007    /*  27 FREF */,
    0x0000022c    /*  28 LOCAL_CALL */,
    0x00000037    /*  29 CONS */,
    0x0000002f    /*  30 RET */,
    0x00000005    /*  31 LREF */,
    0x00000041    /*  32 VECTORP */,
    0x00000017    /*  33 TEST */,
    SG_WORD(16),
    0x00000030    /*  35 FRAME */,
    SG_WORD(10),
    0x00000030    /*  37 FRAME */,
    SG_WORD(4),
    0x00000045    /*  39 LREF_PUSH */,
    0x0000014a    /*  40 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  42 PUSH */,
    0x00000145    /*  43 LREF_PUSH */,
    0x00000007    /*  44 FREF */,
    0x0000022c    /*  45 LOCAL_CALL */,
    0x0000000b    /*  46 PUSH */,
    0x0000014b    /*  47 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list->vector#core.macro> */,
    0x0000002f    /*  49 RET */,
    0x00000005    /*  50 LREF */,
    0x0000002f    /*  51 RET */,
    /* loop */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(6),
    0x00000045    /*   7 LREF_PUSH */,
    0x00000145    /*   8 LREF_PUSH */,
    0x00000107    /*   9 FREF */,
    0x0000022e    /*  10 LOCAL_TAIL_CALL */,
    0x0000002f    /*  11 RET */,
    0x00000005    /*  12 LREF */,
    0x0000003e    /*  13 PAIRP */,
    0x00000017    /*  14 TEST */,
    SG_WORD(197),
    0x00000030    /*  16 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  18 LREF_CAR_PUSH */,
    0x0000014a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  21 TEST */,
    SG_WORD(187),
    0x00000056    /*  23 LREF_CDR */,
    0x0000003e    /*  24 PAIRP */,
    0x00000017    /*  25 TEST */,
    SG_WORD(180),
    0x00000005    /*  27 LREF */,
    0x00000051    /*  28 CDDR */,
    0x00000021    /*  29 BNNULL */,
    SG_WORD(19),
    0x0000005b    /*  31 LREF_CAR_PUSH */,
    0x0000005c    /*  32 LREF_CDR_PUSH */,
    0x00000145    /*  33 LREF_PUSH */,
    0x00000163    /*  34 RESV_STACK */,
    0x00000146    /*  35 FREF_PUSH */,
    0x00000545    /*  36 LREF_PUSH */,
    0x00000129    /*  37 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[84])) /* #<code-builder loop (2 0 2)> */,
    0x00000531    /*  39 INST_STACK */,
    0x00000030    /*  40 FRAME */,
    SG_WORD(5),
    0x00000345    /*  42 LREF_PUSH */,
    0x00000445    /*  43 LREF_PUSH */,
    0x00000505    /*  44 LREF */,
    0x0000022c    /*  45 LOCAL_CALL */,
    0x00000332    /*  46 LEAVE */,
    0x00000037    /*  47 CONS */,
    0x0000002f    /*  48 RET */,
    0x00000005    /*  49 LREF */,
    0x0000003e    /*  50 PAIRP */,
    0x00000017    /*  51 TEST */,
    SG_WORD(151),
    0x00000056    /*  53 LREF_CDR */,
    0x0000003e    /*  54 PAIRP */,
    0x00000017    /*  55 TEST */,
    SG_WORD(144),
    0x00000030    /*  57 FRAME */,
    SG_WORD(6),
    0x00000005    /*  59 LREF */,
    0x0000004f    /*  60 CADR */,
    0x0000000b    /*  61 PUSH */,
    0x0000014a    /*  62 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  64 TEST */,
    SG_WORD(132),
    0x00000005    /*  66 LREF */,
    0x00000051    /*  67 CDDR */,
    0x0000003e    /*  68 PAIRP */,
    0x00000017    /*  69 TEST */,
    SG_WORD(124),
    0x00000030    /*  71 FRAME */,
    SG_WORD(9),
    0x00000030    /*  73 FRAME */,
    SG_WORD(4),
    0x00000045    /*  75 LREF_PUSH */,
    0x0000014a    /*  76 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  78 PUSH */,
    0x0000014a    /*  79 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  81 TEST */,
    SG_WORD(27),
    0x00000030    /*  83 FRAME */,
    SG_WORD(4),
    0x00000045    /*  85 LREF_PUSH */,
    0x0000014a    /*  86 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000328    /*  88 RECEIVE */,
    0x00000030    /*  89 FRAME */,
    SG_WORD(7),
    0x00000245    /*  91 LREF_PUSH */,
    0x00000105    /*  92 LREF */,
    0x0000010f    /*  93 ADDI */,
    0x0000000b    /*  94 PUSH */,
    0x00000007    /*  95 FREF */,
    0x0000022c    /*  96 LOCAL_CALL */,
    0x0000000b    /*  97 PUSH */,
    0x00000047    /*  98 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .ellipsis#core.macro> */,
    0x00000030    /* 100 FRAME */,
    SG_WORD(5),
    0x00000345    /* 102 LREF_PUSH */,
    0x00000145    /* 103 LREF_PUSH */,
    0x00000007    /* 104 FREF */,
    0x0000022c    /* 105 LOCAL_CALL */,
    0x00000037    /* 106 CONS */,
    0x00000239    /* 107 APPEND */,
    0x0000002f    /* 108 RET */,
    0x00000005    /* 109 LREF */,
    0x0000003e    /* 110 PAIRP */,
    0x00000017    /* 111 TEST */,
    SG_WORD(79),
    0x00000056    /* 113 LREF_CDR */,
    0x0000003e    /* 114 PAIRP */,
    0x00000017    /* 115 TEST */,
    SG_WORD(72),
    0x00000030    /* 117 FRAME */,
    SG_WORD(6),
    0x00000005    /* 119 LREF */,
    0x0000004f    /* 120 CADR */,
    0x0000000b    /* 121 PUSH */,
    0x0000014a    /* 122 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 124 TEST */,
    SG_WORD(23),
    0x00000030    /* 126 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 128 LREF_CAR_PUSH */,
    0x00000105    /* 129 LREF */,
    0x0000010f    /* 130 ADDI */,
    0x0000000b    /* 131 PUSH */,
    0x00000007    /* 132 FREF */,
    0x0000022c    /* 133 LOCAL_CALL */,
    0x0000000b    /* 134 PUSH */,
    0x00000047    /* 135 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier .ellipsis#core.macro> */,
    0x00000030    /* 137 FRAME */,
    SG_WORD(7),
    0x00000005    /* 139 LREF */,
    0x00000051    /* 140 CDDR */,
    0x0000000b    /* 141 PUSH */,
    0x00000145    /* 142 LREF_PUSH */,
    0x00000007    /* 143 FREF */,
    0x0000022c    /* 144 LOCAL_CALL */,
    0x00000037    /* 145 CONS */,
    0x00000037    /* 146 CONS */,
    0x0000002f    /* 147 RET */,
    0x00000005    /* 148 LREF */,
    0x0000003e    /* 149 PAIRP */,
    0x00000017    /* 150 TEST */,
    SG_WORD(16),
    0x00000030    /* 152 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 154 LREF_CAR_PUSH */,
    0x00000145    /* 155 LREF_PUSH */,
    0x00000007    /* 156 FREF */,
    0x0000022c    /* 157 LOCAL_CALL */,
    0x0000000b    /* 158 PUSH */,
    0x00000030    /* 159 FRAME */,
    SG_WORD(5),
    0x0000005c    /* 161 LREF_CDR_PUSH */,
    0x00000145    /* 162 LREF_PUSH */,
    0x00000007    /* 163 FREF */,
    0x0000022c    /* 164 LOCAL_CALL */,
    0x00000037    /* 165 CONS */,
    0x0000002f    /* 166 RET */,
    0x00000005    /* 167 LREF */,
    0x00000041    /* 168 VECTORP */,
    0x00000017    /* 169 TEST */,
    SG_WORD(16),
    0x00000030    /* 171 FRAME */,
    SG_WORD(10),
    0x00000030    /* 173 FRAME */,
    SG_WORD(4),
    0x00000045    /* 175 LREF_PUSH */,
    0x0000014a    /* 176 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 178 PUSH */,
    0x00000145    /* 179 LREF_PUSH */,
    0x00000007    /* 180 FREF */,
    0x0000022c    /* 181 LOCAL_CALL */,
    0x0000000b    /* 182 PUSH */,
    0x0000014b    /* 183 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list->vector#core.macro> */,
    0x0000002f    /* 185 RET */,
    0x00000005    /* 186 LREF */,
    0x0000002f    /* 187 RET */,
    0x00000018    /* 188 JUMP */,
    SG_WORD(-41),
    0x0000002f    /* 190 RET */,
    0x00000018    /* 191 JUMP */,
    SG_WORD(-44),
    0x0000002f    /* 193 RET */,
    0x00000018    /* 194 JUMP */,
    SG_WORD(-86),
    0x0000002f    /* 196 RET */,
    0x00000018    /* 197 JUMP */,
    SG_WORD(-89),
    0x0000002f    /* 199 RET */,
    0x00000018    /* 200 JUMP */,
    SG_WORD(-92),
    0x0000002f    /* 202 RET */,
    0x00000018    /* 203 JUMP */,
    SG_WORD(-95),
    0x0000002f    /* 205 RET */,
    0x00000018    /* 206 JUMP */,
    SG_WORD(-158),
    0x0000002f    /* 208 RET */,
    0x00000018    /* 209 JUMP */,
    SG_WORD(-161),
    0x0000002f    /* 211 RET */,
    0x00000018    /* 212 JUMP */,
    SG_WORD(-164),
    0x0000002f    /* 214 RET */,
    /* adapt-to-rank-moved-vars */0x00000030    /*   0 FRAME */,
    SG_WORD(6),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x00000245    /*   4 LREF_PUSH */,
    0x0000034a    /*   5 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier contain-rank-moved-var?#core.macro> */,
    0x00000017    /*   7 TEST */,
    SG_WORD(56),
    0x00000145    /*   9 LREF_PUSH */,
    0x00000245    /*  10 LREF_PUSH */,
    0x00000363    /*  11 RESV_STACK */,
    0x00000030    /*  12 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x00000531    /*  16 INST_STACK */,
    0x00000030    /*  17 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  19 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x00000631    /*  21 INST_STACK */,
    0x00000145    /*  22 LREF_PUSH */,
    0x00000545    /*  23 LREF_PUSH */,
    0x00000345    /*  24 LREF_PUSH */,
    0x00000445    /*  25 LREF_PUSH */,
    0x00000645    /*  26 LREF_PUSH */,
    0x00000245    /*  27 LREF_PUSH */,
    0x00000029    /*  28 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[83])) /* #<code-builder revealed (2 0 6)> */,
    0x00000731    /*  30 INST_STACK */,
    0x00000045    /*  31 LREF_PUSH */,
    0x00000163    /*  32 RESV_STACK */,
    0x00000745    /*  33 LREF_PUSH */,
    0x00000945    /*  34 LREF_PUSH */,
    0x00000129    /*  35 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[85])) /* #<code-builder loop (2 0 2)> */,
    0x00000931    /*  37 INST_STACK */,
    0x00000030    /*  38 FRAME */,
    SG_WORD(5),
    0x00000845    /*  40 LREF_PUSH */,
    0x00000049    /*  41 CONSTI_PUSH */,
    0x00000905    /*  42 LREF */,
    0x0000022c    /*  43 LOCAL_CALL */,
    0x00000232    /*  44 LEAVE */,
    0x0000000b    /*  45 PUSH */,
    0x00000845    /*  46 LREF_PUSH */,
    0x00000145    /*  47 LREF_PUSH */,
    0x00000030    /*  48 FRAME */,
    SG_WORD(4),
    0x00000545    /*  50 LREF_PUSH */,
    0x0000014a    /*  51 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable->alist#core.macro> */,
    0x00000239    /*  53 APPEND */,
    0x0000000b    /*  54 PUSH */,
    0x00000245    /*  55 LREF_PUSH */,
    0x00000030    /*  56 FRAME */,
    SG_WORD(4),
    0x00000645    /*  58 LREF_PUSH */,
    0x0000014a    /*  59 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable->alist#core.macro> */,
    0x00000239    /*  61 APPEND */,
    0x0000033a    /*  62 VALUES */,
    0x0000002f    /*  63 RET */,
    0x00000045    /*  64 LREF_PUSH */,
    0x00000145    /*  65 LREF_PUSH */,
    0x00000205    /*  66 LREF */,
    0x0000033a    /*  67 VALUES */,
    0x0000002f    /*  68 RET */,
    /* loop */0x00000005    /*   0 LREF */,
    0x00000021    /*   1 BNNULL */,
    SG_WORD(3),
    0x00000005    /*   3 LREF */,
    0x0000002f    /*   4 RET */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(7),
    0x00000005    /*   7 LREF */,
    0x0000004e    /*   8 CAAR */,
    0x0000000b    /*   9 PUSH */,
    0x00000446    /*  10 FREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  13 PUSH */,
    0x00000307    /*  14 FREF */,
    0x0000001b    /*  15 BNLT */,
    SG_WORD(9),
    0x0000005b    /*  17 LREF_CAR_PUSH */,
    0x00000030    /*  18 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  20 LREF_CDR_PUSH */,
    0x00000207    /*  21 FREF */,
    0x0000012c    /*  22 LOCAL_CALL */,
    0x00000037    /*  23 CONS */,
    0x0000002f    /*  24 RET */,
    0x00000005    /*  25 LREF */,
    0x00000050    /*  26 CDAR */,
    0x00000021    /*  27 BNNULL */,
    SG_WORD(5),
    0x0000005c    /*  29 LREF_CDR_PUSH */,
    0x00000207    /*  30 FREF */,
    0x0000012e    /*  31 LOCAL_TAIL_CALL */,
    0x0000002f    /*  32 RET */,
    0x00000030    /*  33 FRAME */,
    SG_WORD(4),
    0x00000045    /*  35 LREF_PUSH */,
    0x0000014a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier cddar#core.macro> */,
    0x00000021    /*  38 BNNULL */,
    SG_WORD(8),
    0x00000003    /*  40 CONST */,
    SG_WORD(SG_TRUE) /* #t */,
    0x00000108    /*  42 FSET */,
    0x0000005c    /*  43 LREF_CDR_PUSH */,
    0x00000207    /*  44 FREF */,
    0x0000012e    /*  45 LOCAL_TAIL_CALL */,
    0x0000002f    /*  46 RET */,
    0x00000030    /*  47 FRAME */,
    SG_WORD(6),
    0x00000005    /*  49 LREF */,
    0x00000050    /*  50 CDAR */,
    0x0000000b    /*  51 PUSH */,
    0x0000014a    /*  52 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier circular-list?#core.macro> */,
    0x00000017    /*  54 TEST */,
    SG_WORD(3),
    0x00000018    /*  56 JUMP */,
    SG_WORD(4),
    0x00000003    /*  58 CONST */,
    SG_WORD(SG_TRUE) /* #t */,
    0x00000008    /*  60 FSET */,
    0x00000005    /*  61 LREF */,
    0x0000004e    /*  62 CAAR */,
    0x0000000b    /*  63 PUSH */,
    0x00000030    /*  64 FRAME */,
    SG_WORD(4),
    0x00000045    /*  66 LREF_PUSH */,
    0x0000014a    /*  67 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier cddar#core.macro> */,
    0x00000054    /*  69 CONS_PUSH */,
    0x00000030    /*  70 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  72 LREF_CDR_PUSH */,
    0x00000207    /*  73 FREF */,
    0x0000012c    /*  74 LOCAL_CALL */,
    0x00000037    /*  75 CONS */,
    0x0000002f    /*  76 RET */,
    /* consume-ellipsis-vars */0x00000048    /*   0 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000048    /*   2 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000010c    /*   4 BOX */,
    0x0000000c    /*   5 BOX */,
    0x00000163    /*   6 RESV_STACK */,
    0x00000045    /*   7 LREF_PUSH */,
    0x00000145    /*   8 LREF_PUSH */,
    0x00000545    /*   9 LREF_PUSH */,
    0x00000345    /*  10 LREF_PUSH */,
    0x00000445    /*  11 LREF_PUSH */,
    0x00000329    /*  12 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[87])) /* #<code-builder loop (1 0 5)> */,
    0x00000531    /*  14 INST_STACK */,
    0x00000030    /*  15 FRAME */,
    SG_WORD(4),
    0x00000245    /*  17 LREF_PUSH */,
    0x00000505    /*  18 LREF */,
    0x0000012c    /*  19 LOCAL_CALL */,
    0x00000132    /*  20 LEAVE */,
    0x0000000b    /*  21 PUSH */,
    0x00000405    /*  22 LREF */,
    0x0000000d    /*  23 UNBOX */,
    0x00000017    /*  24 TEST */,
    SG_WORD(8),
    0x00000305    /*  26 LREF */,
    0x0000000d    /*  27 UNBOX */,
    0x00000022    /*  28 NOT */,
    0x00000017    /*  29 TEST */,
    SG_WORD(2),
    0x00000505    /*  31 LREF */,
    0x0000002f    /*  32 RET */,
    0x00000305    /*  33 LREF */,
    0x0000000d    /*  34 UNBOX */,
    0x00000017    /*  35 TEST */,
    SG_WORD(2),
    0x0000002f    /*  37 RET */,
    0x00000061    /*  38 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    /* expand-var */0x00000030    /*   0 FRAME */,
    SG_WORD(7),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x00000047    /*   4 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /*   6 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assoc#core.macro> */,
    0x0000000b    /*   8 PUSH */,
    0x00000205    /*   9 LREF */,
    0x00000017    /*  10 TEST */,
    SG_WORD(9),
    0x00000256    /*  12 LREF_CDR */,
    0x00000021    /*  13 BNNULL */,
    SG_WORD(3),
    0x00000061    /*  15 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    0x00000205    /*  17 LREF */,
    0x0000004f    /*  18 CADR */,
    0x0000002f    /*  19 RET */,
    0x00000048    /*  20 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /*  22 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms have different size of matched input (variable) */,
    0x00000048    /*  24 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* template: */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(4),
    0x00000046    /*  28 FREF_PUSH */,
    0x0000014a    /*  29 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000000b    /*  31 PUSH */,
    0x00000005    /*  32 LREF */,
    0x00000338    /*  33 LIST */,
    0x0000000b    /*  34 PUSH */,
    0x00000048    /*  35 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms: */,
    0x00000030    /*  37 FRAME */,
    SG_WORD(19),
    0x00000030    /*  39 FRAME */,
    SG_WORD(6),
    0x00000047    /*  41 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier car#core.macro> */,
    0x00000145    /*  43 LREF_PUSH */,
    0x0000024a    /*  44 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.macro> */,
    0x0000000b    /*  46 PUSH */,
    0x00000030    /*  47 FRAME */,
    SG_WORD(6),
    0x00000047    /*  49 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15855#core.macro> */,
    0x00000145    /*  51 LREF_PUSH */,
    0x0000024a    /*  52 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier map#core.macro> */,
    0x0000000b    /*  54 PUSH */,
    0x0000024a    /*  55 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier %map-cons#core.macro> */,
    0x00000254    /*  57 CONS_PUSH */,
    0x0000044b    /*  58 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  60 RET */,
    /* expand-ellipsis-template */0x00000048    /*   0 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000030    /*   2 FRAME */,
    SG_WORD(7),
    0x00000045    /*   4 LREF_PUSH */,
    0x00000246    /*   5 FREF_PUSH */,
    0x00000145    /*   6 LREF_PUSH */,
    0x00000245    /*   7 LREF_PUSH */,
    0x0000044a    /*   8 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-ellipsis-vars#core.macro> */,
    0x0000000b    /*  10 PUSH */,
    0x00000405    /*  11 LREF */,
    0x0000003e    /*  12 PAIRP */,
    0x00000017    /*  13 TEST */,
    SG_WORD(24),
    0x00000030    /*  15 FRAME */,
    SG_WORD(7),
    0x00000045    /*  17 LREF_PUSH */,
    0x00000145    /*  18 LREF_PUSH */,
    0x00000445    /*  19 LREF_PUSH */,
    0x00000107    /*  20 FREF */,
    0x0000000d    /*  21 UNBOX */,
    0x0000032c    /*  22 LOCAL_CALL */,
    0x0000000b    /*  23 PUSH */,
    0x00000305    /*  24 LREF */,
    0x00000054    /*  25 CONS_PUSH */,
    0x00000030    /*  26 FRAME */,
    SG_WORD(6),
    0x00000246    /*  28 FREF_PUSH */,
    0x00000145    /*  29 LREF_PUSH */,
    0x00000445    /*  30 LREF_PUSH */,
    0x0000034a    /*  31 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier consume-ellipsis-vars#core.macro> */,
    0x0000000b    /*  33 PUSH */,
    0x00300219    /*  34 SHIFTJ */,
    0x00000018    /*  35 JUMP */,
    SG_WORD(-25),
    0x0000002f    /*  37 RET */,
    0x00000405    /*  38 LREF */,
    0x00000021    /*  39 BNNULL */,
    SG_WORD(3),
    0x00000061    /*  41 CONST_RET */,
    SG_WORD(SG_NIL) /* () */,
    0x00000445    /*  43 LREF_PUSH */,
    0x00000003    /*  44 CONST */,
    SG_WORD(SG_TRUE) /* #t */,
    0x0000001f    /*  46 BNEQ */,
    SG_WORD(5),
    0x00000345    /*  48 LREF_PUSH */,
    0x0000014b    /*  49 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier reverse#core.macro> */,
    0x0000002f    /*  51 RET */,
    0x00000048    /*  52 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /*  54 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms have different size of matched input */,
    0x00000048    /*  56 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* template: */,
    0x00000030    /*  58 FRAME */,
    SG_WORD(4),
    0x00000046    /*  60 FREF_PUSH */,
    0x0000014a    /*  61 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x00000238    /*  63 LIST */,
    0x0000000b    /*  64 PUSH */,
    0x00000048    /*  65 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms: */,
    0x00000030    /*  67 FRAME */,
    SG_WORD(4),
    0x00000245    /*  69 LREF_PUSH */,
    0x0000014a    /*  70 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x00000254    /*  72 CONS_PUSH */,
    0x0000044b    /*  73 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x0000002f    /*  75 RET */,
    /* expand-escaped-template */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(18),
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000246    /*  10 FREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  13 PUSH */,
    0x00000004    /*  14 CONSTI */,
    0x0000001b    /*  15 BNLT */,
    SG_WORD(3),
    0x00000005    /*  17 LREF */,
    0x0000002f    /*  18 RET */,
    0x00000045    /*  19 LREF_PUSH */,
    0x00000245    /*  20 LREF_PUSH */,
    0x00000107    /*  21 FREF */,
    0x0000022e    /*  22 LOCAL_TAIL_CALL */,
    0x0000002f    /*  23 RET */,
    0x00000005    /*  24 LREF */,
    0x0000003e    /*  25 PAIRP */,
    0x00000017    /*  26 TEST */,
    SG_WORD(18),
    0x00000030    /*  28 FRAME */,
    SG_WORD(6),
    0x0000005b    /*  30 LREF_CAR_PUSH */,
    0x00000145    /*  31 LREF_PUSH */,
    0x00000245    /*  32 LREF_PUSH */,
    0x00000007    /*  33 FREF */,
    0x0000032c    /*  34 LOCAL_CALL */,
    0x0000000b    /*  35 PUSH */,
    0x00000030    /*  36 FRAME */,
    SG_WORD(6),
    0x0000005c    /*  38 LREF_CDR_PUSH */,
    0x00000145    /*  39 LREF_PUSH */,
    0x00000245    /*  40 LREF_PUSH */,
    0x00000007    /*  41 FREF */,
    0x0000032c    /*  42 LOCAL_CALL */,
    0x00000037    /*  43 CONS */,
    0x0000002f    /*  44 RET */,
    0x00000005    /*  45 LREF */,
    0x00000041    /*  46 VECTORP */,
    0x00000017    /*  47 TEST */,
    SG_WORD(17),
    0x00000030    /*  49 FRAME */,
    SG_WORD(11),
    0x00000030    /*  51 FRAME */,
    SG_WORD(4),
    0x00000045    /*  53 LREF_PUSH */,
    0x0000014a    /*  54 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /*  56 PUSH */,
    0x00000145    /*  57 LREF_PUSH */,
    0x00000245    /*  58 LREF_PUSH */,
    0x00000007    /*  59 FREF */,
    0x0000032c    /*  60 LOCAL_CALL */,
    0x0000000b    /*  61 PUSH */,
    0x0000014b    /*  62 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list->vector#core.macro> */,
    0x0000002f    /*  64 RET */,
    0x00000005    /*  65 LREF */,
    0x0000002f    /*  66 RET */,
    /* expand-template */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(18),
    0x00000030    /*   7 FRAME */,
    SG_WORD(5),
    0x00000045    /*   9 LREF_PUSH */,
    0x00000546    /*  10 FREF_PUSH */,
    0x0000024a    /*  11 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /*  13 PUSH */,
    0x00000004    /*  14 CONSTI */,
    0x0000001b    /*  15 BNLT */,
    SG_WORD(3),
    0x00000005    /*  17 LREF */,
    0x0000002f    /*  18 RET */,
    0x00000045    /*  19 LREF_PUSH */,
    0x00000245    /*  20 LREF_PUSH */,
    0x00000407    /*  21 FREF */,
    0x0000022e    /*  22 LOCAL_TAIL_CALL */,
    0x0000002f    /*  23 RET */,
    0x00000005    /*  24 LREF */,
    0x0000003e    /*  25 PAIRP */,
    0x00000017    /*  26 TEST */,
    SG_WORD(288),
    0x00000030    /*  28 FRAME */,
    SG_WORD(4),
    0x0000005b    /*  30 LREF_CAR_PUSH */,
    0x0000014a    /*  31 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  33 TEST */,
    SG_WORD(278),
    0x00000056    /*  35 LREF_CDR */,
    0x0000003e    /*  36 PAIRP */,
    0x00000017    /*  37 TEST */,
    SG_WORD(271),
    0x00000005    /*  39 LREF */,
    0x00000051    /*  40 CDDR */,
    0x00000021    /*  41 BNNULL */,
    SG_WORD(9),
    0x00000005    /*  43 LREF */,
    0x0000004f    /*  44 CADR */,
    0x0000000b    /*  45 PUSH */,
    0x00000145    /*  46 LREF_PUSH */,
    0x00000245    /*  47 LREF_PUSH */,
    0x00000307    /*  48 FREF */,
    0x0000032e    /*  49 LOCAL_TAIL_CALL */,
    0x0000002f    /*  50 RET */,
    0x00000005    /*  51 LREF */,
    0x0000003e    /*  52 PAIRP */,
    0x00000017    /*  53 TEST */,
    SG_WORD(252),
    0x00000056    /*  55 LREF_CDR */,
    0x0000003e    /*  56 PAIRP */,
    0x00000017    /*  57 TEST */,
    SG_WORD(245),
    0x00000030    /*  59 FRAME */,
    SG_WORD(6),
    0x00000005    /*  61 LREF */,
    0x0000004f    /*  62 CADR */,
    0x0000000b    /*  63 PUSH */,
    0x0000014a    /*  64 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  66 TEST */,
    SG_WORD(233),
    0x00000005    /*  68 LREF */,
    0x00000051    /*  69 CDDR */,
    0x0000003e    /*  70 PAIRP */,
    0x00000017    /*  71 TEST */,
    SG_WORD(225),
    0x00000030    /*  73 FRAME */,
    SG_WORD(9),
    0x00000030    /*  75 FRAME */,
    SG_WORD(4),
    0x00000045    /*  77 LREF_PUSH */,
    0x0000014a    /*  78 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier caddr#core.macro> */,
    0x0000000b    /*  80 PUSH */,
    0x0000014a    /*  81 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  83 TEST */,
    SG_WORD(32),
    0x00000030    /*  85 FRAME */,
    SG_WORD(4),
    0x00000045    /*  87 LREF_PUSH */,
    0x0000014a    /*  88 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000328    /*  90 RECEIVE */,
    0x00000030    /*  91 FRAME */,
    SG_WORD(13),
    0x00000047    /*  93 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier append#core.macro> */,
    0x00000030    /*  95 FRAME */,
    SG_WORD(8),
    0x00000345    /*  97 LREF_PUSH */,
    0x00000105    /*  98 LREF */,
    0x0000010f    /*  99 ADDI */,
    0x0000000b    /* 100 PUSH */,
    0x00000245    /* 101 LREF_PUSH */,
    0x00000207    /* 102 FREF */,
    0x0000032c    /* 103 LOCAL_CALL */,
    0x0000022a    /* 104 APPLY */,
    0x0000000b    /* 105 PUSH */,
    0x00000030    /* 106 FRAME */,
    SG_WORD(7),
    0x00000445    /* 108 LREF_PUSH */,
    0x00000145    /* 109 LREF_PUSH */,
    0x00000245    /* 110 LREF_PUSH */,
    0x00000107    /* 111 FREF */,
    0x0000000d    /* 112 UNBOX */,
    0x0000032c    /* 113 LOCAL_CALL */,
    0x00000239    /* 114 APPEND */,
    0x0000002f    /* 115 RET */,
    0x00000005    /* 116 LREF */,
    0x0000003e    /* 117 PAIRP */,
    0x00000017    /* 118 TEST */,
    SG_WORD(175),
    0x00000056    /* 120 LREF_CDR */,
    0x0000003e    /* 121 PAIRP */,
    0x00000017    /* 122 TEST */,
    SG_WORD(168),
    0x00000030    /* 124 FRAME */,
    SG_WORD(6),
    0x00000005    /* 126 LREF */,
    0x0000004f    /* 127 CADR */,
    0x0000000b    /* 128 PUSH */,
    0x0000014a    /* 129 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /* 131 TEST */,
    SG_WORD(113),
    0x00000030    /* 133 FRAME */,
    SG_WORD(4),
    0x0000005b    /* 135 LREF_CAR_PUSH */,
    0x0000014a    /* 136 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /* 138 TEST */,
    SG_WORD(78),
    0x00000030    /* 140 FRAME */,
    SG_WORD(5),
    0x0000005b    /* 142 LREF_CAR_PUSH */,
    0x00000546    /* 143 FREF_PUSH */,
    0x0000024a    /* 144 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x0000000b    /* 146 PUSH */,
    0x00000345    /* 147 LREF_PUSH */,
    0x00000105    /* 148 LREF */,
    0x0000010f    /* 149 ADDI */,
    0x0000001a    /* 150 BNNUME */,
    SG_WORD(64),
    0x0000005b    /* 152 LREF_CAR_PUSH */,
    0x00000030    /* 153 FRAME */,
    SG_WORD(7),
    0x00000445    /* 155 LREF_PUSH */,
    0x00000245    /* 156 LREF_PUSH */,
    0x00000047    /* 157 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier bound-identifier=?#core.macro> */,
    0x0000034a    /* 159 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assoc#core.macro> */,
    0x0000000b    /* 161 PUSH */,
    0x00000505    /* 162 LREF */,
    0x00000017    /* 163 TEST */,
    SG_WORD(12),
    0x00000556    /* 165 LREF_CDR */,
    0x00000021    /* 166 BNNULL */,
    SG_WORD(5),
    0x00000003    /* 168 CONST */,
    SG_WORD(SG_NIL) /* () */,
    0x00000018    /* 170 JUMP */,
    SG_WORD(3),
    0x00000505    /* 172 LREF */,
    0x0000004f    /* 173 CADR */,
    0x00000018    /* 174 JUMP */,
    SG_WORD(26),
    0x00000030    /* 176 FRAME */,
    SG_WORD(24),
    0x00000048    /* 178 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax template */,
    0x00000048    /* 180 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms have different size of matched input (ellipsis) */,
    0x00000048    /* 182 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* template: */,
    0x00000030    /* 184 FRAME */,
    SG_WORD(4),
    0x00000046    /* 186 FREF_PUSH */,
    0x0000014a    /* 187 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x00000238    /* 189 LIST */,
    0x0000000b    /* 190 PUSH */,
    0x00000048    /* 191 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* subforms: */,
    0x00000030    /* 193 FRAME */,
    SG_WORD(4),
    0x00000245    /* 195 LREF_PUSH */,
    0x0000014a    /* 196 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x00000254    /* 198 CONS_PUSH */,
    0x0000044a    /* 199 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-violation#core.macro> */,
    0x00000232    /* 201 LEAVE */,
    0x0000000b    /* 202 PUSH */,
    0x00000030    /* 203 FRAME */,
    SG_WORD(9),
    0x00000005    /* 205 LREF */,
    0x00000051    /* 206 CDDR */,
    0x0000000b    /* 207 PUSH */,
    0x00000145    /* 208 LREF_PUSH */,
    0x00000245    /* 209 LREF_PUSH */,
    0x00000107    /* 210 FREF */,
    0x0000000d    /* 211 UNBOX */,
    0x0000032c    /* 212 LOCAL_CALL */,
    0x00000239    /* 213 APPEND */,
    0x0000002f    /* 214 RET */,
    0x00000002    /* 215 UNDEF */,
    0x0000002f    /* 216 RET */,
    0x00000055    /* 217 LREF_CAR */,
    0x0000003e    /* 218 PAIRP */,
    0x00000017    /* 219 TEST */,
    SG_WORD(23),
    0x00000030    /* 221 FRAME */,
    SG_WORD(8),
    0x0000005b    /* 223 LREF_CAR_PUSH */,
    0x00000105    /* 224 LREF */,
    0x0000010f    /* 225 ADDI */,
    0x0000000b    /* 226 PUSH */,
    0x00000245    /* 227 LREF_PUSH */,
    0x00000207    /* 228 FREF */,
    0x0000032c    /* 229 LOCAL_CALL */,
    0x0000000b    /* 230 PUSH */,
    0x00000030    /* 231 FRAME */,
    SG_WORD(9),
    0x00000005    /* 233 LREF */,
    0x00000051    /* 234 CDDR */,
    0x0000000b    /* 235 PUSH */,
    0x00000145    /* 236 LREF_PUSH */,
    0x00000245    /* 237 LREF_PUSH */,
    0x00000107    /* 238 FREF */,
    0x0000000d    /* 239 UNBOX */,
    0x0000032c    /* 240 LOCAL_CALL */,
    0x00000239    /* 241 APPEND */,
    0x0000002f    /* 242 RET */,
    0x00000002    /* 243 UNDEF */,
    0x0000002f    /* 244 RET */,
    0x00000005    /* 245 LREF */,
    0x0000003e    /* 246 PAIRP */,
    0x00000017    /* 247 TEST */,
    SG_WORD(20),
    0x00000030    /* 249 FRAME */,
    SG_WORD(7),
    0x0000005b    /* 251 LREF_CAR_PUSH */,
    0x00000145    /* 252 LREF_PUSH */,
    0x00000245    /* 253 LREF_PUSH */,
    0x00000107    /* 254 FREF */,
    0x0000000d    /* 255 UNBOX */,
    0x0000032c    /* 256 LOCAL_CALL */,
    0x0000000b    /* 257 PUSH */,
    0x00000030    /* 258 FRAME */,
    SG_WORD(7),
    0x0000005c    /* 260 LREF_CDR_PUSH */,
    0x00000145    /* 261 LREF_PUSH */,
    0x00000245    /* 262 LREF_PUSH */,
    0x00000107    /* 263 FREF */,
    0x0000000d    /* 264 UNBOX */,
    0x0000032c    /* 265 LOCAL_CALL */,
    0x00000037    /* 266 CONS */,
    0x0000002f    /* 267 RET */,
    0x00000005    /* 268 LREF */,
    0x00000041    /* 269 VECTORP */,
    0x00000017    /* 270 TEST */,
    SG_WORD(18),
    0x00000030    /* 272 FRAME */,
    SG_WORD(12),
    0x00000030    /* 274 FRAME */,
    SG_WORD(4),
    0x00000045    /* 276 LREF_PUSH */,
    0x0000014a    /* 277 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector->list#core.macro> */,
    0x0000000b    /* 279 PUSH */,
    0x00000145    /* 280 LREF_PUSH */,
    0x00000245    /* 281 LREF_PUSH */,
    0x00000107    /* 282 FREF */,
    0x0000000d    /* 283 UNBOX */,
    0x0000032c    /* 284 LOCAL_CALL */,
    0x0000000b    /* 285 PUSH */,
    0x0000014b    /* 286 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list->vector#core.macro> */,
    0x0000002f    /* 288 RET */,
    0x00000005    /* 289 LREF */,
    0x0000002f    /* 290 RET */,
    0x00000018    /* 291 JUMP */,
    SG_WORD(-47),
    0x0000002f    /* 293 RET */,
    0x00000018    /* 294 JUMP */,
    SG_WORD(-50),
    0x0000002f    /* 296 RET */,
    0x00000018    /* 297 JUMP */,
    SG_WORD(-182),
    0x0000002f    /* 299 RET */,
    0x00000018    /* 300 JUMP */,
    SG_WORD(-185),
    0x0000002f    /* 302 RET */,
    0x00000018    /* 303 JUMP */,
    SG_WORD(-188),
    0x0000002f    /* 305 RET */,
    0x00000018    /* 306 JUMP */,
    SG_WORD(-191),
    0x0000002f    /* 308 RET */,
    0x00000018    /* 309 JUMP */,
    SG_WORD(-259),
    0x0000002f    /* 311 RET */,
    0x00000018    /* 312 JUMP */,
    SG_WORD(-262),
    0x0000002f    /* 314 RET */,
    0x00000018    /* 315 JUMP */,
    SG_WORD(-265),
    0x0000002f    /* 317 RET */,
    /* transcribe-template */0x00000030    /*   0 FRAME */,
    SG_WORD(6),
    0x00000045    /*   2 LREF_PUSH */,
    0x00000145    /*   3 LREF_PUSH */,
    0x00000245    /*   4 LREF_PUSH */,
    0x0000034a    /*   5 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier adapt-to-rank-moved-vars#core.macro> */,
    0x00000328    /*   7 RECEIVE */,
    0x00000463    /*   8 RESV_STACK */,
    0x0000000c    /*   9 BOX */,
    0x00000045    /*  10 LREF_PUSH */,
    0x00000029    /*  11 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[89])) /* #<code-builder expand-var (2 0 1)> */,
    0x00000631    /*  13 INST_STACK */,
    0x00000445    /*  14 LREF_PUSH */,
    0x00000945    /*  15 LREF_PUSH */,
    0x00000045    /*  16 LREF_PUSH */,
    0x00000029    /*  17 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[90])) /* #<code-builder expand-ellipsis-template (3 0 3)> */,
    0x00000731    /*  19 INST_STACK */,
    0x00000445    /*  20 LREF_PUSH */,
    0x00000645    /*  21 LREF_PUSH */,
    0x00000845    /*  22 LREF_PUSH */,
    0x00000129    /*  23 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[91])) /* #<code-builder expand-escaped-template (3 0 3)> */,
    0x00000831    /*  25 INST_STACK */,
    0x00000445    /*  26 LREF_PUSH */,
    0x00000645    /*  27 LREF_PUSH */,
    0x00000845    /*  28 LREF_PUSH */,
    0x00000745    /*  29 LREF_PUSH */,
    0x00000945    /*  30 LREF_PUSH */,
    0x00000045    /*  31 LREF_PUSH */,
    0x00000029    /*  32 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[92])) /* #<code-builder expand-template (3 0 6)> */,
    0x00000906    /*  34 LSET */,
    0x00000030    /*  35 FRAME */,
    SG_WORD(4),
    0x00000345    /*  37 LREF_PUSH */,
    0x0000014a    /*  38 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier length#core.macro> */,
    0x0000000b    /*  40 PUSH */,
    0x00000204    /*  41 CONSTI */,
    0x0000001a    /*  42 BNNUME */,
    SG_WORD(23),
    0x00000030    /*  44 FRAME */,
    SG_WORD(4),
    0x0000035b    /*  46 LREF_CAR_PUSH */,
    0x0000014a    /*  47 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000017    /*  49 TEST */,
    SG_WORD(9),
    0x00000305    /*  51 LREF */,
    0x0000004f    /*  52 CADR */,
    0x0000000b    /*  53 PUSH */,
    0x00000049    /*  54 CONSTI_PUSH */,
    0x00000545    /*  55 LREF_PUSH */,
    0x00000805    /*  56 LREF */,
    0x0000032e    /*  57 LOCAL_TAIL_CALL */,
    0x0000002f    /*  58 RET */,
    0x00000345    /*  59 LREF_PUSH */,
    0x00000049    /*  60 CONSTI_PUSH */,
    0x00000545    /*  61 LREF_PUSH */,
    0x00000905    /*  62 LREF */,
    0x0000000d    /*  63 UNBOX */,
    0x0000032e    /*  64 LOCAL_TAIL_CALL */,
    0x0000002f    /*  65 RET */,
    0x00000018    /*  66 JUMP */,
    SG_WORD(-8),
    0x0000002f    /*  68 RET */,
    /* #f */0x00000045    /*   0 LREF_PUSH */,
    0x00000046    /*   1 FREF_PUSH */,
    0x0000024b    /*   2 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier copy-identifier#core.macro> */,
    0x0000002f    /*   4 RET */,
    /* datum->syntax */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier identifier?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000018    /*   7 JUMP */,
    SG_WORD(15),
    0x00000030    /*   9 FRAME */,
    SG_WORD(13),
    0x00000048    /*  11 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* datum->syntax */,
    0x00000030    /*  13 FRAME */,
    SG_WORD(6),
    0x00000048    /*  15 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* expected identifier, but got ~s */,
    0x00000045    /*  17 LREF_PUSH */,
    0x0000024a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier format#core.macro> */,
    0x0000000b    /*  20 PUSH */,
    0x0000024a    /*  21 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.macro> */,
    0x00000145    /*  23 LREF_PUSH */,
    0x00000030    /*  24 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  26 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x0000000b    /*  28 PUSH */,
    0x00000048    /*  29 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /*  31 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000045    /*  33 LREF_PUSH */,
    0x00000029    /*  34 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[94])) /* #<code-builder #f (3 0 1)> */,
    0x0000000b    /*  36 PUSH */,
    0x00000047    /*  37 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15854#core.macro> */,
    0x0000064b    /*  39 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x0000002f    /*  41 RET */,
    /* syntax->datum */0x00000045    /*   0 LREF_PUSH */,
    0x0000014b    /*   1 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier unwrap-syntax#core.macro> */,
    0x0000002f    /*   3 RET */,
    /* generate-temporaries */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier list?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(3),
    0x00000018    /*   7 JUMP */,
    SG_WORD(15),
    0x00000030    /*   9 FRAME */,
    SG_WORD(13),
    0x00000048    /*  11 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* generate-temporaries */,
    0x00000030    /*  13 FRAME */,
    SG_WORD(6),
    0x00000048    /*  15 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* expected list, but got ~s */,
    0x00000045    /*  17 LREF_PUSH */,
    0x0000024a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier format#core.macro> */,
    0x0000000b    /*  20 PUSH */,
    0x0000024a    /*  21 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.macro> */,
    0x00000030    /*  23 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  25 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vm-current-library#core.macro> */,
    0x0000000b    /*  27 PUSH */,
    0x00000049    /*  28 CONSTI_PUSH */,
    0x00000045    /*  29 LREF_PUSH */,
    0x00000048    /*  30 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000305    /*  32 LREF */,
    0x00000021    /*  33 BNNULL */,
    SG_WORD(3),
    0x00000405    /*  35 LREF */,
    0x0000002f    /*  36 RET */,
    0x00000205    /*  37 LREF */,
    0x0000010f    /*  38 ADDI */,
    0x0000000b    /*  39 PUSH */,
    0x0000035c    /*  40 LREF_CDR_PUSH */,
    0x00000030    /*  41 FRAME */,
    SG_WORD(37),
    0x00000030    /*  43 FRAME */,
    SG_WORD(29),
    0x00000030    /*  45 FRAME */,
    SG_WORD(24),
    0x00000048    /*  47 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ~a.~a'~a */,
    0x00000030    /*  49 FRAME */,
    SG_WORD(5),
    0x00000048    /*  51 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* temp. */,
    0x0000014a    /*  53 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier gensym#core.macro> */,
    0x0000000b    /*  55 PUSH */,
    0x00000030    /*  56 FRAME */,
    SG_WORD(9),
    0x00000030    /*  58 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  60 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier microsecond#core.macro> */,
    0x0000000b    /*  62 PUSH */,
    0x00002049    /*  63 CONSTI_PUSH */,
    0x0000024a    /*  64 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier number->string#core.macro> */,
    0x0000000b    /*  66 PUSH */,
    0x00000245    /*  67 LREF_PUSH */,
    0x0000044a    /*  68 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier format#core.macro> */,
    0x0000000b    /*  70 PUSH */,
    0x0000014a    /*  71 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier string->symbol#core.macro> */,
    0x0000000b    /*  73 PUSH */,
    0x00000048    /*  74 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000145    /*  76 LREF_PUSH */,
    0x0000034a    /*  77 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x0000000b    /*  79 PUSH */,
    0x00000405    /*  80 LREF */,
    0x00000054    /*  81 CONS_PUSH */,
    0x00200319    /*  82 SHIFTJ */,
    0x00000018    /*  83 JUMP */,
    SG_WORD(-52),
    0x0000002f    /*  85 RET */,
    /* variable-transformer? */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(9),
    0x00000030    /*   7 FRAME */,
    SG_WORD(4),
    0x00000045    /*   9 LREF_PUSH */,
    0x0000014a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-data#core.macro> */,
    0x0000000b    /*  12 PUSH */,
    0x0000014b    /*  13 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro?#core.macro> */,
    0x0000002f    /*  15 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000246    /*   2 FREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro?#core.macro> */,
    0x00000017    /*   5 TEST */,
    SG_WORD(22),
    0x00000246    /*   7 FREF_PUSH */,
    0x00000146    /*   8 FREF_PUSH */,
    0x00000030    /*   9 FRAME */,
    SG_WORD(4),
    0x00000046    /*  11 FREF_PUSH */,
    0x0000014a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-env#core.macro> */,
    0x0000000b    /*  14 PUSH */,
    0x00000030    /*  15 FRAME */,
    SG_WORD(4),
    0x00000246    /*  17 FREF_PUSH */,
    0x0000014a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-data#core.macro> */,
    0x0000000b    /*  20 PUSH */,
    0x00000030    /*  21 FRAME */,
    SG_WORD(4),
    0x00000246    /*  23 FREF_PUSH */,
    0x0000014a    /*  24 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-transformer#core.macro> */,
    0x0000042d    /*  26 TAIL_CALL */,
    0x0000002f    /*  27 RET */,
    0x00000146    /*  28 FREF_PUSH */,
    0x00000207    /*  29 FREF */,
    0x0000012d    /*  30 TAIL_CALL */,
    0x0000002f    /*  31 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(5),
    0x00000048    /*   2 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000014a    /*   4 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x00000030    /*   6 FRAME */,
    SG_WORD(4),
    0x00000246    /*   8 FREF_PUSH */,
    0x0000014a    /*   9 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000030    /*  11 FRAME */,
    SG_WORD(4),
    0x00000146    /*  13 FREF_PUSH */,
    0x0000014a    /*  14 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000046    /*  16 FREF_PUSH */,
    0x0000014b    /*  17 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x0000002f    /*  19 RET */,
    /* macro-transform */0x00000030    /*   0 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   2 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x0000000b    /*   4 PUSH */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x0000000b    /*   9 PUSH */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  12 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x0000000b    /*  14 PUSH */,
    0x00000030    /*  15 FRAME */,
    SG_WORD(4),
    0x00000245    /*  17 LREF_PUSH */,
    0x0000014a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000030    /*  20 FRAME */,
    SG_WORD(9),
    0x00000030    /*  22 FRAME */,
    SG_WORD(4),
    0x00000045    /*  24 LREF_PUSH */,
    0x0000014a    /*  25 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-env#core.macro> */,
    0x0000000b    /*  27 PUSH */,
    0x0000014a    /*  28 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000030    /*  30 FRAME */,
    SG_WORD(10),
    0x00000030    /*  32 FRAME */,
    SG_WORD(5),
    0x00000048    /*  34 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* id. */,
    0x0000014a    /*  36 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier gensym#core.macro> */,
    0x0000000b    /*  38 PUSH */,
    0x0000014a    /*  39 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x00000030    /*  41 FRAME */,
    SG_WORD(5),
    0x00000048    /*  43 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000014a    /*  45 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x00000047    /*  47 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier values#core.macro> */,
    0x00000345    /*  49 LREF_PUSH */,
    0x00000145    /*  50 LREF_PUSH */,
    0x00000045    /*  51 LREF_PUSH */,
    0x00000029    /*  52 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[99])) /* #<code-builder #f (0 0 3)> */,
    0x0000000b    /*  54 PUSH */,
    0x00000445    /*  55 LREF_PUSH */,
    0x00000545    /*  56 LREF_PUSH */,
    0x00000645    /*  57 LREF_PUSH */,
    0x00000029    /*  58 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[100])) /* #<code-builder #f (0 0 3)> */,
    0x0000000b    /*  60 PUSH */,
    0x0000034b    /*  61 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier dynamic-wind#core.macro> */,
    0x0000002f    /*  63 RET */,
    /* make-macro-transformer */0x00000045    /*   0 LREF_PUSH */,
    0x00000047    /*   1 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-transform#core.macro> */,
    0x00000030    /*   3 FRAME */,
    SG_WORD(3),
    0x00000105    /*   5 LREF */,
    0x0000002b    /*   6 CALL */,
    0x0000000b    /*   7 PUSH */,
    0x00000245    /*   8 LREF_PUSH */,
    0x00000345    /*   9 LREF_PUSH */,
    0x0000054b    /*  10 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-macro#core.macro> */,
    0x0000002f    /*  12 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(4),
    0x00000045    /*   2 LREF_PUSH */,
    0x0000014a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier pending-identifier?#core.macro> */,
    0x00000022    /*   5 NOT */,
    0x00000017    /*   6 TEST */,
    SG_WORD(14),
    0x00000030    /*   8 FRAME */,
    SG_WORD(11),
    0x00000030    /*  10 FRAME */,
    SG_WORD(6),
    0x00000046    /*  12 FREF_PUSH */,
    0x00000045    /*  13 LREF_PUSH */,
    0x00000249    /*  14 CONSTI_PUSH */,
    0x0000034a    /*  15 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-lookup#core.macro> */,
    0x0000000b    /*  17 PUSH */,
    0x0000014a    /*  18 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier number?#core.macro> */,
    0x00000022    /*  20 NOT */,
    0x0000002f    /*  21 RET */,
    /* #f */0x00000030    /*   0 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   2 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x0000000b    /*   4 PUSH */,
    0x00000030    /*   5 FRAME */,
    SG_WORD(23),
    0x00000145    /*   7 LREF_PUSH */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x0000000b    /*  12 PUSH */,
    0x00000445    /*  13 LREF_PUSH */,
    0x00000104    /*  14 CONSTI */,
    0x00000043    /*  15 VEC_REF */,
    0x0000000b    /*  16 PUSH */,
    0x00000445    /*  17 LREF_PUSH */,
    0x00000004    /*  18 CONSTI */,
    0x00000043    /*  19 VEC_REF */,
    0x0000000b    /*  20 PUSH */,
    0x00000047    /*  21 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15853#core.macro> */,
    0x00000245    /*  23 LREF_PUSH */,
    0x00000029    /*  24 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[103])) /* #<code-builder #f (1 0 1)> */,
    0x0000000b    /*  26 PUSH */,
    0x0000064a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x00000132    /*  29 LEAVE */,
    0x0000000b    /*  30 PUSH */,
    0x00000007    /*  31 FREF */,
    0x0000012d    /*  32 TAIL_CALL */,
    0x0000002f    /*  33 RET */,
    /* make-variable-transformer */0x00000047    /*   0 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier *variable-transformer-mark*#core.macro> */,
    0x00000045    /*   2 LREF_PUSH */,
    0x00000029    /*   3 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[104])) /* #<code-builder #f (4 0 1)> */,
    0x0000000b    /*   5 PUSH */,
    0x00000048    /*   6 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000030    /*   8 FRAME */,
    SG_WORD(3),
    0x0000004a    /*  10 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x0000000b    /*  12 PUSH */,
    0x0000044b    /*  13 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-macro#core.macro> */,
    0x0000002f    /*  15 RET */,
    /* er-rename */0x00000163    /*   0 RESV_STACK */,
    0x00000009    /*   1 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15852#core.macro> */,
    0x00000331    /*   3 INST_STACK */,
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x00000045    /*   6 LREF_PUSH */,
    0x0000014a    /*   7 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /*   9 TEST */,
    SG_WORD(4),
    0x00000002    /*  11 UNDEF */,
    0x00000018    /*  12 JUMP */,
    SG_WORD(16),
    0x00000030    /*  14 FRAME */,
    SG_WORD(14),
    0x00000048    /*  16 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* er-macro-transformer */,
    0x00000030    /*  18 FRAME */,
    SG_WORD(7),
    0x00000048    /*  20 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* a symbol or an identifier */,
    0x00000045    /*  22 LREF_PUSH */,
    0x00000049    /*  23 CONSTI_PUSH */,
    0x0000034a    /*  24 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier wrong-type-argument-message#core.macro> */,
    0x0000000b    /*  26 PUSH */,
    0x0000024a    /*  27 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier assertion-violation#core.macro> */,
    0x00000005    /*  29 LREF */,
    0x0000003f    /*  30 SYMBOLP */,
    0x00000017    /*  31 TEST */,
    SG_WORD(32),
    0x00000205    /*  33 LREF */,
    0x00000017    /*  34 TEST */,
    SG_WORD(26),
    0x00000030    /*  36 FRAME */,
    SG_WORD(7),
    0x00000245    /*  38 LREF_PUSH */,
    0x00000045    /*  39 LREF_PUSH */,
    0x00000048    /*  40 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*  42 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-ref#core.macro> */,
    0x00000017    /*  44 TEST */,
    SG_WORD(2),
    0x0000002f    /*  46 RET */,
    0x00000045    /*  47 LREF_PUSH */,
    0x00000245    /*  48 LREF_PUSH */,
    0x00000145    /*  49 LREF_PUSH */,
    0x00000104    /*  50 CONSTI */,
    0x00000043    /*  51 VEC_REF */,
    0x0000000b    /*  52 PUSH */,
    0x00000145    /*  53 LREF_PUSH */,
    0x00000004    /*  54 CONSTI */,
    0x00000043    /*  55 VEC_REF */,
    0x0000000b    /*  56 PUSH */,
    0x00000009    /*  57 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15852#core.macro> */,
    0x0000042e    /*  59 LOCAL_TAIL_CALL */,
    0x0000002f    /*  60 RET */,
    0x00000018    /*  61 JUMP */,
    SG_WORD(-15),
    0x0000002f    /*  63 RET */,
    0x00000030    /*  64 FRAME */,
    SG_WORD(4),
    0x00000045    /*  66 LREF_PUSH */,
    0x0000014a    /*  67 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier pending-identifier?#core.macro> */,
    0x00000017    /*  69 TEST */,
    SG_WORD(3),
    0x00000005    /*  71 LREF */,
    0x0000002f    /*  72 RET */,
    0x00000205    /*  73 LREF */,
    0x00000017    /*  74 TEST */,
    SG_WORD(32),
    0x00000030    /*  76 FRAME */,
    SG_WORD(7),
    0x00000245    /*  78 LREF_PUSH */,
    0x00000045    /*  79 LREF_PUSH */,
    0x00000048    /*  80 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000034a    /*  82 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier hashtable-ref#core.macro> */,
    0x00000017    /*  84 TEST */,
    SG_WORD(3),
    0x00000018    /*  86 JUMP */,
    SG_WORD(18),
    0x00000030    /*  88 FRAME */,
    SG_WORD(16),
    0x00000045    /*  90 LREF_PUSH */,
    0x00000245    /*  91 LREF_PUSH */,
    0x00000145    /*  92 LREF_PUSH */,
    0x00000104    /*  93 CONSTI */,
    0x00000043    /*  94 VEC_REF */,
    0x0000000b    /*  95 PUSH */,
    0x00000030    /*  96 FRAME */,
    SG_WORD(4),
    0x00000045    /*  98 LREF_PUSH */,
    0x0000014a    /*  99 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier id-library#core.macro> */,
    0x0000000b    /* 101 PUSH */,
    0x00000009    /* 102 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15852#core.macro> */,
    0x0000042c    /* 104 LOCAL_CALL */,
    0x00000018    /* 105 JUMP */,
    SG_WORD(3),
    0x00000018    /* 107 JUMP */,
    SG_WORD(-20),
    0x00000017    /* 109 TEST */,
    SG_WORD(2),
    0x0000002f    /* 111 RET */,
    0x00000005    /* 112 LREF */,
    0x0000002f    /* 113 RET */,
    /* rec */0x00000005    /*   0 LREF */,
    0x0000003e    /*   1 PAIRP */,
    0x00000017    /*   2 TEST */,
    SG_WORD(30),
    0x00000030    /*   4 FRAME */,
    SG_WORD(4),
    0x0000005b    /*   6 LREF_CAR_PUSH */,
    0x00000207    /*   7 FREF */,
    0x0000012c    /*   8 LOCAL_CALL */,
    0x0000000b    /*   9 PUSH */,
    0x00000030    /*  10 FRAME */,
    SG_WORD(4),
    0x0000005c    /*  12 LREF_CDR_PUSH */,
    0x00000207    /*  13 FREF */,
    0x0000012c    /*  14 LOCAL_CALL */,
    0x0000000b    /*  15 PUSH */,
    0x00000145    /*  16 LREF_PUSH */,
    0x00000055    /*  17 LREF_CAR */,
    0x0000001f    /*  18 BNEQ */,
    SG_WORD(11),
    0x00000245    /*  20 LREF_PUSH */,
    0x00000056    /*  21 LREF_CDR */,
    0x0000001f    /*  22 BNEQ */,
    SG_WORD(3),
    0x00000005    /*  24 LREF */,
    0x0000002f    /*  25 RET */,
    0x00000145    /*  26 LREF_PUSH */,
    0x00000205    /*  27 LREF */,
    0x00000037    /*  28 CONS */,
    0x0000002f    /*  29 RET */,
    0x00000018    /*  30 JUMP */,
    SG_WORD(-5),
    0x0000002f    /*  32 RET */,
    0x00000005    /*  33 LREF */,
    0x00000041    /*  34 VECTORP */,
    0x00000017    /*  35 TEST */,
    SG_WORD(63),
    0x00000005    /*  37 LREF */,
    0x00000042    /*  38 VEC_LEN */,
    0x0000000b    /*  39 PUSH */,
    0x00000049    /*  40 CONSTI_PUSH */,
    0x00000048    /*  41 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000245    /*  43 LREF_PUSH */,
    0x00000105    /*  44 LREF */,
    0x0000001a    /*  45 BNNUME */,
    SG_WORD(7),
    0x00000305    /*  47 LREF */,
    0x00000017    /*  48 TEST */,
    SG_WORD(2),
    0x0000002f    /*  50 RET */,
    0x00000005    /*  51 LREF */,
    0x0000002f    /*  52 RET */,
    0x00000030    /*  53 FRAME */,
    SG_WORD(7),
    0x00000045    /*  55 LREF_PUSH */,
    0x00000205    /*  56 LREF */,
    0x00000043    /*  57 VEC_REF */,
    0x0000000b    /*  58 PUSH */,
    0x00000207    /*  59 FREF */,
    0x0000012c    /*  60 LOCAL_CALL */,
    0x0000000b    /*  61 PUSH */,
    0x00000445    /*  62 LREF_PUSH */,
    0x00000045    /*  63 LREF_PUSH */,
    0x00000205    /*  64 LREF */,
    0x00000043    /*  65 VEC_REF */,
    0x0000001f    /*  66 BNEQ */,
    SG_WORD(9),
    0x00000205    /*  68 LREF */,
    0x0000010f    /*  69 ADDI */,
    0x0000000b    /*  70 PUSH */,
    0x00000345    /*  71 LREF_PUSH */,
    0x00200219    /*  72 SHIFTJ */,
    0x00000018    /*  73 JUMP */,
    SG_WORD(-31),
    0x0000002f    /*  75 RET */,
    0x00000305    /*  76 LREF */,
    0x00000017    /*  77 TEST */,
    SG_WORD(3),
    0x00000018    /*  79 JUMP */,
    SG_WORD(6),
    0x00000030    /*  81 FRAME */,
    SG_WORD(4),
    0x00000045    /*  83 LREF_PUSH */,
    0x0000014a    /*  84 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier vector-copy#core.macro> */,
    0x0000000b    /*  86 PUSH */,
    0x00000545    /*  87 LREF_PUSH */,
    0x00000245    /*  88 LREF_PUSH */,
    0x00000405    /*  89 LREF */,
    0x00000044    /*  90 VEC_SET */,
    0x00000205    /*  91 LREF */,
    0x0000010f    /*  92 ADDI */,
    0x0000000b    /*  93 PUSH */,
    0x00000545    /*  94 LREF_PUSH */,
    0x00200219    /*  95 SHIFTJ */,
    0x00000018    /*  96 JUMP */,
    SG_WORD(-54),
    0x0000002f    /*  98 RET */,
    0x00000030    /*  99 FRAME */,
    SG_WORD(4),
    0x00000045    /* 101 LREF_PUSH */,
    0x0000014a    /* 102 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier variable?#core.macro> */,
    0x00000017    /* 104 TEST */,
    SG_WORD(7),
    0x00000045    /* 106 LREF_PUSH */,
    0x00000146    /* 107 FREF_PUSH */,
    0x00000046    /* 108 FREF_PUSH */,
    0x0000034b    /* 109 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier er-rename#core.macro> */,
    0x0000002f    /* 111 RET */,
    0x00000005    /* 112 LREF */,
    0x0000002f    /* 113 RET */,
    /* rename */0x00000263    /*   0 RESV_STACK */,
    0x00000030    /*   1 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   3 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000131    /*   5 INST_STACK */,
    0x00000245    /*   6 LREF_PUSH */,
    0x00000145    /*   7 LREF_PUSH */,
    0x00000046    /*   8 FREF_PUSH */,
    0x00000329    /*   9 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[107])) /* #<code-builder rec (1 0 3)> */,
    0x00000231    /*  11 INST_STACK */,
    0x00000045    /*  12 LREF_PUSH */,
    0x00000205    /*  13 LREF */,
    0x0000012e    /*  14 LOCAL_TAIL_CALL */,
    0x0000002f    /*  15 RET */,
    /* er-macro-transformer */0x00000030    /*   0 FRAME */,
    SG_WORD(3),
    0x0000004a    /*   2 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-eq-hashtable#core.macro> */,
    0x0000000b    /*   4 PUSH */,
    0x00000263    /*   5 RESV_STACK */,
    0x00000145    /*   6 LREF_PUSH */,
    0x00000029    /*   7 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[108])) /* #<code-builder rename (1 0 1)> */,
    0x00000231    /*   9 INST_STACK */,
    0x00000009    /*  10 GREF */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x00000331    /*  12 INST_STACK */,
    0x00000045    /*  13 LREF_PUSH */,
    0x00000245    /*  14 LREF_PUSH */,
    0x00000047    /*  15 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x00000007    /*  17 FREF */,
    0x0000032d    /*  18 TAIL_CALL */,
    0x0000002f    /*  19 RET */,
    /* er-macro-transformer */0x00000045    /*   0 LREF_PUSH */,
    0x00000029    /*   1 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[109])) /* #<code-builder er-macro-transformer (1 0 1)> */,
    0x0000002f    /*   3 RET */,
    /* #f */0x00000029    /*   0 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[0])) /* #<code-builder (literal-match? cmp) (3 0 0)> */,
    0x00000033    /*   2 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15865#core.macro> */,
    0x00000029    /*   4 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[1])) /* #<code-builder (literal-match? lambda15864) (2 0 0)> */,
    0x00000033    /*   6 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15864#core.macro> */,
    0x00000029    /*   8 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[2])) /* #<code-builder (collect-unique-ids loop) (2 0 0)> */,
    0x00000033    /*  10 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15863#core.macro> */,
    0x00000029    /*  12 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[3])) /* #<code-builder (compile-syntax-case lambda15862) (3 0 0)> */,
    0x00000033    /*  14 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15862#core.macro> */,
    0x00000029    /*  16 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[4])) /* #<code-builder (compile-syntax-case lambda15861) (1 0 0)> */,
    0x00000033    /*  18 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15861#core.macro> */,
    0x00000029    /*  20 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[5])) /* #<code-builder (compile-syntax-case lambda15860) (3 0 0)> */,
    0x00000033    /*  22 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15860#core.macro> */,
    0x00000029    /*  24 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[6])) /* #<code-builder (match-pattern? ensure-id) (2 0 0)> */,
    0x00000033    /*  26 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15859#core.macro> */,
    0x00000029    /*  28 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[7])) /* #<code-builder (compile-syntax lambda15858) (3 0 0)> */,
    0x00000033    /*  30 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15858#core.macro> */,
    0x00000029    /*  32 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[8])) /* #<code-builder (expand-syntax loop) (1 0 0)> */,
    0x00000033    /*  34 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15857#core.macro> */,
    0x00000029    /*  36 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[9])) /* #<code-builder (expand-syntax lambda15856) (3 0 0)> */,
    0x00000033    /*  38 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15856#core.macro> */,
    0x00000029    /*  40 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[10])) /* #<code-builder (transcribe-template lambda15855) (1 0 0)> */,
    0x00000033    /*  42 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15855#core.macro> */,
    0x00000029    /*  44 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[11])) /* #<code-builder (datum->syntax lambda15854) (1 0 0)> */,
    0x00000033    /*  46 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15854#core.macro> */,
    0x00000029    /*  48 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[12])) /* #<code-builder (make-variable-transformer lambda15853) (3 0 0)> */,
    0x00000033    /*  50 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15853#core.macro> */,
    0x00000029    /*  52 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[13])) /* #<code-builder (er-rename rename) (4 0 0)> */,
    0x00000033    /*  54 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15852#core.macro> */,
    0x00000029    /*  56 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[14])) /* #<code-builder (er-macro-transformer compare) (2 0 0)> */,
    0x00000033    /*  58 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lambda15851#core.macro> */,
    0x00000034    /*  60 LIBRARY */,
    SG_WORD(SG_UNDEF) /* #<library core.macro> */,
    0x00000004    /*  62 CONSTI */,
    0x00000133    /*  63 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier LEXICAL#core.macro> */,
    0x00000204    /*  65 CONSTI */,
    0x00000133    /*  66 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier PATTERN#core.macro> */,
    0x00000304    /*  68 CONSTI */,
    0x00000133    /*  69 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier BOUNDARY#core.macro> */,
    0x00000030    /*  71 FRAME */,
    SG_WORD(7),
    0x00000048    /*  73 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* user */,
    0x00000048    /*  75 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000024a    /*  77 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier find-library#core.macro> */,
    0x0000000b    /*  79 PUSH */,
    0x00000048    /*  80 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /*  82 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000048    /*  84 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000003    /*  86 CONST */,
    SG_WORD(SG_FALSE) /* #f */,
    0x00000540    /*  88 VECTOR */,
    0x00000033    /*  89 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier *root-env*#core.macro> */,
    0x00000030    /*  91 FRAME */,
    SG_WORD(5),
    0x00000047    /*  93 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier *root-env*#core.macro> */,
    0x0000014a    /*  95 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-core-parameter#core.macro> */,
    0x00000033    /*  97 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier current-usage-env#core.macro> */,
    0x00000030    /*  99 FRAME */,
    SG_WORD(5),
    0x00000047    /* 101 GREF_PUSH */,
    SG_WORD(SG_UNDEF) /* #<identifier *root-env*#core.macro> */,
    0x0000014a    /* 103 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-core-parameter#core.macro> */,
    0x00000033    /* 105 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier current-macro-env#core.macro> */,
    0x00000030    /* 107 FRAME */,
    SG_WORD(5),
    0x00000048    /* 109 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x0000014a    /* 111 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-core-parameter#core.macro> */,
    0x00000033    /* 113 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier current-transformer-env#core.macro> */,
    0x00000029    /* 115 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[15])) /* #<code-builder lookup-transformer-env (1 0 0)> */,
    0x00000033    /* 117 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier lookup-transformer-env#core.macro> */,
    0x00000029    /* 119 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[16])) /* #<code-builder add-to-transformer-env! (2 0 0)> */,
    0x00000033    /* 121 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier add-to-transformer-env!#core.macro> */,
    0x00000030    /* 123 FRAME */,
    SG_WORD(5),
    0x00000048    /* 125 CONST_PUSH */,
    SG_WORD(SG_FALSE) /* #f */,
    0x0000014a    /* 127 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-core-parameter#core.macro> */,
    0x00000033    /* 129 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier current-identity#core.macro> */,
    0x00000029    /* 131 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[17])) /* #<code-builder generate-identity (0 0 0)> */,
    0x00000033    /* 133 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier generate-identity#core.macro> */,
    0x00000029    /* 135 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[18])) /* #<code-builder %make-identifier (4 0 0)> */,
    0x00000033    /* 137 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier %make-identifier#core.macro> */,
    0x00000029    /* 139 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[19])) /* #<code-builder make-identifier (3 0 0)> */,
    0x00000033    /* 141 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000029    /* 143 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[20])) /* #<code-builder make-pending-identifier (3 0 0)> */,
    0x00000033    /* 145 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier make-pending-identifier#core.macro> */,
    0x00000029    /* 147 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[21])) /* #<code-builder p1env-library (1 0 0)> */,
    0x00000033    /* 149 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-library#core.macro> */,
    0x00000029    /* 151 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[22])) /* #<code-builder p1env-frames (1 0 0)> */,
    0x00000033    /* 153 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier p1env-frames#core.macro> */,
    0x00000029    /* 155 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[23])) /* #<code-builder free-identifier=? (2 0 0)> */,
    0x00000033    /* 157 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier free-identifier=?#core.macro> */,
    0x00000030    /* 159 FRAME */,
    SG_WORD(9),
    0x00000048    /* 161 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* .vars */,
    0x00000048    /* 163 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 165 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core macro) */,
    0x0000034a    /* 167 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 169 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .vars#core.macro> */,
    0x00000030    /* 171 FRAME */,
    SG_WORD(9),
    0x00000048    /* 173 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* _ */,
    0x00000048    /* 175 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 177 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core) */,
    0x0000034a    /* 179 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 181 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .bar#core.macro> */,
    0x00000030    /* 183 FRAME */,
    SG_WORD(9),
    0x00000048    /* 185 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ... */,
    0x00000048    /* 187 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 189 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core) */,
    0x0000034a    /* 191 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 193 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .ellipsis#core.macro> */,
    0x00000029    /* 195 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[24])) /* #<code-builder literal-match? (2 0 0)> */,
    0x00000033    /* 197 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier literal-match?#core.macro> */,
    0x00000029    /* 199 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[25])) /* #<code-builder bar? (1 0 0)> */,
    0x00000033    /* 201 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bar?#core.macro> */,
    0x00000029    /* 203 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[26])) /* #<code-builder ellipsis? (1 0 0)> */,
    0x00000033    /* 205 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis?#core.macro> */,
    0x00000029    /* 207 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[27])) /* #<code-builder ellipsis-pair? (1 0 0)> */,
    0x00000033    /* 209 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis-pair?#core.macro> */,
    0x00000029    /* 211 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[28])) /* #<code-builder ellipsis-splicing-pair? (1 0 0)> */,
    0x00000033    /* 213 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis-splicing-pair?#core.macro> */,
    0x00000029    /* 215 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[29])) /* #<code-builder ellipsis-quote? (1 0 0)> */,
    0x00000033    /* 217 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier ellipsis-quote?#core.macro> */,
    0x00000029    /* 219 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[32])) /* #<code-builder check-pattern (2 0 0)> */,
    0x00000033    /* 221 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier check-pattern#core.macro> */,
    0x00000030    /* 223 FRAME */,
    SG_WORD(9),
    0x00000048    /* 225 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* match-syntax-case */,
    0x00000048    /* 227 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 229 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core macro) */,
    0x0000034a    /* 231 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 233 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .match-syntax-case#core.macro> */,
    0x00000030    /* 235 FRAME */,
    SG_WORD(9),
    0x00000048    /* 237 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* list */,
    0x00000048    /* 239 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 241 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core macro) */,
    0x0000034a    /* 243 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 245 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .list#core.macro> */,
    0x00000030    /* 247 FRAME */,
    SG_WORD(9),
    0x00000048    /* 249 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* lambda */,
    0x00000048    /* 251 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 253 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core macro) */,
    0x0000034a    /* 255 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 257 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .lambda#core.macro> */,
    0x00000029    /* 259 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[33])) /* #<code-builder collect-unique-ids (1 0 0)> */,
    0x00000033    /* 261 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-unique-ids#core.macro> */,
    0x00000029    /* 263 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[34])) /* #<code-builder collect-vars-ranks (4 0 0)> */,
    0x00000033    /* 265 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-vars-ranks#core.macro> */,
    0x00000030    /* 267 FRAME */,
    SG_WORD(9),
    0x00000048    /* 269 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* syntax-quote */,
    0x00000048    /* 271 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 273 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (sagittarius compiler) */,
    0x0000034a    /* 275 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 277 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax-quote.#core.macro> */,
    0x00000029    /* 279 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[37])) /* #<code-builder rewrite-form (6 0 0)> */,
    0x00000033    /* 281 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier rewrite-form#core.macro> */,
    0x00000029    /* 283 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[38])) /* #<code-builder pattern-identifier? (2 0 0)> */,
    0x00000033    /* 285 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier pattern-identifier?#core.macro> */,
    0x00000029    /* 287 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[46])) /* #<code-builder compile-syntax-case (8 0 0)> */,
    0x00000033    /* 289 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier compile-syntax-case#core.macro> */,
    0x00000029    /* 291 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[47])) /* #<code-builder count-pair (1 0 0)> */,
    0x00000033    /* 293 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier count-pair#core.macro> */,
    0x00000029    /* 295 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[48])) /* #<code-builder match-ellipsis? (3 0 0)> */,
    0x00000033    /* 297 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis?#core.macro> */,
    0x00000029    /* 299 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[49])) /* #<code-builder match-ellipsis-n? (4 0 0)> */,
    0x00000033    /* 301 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier match-ellipsis-n?#core.macro> */,
    0x00000029    /* 303 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[50])) /* #<code-builder match-pattern? (3 0 0)> */,
    0x00000033    /* 305 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier match-pattern?#core.macro> */,
    0x00000029    /* 307 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[51])) /* #<code-builder union-vars (2 0 0)> */,
    0x00000033    /* 309 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier union-vars#core.macro> */,
    0x00000029    /* 311 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[52])) /* #<code-builder bind-var! (3 0 0)> */,
    0x00000033    /* 313 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-var!#core.macro> */,
    0x00000029    /* 315 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[53])) /* #<code-builder bind-null-ellipsis (3 0 0)> */,
    0x00000033    /* 317 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-null-ellipsis#core.macro> */,
    0x00000029    /* 319 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[54])) /* #<code-builder bind-ellipsis (5 0 0)> */,
    0x00000033    /* 321 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis#core.macro> */,
    0x00000029    /* 323 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[55])) /* #<code-builder bind-ellipsis-n (6 0 0)> */,
    0x00000033    /* 325 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-ellipsis-n#core.macro> */,
    0x00000029    /* 327 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[56])) /* #<code-builder bind-pattern (4 0 0)> */,
    0x00000033    /* 329 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier bind-pattern#core.macro> */,
    0x00000029    /* 331 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[57])) /* #<code-builder match-syntax-case (3 1 0)> */,
    0x00000033    /* 333 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier match-syntax-case#core.macro> */,
    0x00000030    /* 335 FRAME */,
    SG_WORD(9),
    0x00000048    /* 337 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* expand-syntax */,
    0x00000048    /* 339 CONST_PUSH */,
    SG_WORD(SG_NIL) /* () */,
    0x00000048    /* 341 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* (core macro) */,
    0x0000034a    /* 343 GREF_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier make-identifier#core.macro> */,
    0x00000033    /* 345 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .expand-syntax#core.macro> */,
    0x00000029    /* 347 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[59])) /* #<code-builder collect-rename-ids (2 0 0)> */,
    0x00000033    /* 349 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-rename-ids#core.macro> */,
    0x00000029    /* 351 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[60])) /* #<code-builder parse-ellipsis-splicing (1 0 0)> */,
    0x00000033    /* 353 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier parse-ellipsis-splicing#core.macro> */,
    0x00000029    /* 355 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[67])) /* #<code-builder check-template (2 0 0)> */,
    0x00000033    /* 357 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier check-template#core.macro> */,
    0x00000029    /* 359 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[70])) /* #<code-builder compile-syntax (4 0 0)> */,
    0x00000033    /* 361 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier compile-syntax#core.macro> */,
    0x00000029    /* 363 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[76])) /* #<code-builder expand-syntax (3 0 0)> */,
    0x00000033    /* 365 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier expand-syntax#core.macro> */,
    0x00000029    /* 367 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[77])) /* #<code-builder rank-of (2 0 0)> */,
    0x00000033    /* 369 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier rank-of#core.macro> */,
    0x00000029    /* 371 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[78])) /* #<code-builder subform-of (2 0 0)> */,
    0x00000033    /* 373 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier subform-of#core.macro> */,
    0x00000029    /* 375 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[79])) /* #<code-builder collect-ellipsis-vars (4 0 0)> */,
    0x00000033    /* 377 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier collect-ellipsis-vars#core.macro> */,
    0x00000029    /* 379 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[82])) /* #<code-builder contain-rank-moved-var? (3 0 0)> */,
    0x00000033    /* 381 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier contain-rank-moved-var?#core.macro> */,
    0x00000029    /* 383 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[86])) /* #<code-builder adapt-to-rank-moved-vars (3 0 0)> */,
    0x00000033    /* 385 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier adapt-to-rank-moved-vars#core.macro> */,
    0x00000029    /* 387 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[88])) /* #<code-builder consume-ellipsis-vars (3 0 0)> */,
    0x00000033    /* 389 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier consume-ellipsis-vars#core.macro> */,
    0x00000029    /* 391 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[93])) /* #<code-builder transcribe-template (3 0 0)> */,
    0x00000033    /* 393 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier transcribe-template#core.macro> */,
    0x00000029    /* 395 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[95])) /* #<code-builder datum->syntax (2 0 0)> */,
    0x00000033    /* 397 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier datum->syntax#core.macro> */,
    0x00000029    /* 399 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[96])) /* #<code-builder syntax->datum (1 0 0)> */,
    0x00000033    /* 401 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier syntax->datum#core.macro> */,
    0x00000029    /* 403 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[97])) /* #<code-builder generate-temporaries (1 0 0)> */,
    0x00000033    /* 405 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier generate-temporaries#core.macro> */,
    0x00000003    /* 407 CONST */,
    SG_WORD(SG_UNDEF) /* variable-transformer */,
    0x00000138    /* 409 LIST */,
    0x00000033    /* 410 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier *variable-transformer-mark*#core.macro> */,
    0x00000029    /* 412 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[98])) /* #<code-builder variable-transformer? (1 0 0)> */,
    0x00000033    /* 414 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier variable-transformer?#core.macro> */,
    0x00000029    /* 416 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[101])) /* #<code-builder macro-transform (4 0 0)> */,
    0x00000033    /* 418 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier macro-transform#core.macro> */,
    0x00000029    /* 420 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[102])) /* #<code-builder make-macro-transformer (4 0 0)> */,
    0x00000033    /* 422 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier make-macro-transformer#core.macro> */,
    0x00000029    /* 424 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[105])) /* #<code-builder make-variable-transformer (1 0 0)> */,
    0x00000033    /* 426 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier make-variable-transformer#core.macro> */,
    0x00000029    /* 428 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[106])) /* #<code-builder er-rename (3 0 0)> */,
    0x00000033    /* 430 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier er-rename#core.macro> */,
    0x00000029    /* 432 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15752.d15868[110])) /* #<code-builder er-macro-transformer (1 0 0)> */,
    0x00000033    /* 434 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier er-macro-transformer#core.macro> */,
    0x00000002    /* 436 UNDEF */,
    0x0000002f    /* 437 RET */,
  },
  {  /* SgObject d15867 */
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
  },
  {  /* SgCodeBuilder d15868 */
    
    SG_STATIC_CODE_BUILDER( /* (literal-match? cmp) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[0]), SG_FALSE, 3, 0, 0, 14, 20),
    
    SG_STATIC_CODE_BUILDER( /* (literal-match? lambda15864) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[20]), SG_FALSE, 2, 0, 0, 10, 8),
    
    SG_STATIC_CODE_BUILDER( /* (collect-unique-ids loop) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[28]), SG_FALSE, 2, 0, 0, 19, 64),
    
    SG_STATIC_CODE_BUILDER( /* (compile-syntax-case lambda15862) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[92]), SG_FALSE, 3, 0, 0, 12, 15),
    
    SG_STATIC_CODE_BUILDER( /* (compile-syntax-case lambda15861) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[107]), SG_FALSE, 1, 0, 0, 7, 2),
    
    SG_STATIC_CODE_BUILDER( /* (compile-syntax-case lambda15860) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[109]), SG_FALSE, 3, 0, 0, 12, 15),
    
    SG_STATIC_CODE_BUILDER( /* (match-pattern? ensure-id) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[124]), SG_FALSE, 2, 0, 0, 13, 21),
    
    SG_STATIC_CODE_BUILDER( /* (compile-syntax lambda15858) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[145]), SG_FALSE, 3, 0, 0, 12, 15),
    
    SG_STATIC_CODE_BUILDER( /* (expand-syntax loop) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[160]), SG_FALSE, 1, 0, 0, 16, 57),
    
    SG_STATIC_CODE_BUILDER( /* (expand-syntax lambda15856) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[217]), SG_FALSE, 3, 0, 0, 21, 33),
    
    SG_STATIC_CODE_BUILDER( /* (transcribe-template lambda15855) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[250]), SG_FALSE, 1, 0, 0, 8, 4),
    
    SG_STATIC_CODE_BUILDER( /* (datum->syntax lambda15854) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[254]), SG_FALSE, 1, 0, 0, 7, 2),
    
    SG_STATIC_CODE_BUILDER( /* (make-variable-transformer lambda15853) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[256]), SG_FALSE, 3, 0, 0, 12, 15),
    
    SG_STATIC_CODE_BUILDER( /* (er-rename rename) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[271]), SG_FALSE, 4, 0, 0, 18, 23),
    
    SG_STATIC_CODE_BUILDER( /* (er-macro-transformer compare) */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[294]), SG_FALSE, 2, 0, 0, 39, 149),
    
    SG_STATIC_CODE_BUILDER( /* lookup-transformer-env */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[443]), SG_FALSE, 1, 0, 0, 17, 70),
    
    SG_STATIC_CODE_BUILDER( /* add-to-transformer-env! */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[513]), SG_FALSE, 2, 0, 0, 16, 46),
    
    SG_STATIC_CODE_BUILDER( /* generate-identity */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[559]), SG_FALSE, 0, 0, 0, 7, 5),
    
    SG_STATIC_CODE_BUILDER( /* %make-identifier */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[564]), SG_FALSE, 4, 0, 0, 28, 131),
    
    SG_STATIC_CODE_BUILDER( /* make-identifier */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[695]), SG_FALSE, 3, 0, 0, 13, 8),
    
    SG_STATIC_CODE_BUILDER( /* make-pending-identifier */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[703]), SG_FALSE, 3, 0, 0, 13, 8),
    
    SG_STATIC_CODE_BUILDER( /* p1env-library */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[711]), SG_FALSE, 1, 0, 0, 8, 4),
    
    SG_STATIC_CODE_BUILDER( /* p1env-frames */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[715]), SG_FALSE, 1, 0, 0, 8, 4),
    
    SG_STATIC_CODE_BUILDER( /* free-identifier=? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[719]), SG_FALSE, 2, 0, 0, 11, 10),
    
    SG_STATIC_CODE_BUILDER( /* literal-match? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[729]), SG_FALSE, 2, 0, 0, 16, 27),
    
    SG_STATIC_CODE_BUILDER( /* bar? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[756]), SG_FALSE, 1, 0, 0, 16, 50),
    
    SG_STATIC_CODE_BUILDER( /* ellipsis? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[806]), SG_FALSE, 1, 0, 0, 16, 50),
    
    SG_STATIC_CODE_BUILDER( /* ellipsis-pair? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[856]), SG_FALSE, 1, 0, 0, 8, 14),
    
    SG_STATIC_CODE_BUILDER( /* ellipsis-splicing-pair? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[870]), SG_FALSE, 1, 0, 0, 10, 31),
    
    SG_STATIC_CODE_BUILDER( /* ellipsis-quote? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[901]), SG_FALSE, 1, 0, 0, 8, 19),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[920]), SG_FALSE, 1, 0, 4, 25, 134),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1054]), SG_FALSE, 2, 0, 3, 27, 97),
    
    SG_STATIC_CODE_BUILDER( /* check-pattern */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1151]), SG_FALSE, 2, 0, 0, 13, 29),
    
    SG_STATIC_CODE_BUILDER( /* collect-unique-ids */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1180]), SG_FALSE, 1, 0, 0, 10, 11),
    
    SG_STATIC_CODE_BUILDER( /* collect-vars-ranks */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1191]), SG_FALSE, 4, 0, 0, 38, 126),
    
    SG_STATIC_CODE_BUILDER( /* seen-or-gen */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1317]), SG_FALSE, 3, 0, 2, 20, 28),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1345]), SG_FALSE, 1, 0, 5, 41, 139),
    
    SG_STATIC_CODE_BUILDER( /* rewrite-form */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1484]), SG_FALSE, 6, 0, 0, 15, 19),
    
    SG_STATIC_CODE_BUILDER( /* pattern-identifier? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1503]), SG_FALSE, 2, 0, 0, 12, 11),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1514]), SG_FALSE, 2, 0, 1, 11, 6),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1520]), SG_FALSE, 1, 0, 1, 12, 22),
    
    SG_STATIC_CODE_BUILDER( /* rewrite */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1542]), SG_FALSE, 3, 0, 4, 17, 50),
    
    SG_STATIC_CODE_BUILDER( /* gen-patvar */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1592]), SG_FALSE, 1, 0, 2, 13, 11),
    
    SG_STATIC_CODE_BUILDER( /* parse-pattern */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1603]), SG_FALSE, 1, 0, 6, 27, 61),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1664]), SG_FALSE, 0, 0, 3, 45, 95),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1759]), SG_FALSE, 1, 0, 2, 34, 66),
    
    SG_STATIC_CODE_BUILDER( /* compile-syntax-case */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[1825]), SG_FALSE, 8, 0, 0, 37, 184),
    
    SG_STATIC_CODE_BUILDER( /* count-pair */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2009]), SG_FALSE, 1, 0, 0, 11, 16),
    
    SG_STATIC_CODE_BUILDER( /* match-ellipsis? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2025]), SG_FALSE, 3, 0, 0, 16, 23),
    
    SG_STATIC_CODE_BUILDER( /* match-ellipsis-n? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2048]), SG_FALSE, 4, 0, 0, 18, 27),
    
    SG_STATIC_CODE_BUILDER( /* match-pattern? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2075]), SG_FALSE, 3, 0, 0, 61, 226),
    
    SG_STATIC_CODE_BUILDER( /* union-vars */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2301]), SG_FALSE, 2, 0, 0, 13, 26),
    
    SG_STATIC_CODE_BUILDER( /* bind-var! */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2327]), SG_FALSE, 3, 0, 0, 17, 33),
    
    SG_STATIC_CODE_BUILDER( /* bind-null-ellipsis */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2360]), SG_FALSE, 3, 0, 0, 19, 37),
    
    SG_STATIC_CODE_BUILDER( /* bind-ellipsis */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2397]), SG_FALSE, 5, 0, 0, 27, 33),
    
    SG_STATIC_CODE_BUILDER( /* bind-ellipsis-n */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2430]), SG_FALSE, 6, 0, 0, 29, 37),
    
    SG_STATIC_CODE_BUILDER( /* bind-pattern */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2467]), SG_FALSE, 4, 0, 0, 59, 190),
    
    SG_STATIC_CODE_BUILDER( /* match-syntax-case */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2657]), SG_FALSE, 3, 1, 0, 45, 85),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2742]), SG_FALSE, 1, 0, 2, 13, 25),
    
    SG_STATIC_CODE_BUILDER( /* collect-rename-ids */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2767]), SG_FALSE, 2, 0, 0, 13, 16),
    
    SG_STATIC_CODE_BUILDER( /* parse-ellipsis-splicing */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2783]), SG_FALSE, 1, 0, 0, 14, 40),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2823]), SG_FALSE, 1, 0, 2, 9, 10),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[2833]), SG_FALSE, 2, 0, 2, 42, 216),
    
    SG_STATIC_CODE_BUILDER( /* control-patvar-exists? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3049]), SG_FALSE, 2, 0, 1, 11, 11),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3060]), SG_FALSE, 1, 0, 4, 20, 69),
    
    SG_STATIC_CODE_BUILDER( /* check-escaped */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3129]), SG_FALSE, 2, 0, 2, 10, 12),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3141]), SG_FALSE, 2, 0, 5, 74, 373),
    
    SG_STATIC_CODE_BUILDER( /* check-template */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3514]), SG_FALSE, 2, 0, 0, 17, 50),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3564]), SG_FALSE, 1, 0, 1, 14, 19),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3583]), SG_FALSE, 1, 0, 2, 15, 35),
    
    SG_STATIC_CODE_BUILDER( /* compile-syntax */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3618]), SG_FALSE, 4, 0, 0, 48, 125),
    
    SG_STATIC_CODE_BUILDER( /* wrap-symbol */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3743]), SG_FALSE, 1, 0, 1, 17, 18),
    
    SG_STATIC_CODE_BUILDER( /* rename-vector */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3761]), SG_FALSE, 1, 0, 1, 23, 63),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3824]), SG_FALSE, 1, 0, 3, 32, 121),
    
    SG_STATIC_CODE_BUILDER( /* partial-identifier */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3945]), SG_FALSE, 1, 0, 2, 10, 16),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3961]), SG_FALSE, 1, 0, 1, 10, 10),
    
    SG_STATIC_CODE_BUILDER( /* expand-syntax */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[3971]), SG_FALSE, 3, 0, 0, 29, 97),
    
    SG_STATIC_CODE_BUILDER( /* rank-of */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4068]), SG_FALSE, 2, 0, 0, 17, 23),
    
    SG_STATIC_CODE_BUILDER( /* subform-of */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4091]), SG_FALSE, 2, 0, 0, 10, 8),
    
    SG_STATIC_CODE_BUILDER( /* collect-ellipsis-vars */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4099]), SG_FALSE, 4, 0, 0, 42, 87),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4186]), SG_FALSE, 2, 0, 2, 19, 51),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4237]), SG_FALSE, 2, 0, 2, 44, 203),
    
    SG_STATIC_CODE_BUILDER( /* contain-rank-moved-var? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4440]), SG_FALSE, 3, 0, 0, 12, 11),
    
    SG_STATIC_CODE_BUILDER( /* revealed */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4451]), SG_FALSE, 2, 0, 6, 34, 106),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4557]), SG_FALSE, 2, 0, 2, 17, 52),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4609]), SG_FALSE, 2, 0, 2, 43, 215),
    
    SG_STATIC_CODE_BUILDER( /* adapt-to-rank-moved-vars */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4824]), SG_FALSE, 3, 0, 0, 32, 69),
    
    SG_STATIC_CODE_BUILDER( /* loop */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4893]), SG_FALSE, 1, 0, 5, 18, 77),
    
    SG_STATIC_CODE_BUILDER( /* consume-ellipsis-vars */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[4970]), SG_FALSE, 3, 0, 0, 17, 40),
    
    SG_STATIC_CODE_BUILDER( /* expand-var */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5010]), SG_FALSE, 2, 0, 1, 23, 61),
    
    SG_STATIC_CODE_BUILDER( /* expand-ellipsis-template */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5071]), SG_FALSE, 3, 0, 3, 25, 76),
    
    SG_STATIC_CODE_BUILDER( /* expand-escaped-template */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5147]), SG_FALSE, 3, 0, 3, 22, 67),
    
    SG_STATIC_CODE_BUILDER( /* expand-template */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5214]), SG_FALSE, 3, 0, 6, 69, 318),
    
    SG_STATIC_CODE_BUILDER( /* transcribe-template */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5532]), SG_FALSE, 3, 0, 0, 27, 69),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5601]), SG_FALSE, 3, 0, 1, 11, 5),
    
    SG_STATIC_CODE_BUILDER( /* datum->syntax */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5606]), SG_FALSE, 2, 0, 0, 14, 42),
    
    SG_STATIC_CODE_BUILDER( /* syntax->datum */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5648]), SG_FALSE, 1, 0, 0, 8, 4),
    
    SG_STATIC_CODE_BUILDER( /* generate-temporaries */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5652]), SG_FALSE, 1, 0, 0, 23, 86),
    
    SG_STATIC_CODE_BUILDER( /* variable-transformer? */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5738]), SG_FALSE, 1, 0, 0, 10, 16),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5754]), SG_FALSE, 0, 0, 3, 14, 32),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5786]), SG_FALSE, 0, 0, 3, 7, 20),
    
    SG_STATIC_CODE_BUILDER( /* macro-transform */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5806]), SG_FALSE, 4, 0, 0, 19, 64),
    
    SG_STATIC_CODE_BUILDER( /* make-macro-transformer */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5870]), SG_FALSE, 4, 0, 0, 15, 13),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5883]), SG_FALSE, 1, 0, 1, 12, 22),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5905]), SG_FALSE, 4, 0, 1, 19, 34),
    
    SG_STATIC_CODE_BUILDER( /* make-variable-transformer */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5939]), SG_FALSE, 1, 0, 0, 11, 16),
    
    SG_STATIC_CODE_BUILDER( /* er-rename */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[5955]), SG_FALSE, 3, 0, 0, 27, 114),
    
    SG_STATIC_CODE_BUILDER( /* rec */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[6069]), SG_FALSE, 1, 0, 3, 36, 114),
    
    SG_STATIC_CODE_BUILDER( /* rename */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[6183]), SG_FALSE, 1, 0, 1, 10, 16),
    
    SG_STATIC_CODE_BUILDER( /* er-macro-transformer */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[6199]), SG_FALSE, 1, 0, 1, 14, 20),
    
    SG_STATIC_CODE_BUILDER( /* er-macro-transformer */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[6219]), SG_FALSE, 1, 0, 0, 7, 4),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15752.d15866[6223]), SG_FALSE, 0, 0, 0, 0, 438),
  },
};
static SgCodeBuilder *G15753 = 
   SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[111]));
void Sg__Init_core_macro() {
  SgObject save = Sg_VM()->currentLibrary;

  sg__rc_cgen15752.d15867[1] = SG_MAKE_STRING("(literal-match? cmp)");
  sg__rc_cgen15752.d15867[0] = Sg_Intern(sg__rc_cgen15752.d15867[1]); /* (literal-match? cmp) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[0]))->name = sg__rc_cgen15752.d15867[0];/* (literal-match? cmp) */
  sg__rc_cgen15752.d15867[2] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[4] = SG_MAKE_STRING("lambda15865");
  sg__rc_cgen15752.d15867[3] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[4]), FALSE); /* lambda15865 */
  sg__rc_cgen15752.d15867[7] = SG_MAKE_STRING("(core macro)");
  sg__rc_cgen15752.d15867[6] = Sg_Intern(sg__rc_cgen15752.d15867[7]); /* (core macro) */
  sg__rc_cgen15752.d15867[5] = Sg_FindLibrary(SG_SYMBOL(sg__rc_cgen15752.d15867[6]), TRUE);
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[2],SG_SYMBOL(sg__rc_cgen15752.d15867[3]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15865 */
  sg__rc_cgen15752.d15867[8] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[10] = SG_MAKE_STRING("id-name");
  sg__rc_cgen15752.d15867[9] = Sg_Intern(sg__rc_cgen15752.d15867[10]); /* id-name */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[8],SG_SYMBOL(sg__rc_cgen15752.d15867[9]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-name */
  sg__rc_cgen15752.d15867[12] = SG_MAKE_STRING("(literal-match? lambda15864)");
  sg__rc_cgen15752.d15867[11] = Sg_Intern(sg__rc_cgen15752.d15867[12]); /* (literal-match? lambda15864) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[1]))->name = sg__rc_cgen15752.d15867[11];/* (literal-match? lambda15864) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[20]))[5] = SG_WORD(sg__rc_cgen15752.d15867[8]);
  sg__rc_cgen15752.d15867[13] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[15] = SG_MAKE_STRING("lambda15864");
  sg__rc_cgen15752.d15867[14] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[15]), FALSE); /* lambda15864 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[13],SG_SYMBOL(sg__rc_cgen15752.d15867[14]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15864 */
  sg__rc_cgen15752.d15867[16] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[18] = SG_MAKE_STRING("lambda15863");
  sg__rc_cgen15752.d15867[17] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[18]), FALSE); /* lambda15863 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[16],SG_SYMBOL(sg__rc_cgen15752.d15867[17]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15863 */
  sg__rc_cgen15752.d15867[19] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[21] = SG_MAKE_STRING("ellipsis?");
  sg__rc_cgen15752.d15867[20] = Sg_Intern(sg__rc_cgen15752.d15867[21]); /* ellipsis? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[19],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[22] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[24] = SG_MAKE_STRING("variable?");
  sg__rc_cgen15752.d15867[23] = Sg_Intern(sg__rc_cgen15752.d15867[24]); /* variable? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[22],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[25] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[27] = SG_MAKE_STRING("memq");
  sg__rc_cgen15752.d15867[26] = Sg_Intern(sg__rc_cgen15752.d15867[27]); /* memq */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[25],SG_SYMBOL(sg__rc_cgen15752.d15867[26]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* memq */
  sg__rc_cgen15752.d15867[28] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[30] = SG_MAKE_STRING("vector->list");
  sg__rc_cgen15752.d15867[29] = Sg_Intern(sg__rc_cgen15752.d15867[30]); /* vector->list */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[28],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[32] = SG_MAKE_STRING("(collect-unique-ids loop)");
  sg__rc_cgen15752.d15867[31] = Sg_Intern(sg__rc_cgen15752.d15867[32]); /* (collect-unique-ids loop) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[2]))->name = sg__rc_cgen15752.d15867[31];/* (collect-unique-ids loop) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[10] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[14] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[21] = SG_WORD(sg__rc_cgen15752.d15867[19]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[30] = SG_WORD(sg__rc_cgen15752.d15867[22]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[38] = SG_WORD(sg__rc_cgen15752.d15867[25]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[55] = SG_WORD(sg__rc_cgen15752.d15867[28]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[28]))[59] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  sg__rc_cgen15752.d15867[33] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[35] = SG_MAKE_STRING("make-identifier");
  sg__rc_cgen15752.d15867[34] = Sg_Intern(sg__rc_cgen15752.d15867[35]); /* make-identifier */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[33],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[37] = SG_MAKE_STRING("(compile-syntax-case lambda15862)");
  sg__rc_cgen15752.d15867[36] = Sg_Intern(sg__rc_cgen15752.d15867[37]); /* (compile-syntax-case lambda15862) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[3]))->name = sg__rc_cgen15752.d15867[36];/* (compile-syntax-case lambda15862) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[92]))[13] = SG_WORD(sg__rc_cgen15752.d15867[33]);
  sg__rc_cgen15752.d15867[38] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[40] = SG_MAKE_STRING("lambda15862");
  sg__rc_cgen15752.d15867[39] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[40]), FALSE); /* lambda15862 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[38],SG_SYMBOL(sg__rc_cgen15752.d15867[39]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15862 */
  sg__rc_cgen15752.d15867[42] = SG_MAKE_STRING("(compile-syntax-case lambda15861)");
  sg__rc_cgen15752.d15867[41] = Sg_Intern(sg__rc_cgen15752.d15867[42]); /* (compile-syntax-case lambda15861) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[4]))->name = sg__rc_cgen15752.d15867[41];/* (compile-syntax-case lambda15861) */
  sg__rc_cgen15752.d15867[43] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[45] = SG_MAKE_STRING("lambda15861");
  sg__rc_cgen15752.d15867[44] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[45]), FALSE); /* lambda15861 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[43],SG_SYMBOL(sg__rc_cgen15752.d15867[44]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15861 */
  sg__rc_cgen15752.d15867[46] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[46],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[48] = SG_MAKE_STRING("(compile-syntax-case lambda15860)");
  sg__rc_cgen15752.d15867[47] = Sg_Intern(sg__rc_cgen15752.d15867[48]); /* (compile-syntax-case lambda15860) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[5]))->name = sg__rc_cgen15752.d15867[47];/* (compile-syntax-case lambda15860) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[109]))[13] = SG_WORD(sg__rc_cgen15752.d15867[46]);
  sg__rc_cgen15752.d15867[49] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[51] = SG_MAKE_STRING("lambda15860");
  sg__rc_cgen15752.d15867[50] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[51]), FALSE); /* lambda15860 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[49],SG_SYMBOL(sg__rc_cgen15752.d15867[50]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15860 */
  sg__rc_cgen15752.d15867[52] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[54] = SG_MAKE_STRING("identifier?");
  sg__rc_cgen15752.d15867[53] = Sg_Intern(sg__rc_cgen15752.d15867[54]); /* identifier? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[52],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[55] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[55],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[57] = SG_MAKE_STRING("(match-pattern? ensure-id)");
  sg__rc_cgen15752.d15867[56] = Sg_Intern(sg__rc_cgen15752.d15867[57]); /* (match-pattern? ensure-id) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[6]))->name = sg__rc_cgen15752.d15867[56];/* (match-pattern? ensure-id) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[124]))[4] = SG_WORD(sg__rc_cgen15752.d15867[52]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[124]))[19] = SG_WORD(sg__rc_cgen15752.d15867[55]);
  sg__rc_cgen15752.d15867[58] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[60] = SG_MAKE_STRING("lambda15859");
  sg__rc_cgen15752.d15867[59] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[60]), FALSE); /* lambda15859 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[58],SG_SYMBOL(sg__rc_cgen15752.d15867[59]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15859 */
  sg__rc_cgen15752.d15867[61] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[61],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[63] = SG_MAKE_STRING("(compile-syntax lambda15858)");
  sg__rc_cgen15752.d15867[62] = Sg_Intern(sg__rc_cgen15752.d15867[63]); /* (compile-syntax lambda15858) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[7]))->name = sg__rc_cgen15752.d15867[62];/* (compile-syntax lambda15858) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[145]))[13] = SG_WORD(sg__rc_cgen15752.d15867[61]);
  sg__rc_cgen15752.d15867[64] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[66] = SG_MAKE_STRING("lambda15858");
  sg__rc_cgen15752.d15867[65] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[66]), FALSE); /* lambda15858 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[64],SG_SYMBOL(sg__rc_cgen15752.d15867[65]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15858 */
  sg__rc_cgen15752.d15867[67] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[69] = SG_MAKE_STRING("lambda15857");
  sg__rc_cgen15752.d15867[68] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[69]), FALSE); /* lambda15857 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[67],SG_SYMBOL(sg__rc_cgen15752.d15867[68]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15857 */
  sg__rc_cgen15752.d15867[70] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[70],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[72] = SG_MAKE_STRING("(expand-syntax loop)");
  sg__rc_cgen15752.d15867[71] = Sg_Intern(sg__rc_cgen15752.d15867[72]); /* (expand-syntax loop) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[8]))->name = sg__rc_cgen15752.d15867[71];/* (expand-syntax loop) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[160]))[12] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[160]))[19] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[160]))[41] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[160]))[55] = SG_WORD(sg__rc_cgen15752.d15867[70]);
  sg__rc_cgen15752.d15867[73] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[75] = SG_MAKE_STRING("lookup-transformer-env");
  sg__rc_cgen15752.d15867[74] = Sg_Intern(sg__rc_cgen15752.d15867[75]); /* lookup-transformer-env */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[73],SG_SYMBOL(sg__rc_cgen15752.d15867[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lookup-transformer-env */
  sg__rc_cgen15752.d15867[76] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[78] = SG_MAKE_STRING("%make-identifier");
  sg__rc_cgen15752.d15867[77] = Sg_Intern(sg__rc_cgen15752.d15867[78]); /* %make-identifier */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[76],SG_SYMBOL(sg__rc_cgen15752.d15867[77]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* %make-identifier */
  sg__rc_cgen15752.d15867[79] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[81] = SG_MAKE_STRING("add-to-transformer-env!");
  sg__rc_cgen15752.d15867[80] = Sg_Intern(sg__rc_cgen15752.d15867[81]); /* add-to-transformer-env! */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[79],SG_SYMBOL(sg__rc_cgen15752.d15867[80]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* add-to-transformer-env! */
  sg__rc_cgen15752.d15867[83] = SG_MAKE_STRING("(expand-syntax lambda15856)");
  sg__rc_cgen15752.d15867[82] = Sg_Intern(sg__rc_cgen15752.d15867[83]); /* (expand-syntax lambda15856) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[9]))->name = sg__rc_cgen15752.d15867[82];/* (expand-syntax lambda15856) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[217]))[4] = SG_WORD(sg__rc_cgen15752.d15867[73]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[217]))[26] = SG_WORD(sg__rc_cgen15752.d15867[76]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[217]))[31] = SG_WORD(sg__rc_cgen15752.d15867[79]);
  sg__rc_cgen15752.d15867[84] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[86] = SG_MAKE_STRING("lambda15856");
  sg__rc_cgen15752.d15867[85] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[86]), FALSE); /* lambda15856 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[84],SG_SYMBOL(sg__rc_cgen15752.d15867[85]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15856 */
  sg__rc_cgen15752.d15867[87] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[89] = SG_MAKE_STRING("unwrap-syntax");
  sg__rc_cgen15752.d15867[88] = Sg_Intern(sg__rc_cgen15752.d15867[89]); /* unwrap-syntax */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[87],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[91] = SG_MAKE_STRING("(transcribe-template lambda15855)");
  sg__rc_cgen15752.d15867[90] = Sg_Intern(sg__rc_cgen15752.d15867[91]); /* (transcribe-template lambda15855) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[10]))->name = sg__rc_cgen15752.d15867[90];/* (transcribe-template lambda15855) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[250]))[2] = SG_WORD(sg__rc_cgen15752.d15867[87]);
  sg__rc_cgen15752.d15867[92] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[94] = SG_MAKE_STRING("lambda15855");
  sg__rc_cgen15752.d15867[93] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[94]), FALSE); /* lambda15855 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[92],SG_SYMBOL(sg__rc_cgen15752.d15867[93]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15855 */
  sg__rc_cgen15752.d15867[96] = SG_MAKE_STRING("(datum->syntax lambda15854)");
  sg__rc_cgen15752.d15867[95] = Sg_Intern(sg__rc_cgen15752.d15867[96]); /* (datum->syntax lambda15854) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[11]))->name = sg__rc_cgen15752.d15867[95];/* (datum->syntax lambda15854) */
  sg__rc_cgen15752.d15867[97] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[99] = SG_MAKE_STRING("lambda15854");
  sg__rc_cgen15752.d15867[98] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[99]), FALSE); /* lambda15854 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[97],SG_SYMBOL(sg__rc_cgen15752.d15867[98]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15854 */
  sg__rc_cgen15752.d15867[100] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[100],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[102] = SG_MAKE_STRING("(make-variable-transformer lambda15853)");
  sg__rc_cgen15752.d15867[101] = Sg_Intern(sg__rc_cgen15752.d15867[102]); /* (make-variable-transformer lambda15853) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[12]))->name = sg__rc_cgen15752.d15867[101];/* (make-variable-transformer lambda15853) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[256]))[13] = SG_WORD(sg__rc_cgen15752.d15867[100]);
  sg__rc_cgen15752.d15867[103] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[105] = SG_MAKE_STRING("lambda15853");
  sg__rc_cgen15752.d15867[104] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[105]), FALSE); /* lambda15853 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[103],SG_SYMBOL(sg__rc_cgen15752.d15867[104]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15853 */
  sg__rc_cgen15752.d15867[106] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[106],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[107] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[109] = SG_MAKE_STRING("hashtable-set!");
  sg__rc_cgen15752.d15867[108] = Sg_Intern(sg__rc_cgen15752.d15867[109]); /* hashtable-set! */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[107],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  sg__rc_cgen15752.d15867[111] = SG_MAKE_STRING("(er-rename rename)");
  sg__rc_cgen15752.d15867[110] = Sg_Intern(sg__rc_cgen15752.d15867[111]); /* (er-rename rename) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[13]))->name = sg__rc_cgen15752.d15867[110];/* (er-rename rename) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[271]))[6] = SG_WORD(sg__rc_cgen15752.d15867[106]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[271]))[17] = SG_WORD(sg__rc_cgen15752.d15867[107]);
  sg__rc_cgen15752.d15867[112] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[114] = SG_MAKE_STRING("lambda15852");
  sg__rc_cgen15752.d15867[113] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[114]), FALSE); /* lambda15852 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[112],SG_SYMBOL(sg__rc_cgen15752.d15867[113]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15852 */
  sg__rc_cgen15752.d15867[115] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[117] = SG_MAKE_STRING("lambda15851");
  sg__rc_cgen15752.d15867[116] = Sg_MakeSymbol(SG_STRING(sg__rc_cgen15752.d15867[117]), FALSE); /* lambda15851 */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[115],SG_SYMBOL(sg__rc_cgen15752.d15867[116]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lambda15851 */
  sg__rc_cgen15752.d15867[118] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[118],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[119] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[119],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[120] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[122] = SG_MAKE_STRING("current-usage-env");
  sg__rc_cgen15752.d15867[121] = Sg_Intern(sg__rc_cgen15752.d15867[122]); /* current-usage-env */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[120],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[123] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[123],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[124] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[126] = SG_MAKE_STRING("er-rename");
  sg__rc_cgen15752.d15867[125] = Sg_Intern(sg__rc_cgen15752.d15867[126]); /* er-rename */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[124],SG_SYMBOL(sg__rc_cgen15752.d15867[125]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* er-rename */
  sg__rc_cgen15752.d15867[127] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[129] = SG_MAKE_STRING("free-identifier=?");
  sg__rc_cgen15752.d15867[128] = Sg_Intern(sg__rc_cgen15752.d15867[129]); /* free-identifier=? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[127],SG_SYMBOL(sg__rc_cgen15752.d15867[128]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* free-identifier=? */
  sg__rc_cgen15752.d15867[130] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[132] = SG_MAKE_STRING("equal?");
  sg__rc_cgen15752.d15867[131] = Sg_Intern(sg__rc_cgen15752.d15867[132]); /* equal? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[130],SG_SYMBOL(sg__rc_cgen15752.d15867[131]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* equal? */
  sg__rc_cgen15752.d15867[134] = SG_MAKE_STRING("(er-macro-transformer compare)");
  sg__rc_cgen15752.d15867[133] = Sg_Intern(sg__rc_cgen15752.d15867[134]); /* (er-macro-transformer compare) */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[14]))->name = sg__rc_cgen15752.d15867[133];/* (er-macro-transformer compare) */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[13] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[20] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[27] = SG_WORD(sg__rc_cgen15752.d15867[118]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[34] = SG_WORD(sg__rc_cgen15752.d15867[119]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[40] = SG_WORD(sg__rc_cgen15752.d15867[120]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[48] = SG_WORD(sg__rc_cgen15752.d15867[123]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[61] = SG_WORD(sg__rc_cgen15752.d15867[124]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[70] = SG_WORD(sg__rc_cgen15752.d15867[123]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[83] = SG_WORD(sg__rc_cgen15752.d15867[124]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[87] = SG_WORD(sg__rc_cgen15752.d15867[127]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[124] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[294]))[138] = SG_WORD(sg__rc_cgen15752.d15867[130]);
  sg__rc_cgen15752.d15867[135] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[137] = SG_MAKE_STRING("LEXICAL");
  sg__rc_cgen15752.d15867[136] = Sg_Intern(sg__rc_cgen15752.d15867[137]); /* LEXICAL */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[135],SG_SYMBOL(sg__rc_cgen15752.d15867[136]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* LEXICAL */
  sg__rc_cgen15752.d15867[138] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[140] = SG_MAKE_STRING("PATTERN");
  sg__rc_cgen15752.d15867[139] = Sg_Intern(sg__rc_cgen15752.d15867[140]); /* PATTERN */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[138],SG_SYMBOL(sg__rc_cgen15752.d15867[139]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* PATTERN */
  sg__rc_cgen15752.d15867[141] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[143] = SG_MAKE_STRING("BOUNDARY");
  sg__rc_cgen15752.d15867[142] = Sg_Intern(sg__rc_cgen15752.d15867[143]); /* BOUNDARY */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[141],SG_SYMBOL(sg__rc_cgen15752.d15867[142]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* BOUNDARY */
  sg__rc_cgen15752.d15867[145] = SG_MAKE_STRING("user");
  sg__rc_cgen15752.d15867[144] = Sg_Intern(sg__rc_cgen15752.d15867[145]); /* user */
  sg__rc_cgen15752.d15867[146] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[148] = SG_MAKE_STRING("find-library");
  sg__rc_cgen15752.d15867[147] = Sg_Intern(sg__rc_cgen15752.d15867[148]); /* find-library */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[146],SG_SYMBOL(sg__rc_cgen15752.d15867[147]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* find-library */
  sg__rc_cgen15752.d15867[149] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[151] = SG_MAKE_STRING("*root-env*");
  sg__rc_cgen15752.d15867[150] = Sg_Intern(sg__rc_cgen15752.d15867[151]); /* *root-env* */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[149],SG_SYMBOL(sg__rc_cgen15752.d15867[150]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* *root-env* */
  sg__rc_cgen15752.d15867[152] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[152],SG_SYMBOL(sg__rc_cgen15752.d15867[150]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* *root-env* */
  sg__rc_cgen15752.d15867[153] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[155] = SG_MAKE_STRING("make-core-parameter");
  sg__rc_cgen15752.d15867[154] = Sg_Intern(sg__rc_cgen15752.d15867[155]); /* make-core-parameter */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[153],SG_SYMBOL(sg__rc_cgen15752.d15867[154]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-core-parameter */
  sg__rc_cgen15752.d15867[156] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[156],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[157] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[157],SG_SYMBOL(sg__rc_cgen15752.d15867[150]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* *root-env* */
  sg__rc_cgen15752.d15867[158] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[158],SG_SYMBOL(sg__rc_cgen15752.d15867[154]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-core-parameter */
  sg__rc_cgen15752.d15867[159] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[161] = SG_MAKE_STRING("current-macro-env");
  sg__rc_cgen15752.d15867[160] = Sg_Intern(sg__rc_cgen15752.d15867[161]); /* current-macro-env */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[159],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[162] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[162],SG_SYMBOL(sg__rc_cgen15752.d15867[154]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-core-parameter */
  sg__rc_cgen15752.d15867[163] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[165] = SG_MAKE_STRING("current-transformer-env");
  sg__rc_cgen15752.d15867[164] = Sg_Intern(sg__rc_cgen15752.d15867[165]); /* current-transformer-env */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[163],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[166] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[166],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[167] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[167],SG_SYMBOL(sg__rc_cgen15752.d15867[9]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-name */
  sg__rc_cgen15752.d15867[168] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[168],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[169] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[171] = SG_MAKE_STRING("assq");
  sg__rc_cgen15752.d15867[170] = Sg_Intern(sg__rc_cgen15752.d15867[171]); /* assq */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[169],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  sg__rc_cgen15752.d15867[172] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[174] = SG_MAKE_STRING("id-library");
  sg__rc_cgen15752.d15867[173] = Sg_Intern(sg__rc_cgen15752.d15867[174]); /* id-library */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[172],SG_SYMBOL(sg__rc_cgen15752.d15867[173]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-library */
  sg__rc_cgen15752.d15867[175] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[175],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[176] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[176],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[15]))->name = sg__rc_cgen15752.d15867[74];/* lookup-transformer-env */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[4] = SG_WORD(sg__rc_cgen15752.d15867[166]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[13] = SG_WORD(sg__rc_cgen15752.d15867[167]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[18] = SG_WORD(sg__rc_cgen15752.d15867[168]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[21] = SG_WORD(sg__rc_cgen15752.d15867[169]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[34] = SG_WORD(sg__rc_cgen15752.d15867[172]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[53] = SG_WORD(sg__rc_cgen15752.d15867[175]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[443]))[56] = SG_WORD(sg__rc_cgen15752.d15867[176]);
  sg__rc_cgen15752.d15867[177] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[177],SG_SYMBOL(sg__rc_cgen15752.d15867[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lookup-transformer-env */
  sg__rc_cgen15752.d15867[178] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[178],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[179] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[179],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[180] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[180],SG_SYMBOL(sg__rc_cgen15752.d15867[9]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-name */
  sg__rc_cgen15752.d15867[181] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[181],SG_SYMBOL(sg__rc_cgen15752.d15867[173]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-library */
  sg__rc_cgen15752.d15867[182] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[182],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[183] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[183],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[16]))->name = sg__rc_cgen15752.d15867[80];/* add-to-transformer-env! */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[3] = SG_WORD(sg__rc_cgen15752.d15867[178]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[9] = SG_WORD(sg__rc_cgen15752.d15867[179]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[18] = SG_WORD(sg__rc_cgen15752.d15867[180]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[24] = SG_WORD(sg__rc_cgen15752.d15867[181]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[32] = SG_WORD(sg__rc_cgen15752.d15867[182]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[513]))[43] = SG_WORD(sg__rc_cgen15752.d15867[183]);
  sg__rc_cgen15752.d15867[184] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[184],SG_SYMBOL(sg__rc_cgen15752.d15867[80]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* add-to-transformer-env! */
  sg__rc_cgen15752.d15867[185] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[185],SG_SYMBOL(sg__rc_cgen15752.d15867[154]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-core-parameter */
  sg__rc_cgen15752.d15867[186] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[188] = SG_MAKE_STRING("current-identity");
  sg__rc_cgen15752.d15867[187] = Sg_Intern(sg__rc_cgen15752.d15867[188]); /* current-identity */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[186],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  sg__rc_cgen15752.d15867[189] = SG_MAKE_STRING("id.");
  sg__rc_cgen15752.d15867[190] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[192] = SG_MAKE_STRING("gensym");
  sg__rc_cgen15752.d15867[191] = Sg_Intern(sg__rc_cgen15752.d15867[192]); /* gensym */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[190],SG_SYMBOL(sg__rc_cgen15752.d15867[191]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* gensym */
  sg__rc_cgen15752.d15867[194] = SG_MAKE_STRING("generate-identity");
  sg__rc_cgen15752.d15867[193] = Sg_Intern(sg__rc_cgen15752.d15867[194]); /* generate-identity */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[17]))->name = sg__rc_cgen15752.d15867[193];/* generate-identity */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[559]))[1] = SG_WORD(sg__rc_cgen15752.d15867[189]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[559]))[3] = SG_WORD(sg__rc_cgen15752.d15867[190]);
  sg__rc_cgen15752.d15867[195] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[195],SG_SYMBOL(sg__rc_cgen15752.d15867[193]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* generate-identity */
  sg__rc_cgen15752.d15867[196] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[196],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[197] = SG_MAKE_STRING("symbol or identifier is required");
  sg__rc_cgen15752.d15867[198] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[200] = SG_MAKE_STRING("assertion-violation");
  sg__rc_cgen15752.d15867[199] = Sg_Intern(sg__rc_cgen15752.d15867[200]); /* assertion-violation */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[198],SG_SYMBOL(sg__rc_cgen15752.d15867[199]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assertion-violation */
  sg__rc_cgen15752.d15867[201] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[201],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[202] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[202],SG_SYMBOL(sg__rc_cgen15752.d15867[9]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-name */
  sg__rc_cgen15752.d15867[203] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[205] = SG_MAKE_STRING("library?");
  sg__rc_cgen15752.d15867[204] = Sg_Intern(sg__rc_cgen15752.d15867[205]); /* library? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[203],SG_SYMBOL(sg__rc_cgen15752.d15867[204]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* library? */
  sg__rc_cgen15752.d15867[206] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[206],SG_SYMBOL(sg__rc_cgen15752.d15867[147]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* find-library */
  sg__rc_cgen15752.d15867[207] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[207],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[208] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[210] = SG_MAKE_STRING("id-envs");
  sg__rc_cgen15752.d15867[209] = Sg_Intern(sg__rc_cgen15752.d15867[210]); /* id-envs */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[208],SG_SYMBOL(sg__rc_cgen15752.d15867[209]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-envs */
  sg__rc_cgen15752.d15867[211] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[211],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  sg__rc_cgen15752.d15867[212] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[214] = SG_MAKE_STRING("id-identity");
  sg__rc_cgen15752.d15867[213] = Sg_Intern(sg__rc_cgen15752.d15867[214]); /* id-identity */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[212],SG_SYMBOL(sg__rc_cgen15752.d15867[213]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-identity */
  sg__rc_cgen15752.d15867[215] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[215],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  sg__rc_cgen15752.d15867[216] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[218] = SG_MAKE_STRING("make-raw-identifier");
  sg__rc_cgen15752.d15867[217] = Sg_Intern(sg__rc_cgen15752.d15867[218]); /* make-raw-identifier */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[216],SG_SYMBOL(sg__rc_cgen15752.d15867[217]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-raw-identifier */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[18]))->name = sg__rc_cgen15752.d15867[77];/* %make-identifier */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[11] = SG_WORD(sg__rc_cgen15752.d15867[196]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[21] = SG_WORD(sg__rc_cgen15752.d15867[34]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[23] = SG_WORD(sg__rc_cgen15752.d15867[197]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[26] = SG_WORD(sg__rc_cgen15752.d15867[198]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[31] = SG_WORD(sg__rc_cgen15752.d15867[201]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[38] = SG_WORD(sg__rc_cgen15752.d15867[202]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[47] = SG_WORD(sg__rc_cgen15752.d15867[203]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[59] = SG_WORD(sg__rc_cgen15752.d15867[206]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[72] = SG_WORD(sg__rc_cgen15752.d15867[207]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[80] = SG_WORD(sg__rc_cgen15752.d15867[208]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[98] = SG_WORD(sg__rc_cgen15752.d15867[123]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[104] = SG_WORD(sg__rc_cgen15752.d15867[211]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[110] = SG_WORD(sg__rc_cgen15752.d15867[212]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[117] = SG_WORD(sg__rc_cgen15752.d15867[215]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[564]))[129] = SG_WORD(sg__rc_cgen15752.d15867[216]);
  sg__rc_cgen15752.d15867[219] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[219],SG_SYMBOL(sg__rc_cgen15752.d15867[77]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* %make-identifier */
  sg__rc_cgen15752.d15867[220] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[220],SG_SYMBOL(sg__rc_cgen15752.d15867[77]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* %make-identifier */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[19]))->name = sg__rc_cgen15752.d15867[34];/* make-identifier */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[695]))[6] = SG_WORD(sg__rc_cgen15752.d15867[220]);
  sg__rc_cgen15752.d15867[221] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[221],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[223] = SG_MAKE_STRING("make-pending-identifier");
  sg__rc_cgen15752.d15867[222] = Sg_Intern(sg__rc_cgen15752.d15867[223]); /* make-pending-identifier */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[20]))->name = sg__rc_cgen15752.d15867[222];/* make-pending-identifier */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[703]))[6] = SG_WORD(sg__rc_cgen15752.d15867[76]);
  sg__rc_cgen15752.d15867[224] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[224],SG_SYMBOL(sg__rc_cgen15752.d15867[222]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-pending-identifier */
  sg__rc_cgen15752.d15867[226] = SG_MAKE_STRING("p1env-library");
  sg__rc_cgen15752.d15867[225] = Sg_Intern(sg__rc_cgen15752.d15867[226]); /* p1env-library */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[21]))->name = sg__rc_cgen15752.d15867[225];/* p1env-library */
  sg__rc_cgen15752.d15867[227] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[227],SG_SYMBOL(sg__rc_cgen15752.d15867[225]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-library */
  sg__rc_cgen15752.d15867[229] = SG_MAKE_STRING("p1env-frames");
  sg__rc_cgen15752.d15867[228] = Sg_Intern(sg__rc_cgen15752.d15867[229]); /* p1env-frames */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[22]))->name = sg__rc_cgen15752.d15867[228];/* p1env-frames */
  sg__rc_cgen15752.d15867[230] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[230],SG_SYMBOL(sg__rc_cgen15752.d15867[228]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-frames */
  sg__rc_cgen15752.d15867[231] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[231],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[232] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[234] = SG_MAKE_STRING("vm-free-identifier=?");
  sg__rc_cgen15752.d15867[233] = Sg_Intern(sg__rc_cgen15752.d15867[234]); /* vm-free-identifier=? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[232],SG_SYMBOL(sg__rc_cgen15752.d15867[233]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vm-free-identifier=? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[23]))->name = sg__rc_cgen15752.d15867[128];/* free-identifier=? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[719]))[5] = SG_WORD(sg__rc_cgen15752.d15867[231]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[719]))[8] = SG_WORD(sg__rc_cgen15752.d15867[232]);
  sg__rc_cgen15752.d15867[235] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[235],SG_SYMBOL(sg__rc_cgen15752.d15867[128]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* free-identifier=? */
  sg__rc_cgen15752.d15867[237] = SG_MAKE_STRING(".vars");
  sg__rc_cgen15752.d15867[236] = Sg_Intern(sg__rc_cgen15752.d15867[237]); /* .vars */
  sg__rc_cgen15752.d15867[240] = SG_MAKE_STRING("core");
  sg__rc_cgen15752.d15867[239] = Sg_Intern(sg__rc_cgen15752.d15867[240]); /* core */
  sg__rc_cgen15752.d15867[242] = SG_MAKE_STRING("macro");
  sg__rc_cgen15752.d15867[241] = Sg_Intern(sg__rc_cgen15752.d15867[242]); /* macro */
  do {
    /* (core macro) */ 
    SgObject G15869 = SG_NIL, G15870 = SG_NIL;
    SG_APPEND1(G15869, G15870, sg__rc_cgen15752.d15867[239]); /* core */ 
    SG_APPEND1(G15869, G15870, sg__rc_cgen15752.d15867[241]); /* macro */ 
    sg__rc_cgen15752.d15867[238] = G15869;
  } while (0);
  sg__rc_cgen15752.d15867[243] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[243],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[244] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[244],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[246] = SG_MAKE_STRING("_");
  sg__rc_cgen15752.d15867[245] = Sg_Intern(sg__rc_cgen15752.d15867[246]); /* _ */
  do {
    /* (core) */ 
    SgObject G15871 = SG_NIL, G15872 = SG_NIL;
    SG_APPEND1(G15871, G15872, sg__rc_cgen15752.d15867[239]); /* core */ 
    sg__rc_cgen15752.d15867[247] = G15871;
  } while (0);
  sg__rc_cgen15752.d15867[248] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[248],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[249] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[251] = SG_MAKE_STRING(".bar");
  sg__rc_cgen15752.d15867[250] = Sg_Intern(sg__rc_cgen15752.d15867[251]); /* .bar */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[249],SG_SYMBOL(sg__rc_cgen15752.d15867[250]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .bar */
  sg__rc_cgen15752.d15867[253] = SG_MAKE_STRING("...");
  sg__rc_cgen15752.d15867[252] = Sg_Intern(sg__rc_cgen15752.d15867[253]); /* ... */
  sg__rc_cgen15752.d15867[254] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[254],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[255] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[257] = SG_MAKE_STRING(".ellipsis");
  sg__rc_cgen15752.d15867[256] = Sg_Intern(sg__rc_cgen15752.d15867[257]); /* .ellipsis */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[255],SG_SYMBOL(sg__rc_cgen15752.d15867[256]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .ellipsis */
  sg__rc_cgen15752.d15867[258] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[258],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[259] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[261] = SG_MAKE_STRING("bound-identifier=?");
  sg__rc_cgen15752.d15867[260] = Sg_Intern(sg__rc_cgen15752.d15867[261]); /* bound-identifier=? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[259],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[263] = SG_MAKE_STRING("literal-match?");
  sg__rc_cgen15752.d15867[262] = Sg_Intern(sg__rc_cgen15752.d15867[263]); /* literal-match? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[24]))->name = sg__rc_cgen15752.d15867[262];/* literal-match? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[2] = SG_WORD(sg__rc_cgen15752.d15867[2]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[8] = SG_WORD(sg__rc_cgen15752.d15867[258]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[12] = SG_WORD(sg__rc_cgen15752.d15867[259]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[16] = SG_WORD(sg__rc_cgen15752.d15867[2]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[20] = SG_WORD(sg__rc_cgen15752.d15867[13]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[729]))[24] = SG_WORD(sg__rc_cgen15752.d15867[2]);
  sg__rc_cgen15752.d15867[264] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[264],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[265] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[265],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[266] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[266],SG_SYMBOL(sg__rc_cgen15752.d15867[250]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .bar */
  sg__rc_cgen15752.d15867[267] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[267],SG_SYMBOL(sg__rc_cgen15752.d15867[128]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* free-identifier=? */
  sg__rc_cgen15752.d15867[268] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[268],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[269] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[269],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[270] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[272] = SG_MAKE_STRING("p1env-lookup");
  sg__rc_cgen15752.d15867[271] = Sg_Intern(sg__rc_cgen15752.d15867[272]); /* p1env-lookup */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[270],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[273] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[273],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[275] = SG_MAKE_STRING("bar?");
  sg__rc_cgen15752.d15867[274] = Sg_Intern(sg__rc_cgen15752.d15867[275]); /* bar? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[25]))->name = sg__rc_cgen15752.d15867[274];/* bar? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[4] = SG_WORD(sg__rc_cgen15752.d15867[265]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[11] = SG_WORD(sg__rc_cgen15752.d15867[266]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[13] = SG_WORD(sg__rc_cgen15752.d15867[267]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[21] = SG_WORD(sg__rc_cgen15752.d15867[268]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[31] = SG_WORD(sg__rc_cgen15752.d15867[269]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[36] = SG_WORD(sg__rc_cgen15752.d15867[270]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[39] = SG_WORD(sg__rc_cgen15752.d15867[273]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[756]))[44] = SG_WORD(sg__rc_cgen15752.d15867[245]);
  sg__rc_cgen15752.d15867[276] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[276],SG_SYMBOL(sg__rc_cgen15752.d15867[274]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bar? */
  sg__rc_cgen15752.d15867[277] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[277],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[278] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[278],SG_SYMBOL(sg__rc_cgen15752.d15867[256]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .ellipsis */
  sg__rc_cgen15752.d15867[279] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[279],SG_SYMBOL(sg__rc_cgen15752.d15867[128]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* free-identifier=? */
  sg__rc_cgen15752.d15867[280] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[280],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[281] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[281],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[282] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[282],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[283] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[283],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[26]))->name = sg__rc_cgen15752.d15867[20];/* ellipsis? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[4] = SG_WORD(sg__rc_cgen15752.d15867[277]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[11] = SG_WORD(sg__rc_cgen15752.d15867[278]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[13] = SG_WORD(sg__rc_cgen15752.d15867[279]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[21] = SG_WORD(sg__rc_cgen15752.d15867[280]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[31] = SG_WORD(sg__rc_cgen15752.d15867[281]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[36] = SG_WORD(sg__rc_cgen15752.d15867[282]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[39] = SG_WORD(sg__rc_cgen15752.d15867[283]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[806]))[44] = SG_WORD(sg__rc_cgen15752.d15867[252]);
  sg__rc_cgen15752.d15867[284] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[284],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[285] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[285],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[287] = SG_MAKE_STRING("ellipsis-pair?");
  sg__rc_cgen15752.d15867[286] = Sg_Intern(sg__rc_cgen15752.d15867[287]); /* ellipsis-pair? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[27]))->name = sg__rc_cgen15752.d15867[286];/* ellipsis-pair? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[856]))[12] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  sg__rc_cgen15752.d15867[288] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[288],SG_SYMBOL(sg__rc_cgen15752.d15867[286]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis-pair? */
  sg__rc_cgen15752.d15867[289] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[289],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[290] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[292] = SG_MAKE_STRING("caddr");
  sg__rc_cgen15752.d15867[291] = Sg_Intern(sg__rc_cgen15752.d15867[292]); /* caddr */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[290],SG_SYMBOL(sg__rc_cgen15752.d15867[291]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* caddr */
  sg__rc_cgen15752.d15867[293] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[293],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[295] = SG_MAKE_STRING("ellipsis-splicing-pair?");
  sg__rc_cgen15752.d15867[294] = Sg_Intern(sg__rc_cgen15752.d15867[295]); /* ellipsis-splicing-pair? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[28]))->name = sg__rc_cgen15752.d15867[294];/* ellipsis-splicing-pair? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[870]))[14] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[870]))[26] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[870]))[29] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  sg__rc_cgen15752.d15867[296] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[296],SG_SYMBOL(sg__rc_cgen15752.d15867[294]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis-splicing-pair? */
  sg__rc_cgen15752.d15867[297] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[297],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[299] = SG_MAKE_STRING("ellipsis-quote?");
  sg__rc_cgen15752.d15867[298] = Sg_Intern(sg__rc_cgen15752.d15867[299]); /* ellipsis-quote? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[29]))->name = sg__rc_cgen15752.d15867[298];/* ellipsis-quote? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[901]))[8] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  sg__rc_cgen15752.d15867[300] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[300],SG_SYMBOL(sg__rc_cgen15752.d15867[298]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis-quote? */
  sg__rc_cgen15752.d15867[301] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[301],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[302] = SG_MAKE_STRING("syntax pattern");
  sg__rc_cgen15752.d15867[303] = SG_MAKE_STRING("improper use of ellipsis");
  sg__rc_cgen15752.d15867[304] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[304],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[305] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[307] = SG_MAKE_STRING("syntax-violation");
  sg__rc_cgen15752.d15867[306] = Sg_Intern(sg__rc_cgen15752.d15867[307]); /* syntax-violation */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[305],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[308] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[308],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[309] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[309],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[310] = SG_MAKE_STRING("ellipsis following literal");
  sg__rc_cgen15752.d15867[311] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[311],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[312] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[312],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[313] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[313],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[314] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[314],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[315] = SG_MAKE_STRING("ambiguous use of ellipsis");
  sg__rc_cgen15752.d15867[316] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[316],SG_SYMBOL(sg__rc_cgen15752.d15867[199]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assertion-violation */
  sg__rc_cgen15752.d15867[317] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[317],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[319] = SG_MAKE_STRING("loop");
  sg__rc_cgen15752.d15867[318] = Sg_Intern(sg__rc_cgen15752.d15867[319]); /* loop */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[30]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[4] = SG_WORD(sg__rc_cgen15752.d15867[301]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[8] = SG_WORD(sg__rc_cgen15752.d15867[302]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[10] = SG_WORD(sg__rc_cgen15752.d15867[303]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[15] = SG_WORD(sg__rc_cgen15752.d15867[304]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[18] = SG_WORD(sg__rc_cgen15752.d15867[305]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[34] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[41] = SG_WORD(sg__rc_cgen15752.d15867[308]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[49] = SG_WORD(sg__rc_cgen15752.d15867[309]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[55] = SG_WORD(sg__rc_cgen15752.d15867[302]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[57] = SG_WORD(sg__rc_cgen15752.d15867[310]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[62] = SG_WORD(sg__rc_cgen15752.d15867[311]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[68] = SG_WORD(sg__rc_cgen15752.d15867[312]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[71] = SG_WORD(sg__rc_cgen15752.d15867[313]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[83] = SG_WORD(sg__rc_cgen15752.d15867[314]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[87] = SG_WORD(sg__rc_cgen15752.d15867[302]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[89] = SG_WORD(sg__rc_cgen15752.d15867[315]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[92] = SG_WORD(sg__rc_cgen15752.d15867[316]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[920]))[123] = SG_WORD(sg__rc_cgen15752.d15867[317]);
  sg__rc_cgen15752.d15867[320] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[320],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[321] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[321],SG_SYMBOL(sg__rc_cgen15752.d15867[274]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bar? */
  sg__rc_cgen15752.d15867[322] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[322],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[323] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[323],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[324] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[324],SG_SYMBOL(sg__rc_cgen15752.d15867[26]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* memq */
  sg__rc_cgen15752.d15867[325] = SG_MAKE_STRING("duplicate pattern variables");
  sg__rc_cgen15752.d15867[326] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[326],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[327] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[327],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[328] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[328],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[329] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[329],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[31]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[19] = SG_WORD(sg__rc_cgen15752.d15867[320]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[28] = SG_WORD(sg__rc_cgen15752.d15867[321]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[37] = SG_WORD(sg__rc_cgen15752.d15867[322]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[45] = SG_WORD(sg__rc_cgen15752.d15867[323]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[55] = SG_WORD(sg__rc_cgen15752.d15867[324]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[59] = SG_WORD(sg__rc_cgen15752.d15867[302]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[61] = SG_WORD(sg__rc_cgen15752.d15867[325]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[66] = SG_WORD(sg__rc_cgen15752.d15867[326]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[72] = SG_WORD(sg__rc_cgen15752.d15867[327]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[75] = SG_WORD(sg__rc_cgen15752.d15867[328]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1054]))[89] = SG_WORD(sg__rc_cgen15752.d15867[329]);
  sg__rc_cgen15752.d15867[331] = SG_MAKE_STRING("check-pattern");
  sg__rc_cgen15752.d15867[330] = Sg_Intern(sg__rc_cgen15752.d15867[331]); /* check-pattern */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[32]))->name = sg__rc_cgen15752.d15867[330];/* check-pattern */
  sg__rc_cgen15752.d15867[332] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[332],SG_SYMBOL(sg__rc_cgen15752.d15867[330]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* check-pattern */
  sg__rc_cgen15752.d15867[334] = SG_MAKE_STRING("match-syntax-case");
  sg__rc_cgen15752.d15867[333] = Sg_Intern(sg__rc_cgen15752.d15867[334]); /* match-syntax-case */
  sg__rc_cgen15752.d15867[335] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[335],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[336] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[338] = SG_MAKE_STRING(".match-syntax-case");
  sg__rc_cgen15752.d15867[337] = Sg_Intern(sg__rc_cgen15752.d15867[338]); /* .match-syntax-case */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[336],SG_SYMBOL(sg__rc_cgen15752.d15867[337]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .match-syntax-case */
  sg__rc_cgen15752.d15867[340] = SG_MAKE_STRING("list");
  sg__rc_cgen15752.d15867[339] = Sg_Intern(sg__rc_cgen15752.d15867[340]); /* list */
  sg__rc_cgen15752.d15867[341] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[341],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[342] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[344] = SG_MAKE_STRING(".list");
  sg__rc_cgen15752.d15867[343] = Sg_Intern(sg__rc_cgen15752.d15867[344]); /* .list */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[342],SG_SYMBOL(sg__rc_cgen15752.d15867[343]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .list */
  sg__rc_cgen15752.d15867[346] = SG_MAKE_STRING("lambda");
  sg__rc_cgen15752.d15867[345] = Sg_Intern(sg__rc_cgen15752.d15867[346]); /* lambda */
  sg__rc_cgen15752.d15867[347] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[347],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[348] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[350] = SG_MAKE_STRING(".lambda");
  sg__rc_cgen15752.d15867[349] = Sg_Intern(sg__rc_cgen15752.d15867[350]); /* .lambda */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[348],SG_SYMBOL(sg__rc_cgen15752.d15867[349]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .lambda */
  sg__rc_cgen15752.d15867[352] = SG_MAKE_STRING("collect-unique-ids");
  sg__rc_cgen15752.d15867[351] = Sg_Intern(sg__rc_cgen15752.d15867[352]); /* collect-unique-ids */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[33]))->name = sg__rc_cgen15752.d15867[351];/* collect-unique-ids */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1180]))[2] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1180]))[8] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  sg__rc_cgen15752.d15867[353] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[353],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[354] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[354],SG_SYMBOL(sg__rc_cgen15752.d15867[274]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bar? */
  sg__rc_cgen15752.d15867[355] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[355],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[356] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[356],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[357] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[357],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[358] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[360] = SG_MAKE_STRING("collect-vars-ranks");
  sg__rc_cgen15752.d15867[359] = Sg_Intern(sg__rc_cgen15752.d15867[360]); /* collect-vars-ranks */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[358],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[361] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[361],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[362] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[362],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[363] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[363],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[364] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[364],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[365] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[365],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[34]))->name = sg__rc_cgen15752.d15867[359];/* collect-vars-ranks */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[4] = SG_WORD(sg__rc_cgen15752.d15867[354]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[13] = SG_WORD(sg__rc_cgen15752.d15867[355]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[21] = SG_WORD(sg__rc_cgen15752.d15867[356]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[46] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[58] = SG_WORD(sg__rc_cgen15752.d15867[357]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[78] = SG_WORD(sg__rc_cgen15752.d15867[358]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[81] = SG_WORD(sg__rc_cgen15752.d15867[361]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[97] = SG_WORD(sg__rc_cgen15752.d15867[362]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[100] = SG_WORD(sg__rc_cgen15752.d15867[363]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[110] = SG_WORD(sg__rc_cgen15752.d15867[364]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1191]))[116] = SG_WORD(sg__rc_cgen15752.d15867[365]);
  sg__rc_cgen15752.d15867[366] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[366],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[368] = SG_MAKE_STRING("syntax-quote");
  sg__rc_cgen15752.d15867[367] = Sg_Intern(sg__rc_cgen15752.d15867[368]); /* syntax-quote */
  sg__rc_cgen15752.d15867[371] = SG_MAKE_STRING("sagittarius");
  sg__rc_cgen15752.d15867[370] = Sg_Intern(sg__rc_cgen15752.d15867[371]); /* sagittarius */
  sg__rc_cgen15752.d15867[373] = SG_MAKE_STRING("compiler");
  sg__rc_cgen15752.d15867[372] = Sg_Intern(sg__rc_cgen15752.d15867[373]); /* compiler */
  do {
    /* (sagittarius compiler) */ 
    SgObject G15873 = SG_NIL, G15874 = SG_NIL;
    SG_APPEND1(G15873, G15874, sg__rc_cgen15752.d15867[370]); /* sagittarius */ 
    SG_APPEND1(G15873, G15874, sg__rc_cgen15752.d15867[372]); /* compiler */ 
    sg__rc_cgen15752.d15867[369] = G15873;
  } while (0);
  sg__rc_cgen15752.d15867[374] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[374],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[375] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[377] = SG_MAKE_STRING("syntax-quote.");
  sg__rc_cgen15752.d15867[376] = Sg_Intern(sg__rc_cgen15752.d15867[377]); /* syntax-quote. */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[375],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[378] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[380] = SG_MAKE_STRING("hashtable-ref");
  sg__rc_cgen15752.d15867[379] = Sg_Intern(sg__rc_cgen15752.d15867[380]); /* hashtable-ref */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[378],SG_SYMBOL(sg__rc_cgen15752.d15867[379]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-ref */
  sg__rc_cgen15752.d15867[381] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[381],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  sg__rc_cgen15752.d15867[383] = SG_MAKE_STRING("seen-or-gen");
  sg__rc_cgen15752.d15867[382] = Sg_Intern(sg__rc_cgen15752.d15867[383]); /* seen-or-gen */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[35]))->name = sg__rc_cgen15752.d15867[382];/* seen-or-gen */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1317]))[7] = SG_WORD(sg__rc_cgen15752.d15867[378]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1317]))[25] = SG_WORD(sg__rc_cgen15752.d15867[381]);
  sg__rc_cgen15752.d15867[384] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[386] = SG_MAKE_STRING("vector-copy");
  sg__rc_cgen15752.d15867[385] = Sg_Intern(sg__rc_cgen15752.d15867[386]); /* vector-copy */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[384],SG_SYMBOL(sg__rc_cgen15752.d15867[385]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector-copy */
  sg__rc_cgen15752.d15867[387] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[387],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[388] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[388],SG_SYMBOL(sg__rc_cgen15752.d15867[173]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-library */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[36]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1345]))[85] = SG_WORD(sg__rc_cgen15752.d15867[384]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1345]))[103] = SG_WORD(sg__rc_cgen15752.d15867[387]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1345]))[119] = SG_WORD(sg__rc_cgen15752.d15867[388]);
  sg__rc_cgen15752.d15867[390] = SG_MAKE_STRING("rewrite-form");
  sg__rc_cgen15752.d15867[389] = Sg_Intern(sg__rc_cgen15752.d15867[390]); /* rewrite-form */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[37]))->name = sg__rc_cgen15752.d15867[389];/* rewrite-form */
  sg__rc_cgen15752.d15867[391] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[391],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  sg__rc_cgen15752.d15867[392] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[392],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[393] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[395] = SG_MAKE_STRING("number?");
  sg__rc_cgen15752.d15867[394] = Sg_Intern(sg__rc_cgen15752.d15867[395]); /* number? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[393],SG_SYMBOL(sg__rc_cgen15752.d15867[394]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* number? */
  sg__rc_cgen15752.d15867[397] = SG_MAKE_STRING("pattern-identifier?");
  sg__rc_cgen15752.d15867[396] = Sg_Intern(sg__rc_cgen15752.d15867[397]); /* pattern-identifier? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[38]))->name = sg__rc_cgen15752.d15867[396];/* pattern-identifier? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1503]))[6] = SG_WORD(sg__rc_cgen15752.d15867[392]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1503]))[9] = SG_WORD(sg__rc_cgen15752.d15867[393]);
  sg__rc_cgen15752.d15867[398] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[398],SG_SYMBOL(sg__rc_cgen15752.d15867[396]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* pattern-identifier? */
  sg__rc_cgen15752.d15867[399] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[399],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[400] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[400],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[401] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[401],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[402] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[402],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[403] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[405] = SG_MAKE_STRING("make-eq-hashtable");
  sg__rc_cgen15752.d15867[404] = Sg_Intern(sg__rc_cgen15752.d15867[405]); /* make-eq-hashtable */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[403],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[406] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[406],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  sg__rc_cgen15752.d15867[407] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[407],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[408] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[408],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  sg__rc_cgen15752.d15867[409] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[409],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1514]))[4] = SG_WORD(sg__rc_cgen15752.d15867[409]);
  sg__rc_cgen15752.d15867[410] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[412] = SG_MAKE_STRING("for-each");
  sg__rc_cgen15752.d15867[411] = Sg_Intern(sg__rc_cgen15752.d15867[412]); /* for-each */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[410],SG_SYMBOL(sg__rc_cgen15752.d15867[411]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* for-each */
  sg__rc_cgen15752.d15867[413] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[415] = SG_MAKE_STRING("pending-identifier?");
  sg__rc_cgen15752.d15867[414] = Sg_Intern(sg__rc_cgen15752.d15867[415]); /* pending-identifier? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[413],SG_SYMBOL(sg__rc_cgen15752.d15867[414]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* pending-identifier? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1520]))[4] = SG_WORD(sg__rc_cgen15752.d15867[413]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1520]))[16] = SG_WORD(sg__rc_cgen15752.d15867[392]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1520]))[19] = SG_WORD(sg__rc_cgen15752.d15867[393]);
  sg__rc_cgen15752.d15867[416] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[416],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  sg__rc_cgen15752.d15867[418] = SG_MAKE_STRING("rewrite");
  sg__rc_cgen15752.d15867[417] = Sg_Intern(sg__rc_cgen15752.d15867[418]); /* rewrite */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[41]))->name = sg__rc_cgen15752.d15867[417];/* rewrite */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1542]))[4] = SG_WORD(sg__rc_cgen15752.d15867[407]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1542]))[20] = SG_WORD(sg__rc_cgen15752.d15867[408]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1542]))[36] = SG_WORD(sg__rc_cgen15752.d15867[410]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1542]))[42] = SG_WORD(sg__rc_cgen15752.d15867[49]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1542]))[48] = SG_WORD(sg__rc_cgen15752.d15867[416]);
  sg__rc_cgen15752.d15867[419] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[419],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[421] = SG_MAKE_STRING("gen-patvar");
  sg__rc_cgen15752.d15867[420] = Sg_Intern(sg__rc_cgen15752.d15867[421]); /* gen-patvar */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[42]))->name = sg__rc_cgen15752.d15867[420];/* gen-patvar */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1592]))[8] = SG_WORD(sg__rc_cgen15752.d15867[419]);
  sg__rc_cgen15752.d15867[422] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[422],SG_SYMBOL(sg__rc_cgen15752.d15867[330]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* check-pattern */
  sg__rc_cgen15752.d15867[423] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[423],SG_SYMBOL(sg__rc_cgen15752.d15867[359]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-vars-ranks */
  sg__rc_cgen15752.d15867[424] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[426] = SG_MAKE_STRING("map");
  sg__rc_cgen15752.d15867[425] = Sg_Intern(sg__rc_cgen15752.d15867[426]); /* map */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[424],SG_SYMBOL(sg__rc_cgen15752.d15867[425]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* map */
  sg__rc_cgen15752.d15867[428] = SG_MAKE_STRING("parse-pattern");
  sg__rc_cgen15752.d15867[427] = Sg_Intern(sg__rc_cgen15752.d15867[428]); /* parse-pattern */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[43]))->name = sg__rc_cgen15752.d15867[427];/* parse-pattern */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1603]))[11] = SG_WORD(sg__rc_cgen15752.d15867[422]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1603]))[20] = SG_WORD(sg__rc_cgen15752.d15867[423]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1603]))[27] = SG_WORD(sg__rc_cgen15752.d15867[424]);
  sg__rc_cgen15752.d15867[429] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[431] = SG_MAKE_STRING("list?");
  sg__rc_cgen15752.d15867[430] = Sg_Intern(sg__rc_cgen15752.d15867[431]); /* list? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[429],SG_SYMBOL(sg__rc_cgen15752.d15867[430]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list? */
  sg__rc_cgen15752.d15867[432] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[432],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[433] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[435] = SG_MAKE_STRING("for-all");
  sg__rc_cgen15752.d15867[434] = Sg_Intern(sg__rc_cgen15752.d15867[435]); /* for-all */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[433],SG_SYMBOL(sg__rc_cgen15752.d15867[434]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* for-all */
  sg__rc_cgen15752.d15867[437] = SG_MAKE_STRING("syntax-case");
  sg__rc_cgen15752.d15867[436] = Sg_Intern(sg__rc_cgen15752.d15867[437]); /* syntax-case */
  sg__rc_cgen15752.d15867[438] = SG_MAKE_STRING("invalid literals");
  sg__rc_cgen15752.d15867[439] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[439],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[440] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[442] = SG_MAKE_STRING("unique-id-list?");
  sg__rc_cgen15752.d15867[441] = Sg_Intern(sg__rc_cgen15752.d15867[442]); /* unique-id-list? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[440],SG_SYMBOL(sg__rc_cgen15752.d15867[441]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unique-id-list? */
  sg__rc_cgen15752.d15867[443] = SG_MAKE_STRING("duplicate literals");
  sg__rc_cgen15752.d15867[444] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[444],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[445] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[445],SG_SYMBOL(sg__rc_cgen15752.d15867[250]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .bar */
  sg__rc_cgen15752.d15867[446] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[446],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[447] = SG_MAKE_STRING("_ in literals");
  sg__rc_cgen15752.d15867[448] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[448],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[449] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[449],SG_SYMBOL(sg__rc_cgen15752.d15867[256]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .ellipsis */
  sg__rc_cgen15752.d15867[450] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[450],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[451] = SG_MAKE_STRING("... in literals");
  sg__rc_cgen15752.d15867[452] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[452],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[453] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[453],SG_SYMBOL(sg__rc_cgen15752.d15867[343]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .list */
  sg__rc_cgen15752.d15867[454] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[454],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[455] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[455],SG_SYMBOL(sg__rc_cgen15752.d15867[349]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .lambda */
  sg__rc_cgen15752.d15867[456] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[456],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[457] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[457],SG_SYMBOL(sg__rc_cgen15752.d15867[349]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .lambda */
  sg__rc_cgen15752.d15867[458] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[458],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[459] = SG_MAKE_STRING("invalid form");
  sg__rc_cgen15752.d15867[460] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[462] = SG_MAKE_STRING("syntax-error");
  sg__rc_cgen15752.d15867[461] = Sg_Intern(sg__rc_cgen15752.d15867[462]); /* syntax-error */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[460],SG_SYMBOL(sg__rc_cgen15752.d15867[461]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-error */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[31] = SG_WORD(sg__rc_cgen15752.d15867[453]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[33] = SG_WORD(sg__rc_cgen15752.d15867[454]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[38] = SG_WORD(sg__rc_cgen15752.d15867[455]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[40] = SG_WORD(sg__rc_cgen15752.d15867[456]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[53] = SG_WORD(sg__rc_cgen15752.d15867[457]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[55] = SG_WORD(sg__rc_cgen15752.d15867[458]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[72] = SG_WORD(sg__rc_cgen15752.d15867[459]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[75] = SG_WORD(sg__rc_cgen15752.d15867[460]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[78] = SG_WORD(sg__rc_cgen15752.d15867[459]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[81] = SG_WORD(sg__rc_cgen15752.d15867[460]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[84] = SG_WORD(sg__rc_cgen15752.d15867[459]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[87] = SG_WORD(sg__rc_cgen15752.d15867[460]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[90] = SG_WORD(sg__rc_cgen15752.d15867[459]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1664]))[93] = SG_WORD(sg__rc_cgen15752.d15867[460]);
  sg__rc_cgen15752.d15867[463] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[463],SG_SYMBOL(sg__rc_cgen15752.d15867[343]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .list */
  sg__rc_cgen15752.d15867[464] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[464],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[465] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[465],SG_SYMBOL(sg__rc_cgen15752.d15867[349]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .lambda */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1759]))[30] = SG_WORD(sg__rc_cgen15752.d15867[463]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1759]))[32] = SG_WORD(sg__rc_cgen15752.d15867[464]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1759]))[39] = SG_WORD(sg__rc_cgen15752.d15867[465]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1759]))[41] = SG_WORD(sg__rc_cgen15752.d15867[244]);
  sg__rc_cgen15752.d15867[466] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[466],SG_SYMBOL(sg__rc_cgen15752.d15867[425]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* map */
  sg__rc_cgen15752.d15867[467] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[467],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[468] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[468],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[469] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[469],SG_SYMBOL(sg__rc_cgen15752.d15867[337]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .match-syntax-case */
  sg__rc_cgen15752.d15867[470] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[470],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[471] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[471],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[473] = SG_MAKE_STRING("compile-syntax-case");
  sg__rc_cgen15752.d15867[472] = Sg_Intern(sg__rc_cgen15752.d15867[473]); /* compile-syntax-case */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[46]))->name = sg__rc_cgen15752.d15867[472];/* compile-syntax-case */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[4] = SG_WORD(sg__rc_cgen15752.d15867[399]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[9] = SG_WORD(sg__rc_cgen15752.d15867[400]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[15] = SG_WORD(sg__rc_cgen15752.d15867[401]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[20] = SG_WORD(sg__rc_cgen15752.d15867[402]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[25] = SG_WORD(sg__rc_cgen15752.d15867[403]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[34] = SG_WORD(sg__rc_cgen15752.d15867[38]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[36] = SG_WORD(sg__rc_cgen15752.d15867[43]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[38] = SG_WORD(sg__rc_cgen15752.d15867[406]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[62] = SG_WORD(sg__rc_cgen15752.d15867[429]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[68] = SG_WORD(sg__rc_cgen15752.d15867[432]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[71] = SG_WORD(sg__rc_cgen15752.d15867[433]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[79] = SG_WORD(sg__rc_cgen15752.d15867[436]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[81] = SG_WORD(sg__rc_cgen15752.d15867[438]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[85] = SG_WORD(sg__rc_cgen15752.d15867[439]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[94] = SG_WORD(sg__rc_cgen15752.d15867[440]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[102] = SG_WORD(sg__rc_cgen15752.d15867[436]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[104] = SG_WORD(sg__rc_cgen15752.d15867[443]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[108] = SG_WORD(sg__rc_cgen15752.d15867[444]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[112] = SG_WORD(sg__rc_cgen15752.d15867[445]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[115] = SG_WORD(sg__rc_cgen15752.d15867[446]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[121] = SG_WORD(sg__rc_cgen15752.d15867[436]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[123] = SG_WORD(sg__rc_cgen15752.d15867[447]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[127] = SG_WORD(sg__rc_cgen15752.d15867[448]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[131] = SG_WORD(sg__rc_cgen15752.d15867[449]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[134] = SG_WORD(sg__rc_cgen15752.d15867[450]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[140] = SG_WORD(sg__rc_cgen15752.d15867[436]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[142] = SG_WORD(sg__rc_cgen15752.d15867[451]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[146] = SG_WORD(sg__rc_cgen15752.d15867[452]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[156] = SG_WORD(sg__rc_cgen15752.d15867[466]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[162] = SG_WORD(sg__rc_cgen15752.d15867[467]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[167] = SG_WORD(sg__rc_cgen15752.d15867[468]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[169] = SG_WORD(sg__rc_cgen15752.d15867[469]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[175] = SG_WORD(sg__rc_cgen15752.d15867[470]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[1825]))[179] = SG_WORD(sg__rc_cgen15752.d15867[471]);
  sg__rc_cgen15752.d15867[474] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[474],SG_SYMBOL(sg__rc_cgen15752.d15867[472]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* compile-syntax-case */
  sg__rc_cgen15752.d15867[476] = SG_MAKE_STRING("count-pair");
  sg__rc_cgen15752.d15867[475] = Sg_Intern(sg__rc_cgen15752.d15867[476]); /* count-pair */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[47]))->name = sg__rc_cgen15752.d15867[475];/* count-pair */
  sg__rc_cgen15752.d15867[477] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[477],SG_SYMBOL(sg__rc_cgen15752.d15867[475]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* count-pair */
  sg__rc_cgen15752.d15867[478] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[480] = SG_MAKE_STRING("match-pattern?");
  sg__rc_cgen15752.d15867[479] = Sg_Intern(sg__rc_cgen15752.d15867[480]); /* match-pattern? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[478],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[481] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[483] = SG_MAKE_STRING("match-ellipsis?");
  sg__rc_cgen15752.d15867[482] = Sg_Intern(sg__rc_cgen15752.d15867[483]); /* match-ellipsis? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[481],SG_SYMBOL(sg__rc_cgen15752.d15867[482]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[48]))->name = sg__rc_cgen15752.d15867[482];/* match-ellipsis? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2025]))[14] = SG_WORD(sg__rc_cgen15752.d15867[478]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2025]))[21] = SG_WORD(sg__rc_cgen15752.d15867[481]);
  sg__rc_cgen15752.d15867[484] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[484],SG_SYMBOL(sg__rc_cgen15752.d15867[482]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis? */
  sg__rc_cgen15752.d15867[485] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[485],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[486] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[488] = SG_MAKE_STRING("match-ellipsis-n?");
  sg__rc_cgen15752.d15867[487] = Sg_Intern(sg__rc_cgen15752.d15867[488]); /* match-ellipsis-n? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[486],SG_SYMBOL(sg__rc_cgen15752.d15867[487]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis-n? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[49]))->name = sg__rc_cgen15752.d15867[487];/* match-ellipsis-n? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2048]))[15] = SG_WORD(sg__rc_cgen15752.d15867[485]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2048]))[25] = SG_WORD(sg__rc_cgen15752.d15867[486]);
  sg__rc_cgen15752.d15867[489] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[489],SG_SYMBOL(sg__rc_cgen15752.d15867[487]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis-n? */
  sg__rc_cgen15752.d15867[490] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[490],SG_SYMBOL(sg__rc_cgen15752.d15867[274]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bar? */
  sg__rc_cgen15752.d15867[491] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[491],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[492] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[492],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[493] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[493],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[494] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[494],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[495] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[495],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[496] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[496],SG_SYMBOL(sg__rc_cgen15752.d15867[128]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* free-identifier=? */
  sg__rc_cgen15752.d15867[497] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[497],SG_SYMBOL(sg__rc_cgen15752.d15867[430]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list? */
  sg__rc_cgen15752.d15867[498] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[498],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[499] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[499],SG_SYMBOL(sg__rc_cgen15752.d15867[482]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis? */
  sg__rc_cgen15752.d15867[500] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[500],SG_SYMBOL(sg__rc_cgen15752.d15867[475]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* count-pair */
  sg__rc_cgen15752.d15867[501] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[501],SG_SYMBOL(sg__rc_cgen15752.d15867[475]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* count-pair */
  sg__rc_cgen15752.d15867[502] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[502],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[503] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[503],SG_SYMBOL(sg__rc_cgen15752.d15867[487]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-ellipsis-n? */
  sg__rc_cgen15752.d15867[504] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[506] = SG_MAKE_STRING("list-tail");
  sg__rc_cgen15752.d15867[505] = Sg_Intern(sg__rc_cgen15752.d15867[506]); /* list-tail */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[504],SG_SYMBOL(sg__rc_cgen15752.d15867[505]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list-tail */
  sg__rc_cgen15752.d15867[507] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[507],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[508] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[508],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[509] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[509],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[510] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[510],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[511] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[511],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[512] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[512],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[513] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[513],SG_SYMBOL(sg__rc_cgen15752.d15867[131]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* equal? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[50]))->name = sg__rc_cgen15752.d15867[479];/* match-pattern? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[4] = SG_WORD(sg__rc_cgen15752.d15867[490]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[13] = SG_WORD(sg__rc_cgen15752.d15867[491]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[21] = SG_WORD(sg__rc_cgen15752.d15867[492]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[28] = SG_WORD(sg__rc_cgen15752.d15867[493]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[35] = SG_WORD(sg__rc_cgen15752.d15867[58]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[43] = SG_WORD(sg__rc_cgen15752.d15867[494]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[46] = SG_WORD(sg__rc_cgen15752.d15867[58]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[55] = SG_WORD(sg__rc_cgen15752.d15867[495]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[58] = SG_WORD(sg__rc_cgen15752.d15867[58]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[64] = SG_WORD(sg__rc_cgen15752.d15867[496]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[82] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[93] = SG_WORD(sg__rc_cgen15752.d15867[497]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[100] = SG_WORD(sg__rc_cgen15752.d15867[498]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[108] = SG_WORD(sg__rc_cgen15752.d15867[499]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[114] = SG_WORD(sg__rc_cgen15752.d15867[500]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[122] = SG_WORD(sg__rc_cgen15752.d15867[501]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[135] = SG_WORD(sg__rc_cgen15752.d15867[502]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[148] = SG_WORD(sg__rc_cgen15752.d15867[503]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[156] = SG_WORD(sg__rc_cgen15752.d15867[504]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[163] = SG_WORD(sg__rc_cgen15752.d15867[507]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[182] = SG_WORD(sg__rc_cgen15752.d15867[508]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[189] = SG_WORD(sg__rc_cgen15752.d15867[509]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[203] = SG_WORD(sg__rc_cgen15752.d15867[510]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[209] = SG_WORD(sg__rc_cgen15752.d15867[511]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[213] = SG_WORD(sg__rc_cgen15752.d15867[512]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2075]))[218] = SG_WORD(sg__rc_cgen15752.d15867[513]);
  sg__rc_cgen15752.d15867[514] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[514],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[515] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[517] = SG_MAKE_STRING("reverse");
  sg__rc_cgen15752.d15867[516] = Sg_Intern(sg__rc_cgen15752.d15867[517]); /* reverse */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[515],SG_SYMBOL(sg__rc_cgen15752.d15867[516]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* reverse */
  sg__rc_cgen15752.d15867[518] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[520] = SG_MAKE_STRING("bind-var!");
  sg__rc_cgen15752.d15867[519] = Sg_Intern(sg__rc_cgen15752.d15867[520]); /* bind-var! */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[518],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[521] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[523] = SG_MAKE_STRING("union-vars");
  sg__rc_cgen15752.d15867[522] = Sg_Intern(sg__rc_cgen15752.d15867[523]); /* union-vars */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[521],SG_SYMBOL(sg__rc_cgen15752.d15867[522]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* union-vars */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[51]))->name = sg__rc_cgen15752.d15867[522];/* union-vars */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2301]))[16] = SG_WORD(sg__rc_cgen15752.d15867[515]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2301]))[20] = SG_WORD(sg__rc_cgen15752.d15867[518]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2301]))[24] = SG_WORD(sg__rc_cgen15752.d15867[521]);
  sg__rc_cgen15752.d15867[524] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[524],SG_SYMBOL(sg__rc_cgen15752.d15867[522]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* union-vars */
  sg__rc_cgen15752.d15867[525] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[525],SG_SYMBOL(sg__rc_cgen15752.d15867[274]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bar? */
  sg__rc_cgen15752.d15867[526] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[526],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[52]))->name = sg__rc_cgen15752.d15867[519];/* bind-var! */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2327]))[4] = SG_WORD(sg__rc_cgen15752.d15867[525]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2327]))[14] = SG_WORD(sg__rc_cgen15752.d15867[526]);
  sg__rc_cgen15752.d15867[527] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[527],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[528] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[528],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[529] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[529],SG_SYMBOL(sg__rc_cgen15752.d15867[26]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* memq */
  sg__rc_cgen15752.d15867[530] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[530],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[532] = SG_MAKE_STRING("bind-null-ellipsis");
  sg__rc_cgen15752.d15867[531] = Sg_Intern(sg__rc_cgen15752.d15867[532]); /* bind-null-ellipsis */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[53]))->name = sg__rc_cgen15752.d15867[531];/* bind-null-ellipsis */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2360]))[4] = SG_WORD(sg__rc_cgen15752.d15867[528]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2360]))[18] = SG_WORD(sg__rc_cgen15752.d15867[529]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2360]))[31] = SG_WORD(sg__rc_cgen15752.d15867[530]);
  sg__rc_cgen15752.d15867[533] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[533],SG_SYMBOL(sg__rc_cgen15752.d15867[531]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-null-ellipsis */
  sg__rc_cgen15752.d15867[534] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[534],SG_SYMBOL(sg__rc_cgen15752.d15867[531]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-null-ellipsis */
  sg__rc_cgen15752.d15867[535] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[535],SG_SYMBOL(sg__rc_cgen15752.d15867[522]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* union-vars */
  sg__rc_cgen15752.d15867[536] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[538] = SG_MAKE_STRING("bind-pattern");
  sg__rc_cgen15752.d15867[537] = Sg_Intern(sg__rc_cgen15752.d15867[538]); /* bind-pattern */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[536],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[539] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[541] = SG_MAKE_STRING("bind-ellipsis");
  sg__rc_cgen15752.d15867[540] = Sg_Intern(sg__rc_cgen15752.d15867[541]); /* bind-ellipsis */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[539],SG_SYMBOL(sg__rc_cgen15752.d15867[540]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[54]))->name = sg__rc_cgen15752.d15867[540];/* bind-ellipsis */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2397]))[10] = SG_WORD(sg__rc_cgen15752.d15867[534]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2397]))[15] = SG_WORD(sg__rc_cgen15752.d15867[535]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2397]))[28] = SG_WORD(sg__rc_cgen15752.d15867[536]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2397]))[31] = SG_WORD(sg__rc_cgen15752.d15867[539]);
  sg__rc_cgen15752.d15867[542] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[542],SG_SYMBOL(sg__rc_cgen15752.d15867[540]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis */
  sg__rc_cgen15752.d15867[543] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[543],SG_SYMBOL(sg__rc_cgen15752.d15867[531]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-null-ellipsis */
  sg__rc_cgen15752.d15867[544] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[544],SG_SYMBOL(sg__rc_cgen15752.d15867[522]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* union-vars */
  sg__rc_cgen15752.d15867[545] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[545],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[546] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[548] = SG_MAKE_STRING("bind-ellipsis-n");
  sg__rc_cgen15752.d15867[547] = Sg_Intern(sg__rc_cgen15752.d15867[548]); /* bind-ellipsis-n */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[546],SG_SYMBOL(sg__rc_cgen15752.d15867[547]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis-n */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[55]))->name = sg__rc_cgen15752.d15867[547];/* bind-ellipsis-n */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2430]))[11] = SG_WORD(sg__rc_cgen15752.d15867[543]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2430]))[16] = SG_WORD(sg__rc_cgen15752.d15867[544]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2430]))[32] = SG_WORD(sg__rc_cgen15752.d15867[545]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2430]))[35] = SG_WORD(sg__rc_cgen15752.d15867[546]);
  sg__rc_cgen15752.d15867[549] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[549],SG_SYMBOL(sg__rc_cgen15752.d15867[547]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis-n */
  sg__rc_cgen15752.d15867[550] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[550],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[551] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[551],SG_SYMBOL(sg__rc_cgen15752.d15867[262]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* literal-match? */
  sg__rc_cgen15752.d15867[552] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[552],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[553] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[553],SG_SYMBOL(sg__rc_cgen15752.d15867[430]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list? */
  sg__rc_cgen15752.d15867[554] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[554],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[555] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[555],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[556] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[556],SG_SYMBOL(sg__rc_cgen15752.d15867[540]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis */
  sg__rc_cgen15752.d15867[557] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[557],SG_SYMBOL(sg__rc_cgen15752.d15867[475]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* count-pair */
  sg__rc_cgen15752.d15867[558] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[558],SG_SYMBOL(sg__rc_cgen15752.d15867[475]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* count-pair */
  sg__rc_cgen15752.d15867[559] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[559],SG_SYMBOL(sg__rc_cgen15752.d15867[505]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list-tail */
  sg__rc_cgen15752.d15867[560] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[560],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[561] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[561],SG_SYMBOL(sg__rc_cgen15752.d15867[519]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-var! */
  sg__rc_cgen15752.d15867[562] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[562],SG_SYMBOL(sg__rc_cgen15752.d15867[547]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-ellipsis-n */
  sg__rc_cgen15752.d15867[563] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[563],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[564] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[564],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[565] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[565],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[566] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[566],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[567] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[567],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[568] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[568],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[56]))->name = sg__rc_cgen15752.d15867[537];/* bind-pattern */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[4] = SG_WORD(sg__rc_cgen15752.d15867[550]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[12] = SG_WORD(sg__rc_cgen15752.d15867[551]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[21] = SG_WORD(sg__rc_cgen15752.d15867[552]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[37] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[48] = SG_WORD(sg__rc_cgen15752.d15867[553]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[55] = SG_WORD(sg__rc_cgen15752.d15867[554]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[62] = SG_WORD(sg__rc_cgen15752.d15867[555]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[71] = SG_WORD(sg__rc_cgen15752.d15867[556]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[77] = SG_WORD(sg__rc_cgen15752.d15867[557]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[85] = SG_WORD(sg__rc_cgen15752.d15867[558]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[93] = SG_WORD(sg__rc_cgen15752.d15867[559]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[107] = SG_WORD(sg__rc_cgen15752.d15867[560]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[117] = SG_WORD(sg__rc_cgen15752.d15867[561]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[130] = SG_WORD(sg__rc_cgen15752.d15867[562]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[137] = SG_WORD(sg__rc_cgen15752.d15867[563]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[156] = SG_WORD(sg__rc_cgen15752.d15867[564]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[159] = SG_WORD(sg__rc_cgen15752.d15867[565]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[169] = SG_WORD(sg__rc_cgen15752.d15867[566]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[175] = SG_WORD(sg__rc_cgen15752.d15867[567]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2467]))[180] = SG_WORD(sg__rc_cgen15752.d15867[568]);
  sg__rc_cgen15752.d15867[569] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[569],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  sg__rc_cgen15752.d15867[570] = SG_MAKE_STRING("invalid syntax");
  sg__rc_cgen15752.d15867[571] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[571],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[572] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[572],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[573] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[573],SG_SYMBOL(sg__rc_cgen15752.d15867[291]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* caddr */
  sg__rc_cgen15752.d15867[574] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[574],SG_SYMBOL(sg__rc_cgen15752.d15867[479]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-pattern? */
  sg__rc_cgen15752.d15867[575] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[575],SG_SYMBOL(sg__rc_cgen15752.d15867[537]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bind-pattern */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[57]))->name = sg__rc_cgen15752.d15867[333];/* match-syntax-case */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[11] = SG_WORD(sg__rc_cgen15752.d15867[570]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[16] = SG_WORD(sg__rc_cgen15752.d15867[571]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[19] = SG_WORD(sg__rc_cgen15752.d15867[572]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[30] = SG_WORD(sg__rc_cgen15752.d15867[573]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[38] = SG_WORD(sg__rc_cgen15752.d15867[574]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2657]))[49] = SG_WORD(sg__rc_cgen15752.d15867[575]);
  sg__rc_cgen15752.d15867[576] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[576],SG_SYMBOL(sg__rc_cgen15752.d15867[333]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* match-syntax-case */
  sg__rc_cgen15752.d15867[578] = SG_MAKE_STRING("expand-syntax");
  sg__rc_cgen15752.d15867[577] = Sg_Intern(sg__rc_cgen15752.d15867[578]); /* expand-syntax */
  sg__rc_cgen15752.d15867[579] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[579],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[580] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[582] = SG_MAKE_STRING(".expand-syntax");
  sg__rc_cgen15752.d15867[581] = Sg_Intern(sg__rc_cgen15752.d15867[582]); /* .expand-syntax */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[580],SG_SYMBOL(sg__rc_cgen15752.d15867[581]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .expand-syntax */
  sg__rc_cgen15752.d15867[583] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[583],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[584] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[584],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[58]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2742]))[10] = SG_WORD(sg__rc_cgen15752.d15867[584]);
  sg__rc_cgen15752.d15867[586] = SG_MAKE_STRING("collect-rename-ids");
  sg__rc_cgen15752.d15867[585] = Sg_Intern(sg__rc_cgen15752.d15867[586]); /* collect-rename-ids */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[59]))->name = sg__rc_cgen15752.d15867[585];/* collect-rename-ids */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2767]))[4] = SG_WORD(sg__rc_cgen15752.d15867[583]);
  sg__rc_cgen15752.d15867[587] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[587],SG_SYMBOL(sg__rc_cgen15752.d15867[585]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-rename-ids */
  sg__rc_cgen15752.d15867[588] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[590] = SG_MAKE_STRING("cdddr");
  sg__rc_cgen15752.d15867[589] = Sg_Intern(sg__rc_cgen15752.d15867[590]); /* cdddr */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[588],SG_SYMBOL(sg__rc_cgen15752.d15867[589]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* cdddr */
  sg__rc_cgen15752.d15867[591] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[591],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[592] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[594] = SG_MAKE_STRING("list-head");
  sg__rc_cgen15752.d15867[593] = Sg_Intern(sg__rc_cgen15752.d15867[594]); /* list-head */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[592],SG_SYMBOL(sg__rc_cgen15752.d15867[593]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list-head */
  sg__rc_cgen15752.d15867[596] = SG_MAKE_STRING("parse-ellipsis-splicing");
  sg__rc_cgen15752.d15867[595] = Sg_Intern(sg__rc_cgen15752.d15867[596]); /* parse-ellipsis-splicing */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[60]))->name = sg__rc_cgen15752.d15867[595];/* parse-ellipsis-splicing */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2783]))[5] = SG_WORD(sg__rc_cgen15752.d15867[588]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2783]))[15] = SG_WORD(sg__rc_cgen15752.d15867[591]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2783]))[31] = SG_WORD(sg__rc_cgen15752.d15867[592]);
  sg__rc_cgen15752.d15867[597] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[597],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[598] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[598],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[599] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[601] = SG_MAKE_STRING("rank-of");
  sg__rc_cgen15752.d15867[600] = Sg_Intern(sg__rc_cgen15752.d15867[601]); /* rank-of */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[599],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[602] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[602],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2823]))[5] = SG_WORD(sg__rc_cgen15752.d15867[602]);
  sg__rc_cgen15752.d15867[603] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[603],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[604] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[606] = SG_MAKE_STRING("exists");
  sg__rc_cgen15752.d15867[605] = Sg_Intern(sg__rc_cgen15752.d15867[606]); /* exists */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[604],SG_SYMBOL(sg__rc_cgen15752.d15867[605]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* exists */
  sg__rc_cgen15752.d15867[607] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[607],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[608] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[608],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[62]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[4] = SG_WORD(sg__rc_cgen15752.d15867[598]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[12] = SG_WORD(sg__rc_cgen15752.d15867[599]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[25] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[45] = SG_WORD(sg__rc_cgen15752.d15867[603]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[48] = SG_WORD(sg__rc_cgen15752.d15867[604]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[64] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[78] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[81] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[88] = SG_WORD(sg__rc_cgen15752.d15867[607]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[128] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[2833]))[183] = SG_WORD(sg__rc_cgen15752.d15867[608]);
  sg__rc_cgen15752.d15867[610] = SG_MAKE_STRING("control-patvar-exists?");
  sg__rc_cgen15752.d15867[609] = Sg_Intern(sg__rc_cgen15752.d15867[610]); /* control-patvar-exists? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[63]))->name = sg__rc_cgen15752.d15867[609];/* control-patvar-exists? */
  sg__rc_cgen15752.d15867[611] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[611],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[612] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[612],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[613] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[615] = SG_MAKE_STRING("<");
  sg__rc_cgen15752.d15867[614] = Sg_Intern(sg__rc_cgen15752.d15867[615]); /* < */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[613],SG_SYMBOL(sg__rc_cgen15752.d15867[614]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* < */
  sg__rc_cgen15752.d15867[616] = SG_MAKE_STRING("syntax template");
  sg__rc_cgen15752.d15867[617] = SG_MAKE_STRING("too few ellipsis following subtemplate");
  sg__rc_cgen15752.d15867[618] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[618],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[619] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[619],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[620] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[620],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[621] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[621],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[64]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[4] = SG_WORD(sg__rc_cgen15752.d15867[611]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[15] = SG_WORD(sg__rc_cgen15752.d15867[612]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[19] = SG_WORD(sg__rc_cgen15752.d15867[613]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[23] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[25] = SG_WORD(sg__rc_cgen15752.d15867[617]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[30] = SG_WORD(sg__rc_cgen15752.d15867[618]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[36] = SG_WORD(sg__rc_cgen15752.d15867[619]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[39] = SG_WORD(sg__rc_cgen15752.d15867[620]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3060]))[62] = SG_WORD(sg__rc_cgen15752.d15867[621]);
  sg__rc_cgen15752.d15867[623] = SG_MAKE_STRING("check-escaped");
  sg__rc_cgen15752.d15867[622] = Sg_Intern(sg__rc_cgen15752.d15867[623]); /* check-escaped */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[65]))->name = sg__rc_cgen15752.d15867[622];/* check-escaped */
  sg__rc_cgen15752.d15867[624] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[626] = SG_MAKE_STRING("length");
  sg__rc_cgen15752.d15867[625] = Sg_Intern(sg__rc_cgen15752.d15867[626]); /* length */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[624],SG_SYMBOL(sg__rc_cgen15752.d15867[625]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* length */
  sg__rc_cgen15752.d15867[627] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[627],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[628] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[628],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[629] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[629],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  sg__rc_cgen15752.d15867[630] = SG_MAKE_STRING("misplaced ellipsis");
  sg__rc_cgen15752.d15867[631] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[631],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[632] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[632],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[633] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[633],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[634] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[634],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[635] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[635],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[636] = SG_MAKE_STRING("missing pattern variable that used in same level as in pattern");
  sg__rc_cgen15752.d15867[637] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[637],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[638] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[638],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[639] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[639],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[640] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[640],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[641] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[641],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[642] = SG_MAKE_STRING("misplaced ellipsis following literal");
  sg__rc_cgen15752.d15867[643] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[643],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[644] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[644],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[645] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[645],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[646] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[646],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[647] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[647],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[648] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[648],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[649] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[649],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[650] = SG_MAKE_STRING("misplaced ellipsis following empty list");
  sg__rc_cgen15752.d15867[651] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[651],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[652] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[652],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[653] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[653],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[66]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[4] = SG_WORD(sg__rc_cgen15752.d15867[628]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[11] = SG_WORD(sg__rc_cgen15752.d15867[629]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[17] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[19] = SG_WORD(sg__rc_cgen15752.d15867[630]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[22] = SG_WORD(sg__rc_cgen15752.d15867[631]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[28] = SG_WORD(sg__rc_cgen15752.d15867[632]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[34] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[36] = SG_WORD(sg__rc_cgen15752.d15867[617]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[41] = SG_WORD(sg__rc_cgen15752.d15867[633]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[47] = SG_WORD(sg__rc_cgen15752.d15867[634]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[50] = SG_WORD(sg__rc_cgen15752.d15867[448]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[60] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[92] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[106] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[109] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[116] = SG_WORD(sg__rc_cgen15752.d15867[635]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[135] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[137] = SG_WORD(sg__rc_cgen15752.d15867[636]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[142] = SG_WORD(sg__rc_cgen15752.d15867[637]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[148] = SG_WORD(sg__rc_cgen15752.d15867[638]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[151] = SG_WORD(sg__rc_cgen15752.d15867[639]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[179] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[186] = SG_WORD(sg__rc_cgen15752.d15867[640]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[194] = SG_WORD(sg__rc_cgen15752.d15867[641]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[201] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[203] = SG_WORD(sg__rc_cgen15752.d15867[642]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[208] = SG_WORD(sg__rc_cgen15752.d15867[643]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[214] = SG_WORD(sg__rc_cgen15752.d15867[644]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[217] = SG_WORD(sg__rc_cgen15752.d15867[645]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[225] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[227] = SG_WORD(sg__rc_cgen15752.d15867[617]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[232] = SG_WORD(sg__rc_cgen15752.d15867[646]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[238] = SG_WORD(sg__rc_cgen15752.d15867[647]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[241] = SG_WORD(sg__rc_cgen15752.d15867[648]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[273] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[275] = SG_WORD(sg__rc_cgen15752.d15867[636]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[279] = SG_WORD(sg__rc_cgen15752.d15867[649]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[299] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[301] = SG_WORD(sg__rc_cgen15752.d15867[650]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[304] = SG_WORD(sg__rc_cgen15752.d15867[651]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[307] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[309] = SG_WORD(sg__rc_cgen15752.d15867[642]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[313] = SG_WORD(sg__rc_cgen15752.d15867[652]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3141]))[338] = SG_WORD(sg__rc_cgen15752.d15867[653]);
  sg__rc_cgen15752.d15867[655] = SG_MAKE_STRING("check-template");
  sg__rc_cgen15752.d15867[654] = Sg_Intern(sg__rc_cgen15752.d15867[655]); /* check-template */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[67]))->name = sg__rc_cgen15752.d15867[654];/* check-template */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3514]))[14] = SG_WORD(sg__rc_cgen15752.d15867[624]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3514]))[23] = SG_WORD(sg__rc_cgen15752.d15867[627]);
  sg__rc_cgen15752.d15867[656] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[656],SG_SYMBOL(sg__rc_cgen15752.d15867[654]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* check-template */
  sg__rc_cgen15752.d15867[657] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[657],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[658] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[658],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[659] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[659],SG_SYMBOL(sg__rc_cgen15752.d15867[394]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* number? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3564]))[6] = SG_WORD(sg__rc_cgen15752.d15867[658]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3564]))[12] = SG_WORD(sg__rc_cgen15752.d15867[659]);
  sg__rc_cgen15752.d15867[660] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[662] = SG_MAKE_STRING("filter-map");
  sg__rc_cgen15752.d15867[661] = Sg_Intern(sg__rc_cgen15752.d15867[662]); /* filter-map */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[660],SG_SYMBOL(sg__rc_cgen15752.d15867[661]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* filter-map */
  sg__rc_cgen15752.d15867[663] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[663],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[664] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[664],SG_SYMBOL(sg__rc_cgen15752.d15867[414]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* pending-identifier? */
  sg__rc_cgen15752.d15867[665] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[665],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[666] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[666],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[667] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[667],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[668] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[670] = SG_MAKE_STRING("assoc");
  sg__rc_cgen15752.d15867[669] = Sg_Intern(sg__rc_cgen15752.d15867[670]); /* assoc */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[668],SG_SYMBOL(sg__rc_cgen15752.d15867[669]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assoc */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3583]))[4] = SG_WORD(sg__rc_cgen15752.d15867[664]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3583]))[15] = SG_WORD(sg__rc_cgen15752.d15867[665]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3583]))[18] = SG_WORD(sg__rc_cgen15752.d15867[666]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3583]))[27] = SG_WORD(sg__rc_cgen15752.d15867[667]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3583]))[29] = SG_WORD(sg__rc_cgen15752.d15867[668]);
  sg__rc_cgen15752.d15867[671] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[671],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  sg__rc_cgen15752.d15867[672] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[672],SG_SYMBOL(sg__rc_cgen15752.d15867[654]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* check-template */
  sg__rc_cgen15752.d15867[673] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[673],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[674] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[674],SG_SYMBOL(sg__rc_cgen15752.d15867[271]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* p1env-lookup */
  sg__rc_cgen15752.d15867[675] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[675],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[676] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[676],SG_SYMBOL(sg__rc_cgen15752.d15867[236]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .vars */
  sg__rc_cgen15752.d15867[677] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[677],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[678] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[678],SG_SYMBOL(sg__rc_cgen15752.d15867[581]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .expand-syntax */
  sg__rc_cgen15752.d15867[679] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[679],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  do {
    /* (()) */ 
    SgObject G15875 = SG_NIL, G15876 = SG_NIL;
    SG_APPEND1(G15875, G15876, SG_NIL); /* () */ 
    sg__rc_cgen15752.d15867[680] = G15875;
  } while (0);
  sg__rc_cgen15752.d15867[681] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[681],SG_SYMBOL(sg__rc_cgen15752.d15867[581]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .expand-syntax */
  sg__rc_cgen15752.d15867[682] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[682],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[683] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[683],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[684] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[684],SG_SYMBOL(sg__rc_cgen15752.d15867[581]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .expand-syntax */
  sg__rc_cgen15752.d15867[685] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[685],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[686] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[686],SG_SYMBOL(sg__rc_cgen15752.d15867[376]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-quote. */
  sg__rc_cgen15752.d15867[688] = SG_MAKE_STRING("compile-syntax");
  sg__rc_cgen15752.d15867[687] = Sg_Intern(sg__rc_cgen15752.d15867[688]); /* compile-syntax */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[70]))->name = sg__rc_cgen15752.d15867[687];/* compile-syntax */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[4] = SG_WORD(sg__rc_cgen15752.d15867[657]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[14] = SG_WORD(sg__rc_cgen15752.d15867[660]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[22] = SG_WORD(sg__rc_cgen15752.d15867[663]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[30] = SG_WORD(sg__rc_cgen15752.d15867[64]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[37] = SG_WORD(sg__rc_cgen15752.d15867[671]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[44] = SG_WORD(sg__rc_cgen15752.d15867[672]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[49] = SG_WORD(sg__rc_cgen15752.d15867[673]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[53] = SG_WORD(sg__rc_cgen15752.d15867[674]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[59] = SG_WORD(sg__rc_cgen15752.d15867[675]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[67] = SG_WORD(sg__rc_cgen15752.d15867[676]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[74] = SG_WORD(sg__rc_cgen15752.d15867[677]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[81] = SG_WORD(sg__rc_cgen15752.d15867[678]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[84] = SG_WORD(sg__rc_cgen15752.d15867[679]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[89] = SG_WORD(sg__rc_cgen15752.d15867[680]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[95] = SG_WORD(sg__rc_cgen15752.d15867[681]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[98] = SG_WORD(sg__rc_cgen15752.d15867[682]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[103] = SG_WORD(sg__rc_cgen15752.d15867[683]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[112] = SG_WORD(sg__rc_cgen15752.d15867[684]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[115] = SG_WORD(sg__rc_cgen15752.d15867[685]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3618]))[120] = SG_WORD(sg__rc_cgen15752.d15867[686]);
  sg__rc_cgen15752.d15867[689] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[689],SG_SYMBOL(sg__rc_cgen15752.d15867[687]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* compile-syntax */
  sg__rc_cgen15752.d15867[690] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[690],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[691] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[691],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[692] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[692],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  sg__rc_cgen15752.d15867[693] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[693],SG_SYMBOL(sg__rc_cgen15752.d15867[80]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* add-to-transformer-env! */
  sg__rc_cgen15752.d15867[695] = SG_MAKE_STRING("wrap-symbol");
  sg__rc_cgen15752.d15867[694] = Sg_Intern(sg__rc_cgen15752.d15867[695]); /* wrap-symbol */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[71]))->name = sg__rc_cgen15752.d15867[694];/* wrap-symbol */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3743]))[11] = SG_WORD(sg__rc_cgen15752.d15867[692]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3743]))[16] = SG_WORD(sg__rc_cgen15752.d15867[693]);
  sg__rc_cgen15752.d15867[696] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[696],SG_SYMBOL(sg__rc_cgen15752.d15867[385]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector-copy */
  sg__rc_cgen15752.d15867[698] = SG_MAKE_STRING("rename-vector");
  sg__rc_cgen15752.d15867[697] = Sg_Intern(sg__rc_cgen15752.d15867[698]); /* rename-vector */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[72]))->name = sg__rc_cgen15752.d15867[697];/* rename-vector */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3761]))[49] = SG_WORD(sg__rc_cgen15752.d15867[696]);
  sg__rc_cgen15752.d15867[699] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[699],SG_SYMBOL(sg__rc_cgen15752.d15867[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lookup-transformer-env */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[73]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3824]))[3] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3824]))[9] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3824]))[70] = SG_WORD(sg__rc_cgen15752.d15867[699]);
  sg__rc_cgen15752.d15867[701] = SG_MAKE_STRING("partial-identifier");
  sg__rc_cgen15752.d15867[700] = Sg_Intern(sg__rc_cgen15752.d15867[701]); /* partial-identifier */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[74]))->name = sg__rc_cgen15752.d15867[700];/* partial-identifier */
  sg__rc_cgen15752.d15867[702] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[702],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[703] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[703],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[704] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[704],SG_SYMBOL(sg__rc_cgen15752.d15867[669]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assoc */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3961]))[5] = SG_WORD(sg__rc_cgen15752.d15867[703]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3961]))[7] = SG_WORD(sg__rc_cgen15752.d15867[704]);
  sg__rc_cgen15752.d15867[705] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[705],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  sg__rc_cgen15752.d15867[706] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[708] = SG_MAKE_STRING("transcribe-template");
  sg__rc_cgen15752.d15867[707] = Sg_Intern(sg__rc_cgen15752.d15867[708]); /* transcribe-template */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[706],SG_SYMBOL(sg__rc_cgen15752.d15867[707]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* transcribe-template */
  sg__rc_cgen15752.d15867[709] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[709],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[710] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[710],SG_SYMBOL(sg__rc_cgen15752.d15867[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* lookup-transformer-env */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[76]))->name = sg__rc_cgen15752.d15867[577];/* expand-syntax */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[4] = SG_WORD(sg__rc_cgen15752.d15867[690]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[9] = SG_WORD(sg__rc_cgen15752.d15867[691]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[33] = SG_WORD(sg__rc_cgen15752.d15867[702]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[44] = SG_WORD(sg__rc_cgen15752.d15867[84]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[50] = SG_WORD(sg__rc_cgen15752.d15867[705]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[55] = SG_WORD(sg__rc_cgen15752.d15867[706]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[72] = SG_WORD(sg__rc_cgen15752.d15867[709]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[3971]))[85] = SG_WORD(sg__rc_cgen15752.d15867[710]);
  sg__rc_cgen15752.d15867[711] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[711],SG_SYMBOL(sg__rc_cgen15752.d15867[577]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* expand-syntax */
  sg__rc_cgen15752.d15867[712] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[712],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[77]))->name = sg__rc_cgen15752.d15867[600];/* rank-of */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4068]))[13] = SG_WORD(sg__rc_cgen15752.d15867[712]);
  sg__rc_cgen15752.d15867[713] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[713],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[714] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[714],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  sg__rc_cgen15752.d15867[716] = SG_MAKE_STRING("subform-of");
  sg__rc_cgen15752.d15867[715] = Sg_Intern(sg__rc_cgen15752.d15867[716]); /* subform-of */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[78]))->name = sg__rc_cgen15752.d15867[715];/* subform-of */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4091]))[5] = SG_WORD(sg__rc_cgen15752.d15867[714]);
  sg__rc_cgen15752.d15867[717] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[717],SG_SYMBOL(sg__rc_cgen15752.d15867[715]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* subform-of */
  sg__rc_cgen15752.d15867[718] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[718],SG_SYMBOL(sg__rc_cgen15752.d15867[351]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-unique-ids */
  sg__rc_cgen15752.d15867[719] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[721] = SG_MAKE_STRING("reverse!");
  sg__rc_cgen15752.d15867[720] = Sg_Intern(sg__rc_cgen15752.d15867[721]); /* reverse! */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[719],SG_SYMBOL(sg__rc_cgen15752.d15867[720]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* reverse! */
  sg__rc_cgen15752.d15867[722] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[722],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[723] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[725] = SG_MAKE_STRING("member");
  sg__rc_cgen15752.d15867[724] = Sg_Intern(sg__rc_cgen15752.d15867[725]); /* member */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[723],SG_SYMBOL(sg__rc_cgen15752.d15867[724]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* member */
  sg__rc_cgen15752.d15867[726] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[726],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[727] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[727],SG_SYMBOL(sg__rc_cgen15752.d15867[724]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* member */
  sg__rc_cgen15752.d15867[728] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[728],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[729] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[729],SG_SYMBOL(sg__rc_cgen15752.d15867[669]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assoc */
  sg__rc_cgen15752.d15867[731] = SG_MAKE_STRING("collect-ellipsis-vars");
  sg__rc_cgen15752.d15867[730] = Sg_Intern(sg__rc_cgen15752.d15867[731]); /* collect-ellipsis-vars */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[79]))->name = sg__rc_cgen15752.d15867[730];/* collect-ellipsis-vars */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[4] = SG_WORD(sg__rc_cgen15752.d15867[718]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[16] = SG_WORD(sg__rc_cgen15752.d15867[719]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[25] = SG_WORD(sg__rc_cgen15752.d15867[722]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[27] = SG_WORD(sg__rc_cgen15752.d15867[723]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[35] = SG_WORD(sg__rc_cgen15752.d15867[726]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[37] = SG_WORD(sg__rc_cgen15752.d15867[727]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[52] = SG_WORD(sg__rc_cgen15752.d15867[728]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4099]))[54] = SG_WORD(sg__rc_cgen15752.d15867[729]);
  sg__rc_cgen15752.d15867[732] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[732],SG_SYMBOL(sg__rc_cgen15752.d15867[730]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-ellipsis-vars */
  sg__rc_cgen15752.d15867[733] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[733],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[734] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[734],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[735] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[735],SG_SYMBOL(sg__rc_cgen15752.d15867[614]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* < */
  sg__rc_cgen15752.d15867[736] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[736],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[737] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[737],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[738] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[738],SG_SYMBOL(sg__rc_cgen15752.d15867[614]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* < */
  sg__rc_cgen15752.d15867[739] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[739],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[80]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4186]))[4] = SG_WORD(sg__rc_cgen15752.d15867[736]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4186]))[13] = SG_WORD(sg__rc_cgen15752.d15867[737]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4186]))[17] = SG_WORD(sg__rc_cgen15752.d15867[738]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4186]))[45] = SG_WORD(sg__rc_cgen15752.d15867[739]);
  sg__rc_cgen15752.d15867[740] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[740],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[741] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[741],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[81]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[4] = SG_WORD(sg__rc_cgen15752.d15867[733]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[13] = SG_WORD(sg__rc_cgen15752.d15867[734]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[17] = SG_WORD(sg__rc_cgen15752.d15867[735]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[27] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[67] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[81] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[84] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[91] = SG_WORD(sg__rc_cgen15752.d15867[740]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[123] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4237]))[170] = SG_WORD(sg__rc_cgen15752.d15867[741]);
  sg__rc_cgen15752.d15867[743] = SG_MAKE_STRING("contain-rank-moved-var?");
  sg__rc_cgen15752.d15867[742] = Sg_Intern(sg__rc_cgen15752.d15867[743]); /* contain-rank-moved-var? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[82]))->name = sg__rc_cgen15752.d15867[742];/* contain-rank-moved-var? */
  sg__rc_cgen15752.d15867[744] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[744],SG_SYMBOL(sg__rc_cgen15752.d15867[742]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* contain-rank-moved-var? */
  sg__rc_cgen15752.d15867[745] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[745],SG_SYMBOL(sg__rc_cgen15752.d15867[742]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* contain-rank-moved-var? */
  sg__rc_cgen15752.d15867[746] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[746],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[747] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[747],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[748] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[748],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[749] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[749],SG_SYMBOL(sg__rc_cgen15752.d15867[614]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* < */
  sg__rc_cgen15752.d15867[750] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[750],SG_SYMBOL(sg__rc_cgen15752.d15867[191]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* gensym */
  sg__rc_cgen15752.d15867[751] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[753] = SG_MAKE_STRING("copy-identifier");
  sg__rc_cgen15752.d15867[752] = Sg_Intern(sg__rc_cgen15752.d15867[753]); /* copy-identifier */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[751],SG_SYMBOL(sg__rc_cgen15752.d15867[752]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* copy-identifier */
  sg__rc_cgen15752.d15867[754] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[754],SG_SYMBOL(sg__rc_cgen15752.d15867[379]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-ref */
  sg__rc_cgen15752.d15867[755] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[755],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[756] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[756],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  sg__rc_cgen15752.d15867[757] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[757],SG_SYMBOL(sg__rc_cgen15752.d15867[108]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-set! */
  sg__rc_cgen15752.d15867[758] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[758],SG_SYMBOL(sg__rc_cgen15752.d15867[170]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assq */
  sg__rc_cgen15752.d15867[760] = SG_MAKE_STRING("revealed");
  sg__rc_cgen15752.d15867[759] = Sg_Intern(sg__rc_cgen15752.d15867[760]); /* revealed */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[83]))->name = sg__rc_cgen15752.d15867[759];/* revealed */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[8] = SG_WORD(sg__rc_cgen15752.d15867[748]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[12] = SG_WORD(sg__rc_cgen15752.d15867[749]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[20] = SG_WORD(sg__rc_cgen15752.d15867[750]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[24] = SG_WORD(sg__rc_cgen15752.d15867[751]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[33] = SG_WORD(sg__rc_cgen15752.d15867[754]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[44] = SG_WORD(sg__rc_cgen15752.d15867[755]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[52] = SG_WORD(sg__rc_cgen15752.d15867[714]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[83] = SG_WORD(sg__rc_cgen15752.d15867[756]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[90] = SG_WORD(sg__rc_cgen15752.d15867[757]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4451]))[99] = SG_WORD(sg__rc_cgen15752.d15867[758]);
  sg__rc_cgen15752.d15867[761] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[761],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[762] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[762],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[763] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[763],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[764] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[766] = SG_MAKE_STRING("list->vector");
  sg__rc_cgen15752.d15867[765] = Sg_Intern(sg__rc_cgen15752.d15867[766]); /* list->vector */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[764],SG_SYMBOL(sg__rc_cgen15752.d15867[765]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list->vector */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[84]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4557]))[4] = SG_WORD(sg__rc_cgen15752.d15867[762]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4557]))[41] = SG_WORD(sg__rc_cgen15752.d15867[763]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4557]))[48] = SG_WORD(sg__rc_cgen15752.d15867[764]);
  sg__rc_cgen15752.d15867[767] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[767],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[768] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[768],SG_SYMBOL(sg__rc_cgen15752.d15867[256]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .ellipsis */
  sg__rc_cgen15752.d15867[769] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[769],SG_SYMBOL(sg__rc_cgen15752.d15867[256]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* .ellipsis */
  sg__rc_cgen15752.d15867[770] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[770],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[771] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[771],SG_SYMBOL(sg__rc_cgen15752.d15867[765]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list->vector */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[85]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[4] = SG_WORD(sg__rc_cgen15752.d15867[761]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[20] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[63] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[77] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[80] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[87] = SG_WORD(sg__rc_cgen15752.d15867[767]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[99] = SG_WORD(sg__rc_cgen15752.d15867[768]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[123] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[136] = SG_WORD(sg__rc_cgen15752.d15867[769]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[177] = SG_WORD(sg__rc_cgen15752.d15867[770]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4609]))[184] = SG_WORD(sg__rc_cgen15752.d15867[771]);
  sg__rc_cgen15752.d15867[772] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[774] = SG_MAKE_STRING("hashtable->alist");
  sg__rc_cgen15752.d15867[773] = Sg_Intern(sg__rc_cgen15752.d15867[774]); /* hashtable->alist */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[772],SG_SYMBOL(sg__rc_cgen15752.d15867[773]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable->alist */
  sg__rc_cgen15752.d15867[775] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[775],SG_SYMBOL(sg__rc_cgen15752.d15867[773]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable->alist */
  sg__rc_cgen15752.d15867[777] = SG_MAKE_STRING("adapt-to-rank-moved-vars");
  sg__rc_cgen15752.d15867[776] = Sg_Intern(sg__rc_cgen15752.d15867[777]); /* adapt-to-rank-moved-vars */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[86]))->name = sg__rc_cgen15752.d15867[776];/* adapt-to-rank-moved-vars */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4824]))[6] = SG_WORD(sg__rc_cgen15752.d15867[745]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4824]))[15] = SG_WORD(sg__rc_cgen15752.d15867[746]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4824]))[20] = SG_WORD(sg__rc_cgen15752.d15867[747]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4824]))[52] = SG_WORD(sg__rc_cgen15752.d15867[772]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4824]))[60] = SG_WORD(sg__rc_cgen15752.d15867[775]);
  sg__rc_cgen15752.d15867[778] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[778],SG_SYMBOL(sg__rc_cgen15752.d15867[776]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* adapt-to-rank-moved-vars */
  sg__rc_cgen15752.d15867[779] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[779],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[780] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[782] = SG_MAKE_STRING("cddar");
  sg__rc_cgen15752.d15867[781] = Sg_Intern(sg__rc_cgen15752.d15867[782]); /* cddar */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[780],SG_SYMBOL(sg__rc_cgen15752.d15867[781]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* cddar */
  sg__rc_cgen15752.d15867[783] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[785] = SG_MAKE_STRING("circular-list?");
  sg__rc_cgen15752.d15867[784] = Sg_Intern(sg__rc_cgen15752.d15867[785]); /* circular-list? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[783],SG_SYMBOL(sg__rc_cgen15752.d15867[784]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* circular-list? */
  sg__rc_cgen15752.d15867[786] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[786],SG_SYMBOL(sg__rc_cgen15752.d15867[781]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* cddar */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[87]))->name = sg__rc_cgen15752.d15867[318];/* loop */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4893]))[12] = SG_WORD(sg__rc_cgen15752.d15867[779]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4893]))[37] = SG_WORD(sg__rc_cgen15752.d15867[780]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4893]))[53] = SG_WORD(sg__rc_cgen15752.d15867[783]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[4893]))[68] = SG_WORD(sg__rc_cgen15752.d15867[786]);
  sg__rc_cgen15752.d15867[788] = SG_MAKE_STRING("consume-ellipsis-vars");
  sg__rc_cgen15752.d15867[787] = Sg_Intern(sg__rc_cgen15752.d15867[788]); /* consume-ellipsis-vars */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[88]))->name = sg__rc_cgen15752.d15867[787];/* consume-ellipsis-vars */
  sg__rc_cgen15752.d15867[789] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[789],SG_SYMBOL(sg__rc_cgen15752.d15867[787]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* consume-ellipsis-vars */
  sg__rc_cgen15752.d15867[790] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[790],SG_SYMBOL(sg__rc_cgen15752.d15867[776]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* adapt-to-rank-moved-vars */
  sg__rc_cgen15752.d15867[791] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[791],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[792] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[792],SG_SYMBOL(sg__rc_cgen15752.d15867[669]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assoc */
  sg__rc_cgen15752.d15867[793] = SG_MAKE_STRING("subforms have different size of matched input (variable)");
  sg__rc_cgen15752.d15867[795] = SG_MAKE_STRING("template:");
  sg__rc_cgen15752.d15867[794] = Sg_Intern(sg__rc_cgen15752.d15867[795]); /* template: */
  sg__rc_cgen15752.d15867[796] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[796],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[798] = SG_MAKE_STRING("subforms:");
  sg__rc_cgen15752.d15867[797] = Sg_Intern(sg__rc_cgen15752.d15867[798]); /* subforms: */
  sg__rc_cgen15752.d15867[799] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[801] = SG_MAKE_STRING("car");
  sg__rc_cgen15752.d15867[800] = Sg_Intern(sg__rc_cgen15752.d15867[801]); /* car */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[799],SG_SYMBOL(sg__rc_cgen15752.d15867[800]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* car */
  sg__rc_cgen15752.d15867[802] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[802],SG_SYMBOL(sg__rc_cgen15752.d15867[425]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* map */
  sg__rc_cgen15752.d15867[803] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[803],SG_SYMBOL(sg__rc_cgen15752.d15867[425]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* map */
  sg__rc_cgen15752.d15867[804] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[806] = SG_MAKE_STRING("%map-cons");
  sg__rc_cgen15752.d15867[805] = Sg_Intern(sg__rc_cgen15752.d15867[806]); /* %map-cons */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[804],SG_SYMBOL(sg__rc_cgen15752.d15867[805]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* %map-cons */
  sg__rc_cgen15752.d15867[807] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[807],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[809] = SG_MAKE_STRING("expand-var");
  sg__rc_cgen15752.d15867[808] = Sg_Intern(sg__rc_cgen15752.d15867[809]); /* expand-var */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[89]))->name = sg__rc_cgen15752.d15867[808];/* expand-var */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[5] = SG_WORD(sg__rc_cgen15752.d15867[791]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[7] = SG_WORD(sg__rc_cgen15752.d15867[792]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[21] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[23] = SG_WORD(sg__rc_cgen15752.d15867[793]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[25] = SG_WORD(sg__rc_cgen15752.d15867[794]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[30] = SG_WORD(sg__rc_cgen15752.d15867[796]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[36] = SG_WORD(sg__rc_cgen15752.d15867[797]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[42] = SG_WORD(sg__rc_cgen15752.d15867[799]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[45] = SG_WORD(sg__rc_cgen15752.d15867[802]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[50] = SG_WORD(sg__rc_cgen15752.d15867[92]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[53] = SG_WORD(sg__rc_cgen15752.d15867[803]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[56] = SG_WORD(sg__rc_cgen15752.d15867[804]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5010]))[59] = SG_WORD(sg__rc_cgen15752.d15867[807]);
  sg__rc_cgen15752.d15867[810] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[810],SG_SYMBOL(sg__rc_cgen15752.d15867[730]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* collect-ellipsis-vars */
  sg__rc_cgen15752.d15867[811] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[811],SG_SYMBOL(sg__rc_cgen15752.d15867[787]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* consume-ellipsis-vars */
  sg__rc_cgen15752.d15867[812] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[812],SG_SYMBOL(sg__rc_cgen15752.d15867[516]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* reverse */
  sg__rc_cgen15752.d15867[813] = SG_MAKE_STRING("subforms have different size of matched input");
  sg__rc_cgen15752.d15867[814] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[814],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[815] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[815],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[816] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[816],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[818] = SG_MAKE_STRING("expand-ellipsis-template");
  sg__rc_cgen15752.d15867[817] = Sg_Intern(sg__rc_cgen15752.d15867[818]); /* expand-ellipsis-template */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[90]))->name = sg__rc_cgen15752.d15867[817];/* expand-ellipsis-template */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[9] = SG_WORD(sg__rc_cgen15752.d15867[810]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[32] = SG_WORD(sg__rc_cgen15752.d15867[811]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[50] = SG_WORD(sg__rc_cgen15752.d15867[812]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[53] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[55] = SG_WORD(sg__rc_cgen15752.d15867[813]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[57] = SG_WORD(sg__rc_cgen15752.d15867[794]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[62] = SG_WORD(sg__rc_cgen15752.d15867[814]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[66] = SG_WORD(sg__rc_cgen15752.d15867[797]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[71] = SG_WORD(sg__rc_cgen15752.d15867[815]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5071]))[74] = SG_WORD(sg__rc_cgen15752.d15867[816]);
  sg__rc_cgen15752.d15867[819] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[819],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[820] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[820],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[821] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[821],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[822] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[822],SG_SYMBOL(sg__rc_cgen15752.d15867[765]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list->vector */
  sg__rc_cgen15752.d15867[824] = SG_MAKE_STRING("expand-escaped-template");
  sg__rc_cgen15752.d15867[823] = Sg_Intern(sg__rc_cgen15752.d15867[824]); /* expand-escaped-template */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[91]))->name = sg__rc_cgen15752.d15867[823];/* expand-escaped-template */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5147]))[4] = SG_WORD(sg__rc_cgen15752.d15867[819]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5147]))[12] = SG_WORD(sg__rc_cgen15752.d15867[820]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5147]))[55] = SG_WORD(sg__rc_cgen15752.d15867[821]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5147]))[63] = SG_WORD(sg__rc_cgen15752.d15867[822]);
  sg__rc_cgen15752.d15867[825] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[825],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[826] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[826],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[827] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[827],SG_SYMBOL(sg__rc_cgen15752.d15867[595]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* parse-ellipsis-splicing */
  sg__rc_cgen15752.d15867[828] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[830] = SG_MAKE_STRING("append");
  sg__rc_cgen15752.d15867[829] = Sg_Intern(sg__rc_cgen15752.d15867[830]); /* append */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[828],SG_SYMBOL(sg__rc_cgen15752.d15867[829]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* append */
  sg__rc_cgen15752.d15867[831] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[831],SG_SYMBOL(sg__rc_cgen15752.d15867[600]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rank-of */
  sg__rc_cgen15752.d15867[832] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[832],SG_SYMBOL(sg__rc_cgen15752.d15867[260]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* bound-identifier=? */
  sg__rc_cgen15752.d15867[833] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[833],SG_SYMBOL(sg__rc_cgen15752.d15867[669]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assoc */
  sg__rc_cgen15752.d15867[834] = SG_MAKE_STRING("subforms have different size of matched input (ellipsis)");
  sg__rc_cgen15752.d15867[835] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[835],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[836] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[836],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[837] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[837],SG_SYMBOL(sg__rc_cgen15752.d15867[306]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax-violation */
  sg__rc_cgen15752.d15867[838] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[838],SG_SYMBOL(sg__rc_cgen15752.d15867[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector->list */
  sg__rc_cgen15752.d15867[839] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[839],SG_SYMBOL(sg__rc_cgen15752.d15867[765]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list->vector */
  sg__rc_cgen15752.d15867[841] = SG_MAKE_STRING("expand-template");
  sg__rc_cgen15752.d15867[840] = Sg_Intern(sg__rc_cgen15752.d15867[841]); /* expand-template */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[92]))->name = sg__rc_cgen15752.d15867[840];/* expand-template */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[4] = SG_WORD(sg__rc_cgen15752.d15867[825]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[12] = SG_WORD(sg__rc_cgen15752.d15867[826]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[32] = SG_WORD(sg__rc_cgen15752.d15867[297]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[65] = SG_WORD(sg__rc_cgen15752.d15867[289]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[79] = SG_WORD(sg__rc_cgen15752.d15867[290]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[82] = SG_WORD(sg__rc_cgen15752.d15867[293]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[89] = SG_WORD(sg__rc_cgen15752.d15867[827]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[94] = SG_WORD(sg__rc_cgen15752.d15867[828]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[130] = SG_WORD(sg__rc_cgen15752.d15867[285]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[137] = SG_WORD(sg__rc_cgen15752.d15867[357]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[145] = SG_WORD(sg__rc_cgen15752.d15867[831]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[158] = SG_WORD(sg__rc_cgen15752.d15867[832]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[160] = SG_WORD(sg__rc_cgen15752.d15867[833]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[179] = SG_WORD(sg__rc_cgen15752.d15867[616]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[181] = SG_WORD(sg__rc_cgen15752.d15867[834]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[183] = SG_WORD(sg__rc_cgen15752.d15867[794]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[188] = SG_WORD(sg__rc_cgen15752.d15867[835]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[192] = SG_WORD(sg__rc_cgen15752.d15867[797]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[197] = SG_WORD(sg__rc_cgen15752.d15867[836]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[200] = SG_WORD(sg__rc_cgen15752.d15867[837]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[278] = SG_WORD(sg__rc_cgen15752.d15867[838]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5214]))[287] = SG_WORD(sg__rc_cgen15752.d15867[839]);
  sg__rc_cgen15752.d15867[842] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[842],SG_SYMBOL(sg__rc_cgen15752.d15867[625]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* length */
  sg__rc_cgen15752.d15867[843] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[843],SG_SYMBOL(sg__rc_cgen15752.d15867[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* ellipsis? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[93]))->name = sg__rc_cgen15752.d15867[707];/* transcribe-template */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5532]))[6] = SG_WORD(sg__rc_cgen15752.d15867[790]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5532]))[39] = SG_WORD(sg__rc_cgen15752.d15867[842]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5532]))[48] = SG_WORD(sg__rc_cgen15752.d15867[843]);
  sg__rc_cgen15752.d15867[844] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[844],SG_SYMBOL(sg__rc_cgen15752.d15867[707]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* transcribe-template */
  sg__rc_cgen15752.d15867[845] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[845],SG_SYMBOL(sg__rc_cgen15752.d15867[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* identifier? */
  sg__rc_cgen15752.d15867[847] = SG_MAKE_STRING("datum->syntax");
  sg__rc_cgen15752.d15867[846] = Sg_Intern(sg__rc_cgen15752.d15867[847]); /* datum->syntax */
  sg__rc_cgen15752.d15867[848] = SG_MAKE_STRING("expected identifier, but got ~s");
  sg__rc_cgen15752.d15867[849] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[851] = SG_MAKE_STRING("format");
  sg__rc_cgen15752.d15867[850] = Sg_Intern(sg__rc_cgen15752.d15867[851]); /* format */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[849],SG_SYMBOL(sg__rc_cgen15752.d15867[850]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* format */
  sg__rc_cgen15752.d15867[852] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[852],SG_SYMBOL(sg__rc_cgen15752.d15867[199]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assertion-violation */
  sg__rc_cgen15752.d15867[853] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[853],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[854] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[854],SG_SYMBOL(sg__rc_cgen15752.d15867[752]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* copy-identifier */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5601]))[3] = SG_WORD(sg__rc_cgen15752.d15867[854]);
  sg__rc_cgen15752.d15867[855] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[855],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[95]))->name = sg__rc_cgen15752.d15867[846];/* datum->syntax */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[4] = SG_WORD(sg__rc_cgen15752.d15867[845]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[12] = SG_WORD(sg__rc_cgen15752.d15867[846]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[16] = SG_WORD(sg__rc_cgen15752.d15867[848]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[19] = SG_WORD(sg__rc_cgen15752.d15867[849]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[22] = SG_WORD(sg__rc_cgen15752.d15867[852]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[27] = SG_WORD(sg__rc_cgen15752.d15867[853]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[38] = SG_WORD(sg__rc_cgen15752.d15867[97]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5606]))[40] = SG_WORD(sg__rc_cgen15752.d15867[855]);
  sg__rc_cgen15752.d15867[856] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[856],SG_SYMBOL(sg__rc_cgen15752.d15867[846]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* datum->syntax */
  sg__rc_cgen15752.d15867[857] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[857],SG_SYMBOL(sg__rc_cgen15752.d15867[88]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* unwrap-syntax */
  sg__rc_cgen15752.d15867[859] = SG_MAKE_STRING("syntax->datum");
  sg__rc_cgen15752.d15867[858] = Sg_Intern(sg__rc_cgen15752.d15867[859]); /* syntax->datum */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[96]))->name = sg__rc_cgen15752.d15867[858];/* syntax->datum */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5648]))[2] = SG_WORD(sg__rc_cgen15752.d15867[857]);
  sg__rc_cgen15752.d15867[860] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[860],SG_SYMBOL(sg__rc_cgen15752.d15867[858]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* syntax->datum */
  sg__rc_cgen15752.d15867[861] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[861],SG_SYMBOL(sg__rc_cgen15752.d15867[430]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* list? */
  sg__rc_cgen15752.d15867[863] = SG_MAKE_STRING("generate-temporaries");
  sg__rc_cgen15752.d15867[862] = Sg_Intern(sg__rc_cgen15752.d15867[863]); /* generate-temporaries */
  sg__rc_cgen15752.d15867[864] = SG_MAKE_STRING("expected list, but got ~s");
  sg__rc_cgen15752.d15867[865] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[865],SG_SYMBOL(sg__rc_cgen15752.d15867[850]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* format */
  sg__rc_cgen15752.d15867[866] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[866],SG_SYMBOL(sg__rc_cgen15752.d15867[199]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assertion-violation */
  sg__rc_cgen15752.d15867[867] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[869] = SG_MAKE_STRING("vm-current-library");
  sg__rc_cgen15752.d15867[868] = Sg_Intern(sg__rc_cgen15752.d15867[869]); /* vm-current-library */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[867],SG_SYMBOL(sg__rc_cgen15752.d15867[868]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vm-current-library */
  sg__rc_cgen15752.d15867[870] = SG_MAKE_STRING("~a.~a'~a");
  sg__rc_cgen15752.d15867[871] = SG_MAKE_STRING("temp.");
  sg__rc_cgen15752.d15867[872] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[872],SG_SYMBOL(sg__rc_cgen15752.d15867[191]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* gensym */
  sg__rc_cgen15752.d15867[873] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[875] = SG_MAKE_STRING("microsecond");
  sg__rc_cgen15752.d15867[874] = Sg_Intern(sg__rc_cgen15752.d15867[875]); /* microsecond */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[873],SG_SYMBOL(sg__rc_cgen15752.d15867[874]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* microsecond */
  sg__rc_cgen15752.d15867[876] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[878] = SG_MAKE_STRING("number->string");
  sg__rc_cgen15752.d15867[877] = Sg_Intern(sg__rc_cgen15752.d15867[878]); /* number->string */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[876],SG_SYMBOL(sg__rc_cgen15752.d15867[877]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* number->string */
  sg__rc_cgen15752.d15867[879] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[879],SG_SYMBOL(sg__rc_cgen15752.d15867[850]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* format */
  sg__rc_cgen15752.d15867[880] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[882] = SG_MAKE_STRING("string->symbol");
  sg__rc_cgen15752.d15867[881] = Sg_Intern(sg__rc_cgen15752.d15867[882]); /* string->symbol */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[880],SG_SYMBOL(sg__rc_cgen15752.d15867[881]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* string->symbol */
  sg__rc_cgen15752.d15867[883] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[883],SG_SYMBOL(sg__rc_cgen15752.d15867[34]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-identifier */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[97]))->name = sg__rc_cgen15752.d15867[862];/* generate-temporaries */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[4] = SG_WORD(sg__rc_cgen15752.d15867[861]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[12] = SG_WORD(sg__rc_cgen15752.d15867[862]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[16] = SG_WORD(sg__rc_cgen15752.d15867[864]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[19] = SG_WORD(sg__rc_cgen15752.d15867[865]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[22] = SG_WORD(sg__rc_cgen15752.d15867[866]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[26] = SG_WORD(sg__rc_cgen15752.d15867[867]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[48] = SG_WORD(sg__rc_cgen15752.d15867[870]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[52] = SG_WORD(sg__rc_cgen15752.d15867[871]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[54] = SG_WORD(sg__rc_cgen15752.d15867[872]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[61] = SG_WORD(sg__rc_cgen15752.d15867[873]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[65] = SG_WORD(sg__rc_cgen15752.d15867[876]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[69] = SG_WORD(sg__rc_cgen15752.d15867[879]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[72] = SG_WORD(sg__rc_cgen15752.d15867[880]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5652]))[78] = SG_WORD(sg__rc_cgen15752.d15867[883]);
  sg__rc_cgen15752.d15867[884] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[884],SG_SYMBOL(sg__rc_cgen15752.d15867[862]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* generate-temporaries */
  sg__rc_cgen15752.d15867[886] = SG_MAKE_STRING("variable-transformer");
  sg__rc_cgen15752.d15867[885] = Sg_Intern(sg__rc_cgen15752.d15867[886]); /* variable-transformer */
  sg__rc_cgen15752.d15867[887] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[889] = SG_MAKE_STRING("*variable-transformer-mark*");
  sg__rc_cgen15752.d15867[888] = Sg_Intern(sg__rc_cgen15752.d15867[889]); /* *variable-transformer-mark* */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[887],SG_SYMBOL(sg__rc_cgen15752.d15867[888]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* *variable-transformer-mark* */
  sg__rc_cgen15752.d15867[890] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[892] = SG_MAKE_STRING("macro?");
  sg__rc_cgen15752.d15867[891] = Sg_Intern(sg__rc_cgen15752.d15867[892]); /* macro? */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[890],SG_SYMBOL(sg__rc_cgen15752.d15867[891]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro? */
  sg__rc_cgen15752.d15867[893] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[895] = SG_MAKE_STRING("macro-data");
  sg__rc_cgen15752.d15867[894] = Sg_Intern(sg__rc_cgen15752.d15867[895]); /* macro-data */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[893],SG_SYMBOL(sg__rc_cgen15752.d15867[894]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-data */
  sg__rc_cgen15752.d15867[896] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[896],SG_SYMBOL(sg__rc_cgen15752.d15867[891]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro? */
  sg__rc_cgen15752.d15867[898] = SG_MAKE_STRING("variable-transformer?");
  sg__rc_cgen15752.d15867[897] = Sg_Intern(sg__rc_cgen15752.d15867[898]); /* variable-transformer? */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[98]))->name = sg__rc_cgen15752.d15867[897];/* variable-transformer? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5738]))[4] = SG_WORD(sg__rc_cgen15752.d15867[890]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5738]))[11] = SG_WORD(sg__rc_cgen15752.d15867[893]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5738]))[14] = SG_WORD(sg__rc_cgen15752.d15867[896]);
  sg__rc_cgen15752.d15867[899] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[899],SG_SYMBOL(sg__rc_cgen15752.d15867[897]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable-transformer? */
  sg__rc_cgen15752.d15867[900] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[900],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[901] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[901],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[902] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[902],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  sg__rc_cgen15752.d15867[903] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[903],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[904] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[906] = SG_MAKE_STRING("macro-env");
  sg__rc_cgen15752.d15867[905] = Sg_Intern(sg__rc_cgen15752.d15867[906]); /* macro-env */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[904],SG_SYMBOL(sg__rc_cgen15752.d15867[905]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-env */
  sg__rc_cgen15752.d15867[907] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[907],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[908] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[908],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  sg__rc_cgen15752.d15867[909] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[909],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[910] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[912] = SG_MAKE_STRING("values");
  sg__rc_cgen15752.d15867[911] = Sg_Intern(sg__rc_cgen15752.d15867[912]); /* values */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[910],SG_SYMBOL(sg__rc_cgen15752.d15867[911]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* values */
  sg__rc_cgen15752.d15867[913] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[913],SG_SYMBOL(sg__rc_cgen15752.d15867[891]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro? */
  sg__rc_cgen15752.d15867[914] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[914],SG_SYMBOL(sg__rc_cgen15752.d15867[905]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-env */
  sg__rc_cgen15752.d15867[915] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[915],SG_SYMBOL(sg__rc_cgen15752.d15867[894]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-data */
  sg__rc_cgen15752.d15867[916] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[918] = SG_MAKE_STRING("macro-transformer");
  sg__rc_cgen15752.d15867[917] = Sg_Intern(sg__rc_cgen15752.d15867[918]); /* macro-transformer */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[916],SG_SYMBOL(sg__rc_cgen15752.d15867[917]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-transformer */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5754]))[4] = SG_WORD(sg__rc_cgen15752.d15867[913]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5754]))[13] = SG_WORD(sg__rc_cgen15752.d15867[914]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5754]))[19] = SG_WORD(sg__rc_cgen15752.d15867[915]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5754]))[25] = SG_WORD(sg__rc_cgen15752.d15867[916]);
  sg__rc_cgen15752.d15867[919] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[919],SG_SYMBOL(sg__rc_cgen15752.d15867[164]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-transformer-env */
  sg__rc_cgen15752.d15867[920] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[920],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[921] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[921],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[922] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[922],SG_SYMBOL(sg__rc_cgen15752.d15867[187]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-identity */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5786]))[5] = SG_WORD(sg__rc_cgen15752.d15867[919]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5786]))[10] = SG_WORD(sg__rc_cgen15752.d15867[920]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5786]))[15] = SG_WORD(sg__rc_cgen15752.d15867[921]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5786]))[18] = SG_WORD(sg__rc_cgen15752.d15867[922]);
  sg__rc_cgen15752.d15867[923] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[925] = SG_MAKE_STRING("dynamic-wind");
  sg__rc_cgen15752.d15867[924] = Sg_Intern(sg__rc_cgen15752.d15867[925]); /* dynamic-wind */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[923],SG_SYMBOL(sg__rc_cgen15752.d15867[924]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* dynamic-wind */
  sg__rc_cgen15752.d15867[927] = SG_MAKE_STRING("macro-transform");
  sg__rc_cgen15752.d15867[926] = Sg_Intern(sg__rc_cgen15752.d15867[927]); /* macro-transform */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[101]))->name = sg__rc_cgen15752.d15867[926];/* macro-transform */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[3] = SG_WORD(sg__rc_cgen15752.d15867[900]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[8] = SG_WORD(sg__rc_cgen15752.d15867[901]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[13] = SG_WORD(sg__rc_cgen15752.d15867[902]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[19] = SG_WORD(sg__rc_cgen15752.d15867[903]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[26] = SG_WORD(sg__rc_cgen15752.d15867[904]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[29] = SG_WORD(sg__rc_cgen15752.d15867[907]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[35] = SG_WORD(sg__rc_cgen15752.d15867[189]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[37] = SG_WORD(sg__rc_cgen15752.d15867[190]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[40] = SG_WORD(sg__rc_cgen15752.d15867[908]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[46] = SG_WORD(sg__rc_cgen15752.d15867[909]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[48] = SG_WORD(sg__rc_cgen15752.d15867[910]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5806]))[62] = SG_WORD(sg__rc_cgen15752.d15867[923]);
  sg__rc_cgen15752.d15867[928] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[928],SG_SYMBOL(sg__rc_cgen15752.d15867[926]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-transform */
  sg__rc_cgen15752.d15867[929] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[929],SG_SYMBOL(sg__rc_cgen15752.d15867[926]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* macro-transform */
  sg__rc_cgen15752.d15867[930] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[932] = SG_MAKE_STRING("make-macro");
  sg__rc_cgen15752.d15867[931] = Sg_Intern(sg__rc_cgen15752.d15867[932]); /* make-macro */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[930],SG_SYMBOL(sg__rc_cgen15752.d15867[931]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-macro */
  sg__rc_cgen15752.d15867[934] = SG_MAKE_STRING("make-macro-transformer");
  sg__rc_cgen15752.d15867[933] = Sg_Intern(sg__rc_cgen15752.d15867[934]); /* make-macro-transformer */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[102]))->name = sg__rc_cgen15752.d15867[933];/* make-macro-transformer */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5870]))[2] = SG_WORD(sg__rc_cgen15752.d15867[929]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5870]))[11] = SG_WORD(sg__rc_cgen15752.d15867[930]);
  sg__rc_cgen15752.d15867[935] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[935],SG_SYMBOL(sg__rc_cgen15752.d15867[933]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-macro-transformer */
  sg__rc_cgen15752.d15867[936] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[936],SG_SYMBOL(sg__rc_cgen15752.d15867[888]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* *variable-transformer-mark* */
  sg__rc_cgen15752.d15867[937] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[937],SG_SYMBOL(sg__rc_cgen15752.d15867[121]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-usage-env */
  sg__rc_cgen15752.d15867[938] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[938],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[939] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[939],SG_SYMBOL(sg__rc_cgen15752.d15867[414]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* pending-identifier? */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5883]))[4] = SG_WORD(sg__rc_cgen15752.d15867[939]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5883]))[16] = SG_WORD(sg__rc_cgen15752.d15867[392]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5883]))[19] = SG_WORD(sg__rc_cgen15752.d15867[393]);
  sg__rc_cgen15752.d15867[940] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[940],SG_SYMBOL(sg__rc_cgen15752.d15867[389]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* rewrite-form */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5905]))[3] = SG_WORD(sg__rc_cgen15752.d15867[937]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5905]))[11] = SG_WORD(sg__rc_cgen15752.d15867[938]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5905]))[22] = SG_WORD(sg__rc_cgen15752.d15867[103]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5905]))[28] = SG_WORD(sg__rc_cgen15752.d15867[940]);
  sg__rc_cgen15752.d15867[941] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[941],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[942] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[942],SG_SYMBOL(sg__rc_cgen15752.d15867[931]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-macro */
  sg__rc_cgen15752.d15867[944] = SG_MAKE_STRING("make-variable-transformer");
  sg__rc_cgen15752.d15867[943] = Sg_Intern(sg__rc_cgen15752.d15867[944]); /* make-variable-transformer */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[105]))->name = sg__rc_cgen15752.d15867[943];/* make-variable-transformer */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5939]))[1] = SG_WORD(sg__rc_cgen15752.d15867[936]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5939]))[11] = SG_WORD(sg__rc_cgen15752.d15867[941]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5939]))[14] = SG_WORD(sg__rc_cgen15752.d15867[942]);
  sg__rc_cgen15752.d15867[945] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[945],SG_SYMBOL(sg__rc_cgen15752.d15867[943]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-variable-transformer */
  sg__rc_cgen15752.d15867[946] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[946],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[948] = SG_MAKE_STRING("er-macro-transformer");
  sg__rc_cgen15752.d15867[947] = Sg_Intern(sg__rc_cgen15752.d15867[948]); /* er-macro-transformer */
  sg__rc_cgen15752.d15867[949] = SG_MAKE_STRING("a symbol or an identifier");
  sg__rc_cgen15752.d15867[950] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15752.d15867[952] = SG_MAKE_STRING("wrong-type-argument-message");
  sg__rc_cgen15752.d15867[951] = Sg_Intern(sg__rc_cgen15752.d15867[952]); /* wrong-type-argument-message */
  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[950],SG_SYMBOL(sg__rc_cgen15752.d15867[951]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* wrong-type-argument-message */
  sg__rc_cgen15752.d15867[953] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[953],SG_SYMBOL(sg__rc_cgen15752.d15867[199]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* assertion-violation */
  sg__rc_cgen15752.d15867[954] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[954],SG_SYMBOL(sg__rc_cgen15752.d15867[379]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-ref */
  sg__rc_cgen15752.d15867[955] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[955],SG_SYMBOL(sg__rc_cgen15752.d15867[414]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* pending-identifier? */
  sg__rc_cgen15752.d15867[956] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[956],SG_SYMBOL(sg__rc_cgen15752.d15867[379]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* hashtable-ref */
  sg__rc_cgen15752.d15867[957] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[957],SG_SYMBOL(sg__rc_cgen15752.d15867[173]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* id-library */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[106]))->name = sg__rc_cgen15752.d15867[125];/* er-rename */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[2] = SG_WORD(sg__rc_cgen15752.d15867[112]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[8] = SG_WORD(sg__rc_cgen15752.d15867[946]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[17] = SG_WORD(sg__rc_cgen15752.d15867[947]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[21] = SG_WORD(sg__rc_cgen15752.d15867[949]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[25] = SG_WORD(sg__rc_cgen15752.d15867[950]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[28] = SG_WORD(sg__rc_cgen15752.d15867[953]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[43] = SG_WORD(sg__rc_cgen15752.d15867[954]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[58] = SG_WORD(sg__rc_cgen15752.d15867[112]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[68] = SG_WORD(sg__rc_cgen15752.d15867[955]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[83] = SG_WORD(sg__rc_cgen15752.d15867[956]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[100] = SG_WORD(sg__rc_cgen15752.d15867[957]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[5955]))[103] = SG_WORD(sg__rc_cgen15752.d15867[112]);
  sg__rc_cgen15752.d15867[958] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[958],SG_SYMBOL(sg__rc_cgen15752.d15867[125]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* er-rename */
  sg__rc_cgen15752.d15867[959] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[959],SG_SYMBOL(sg__rc_cgen15752.d15867[404]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* make-eq-hashtable */
  sg__rc_cgen15752.d15867[960] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[960],SG_SYMBOL(sg__rc_cgen15752.d15867[160]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* current-macro-env */
  sg__rc_cgen15752.d15867[961] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[961],SG_SYMBOL(sg__rc_cgen15752.d15867[385]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* vector-copy */
  sg__rc_cgen15752.d15867[962] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[962],SG_SYMBOL(sg__rc_cgen15752.d15867[23]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* variable? */
  sg__rc_cgen15752.d15867[963] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[963],SG_SYMBOL(sg__rc_cgen15752.d15867[125]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* er-rename */
  sg__rc_cgen15752.d15867[965] = SG_MAKE_STRING("rec");
  sg__rc_cgen15752.d15867[964] = Sg_Intern(sg__rc_cgen15752.d15867[965]); /* rec */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[107]))->name = sg__rc_cgen15752.d15867[964];/* rec */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6069]))[85] = SG_WORD(sg__rc_cgen15752.d15867[961]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6069]))[103] = SG_WORD(sg__rc_cgen15752.d15867[962]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6069]))[110] = SG_WORD(sg__rc_cgen15752.d15867[963]);
  sg__rc_cgen15752.d15867[967] = SG_MAKE_STRING("rename");
  sg__rc_cgen15752.d15867[966] = Sg_Intern(sg__rc_cgen15752.d15867[967]); /* rename */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[108]))->name = sg__rc_cgen15752.d15867[966];/* rename */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6183]))[4] = SG_WORD(sg__rc_cgen15752.d15867[960]);
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[109]))->name = sg__rc_cgen15752.d15867[947];/* er-macro-transformer */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6199]))[3] = SG_WORD(sg__rc_cgen15752.d15867[959]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6199]))[11] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6199]))[16] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15752.d15868[110]))->name = sg__rc_cgen15752.d15867[947];/* er-macro-transformer */
  sg__rc_cgen15752.d15867[968] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15752.d15867[968],SG_SYMBOL(sg__rc_cgen15752.d15867[947]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15752.d15867[5]),FALSE); /* er-macro-transformer */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[3] = SG_WORD(sg__rc_cgen15752.d15867[2]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[7] = SG_WORD(sg__rc_cgen15752.d15867[13]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[11] = SG_WORD(sg__rc_cgen15752.d15867[16]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[15] = SG_WORD(sg__rc_cgen15752.d15867[38]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[19] = SG_WORD(sg__rc_cgen15752.d15867[43]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[23] = SG_WORD(sg__rc_cgen15752.d15867[49]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[27] = SG_WORD(sg__rc_cgen15752.d15867[58]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[31] = SG_WORD(sg__rc_cgen15752.d15867[64]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[35] = SG_WORD(sg__rc_cgen15752.d15867[67]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[39] = SG_WORD(sg__rc_cgen15752.d15867[84]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[43] = SG_WORD(sg__rc_cgen15752.d15867[92]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[47] = SG_WORD(sg__rc_cgen15752.d15867[97]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[51] = SG_WORD(sg__rc_cgen15752.d15867[103]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[55] = SG_WORD(sg__rc_cgen15752.d15867[112]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[59] = SG_WORD(sg__rc_cgen15752.d15867[115]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[61] = SG_WORD(sg__rc_cgen15752.d15867[5]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[64] = SG_WORD(sg__rc_cgen15752.d15867[135]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[67] = SG_WORD(sg__rc_cgen15752.d15867[138]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[70] = SG_WORD(sg__rc_cgen15752.d15867[141]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[74] = SG_WORD(sg__rc_cgen15752.d15867[144]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[78] = SG_WORD(sg__rc_cgen15752.d15867[146]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[90] = SG_WORD(sg__rc_cgen15752.d15867[149]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[94] = SG_WORD(sg__rc_cgen15752.d15867[152]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[96] = SG_WORD(sg__rc_cgen15752.d15867[153]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[98] = SG_WORD(sg__rc_cgen15752.d15867[156]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[102] = SG_WORD(sg__rc_cgen15752.d15867[157]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[104] = SG_WORD(sg__rc_cgen15752.d15867[158]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[106] = SG_WORD(sg__rc_cgen15752.d15867[159]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[112] = SG_WORD(sg__rc_cgen15752.d15867[162]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[114] = SG_WORD(sg__rc_cgen15752.d15867[163]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[118] = SG_WORD(sg__rc_cgen15752.d15867[177]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[122] = SG_WORD(sg__rc_cgen15752.d15867[184]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[128] = SG_WORD(sg__rc_cgen15752.d15867[185]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[130] = SG_WORD(sg__rc_cgen15752.d15867[186]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[134] = SG_WORD(sg__rc_cgen15752.d15867[195]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[138] = SG_WORD(sg__rc_cgen15752.d15867[219]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[142] = SG_WORD(sg__rc_cgen15752.d15867[221]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[146] = SG_WORD(sg__rc_cgen15752.d15867[224]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[150] = SG_WORD(sg__rc_cgen15752.d15867[227]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[154] = SG_WORD(sg__rc_cgen15752.d15867[230]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[158] = SG_WORD(sg__rc_cgen15752.d15867[235]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[162] = SG_WORD(sg__rc_cgen15752.d15867[236]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[166] = SG_WORD(sg__rc_cgen15752.d15867[238]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[168] = SG_WORD(sg__rc_cgen15752.d15867[243]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[170] = SG_WORD(sg__rc_cgen15752.d15867[244]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[174] = SG_WORD(sg__rc_cgen15752.d15867[245]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[178] = SG_WORD(sg__rc_cgen15752.d15867[247]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[180] = SG_WORD(sg__rc_cgen15752.d15867[248]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[182] = SG_WORD(sg__rc_cgen15752.d15867[249]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[186] = SG_WORD(sg__rc_cgen15752.d15867[252]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[190] = SG_WORD(sg__rc_cgen15752.d15867[247]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[192] = SG_WORD(sg__rc_cgen15752.d15867[254]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[194] = SG_WORD(sg__rc_cgen15752.d15867[255]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[198] = SG_WORD(sg__rc_cgen15752.d15867[264]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[202] = SG_WORD(sg__rc_cgen15752.d15867[276]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[206] = SG_WORD(sg__rc_cgen15752.d15867[284]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[210] = SG_WORD(sg__rc_cgen15752.d15867[288]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[214] = SG_WORD(sg__rc_cgen15752.d15867[296]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[218] = SG_WORD(sg__rc_cgen15752.d15867[300]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[222] = SG_WORD(sg__rc_cgen15752.d15867[332]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[226] = SG_WORD(sg__rc_cgen15752.d15867[333]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[230] = SG_WORD(sg__rc_cgen15752.d15867[238]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[232] = SG_WORD(sg__rc_cgen15752.d15867[335]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[234] = SG_WORD(sg__rc_cgen15752.d15867[336]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[238] = SG_WORD(sg__rc_cgen15752.d15867[339]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[242] = SG_WORD(sg__rc_cgen15752.d15867[238]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[244] = SG_WORD(sg__rc_cgen15752.d15867[341]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[246] = SG_WORD(sg__rc_cgen15752.d15867[342]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[250] = SG_WORD(sg__rc_cgen15752.d15867[345]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[254] = SG_WORD(sg__rc_cgen15752.d15867[238]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[256] = SG_WORD(sg__rc_cgen15752.d15867[347]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[258] = SG_WORD(sg__rc_cgen15752.d15867[348]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[262] = SG_WORD(sg__rc_cgen15752.d15867[353]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[266] = SG_WORD(sg__rc_cgen15752.d15867[366]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[270] = SG_WORD(sg__rc_cgen15752.d15867[367]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[274] = SG_WORD(sg__rc_cgen15752.d15867[369]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[276] = SG_WORD(sg__rc_cgen15752.d15867[374]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[278] = SG_WORD(sg__rc_cgen15752.d15867[375]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[282] = SG_WORD(sg__rc_cgen15752.d15867[391]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[286] = SG_WORD(sg__rc_cgen15752.d15867[398]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[290] = SG_WORD(sg__rc_cgen15752.d15867[474]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[294] = SG_WORD(sg__rc_cgen15752.d15867[477]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[298] = SG_WORD(sg__rc_cgen15752.d15867[484]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[302] = SG_WORD(sg__rc_cgen15752.d15867[489]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[306] = SG_WORD(sg__rc_cgen15752.d15867[514]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[310] = SG_WORD(sg__rc_cgen15752.d15867[524]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[314] = SG_WORD(sg__rc_cgen15752.d15867[527]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[318] = SG_WORD(sg__rc_cgen15752.d15867[533]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[322] = SG_WORD(sg__rc_cgen15752.d15867[542]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[326] = SG_WORD(sg__rc_cgen15752.d15867[549]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[330] = SG_WORD(sg__rc_cgen15752.d15867[569]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[334] = SG_WORD(sg__rc_cgen15752.d15867[576]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[338] = SG_WORD(sg__rc_cgen15752.d15867[577]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[342] = SG_WORD(sg__rc_cgen15752.d15867[238]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[344] = SG_WORD(sg__rc_cgen15752.d15867[579]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[346] = SG_WORD(sg__rc_cgen15752.d15867[580]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[350] = SG_WORD(sg__rc_cgen15752.d15867[587]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[354] = SG_WORD(sg__rc_cgen15752.d15867[597]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[358] = SG_WORD(sg__rc_cgen15752.d15867[656]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[362] = SG_WORD(sg__rc_cgen15752.d15867[689]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[366] = SG_WORD(sg__rc_cgen15752.d15867[711]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[370] = SG_WORD(sg__rc_cgen15752.d15867[713]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[374] = SG_WORD(sg__rc_cgen15752.d15867[717]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[378] = SG_WORD(sg__rc_cgen15752.d15867[732]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[382] = SG_WORD(sg__rc_cgen15752.d15867[744]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[386] = SG_WORD(sg__rc_cgen15752.d15867[778]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[390] = SG_WORD(sg__rc_cgen15752.d15867[789]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[394] = SG_WORD(sg__rc_cgen15752.d15867[844]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[398] = SG_WORD(sg__rc_cgen15752.d15867[856]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[402] = SG_WORD(sg__rc_cgen15752.d15867[860]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[406] = SG_WORD(sg__rc_cgen15752.d15867[884]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[408] = SG_WORD(sg__rc_cgen15752.d15867[885]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[411] = SG_WORD(sg__rc_cgen15752.d15867[887]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[415] = SG_WORD(sg__rc_cgen15752.d15867[899]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[419] = SG_WORD(sg__rc_cgen15752.d15867[928]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[423] = SG_WORD(sg__rc_cgen15752.d15867[935]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[427] = SG_WORD(sg__rc_cgen15752.d15867[945]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[431] = SG_WORD(sg__rc_cgen15752.d15867[958]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15752.d15866[6223]))[435] = SG_WORD(sg__rc_cgen15752.d15867[968]);
  sg__rc_cgen15752.d15867[970] = SG_MAKE_STRING("(core)");
  sg__rc_cgen15752.d15867[969] = Sg_Intern(sg__rc_cgen15752.d15867[970]); /* (core) */
  Sg_ImportLibrary(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[969]);

  sg__rc_cgen15752.d15867[972] = SG_MAKE_STRING("(core base)");
  sg__rc_cgen15752.d15867[971] = Sg_Intern(sg__rc_cgen15752.d15867[972]); /* (core base) */
  Sg_ImportLibrary(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[971]);

  sg__rc_cgen15752.d15867[974] = SG_MAKE_STRING("(core errors)");
  sg__rc_cgen15752.d15867[973] = Sg_Intern(sg__rc_cgen15752.d15867[974]); /* (core errors) */
  Sg_ImportLibrary(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[973]);

  sg__rc_cgen15752.d15867[976] = SG_MAKE_STRING("(sagittarius)");
  sg__rc_cgen15752.d15867[975] = Sg_Intern(sg__rc_cgen15752.d15867[976]); /* (sagittarius) */
  Sg_ImportLibrary(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[975]);

  sg__rc_cgen15752.d15867[978] = SG_MAKE_STRING("(sagittarius vm)");
  sg__rc_cgen15752.d15867[977] = Sg_Intern(sg__rc_cgen15752.d15867[978]); /* (sagittarius vm) */
  Sg_ImportLibrary(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[977]);

  do {
    /* (er-macro-transformer generate-temporaries syntax->datum datum->syntax bound-identifier=? free-identifier=? make-variable-transformer make-identifier make-core-parameter variable-transformer? make-macro-transformer current-macro-env current-usage-env compile-syntax compile-syntax-case) */ 
    SgObject G15877 = SG_NIL, G15878 = SG_NIL;
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[947]); /* er-macro-transformer */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[862]); /* generate-temporaries */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[858]); /* syntax->datum */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[846]); /* datum->syntax */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[260]); /* bound-identifier=? */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[128]); /* free-identifier=? */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[943]); /* make-variable-transformer */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[34]); /* make-identifier */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[154]); /* make-core-parameter */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[897]); /* variable-transformer? */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[933]); /* make-macro-transformer */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[160]); /* current-macro-env */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[121]); /* current-usage-env */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[687]); /* compile-syntax */ 
    SG_APPEND1(G15877, G15878, sg__rc_cgen15752.d15867[472]); /* compile-syntax-case */ 
    sg__rc_cgen15752.d15867[980] = G15877;
  } while (0);
  do {
    /* ((er-macro-transformer generate-temporaries syntax->datum datum->syntax bound-identifier=? free-identifier=? make-variable-transformer make-identifier make-core-parameter variable-transformer? make-macro-transformer current-macro-env current-usage-env compile-syntax compile-syntax-case)) */ 
    SgObject G15879 = SG_NIL, G15880 = SG_NIL;
    SG_APPEND1(G15879, G15880, sg__rc_cgen15752.d15867[980]); /* (er-macro-transformer generate-temporaries syntax->datum datum->syntax bound-identifier=? free-identifier=? make-variable-transformer make-identifier make-core-parameter variable-transformer? make-macro-transformer current-macro-env current-usage-env compile-syntax compile-syntax-case) */ 
    sg__rc_cgen15752.d15867[979] = G15879;
  } while (0);
  Sg_LibraryExportedSet(sg__rc_cgen15752.d15867[5], sg__rc_cgen15752.d15867[979]);

  Sg_VM()->currentLibrary = sg__rc_cgen15752.d15867[5];
  Sg_VMExecute(SG_OBJ(G15753));
  Sg_VM()->currentLibrary = save;
}
