/* Generated automatically from ../boot/compiler-aux.scm. DO NOT EDIT! */
#define LIBSAGITTARIUS_BODY 
#include <sagittarius.h>
#include <sagittarius/private.h>
static struct sg__rc_cgen15885Rec {
  SgObject d15909[115];
  SgWord d15910[102];
  SgCodeBuilder d15911[2];
} sg__rc_cgen15885 = {
  {  /* SgObject d15909 */
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
    SG_UNBOUND,
  },
  {  /* SgWord d15910 */
    /* ensure-library-name */0x00000045    /*   0 LREF_PUSH */,
    0x00000003    /*   1 CONST */,
    SG_WORD(SG_UNDEF) /* null */,
    0x00000020    /*   3 BNEQV */,
    SG_WORD(3),
    0x00000061    /*   5 CONST_RET */,
    SG_WORD(SG_UNDEF) /* (core) */,
    0x00000045    /*   7 LREF_PUSH */,
    0x00000003    /*   8 CONST */,
    SG_WORD(SG_UNDEF) /* sagittarius */,
    0x00000020    /*  10 BNEQV */,
    SG_WORD(3),
    0x00000061    /*  12 CONST_RET */,
    SG_WORD(SG_UNDEF) /* (sagittarius) */,
    0x00000045    /*  14 LREF_PUSH */,
    0x00000003    /*  15 CONST */,
    SG_WORD(SG_UNDEF) /* base */,
    0x00000020    /*  17 BNEQV */,
    SG_WORD(3),
    0x00000061    /*  19 CONST_RET */,
    SG_WORD(SG_UNDEF) /* (core base) */,
    0x00000045    /*  21 LREF_PUSH */,
    0x00000003    /*  22 CONST */,
    SG_WORD(SG_UNDEF) /* r6rs-script */,
    0x00000020    /*  24 BNEQV */,
    SG_WORD(3),
    0x00000061    /*  26 CONST_RET */,
    SG_WORD(SG_UNDEF) /* (r6rs-script) */,
    0x00000048    /*  28 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* ensure-library-name */,
    0x00000048    /*  30 CONST_PUSH */,
    SG_WORD(SG_UNDEF) /* invalid library tag: */,
    0x00000045    /*  32 LREF_PUSH */,
    0x0000034b    /*  33 GREF_TAIL_CALL */,
    SG_WORD(SG_UNDEF) /* #<identifier error#sagittarius.compiler.util> */,
    0x0000002f    /*  35 RET */,
    /* #f */0x00000034    /*   0 LIBRARY */,
    SG_WORD(SG_UNDEF) /* #<library sagittarius.compiler.util> */,
    0x00000029    /*   2 CLOSURE */,
    SG_WORD(SG_OBJ(&sg__rc_cgen15885.d15911[0])) /* #<code-builder ensure-library-name (1 0 0)> */,
    0x00000033    /*   4 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier ensure-library-name#sagittarius.compiler.util> */,
    0x00000003    /*   6 CONST */,
    SG_WORD(SG_UNDEF) /* (($UNDEF . 0) ($DEFINE . 1) ($LREF . 2) ($LSET . 3) ($GREF . 4) ($GSET . 5) ($CONST . 6) ($IF . 7) ($LET . 8) ($LAMBDA . 9) ($RECEIVE . 10) ($LABEL . 11) ($SEQ . 12) ($CALL . 13) ($ASM . 14) ($IT . 15) ($LIST . 16) ($LIBRARY . 17)) */,
    0x00000133    /*   8 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier .intermediate-tags.#sagittarius.compiler.util> */,
    0x00000004    /*  10 CONSTI */,
    0x00000133    /*  11 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $UNDEF#sagittarius.compiler.util> */,
    0x00000104    /*  13 CONSTI */,
    0x00000133    /*  14 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $DEFINE#sagittarius.compiler.util> */,
    0x00000204    /*  16 CONSTI */,
    0x00000133    /*  17 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LREF#sagittarius.compiler.util> */,
    0x00000304    /*  19 CONSTI */,
    0x00000133    /*  20 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LSET#sagittarius.compiler.util> */,
    0x00000404    /*  22 CONSTI */,
    0x00000133    /*  23 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $GREF#sagittarius.compiler.util> */,
    0x00000504    /*  25 CONSTI */,
    0x00000133    /*  26 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $GSET#sagittarius.compiler.util> */,
    0x00000604    /*  28 CONSTI */,
    0x00000133    /*  29 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $CONST#sagittarius.compiler.util> */,
    0x00000704    /*  31 CONSTI */,
    0x00000133    /*  32 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $IF#sagittarius.compiler.util> */,
    0x00000804    /*  34 CONSTI */,
    0x00000133    /*  35 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LET#sagittarius.compiler.util> */,
    0x00000904    /*  37 CONSTI */,
    0x00000133    /*  38 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LAMBDA#sagittarius.compiler.util> */,
    0x00000a04    /*  40 CONSTI */,
    0x00000133    /*  41 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $RECEIVE#sagittarius.compiler.util> */,
    0x00000b04    /*  43 CONSTI */,
    0x00000133    /*  44 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LABEL#sagittarius.compiler.util> */,
    0x00000c04    /*  46 CONSTI */,
    0x00000133    /*  47 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $SEQ#sagittarius.compiler.util> */,
    0x00000d04    /*  49 CONSTI */,
    0x00000133    /*  50 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $CALL#sagittarius.compiler.util> */,
    0x00000e04    /*  52 CONSTI */,
    0x00000133    /*  53 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $ASM#sagittarius.compiler.util> */,
    0x00000f04    /*  55 CONSTI */,
    0x00000133    /*  56 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $IT#sagittarius.compiler.util> */,
    0x00001004    /*  58 CONSTI */,
    0x00000133    /*  59 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LIST#sagittarius.compiler.util> */,
    0x00001104    /*  61 CONSTI */,
    0x00000133    /*  62 DEFINE */,
    SG_WORD(SG_UNDEF) /* #<identifier $LIBRARY#sagittarius.compiler.util> */,
    0x00000002    /*  64 UNDEF */,
    0x0000002f    /*  65 RET */,
  },
  {  /* SgCodeBuilder d15911 */
    
    SG_STATIC_CODE_BUILDER( /* ensure-library-name */
      (SgWord *)SG_OBJ(&sg__rc_cgen15885.d15910[0]), SG_FALSE, 1, 0, 0, 14, 36),
    
    SG_STATIC_CODE_BUILDER( /* #f */
      (SgWord *)SG_OBJ(&sg__rc_cgen15885.d15910[36]), SG_FALSE, 0, 0, 0, 0, 66),
  },
};
static SgCodeBuilder *G15886 = 
   SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15885.d15911[1]));
void Sg__Init_sagittarius_compiler_util() {
  SgObject save = Sg_VM()->currentLibrary;

  sg__rc_cgen15885.d15909[2] = SG_MAKE_STRING("(sagittarius compiler util)");
  sg__rc_cgen15885.d15909[1] = Sg_Intern(sg__rc_cgen15885.d15909[2]); /* (sagittarius compiler util) */
  sg__rc_cgen15885.d15909[0] = Sg_FindLibrary(SG_SYMBOL(sg__rc_cgen15885.d15909[1]), TRUE);
  sg__rc_cgen15885.d15909[4] = SG_MAKE_STRING("null");
  sg__rc_cgen15885.d15909[3] = Sg_MakeKeyword(SG_STRING(sg__rc_cgen15885.d15909[4])); /* null */
  sg__rc_cgen15885.d15909[7] = SG_MAKE_STRING("core");
  sg__rc_cgen15885.d15909[6] = Sg_Intern(sg__rc_cgen15885.d15909[7]); /* core */
  do {
    /* (core) */ 
    SgObject G15912 = SG_NIL, G15913 = SG_NIL;
    SG_APPEND1(G15912, G15913, sg__rc_cgen15885.d15909[6]); /* core */ 
    sg__rc_cgen15885.d15909[5] = G15912;
  } while (0);
  sg__rc_cgen15885.d15909[9] = SG_MAKE_STRING("sagittarius");
  sg__rc_cgen15885.d15909[8] = Sg_MakeKeyword(SG_STRING(sg__rc_cgen15885.d15909[9])); /* sagittarius */
  sg__rc_cgen15885.d15909[11] = Sg_Intern(sg__rc_cgen15885.d15909[9]); /* sagittarius */
  do {
    /* (sagittarius) */ 
    SgObject G15914 = SG_NIL, G15915 = SG_NIL;
    SG_APPEND1(G15914, G15915, sg__rc_cgen15885.d15909[11]); /* sagittarius */ 
    sg__rc_cgen15885.d15909[10] = G15914;
  } while (0);
  sg__rc_cgen15885.d15909[13] = SG_MAKE_STRING("base");
  sg__rc_cgen15885.d15909[12] = Sg_MakeKeyword(SG_STRING(sg__rc_cgen15885.d15909[13])); /* base */
  sg__rc_cgen15885.d15909[15] = Sg_Intern(sg__rc_cgen15885.d15909[13]); /* base */
  do {
    /* (core base) */ 
    SgObject G15916 = SG_NIL, G15917 = SG_NIL;
    SG_APPEND1(G15916, G15917, sg__rc_cgen15885.d15909[6]); /* core */ 
    SG_APPEND1(G15916, G15917, sg__rc_cgen15885.d15909[15]); /* base */ 
    sg__rc_cgen15885.d15909[14] = G15916;
  } while (0);
  sg__rc_cgen15885.d15909[17] = SG_MAKE_STRING("r6rs-script");
  sg__rc_cgen15885.d15909[16] = Sg_MakeKeyword(SG_STRING(sg__rc_cgen15885.d15909[17])); /* r6rs-script */
  sg__rc_cgen15885.d15909[19] = Sg_Intern(sg__rc_cgen15885.d15909[17]); /* r6rs-script */
  do {
    /* (r6rs-script) */ 
    SgObject G15918 = SG_NIL, G15919 = SG_NIL;
    SG_APPEND1(G15918, G15919, sg__rc_cgen15885.d15909[19]); /* r6rs-script */ 
    sg__rc_cgen15885.d15909[18] = G15918;
  } while (0);
  sg__rc_cgen15885.d15909[21] = SG_MAKE_STRING("ensure-library-name");
  sg__rc_cgen15885.d15909[20] = Sg_Intern(sg__rc_cgen15885.d15909[21]); /* ensure-library-name */
  sg__rc_cgen15885.d15909[22] = SG_MAKE_STRING("invalid library tag:");
  sg__rc_cgen15885.d15909[23] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15885.d15909[25] = SG_MAKE_STRING("error");
  sg__rc_cgen15885.d15909[24] = Sg_Intern(sg__rc_cgen15885.d15909[25]); /* error */
  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[23],SG_SYMBOL(sg__rc_cgen15885.d15909[24]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* error */
  SG_CODE_BUILDER(SG_OBJ(&sg__rc_cgen15885.d15911[0]))->name = sg__rc_cgen15885.d15909[20];/* ensure-library-name */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[2] = SG_WORD(sg__rc_cgen15885.d15909[3]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[6] = SG_WORD(sg__rc_cgen15885.d15909[5]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[9] = SG_WORD(sg__rc_cgen15885.d15909[8]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[13] = SG_WORD(sg__rc_cgen15885.d15909[10]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[16] = SG_WORD(sg__rc_cgen15885.d15909[12]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[20] = SG_WORD(sg__rc_cgen15885.d15909[14]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[23] = SG_WORD(sg__rc_cgen15885.d15909[16]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[27] = SG_WORD(sg__rc_cgen15885.d15909[18]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[29] = SG_WORD(sg__rc_cgen15885.d15909[20]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[31] = SG_WORD(sg__rc_cgen15885.d15909[22]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[0]))[34] = SG_WORD(sg__rc_cgen15885.d15909[23]);
  sg__rc_cgen15885.d15909[26] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[26],SG_SYMBOL(sg__rc_cgen15885.d15909[20]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* ensure-library-name */
  sg__rc_cgen15885.d15909[30] = SG_MAKE_STRING("$UNDEF");
  sg__rc_cgen15885.d15909[29] = Sg_Intern(sg__rc_cgen15885.d15909[30]); /* $UNDEF */
  do {
    /* ($UNDEF . 0) */ 
    SgObject G15920 = SG_NIL, G15921 = SG_NIL;
    SG_APPEND1(G15920, G15921, sg__rc_cgen15885.d15909[29]); /* $UNDEF */ 
    SG_SET_CDR(G15921, SG_MAKE_INT(0)); /* #<<cgen-scheme-integer> 0x1130b8860> */
    sg__rc_cgen15885.d15909[28] = G15920;
  } while (0);
  sg__rc_cgen15885.d15909[33] = SG_MAKE_STRING("$DEFINE");
  sg__rc_cgen15885.d15909[32] = Sg_Intern(sg__rc_cgen15885.d15909[33]); /* $DEFINE */
  do {
    /* ($DEFINE . 1) */ 
    SgObject G15922 = SG_NIL, G15923 = SG_NIL;
    SG_APPEND1(G15922, G15923, sg__rc_cgen15885.d15909[32]); /* $DEFINE */ 
    SG_SET_CDR(G15923, SG_MAKE_INT(1U)); /* #<<cgen-scheme-integer> 0x1130beb20> */
    sg__rc_cgen15885.d15909[31] = G15922;
  } while (0);
  sg__rc_cgen15885.d15909[36] = SG_MAKE_STRING("$LREF");
  sg__rc_cgen15885.d15909[35] = Sg_Intern(sg__rc_cgen15885.d15909[36]); /* $LREF */
  do {
    /* ($LREF . 2) */ 
    SgObject G15924 = SG_NIL, G15925 = SG_NIL;
    SG_APPEND1(G15924, G15925, sg__rc_cgen15885.d15909[35]); /* $LREF */ 
    SG_SET_CDR(G15925, SG_MAKE_INT(2U)); /* #<<cgen-scheme-integer> 0x1130c4fa0> */
    sg__rc_cgen15885.d15909[34] = G15924;
  } while (0);
  sg__rc_cgen15885.d15909[39] = SG_MAKE_STRING("$LSET");
  sg__rc_cgen15885.d15909[38] = Sg_Intern(sg__rc_cgen15885.d15909[39]); /* $LSET */
  do {
    /* ($LSET . 3) */ 
    SgObject G15926 = SG_NIL, G15927 = SG_NIL;
    SG_APPEND1(G15926, G15927, sg__rc_cgen15885.d15909[38]); /* $LSET */ 
    SG_SET_CDR(G15927, SG_MAKE_INT(3U)); /* #<<cgen-scheme-integer> 0x1130cbd60> */
    sg__rc_cgen15885.d15909[37] = G15926;
  } while (0);
  sg__rc_cgen15885.d15909[42] = SG_MAKE_STRING("$GREF");
  sg__rc_cgen15885.d15909[41] = Sg_Intern(sg__rc_cgen15885.d15909[42]); /* $GREF */
  do {
    /* ($GREF . 4) */ 
    SgObject G15928 = SG_NIL, G15929 = SG_NIL;
    SG_APPEND1(G15928, G15929, sg__rc_cgen15885.d15909[41]); /* $GREF */ 
    SG_SET_CDR(G15929, SG_MAKE_INT(4U)); /* #<<cgen-scheme-integer> 0x1130d2620> */
    sg__rc_cgen15885.d15909[40] = G15928;
  } while (0);
  sg__rc_cgen15885.d15909[45] = SG_MAKE_STRING("$GSET");
  sg__rc_cgen15885.d15909[44] = Sg_Intern(sg__rc_cgen15885.d15909[45]); /* $GSET */
  do {
    /* ($GSET . 5) */ 
    SgObject G15930 = SG_NIL, G15931 = SG_NIL;
    SG_APPEND1(G15930, G15931, sg__rc_cgen15885.d15909[44]); /* $GSET */ 
    SG_SET_CDR(G15931, SG_MAKE_INT(5U)); /* #<<cgen-scheme-integer> 0x1130da780> */
    sg__rc_cgen15885.d15909[43] = G15930;
  } while (0);
  sg__rc_cgen15885.d15909[48] = SG_MAKE_STRING("$CONST");
  sg__rc_cgen15885.d15909[47] = Sg_Intern(sg__rc_cgen15885.d15909[48]); /* $CONST */
  do {
    /* ($CONST . 6) */ 
    SgObject G15932 = SG_NIL, G15933 = SG_NIL;
    SG_APPEND1(G15932, G15933, sg__rc_cgen15885.d15909[47]); /* $CONST */ 
    SG_SET_CDR(G15933, SG_MAKE_INT(6U)); /* #<<cgen-scheme-integer> 0x1130e2540> */
    sg__rc_cgen15885.d15909[46] = G15932;
  } while (0);
  sg__rc_cgen15885.d15909[51] = SG_MAKE_STRING("$IF");
  sg__rc_cgen15885.d15909[50] = Sg_Intern(sg__rc_cgen15885.d15909[51]); /* $IF */
  do {
    /* ($IF . 7) */ 
    SgObject G15934 = SG_NIL, G15935 = SG_NIL;
    SG_APPEND1(G15934, G15935, sg__rc_cgen15885.d15909[50]); /* $IF */ 
    SG_SET_CDR(G15935, SG_MAKE_INT(7U)); /* #<<cgen-scheme-integer> 0x1130ea5e0> */
    sg__rc_cgen15885.d15909[49] = G15934;
  } while (0);
  sg__rc_cgen15885.d15909[54] = SG_MAKE_STRING("$LET");
  sg__rc_cgen15885.d15909[53] = Sg_Intern(sg__rc_cgen15885.d15909[54]); /* $LET */
  do {
    /* ($LET . 8) */ 
    SgObject G15936 = SG_NIL, G15937 = SG_NIL;
    SG_APPEND1(G15936, G15937, sg__rc_cgen15885.d15909[53]); /* $LET */ 
    SG_SET_CDR(G15937, SG_MAKE_INT(8U)); /* #<<cgen-scheme-integer> 0x1130f6040> */
    sg__rc_cgen15885.d15909[52] = G15936;
  } while (0);
  sg__rc_cgen15885.d15909[57] = SG_MAKE_STRING("$LAMBDA");
  sg__rc_cgen15885.d15909[56] = Sg_Intern(sg__rc_cgen15885.d15909[57]); /* $LAMBDA */
  do {
    /* ($LAMBDA . 9) */ 
    SgObject G15938 = SG_NIL, G15939 = SG_NIL;
    SG_APPEND1(G15938, G15939, sg__rc_cgen15885.d15909[56]); /* $LAMBDA */ 
    SG_SET_CDR(G15939, SG_MAKE_INT(9U)); /* #<<cgen-scheme-integer> 0x1130fc260> */
    sg__rc_cgen15885.d15909[55] = G15938;
  } while (0);
  sg__rc_cgen15885.d15909[60] = SG_MAKE_STRING("$RECEIVE");
  sg__rc_cgen15885.d15909[59] = Sg_Intern(sg__rc_cgen15885.d15909[60]); /* $RECEIVE */
  do {
    /* ($RECEIVE . 10) */ 
    SgObject G15940 = SG_NIL, G15941 = SG_NIL;
    SG_APPEND1(G15940, G15941, sg__rc_cgen15885.d15909[59]); /* $RECEIVE */ 
    SG_SET_CDR(G15941, SG_MAKE_INT(10U)); /* #<<cgen-scheme-integer> 0x113108300> */
    sg__rc_cgen15885.d15909[58] = G15940;
  } while (0);
  sg__rc_cgen15885.d15909[63] = SG_MAKE_STRING("$LABEL");
  sg__rc_cgen15885.d15909[62] = Sg_Intern(sg__rc_cgen15885.d15909[63]); /* $LABEL */
  do {
    /* ($LABEL . 11) */ 
    SgObject G15942 = SG_NIL, G15943 = SG_NIL;
    SG_APPEND1(G15942, G15943, sg__rc_cgen15885.d15909[62]); /* $LABEL */ 
    SG_SET_CDR(G15943, SG_MAKE_INT(11U)); /* #<<cgen-scheme-integer> 0x113114cc0> */
    sg__rc_cgen15885.d15909[61] = G15942;
  } while (0);
  sg__rc_cgen15885.d15909[66] = SG_MAKE_STRING("$SEQ");
  sg__rc_cgen15885.d15909[65] = Sg_Intern(sg__rc_cgen15885.d15909[66]); /* $SEQ */
  do {
    /* ($SEQ . 12) */ 
    SgObject G15944 = SG_NIL, G15945 = SG_NIL;
    SG_APPEND1(G15944, G15945, sg__rc_cgen15885.d15909[65]); /* $SEQ */ 
    SG_SET_CDR(G15945, SG_MAKE_INT(12U)); /* #<<cgen-scheme-integer> 0x11311d820> */
    sg__rc_cgen15885.d15909[64] = G15944;
  } while (0);
  sg__rc_cgen15885.d15909[69] = SG_MAKE_STRING("$CALL");
  sg__rc_cgen15885.d15909[68] = Sg_Intern(sg__rc_cgen15885.d15909[69]); /* $CALL */
  do {
    /* ($CALL . 13) */ 
    SgObject G15946 = SG_NIL, G15947 = SG_NIL;
    SG_APPEND1(G15946, G15947, sg__rc_cgen15885.d15909[68]); /* $CALL */ 
    SG_SET_CDR(G15947, SG_MAKE_INT(13U)); /* #<<cgen-scheme-integer> 0x113129540> */
    sg__rc_cgen15885.d15909[67] = G15946;
  } while (0);
  sg__rc_cgen15885.d15909[72] = SG_MAKE_STRING("$ASM");
  sg__rc_cgen15885.d15909[71] = Sg_Intern(sg__rc_cgen15885.d15909[72]); /* $ASM */
  do {
    /* ($ASM . 14) */ 
    SgObject G15948 = SG_NIL, G15949 = SG_NIL;
    SG_APPEND1(G15948, G15949, sg__rc_cgen15885.d15909[71]); /* $ASM */ 
    SG_SET_CDR(G15949, SG_MAKE_INT(14U)); /* #<<cgen-scheme-integer> 0x11313f500> */
    sg__rc_cgen15885.d15909[70] = G15948;
  } while (0);
  sg__rc_cgen15885.d15909[75] = SG_MAKE_STRING("$IT");
  sg__rc_cgen15885.d15909[74] = Sg_Intern(sg__rc_cgen15885.d15909[75]); /* $IT */
  do {
    /* ($IT . 15) */ 
    SgObject G15950 = SG_NIL, G15951 = SG_NIL;
    SG_APPEND1(G15950, G15951, sg__rc_cgen15885.d15909[74]); /* $IT */ 
    SG_SET_CDR(G15951, SG_MAKE_INT(15U)); /* #<<cgen-scheme-integer> 0x113149e80> */
    sg__rc_cgen15885.d15909[73] = G15950;
  } while (0);
  sg__rc_cgen15885.d15909[78] = SG_MAKE_STRING("$LIST");
  sg__rc_cgen15885.d15909[77] = Sg_Intern(sg__rc_cgen15885.d15909[78]); /* $LIST */
  do {
    /* ($LIST . 16) */ 
    SgObject G15952 = SG_NIL, G15953 = SG_NIL;
    SG_APPEND1(G15952, G15953, sg__rc_cgen15885.d15909[77]); /* $LIST */ 
    SG_SET_CDR(G15953, SG_MAKE_INT(16U)); /* #<<cgen-scheme-integer> 0x113153520> */
    sg__rc_cgen15885.d15909[76] = G15952;
  } while (0);
  sg__rc_cgen15885.d15909[81] = SG_MAKE_STRING("$LIBRARY");
  sg__rc_cgen15885.d15909[80] = Sg_Intern(sg__rc_cgen15885.d15909[81]); /* $LIBRARY */
  do {
    /* ($LIBRARY . 17) */ 
    SgObject G15954 = SG_NIL, G15955 = SG_NIL;
    SG_APPEND1(G15954, G15955, sg__rc_cgen15885.d15909[80]); /* $LIBRARY */ 
    SG_SET_CDR(G15955, SG_MAKE_INT(17U)); /* #<<cgen-scheme-integer> 0x11315e020> */
    sg__rc_cgen15885.d15909[79] = G15954;
  } while (0);
  do {
    /* (($UNDEF . 0) ($DEFINE . 1) ($LREF . 2) ($LSET . 3) ($GREF . 4) ($GSET . 5) ($CONST . 6) ($IF . 7) ($LET . 8) ($LAMBDA . 9) ($RECEIVE . 10) ($LABEL . 11) ($SEQ . 12) ($CALL . 13) ($ASM . 14) ($IT . 15) ($LIST . 16) ($LIBRARY . 17)) */ 
    SgObject G15956 = SG_NIL, G15957 = SG_NIL;
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[28]); /* ($UNDEF . 0) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[31]); /* ($DEFINE . 1) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[34]); /* ($LREF . 2) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[37]); /* ($LSET . 3) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[40]); /* ($GREF . 4) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[43]); /* ($GSET . 5) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[46]); /* ($CONST . 6) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[49]); /* ($IF . 7) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[52]); /* ($LET . 8) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[55]); /* ($LAMBDA . 9) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[58]); /* ($RECEIVE . 10) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[61]); /* ($LABEL . 11) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[64]); /* ($SEQ . 12) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[67]); /* ($CALL . 13) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[70]); /* ($ASM . 14) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[73]); /* ($IT . 15) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[76]); /* ($LIST . 16) */ 
    SG_APPEND1(G15956, G15957, sg__rc_cgen15885.d15909[79]); /* ($LIBRARY . 17) */ 
    sg__rc_cgen15885.d15909[27] = G15956;
  } while (0);
  sg__rc_cgen15885.d15909[82] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  sg__rc_cgen15885.d15909[84] = SG_MAKE_STRING(".intermediate-tags.");
  sg__rc_cgen15885.d15909[83] = Sg_Intern(sg__rc_cgen15885.d15909[84]); /* .intermediate-tags. */
  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[82],SG_SYMBOL(sg__rc_cgen15885.d15909[83]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* .intermediate-tags. */
  sg__rc_cgen15885.d15909[85] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[85],SG_SYMBOL(sg__rc_cgen15885.d15909[29]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $UNDEF */
  sg__rc_cgen15885.d15909[86] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[86],SG_SYMBOL(sg__rc_cgen15885.d15909[32]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $DEFINE */
  sg__rc_cgen15885.d15909[87] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[87],SG_SYMBOL(sg__rc_cgen15885.d15909[35]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LREF */
  sg__rc_cgen15885.d15909[88] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[88],SG_SYMBOL(sg__rc_cgen15885.d15909[38]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LSET */
  sg__rc_cgen15885.d15909[89] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[89],SG_SYMBOL(sg__rc_cgen15885.d15909[41]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $GREF */
  sg__rc_cgen15885.d15909[90] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[90],SG_SYMBOL(sg__rc_cgen15885.d15909[44]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $GSET */
  sg__rc_cgen15885.d15909[91] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[91],SG_SYMBOL(sg__rc_cgen15885.d15909[47]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $CONST */
  sg__rc_cgen15885.d15909[92] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[92],SG_SYMBOL(sg__rc_cgen15885.d15909[50]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $IF */
  sg__rc_cgen15885.d15909[93] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[93],SG_SYMBOL(sg__rc_cgen15885.d15909[53]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LET */
  sg__rc_cgen15885.d15909[94] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[94],SG_SYMBOL(sg__rc_cgen15885.d15909[56]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LAMBDA */
  sg__rc_cgen15885.d15909[95] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[95],SG_SYMBOL(sg__rc_cgen15885.d15909[59]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $RECEIVE */
  sg__rc_cgen15885.d15909[96] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[96],SG_SYMBOL(sg__rc_cgen15885.d15909[62]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LABEL */
  sg__rc_cgen15885.d15909[97] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[97],SG_SYMBOL(sg__rc_cgen15885.d15909[65]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $SEQ */
  sg__rc_cgen15885.d15909[98] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[98],SG_SYMBOL(sg__rc_cgen15885.d15909[68]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $CALL */
  sg__rc_cgen15885.d15909[99] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[99],SG_SYMBOL(sg__rc_cgen15885.d15909[71]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $ASM */
  sg__rc_cgen15885.d15909[100] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[100],SG_SYMBOL(sg__rc_cgen15885.d15909[74]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $IT */
  sg__rc_cgen15885.d15909[101] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[101],SG_SYMBOL(sg__rc_cgen15885.d15909[77]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LIST */
  sg__rc_cgen15885.d15909[102] = Sg_MakeRawIdentifier(NULL, NULL, NULL, NULL, 0);

  SG_INIT_IDENTIFIER(sg__rc_cgen15885.d15909[102],SG_SYMBOL(sg__rc_cgen15885.d15909[80]),SG_NIL,SG_FALSE,SG_LIBRARY(sg__rc_cgen15885.d15909[0]),FALSE); /* $LIBRARY */
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[1] = SG_WORD(sg__rc_cgen15885.d15909[0]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[5] = SG_WORD(sg__rc_cgen15885.d15909[26]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[7] = SG_WORD(sg__rc_cgen15885.d15909[27]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[9] = SG_WORD(sg__rc_cgen15885.d15909[82]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[12] = SG_WORD(sg__rc_cgen15885.d15909[85]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[15] = SG_WORD(sg__rc_cgen15885.d15909[86]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[18] = SG_WORD(sg__rc_cgen15885.d15909[87]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[21] = SG_WORD(sg__rc_cgen15885.d15909[88]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[24] = SG_WORD(sg__rc_cgen15885.d15909[89]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[27] = SG_WORD(sg__rc_cgen15885.d15909[90]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[30] = SG_WORD(sg__rc_cgen15885.d15909[91]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[33] = SG_WORD(sg__rc_cgen15885.d15909[92]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[36] = SG_WORD(sg__rc_cgen15885.d15909[93]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[39] = SG_WORD(sg__rc_cgen15885.d15909[94]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[42] = SG_WORD(sg__rc_cgen15885.d15909[95]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[45] = SG_WORD(sg__rc_cgen15885.d15909[96]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[48] = SG_WORD(sg__rc_cgen15885.d15909[97]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[51] = SG_WORD(sg__rc_cgen15885.d15909[98]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[54] = SG_WORD(sg__rc_cgen15885.d15909[99]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[57] = SG_WORD(sg__rc_cgen15885.d15909[100]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[60] = SG_WORD(sg__rc_cgen15885.d15909[101]);
  ((SgWord*)SG_OBJ(&sg__rc_cgen15885.d15910[36]))[63] = SG_WORD(sg__rc_cgen15885.d15909[102]);
  sg__rc_cgen15885.d15909[104] = SG_MAKE_STRING("(core)");
  sg__rc_cgen15885.d15909[103] = Sg_Intern(sg__rc_cgen15885.d15909[104]); /* (core) */
  Sg_ImportLibrary(sg__rc_cgen15885.d15909[0], sg__rc_cgen15885.d15909[103]);

  sg__rc_cgen15885.d15909[106] = SG_MAKE_STRING("(core errors)");
  sg__rc_cgen15885.d15909[105] = Sg_Intern(sg__rc_cgen15885.d15909[106]); /* (core errors) */
  Sg_ImportLibrary(sg__rc_cgen15885.d15909[0], sg__rc_cgen15885.d15909[105]);

  sg__rc_cgen15885.d15909[108] = SG_MAKE_STRING("(sagittarius)");
  sg__rc_cgen15885.d15909[107] = Sg_Intern(sg__rc_cgen15885.d15909[108]); /* (sagittarius) */
  Sg_ImportLibrary(sg__rc_cgen15885.d15909[0], sg__rc_cgen15885.d15909[107]);

  sg__rc_cgen15885.d15909[110] = SG_MAKE_STRING("(sagittarius vm)");
  sg__rc_cgen15885.d15909[109] = Sg_Intern(sg__rc_cgen15885.d15909[110]); /* (sagittarius vm) */
  Sg_ImportLibrary(sg__rc_cgen15885.d15909[0], sg__rc_cgen15885.d15909[109]);

  sg__rc_cgen15885.d15909[114] = SG_MAKE_STRING("define-enum");
  sg__rc_cgen15885.d15909[113] = Sg_Intern(sg__rc_cgen15885.d15909[114]); /* define-enum */
  do {
    /* (ensure-library-name define-enum .intermediate-tags. $UNDEF $DEFINE $LREF $LSET $GREF $GSET $CONST $IF $LET $LAMBDA $RECEIVE $LABEL $SEQ $CALL $ASM $IT $LIST $LIBRARY) */ 
    SgObject G15958 = SG_NIL, G15959 = SG_NIL;
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[20]); /* ensure-library-name */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[113]); /* define-enum */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[83]); /* .intermediate-tags. */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[29]); /* $UNDEF */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[32]); /* $DEFINE */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[35]); /* $LREF */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[38]); /* $LSET */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[41]); /* $GREF */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[44]); /* $GSET */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[47]); /* $CONST */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[50]); /* $IF */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[53]); /* $LET */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[56]); /* $LAMBDA */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[59]); /* $RECEIVE */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[62]); /* $LABEL */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[65]); /* $SEQ */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[68]); /* $CALL */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[71]); /* $ASM */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[74]); /* $IT */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[77]); /* $LIST */ 
    SG_APPEND1(G15958, G15959, sg__rc_cgen15885.d15909[80]); /* $LIBRARY */ 
    sg__rc_cgen15885.d15909[112] = G15958;
  } while (0);
  do {
    /* ((ensure-library-name define-enum .intermediate-tags. $UNDEF $DEFINE $LREF $LSET $GREF $GSET $CONST $IF $LET $LAMBDA $RECEIVE $LABEL $SEQ $CALL $ASM $IT $LIST $LIBRARY)) */ 
    SgObject G15960 = SG_NIL, G15961 = SG_NIL;
    SG_APPEND1(G15960, G15961, sg__rc_cgen15885.d15909[112]); /* (ensure-library-name define-enum .intermediate-tags. $UNDEF $DEFINE $LREF $LSET $GREF $GSET $CONST $IF $LET $LAMBDA $RECEIVE $LABEL $SEQ $CALL $ASM $IT $LIST $LIBRARY) */ 
    sg__rc_cgen15885.d15909[111] = G15960;
  } while (0);
  Sg_LibraryExportedSet(sg__rc_cgen15885.d15909[0], sg__rc_cgen15885.d15909[111]);

  Sg_VM()->currentLibrary = sg__rc_cgen15885.d15909[0];
  Sg_VMExecute(SG_OBJ(G15886));
  Sg_VM()->currentLibrary = save;
}
